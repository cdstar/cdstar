package de.gwdg.cdstar.utils.test;

import static org.junit.Assert.fail;

public class TestUtils {

	@FunctionalInterface
	public interface CheckedRunnable<T extends Throwable> {
		void run() throws T;
	}

	@SuppressWarnings("unchecked")
	public static <T extends Exception> T assertRaises(Class<T> expect, CheckedRunnable<Exception> cb) {
		try {
			cb.run();
			fail("Expected exception of type " + expect.getName() + ", but no exception was thrown");
		} catch (final Exception e) {
			if (expect.isInstance(e))
				return (T) e;
			throw new AssertionError("Wrong exception thrown. Expected " + expect.getName() + " but got " + e.getClass().getName(), e);
		}
		throw new RuntimeException("I'm just here to make the compiler happy");
	}
}
