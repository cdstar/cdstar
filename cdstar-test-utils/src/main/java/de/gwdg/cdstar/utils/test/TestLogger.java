
package de.gwdg.cdstar.utils.test;

import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.junit.AssumptionViolatedException;
import org.junit.BeforeClass;
import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;
import org.slf4j.LoggerFactory;
import org.slf4j.bridge.SLF4JBridgeHandler;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.encoder.PatternLayoutEncoder;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.Appender;
import ch.qos.logback.core.AppenderBase;
import de.gwdg.cdstar.utils.test.TestUtils.CheckedRunnable;

/**
 * Capture slf4j logging events during a test and allow to inspect the captured
 * events. The root logger is set to WARN for the duration of the test to reduce
 * noise, but any logger of interest can be configured to also capture lower log
 * levels (DEBUG by default).
 *
 * If a test fails, all log events captured during the runtime of the test are
 * re-emitted under normal logging conditions. Successful tests remain silent.
 */
public class TestLogger implements TestRule {

	private final MemoryAppender appender;
	private final PatternLayoutEncoder encoder;

	private final HashMap<String, CapturedLoggerHolder> loggers;
	private int reentry;

	/**
	 * A thread-safe appender that caches all messages in-memory.
	 */
	private class MemoryAppender extends AppenderBase<ILoggingEvent> {

		private final List<ILoggingEvent> list = new ArrayList<>();

		@Override
		protected void append(ILoggingEvent e) {
			e.prepareForDeferredProcessing();
			synchronized (list) {
				list.add(e);
			}
		}

		List<ILoggingEvent> getEvents() {
			synchronized (list) {
				return new ArrayList<>(list);
			}
		}

		public void reset() {
			synchronized (list) {
				list.clear();
			}
		}
	}

	/**
	 * A container remembering the original state of a logger and reversing any
	 * changes when {@link #stopCapture()} is called.
	 *
	 * By default, the captured logger is silenced {@link Level#OFF}.
	 */
	private class CapturedLoggerHolder {
		private final Logger logger;
		private final Level testLevel;

		private Level origLevel;
		private boolean origAdditivity;
		private List<Appender<ILoggingEvent>> originalAppenders;


		public CapturedLoggerHolder(String name, Level level) {
			logger = (Logger) LoggerFactory.getLogger(name);
			testLevel = level;
		}

		private List<Appender<ILoggingEvent>> currentAppenderList() {
			final List<Appender<ILoggingEvent>> all = new ArrayList<>();
			logger.iteratorForAppenders().forEachRemaining(all::add);
			return all;
		}

		protected synchronized void startCapture() {
			origLevel = logger.getLevel();
			origAdditivity = logger.isAdditive();
			originalAppenders = currentAppenderList();

			originalAppenders.forEach(logger::detachAppender);
			logger.addAppender(appender);
			logger.setAdditive(false);
			setLevel(testLevel);
		}

		protected synchronized void stopCapture() {
			currentAppenderList().forEach(logger::detachAppender);
			logger.setLevel(origLevel);
			logger.setAdditive(origAdditivity);
			originalAppenders.forEach(logger::addAppender);
		}

		public void setLevel(Level level) {
			logger.setLevel(level);
		}
	}

	/**
	 * Create a new configured {@link TestLogger}. The 'ROOT' logger is set to WARN
	 * and all loggers specified here are set to DEBUG by default.
	 *
	 * @param loggerNames a list if logger names that should be captured as DEBUG
	 *                    instead of WARN.
	 */
	public TestLogger(String... loggerNames) {

		if (!SLF4JBridgeHandler.isInstalled()) {
			SLF4JBridgeHandler.removeHandlersForRootLogger(); // cannot be undone, unfortunately
			SLF4JBridgeHandler.install();
		}

		appender = new MemoryAppender();
		appender.setContext((LoggerContext) LoggerFactory.getILoggerFactory());
		appender.start();

		encoder = new PatternLayoutEncoder();
		encoder.setPattern("TEST %r %-5level [%thread] %logger{16}: %msg%n");
		encoder.setContext((LoggerContext) LoggerFactory.getILoggerFactory());
		encoder.start();

		loggers = new HashMap<>();
		setLevel(org.slf4j.Logger.ROOT_LOGGER_NAME, Level.WARN);
		for (final String name : loggerNames)
			setLevel(name, Level.DEBUG);
	}

	private synchronized void startCapture() {
		if (reentry++ > 0)
			return;

		loggers.values().forEach(CapturedLoggerHolder::startCapture);
	}

	private synchronized void stopCapture(boolean replayEvents) {
		if (--reentry < 0)
			throw new IllegalStateException("Unbalanced call to reset()");

		if (reentry > 0)
			return;

		loggers.values().forEach(CapturedLoggerHolder::stopCapture);
		if (replayEvents) {
			for (final ILoggingEvent event : appender.getEvents()) {
				((ch.qos.logback.classic.Logger) LoggerFactory.getLogger(event.getLoggerName())).callAppenders(event);
			}
		}
		appender.reset();
	}

	/**
	 * Configure the log-level of a logger.
	 *
	 * If called from within a test, the level change only affects future events
	 * during the same test.
	 */
	public synchronized TestLogger setLevel(String name, Level level) {
		loggers.compute(name, (key, value) -> {
			if (value == null) {
				value = new CapturedLoggerHolder(name, level);
				if (reentry > 0)
					value.startCapture();
				return value;
			}
			value.setLevel(level);
			return value;
		});
		return this;
	}

	@Override
	public Statement apply(Statement base, Description description) {
		if (description.isSuite())
			throw new AssertionError("TestLogger should not be run as a ClassRule.");

		return new Statement() {
			@Override
			public void evaluate() throws Throwable {
				run(base::evaluate);
			}
		};
	}

	/**
	 * Run a piece of code as if it were a test case wrapped by this logger.
	 *
	 * This can be used to run code in e.g. {@link BeforeClass} that produces log
	 * output, without wrapping the entire suite in a {@link TestLogger}.
	 */
	public TestLogger run(Failable runner) throws Throwable {
		startCapture();
		try {
			runner.run();
			stopCapture(false);
			return this;
		} catch (final AssumptionViolatedException e) {
			// Skipped tests should not emit logging
			stopCapture(false);
			throw e;
		} catch (final Throwable e) {
			stopCapture(true);
			throw e;
		}
	}

	@FunctionalInterface
	public interface Failable {
		void run() throws Throwable;
	}

	/**
	 * Return events collected so far.
	 */
	public List<ILoggingEvent> getEvents() {
		return appender.getEvents();
	}

	public List<ILoggingEvent> getEventsByLogger(String logger) {
		return getEvents().stream().filter(e -> e.getLoggerName().equals(logger)).collect(Collectors.toList());
	}

	public List<ILoggingEvent> getEventsByLogger(Class<?> klass) {
		return getEventsByLogger(klass.getName());
	}

	public void assertLogEmitted(String message, Predicate<ILoggingEvent> test) {
		if (getEvents().stream().noneMatch(test))
			fail(message);
	}

	public void assertLogNotEmitted(String message, Predicate<ILoggingEvent> test) {
		if (getEvents().stream().anyMatch(test))
			fail(message);
	}

	public synchronized <T extends Throwable> void assertEmitsLog(String message, Predicate<ILoggingEvent> test,
		CheckedRunnable<T> runnable) throws T {
		final int checkpoint = getEvents().size();
		runnable.run();
		if (getEvents().stream().skip(checkpoint).noneMatch(test))
			fail(message);
	}

	public static class LevelTest implements Predicate<ILoggingEvent> {
		private final Level minLevel;

		public LevelTest(Level minLevel) {
			this.minLevel=minLevel;
		}

		@Override
		public boolean test(ILoggingEvent e) {
			return e.getLevel().isGreaterOrEqual(minLevel);
		}
	}

	public static class NameTest implements Predicate<ILoggingEvent> {
		private final String name;

		public NameTest(String name) {
			this.name = name;
		}

		@Override
		public boolean test(ILoggingEvent e) {
			return e.getLoggerName().equals(name);
		}
	}

}
