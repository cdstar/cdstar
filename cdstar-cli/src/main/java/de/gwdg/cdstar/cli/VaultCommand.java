package de.gwdg.cdstar.cli;

import java.util.ArrayList;
import java.util.List;

import de.gwdg.cdstar.config.MapConfig;
import de.gwdg.cdstar.runtime.CDStarRuntime;
import de.gwdg.cdstar.runtime.services.VaultRegistry;
import picocli.CommandLine.Command;
import picocli.CommandLine.Parameters;

@Command(name = "vault", description = "Manage vaults", subcommands = { VaultCommand.Ls.class,
		VaultCommand.Create.class })
public class VaultCommand extends AbstractCommand {

	@Command(name = "list", description = "List all vaults")
	public static class Ls extends AbstractCommand {

		@Override
		protected int run() throws Exception {
			try (CDStarRuntime runtime = CDStarRuntime.bootstrap(getMain().getConfig())) {
				VaultRegistry vreg = runtime.lookupRequired(VaultRegistry.class);
				vreg.discover();
				vreg.getAll().forEach(vault -> {
					System.out.println(vault.getName() + " " + vault.getPropertyMap());
				});
			}
			return 0;
		}
	}

	@Command(name = "create", description = "Creat a new vault")
	public static class Create extends AbstractCommand {

		@Parameters(index = "0", description = "Name of the new vault. Must not exist.")
		String name;

		@Parameters(index = "1...", description = "Properties in KEY=VALUE format.")
		List<Setting> options = new ArrayList<>();

		@Override
		protected int run() throws Exception {
			try (CDStarRuntime runtime = CDStarRuntime.bootstrap(getMain().getConfig())) {
				VaultRegistry vreg = runtime.lookupRequired(VaultRegistry.class);
				vreg.discover();
				if (vreg.getVault(name).isPresent())
					throw new IllegalStateException("Vault exists: " + name);
				MapConfig config = new MapConfig();
				options.forEach(opt -> config.set(opt.getKey(), opt.getValue()));
				vreg.createVault(name, config);
				System.out.println("Vault created!");
			}
			return 0;
		}
	}

}
