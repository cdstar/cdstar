package de.gwdg.cdstar.cli;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.core.config.Configurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.gwdg.cdstar.GitInfo;
import de.gwdg.cdstar.config.ConfigLoader;
import de.gwdg.cdstar.config.MapConfig;
import de.gwdg.cdstar.runtime.Config;
import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;

@Command(name = "cdstar", description = "Run or manage CDSTAR instances", subcommands = {
		RunCommand.class, ConfigCommand.class, VaultCommand.class
})
public class Main extends AbstractCommand {

	private static final Logger log = LoggerFactory.getLogger(Main.class);

	@Option(names = { "--version" }, description = "Show version string and exit", versionHelp = true)
	private boolean showVersion;

	@Option(names = { "-c",
			"--config" }, paramLabel = "<file>", description = "Load configuration from this file(s). Prefix the filename with '?' to mark it optional")
	private final List<String> configFiles = new ArrayList<>();

	@Option(names = {
			"-C" }, paramLabel = "<key=value>", description = "Override individual configuration parameter. Use 'KEY=VALUE' to override or 'KEY+VALUE' to append")
	private final List<Setting> configOverrides = new ArrayList<>();

	@Option(names = {
			"--debug" }, paramLabel = "<logger>", description = "Increase loggin for specific packages. The value 'ROOT' may be used as an alias for the root logger")
	private final List<String> debugLoggers = new ArrayList<>();

	@Option(names = {
			"--trace" }, paramLabel = "<logger>", description = "Increase loggin for specific packages. The value 'ROOT' may be used as an alias for the root logger")
	private final List<String> traceLoggers = new ArrayList<>();

	@Option(names = { "--log-config" }, paramLabel = "<file>", description = "Provide a custom log4j2.properties file")
	private String logConfig;

	private Config config;

	public static void main(String[] args) {
		// Bootstrap phase
		final Main bootstrapMain = new Main();
		final CommandLine bootstrapParser = new CommandLine(bootstrapMain);
		bootstrapParser.registerConverter(Setting.class, str -> new Setting(str));
		bootstrapParser.setStopAtUnmatched(true);
		try {
			bootstrapParser.parseArgs(args);
		} catch (final Exception e) {
		}

		if (bootstrapMain.showVersion) {
			System.out.println("cdstar-" + getVersion(true));
			System.exit(0);
		}

		bootstrapMain.setupLogging();

		// Actual command phase plugins
		final Main main = new Main();
		final CommandLine parser = new CommandLine(main);

		// TODO: Scan for subcommands in plugin classpath
		parser.registerConverter(Setting.class, str -> new Setting(str));

		System.exit(parser.execute(args));
	}

	public static String getVersion(boolean longVersion) {
		if (GitInfo.isAvailable()) {
			final GitInfo git = GitInfo.getInstance();
			return git.getVersion()
					+ (longVersion ? " (" + git.describe(true) + ")" : "");
		} else {
			throw new RuntimeException("No build information on classpath");
		}
	}

	private void loadConfigFile(File file, boolean optional) {
		if (!file.exists()) {
			if (optional)
				return;
			throw new RuntimeException("Config file does not exist: " + file);
		}
		if (!file.isFile())
			throw new RuntimeException("Config file is not a file: " + file);
		if (!file.canRead())
			throw new RuntimeException("Config file is not readable: " + file);
		try {
			config.setAll(ConfigLoader.fromFile(file));
		} catch (final IOException e) {
			throw new RuntimeException("Could not load confg from file: " + file, e);
		}
	}

	private void setupLogging() {

		if (logConfig != null) {
			Configurator.initialize(null, logConfig);
			log.info("Using custom logging configuration: {}", logConfig);
		}

		for (final String pkg : debugLoggers) {
			if (pkg.equals("*") || pkg.equalsIgnoreCase("root"))
				Configurator.setRootLevel(Level.DEBUG);
			else
				Configurator.setLevel(pkg, Level.DEBUG);
		}
		for (final String pkg : traceLoggers) {
			if (pkg.equals("*") || pkg.equalsIgnoreCase("root"))
				Configurator.setRootLevel(Level.TRACE);
			else
				Configurator.setLevel(pkg, Level.TRACE);
		}
	}

	synchronized Config getConfig() {
		if (config != null)
			return config;

		config = new MapConfig();

		if (System.getenv("CDSTAR_CONFIG") != null) {
			for (final String f : System.getenv("CDSTAR_CONFIG").split(":"))
				loadConfigFile(new File(f), false);
		}

		for (final Entry<String, String> e : System.getenv().entrySet()) {
			final String key = e.getKey();
			if (key.startsWith("CDSTAR_") && !key.equals("CDSTAR_CONFIG")) {
				config.set(key.substring("CDSTAR_".length()).toLowerCase().replace("_", "."), e.getValue());
			}
		}

		for (final String f : configFiles) {
			if (f.startsWith("?")) {
				loadConfigFile(new File(f.substring(1)), true);
			} else {
				loadConfigFile(new File(f), false);
			}
		}

		for (final Setting param : configOverrides) {
			if (param.isAdditive() && param.hasValue() && config.hasKey(param.getKey()))
				config.set(param.getKey(), config.get(param.getKey(), "") + ", " + param.getValue());
			else
				config.set(param.getKey(), param.getValue());
		}

		return config;
	}

}
