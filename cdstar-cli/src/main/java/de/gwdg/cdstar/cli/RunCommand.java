package de.gwdg.cdstar.cli;

import java.nio.file.Paths;
import java.util.ServiceLoader;

import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.rest.servlet.CDStarServletInitializer;
import de.gwdg.cdstar.runtime.CDStarRuntime;
import de.gwdg.cdstar.runtime.Config;
import de.gwdg.cdstar.runtime.RuntimeContext;
import de.gwdg.cdstar.runtime.listener.RuntimeListener;
import de.gwdg.cdstar.runtime.services.PluginLoader;
import de.gwdg.cdstar.server.jetty.JettyServer;
import jakarta.servlet.ServletContainerInitializer;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;

@Command(name = "run", description = "Start server instance")
public class RunCommand extends AbstractCommand {

	@Option(names = { "-p", "--port" }, description = "Override 'http.port' setting")
	Integer port;
	@Option(names = { "-b", "--bind" }, description = "Override 'http.host' setting")
	String bind;

	@Override
	protected int run() throws Exception {
		Config cfg = getMain().getConfig();
		cfg.setDefault("http.host", "127.0.0.1");
		cfg.setDefault("http.port", "8080");

		if (port != null)
			cfg.set("http.port", port.toString());
		if (bind != null)
			cfg.set("http.host", bind);

		final CDStarRuntime runtime = CDStarRuntime.bootstrap(cfg);
		cfg = runtime.getConfig(); // now wrapped and resolved
		final JettyServer server = new JettyServer();

		if (cfg.getInt("http.port") >= 0)
			server.enableHttp(cfg.get("http.host"), cfg.getInt("http.port"));

		if (cfg.hasKey("https.certfile")) {
			cfg.setDefault("https.host", cfg.get("http.host"));
			cfg.setDefault("https.port", "8433");
			cfg.setDefault("https.h2", "false");
			server.enableHttps(cfg.get("https.host"), cfg.getInt("https.port"),
				Paths.get(cfg.get("https.certfile")), cfg.getBool("https.h2"));
		}

		runtime.register(new RuntimeListener() {
			@Override
			public void onShutdown(RuntimeContext ctx) {
				Utils.closeQuietly(server::stop);
			}
		});

		runtime.start();

		server.addComponent(new CDStarServletInitializer(runtime));

		// Load SCIs (including our own CDStarServletInitializer) from the plugin
		// classloader to allow plugins (e.g. cdstar-sentry) to install their own.
		final ClassLoader pluginClassloader = runtime.lookupRequired(PluginLoader.class).getPluginClassLoader();
		for(final ServletContainerInitializer sci: ServiceLoader.load(ServletContainerInitializer.class, pluginClassloader)) {
			if(sci instanceof CDStarServletInitializer)
				continue;
			server.addComponent(sci);
		}

		try {
			server.start();
			server.join();
			return 0;
		} catch (final Exception e) {
			return 1;
		} finally {
			runtime.close();
		}

	}

}
