package de.gwdg.cdstar.cli;

import de.gwdg.cdstar.Utils;

public class Setting {

	private String key;
	private String value;
	private boolean additive;

	public Setting(String str) {
		final String[] parts = str.split("=|\\+", 2);
		if (parts.length == 2) {
			key = parts[0];
			value = parts[1];
			additive = str.charAt(parts[0].length()) == '+';
		} else {
			key = value;
			value = null;
		}
	}

	public boolean isAdditive() {
		return additive;
	}

	public String getKey() {
		return key;
	}

	public String getValue() {
		return value;
	}

	public boolean hasValue() {
		return Utils.notNullOrEmpty(value);
	}

}
