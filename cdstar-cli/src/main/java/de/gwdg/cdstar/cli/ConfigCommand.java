package de.gwdg.cdstar.cli;

import de.gwdg.cdstar.runtime.CDStarRuntime;
import de.gwdg.cdstar.runtime.Config;
import de.gwdg.cdstar.runtime.ConfigException;
import picocli.CommandLine.Command;

@Command(name = "config", description = "Manage configuration", subcommands = {
	ConfigCommand.Print.class, ConfigCommand.Test.class })
public class ConfigCommand extends AbstractCommand {

	@Command(name = "print", description = "Print configuration")
	public static class Print extends AbstractCommand {

		@Override
		protected int run() throws Exception {
			final Config cfg = getMain().getConfig();

			if (cfg.size() == 0)
				System.out.println("# empty config");

			cfg.keySet().stream().sorted().forEach(key -> {
				try {
					System.out.println(key + ": " + cfg.get(key));
				} catch (final ConfigException e) {
					System.out.println("# Warning: " + e.getMessage());
					System.out.println("# " + key + ": " + cfg.get(key, null));
				}
			});

			return 0;
		}
	}

	@Command(name = "test", description = "Test configuration")
	public static class Test extends AbstractCommand {

		@Override
		protected int run() throws Exception {
			CDStarRuntime.bootstrap(getMain().getConfig()).close();
			return 0;
		}
	}

}
