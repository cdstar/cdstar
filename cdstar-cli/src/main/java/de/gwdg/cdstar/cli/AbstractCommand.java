package de.gwdg.cdstar.cli;

import java.util.concurrent.Callable;

import picocli.CommandLine;
import picocli.CommandLine.Model.CommandSpec;
import picocli.CommandLine.Option;
import picocli.CommandLine.ParentCommand;
import picocli.CommandLine.Spec;

public abstract class AbstractCommand implements Callable<Integer> {

	@ParentCommand
	protected AbstractCommand cliParent;

	@Spec
	protected CommandSpec cliSpec;

	@Option(names = { "-h", "--help" }, usageHelp = true, description = "Print help and exit")
	private boolean help;


	RuntimeException cliError(String msg) {
		throw new CommandLine.ParameterException(cliSpec.commandLine(), msg);
	}

	public final Main getMain() {
		for (AbstractCommand current = this; current != null; current = current.cliParent) {
			if (current instanceof Main)
				return (Main) current;
		}
		throw new RuntimeException("Faled to find root command");
	}

	@Override
	public final Integer call() throws Exception {
		return run();
	}

	protected int run() throws Exception {
		throw new CommandLine.ParameterException(cliSpec.commandLine(),
			"Command cannot be run directly. Missing subcommand?");
	}

}
