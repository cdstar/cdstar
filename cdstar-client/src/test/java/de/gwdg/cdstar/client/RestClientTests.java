package de.gwdg.cdstar.client;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.concurrent.ExecutionException;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.auth.realm.StaticRealm;
import de.gwdg.cdstar.client.actions.CheckArchiveExists;
import de.gwdg.cdstar.client.actions.CheckFileExists;
import de.gwdg.cdstar.client.actions.CheckVaultExists;
import de.gwdg.cdstar.client.actions.CreateArchive;
import de.gwdg.cdstar.client.actions.DeleteFile;
import de.gwdg.cdstar.client.actions.DownloadFile;
import de.gwdg.cdstar.client.actions.DownloadFile.DownloadReport;
import de.gwdg.cdstar.client.actions.GetArchiveInfo;
import de.gwdg.cdstar.client.actions.GetFileInfo;
import de.gwdg.cdstar.client.actions.GetFileStream;
import de.gwdg.cdstar.client.actions.GetVaultInfo;
import de.gwdg.cdstar.client.actions.UploadFile;
import de.gwdg.cdstar.client.helper.ArchiveFileIterable;
import de.gwdg.cdstar.config.MapConfig;
import de.gwdg.cdstar.rest.servlet.CDStarServletInitializer;
import de.gwdg.cdstar.runtime.CDStarRuntime;
import de.gwdg.cdstar.server.jetty.JettyServer;
import de.gwdg.cdstar.utils.test.TestLogger;
import de.gwdg.cdstar.web.common.model.ArchiveInfo;
import de.gwdg.cdstar.web.common.model.ArchiveUpdated;
import de.gwdg.cdstar.web.common.model.ArchiveUpdated.FileChange;
import de.gwdg.cdstar.web.common.model.FileInfo;

public class RestClientTests {

	@Rule
	public TestLogger log = new TestLogger("de.gwdg");

	private CDStarRestClient client;
	private static CDStarRuntime runtime;

	@ClassRule
	public static TemporaryFolder temp = new TemporaryFolder();

	private static JettyServer server;

	@BeforeClass
	public static void startServer() throws Throwable {
		new TestLogger("de.gwdg").run(() -> {
			final Path workDir = temp.newFolder().toPath();

			final String user = "test";
			final String password = "test";
			final String vault = "test";

			final MapConfig cfg = new MapConfig();
			cfg.set("path.home", workDir.toString());
			cfg.set("realm.static.class", StaticRealm.class.getName());
			cfg.set("realm.static.user." + user + ".password", StaticRealm.hashPassword(password.toCharArray()));
			cfg.set("realm.static.user." + user + ".permissions",
				"vault:" + vault + ":create, vault:" + vault + ":read");
			cfg.set("vault." + vault + ".create", "true");
			runtime = CDStarRuntime.bootstrap(cfg);
			runtime.start();

			server = new JettyServer("127.0.0.1", 0);
			server.addComponent(new CDStarServletInitializer(runtime));
			server.start();
		});

	}

	@AfterClass
	public static void stopServer() throws Throwable {
		new TestLogger("de.gwdg").run(() -> {
			server.stop();
			if (runtime != null)
				runtime.close();
		});
	}

	protected URI getBaseUri() {
		return server.getURI().resolve("v3/");
	}

	@Before
	public void setupClient() {
		client = new CDStarRestClient(new CDStarClientConfig(getBaseUri()).basicAuth("test", "test"));
	}

	@After
	public void teardownClient() throws IOException {
		client.close();
	}

	@Test
	public void testVaultInfo() throws InterruptedException, ExecutionException, IOException {
		assertFalse(new CheckVaultExists("noSuchVault").execute(client).booleanValue());
		assertTrue(new CheckVaultExists("test").execute(client).booleanValue());
		assertFalse(new GetVaultInfo("test").execute(client).isPublic);
	}

	@Test
	public void testCreateEmptyArchive() throws InterruptedException, ExecutionException, IOException {
		final String id = new CreateArchive("test").execute(client).id;
		assertTrue(new CheckArchiveExists("test", id).execute(client).booleanValue());
	}

	@Test
	public void testFileUpload() throws InterruptedException, ExecutionException, IOException {
		final CreateArchive action = new CreateArchive("test");
		action.addFile("file.txt", new ByteArrayInputStream("hello".getBytes(StandardCharsets.UTF_8)), "file.txt");
		final ArchiveUpdated result = action.execute(client);
		assertNotNull(result.id);
		assertNotNull(result.revision);
		assertEquals("test", result.vault);
		assertNotNull(result.report);
		assertEquals(1, result.report.size());
		assertTrue(result.report.get(0) instanceof FileChange);
		final FileChange change = (FileChange) result.report.get(0);
		assertEquals("file.txt", change.name);
		assertEquals(5, change.info.size);
		assertEquals("text/plain", change.info.type);
		assertEquals("file.txt", change.info.name);
		assertEquals("5d41402abc4b2a76b9719d911017c592", change.info.digests.md5);
		assertEquals("aaf4c61ddcc5e8a2dabede0f3b482cd9aea9434d", change.info.digests.sha1);
	}

	@Test
	public void testFileUploadDirect() throws InterruptedException, ExecutionException, IOException {
		final String id = client.execute(new CreateArchive("test")).id;
		final FileInfo result = new UploadFile("test", id, "file.txt")
			.source(new ByteArrayInputStream("hello".getBytes(StandardCharsets.UTF_8)))
			.execute(client);

		assertEquals("file.txt", result.name);
		assertEquals(5, result.size);
		assertEquals("text/plain", result.type);
		assertEquals("5d41402abc4b2a76b9719d911017c592", result.digests.md5);
		assertEquals("aaf4c61ddcc5e8a2dabede0f3b482cd9aea9434d", result.digests.sha1);
	}

	@Test
	public void testExplictTransactions() throws InterruptedException, ExecutionException, IOException {
		final CDStarRestClient clone = client.clone();

		assertFalse(clone.isTransactional());
		clone.begin();
		assertTrue(clone.isTransactional());

		final String id = clone.execute(new CreateArchive("test")).id;

		assertFalse(new CheckArchiveExists("test", id).execute(client).booleanValue());
		assertTrue(new CheckArchiveExists("test", id).execute(clone).booleanValue());

		clone.commit();
		assertFalse(clone.isTransactional());
		assertTrue(new CheckArchiveExists("test", id).execute(client).booleanValue());
		assertTrue(new CheckArchiveExists("test", id).execute(clone).booleanValue());

		clone.close();
	}

	@Test
	public void testGetArchiveInfo() throws Exception {
		final ArchiveUpdated report = new CreateArchive("test")
			.setMeta("dc:title", "Teeest")
			.addFile("test.txt", new ByteArrayInputStream("hello".getBytes(StandardCharsets.UTF_8)), "file.txt")
			.setFileMeta("test.txt", "dc:title", "Test")
			.execute(client);

		ArchiveInfo info;
		info = new GetArchiveInfo("test", report.id).execute(client);
		assertNull(info.acl);
		assertNull(info.meta);
		assertNull(info.files);

		info = new GetArchiveInfo("test", report.id).withMeta(true).execute(client);
		assertNull(info.acl);
		assertEquals("Teeest", info.meta.getMap().get("dc:title").get(0));
		assertNull(info.files);

		info = new GetArchiveInfo("test", report.id).withAcl(true).execute(client);
		assertNotNull(info.acl);
		assertNull(info.meta);
		assertNull(info.files);

		info = new GetArchiveInfo("test", report.id).withFiles(true).execute(client);
		assertNull(info.acl);
		assertNull(info.meta);
		assertEquals("test.txt", info.files.get(0).name);
		assertNull(info.files.get(0).meta);

		info = new GetArchiveInfo("test", report.id).withFiles(true).withMeta(true).execute(client);
		assertNull(info.acl);
		assertEquals("Teeest", info.meta.getMap().get("dc:title").get(0));
		assertEquals("test.txt", info.files.get(0).name);
		assertEquals("Test", info.files.get(0).meta.getMap().get("dc:title").get(0));
	}

	@Test
	public void testGetFileInfo() throws Exception {
		final ArchiveUpdated report = new CreateArchive("test")
			.addFile("test.txt", new ByteArrayInputStream("hello".getBytes(StandardCharsets.UTF_8)), "file.txt")
			.setFileMeta("test.txt", "dc:title", "Test")
			.execute(client);

		FileInfo fileInfo = new GetFileInfo("test", report.id, "test.txt").execute(client);
		assertEquals("test.txt", fileInfo.name);
		assertEquals(5, fileInfo.size);
		assertNull(fileInfo.meta);

		fileInfo = new GetFileInfo("test", report.id, "test.txt").withMeta(true).execute(client);
		assertEquals("test.txt", fileInfo.name);
		assertEquals(5, fileInfo.size);
		assertEquals("Test", fileInfo.meta.getMap().get("dc:title").get(0));
	}

	@Test
	public void testDownloadFile() throws Exception {
		final String id = new CreateArchive("test")
			.addFile("test.txt", new ByteArrayInputStream("hello".getBytes(StandardCharsets.UTF_8)), "file.txt")
			.execute(client).id;

		DownloadReport dl = new DownloadFile("test", id, "test.txt")
			.path(temp.newFolder().toPath())
			.execute(client);
		assertEquals(5, dl.getBytes());
		assertArrayEquals("hello".getBytes(StandardCharsets.UTF_8), Files.readAllBytes(dl.getPath()));

		dl = new DownloadFile("test", id, "test.txt")
			.path(temp.newFolder().toPath())
			.range(1, 3)
			.execute(client);
		assertEquals(3, dl.getBytes());
		assertArrayEquals("ell".getBytes(StandardCharsets.UTF_8), Files.readAllBytes(dl.getPath()));

		dl = new DownloadFile("test", id, "test.txt")
			.path(temp.newFolder().toPath())
			.range(1, -1)
			.execute(client);
		assertEquals(4, dl.getBytes());
		assertArrayEquals("ello".getBytes(StandardCharsets.UTF_8), Files.readAllBytes(dl.getPath()));
	}

	@Test
	public void testStreamFile() throws Exception {
		final String id = new CreateArchive("test")
			.addFile("test.txt", new ByteArrayInputStream("hello".getBytes(StandardCharsets.UTF_8)), "file.txt")
			.execute(client).id;

		final InputStream is = new GetFileStream("test", id, "test.txt")
			.execute(client);

		assertEquals("hello", Utils.toString(is));
		is.close();
	}

	@Test
	public void testDeleteFile() throws Exception {
		final String id = new CreateArchive("test")
			.addFile("test.txt", new ByteArrayInputStream("hello".getBytes(StandardCharsets.UTF_8)), "file.txt")
			.execute(client).id;

		new DeleteFile("test", id, "test.txt").execute(client);
		assertFalse(new CheckFileExists("test", id, "file.txt").execute(client).booleanValue());
	}

	@Test
	public void testListFilesIterator() throws Exception {
		final CreateArchive upload = new CreateArchive("test");
		for (int i = 0; i < 100; i++)
			upload.addFile(i + ".txt", new ByteArrayInputStream("hello".getBytes(StandardCharsets.UTF_8)), i + ".txt");
		final String id = upload.execute(client).id;

		final List<FileInfo> all = new ArchiveFileIterable(client, "test", id).limit(10).asList();
		assertEquals(100, all.size());
	}

	@Test
	public void testEndTransactionNoRefresh() throws Exception {
		client.begin(true);
		client.close();
		assertFalse(client.isTransactional());
	}

}
