package de.gwdg.cdstar.client.actions;

import java.io.IOException;
import java.io.InputStream;

import org.apache.http.HttpResponse;

import de.gwdg.cdstar.client.BaseAction;
import de.gwdg.cdstar.client.RequestBuilder;

/**
 * Download a cdstar file as a stream.
 *
 * The file content is directly streamed over the network and no temporary file
 * is created. The stream MUST be closed after reading.
 */
public class GetFileStream extends BaseAction<InputStream> {
	private final String vault;
	private final String id;
	private final String name;
	private long offset = -1;
	private long length = -1;

	public GetFileStream(String vault, String id, String name) {
		super(InputStream.class);
		this.vault = vault;
		this.id = id;
		this.name = name;
	}

	@Override
	protected void prepareRequest(RequestBuilder builder) {
		builder.GET(vault, id, name);
		if (offset > 0 && length > 0)
			builder.addHeader("Range", "bytes=" + offset + "-" + (offset + length - 1));
		else if (offset > 0)
			builder.addHeader("Range", "bytes=" + offset + "-");
		else if (length > 0)
			builder.addHeader("Range", "bytes=0-" + (length - 1));
	}

	public GetFileStream range(long offset, long length) {
		this.offset = offset;
		this.length = length;
		return this;
	}

	@Override
	protected InputStream handleResponse(HttpResponse response) throws IOException {
		if ((offset > 0 || length > 0) && response.getHeaders("Content-Range") == null)
			throw new IOException("Server support for range requests missing");
		return super.handleResponse(response);
	}
}
