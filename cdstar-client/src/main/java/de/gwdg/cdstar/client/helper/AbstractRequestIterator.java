package de.gwdg.cdstar.client.helper;

import java.io.IOException;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Deque;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.CompletableFuture;

import de.gwdg.cdstar.client.CDStarRestClient;

public abstract class AbstractRequestIterator<ResultType> implements Iterator<ResultType>, Iterable<ResultType> {

	protected final CDStarRestClient client;
	Deque<ResultType> lastBatch = new ArrayDeque<>();
	protected Exception lastError;
	boolean lastBatchRecieved = false;

	public AbstractRequestIterator(CDStarRestClient client) {
		this.client = client;
	}

	protected void checkIterationNotStarted() {
		if (lastBatchRecieved || lastError != null || !lastBatch.isEmpty())
			throw new IllegalStateException("First request already sent. Modifications are not allowed anymore.");
	}

	/**
	 * Return all entities as a list.
	 */
	public List<ResultType> asList() {
		final List<ResultType> results = new ArrayList<>();
		forEach(f -> results.add(f));
		return results;
	}

	@Override
	public Iterator<ResultType> iterator() {
		return this;
	}

	@Override
	public boolean hasNext() {
		if (!lastBatch.isEmpty())
			return true;

		if (lastError != null)
			return false;

		if (lastBatchRecieved)
			return false;

		try {
			lastBatch.addAll(getNextBatch().get());
			lastBatchRecieved = lastBatch.isEmpty();
			return !lastBatchRecieved;
		} catch (final Exception e) {
			lastError = e;
			throw new RuntimeException(e);
		}
	}

	/**
	 * Fetch the next batch of values.
	 *
	 * Implementors: The returned future MUST complete eventually, either with a
	 * result or with an error. To signal the end of the iterator, return an empty
	 * batch.
	 *
	 * If called directly (not through the iterator interface), then make sure to
	 * wait for the last batch future to complete before requesting the next batch.
	 * Requesting multiple batches in parallel has undefined behavior.
	 */
	public abstract CompletableFuture<Collection<ResultType>> getNextBatch() throws IOException;

	@Override
	public ResultType next() {
		if (hasNext())
			return lastBatch.pop();
		throw new NoSuchElementException();
	}

}