package de.gwdg.cdstar.client.actions;

import de.gwdg.cdstar.client.BaseAction;
import de.gwdg.cdstar.client.RequestBuilder;
import de.gwdg.cdstar.web.common.model.FileInfo;

public class GetFileInfo extends BaseAction<FileInfo> {
	private final String vault;
	private final String id;
	private final String name;
	private boolean withMeta;

	public GetFileInfo(String vault, String id, String name) {
		super(FileInfo.class);
		this.vault = vault;
		this.id = id;
		this.name = name;
	}

	public GetFileInfo withMeta(boolean enable) {
		withMeta = enable;
		return this;
	}

	@Override
	protected void prepareRequest(RequestBuilder builder) {
		builder.GET(vault, id, name);
		builder.query("info", "");
		if (withMeta)
			builder.query("with", "meta");
	}
}
