package de.gwdg.cdstar.client.actions;

import java.io.IOException;

import org.apache.http.HttpResponse;

import de.gwdg.cdstar.client.BaseAction;
import de.gwdg.cdstar.client.RequestBuilder;

public class CheckFileExists extends BaseAction<Boolean> {
	private final String vault;
	private final String id;
	private final String name;

	public CheckFileExists(String vault, String id, String name) {
		super(Boolean.class);
		this.vault = vault;
		this.id = id;
		this.name = name;
	}

	@Override
	protected Boolean handleResponse(HttpResponse response) throws IOException {
		final int code = response.getStatusLine().getStatusCode();
		if (code == 200)
			return Boolean.TRUE;
		if (code == 404)
			return Boolean.FALSE;
		return super.handleResponse(response);
	}

	@Override
	protected void prepareRequest(RequestBuilder builder) {
		builder.HEAD(vault, id, name);
	}

}
