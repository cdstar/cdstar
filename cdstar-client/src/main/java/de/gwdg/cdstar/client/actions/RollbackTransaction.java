package de.gwdg.cdstar.client.actions;

import de.gwdg.cdstar.client.BaseAction;
import de.gwdg.cdstar.client.RequestBuilder;

public class RollbackTransaction extends BaseAction<Void> {

	private final String tx;

	public RollbackTransaction(String tx) {
		super(Void.class);
		this.tx = tx;
	}

	@Override
	protected void prepareRequest(RequestBuilder builder) {
		builder.DELETE("_tx", tx);
	}

}
