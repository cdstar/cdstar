package de.gwdg.cdstar.client;

import java.io.Closeable;
import java.io.IOException;
import java.net.URI;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.SSLContext;

import org.apache.http.HttpHeaders;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.gwdg.cdstar.NamedThreadFactory;
import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.client.actions.BeginTransaction;
import de.gwdg.cdstar.client.actions.CommitTransaction;
import de.gwdg.cdstar.client.actions.RenewTransaction;
import de.gwdg.cdstar.client.actions.RollbackTransaction;
import de.gwdg.cdstar.web.common.model.TransactionInfo;

/**
 * REST Client to access a CDStar instance using apache httpclient 4.x.
 *
 * This implementation does not use apache httpasyncclient for a reason: The
 * sync variant is faster for low connection counts (and therefore better suited
 * for a client) and httpmime is not compatible with the async client. This
 * difference may be evaluated again once apache httpclient 5.x is released.
 */
public class CDStarRestClient implements Closeable {
	public static final String X_AUTODETECT = "application/x-autodetect";

	private static final Logger log = LoggerFactory.getLogger(CDStarRestClient.class);

	private TransactionInfo tx;
	private ScheduledExecutorService cron;
	private final CDStarClientConfig config;

	private final CloseableHttpClient http;
	static final Executor pool = Executors.newCachedThreadPool(new NamedThreadFactory("cdstar-client"));

	public CDStarRestClient(CDStarClientConfig cfg) {
		config = cfg;

		try {
			final RequestConfig.Builder requestConfigBuilder = RequestConfig.custom()
				.setConnectTimeout(cfg.connectTimeout)
				.setConnectionRequestTimeout(cfg.connectTimeout)
				.setSocketTimeout(cfg.socketTimeout);

			final HttpClientBuilder httpClientBuilder = HttpClientBuilder.create()
				.setDefaultRequestConfig(requestConfigBuilder.build())
				.setMaxConnPerRoute(cfg.maxConn)
				.setMaxConnTotal(cfg.maxConn)
				.setSSLContext(SSLContext.getDefault());

			http = httpClientBuilder.build();

		} catch (final NoSuchAlgorithmException e) {
			throw new RuntimeException(e);
		}
	}

	public URI getBaseUri() {
		return config.uri;
	}

	/**
	 * Convenience method that calls
	 * {@link BaseAction#submit(CDStarRestClient)}.
	 */
	public <T> CompletableFuture<T> submit(BaseAction<T> action) {
		return action.submit(this);
	}

	/**
	 * Convenience method that calls
	 * {@link BaseAction#execute(CDStarRestClient)}.
	 */
	public <T> T execute(BaseAction<T> action) throws IOException {
		return action.execute(this);
	}

	RequestBuilder getRequestBuilder() {
		final RequestBuilder rb = new RequestBuilder()
			.baseUri(getBaseUri())
			.header(HttpHeaders.ACCEPT, "application/json, */*;q=0.1");

		if (tx != null)
			rb.header("X-Transaction", tx.id);
		if (config.auth != null)
			rb.header("Authorization", config.auth);

		return rb;
	}

	/**
	 * Start a writeable transaction.
	 */
	public void begin() throws IOException {
		begin(false);
	}

	/**
	 * Start a new transaction and bind it to this client instance. The
	 * transaction will be renewed automatically and attached to all requests
	 * send to this client until either {@link #commit()} or {@link #rollback()}
	 * is called.
	 */
	public synchronized void begin(boolean readOnly) throws IOException {
		if (tx != null)
			throw new IllegalStateException("Cannot begin new transaction while another is still open.");
		if (cron == null)
			cron = Executors.newSingleThreadScheduledExecutor();

		tx = new BeginTransaction().readOnly(readOnly).execute(this);

		if (tx.ttl > 0) {
			final long ms = tx.ttl * 750; // 75% in ms (ttl is in seconds)
			cron.schedule(this::renewTransaction, ms, TimeUnit.MILLISECONDS);
		}
	}

	synchronized void renewTransaction() {
		if (tx == null)
			return;
		try {
			tx = new RenewTransaction(tx.id).execute(this);

			final long ms = tx.ttl * 750; // 75% in ms (ttl is in seconds)
			cron.schedule(this::renewTransaction, ms, TimeUnit.MILLISECONDS);

		} catch (final Exception e) {
			// Do not re-schedule the renew action, but keep tx as is, so future
			// request won't silently continue outside of the transaction.
			log.error("Failed to renew transaction", e);
		}
	}

	public synchronized void commit() throws IOException {
		if (tx == null)
			throw new IllegalStateException("No transaction to commit");

		new CommitTransaction(tx.id).execute(this);
		tx = null;
	}

	public synchronized void rollback() throws IOException {
		if (tx == null)
			throw new IllegalStateException("No transaction to rollback");

		new RollbackTransaction(tx.id).execute(this);
		tx = null;
	}

	@Override
	public synchronized void close() throws IOException {
		try {
			if (cron != null)
				cron.shutdownNow();
			if (tx != null)
				rollback();
			Utils.closeQuietly(http);
		} finally {
			// Ensure that tx is nulled even if a rollback failed to stop refresh requests.
			tx = null;
		}
	}

	public synchronized boolean isTransactional() {
		return tx != null;
	}

	/**
	 * Return a new identically configured client, but with independent internal
	 * state (e.g. transaction).
	 */
	@Override
	public CDStarRestClient clone() {
		return new CDStarRestClient(config.clone());
	}

	CloseableHttpResponse sendHttpRequest(HttpUriRequest request) throws IOException {
		return http.execute(request);
	}

}
