package de.gwdg.cdstar.client.actions;

import de.gwdg.cdstar.client.BaseAction;
import de.gwdg.cdstar.client.RequestBuilder;
import de.gwdg.cdstar.web.common.model.TransactionInfo;

public class BeginTransaction extends BaseAction<TransactionInfo> {

	private boolean readOnly;

	public BeginTransaction() {
		super(TransactionInfo.class);
	}

	public BeginTransaction readOnly(boolean enable) {
		readOnly = enable;
		return this;
	}

	@Override
	protected void prepareRequest(RequestBuilder builder) {
		builder.POST("_tx").form("readonly", Boolean.toString(readOnly));
	}

}
