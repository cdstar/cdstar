package de.gwdg.cdstar.client;

import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import java.util.concurrent.ExecutionException;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpUriRequest;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.web.common.model.ErrorResponse;

/**
 * Base class for actions. Implementations usually represent a specific REST API
 * endpoint or an intent that can be performed by a single atomic request to the
 * service.
 *
 * Actions are one-shot instances. They can only be submitted once and should
 * not be modified after the request was submitted.
 */
public abstract class BaseAction<T> {
	private final Class<T> responseType;
	private CDStarRestClient client = null;
	private boolean autoClose = true;

	public BaseAction(Class<T> responseType) {
		this.responseType = responseType;
	}

	/**
	 * Enable (default) or disable auto-closing of the response and its body
	 * after {@link #handleResponse(HttpResponse)} returns. Can be disabled to
	 * allow response streaming.
	 */
	protected void configAutoClose(boolean enable) {
		this.autoClose = enable;
	}

	/**
	 * Prepare a request by configuring the given {@link RequestBuilder}. This
	 * method should be side-effect free (apart from modifying the passed in
	 * instance) as it may be called multiple times.
	 *
	 * @param builder
	 *            A builder initialized with the clients base URI, transaction
	 *            and auth headers as well as an accept header of
	 *            <code>application/json, *\/*;q=0.1</code>
	 */
	protected abstract void prepareRequest(RequestBuilder builder);

	/**
	 * Extract an entity out of the response or throw an exception. This can be
	 * overwritten by a subclass for special entity handling.
	 *
	 * The default implementation is able to extract {@link Void},
	 * {@link Boolean} and {@link InputStream} out of any response, and other
	 * classes out of json responses, as well as API errors from json error
	 * responses. In case of {@link InputStream},
	 * {@link #configAutoClose(boolean)} is disabled automatically.
	 */
	@SuppressWarnings("unchecked")
	protected T handleResponse(HttpResponse response) throws IOException {
		final int code = response.getStatusLine().getStatusCode();

		if (code >= 400) {
			throw new ErrorResponseException(parseJson(response, ErrorResponse.class));
		}

		if (responseType.isAssignableFrom(Boolean.class)) {
			return (T) Boolean.valueOf(code < 400);
		}

		if (responseType.isAssignableFrom(Void.class)) {
			if (response.getEntity() != null && response.getEntity().getContentLength() != 0)
				throw new IOException("Did not expect response entity, got: " + response.getEntity());
			return null;
		}

		if (response.getEntity() == null)
			throw new IOException("Expected response entity");

		if (responseType.isAssignableFrom(InputStream.class)) {
			autoClose = false;
			return (T) response.getEntity().getContent();
		}

		return parseJson(response, responseType);
	}

	private <J> J parseJson(HttpResponse response, Class<J> jsonType) throws IOException {
		final Header ctype = response.getEntity().getContentType();
		if (ctype == null || !ctype.getValue().equalsIgnoreCase("application/json"))
			throw new IOException("Expected json content type, got " + ctype);

		try {
			return HttpUtils.json.readValue(response.getEntity().getContent(), jsonType);
		} catch (final JsonParseException e) {
			throw new IOException("Response failed to parse", e);
		} catch (final JsonMappingException e) {
			throw new IOException("Response failed to parse into expected type", e);
		}
	}

	/**
	 * Execute an action synchronously and return the result.
	 *
	 * This is generally the better option compared to submit().get(), as the
	 * exceptions thrown will have nicer stack traces and in case of an error,
	 * the actual exception that caused the error is thrown instead of the
	 * {@link ExecutionException} wrapper.
	 *
	 * This method may block for a time limited only by the timeouts specified
	 * in the client configuration.
	 */
	public final T execute(CDStarRestClient client) throws IOException {
		if (this.client != null)
			throw new IllegalStateException("Action already submitted");
		this.client = client;
		final RequestBuilder rb = client.getRequestBuilder();
		prepareRequest(rb);
		return performRequest(rb);
	}

	/**
	 * Query an action to be executed in a background thread.
	 *
	 * Note that aborting (canceling) the future will not cancel the request, as
	 * there is no way to guarantee the request did not already succeed on
	 * server side.
	 */
	public final CompletableFuture<T> submit(CDStarRestClient client) {
		if (this.client != null)
			throw new IllegalStateException("Action already submitted");
		this.client = client;
		final RequestBuilder rb = client.getRequestBuilder();
		prepareRequest(rb);

		return CompletableFuture.supplyAsync(() -> {
			try {
				return performRequest(rb);
			} catch (final IOException e) {
				throw new CompletionException(e);
			}
		}, CDStarRestClient.pool);
	}

	private T performRequest(RequestBuilder rb) throws IOException {
		final HttpUriRequest request = rb.buildRequest();
		final CloseableHttpResponse response = client.sendHttpRequest(request);

		try {
			return handleResponse(response);
		} catch (final ErrorResponseException e) {
			e.attachRequest(request);
			throw e;
		} finally {
			if (autoClose) {
				Utils.closeQuietly(response);
			}
		}
	}

}
