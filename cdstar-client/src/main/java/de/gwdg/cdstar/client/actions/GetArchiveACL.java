package de.gwdg.cdstar.client.actions;

import de.gwdg.cdstar.client.BaseAction;
import de.gwdg.cdstar.client.RequestBuilder;
import de.gwdg.cdstar.web.common.model.AclMap;

public class GetArchiveACL extends BaseAction<AclMap> {
	private final String vault;
	private final String id;

	public GetArchiveACL(String vault, String id) {
		super(AclMap.class);
		this.vault = vault;
		this.id = id;
	}

	@Override
	protected void prepareRequest(RequestBuilder builder) {
		builder.GET(vault, id);
		builder.query("acl", "");
	}
}
