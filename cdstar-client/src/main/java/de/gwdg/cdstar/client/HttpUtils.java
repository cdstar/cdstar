package de.gwdg.cdstar.client;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

import org.apache.http.entity.ContentType;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

final class HttpUtils {
	public static final ContentType UTF8_TEXT = ContentType.TEXT_PLAIN.withCharset(StandardCharsets.UTF_8);
	public static final ObjectMapper json = new ObjectMapper();

	static {
		json.registerModule(new JavaTimeModule());
	}

	protected final static char[] hexArray = "0123456789abcdef".toCharArray();

	public static String urlEncode(String path, boolean encodePlus, Charset charset) {
		final StringBuilder sb = new StringBuilder(path.length());
		final ByteBuffer bb = charset.encode(path);
		boolean changed = false;
		while (bb.hasRemaining()) {
			final int c = bb.get() & 0xff;
			if ('a' <= c && c <= 'z' || 'A' <= c && c <= 'Z' || '0' <= c && c <= '9') {
				sb.append((char) c);
			} else if (c == '.' || c == '-' || c == '_' || c == '~') {
				sb.append((char) c);
			} else if (encodePlus && c == ' ') {
				changed = true;
				sb.append('+');
			} else {
				changed = true;
				sb.append("%");
				sb.append(hexArray[(c >> 4) & 0xF]);
				sb.append(hexArray[c & 0xF]);
			}
		}
		return changed ? sb.toString() : path;
	}
}
