package de.gwdg.cdstar.client.actions;

import de.gwdg.cdstar.client.BaseAction;
import de.gwdg.cdstar.client.RequestBuilder;
import de.gwdg.cdstar.web.common.model.ScrollResult;

public class ScrollVault extends BaseAction<ScrollResult> {
	private final String vault;
	private String scroll;
	private int limit = -1;

	public ScrollVault(String vault, String scroll) {
		super(ScrollResult.class);
		this.vault = vault;
		this.scroll = scroll;
	}

	public ScrollVault(String vault) {
		this(vault, "");
	}

	public ScrollVault limit(int limit) {
		this.limit = limit;
		return this;
	}

	@Override
	protected void prepareRequest(RequestBuilder builder) {
		if (limit >= 0)
			builder.query("limit", limit);
		builder.query("scroll", scroll);
		builder.GET(vault);
	}
}
