package de.gwdg.cdstar.client;

import java.net.URI;
import java.net.URISyntaxException;

import de.gwdg.cdstar.Utils;

public class CDStarClientConfig {

	private static final int DEFAULT_CONNECT_TIMEOUT = 3000;
	private static final int DEFAULT_SOCKET_TIMEOUT = 30000;
	private static final int DEFAULT_MAX_CONN = 4;

	int connectTimeout = DEFAULT_CONNECT_TIMEOUT;
	int socketTimeout = DEFAULT_SOCKET_TIMEOUT;
	int maxConn = DEFAULT_MAX_CONN;

	final URI uri;
	String auth;

	public CDStarClientConfig(URI uri) {
		try {
			if (uri.getUserInfo() != null) {
				auth = "Basic " + Utils.base64encode(uri.getUserInfo());
				uri = new URI(uri.getScheme(), null, uri.getHost(), uri.getPort(), uri.getPath(), null, null);
			}
		} catch (final URISyntaxException e) {
			throw new IllegalArgumentException(e);
		}

		this.uri = uri;
	}

	public CDStarClientConfig basicAuth(String username, String password) {
		auth = "Basic " + Utils.base64encode(username + ":" + password);
		return this;
	}

	public CDStarClientConfig tokenAuth(String token) {
		if (token.contains("\n"))
			throw new IllegalArgumentException("Tokens must not contain newlines");
		auth = "Bearer " + token;
		return this;
	}

	@Override
	public CDStarClientConfig clone() {
		final CDStarClientConfig clone = new CDStarClientConfig(uri);
		clone.auth = auth;
		return clone;
	}

}
