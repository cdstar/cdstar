package de.gwdg.cdstar.client.actions;

import java.io.InputStream;
import java.nio.file.Path;

import de.gwdg.cdstar.client.BaseAction;
import de.gwdg.cdstar.client.CDStarRestClient;
import de.gwdg.cdstar.client.RequestBuilder;
import de.gwdg.cdstar.web.common.model.FileInfo;

public class UploadFile extends BaseAction<FileInfo> {
	private final String vault;
	private final String id;
	private final String name;

	private String mimeType;
	private Path file;
	private InputStream stream;

	public UploadFile(String vault, String id, String name) {
		super(FileInfo.class);
		this.vault = vault;
		this.id = id;
		this.name = name;
	}

	public UploadFile source(Path file) {
		source(file, CDStarRestClient.X_AUTODETECT);
		return this;
	}

	public UploadFile source(Path file, String mimeType) {
		this.file = file;
		this.mimeType = mimeType;
		return this;
	}

	public UploadFile source(InputStream stream) {
		source(stream, CDStarRestClient.X_AUTODETECT);
		return this;
	}

	public UploadFile source(InputStream stream, String mimeType) {
		this.stream = stream;
		this.mimeType = mimeType;
		return this;
	}

	@Override
	protected void prepareRequest(RequestBuilder builder) {
		builder.PUT(vault, id, name);
		if (file != null)
			builder.entity(file, mimeType, null);
		else if (stream != null)
			builder.entity(stream, mimeType, null);
		else
			builder.entity("", mimeType, null);
	}
}
