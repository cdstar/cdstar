package de.gwdg.cdstar.client.actions;

import de.gwdg.cdstar.client.BaseAction;
import de.gwdg.cdstar.client.RequestBuilder;
import de.gwdg.cdstar.web.common.model.TransactionInfo;

public class RenewTransaction extends BaseAction<TransactionInfo> {

	private final String tx;

	public RenewTransaction(String tx) {
		super(TransactionInfo.class);
		this.tx = tx;
	}

	@Override
	protected void prepareRequest(RequestBuilder builder) {
		builder.POST("_tx", tx).query("renew", true);
	}

}
