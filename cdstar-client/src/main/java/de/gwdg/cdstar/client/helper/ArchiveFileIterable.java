package de.gwdg.cdstar.client.helper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import de.gwdg.cdstar.client.CDStarRestClient;
import de.gwdg.cdstar.client.actions.GetArchiveFileList;
import de.gwdg.cdstar.web.common.model.FileInfo;
import de.gwdg.cdstar.web.common.model.FileList;

/**
 * An iterator over all {@link FileInfo} entries of an archive, optionally
 * filtered and ordered. Requests are triggered lazily while the iterator is
 * consumed. Every {@link #limit(int)} items, a new request may be triggered to
 * fetch more items. The client should have a running transaction to make sure
 * the list is complete and consistent.
 */

public class ArchiveFileIterable extends AbstractRequestIterator<FileInfo> {

	final String vault;
	final String id;

	boolean withMeta;
	List<String> includeFilter = new ArrayList<>();
	List<String> excludeFilter = new ArrayList<>();
	int limit = 1024;
	long offset = 0;
	long total = -1;
	String order = "name";
	boolean reverse;

	public ArchiveFileIterable(CDStarRestClient client, String vault, String archive) {
		super(client);
		this.vault = vault;
		id = archive;
	}

	public ArchiveFileIterable withMeta(boolean enable) {
		checkIterationNotStarted();
		withMeta = enable;
		return this;
	}

	public ArchiveFileIterable includeFiles(String... glob) {
		checkIterationNotStarted();
		if (includeFilter == null)
			includeFilter = new ArrayList<>();
		includeFilter.addAll(Arrays.asList(glob));
		return this;
	}

	public ArchiveFileIterable excludeFiles(String... glob) {
		checkIterationNotStarted();
		if (excludeFilter == null)
			excludeFilter = new ArrayList<>();
		excludeFilter.addAll(Arrays.asList(glob));
		return this;
	}

	public ArchiveFileIterable offset(int offset) {
		checkIterationNotStarted();
		this.offset = offset;
		return this;
	}

	public ArchiveFileIterable limit(int limit) {
		checkIterationNotStarted();
		this.limit = limit;
		return this;
	}

	public ArchiveFileIterable order(String orderBy, boolean desc) {
		checkIterationNotStarted();
		order = orderBy;
		reverse = desc;
		return this;
	}

	/**
	 * Return all entities as a map, using file names as key.
	 */
	public Map<String, FileInfo> asMap() {
		final Map<String, FileInfo> results = new HashMap<>();
		forEach(f -> results.put(f.name, f));
		return results;
	}

	@Override
	public CompletableFuture<Collection<FileInfo>> getNextBatch() throws IOException {
		if (total > -1 && offset >= total)
			return CompletableFuture.completedFuture(Collections.emptyList());
		final GetArchiveFileList rq = new GetArchiveFileList(vault, id).withMeta(withMeta);
		if (includeFilter.size() > 0)
			rq.includeFiles((String[]) includeFilter.toArray());
		if (excludeFilter.size() > 0)
			rq.excludeFiles((String[]) excludeFilter.toArray());
		rq.order(order, reverse);
		rq.page(offset, limit);
		return rq.submit(client).thenApply((FileList list) -> {
			offset += list.getItems().size();
			total = list.getTotal();
			return list.getItems();
		});
	}

}
