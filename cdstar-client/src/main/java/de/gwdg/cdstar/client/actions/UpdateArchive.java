package de.gwdg.cdstar.client.actions;

import java.io.InputStream;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Objects;

import de.gwdg.cdstar.client.BaseAction;
import de.gwdg.cdstar.client.CDStarRestClient;
import de.gwdg.cdstar.client.RequestBuilder;
import de.gwdg.cdstar.web.common.model.ArchiveUpdated;

public class UpdateArchive extends BaseAction<ArchiveUpdated> {

	private final String id;
	private final String vault;
	private final RequestBuilder formBuilder;

	public UpdateArchive(String vault, String id) {
		super(ArchiveUpdated.class);
		this.vault = Objects.requireNonNull(vault, "Vault name must not be null");
		this.id = id; // null for createArchive calls

		formBuilder = new RequestBuilder();
	}

	private String absolteFilename(String fileName) {
		if (fileName.startsWith("/"))
			return fileName;
		return "/" + fileName;
	}

	public UpdateArchive addFile(String name, Path source) {
		formBuilder.multipart(absolteFilename(name), source, source.getFileName().toString(),
			CDStarRestClient.X_AUTODETECT);
		return this;
	}

	public UpdateArchive addFile(String name, Path source, String contentType) {
		formBuilder.multipart(absolteFilename(name), source, source.getFileName().toString(), contentType);
		return this;
	}

	public UpdateArchive addFile(String name, InputStream source, String fileName) {
		formBuilder.multipart(absolteFilename(name), source, fileName, CDStarRestClient.X_AUTODETECT);
		return this;
	}

	public UpdateArchive addFile(String name, InputStream source, String fileName, String contentType) {
		formBuilder.multipart(absolteFilename(name), source, fileName, contentType);
		return this;
	}

	public UpdateArchive moveFile(String name, String newName) {
		formBuilder.multipart("move:" + absolteFilename(newName), name);
		return this;
	}

	public UpdateArchive copyFile(String name, String newName) {
		formBuilder.multipart("copy:" + absolteFilename(newName), name);
		return this;
	}

	public UpdateArchive cloneFile(String name, String newName) {
		formBuilder.multipart("clone:" + absolteFilename(newName), name);
		return this;
	}

	public UpdateArchive fetchFile(String name, String source) {
		formBuilder.multipart("fetch:" + absolteFilename(name), source);
		return this;
	}

	public UpdateArchive removeFile(String name) {
		formBuilder.multipart("delete:" + absolteFilename(name), name);
		return this;
	}

	public UpdateArchive setMeta(String key, String... values) {
		return setMeta(key, Arrays.asList(values));
	}

	public UpdateArchive setMeta(String key, Iterable<String> values) {
		for (final String value : values)
			formBuilder.multipart("meta:" + key, value);
		return this;
	}

	public UpdateArchive setFileMeta(String file, String key, String... values) {
		return setFileMeta(file, key, Arrays.asList(values));
	}

	public UpdateArchive setFileMeta(String file, String key, Iterable<String> values) {
		for (final String value : values)
			formBuilder.multipart("meta:" + key + ":" + absolteFilename(file), value);
		return this;
	}

	public UpdateArchive setProfile(String name) {
		formBuilder.multipart("profile", name);
		return this;
	}

	@Override
	protected void prepareRequest(RequestBuilder builder) {
		if (id == null)
			builder.POST(vault);
		else
			builder.POST(vault, id);
		builder.entity(formBuilder.buildEntity());
	}

}
