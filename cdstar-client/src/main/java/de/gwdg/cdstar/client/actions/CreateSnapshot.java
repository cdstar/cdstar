package de.gwdg.cdstar.client.actions;

import de.gwdg.cdstar.client.BaseAction;
import de.gwdg.cdstar.client.RequestBuilder;
import de.gwdg.cdstar.web.common.model.SnapshotInfo;

public class CreateSnapshot extends BaseAction<SnapshotInfo> {

	public static final String SNAPSHOT_SEPARATOR = "@";

	String vault;
	String id;
	String name;

	public CreateSnapshot(String vault, String id, String name) {
		super(SnapshotInfo.class);
		this.vault = vault;
		this.id = id;
		this.name = name;
	}

	@Override
	protected void prepareRequest(RequestBuilder builder) {
		builder.POST(vault, id)
				.query("snapshots", "")
				.form("name", name);
	}

	static String buildSnapshotId(String archiveId, String snapshotName) {
		return snapshotName == null ? archiveId : archiveId + SNAPSHOT_SEPARATOR + snapshotName;
	}

}
