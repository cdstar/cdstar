package de.gwdg.cdstar.client;

import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpHead;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.client.utils.URIUtils;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.FileEntity;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;

import com.fasterxml.jackson.core.JsonProcessingException;

/**
 * Convenient builder for {@link HttpUriRequest}s. The same builder can build
 * the same request multiple times, allowing auto-repeat-logic to work.
 */
public class RequestBuilder {
	private static final ContentType UTF8_TEXT = ContentType.TEXT_PLAIN.withCharset(StandardCharsets.UTF_8);
	private static final List<String> ALLOWED_METHODS = Arrays.asList("GET", "HEAD", "POST", "PUT", "DELETE");

	private final List<String> path = new ArrayList<>();
	private final List<NameValuePair> query = new ArrayList<>();
	private final List<Header> headers = new ArrayList<>();
	private String method = "GET";

	// These are mutely exclusive
	private HttpEntity entity;
	private List<NameValuePair> form;
	private MultipartEntityBuilder multipart;
	private URI baseUri;
	private Object context;

	/**
	 * Create a new empty builder that defaults to GET and no
	 * {@link #baseUri(URI)}.
	 */
	public RequestBuilder() {
	}

	/**
	 * Bind a context to this repeatable request.
	 */
	public RequestBuilder bindContext(Object context) {
		this.context = context;
		return this;
	}

	public Object getContext() {
		return context;
	}

	/**
	 * Create a new empty builder with a given baseURI, method and path.
	 */
	public RequestBuilder(String method, URI baseUri, String... path) {
		baseUri(baseUri);
		method(method);
		path(path);
	}

	public RequestBuilder baseUri(URI base) {
		baseUri = base;
		return this;
	}

	public RequestBuilder GET(String... parts) {
		path.clear();
		return method("GET").path(parts);
	}

	public RequestBuilder HEAD(String... parts) {
		path.clear();
		return method("HEAD").path(parts);
	}

	public RequestBuilder POST(String... parts) {
		path.clear();
		return method("POST").path(parts);
	}

	public RequestBuilder PUT(String... parts) {
		path.clear();
		return method("PUT").path(parts);
	}

	public RequestBuilder DELETE(String... parts) {
		path.clear();
		return method("DELETE").path(parts);
	}

	public RequestBuilder method(String method) {
		if (!ALLOWED_METHODS.contains(method))
			throw new UnsupportedOperationException("Unknown HTTP method: " + method);
		this.method = method;
		return this;
	}

	public RequestBuilder path(String part) {
		path.add(part);
		return this;
	}

	public RequestBuilder path(String... parts) {
		path.addAll(Arrays.asList(parts));
		return this;
	}

	public RequestBuilder query(String name, String value) {
		query.add(new BasicNameValuePair(name, value));
		return this;
	}

	public RequestBuilder query(String name, boolean value) {
		return query(name, Boolean.toString(value));
	}

	public RequestBuilder query(String name, int value) {
		return query(name, Integer.toString(value));
	}

	public RequestBuilder query(String name, long value) {
		return query(name, Long.toString(value));
	}

	public RequestBuilder header(String name, String value) {
		headers.removeIf(h -> h.getName().equalsIgnoreCase(name));
		return addHeader(name, value);
	}

	public RequestBuilder addHeader(String name, String value) {
		headers.add(new BasicHeader(name, value));
		return this;
	}

	public RequestBuilder accept(String acceptHeader) {
		return header("Accpet", acceptHeader);
	}

	public RequestBuilder form(String key, String value) {
		if (form == null) {
			if (multipart != null)
				throw new IllegalAccessError("Request already set as multipart/form-data");
			if (entity != null)
				throw new IllegalAccessError("Request already set as " + entity.getContentType());
			form = new ArrayList<>();
		}
		form.add(new BasicNameValuePair(key, value));
		return this;
	}

	private MultipartEntityBuilder prepareMultipart() {
		if (multipart == null) {
			if (form != null)
				throw new IllegalAccessError("Request already set as application/x-www-form-urlencoded");
			if (entity != null)
				throw new IllegalAccessError("Request already set as " + entity.getContentType());
			multipart = MultipartEntityBuilder.create();
		}
		return multipart;
	}

	public RequestBuilder multipart(String key, String value) {
		prepareMultipart().addTextBody(key, value, UTF8_TEXT);
		return this;
	}

	public RequestBuilder multipart(String key, Path value, String filename, String contentType) {
		if (filename == null)
			filename = value.getFileName().toString();
		if (contentType == null)
			contentType = "application/x-autodetect";
		prepareMultipart().addBinaryBody(key, value.toFile(), ContentType.create(contentType), filename);
		return this;
	}

	public RequestBuilder multipart(String key, InputStream value, String filename, String contentType) {
		if (filename == null)
			filename = "";
		if (contentType == null)
			contentType = "application/x-autodetect";
		prepareMultipart().addBinaryBody(key, value, ContentType.create(contentType), filename);
		return this;
	}

	public RequestBuilder multipart(String key, Path value) {
		return multipart(key, value, null, null);
	}

	public RequestBuilder entity(HttpEntity entity) {
		if (multipart != null)
			throw new IllegalAccessError("Request already set as multipart/form-data");
		if (form != null)
			throw new IllegalAccessError("Request already set as application/x-www-form-urlencoded");
		this.entity = entity;
		return this;
	}

	public RequestBuilder entity(String entity, String mimeType) {
		return entity(entity, mimeType, StandardCharsets.UTF_8);
	}

	public RequestBuilder entity(String entity, String mimeType, Charset charset) {
		ContentType ctype = ContentType.create(mimeType);
		if (charset != null)
			ctype = ctype.withCharset(charset);
		return entity(new StringEntity(entity, ctype));
	}

	public RequestBuilder entity(byte[] entity, String mimeType, Charset charset) {
		ContentType ctype = ContentType.create(mimeType);
		if (charset != null)
			ctype = ctype.withCharset(charset);
		return entity(new ByteArrayEntity(entity, ctype));
	}

	public RequestBuilder entity(Path entity, String mimeType, Charset charset) {
		ContentType ctype = ContentType.create(mimeType);
		if (charset != null)
			ctype = ctype.withCharset(charset);
		return entity(new FileEntity(entity.toFile(), ctype));
	}

	public RequestBuilder entity(InputStream entity, String mimeType, Charset charset) {
		ContentType ctype = ContentType.create(mimeType);
		if (charset != null)
			ctype = ctype.withCharset(charset);
		return entity(new InputStreamEntity(entity, ctype));
	}

	public RequestBuilder json(Object pojo) {
		try {
			entity(new ByteArrayEntity(HttpUtils.json.writeValueAsBytes(pojo), ContentType.APPLICATION_JSON));
		} catch (final JsonProcessingException e) {
			throw new IllegalArgumentException("Json entity failed to serialize", e);
		}
		return this;
	}

	/**
	 * Return the full URI for this request, including query parameters. This
	 * returns a relative URI if the base URI is not set.
	 */
	private String encodePath(String p) {
		return HttpUtils.urlEncode(p, false, StandardCharsets.UTF_8);
	}

	public URI buildURI() {
		try {
			final String fpath = path.stream().map(this::encodePath).collect(Collectors.joining("/"));
			URI uri = new URIBuilder(fpath).addParameters(query).build();
			if (baseUri != null)
				uri = URIUtils.resolve(baseUri, uri);
			return uri;
		} catch (final URISyntaxException e) {
			// Unlikely
			throw new IllegalStateException("Failed do build URI out of the given parts", e);
		}
	}

	/**
	 * Return an HttpEntity that is not affected by future changes in this
	 * builder.
	 */
	public HttpEntity buildEntity() {
		if (entity != null)
			return entity;
		if (form != null)
			// No copy needed, parameters are turned into a string immediately.
			return new UrlEncodedFormEntity(form, StandardCharsets.UTF_8);
		if (multipart != null)
			return multipart.build();

		// Default to an empty form for POST requests.
		if ("POST".equalsIgnoreCase(method))
			return new UrlEncodedFormEntity(Collections.emptyList(), StandardCharsets.UTF_8);

		return null;
	}

	/**
	 * Build an HTTP request that is not affected by future changes in this
	 * builder. This requires a base URI to be set.
	 */
	public HttpUriRequest buildRequest() {
		HttpUriRequest rq;
		if ("GET".equalsIgnoreCase(method)) {
			rq = new HttpGet(buildURI());
		} else if ("HEAD".equalsIgnoreCase(method)) {
			rq = new HttpHead(buildURI());
		} else if ("POST".equalsIgnoreCase(method)) {
			rq = new HttpPost(buildURI());
		} else if ("PUT".equalsIgnoreCase(method)) {
			rq = new HttpPut(buildURI());
		} else if ("DELETE".equalsIgnoreCase(method)) {
			rq = new HttpDelete(buildURI());
		} else {
			throw new UnsupportedOperationException("Unknown method: " + method);
		}

		if (headers != null)
			headers.forEach(rq::addHeader);

		if (rq instanceof HttpEntityEnclosingRequestBase) {
			final HttpEntity entity = buildEntity();
			if (entity != null) {
				((HttpEntityEnclosingRequestBase) rq).setEntity(entity);
			}
		}

		return rq;
	}

	@Override
	public String toString() {
		return method + " " + buildURI();
	}

}