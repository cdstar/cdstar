package de.gwdg.cdstar.client.helper;

import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.concurrent.CompletableFuture;

import de.gwdg.cdstar.client.CDStarRestClient;
import de.gwdg.cdstar.client.actions.ScrollVault;

/**
 * An iterator over all archive IDs in a vault. Requests are triggered lazily
 * while the iterator is consumed. Every {@link #limit(int)} items, a new
 * request may be triggered to fetch more items.
 */

public class ListVaultIterable extends AbstractRequestIterator<String> {

	private final String vault;
	private String scrollId;
	private int limit = -1;

	public ListVaultIterable(CDStarRestClient client, String vault) {
		this(client, vault, "");
	}

	public ListVaultIterable(CDStarRestClient client, String vault, String startId) {
		super(client);
		this.vault = vault;
		scrollId = startId;
	}

	public ListVaultIterable limit(int limit) {
		checkIterationNotStarted();
		this.limit = limit;
		return this;
	}

	@Override
	public CompletableFuture<Collection<String>> getNextBatch() throws IOException {
		return new ScrollVault(vault, scrollId).limit(limit)
			.submit(client)
			.thenApply((list) -> {
				if (list.results.isEmpty())
					return Collections.emptyList();
				scrollId = list.results.get(list.results.size() - 1);
				return list.results;
			});
	}

}
