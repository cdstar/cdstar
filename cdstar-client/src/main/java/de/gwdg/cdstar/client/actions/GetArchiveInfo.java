package de.gwdg.cdstar.client.actions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import de.gwdg.cdstar.client.BaseAction;
import de.gwdg.cdstar.client.RequestBuilder;
import de.gwdg.cdstar.web.common.model.ArchiveInfo;

public class GetArchiveInfo extends BaseAction<ArchiveInfo> {
	private final String vault;
	private final String id;
	private boolean withMeta;
	private boolean withACL;
	private boolean withFiles;
	private boolean withSnapshots;
	private List<String> includeFilter;
	private List<String> excludeFilter;
	private int limit = -1;
	private int offset = -1;
	private String order;
	private Boolean reverse;

	public GetArchiveInfo(String vault, String id) {
		super(ArchiveInfo.class);
		this.vault = vault;
		this.id = id;
	}

	public GetArchiveInfo withMeta(boolean enable) {
		withMeta = enable;
		return this;
	}

	public GetArchiveInfo withAcl(boolean enable) {
		withACL = enable;
		return this;
	}

	public GetArchiveInfo withFiles(boolean enable) {
		withFiles = enable;
		return this;
	}

	public GetArchiveInfo withSnapshots(boolean enable) {
		withSnapshots = enable;
		return this;
	}

	public GetArchiveInfo includeFiles(String... glob) {
		if (includeFilter == null)
			includeFilter = new ArrayList<>();
		includeFilter.addAll(Arrays.asList(glob));
		return this;
	}

	public GetArchiveInfo excludeFiles(String... glob) {
		if (excludeFilter == null)
			excludeFilter = new ArrayList<>();
		excludeFilter.addAll(Arrays.asList(glob));
		return this;
	}

	public GetArchiveInfo page(int offset, int limit) {
		this.offset = offset;
		this.limit = limit;
		return this;
	}

	public GetArchiveInfo order(String orderBy, boolean desc) {
		order = orderBy;
		reverse = Boolean.valueOf(desc);
		return this;
	}

	@Override
	protected void prepareRequest(RequestBuilder builder) {
		builder.GET(vault, id);
		if (withMeta)
			builder.query("with", "meta");
		if (withACL)
			builder.query("with", "acl");
		if (withFiles)
			builder.query("with", "files");
		if (withSnapshots)
			builder.query("with", "snapshots");
		if (includeFilter != null)
			for (final String glob : includeFilter)
				builder.query("include", glob);
		if (excludeFilter != null)
			for (final String glob : excludeFilter)
				builder.query("exclude", glob);
		if (offset > 0)
			builder.query("offset", Integer.toString(offset));
		if (limit > 0)
			builder.query("limit", Integer.toString(limit));
		if (order != null)
			builder.query("order", order);
		if (reverse != null && reverse.booleanValue())
			builder.query("reverse", "true");
	}
}
