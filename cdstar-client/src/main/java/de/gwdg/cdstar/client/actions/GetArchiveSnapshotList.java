package de.gwdg.cdstar.client.actions;

import de.gwdg.cdstar.client.BaseAction;
import de.gwdg.cdstar.client.RequestBuilder;
import de.gwdg.cdstar.web.common.model.SnapshotList;

public class GetArchiveSnapshotList extends BaseAction<SnapshotList> {
	private final String vault;
	private final String id;

	public GetArchiveSnapshotList(String vault, String id) {
		super(SnapshotList.class);
		this.vault = vault;
		this.id = id;
	}

	@Override
	protected void prepareRequest(RequestBuilder builder) {
		builder.GET(vault, id);
		builder.query("snapshots", "");
	}
}
