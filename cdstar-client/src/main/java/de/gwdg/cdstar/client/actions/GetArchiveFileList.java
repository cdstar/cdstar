package de.gwdg.cdstar.client.actions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import de.gwdg.cdstar.client.BaseAction;
import de.gwdg.cdstar.client.RequestBuilder;
import de.gwdg.cdstar.web.common.model.FileList;

public class GetArchiveFileList extends BaseAction<FileList> {
	private final String vault;
	private final String id;
	private String snapshot;
	private boolean withMeta;
	private List<String> includeFilter;
	private List<String> excludeFilter;
	private int limit = -1;
	private long offset = 0;
	private String order;
	private Boolean reverse;

	public GetArchiveFileList(String vault, String id, String snapshot) {
		super(FileList.class);
		this.vault = vault;
		this.id = id;
		this.snapshot = snapshot;
	}

	public GetArchiveFileList(String vault, String id) {
		this(vault, id, null);
	}

	public GetArchiveFileList withMeta(boolean enable) {
		withMeta = enable;
		return this;
	}

	public GetArchiveFileList includeFiles(String... glob) {
		if (includeFilter == null)
			includeFilter = new ArrayList<>();
		includeFilter.addAll(Arrays.asList(glob));
		return this;
	}

	public GetArchiveFileList excludeFiles(String... glob) {
		if (excludeFilter == null)
			excludeFilter = new ArrayList<>();
		excludeFilter.addAll(Arrays.asList(glob));
		return this;
	}

	public GetArchiveFileList page(long offset, int limit) {
		this.offset = offset;
		this.limit = limit;
		return this;
	}

	public GetArchiveFileList order(String orderBy, boolean desc) {
		order = orderBy;
		reverse = Boolean.valueOf(desc);
		return this;
	}

	@Override
	protected void prepareRequest(RequestBuilder builder) {
		builder.GET(vault, id).query("files", true);
		if (withMeta)
			builder.query("with", "meta");
		if (includeFilter != null)
			for (final String glob : includeFilter)
				builder.query("include", glob);
		if (excludeFilter != null)
			for (final String glob : excludeFilter)
				builder.query("exclude", glob);
		if (offset > 0)
			builder.query("offset", Long.toString(offset));
		if (limit > 0)
			builder.query("limit", Integer.toString(limit));
		if (order != null)
			builder.query("order", order);
		if (reverse != null && reverse.booleanValue())
			builder.query("reverse", "true");
	}
}
