package de.gwdg.cdstar.client.actions;

import de.gwdg.cdstar.client.BaseAction;
import de.gwdg.cdstar.client.RequestBuilder;

public class CommitTransaction extends BaseAction<Void> {

	private final String tx;

	public CommitTransaction(String tx) {
		super(Void.class);
		this.tx = tx;
	}

	@Override
	protected void prepareRequest(RequestBuilder builder) {
		builder.POST("_tx", tx);
	}

}
