package de.gwdg.cdstar.client.actions;

import de.gwdg.cdstar.client.BaseAction;
import de.gwdg.cdstar.client.RequestBuilder;

public class DeleteFile extends BaseAction<Void> {
	private final String vault;
	private final String id;
	private final String name;

	public DeleteFile(String vault, String id, String name) {
		super(Void.class);
		this.vault = vault;
		this.id = id;
		this.name = name;
	}

	@Override
	protected void prepareRequest(RequestBuilder builder) {
		builder.DELETE(vault, id, name);
	}
}
