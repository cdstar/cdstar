package de.gwdg.cdstar.client.actions;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;

import de.gwdg.cdstar.client.BaseAction;
import de.gwdg.cdstar.client.RequestBuilder;
import de.gwdg.cdstar.client.actions.DownloadFile.DownloadReport;

/**
 * Download a file from an CDStar archive to a temporary file.
 *
 * The location of the temporary file can be specified so it is created directly
 * on the target device and only needs to be renamed after the download is
 * complete.
 */
public class DownloadFile extends BaseAction<DownloadReport> {

	private final String vault;
	private final String id;
	private final String name;
	private long limit = -1;
	private long offset = 0;
	private Path path;
	private boolean keepPartial;

	public DownloadFile(String vault, String id, String fileName) {
		super(DownloadReport.class);
		this.vault = vault;
		this.id = id;
		name = fileName;
	}

	public static final class DownloadReport {
		final String contentType;
		final long bytes;

		final Path target;

		DownloadReport(String contentType, long bytes, Path target) {
			this.contentType = contentType;
			this.bytes = bytes;
			this.target = target;
		}

		/**
		 * Return the number of bytes successfully downloaded, or -1 for partial
		 * downloads. You'll have to check the actual file size on disk to see how much
		 * was downloaded successfully.
		 */
		public long getBytes() {
			return bytes;
		}

		/**
		 * Return true if the download failed or was aborted after some bytes were
		 * stored.
		 */
		public boolean isPartial() {
			return bytes > 0;
		}

		/**
		 * Get the path to the (partially) downloaded file.
		 */
		public Path getPath() {
			return target;
		}
	}

	/**
	 * Download only a part of a given file.
	 */
	public DownloadFile range(long offset, long limit) {
		if (offset < 0)
			throw new IllegalArgumentException("Offset must be at least 0");
		if (limit < 0 && limit != -1)
			throw new IllegalArgumentException("Limit must be larger than 0, or -1");
		this.offset = offset;
		this.limit = limit;
		return this;
	}

	/**
	 * Download file to this (existing) folder.
	 *
	 * @throws IOException if the path is not a folder
	 */
	public DownloadFile path(Path path) throws IOException {
		if (!Files.isDirectory(path))
			throw new IOException("Not a directory: " + path);
		this.path = path;
		return this;
	}

	/**
	 * If true, IOExceptions during the download are ignored and partial downloads
	 * are kept on disk.
	 */
	public DownloadFile keepPartial(boolean enable) {
		keepPartial = enable;
		return this;
	}

	/**
	 * Download file to this (existing) folder.
	 *
	 * @throws IOException if the path is not a folder
	 */
	public DownloadFile path(String path) throws IOException {
		return path(Paths.get(path));
	}

	@Override
	protected void prepareRequest(RequestBuilder builder) {
		builder.GET(vault, id, name);
		if (offset > 0 && limit > 0)
			builder.addHeader("Range", "bytes=" + offset + "-" + (offset + limit - 1));
		else if (offset > 0)
			builder.addHeader("Range", "bytes=" + offset + "-");
		else if (limit > 0)
			builder.addHeader("Range", "bytes=0-" + (limit - 1));
	}

	@Override
	protected DownloadReport handleResponse(HttpResponse response) throws IOException {
		final int code = response.getStatusLine().getStatusCode();

		if (code != 200 && code != 206)
			return super.handleResponse(response);

		final HttpEntity entity = response.getEntity();
		if (entity == null)
			throw new IOException("Expected response entity");

		final Path target = path != null
			? Files.createTempFile(path, "cdstar", ".part")
			: Files.createTempFile("cdstar", ".part");

		if ((offset > 0 || limit > 0) && code != 206) {
			throw new IOException("Missing server support for range requests.");
		}

		final String contentType = entity.getContentType().getValue();

		long bytes = 0;
		try (FileOutputStream out = new FileOutputStream(target.toFile());
			InputStream in = entity.getContent()) {

			final byte[] buffer = new byte[1024 * 8];
			int n;
			while ((n = in.read(buffer)) > 0) {
				out.write(buffer, 0, n);
				bytes += n;
			}

		} catch (final Exception e) {
			if (keepPartial && bytes > 0)
				return new DownloadReport(contentType, -1, target);
			Files.deleteIfExists(target);
			throw e;
		}
		return new DownloadReport(contentType, bytes, target);
	}

}
