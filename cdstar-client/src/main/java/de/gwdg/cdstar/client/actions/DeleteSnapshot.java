package de.gwdg.cdstar.client.actions;

import de.gwdg.cdstar.client.BaseAction;
import de.gwdg.cdstar.client.RequestBuilder;

public class DeleteSnapshot extends BaseAction<Void> {

	String vault;
	String id;
	String name;

	public DeleteSnapshot(String vault, String id, String name) {
		super(Void.class);
		this.vault = vault;
		this.id = id;
		this.name = name;
	}

	@Override
	protected void prepareRequest(RequestBuilder builder) {
		builder.DELETE(vault, id + "@" + name);
	}

}
