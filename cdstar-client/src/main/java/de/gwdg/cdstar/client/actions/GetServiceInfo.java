package de.gwdg.cdstar.client.actions;

import de.gwdg.cdstar.client.BaseAction;
import de.gwdg.cdstar.client.RequestBuilder;
import de.gwdg.cdstar.web.common.model.ServiceInfo;

/**
 * Return the service info and the list of public or private vaults the current
 * user can see.
 */
public class GetServiceInfo extends BaseAction<ServiceInfo> {

	public GetServiceInfo() {
		super(ServiceInfo.class);
	}

	@Override
	protected void prepareRequest(RequestBuilder builder) {
		builder.GET();
	}
}
