package de.gwdg.cdstar.client.actions;

import de.gwdg.cdstar.client.BaseAction;
import de.gwdg.cdstar.client.RequestBuilder;
import de.gwdg.cdstar.web.common.model.VaultInfo;

public class GetVaultInfo extends BaseAction<VaultInfo> {
	private final String vault;

	public GetVaultInfo(String vault) {
		super(VaultInfo.class);
		this.vault = vault;
	}

	@Override
	protected void prepareRequest(RequestBuilder builder) {
		builder.GET(vault);
	}
}
