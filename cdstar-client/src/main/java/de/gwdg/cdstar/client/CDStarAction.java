package de.gwdg.cdstar.client;

import de.gwdg.cdstar.client.actions.BeginTransaction;
import de.gwdg.cdstar.client.actions.CheckArchiveExists;
import de.gwdg.cdstar.client.actions.CheckFileExists;
import de.gwdg.cdstar.client.actions.CheckVaultExists;
import de.gwdg.cdstar.client.actions.CommitTransaction;
import de.gwdg.cdstar.client.actions.CreateArchive;
import de.gwdg.cdstar.client.actions.DeleteFile;
import de.gwdg.cdstar.client.actions.DownloadFile;
import de.gwdg.cdstar.client.actions.GetArchiveFileList;
import de.gwdg.cdstar.client.actions.GetArchiveInfo;
import de.gwdg.cdstar.client.actions.GetFileInfo;
import de.gwdg.cdstar.client.actions.GetFileStream;
import de.gwdg.cdstar.client.actions.GetServiceInfo;
import de.gwdg.cdstar.client.actions.GetVaultInfo;
import de.gwdg.cdstar.client.actions.RenewTransaction;
import de.gwdg.cdstar.client.actions.RollbackTransaction;
import de.gwdg.cdstar.client.actions.ScrollVault;
import de.gwdg.cdstar.client.actions.UpdateArchive;
import de.gwdg.cdstar.client.actions.UploadFile;

/**
 * Helper class to easily browse and access cdstar rest client actions via
 * static methods. If not documented otherwise, each static method corresponds
 * to a {@link BaseAction} with the same name and simply returns a new instances
 * by passing all parameters to a suitable constructor.
 *
 * For example, {@link #createArchive(String)} corresponds to
 * {@link CreateArchive#CreateArchive(String)}.
 *
 * TODO: Add more actions as they are used.
 */
public class CDStarAction {
	private CDStarAction() {
	}

	public static BeginTransaction beginTransaction() {
		return new BeginTransaction();
	}

	public static CheckArchiveExists checkArchiveExists(String vault, String id) {
		return new CheckArchiveExists(vault, id);
	}

	public static CheckFileExists checkFileExists(String vault, String id, String fname) {
		return new CheckFileExists(vault, id, fname);
	}

	public static CheckVaultExists checkVaultExists(String vault) {
		return new CheckVaultExists(vault);
	}

	public static CommitTransaction commitTransaction(String tx) {
		return new CommitTransaction(tx);
	}

	public static CreateArchive createArchive(String vault) {
		return new CreateArchive(vault);
	}

	public static DeleteFile deleteFile(String vault, String id, String fname) {
		return new DeleteFile(vault, id, fname);
	}

	public static DownloadFile downloadFile(String vault, String id, String fname) {
		return new DownloadFile(vault, id, fname);
	}

	public static GetArchiveFileList getArchiveFileList(String vault, String id) {
		return new GetArchiveFileList(vault, id);
	}

	public static GetArchiveInfo getArchiveInfo(String vaultName, String archiveId) {
		return new GetArchiveInfo(vaultName, archiveId);
	}

	public static GetFileInfo getFileInfo(String vault, String id, String fname) {
		return new GetFileInfo(vault, id, fname);
	}

	public static GetFileStream getFileStream(String vault, String id, String name) {
		return new GetFileStream(vault, id, name);
	}

	public static GetServiceInfo getSerciveInfo() {
		return new GetServiceInfo();
	}

	public static GetVaultInfo getVaultInfo(String vault) {
		return new GetVaultInfo(vault);
	}

	public static RenewTransaction renewTransaction(String tx) {
		return new RenewTransaction(tx);
	}

	public static RollbackTransaction rollbackTransaction(String tx) {
		return new RollbackTransaction(tx);
	}

	public static ScrollVault scrollVault(String vault) {
		return new ScrollVault(vault);
	}

	public static UpdateArchive updateArchive(String vault, String id) {
		return new UpdateArchive(vault, id);
	}

	public static UploadFile uploadFile(String vault, String id, String fname) {
		return new UploadFile(vault, id, fname);
	}

}
