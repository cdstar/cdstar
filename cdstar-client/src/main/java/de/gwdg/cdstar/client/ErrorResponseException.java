package de.gwdg.cdstar.client;

import java.io.IOException;
import java.net.URI;

import org.apache.http.client.methods.HttpUriRequest;

import de.gwdg.cdstar.web.common.model.ErrorResponse;

public class ErrorResponseException extends IOException {
	private static final long serialVersionUID = 2816295717894991445L;
	private final ErrorResponse error;
	private String method;
	private URI uri;

	public ErrorResponseException(ErrorResponse error) {
		this.error = error;
	}

	public ErrorResponse getError() {
		return error;
	}

	public void attachRequest(HttpUriRequest rq) {
		method = rq.getMethod();
		uri = rq.getURI();
	}

	@Override
	public String getMessage() {
		return toString();
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder();
		sb.append("Request [").append(method).append(" ").append(uri.toString()).append("]");
		sb.append(" failed: ").append(error);
		return sb.toString();
	}

}
