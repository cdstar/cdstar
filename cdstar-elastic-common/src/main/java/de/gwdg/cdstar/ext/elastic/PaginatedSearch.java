package de.gwdg.cdstar.ext.elastic;

import java.time.Duration;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import com.fasterxml.jackson.core.JsonProcessingException;

import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.ext.elastic.SearchRequest.ESSearchResult;

public class PaginatedSearch implements Iterable<ESSearchResult>, AutoCloseable {
	private SearchRequest query;
	private Duration fetchTimeout;
	private PaginatedSearchIterator iterator;

	PaginatedSearch(SearchRequest query, Duration fetchTimeout) {
		this.query = query.clone();
		this.fetchTimeout = fetchTimeout;
		this.iterator = new PaginatedSearchIterator();
	}

	@Override
	public Iterator<ESSearchResult> iterator() {
		return iterator;
	}

	@Override
	public void close() {
		Utils.closeQuietly(iterator::cleanup);
	}

	public class PaginatedSearchIterator implements Iterator<ESSearchResult> {

		private ESSearchResult nextResult;
		private ESSearchResult lastResult;
		private boolean isClosed;

		@Override
		public synchronized boolean hasNext() {
			if (nextResult == null)
				nextResult = fetchNext();
			return nextResult != null;
		}

		@Override
		public synchronized ESSearchResult next() {
			if (!hasNext())
				throw new NoSuchElementException();
			lastResult = nextResult;
			nextResult = null;
			return lastResult;
		}

		private ESSearchResult fetchNext() {
			if (isClosed)
				return null;

			try {
				var nextQuery = query.clone();
				if (lastResult == null) {
					return nextQuery.execute(fetchTimeout.toMillis(), TimeUnit.MILLISECONDS);
				} else if (!lastResult.hits().isEmpty()) {
					nextQuery.nextPage(lastResult);
					return nextQuery.execute(fetchTimeout.toMillis(), TimeUnit.MILLISECONDS);
				} else {
					Utils.closeQuietly(this::cleanup);
					return null;
				}

			} catch (InterruptedException | TimeoutException | CancellationException | ESErrorResponse e) {
				return null;
			}
		}

		private void cleanup() throws JsonProcessingException, CancellationException, ExecutionException,
			InterruptedException, TimeoutException {

			if (isClosed)
				return;

			isClosed = true;

			if (query.pitId == SearchRequest.SCROLL_REQUEST) {
				var rq = query.esClient.buildRequest("_pit");
				var body = query.esClient.json();
				body.put("id", lastResult.getPitId());
				query.esClient.sendMethod("DELETE", rq, body).get(fetchTimeout.toMillis(), TimeUnit.MILLISECONDS);
			}

			if (query.scrollId == SearchRequest.SCROLL_REQUEST) {
				var rq = query.esClient.buildRequest("_search", "scroll");
				var body = query.esClient.json();
				body.put("scroll_id", lastResult.getScrollId());
				query.esClient.sendMethod("DELETE", rq, body).get(fetchTimeout.toMillis(), TimeUnit.MILLISECONDS);
			}

		}

	}

}