package de.gwdg.cdstar.ext.elastic;

import java.net.http.HttpResponse;

import com.fasterxml.jackson.databind.node.ObjectNode;

public class ESErrorResponse extends Exception {
	private static final long serialVersionUID = -5606901844607233663L;

	public static final String TYPE_INDEX_ALREADY_EXISTS = "resource_already_exists_exception";
	public static final String TYPE_INDEX_NOT_FOUND = "index_not_found_exception";

	private final HttpResponse<?> response;
	private final ObjectNode json;

	public ESErrorResponse(HttpResponse<?> response, ObjectNode objectNode) {
		super(response.statusCode() + ": " + objectNode.toString());
		this.response = response;
		json = objectNode;
	}

	public ObjectNode getJson() {
		return json;
	}

	public String getType() {
		return json.path("error").path("type").asText("missing_error_type");
	}

	public String getReason() {
		return json.path("error").path("reason").asText("No reason provided");
	}

	public HttpResponse<?> getResponse() {
		return response;
	}
}
