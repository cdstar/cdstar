package de.gwdg.cdstar.ext.elastic;

import java.net.http.HttpRequest.Builder;

import com.fasterxml.jackson.databind.node.ObjectNode;

import de.gwdg.cdstar.Promise;
import de.gwdg.cdstar.ext.elastic.ElasticSearchClient.BaseResult;
import de.gwdg.cdstar.ext.elastic.IndexRequest.IndexResponse;

public class IndexRequest implements ElasticSearchClient.ESPreparedRequest<IndexResponse> {
	private final ElasticSearchClient esClient;
	private final String index;
	private final String id;
	private final Object document;

	public IndexRequest(ElasticSearchClient elasticSearchClient, String index, String id, Object document) {
		esClient = elasticSearchClient;
		this.index = index;
		this.id = id;
		this.document = document;
	}

	@Override
	public Promise<IndexResponse> submit() {

		Builder rq = esClient.version().needsDoc()
			? (id == null
				? esClient.buildRequest(index, ElasticSearchClient.DEFAULT_TYPE_NAME)
				: esClient.buildRequest(index, ElasticSearchClient.DEFAULT_TYPE_NAME, id))
			: (id == null
				? esClient.buildRequest(index)
				: esClient.buildRequest(index, id));

		return esClient.put(rq, document).map(r -> new IndexResponse(r));
	}

	public class IndexResponse extends BaseResult {

		public IndexResponse(ObjectNode r) {
			super(r);
		}

	}

}
