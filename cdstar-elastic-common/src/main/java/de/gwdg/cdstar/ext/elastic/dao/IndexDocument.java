package de.gwdg.cdstar.ext.elastic.dao;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = IndexDocument.SCOPE_MARKER)
@JsonSubTypes({ @Type(value = ArchiveDocument.class, name = ArchiveDocument.SCOPE_NAME),
		@Type(value = FileDocument.class, name = FileDocument.SCOPE_NAME) })
public abstract class IndexDocument {

	@JsonIgnore
	public abstract String getIndexID();

	public static final String SCOPE_MARKER = "scope";

	public static final String ID_FIELD = "id";
	public static final String VAULT_FIELD = "vault";
	public static final String OWNER_FIELD = "owner";
	public static final String TAGS_FIELD = "is";
	public static final String CREATED_FIELD = "created";
	public static final String MODIFIED_FIELD = "modified";

	public static final String SIZE_FIELD = "size";
	public static final String META_FIELD = "meta";

	public static final String READ_ACCESS_FIELD = "__read_access";

	@JsonProperty(ID_FIELD)
	public String id;
	@JsonProperty(VAULT_FIELD)
	public String vault;
	@JsonProperty(OWNER_FIELD)
	public String owner;
	@JsonProperty(TAGS_FIELD)
	public Set<String> tags;
	@JsonProperty(CREATED_FIELD)
	public Date created;
	@JsonProperty(MODIFIED_FIELD)
	public Date modified;

	@JsonProperty(SIZE_FIELD)
	public long size;

	/**
	 * Only known keys in this dict are indexed.
	 */
	@JsonProperty(META_FIELD)
	public Map<String, List<String>> meta;

	@JsonProperty(READ_ACCESS_FIELD)
	public Set<String> read_access;

}
