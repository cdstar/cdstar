package de.gwdg.cdstar.ext.elastic;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.fasterxml.jackson.databind.node.ObjectNode;

import de.gwdg.cdstar.Promise;
import de.gwdg.cdstar.ext.elastic.BulkRequest.ESBulkResult;
import de.gwdg.cdstar.ext.elastic.ElasticSearchClient.BaseResult;

public class BulkRequest implements ElasticSearchClient.ESPreparedRequest<ESBulkResult> {
	private final ElasticSearchClient esClient;

	BulkRequest(ElasticSearchClient elasticSearchClient) {
		esClient = elasticSearchClient;
	}

	private final List<BulkEntry> bulks = new ArrayList<>();

	private BulkRequest add(String type, String index, String id, ObjectNode data) {
		final ObjectNode command = esClient.json();
		command.with(type)
			.put("_index", index)
			.put("_id", id);
		if (esClient.version().needsDoc())
			command.with(type).put("_type", ElasticSearchClient.DEFAULT_TYPE_NAME);
		bulks.add(new BulkEntry(command, data));
		return this;
	}

	public BulkRequest index(String index, String id, Object data) {
		if (data != null && !(data instanceof ObjectNode))
			data = ElasticSearchClient.om.valueToTree(data);
		return add("index", index, id, (ObjectNode) data);
	}

	public BulkRequest delete(String index, String id) {
		return add("delete", index, id, null);
	}

	public BulkRequest update(String index, String id, Object data) {
		if (data != null && !(data instanceof ObjectNode))
			data = ElasticSearchClient.om.valueToTree(data);
		
		ObjectNode ob = (ObjectNode) data;
		if (!(ob.has("script") || ob.has("doc")))
			ob = (ObjectNode) esClient.json().set("doc", ob);
		
		return add("update", index, id, ob);
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder();
		sb.append("POST _bulk\n");
		for (final BulkEntry b : bulks) {
			sb.append(b.toString()).append("\n");
		}
		return sb.toString();
	}

	public int size() {
		return bulks.size();
	}

	public boolean isEmpty() {
		return bulks.isEmpty();
	}

	@Override
	public Promise<ESBulkResult> submit() {
		if (bulks.isEmpty())
			return Promise.ofError(new IllegalStateException("Empty bulk request"));

		Collection<Object> copy = new ArrayList<>(bulks.size() * 2);
		bulks.forEach(item -> {
			copy.add(item.command);
			if (item.data != null)
				copy.add(item.data);
		});
		bulks.clear(); // Allow GC

		return esClient
			.sendBulk(esClient.buildRequest("_bulk"), copy)
			.map(r -> new ESBulkResult(r));
	}

	public class BulkEntry {
		ObjectNode command;
		ObjectNode data;

		public BulkEntry(ObjectNode command, ObjectNode data) {
			this.command = command;
			this.data = data;
		}

		@Override
		public String toString() {
			if (data != null)
				return esClient.json2string(command) + "\n  " + esClient.json2string(data);
			return esClient.json2string(command);
		}
	}

	public static class ESBulkResult extends BaseResult {

		public ESBulkResult(ObjectNode result) {
			super(result);
		}

		public boolean hasErrors() {
			return getResult().get("errors").asBoolean();
		}

	}

}