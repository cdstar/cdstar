package de.gwdg.cdstar.ext.elastic;

import com.fasterxml.jackson.databind.node.ObjectNode;

import de.gwdg.cdstar.Promise;
import de.gwdg.cdstar.ext.elastic.DeleteByQueryRequest.DeleteByQueryResult;
import de.gwdg.cdstar.ext.elastic.ElasticSearchClient.BaseResult;

public class DeleteByQueryRequest implements ElasticSearchClient.ESPreparedRequest<DeleteByQueryResult> {

	private final ElasticSearchClient esClient;
	private final String index;
	private final ObjectNode query;

	DeleteByQueryRequest(ElasticSearchClient elasticSearchClient, String index) {
		esClient = elasticSearchClient;
		this.index = index;
		query = esClient.json();
	}

	public ObjectNode dsl() {
		return query.with("query");
	}

	@Override
	public Promise<DeleteByQueryResult> submit() {
		return esClient.post(esClient.buildRequest(index, "_delete_by_query"), query)
			.map(r -> new DeleteByQueryResult(r));
	}

	public class DeleteByQueryResult extends BaseResult {

		public DeleteByQueryResult(ObjectNode answer) {
			super(answer);
		}


	}

}
