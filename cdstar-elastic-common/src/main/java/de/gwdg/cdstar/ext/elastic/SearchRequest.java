package de.gwdg.cdstar.ext.elastic;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import de.gwdg.cdstar.Promise;
import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.ext.elastic.ElasticSearchClient.BaseResult;
import de.gwdg.cdstar.ext.elastic.SearchRequest.ESSearchResult;

public class SearchRequest implements Cloneable, ElasticSearchClient.ESPreparedRequest<ESSearchResult> {
	static final String SCROLL_REQUEST = "<request new scroll id>";
	final ElasticSearchClient esClient;
	private final String index;
	private int limit = 25;
	private final ObjectNode query;
	private ArrayNode searchAfter;
	private List<String> sourceInclude = new ArrayList<>();
	private List<String> sourceExclude = new ArrayList<>();
	private List<SortKey> sort = new ArrayList<>();

	String pitId;
	String scrollId;
	private Duration keepAlive;

	SearchRequest(ElasticSearchClient elasticSearchClient, String index) {
		esClient = elasticSearchClient;
		this.index = index;
		query = esClient.json();
	}

	SearchRequest(ElasticSearchClient elasticSearchClient, SearchRequest other) {
		esClient = elasticSearchClient;
		index = other.index;
		limit = other.limit;
		query = other.query.deepCopy();
		// We expect these to not be modified in-place
		searchAfter = other.searchAfter;
		sourceInclude = other.sourceInclude;
		sourceExclude = other.sourceExclude;
		sort = other.sort;

		pitId = other.pitId;
		scrollId = other.scrollId;
		keepAlive = other.keepAlive;
	}

	ObjectNode buildSearchBody() {
		final ObjectNode request = esClient.json();

		request.put("size", limit);
		request.set("query", query);

		for (final String include : sourceInclude)
			request.with("_source").withArray("includes").add(include);
		for (final String exclude : sourceExclude)
			request.with("_source").withArray("excludes").add(exclude);
		for (final SortKey key : sort)
			request.with("sort").put(key.getField(), key.isReversed() ? "DESC" : "ASC");
		if (searchAfter != null)
			request.set("search_after", searchAfter);

		return request;
	}

	@Override
	public Promise<ESSearchResult> submit() {

		var index = Utils.join(",", this.index);
		Promise<ObjectNode> searchResponse;

		if (scrollId == SCROLL_REQUEST) {
			searchResponse = esClient.post(esClient.buildRequest(
				Map.of("scroll", keepAlive.getSeconds() + "s"),
				index, "_search"),
				buildSearchBody());
		} else if (scrollId != null) {
			var body = esClient.json();
			body.put("scroll", keepAlive.toSeconds() + "s");
			body.put("scroll_id", scrollId);
			searchResponse = esClient.post(esClient.buildRequest("_search", "scroll"), body);
		} else if (pitId == SCROLL_REQUEST) {
			searchResponse = esClient.post(
				esClient.buildRequest(Map.of("keep_alive", keepAlive.toSeconds() + "s"),
					index, "_pit"),
				null)
				.flatMap(pitResponse -> {
					var body = buildSearchBody();
					body.with("pit").put("id", pitResponse.get("id").textValue());
					body.with("pit").put("keep_alive", keepAlive.toSeconds() + "s");
					return esClient.post(esClient.buildRequest("_search"), body);
				});
		} else if (pitId != null) {
			var body = buildSearchBody();
			body.with("pit").put("id", pitId);
			body.with("pit").put("keep_alive", keepAlive.toSeconds() + "s");
			searchResponse = esClient.post(esClient.buildRequest("_search"), body);
		} else {
			searchResponse = esClient.post(esClient.buildRequest(index, "_search"), buildSearchBody());
		}

		return searchResponse.map(r -> new ESSearchResult(r))
			.then(null, searchResponse::tryReject);
	}

	int getLimit() {
		return limit;
	}

	String getIndex() {
		return index;
	}

	List<String> getSourceInclude() {
		return sourceInclude;
	}

	List<String> getSourceExclude() {
		return sourceExclude;
	}

	List<SortKey> getSort() {
		return sort;
	}

	ArrayNode getSearchAfter() {
		return searchAfter;
	}

	public ObjectNode dsl() {
		return query;
	}

	public SearchRequest addSource(String field) {
		if(!sourceInclude.contains(field))
			sourceInclude.add(field);
		return this;
	}

	public SearchRequest sources(Collection<String> fields) {
		sourceInclude = new ArrayList<>(fields);
		return this;
	}

	public SearchRequest sources(String... fields) {
		return sources(Arrays.asList(fields));
	}

	public SearchRequest sourceExclude(String... fields) {
		sourceExclude = Arrays.asList(fields);
		return this;
	}

	public SearchRequest limit(int limit) {
		this.limit = limit;
		return this;
	}

	public SearchRequest searchAfter(String... valueList) {
		return searchAfter(List.of(valueList));
	}

	public SearchRequest searchAfter(List<String> valueList) {
		searchAfter = ElasticSearchClient.om.createArrayNode();
		for (String value : valueList)
			searchAfter.add(value);
		return this;
	}

	public SearchRequest searchAfter(ArrayNode valueList) {
		searchAfter = valueList.deepCopy();
		return this;
	}

	/**
	 * Update this query to fetch the next page of a paginated search. This uses
	 * pit_id+search_aftrer, _scroll_id or just search_after if neither pid_id nor
	 * _scroll_id is available.
	 */
	SearchRequest nextPage(ESSearchResult lastPage) {
		if (lastPage.getPitId() != null) {
			pit(lastPage.getPitId(), keepAlive);
			searchAfter = lastPage.getLastSortValues();
		} else if (lastPage.getScrollId() != null) {
			continueScroll(lastPage.getScrollId(), keepAlive);
		} else {
			searchAfter = lastPage.getLastSortValues();
		}
		return this;
	}

	/**
	 * Start a paginated search using either point-in-time or scroll APIs based on
	 * cluster capabilities.
	 *
	 * If there is no point-in-time or scroll token already associated with this
	 * search, one will be created and also cleaned up when the returned iterator is
	 * either exhausting or closed.
	 */
	public PaginatedSearch paginate(Duration keepAlive, Duration fetchTimeout) {
		var q = this.clone();
		if (pitId == null && scrollId == null) {
			if (esClient.version().supportsPIT())
				q.startPit(keepAlive);
			else
				q.startScroll(keepAlive);
		}
		return new PaginatedSearch(q, fetchTimeout);
	}

	@Override
	protected SearchRequest clone() {
		return new SearchRequest(esClient, this);
	}

	public SearchRequest sort(String... order) {
		sort = new ArrayList<>(order.length + 1);
		for (final String key : order)
			sort.add(SortKey.valueOf(key));
		return this;
	}

	public SearchRequest sort(Collection<SortKey> order) {
		sort = new ArrayList<>(order);
		return this;
	}

	/**
	 * Request a scroll ID with this search. Remember to terminate scroll IDs after
	 * use.
	 */
	SearchRequest startScroll(Duration keepAlive) {
		this.scrollId = SCROLL_REQUEST;
		this.keepAlive = keepAlive;
		return this;
	}

	/**
	 * Continue a previous search based on scroll ID. Note that all other search
	 * parameters are ignored because they are already persisted into the scrolled
	 * search.
	 */
	public SearchRequest continueScroll(String scrollId, Duration keepAlive) {
		this.scrollId = scrollId;
		this.keepAlive = keepAlive;
		return this;
	}

	/**
	 * Request a point-in-time token for this search. Remember to terminate pid_ids
	 * after use.
	 */
	SearchRequest startPit(Duration keepAlive) {
		this.pitId = SCROLL_REQUEST;
		this.keepAlive = keepAlive;
		return this;
	}

	/**
	 * Associate this search with an existing PIT (point-in-time) token. Note that
	 * the search index is fixed for a PIT token and cannot be changed. Also, if PIT
	 * is used for paginated search, better use
	 * {@link #continueSearch(ESSearchResult, Duration)} or .
	 */
	public SearchRequest pit(String pit, Duration keepAlive) {
		this.pitId = pit;
		this.keepAlive = keepAlive;
		return this;
	}

	public class ESSearchResult extends BaseResult {

		private final List<ESSearchHit> hits;

		public ESSearchResult(ObjectNode json) {
			super(json);
			// TODO: fail on timeout or _shards.failed > 0
			final JsonNode hitsNode = json.get("hits");
			hits = new ArrayList<>(hitsNode.get("hits").size());
			for (final JsonNode hit : hitsNode.get("hits")) {
				// TODO: ensure that _index and _type are as expected
				hits.add(new ESSearchHit((ObjectNode) hit));
			}
		}

		public long getTotal() {
			// total is a number in ES7, but an object in ES8
			var totalHits = getResult().get("hits").get("total");
			return totalHits.isNumber() ? totalHits.asLong() : totalHits.path("value").asLong();
		}

		public List<ESSearchHit> hits() {
			return hits;
		}

		public boolean isLastPage() {
			return hits.size() < getLimit();
		}

		public String getScrollId() {
			return getResult().path("_scroll_id").textValue();
		}

		public String getPitId() {
			return getResult().path("pid_id").textValue();
		}

		public ArrayNode getLastSortValues() {
			return hits.get(hits.size() - 1).getSortValues();
		}

		public int size() {
			return hits.size();
		}
	}

	public class ESSearchHit extends ESDocument {

		public ESSearchHit(ObjectNode doc) {
			super(doc);
		}

		public double getScore() {
			return getResult().path("_score").asDouble();
		}

		public ArrayNode getSortValues() {
			return getResult().withArray("sort");
		}
	}

}