package de.gwdg.cdstar.ext.elastic;

import com.fasterxml.jackson.databind.node.ObjectNode;

import de.gwdg.cdstar.Promise;
import de.gwdg.cdstar.ext.elastic.DeleteIndexRequest.DeleteIndexResult;
import de.gwdg.cdstar.ext.elastic.ElasticSearchClient.BaseResult;

public class DeleteIndexRequest implements ElasticSearchClient.ESPreparedRequest<DeleteIndexResult> {

	private final ElasticSearchClient esClient;
	private final String index;

	public DeleteIndexRequest(ElasticSearchClient elasticSearchClient, String index) {
		esClient = elasticSearchClient;
		this.index = index;
	}

	@Override
	public Promise<DeleteIndexResult> submit() {
		return esClient.send(esClient.buildRequest(index).DELETE()).map(DeleteIndexResult::new);
	}

	public class DeleteIndexResult extends BaseResult {

		public DeleteIndexResult(ObjectNode answer) {
			super(answer);
		}
	}

}
