package de.gwdg.cdstar.ext.elastic;

import de.gwdg.cdstar.Promise;

public class FetchRequest implements ElasticSearchClient.ESPreparedRequest<ESDocument> {
	private final ElasticSearchClient esClient;
	private final String index;
	private final String id;

	FetchRequest(ElasticSearchClient elasticSearchClient, String index, String id) {
		esClient = elasticSearchClient;
		this.index = index;
		this.id = id;
	}

	@Override
	public Promise<ESDocument> submit() {
		var rq = esClient.buildRequest(index, "_doc", id);
		return esClient.send(rq).map(r -> new ESDocument(r));
	}
}