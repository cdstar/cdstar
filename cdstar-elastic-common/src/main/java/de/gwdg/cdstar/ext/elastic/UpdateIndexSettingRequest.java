package de.gwdg.cdstar.ext.elastic;

import com.fasterxml.jackson.databind.node.ObjectNode;

import de.gwdg.cdstar.Promise;
import de.gwdg.cdstar.ext.elastic.ElasticSearchClient.BaseResult;
import de.gwdg.cdstar.ext.elastic.UpdateIndexSettingRequest.UpdateIndexSettingResult;

public class UpdateIndexSettingRequest implements ElasticSearchClient.ESPreparedRequest<UpdateIndexSettingResult> {

	private final ElasticSearchClient esClient;
	private final String index;
	private final ObjectNode payload;

	public UpdateIndexSettingRequest(ElasticSearchClient elasticSearchClient, String index) {
		esClient = elasticSearchClient;
		this.index = index;
		payload = esClient.json();
	}

	@Override
	public Promise<UpdateIndexSettingResult> submit() {
		return esClient.put(esClient.buildRequest(index, "_settings"), payload)
			.map(r -> new UpdateIndexSettingResult(r));
	}

	public ObjectNode settings() {
		return payload;
	}

	public UpdateIndexSettingRequest settings(ObjectNode newSettings) {
		payload.setAll(newSettings);
		return this;
	}

	public class UpdateIndexSettingResult extends BaseResult {
		public UpdateIndexSettingResult(ObjectNode r) {
			super(r);
		}
	}

}
