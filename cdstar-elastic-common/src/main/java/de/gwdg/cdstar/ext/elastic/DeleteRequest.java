package de.gwdg.cdstar.ext.elastic;

import com.fasterxml.jackson.databind.node.ObjectNode;

import de.gwdg.cdstar.Promise;
import de.gwdg.cdstar.ext.elastic.DeleteRequest.DeleteResult;
import de.gwdg.cdstar.ext.elastic.ElasticSearchClient.BaseResult;

public class DeleteRequest implements ElasticSearchClient.ESPreparedRequest<DeleteResult> {

	private final ElasticSearchClient esClient;
	private final String index;
	private final String id;

	DeleteRequest(ElasticSearchClient elasticSearchClient, String index, String id) {
		esClient = elasticSearchClient;
		this.index = index;
		this.id = id;
	}

	@Override
	public Promise<DeleteResult> submit() {
		return esClient
			.send(
				(esClient.version().needsDoc()
					? esClient.buildRequest(index, ElasticSearchClient.DEFAULT_TYPE_NAME, id)
					: esClient.buildRequest(index, id)).DELETE())
			.map(r -> new DeleteResult(r));
	}

	public class DeleteResult extends BaseResult {

		public DeleteResult(ObjectNode answer) {
			super(answer);
			// TODO Auto-generated constructor stub
		}

	}

}
