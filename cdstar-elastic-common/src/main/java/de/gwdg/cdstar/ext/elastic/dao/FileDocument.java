package de.gwdg.cdstar.ext.elastic.dao;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FileDocument extends IndexDocument {
	public static final String SCOPE_NAME = "file";

	public static final String FID_FIELD = "fid";
	public static final String NAME_FIELD = "name";
	public static final String FILENAME_FIELD = "filename";
	public static final String TYPE_FIELD = "type";
	public static final String SHA256_FIELD = "sha256";
	public static final String CONTENT_FIELD = "content";

	@JsonProperty(FID_FIELD)
	public String fid;
	@JsonProperty(NAME_FIELD)
	public String name;
	@JsonProperty(FILENAME_FIELD)
	public String filename;
	@JsonProperty(TYPE_FIELD)
	public String type;
	@JsonProperty(SHA256_FIELD)
	public String sha256;
	@JsonProperty(CONTENT_FIELD)
	public String content;

	@Override
	public String getIndexID() {
		return fullId(id, fid);
	}

	public static String fullId(String archiveId, String fileId) {
		return archiveId + "_" + fileId;
	}

}
