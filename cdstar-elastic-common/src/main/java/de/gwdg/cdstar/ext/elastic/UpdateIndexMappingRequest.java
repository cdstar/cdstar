package de.gwdg.cdstar.ext.elastic;

import com.fasterxml.jackson.databind.node.ObjectNode;

import de.gwdg.cdstar.Promise;
import de.gwdg.cdstar.ext.elastic.ElasticSearchClient.BaseResult;
import de.gwdg.cdstar.ext.elastic.UpdateIndexMappingRequest.UpdateIndexMappingResult;

public class UpdateIndexMappingRequest implements ElasticSearchClient.ESPreparedRequest<UpdateIndexMappingResult> {

	private final ElasticSearchClient esClient;
	private final String index;
	private final ObjectNode payload;

	public UpdateIndexMappingRequest(ElasticSearchClient elasticSearchClient, String index) {
		esClient = elasticSearchClient;
		this.index = index;
		payload = esClient.json();
	}

	@Override
	public Promise<UpdateIndexMappingResult> submit() {
		return esClient.put(esClient.buildRequest(index, "_mapping"), payload).map(r -> new UpdateIndexMappingResult(r));
	}

	public ObjectNode mapping() {
		return esClient.version().needsDoc()
			? payload.with(ElasticSearchClient.DEFAULT_TYPE_NAME)
			: payload;
	}

	public class UpdateIndexMappingResult extends BaseResult {
		public UpdateIndexMappingResult(ObjectNode r) {
			super(r);
		}
	}

}
