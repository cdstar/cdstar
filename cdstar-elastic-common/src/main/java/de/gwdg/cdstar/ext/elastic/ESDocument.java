package de.gwdg.cdstar.ext.elastic;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.node.ObjectNode;

import de.gwdg.cdstar.ext.elastic.ElasticSearchClient.BaseResult;

public class ESDocument extends BaseResult {

	public ESDocument(ObjectNode doc) {
		super(doc);
	}

	public ObjectNode source() {
		return getResult().with("_source");
	}

	public <T> T source(Class<T> type) throws JsonProcessingException {
		return ElasticSearchClient.om.treeToValue(source(), type);
	}

	public String getId() {
		return getResult().get("_id").asText();
	}
}