package de.gwdg.cdstar.ext.elastic;

import com.fasterxml.jackson.databind.node.ObjectNode;

import de.gwdg.cdstar.Promise;
import de.gwdg.cdstar.ext.elastic.CreateIndexRequest.CereateIndexResult;
import de.gwdg.cdstar.ext.elastic.ElasticSearchClient.BaseResult;

public class CreateIndexRequest implements ElasticSearchClient.ESPreparedRequest<CereateIndexResult> {

	private final ElasticSearchClient esClient;
	private final String index;
	private final ObjectNode payload;

	public CreateIndexRequest(ElasticSearchClient elasticSearchClient, String index) {
		esClient = elasticSearchClient;
		this.index = index;
		payload = esClient.json();
	}

	@Override
	public Promise<CereateIndexResult> submit() {
		return esClient.put(esClient.buildRequest(index), payload).map(r -> new CereateIndexResult(r));
	}

	public class CereateIndexResult extends BaseResult {
		public CereateIndexResult(ObjectNode r) {
			super(r);
		}
	}

	public CreateIndexRequest mappings(ObjectNode mappings) {
		mappings().setAll(mappings);
		return this;
	}

	public ObjectNode mappings() {
		return esClient.version().needsDoc()
			? payload.with("mappings").with(ElasticSearchClient.DEFAULT_TYPE_NAME)
			: payload.with("mappings");
	}

	public ObjectNode settings() {
		return payload.with("settings");
	}

	public CreateIndexRequest settings(ObjectNode settings) {
		settings().setAll(settings);
		return this;
	}

}
