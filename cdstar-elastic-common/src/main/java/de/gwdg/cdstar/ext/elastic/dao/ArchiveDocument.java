package de.gwdg.cdstar.ext.elastic.dao;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ArchiveDocument extends IndexDocument {

	public static final String REV_FIELD = "revision";
	public static final String PROFILE_FIELD = "profile";
	public static final String SCOPE_NAME = "archive";

	@JsonProperty(REV_FIELD)
	public String revision;

	@JsonProperty(PROFILE_FIELD)
	public String profile;

	@Override
	public String getIndexID() {
		return id;
	}
}
