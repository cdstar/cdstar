package de.gwdg.cdstar.ext.elastic;

import java.net.http.HttpRequest;
import java.util.concurrent.TimeUnit;

import com.fasterxml.jackson.databind.node.ObjectNode;

import de.gwdg.cdstar.Promise;
import de.gwdg.cdstar.ext.elastic.ElasticSearchClient.BaseResult;
import de.gwdg.cdstar.ext.elastic.HealthRequest.HealthResult;

public class HealthRequest implements ElasticSearchClient.ESPreparedRequest<HealthResult> {

	private final ElasticSearchClient esClient;
	private HealthStatus waitForStatus;
	private long timeout;

	HealthRequest(ElasticSearchClient elasticSearchClient) {
		esClient = elasticSearchClient;
	}

	public HealthRequest waitForStatus(HealthStatus status, int timeout, TimeUnit unit) {
		waitForStatus = status;
		this.timeout = unit == null || timeout < 0 ? -1 : unit.convert(timeout, TimeUnit.MILLISECONDS);
		return this;
	}

	public HealthRequest waitForStatus(HealthStatus status) {
		return waitForStatus(status, -1, null);
	}

	@Override
	public Promise<HealthResult> submit() {
		var ub = esClient.buildUri("cluster", "health");
		if (waitForStatus != null)
			ub.query("wait_for_status", waitForStatus.name().toLowerCase());
		if (timeout >= 0)
			ub.query("timeout", Long.toString(timeout) + "ms");
		var rq = ub.map(HttpRequest::newBuilder);
		return esClient.send(rq).map(r -> new HealthResult(r));
	}

	public enum HealthStatus {
		RED, YELLOW, GREEN
	}

	public class HealthResult extends BaseResult {

		public HealthResult(ObjectNode r) {
			super(r);
		}

	}

}
