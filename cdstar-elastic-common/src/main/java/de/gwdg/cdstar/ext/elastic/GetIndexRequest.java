package de.gwdg.cdstar.ext.elastic;

import com.fasterxml.jackson.databind.node.ObjectNode;

import de.gwdg.cdstar.Promise;
import de.gwdg.cdstar.ext.elastic.ElasticSearchClient.BaseResult;
import de.gwdg.cdstar.ext.elastic.GetIndexRequest.GetIndexResult;

public class GetIndexRequest implements ElasticSearchClient.ESPreparedRequest<GetIndexResult> {

	private final ElasticSearchClient esClient;
	private final String index;

	public GetIndexRequest(ElasticSearchClient elasticSearchClient, String index) {
		esClient = elasticSearchClient;
		this.index = index;
	}

	@Override
	public Promise<GetIndexResult> submit() {
		return esClient.get(esClient.buildRequest(index), null).map(r -> new GetIndexResult(r));
	}

	public class GetIndexResult extends BaseResult {
		public GetIndexResult(ObjectNode answer) {
			super(answer);
		}
	}

}
