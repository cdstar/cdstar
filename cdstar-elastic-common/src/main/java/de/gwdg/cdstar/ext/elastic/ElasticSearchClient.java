package de.gwdg.cdstar.ext.elastic;

import java.io.Closeable;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpClient.Version;
import java.net.http.HttpRequest;
import java.net.http.HttpRequest.BodyPublishers;
import java.net.http.HttpRequest.Builder;
import java.net.http.HttpResponse.BodyHandlers;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CancellationException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.node.ObjectNode;

import de.gwdg.cdstar.Promise;
import de.gwdg.cdstar.UriBuilder;
import de.gwdg.cdstar.Utils;

/**
 * A basic elasticsearch REST client based on Apache {@link HttpClient}, cdstar
 * {@link Promise}s and jackson (for json processing). The elasticsearch java
 * libraries are not used.
 *
 * This implementation aims to be more usable and convenient than the official
 * low-level client, but less dependant on the whole elasticsearch stack and
 * more stable than the official high-level client. Only the most commonly used
 * parts of the ES API are implemented, but direct API access is allowed, so any
 * missing parts can be worked around by manually building the REST calls.
 */

public class ElasticSearchClient implements Closeable {
	public static ObjectMapper om = new ObjectMapper();
	static final Logger log = LoggerFactory.getLogger(ElasticSearchClient.class);
	public static final String DEFAULT_TYPE_NAME = "doc";
	private static final byte[] NL = new byte[] { '\n' };

	Set<Promise<?>> inFlight = ConcurrentHashMap.newKeySet();

	static {
		// These are the default, but better be save than sorry. The bulk api
		// breaks if a document spans multiple lines, and we assume date fields
		// to be long values in multiple places.
		om.disable(SerializationFeature.INDENT_OUTPUT);
		om.enable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);

		// ES expects milliseconds, not nanoseconds.
		om.disable(SerializationFeature.WRITE_DATE_TIMESTAMPS_AS_NANOSECONDS);
	}

	final URI baseUri;
	private HttpClient http;
	private ESVersion version;

	public ElasticSearchClient(URI clusterUri) {
		this.baseUri = clusterUri;
		this.version = ESVersion.AUTODETECT;

		http = HttpClient.newBuilder()
				.version(Version.HTTP_1_1)
				.connectTimeout(Duration.ofSeconds(60))
				.build();
	}

	/**
	 * If version is still {@link ESVersion#AUTODETECT}, then try to figure out
	 * version now. Otherwise, return whatever was detected previously or configured
	 * explicitly.
	 *
	 * @throws InterruptedException
	 * @throws ExecutionException
	 * @throws CancellationException
	 *
	 */
	public ESVersion resolveVersion() {
		if (version != ESVersion.AUTODETECT)
			return version;

		ObjectNode clusterInfo;
		try {
			clusterInfo = get(buildRequest(), null).get();
		} catch (CancellationException | ExecutionException | InterruptedException e) {
			throw new RuntimeException("Unable to detect ES cluister version", e);
		}

		var dist = clusterInfo.path("version").path("distribution").textValue();
		var num = clusterInfo.path("version").path("number").textValue();

		if ("opensearch".equals(dist)) {
			if (num.startsWith("1."))
				version = ESVersion.OS1;
		} else {
			if (num.startsWith("6."))
				version = ESVersion.ES6;
			else if (num.startsWith("7."))
				version = ESVersion.ES7;
			else if (num.startsWith("8."))
				version = ESVersion.ES8;
		}
		if (version == ESVersion.AUTODETECT)
			throw new RuntimeException("Unsupported ES version: " + num + "(" + dist + ")");
		return version;
	}

	@Override
	public void close() {
		inFlight.forEach(Promise::cancel);
	}

	Promise<ObjectNode> sendBulk(HttpRequest.Builder rq, Collection<Object> bodyParts) {
		rq.setHeader("Content-Type", "application/x-ndjson");
		var lines = new ArrayList<byte[]>(bodyParts.size() * 2);
		try {
			for (var part : bodyParts) {
				lines.add(om.writeValueAsBytes(part));
				lines.add(NL);
			}
		} catch (JsonProcessingException e) {
			throw new RuntimeException(e);
		}
		rq.POST(BodyPublishers.ofByteArrays(lines));
		return send(rq);
	}

	Promise<ObjectNode> sendMethod(String method, HttpRequest.Builder rq, Object body) {
		try {
			if (body != null) {
				rq.setHeader("Content-Type", "application/json");
				rq.method(method, BodyPublishers.ofByteArray(om.writeValueAsBytes(body)));
			} else {
				rq.method(method, BodyPublishers.noBody());
			}
			return send(rq);
		} catch (final Exception e) {
			return Promise.ofError(e);
		}
	}

	Promise<ObjectNode> get(HttpRequest.Builder rq, Object body) {
		return sendMethod("GET", rq, body);
	}

	Promise<ObjectNode> post(HttpRequest.Builder rq, Object body) {
		return sendMethod("POST", rq, body);
	}

	Promise<ObjectNode> put(HttpRequest.Builder rq, Object body) {
		return sendMethod("PUT", rq, body);
	}

	public UriBuilder buildUri(String... path) {
		return new UriBuilder(baseUri).path(path);
	}

	public Builder buildRequest(String... parts) {
		return buildUri(parts).map(HttpRequest::newBuilder);
	}

	public Builder buildRequest(Map<String, String> params, String... parts) {
		var urib = buildUri(parts);
		params.forEach(urib::query);
		return urib.map(HttpRequest::newBuilder);
	}

	/**
	 * Return an cancelable promise for the response.
	 */
	Promise<ObjectNode> send(HttpRequest.Builder rq) {
		var rqb = rq.build();
		var p = Promise
				.wrap(http.sendAsync(rqb, BodyHandlers.ofInputStream()))
				.map(rs -> {
					var json = om.readValue(rs.body(), ObjectNode.class);
					if (rs.statusCode() > 299)
						throw new ESErrorResponse(rs, json);
					return json;
				})
				.then(
						r -> log.debug("ES Result: {}", r),
						e -> log.warn("Search error:", e));
		inFlight.add(p);
		p.then((v, e) -> {
			inFlight.remove(p);
		});

		return p;
	}

	public SearchRequest search(String index) {
		return new SearchRequest(this, index);
	}

	public IndexRequest index(String index, String id, Object document) {
		return new IndexRequest(this, index, id, document);
	}

	public FetchRequest fetch(String index, String id) {
		return new FetchRequest(this, index, id);
	}

	public DeleteRequest delete(String index, String id) {
		return new DeleteRequest(this, index, id);
	}

	public DeleteByQueryRequest deleteByQuery(String index) {
		return new DeleteByQueryRequest(this, index);
	}

	public RefreshRequest refresh(String index) {
		return new RefreshRequest(this, index);
	}

	public CreateIndexRequest createIndex(String index) {
		return new CreateIndexRequest(this, index);
	}

	public GetIndexRequest getIndex(String index) {
		return new GetIndexRequest(this, index);
	}

	public UpdateIndexSettingRequest updateIndexSettings(String index) {
		return new UpdateIndexSettingRequest(this, index);
	}

	public UpdateIndexMappingRequest updateIndexMapping(String index) {
		return new UpdateIndexMappingRequest(this, index);
	}

	public DeleteIndexRequest deleteIndex(String index) {
		return new DeleteIndexRequest(this, index);
	}

	public BulkRequest bulk() {
		return new BulkRequest(this);
	}

	ObjectNode json() {
		return om.createObjectNode();
	}

	String json2string(JsonNode node) {
		try {
			return om.writeValueAsString(node);
		} catch (final JsonProcessingException e) {
			throw Utils.wtf("Failed to turn a JsonNode into json.", e);
		}
	}

	public interface ESPreparedRequest<T extends BaseResult> {
		Promise<T> submit();

		default CompletableFuture<T> submitFuture() {
			return submit().toCompletableFuture();
		}

		default T execute() throws CancellationException, InterruptedException, ESErrorResponse {
			try {
				return submit().get();
			} catch (ExecutionException e) {
				var cause = e.getCause();
				if (cause instanceof ESErrorResponse)
					throw (ESErrorResponse) cause;
				if (cause instanceof RuntimeException)
					throw (RuntimeException) cause;
				throw new RuntimeException(cause);
			}
		}

		default T execute(long timeout, TimeUnit unit)
				throws CancellationException, InterruptedException, TimeoutException, ESErrorResponse {
			try {
				return submit().get(timeout, unit);
			} catch (ExecutionException e) {
				var cause = e.getCause();
				if (cause instanceof ESErrorResponse)
					throw (ESErrorResponse) cause;
				if (cause instanceof RuntimeException)
					throw (RuntimeException) cause;
				throw new RuntimeException(cause);
			}
		}

	}

	public static abstract class BaseResult {
		private ObjectNode answer;

		public BaseResult(ObjectNode answer) {
			this.answer = answer;
		}

		public ObjectNode getResult() {
			return answer;
		}

		@Override
		public String toString() {
			return answer.toString();
		}
	}

	public static enum ESVersion {
		AUTODETECT, ES6, ES7, ES8, OS1;

		public boolean needsDoc() {
			return this == ES6;
		}

		boolean supportsPIT() {
			return this == ES8;
		}

		boolean supportsRuntimeFields() {
			return EnumSet.of(ES7, ES8).contains(this);
		}
	}

	public Promise<String> waitForYellow() {
		var rq = buildUri("_cluster", "health").query("wait_for_status", "yellow").map(HttpRequest::newBuilder);
		return send(rq).map(r -> r.path("status").asText("unknown"));
	}

	public ESVersion version() {
		if (version == ESVersion.AUTODETECT)
			throw new IllegalStateException("ES Version not known. Resolve version first.");
		return version;
	}

}
