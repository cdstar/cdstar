package de.gwdg.cdstar.ext.elastic;

import java.util.Objects;

public class SortKey {
	// TODO: Move this into the search query API. Can be useful for other
	// provider backends, too.

	private final String field;
	private final boolean reversed;

	public SortKey(String field, boolean reversed) {
		this.field = field;
		this.reversed = reversed;
	}

	public String getField() {
		return field;
	}

	public boolean isReversed() {
		return reversed;
	}

	@Override
	public String toString() {
		return reversed ? "desc(" + field + ")" : field;
	}

	/**
	 * Parse a string in the form "field", "+field", "-field", "field ASC",
	 * "field DESC" and return a {@link #SortKey}.
	 */
	public static SortKey valueOf(String str) {
		Objects.requireNonNull(str);
		if (str.startsWith("-"))
			return new SortKey(str.substring(1), true);
		if (str.startsWith("+"))
			return new SortKey(str.substring(1), false);
		if (str.endsWith(" DESC"))
			return new SortKey(str.substring(0, str.length() - 5), true);
		if (str.endsWith(" ASC"))
			return new SortKey(str.substring(0, str.length() - 4), false);
		return new SortKey(str, false);
	}

}
