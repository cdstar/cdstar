package de.gwdg.cdstar.ext.elastic;

import com.fasterxml.jackson.databind.node.ObjectNode;

import de.gwdg.cdstar.Promise;
import de.gwdg.cdstar.ext.elastic.ElasticSearchClient.BaseResult;
import de.gwdg.cdstar.ext.elastic.RefreshRequest.RefreshResult;

public class RefreshRequest implements ElasticSearchClient.ESPreparedRequest<RefreshResult> {

	private final ElasticSearchClient esClient;
	private final String index;

	RefreshRequest(ElasticSearchClient elasticSearchClient, String index) {
		esClient = elasticSearchClient;
		this.index = index;
	}

	@Override
	public Promise<RefreshResult> submit() {
		var rq = esClient.buildRequest(index, "_refresh");
		return esClient.send(rq).map(r -> new RefreshResult(r));
	}

	public class RefreshResult extends BaseResult {

		public RefreshResult(ObjectNode r) {
			super(r);
		}

	}

}
