package de.gwdg.cdstar.web.common.model;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({ "vault", "id", "revision" })
public class ArchiveCreated {

	public String vault;
	public String id;
	public String revision;

	public ArchiveCreated() {
	}

	public ArchiveCreated(String vault, String id, String revision) {
		this.vault = vault;
		this.id = id;
		this.revision = revision;
	}

}
