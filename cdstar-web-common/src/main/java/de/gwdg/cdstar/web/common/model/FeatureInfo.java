package de.gwdg.cdstar.web.common.model;

import java.util.Map;

public class FeatureInfo {
	public String name;
	public String version;
	public Map<String, String> info;
}
