package de.gwdg.cdstar.web.common.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({ "id", "name", "type", "size", "created", "modified", "digests", "meta" })
public class FileInfo {
	public String id;
	public String name;
	public String type;
	public long size;
	public Date created;
	public Date modified;
	public DigestInfo digests;

	@JsonInclude(value = Include.NON_NULL)
	public MetaMap meta;
}
