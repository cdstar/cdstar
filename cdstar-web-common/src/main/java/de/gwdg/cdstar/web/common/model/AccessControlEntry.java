package de.gwdg.cdstar.web.common.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({ "subject", "permissions" })
public class AccessControlEntry {
	public String subject;
	public List<String> permissions;
}
