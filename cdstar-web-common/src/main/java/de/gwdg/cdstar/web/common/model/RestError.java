package de.gwdg.cdstar.web.common.model;

public class RestError extends RuntimeException {

	private static final long serialVersionUID = -5362095739911045510L;
	private final int status;

	public RestError(int status, String message) {
		super(message);
		this.status = status;
	}

	public int getStatus() {
		return status;
	}
}
