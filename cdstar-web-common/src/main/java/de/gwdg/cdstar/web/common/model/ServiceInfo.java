package de.gwdg.cdstar.web.common.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ServiceInfo {
	public VersionInfo version = new VersionInfo();
	public List<String> vaults = new ArrayList<>();

	@JsonInclude(value = Include.NON_EMPTY)
	@JsonProperty("features")
	public Map<String, Map<String, String>> feature;

}
