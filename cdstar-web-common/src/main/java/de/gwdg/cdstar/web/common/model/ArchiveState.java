package de.gwdg.cdstar.web.common.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum ArchiveState {
	// TODO: PENDING_RECOVER makes no sense if an archive is moved from local to
	// mirrored mode. Rework these states.

	OPEN, LOCKED, ARCHIVED, PENDING_ARCHIVE, PENDING_RECOVER;

	private final String jsonName;

	private ArchiveState() {
		jsonName = name().toLowerCase().replace('_', '-');
	}

	@JsonCreator
	public static ArchiveState fromString(String name) {
		for (final ArchiveState value : ArchiveState.values())
			if (value.jsonName.equals(name))
				return value;
		throw new IllegalArgumentException("Unsupported archive state: " + name);
	}

	@JsonValue
	public String getJsonName() {
		return jsonName;
	}

	public boolean isReadable() {
		return this == ArchiveState.OPEN || this == ArchiveState.LOCKED;
	}

	public boolean isWriteable() {
		return this == ArchiveState.OPEN;
	}

}