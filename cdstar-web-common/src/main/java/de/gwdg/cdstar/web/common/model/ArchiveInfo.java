package de.gwdg.cdstar.web.common.model;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder(value = { "id", "vault", "revision", "profile", "state", "created", "modified", "file_count",
		"files", "meta", "acl" }, alphabetic = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ArchiveInfo {

	public String id;
	public String vault;
	public String revision;

	public String profile;
	public ArchiveState state;

	public Date created;
	public Date modified;
	public long file_count;
	public List<FileInfo> files;
	public MetaMap meta;
	public String owner;
	public AclMap acl;
	public List<SnapshotInfo> snapshots;
}
