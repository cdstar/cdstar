package de.gwdg.cdstar.web.common.model;

import java.lang.management.ManagementFactory;
import java.time.Instant;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import de.gwdg.cdstar.GitInfo;

public class VersionInfo {
	public String cdstar;
	public String api;
	public String java;

	@JsonInclude(value = Include.NON_NULL)
	public SourceInfo source;


	public static class SourceInfo {
		public String branch;
		public String commit;
		public Instant date;

		public SourceInfo() {
			if (GitInfo.isAvailable()) {
				GitInfo git = GitInfo.getInstance();
				branch = git.getBranch();
				commit = git.getCommit() + (git.isDirty() ? "-dirty" : "");
				date = git.getCommitTime();
			}
		}
	}

	public VersionInfo() {
		java = ManagementFactory.getRuntimeMXBean().getVmVersion();
		api = "3.1";
		cdstar = "unknown";
		if (GitInfo.isAvailable()) {
			cdstar = GitInfo.getInstance().getVersion();
		}
	}
}
