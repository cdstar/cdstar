package de.gwdg.cdstar.web.common.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class VaultInfo {

	@JsonProperty("public")
	public boolean isPublic;

	@JsonCreator
	public VaultInfo(@JsonProperty("public") boolean isPublic) {
		this.isPublic = isPublic;
	}

}
