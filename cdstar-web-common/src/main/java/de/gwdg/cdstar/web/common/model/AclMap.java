package de.gwdg.cdstar.web.common.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;

public class AclMap {

	Map<String, List<String>> acl = new HashMap<>();

	public AclMap with(String subject, String... permissions) {
		acl.put(subject, new ArrayList<>(Arrays.asList(permissions)));
		return this;
	}

	@JsonAnySetter
	public void put(String key, List<String> values) {
		acl.put(key, values);
	}

	@JsonAnyGetter
	public Map<String, List<String>> getMap() {
		return acl;
	}

}
