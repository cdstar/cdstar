package de.gwdg.cdstar.web.common.model;

import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;

import de.gwdg.cdstar.web.common.model.SearchHits.Hit;

public class SearchHits extends AbstractList<Hit> {
	private String scroll;

	public SearchHits() {
		super();
	}

	public SearchHits(List<Hit> items, long total, String scroll) {
		super(items, total);
		this.setScroll(scroll);
	}

	@Override
	@JsonProperty("hits")
	public List<Hit> getItems() {
		return super.getItems();
	}

	public String getScroll() {
		return scroll;
	}

	public void setScroll(String scroll) {
		this.scroll = scroll;
	}

	public static class Hit {
		public String id;
		public double score;
		public String type;
		@JsonInclude(Include.NON_NULL)
		public String name;
		public Map<String, JsonNode> fields;
	}

}
