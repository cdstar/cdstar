package de.gwdg.cdstar.web.common.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({ "limit", "results" })
public class ScrollResult {

	public int limit = 25;
	public List<String> results;
	public String scroll;

	public ScrollResult() {
	}

	public ScrollResult(int limit, List<String> result) {
		this.limit = limit;
		results = result;
		if (result.size() >= limit)
			scroll = results.get(results.size() - 1);
	}

	@JsonProperty(access = JsonProperty.Access.READ_ONLY)
	public int getCount() {
		return results.size();
	}

}
