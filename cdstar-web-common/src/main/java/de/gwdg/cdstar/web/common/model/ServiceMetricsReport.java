package de.gwdg.cdstar.web.common.model;

import java.util.Map;
import java.util.TreeMap;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;

public class ServiceMetricsReport {
	private final Map<String, Object> metrics = new TreeMap<>();

	@JsonAnyGetter
	Map<String, Object> getMetrics() {
		return metrics;
	}

	@JsonAnySetter
	public void setMetric(String name, Object value) {
		metrics.put(name, value);
	}
}
