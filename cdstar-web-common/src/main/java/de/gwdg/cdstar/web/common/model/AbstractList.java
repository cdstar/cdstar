package de.gwdg.cdstar.web.common.model;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * Abstract base class for list-like response documents with count and total.
 * Implementors may rename the items
 */
@JsonPropertyOrder({ "total", "count" })
@JsonIgnoreProperties(value = { "count" }, allowGetters = true)
public class AbstractList<T> implements Iterable<T> {
	private List<T> items;
	private long total = 0;

	public AbstractList() {
	}

	public AbstractList(List<T> items, long total) {
		this.items = items;
		this.total = total;
	}

	public List<T> getItems() {
		return items;
	}

	public void setItems(List<T> items) {
		this.items = items;
	}

	public long getTotal() {
		return total;
	}

	public void setTotal(long total) {
		this.total = total;
	}

	@Override
	public Iterator<T> iterator() {
		return items != null ? items.iterator() : Collections.emptyIterator();
	}

	@JsonProperty(value = "count", access = JsonProperty.Access.READ_ONLY)
	public int getCount() {
		return items != null ? items.size() : 0;
	}
}
