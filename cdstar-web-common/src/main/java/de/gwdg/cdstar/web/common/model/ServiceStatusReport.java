package de.gwdg.cdstar.web.common.model;

import java.util.Map;
import java.util.TreeMap;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

public class ServiceStatusReport {
	public String status;

	/**
	 * A map of detailed health check informations. Only defined if the user has
	 * permissions.
	 */
	@JsonInclude(value = Include.NON_EMPTY)
	public Map<String, String> checks = new TreeMap<>();
}
