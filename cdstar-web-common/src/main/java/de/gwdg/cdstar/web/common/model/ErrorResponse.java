package de.gwdg.cdstar.web.common.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.web.common.HttpStatus;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonAutoDetect(getterVisibility = Visibility.NONE, isGetterVisibility = Visibility.NONE)
public class ErrorResponse extends RestError {
	private static final Logger log = LoggerFactory.getLogger(ErrorResponse.class);

	private static final long serialVersionUID = -3869202589227193534L;
	private final String error;
	private final String message;
	private final Map<String, Object> detail = new HashMap<>();
	private final List<ErrorResponse> more = new ArrayList<>(0);

	@JsonCreator
	public ErrorResponse(@JsonProperty("status") int status, @JsonProperty("error") String error,
			@JsonProperty("message") String message, @JsonProperty("detail") Map<String, ? extends Object> detail,
			@JsonProperty("other") List<ErrorResponse> more) {
		super(status, error + ": " + message);
		if (log.isDebugEnabled() && error.contains(" "))
			log.debug("Error response identifier should be CamelCase: {}", error, new RuntimeException());
		this.error = error;
		this.message = message;
		if (detail != null)
			this.detail.putAll(detail);
		if (more != null)
			this.more.addAll(more);
	}

	@Deprecated
	public ErrorResponse(String message) {
		this(500, message);
	}

	@Deprecated
	public ErrorResponse(int code) {
		this(code, HttpStatus.valueOf(code).getReasonPhrase());
	}

	@Deprecated
	public ErrorResponse(int code, String message) {
		this(code, HttpStatus.valueOf(code).getReasonPhrase(), message);
	}

	public ErrorResponse(int status, String error, String message) {
		this(status, error, message, null, null);
	}

	public ErrorResponse(Exception e) {
		this(500, "InternalServerError", "Internal Server Error");
		cause(e);
	}

	@JsonProperty
	public String getError() {
		return error;
	}

	@JsonProperty("message")
	public String getMsg() {
		return message;
	}

	@Override
	@JsonProperty("status")
	public int getStatus() {
		return super.getStatus();
	}

	@JsonProperty
	@JsonInclude(value = Include.NON_EMPTY, content = Include.NON_NULL)
	public List<ErrorResponse> getOther() {
		return more;
	}

	@JsonProperty
	@JsonInclude(value = Include.NON_EMPTY, content = Include.NON_NULL)
	public Map<String, Object> getDetail() {
		return detail;
	}

	public ErrorResponse detail(String key, Object value) {
		detail.put(key, value);
		return this;
	}

	public ErrorResponse attach(ErrorResponse other) {
		more.add(other);
		return this;
	}

	public ErrorResponse cause(Throwable e) {
		initCause(e);
		return this;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder();
		sb.append("[").append(getStatus()).append("] ");
		sb.append(error).append(": ").append(detail);
		return sb.toString();
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof ErrorResponse))
			return false;
		final ErrorResponse other = (ErrorResponse) obj;
		return other.getStatus() == getStatus() && Utils.equal(other.error, error)
				&& Utils.equal(other.message, message) && Utils.equal(other.detail, detail);
	}

}
