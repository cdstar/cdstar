package de.gwdg.cdstar.web.common.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_EMPTY)
public class DigestInfo {

	public DigestInfo() {
	}

	public String md5;
	public String sha1;
	public String sha256;

}
