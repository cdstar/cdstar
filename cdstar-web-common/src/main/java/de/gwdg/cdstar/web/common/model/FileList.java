package de.gwdg.cdstar.web.common.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FileList extends AbstractList<FileInfo> {

	public FileList() {
		super();
	}

	public FileList(List<FileInfo> items, int total) {
		super(items, total);
	}

	@Override
	@JsonProperty("files")
	public List<FileInfo> getItems() {
		return super.getItems();
	}

}
