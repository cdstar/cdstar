package de.gwdg.cdstar.web.common.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SnapshotList extends AbstractList<SnapshotInfo> {
	public SnapshotList() {
		super();
	}

	public SnapshotList(List<SnapshotInfo> items, int total) {
		super(items, total);
	}

	@Override
	@JsonProperty("snapshots")
	public List<SnapshotInfo> getItems() {
		return super.getItems();
	}

}
