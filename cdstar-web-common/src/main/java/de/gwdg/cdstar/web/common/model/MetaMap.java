package de.gwdg.cdstar.web.common.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonFormat;

public class MetaMap {

	Map<String, List<String>> meta = new HashMap<>();

	@JsonAnySetter
	@JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
	public void put(String key, List<String> values) {
		if (values == null || values.isEmpty())
			return;
		final List<String> copy = new ArrayList<>(values);
		copy.removeIf(Objects::isNull);
		meta.put(key, copy);
	}

	public void add(String key, String... values) {
		final List<String> copy = Arrays.asList(values);
		copy.removeIf(Objects::isNull);
		meta.computeIfAbsent(key, k -> new ArrayList<>(copy.size())).addAll(copy);
	}

	@JsonAnyGetter
	public Map<String, List<String>> getMap() {
		return meta;
	}

}
