package de.gwdg.cdstar.web.common.model;

import java.util.Date;


public class SnapshotInfo {
	public String name;
	public String revision;
	public String creator;
	public Date created;
	public String profile;
}
