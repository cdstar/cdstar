package de.gwdg.cdstar.web.common.model;

public class TransactionInfo {
	public String id;
	public String isolation;
	public boolean readonly;
	public long ttl;
	public long timeout;

}
