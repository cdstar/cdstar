package de.gwdg.cdstar.web.common.model;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

public class ArchiveUpdated extends ArchiveCreated {

	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	public List<Change> report = new ArrayList<>();

	public ArchiveUpdated() {
		super();
	}

	public ArchiveUpdated(String vault, String id, String revision) {
		super(vault, id, revision);
	}

	@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.EXISTING_PROPERTY, property = "change")
	@JsonSubTypes({
			@JsonSubTypes.Type(value = FileChange.class, name = "file"),
			@JsonSubTypes.Type(value = MetaChange.class, name = "meta")
	})
	public static abstract class Change {
		public String change;

		public Change() {
		}

		public Change(String changeName) {
			change = changeName;
		}
	}

	@JsonPropertyOrder({ "change", "id", "name", "info" })
	public static class FileChange extends Change {

		/**
		 * Original file name, or null if file was created
		 */
		public String name;

		/**
		 * File ID
		 */
		public String id;

		/**
		 * File info, or null if file was removed.
		 */
		public FileInfo info;

		public FileChange() {
			super("file");
		}

		public FileChange(String id, String name, FileInfo info) {
			super("file");
			this.id = id;
			this.name = name;
			this.info = info;
		}

	}

	@JsonPropertyOrder({ "change", "field", "file", "value" })
	public static class MetaChange extends Change {

		/**
		 * Property name
		 */
		public String field;

		/**
		 * New values
		 */
		public List<String> values;

		/**
		 * (New) name of file, or null if the property is defined on the archive
		 * itself.
		 */
		@JsonInclude(JsonInclude.Include.NON_NULL)
		public String file = null;

		public MetaChange() {
			super();
		}

		public MetaChange(String key, List<String> value, String ref) {
			super("meta");
			field = key;
			values = value;
			if (ref != null)
				file = ref;
		}
	}

	public void addChange(Change change) {
		report.add(change);
	}

}
