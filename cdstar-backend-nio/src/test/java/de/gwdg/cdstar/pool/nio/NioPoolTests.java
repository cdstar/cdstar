package de.gwdg.cdstar.pool.nio;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThrows;

import java.nio.ByteBuffer;
import java.nio.channels.WritableByteChannel;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;

import org.junit.Test;

import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.config.MapConfig;
import de.gwdg.cdstar.pool.PoolError;
import de.gwdg.cdstar.pool.Resource;
import de.gwdg.cdstar.pool.StorageObject;
import de.gwdg.cdstar.pool.StoragePool;
import de.gwdg.cdstar.runtime.Config;

public class NioPoolTests extends AbstractPoolTest {

	private Path basePath;
	private NioPool nioPool;

	final Config cfg = new MapConfig();

	@Override
	StoragePool createPool() {
		try {
			if (basePath == null) {
				basePath = tmp.newFolder().toPath();
				cfg.setDefault(NioPool.PROP_PATH, basePath.toString());
			}
			if (nioPool == null)
				nioPool = new NioPool(cfg);
			return nioPool;
		} catch (Exception e) {
			throw Utils.toUnchecked(e);
		}
	}

	@Test
	public void testLargeScope() throws Exception {
		for (int i = 0; i < NioSession.MAX_SIZE; i++) {
			createObject();
		}

		expect.expect(PoolError.class);
		createObject();
	}

	@Test
	public void testNoJunkAfterRollback() throws Exception {
		final StorageObject obj = txStore().createObject();
		final String id = obj.getId();
		final Resource res = obj.createResource("test.txt");

		try (WritableByteChannel ch = res.getWriteChannel(0)) {
			ch.write(ByteBuffer.wrap("foo".getBytes(StandardCharsets.UTF_8)));
		}
		rollback();
		assertFalse(Files.exists(nioPool.getObjectPath(id)));
	}

	@Test
	public void testNoJunkAfterRollbackInExistingObject() throws Exception {
		StorageObject obj = txStore().createObject();
		final String id = obj.getId();
		commit();
		assertEquals(2, Files.list(nioPool.getObjectPath(id)).count());

		obj = readObject(id);
		final Resource res = obj.createResource("test.txt");
		try (WritableByteChannel ch = res.getWriteChannel(0)) {
			ch.write(ByteBuffer.wrap("foo".getBytes(StandardCharsets.UTF_8)));
		}
		rollback();
		// HEAD and revision files, nothing else
		assertEquals(2, Files.list(nioPool.getObjectPath(id)).count());
	}

	@Test
	public void testNoJunkAfterRollbackAClonedFile() throws Exception {
		StorageObject src = txStore().createObject();
		StorageObject dst = txStore().createObject();
		final Resource res = src.createResource("test.txt");
		try (WritableByteChannel ch = res.getWriteChannel(0)) {
			ch.write(ByteBuffer.wrap("foo".getBytes(StandardCharsets.UTF_8)));
		}
		commit();

		src = readObject(src.getId());
		dst = readObject(dst.getId());
		dst.createResource("clone.txt").cloneFrom(src.getResource("test.txt"));
		assertEquals(1, Files.list(nioPool.getObjectPath(dst.getId())).filter(p -> p.toString().endsWith(".tmp")).count());
		rollback();
		assertEquals(0, Files.list(nioPool.getObjectPath(dst.getId())).filter(p -> p.toString().endsWith(".tmp")).count());
	}

	@Test
	public void testEnableUnknownDigest() throws Exception {
		cfg.set(NioPool.PROP_DIGESTS, "fuzzy");
		assertThrows(PoolError.class, this::createPool);
	}

	@Test
	public void testChangeDigestAlgos() throws Exception {

		// Enable MD5 (and SHA-256), disable SHA-1
		cfg.set(NioPool.PROP_DIGESTS, Resource.DIGEST_MD5);

		// Write some data
		Resource res = txStore().createObject().createResource("test.txt");
		try (WritableByteChannel ch = res.getWriteChannel(0)) {
			ch.write(ByteBuffer.wrap("foo".getBytes(StandardCharsets.UTF_8)));
		}
		commit();

		// Assume only the enabled digests are calculated.
		res = readObject(res.getObject().getId()).getResource("test.txt");
		assertNotNull(res.getDigests().get(Resource.DIGEST_MD5));
		assertNotNull(res.getDigests().get(Resource.DIGEST_SHA256));
		assertNull(res.getDigests().get(Resource.DIGEST_SHA1));
		rollback();

		// Disable MD5, enable SHA-1 (and SHA-256)
		nioPool.setDigests(Arrays.asList(Resource.DIGEST_SHA1));

		// Assume already computed values are still available
		res = readObject(res.getObject().getId()).getResource("test.txt");
		assertNotNull(res.getDigests().get(Resource.DIGEST_MD5));
		assertNotNull(res.getDigests().get(Resource.DIGEST_SHA256));
		assertNull(res.getDigests().get(Resource.DIGEST_SHA1));

		// Write some data. This should reset digests.
		try (WritableByteChannel ch = res.getWriteChannel(0)) {
			ch.write(ByteBuffer.wrap("bar".getBytes(StandardCharsets.UTF_8)));
		}

		// Assume the new digests are now available and the disabled ones disappear
		assertNull(res.getDigests().get(Resource.DIGEST_MD5));
		assertNotNull(res.getDigests().get(Resource.DIGEST_SHA256));
		assertNotNull(res.getDigests().get(Resource.DIGEST_SHA1));
		commit();
	}

}
