package de.gwdg.cdstar.pool.nio;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.function.BiConsumer;

import org.apache.commons.io.IOUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.TemporaryFolder;

import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.pool.PoolError;
import de.gwdg.cdstar.pool.PoolError.ExternalResourceException;
import de.gwdg.cdstar.pool.PoolError.NameConflict;
import de.gwdg.cdstar.pool.PoolError.NotFound;
import de.gwdg.cdstar.pool.PoolError.StaleHandle;
import de.gwdg.cdstar.pool.Resource;
import de.gwdg.cdstar.pool.StorageObject;
import de.gwdg.cdstar.pool.StoragePool;
import de.gwdg.cdstar.pool.StorageSession;
import de.gwdg.cdstar.pool.nio.TestTransactionManager.TestTransaction;
import de.gwdg.cdstar.ta.TAListener;
import de.gwdg.cdstar.ta.TAResource;
import de.gwdg.cdstar.ta.TransactionHandle;
import de.gwdg.cdstar.ta.TransactionInfo;
import de.gwdg.cdstar.ta.TransactionInfo.Mode;
import de.gwdg.cdstar.ta.exc.TARollbackException;
import de.gwdg.cdstar.utils.test.TestLogger;
import de.gwdg.cdstar.utils.test.TestUtils;

public abstract class AbstractPoolTest {

	@Rule
	public TemporaryFolder tmp = new TemporaryFolder();

	@Rule
	public TestLogger log = new TestLogger("de.gwdg");

	@Rule
	public final ExpectedException expect = ExpectedException.none();

	private StoragePool pool;

	private TestTransactionManager txm;
	private TestTransaction tx;
	private StorageSession sess;

	abstract StoragePool createPool();

	@Before
	public void setupStore() throws Exception {
		txm = new TestTransactionManager();
	}

	public StoragePool getPool() {
		if(pool==null)
			pool = createPool();
		return pool;
	}

	@After
	public void teardown() throws Exception {
		if (tx != null && tx.isOpen())
			tx.rollback();

		if (pool != null)
			pool.close();
	}

	/** Return a new (unmanaged) transaction. **/
	TestTransaction newTx() {
		return (TestTransaction) txm.begin();
	}

	TestTransaction tx() {
		if (tx == null) {
			tx = (TestTransaction) txm.begin();
			tx.addListener(new TAListener() {
				@Override
				public void afterCommit(TransactionInfo t) {
					tx = null;
					sess = null;
				}

				@Override
				public void afterRollback(TransactionInfo t) {
					tx = null;
					sess = null;
				}
			});
		}
		return tx;
	}

	StorageSession txStore() {
		if (sess == null)
			sess = getPool().open(tx());
		return sess;
	}

	StorageObject createObject() {
		return txStore().createObject();
	}

	StorageObject readObject(String id) {
		return txStore().readObject(id);
	}

	StorageObject readObject(String id, String rev) {
		return txStore().readObject(id, rev);
	}

	void commit() throws TARollbackException {
		tx().commit();
	}

	void rollback() {
		tx().rollback();
	}

	private void writeString(Resource res, String str) throws IOException {
		try (WritableByteChannel ch = res.getWriteChannel(Long.MAX_VALUE)) {
			ch.write(ByteBuffer.wrap(str.getBytes(StandardCharsets.UTF_8)));
		}
	}

	private String readString(Resource res) throws StaleHandle, ExternalResourceException, IOException {
		try (ReadableByteChannel ch = res.getReadChannel(0)) {
			final ByteBuffer buf = ByteBuffer.allocate((int) res.getSize());
			ch.read(buf);
			assertEquals(res.getSize(), buf.position());
			return new String(buf.array(), 0, buf.position(), StandardCharsets.UTF_8);
		}
	}

	@Test
	public void testOpenDirect() throws Exception {
		final StorageObject obj = createObject();
		final Resource res = obj.createResource("test.txt");
		writeString(res, "Test!");
		commit();

		StorageObject d = getPool().loadObjectDirect(obj.getId(), null);
		assertTrue(d.hasResource("test.txt"));

		try {
			d.setProperty("whatever", "yea, no");
			fail("Expected StaleHandle exception");
		} catch (final StaleHandle e) {
			// pass
		}

		// Try explicit revision load
		d = getPool().loadObjectDirect(obj.getId(), obj.getNextRevision());
		assertEquals(obj.getNextRevision(), d.getRevision());

	}

	@Test
	public void testCreateObjectAndResource() throws Exception {
		StorageObject obj = createObject();
		Resource res = obj.createResource("test.txt");
		writeString(res, "Test!");
		assertEquals(1, obj.getResources().size());
		assertEquals("Test!", readString(res));
		assertNotNull(res.getId());
		commit();

		obj = readObject(obj.getId());
		res = obj.getResource("test.txt");
		assertEquals(1, obj.getResources().size());
		assertEquals("Test!", readString(res));
		assertNotNull(res.getId());

	}

	@Test
	public void testEnsureHeadEqualsExplicitRevision() throws Exception {
		StorageObject obj, obj2;

		obj = createObject();
		commit();

		obj = readObject(obj.getId());
		obj2 = readObject(obj.getId(), obj.getRevision());
		assertTrue(obj == obj2);

		rollback();

		obj = readObject(obj.getId(), obj.getRevision());
		obj2 = readObject(obj.getId());
		assertEquals(obj.getRevision(), obj2.getRevision());
		assertTrue(obj == obj2);

		rollback();
	}

	@Test
	public void testHandleRevisionSameAfterCommit() throws Exception {

		StorageObject obj = createObject();

		String rev = obj.getRevision();
		String nrev = obj.getNextRevision();

		assertNull(rev);
		assertNotNull(nrev);

		commit();

		assertNull(rev);
		assertNotNull(nrev);

		obj = readObject(obj.getId());

		assertEquals(nrev, obj.getRevision());
		rev = nrev;
		nrev = obj.getNextRevision();

		obj.touch();
		commit();

		assertEquals(rev, obj.getRevision());
		assertEquals(nrev, obj.getNextRevision());
	}

	@Test
	public void testRemoveResource() throws Exception {
		StorageObject obj;

		obj = createObject();
		writeString(obj.createResource("test1.txt"), "MEH");
		obj.createResource("test2.txt").remove();
		writeString(obj.createResource("test3.txt"), "MEH");
		obj.getResource("test3.txt").remove();

		commit();

		obj = readObject(obj.getId());
		assertEquals(1, obj.getResources().size());
		obj.getResource("test1.txt").remove();

		commit();

		obj = readObject(obj.getId());
		assertEquals(0, obj.getResources().size());
	}

	@Test
	public void testResourceNameConflictCreate() throws Exception {

		final StorageObject obj = createObject();
		obj.createResource("test1.txt");
		expect.expect(NameConflict.class);
		obj.createResource("test1.txt");
	}

	@Test
	public void testResourceNameConflictRename() throws Exception {
		final StorageObject obj = createObject();
		obj.createResource("test1.txt");
		expect.expect(NameConflict.class);
		obj.createResource("test2.txt").setName("test1.txt");
	}

	@Test
	public void testResourceNameConflictRenameAfterCommit() throws Exception {

		StorageObject obj = createObject();
		obj.createResource("test1.txt");
		obj.createResource("test2.txt");

		commit();

		obj = readObject(obj.getId());
		expect.expect(NameConflict.class);
		obj.getResource("test2.txt").setName("test1.txt");
	}

	@Test
	public void testResourceNameConflictUnicode() throws Exception {

		final StorageObject obj = createObject();
		obj.createResource("\ufb01.txt"); // ﬁ <- single character
		expect.expect(NameConflict.class);
		obj.createResource("fi.txt"); // fi <- two characters
	}

	@Test
	public void testStableResourceID() throws Exception {

		StorageObject obj = createObject();
		final Resource r = obj.createResource("1.txt");
		final String rid = r.getId();
		r.setName("2.txt");

		commit();

		obj = readObject(obj.getId());
		obj.getResource("2.txt").setName("3.txt");

		commit();

		obj = readObject(obj.getId());
		assertEquals(rid, obj.getResource("3.txt").getId());
		assertEquals("3.txt", obj.getResourceByID(rid).getName());
	}

	@Test
	public void testTruncateResource() throws Exception {
		final String content = "Hello World";
		final String moreContent = "moreContent";
		final int len1 = content.length();
		final int len2 = moreContent.length();
		final int[] sizes = new int[] { -1, 0, len1 / 2, len1 - 1, len1,
				len1 + 1, len1 + len2 - 1, len1 + len2,
				len1 + len2 + 1 };

		/*
		 * This test covers a lot of cases: 1) Truncate size negative, zero, smaller,
		 * equal or larger than resource size. 2) Resource is empty or has content. 3)
		 * Resource was modified during the same transaction or not 4) Resource was
		 * created during the same transaction or not.
		 */

		for (final int size : sizes) {
			StorageObject obj = createObject();
			obj.createResource("empty.txt");
			writeString(obj.createResource("full.txt"), content);
			writeString(obj.createResource("full2.txt"), content);

			commit();

			final BiConsumer<Resource, Integer> truncate = (r, l) -> {
				try {
					r.getWriteChannel(l).close();
				} catch (StaleHandle | ExternalResourceException | IOException e) {
					Utils.wtf(e);
				}
			};

			obj = readObject(obj.getId());
			truncate.accept(obj.getResource("empty.txt"), size);
			truncate.accept(obj.getResource("full.txt"), size);
			writeString(obj.getResource("full2.txt"), moreContent);
			truncate.accept(obj.getResource("full2.txt"), size);
			writeString(obj.createResource("new.txt"), content);
			truncate.accept(obj.getResource("new.txt"), size);

			commit();

			final int expectedSize, expectedSize2;
			if (size < 0) {
				expectedSize = content.length();
				expectedSize2 = content.length() + moreContent.length();
			} else {
				expectedSize = Math.min(content.length(), Math.max(size, 0));
				expectedSize2 = Math.min(content.length() + moreContent.length(), Math.max(size, 0));
			}

			obj = readObject(obj.getId());
			assertEquals(0, obj.getResource("empty.txt").getSize());

			assertEquals(expectedSize, obj.getResource("full.txt").getSize());
			assertEquals(expectedSize, obj.getResource("new.txt").getSize());
			assertEquals(expectedSize2, obj.getResource("full2.txt").getSize());
		}
	}

	@Test
	public void testAppendWrite() throws Exception {

		StorageObject obj = createObject();
		Resource res = obj.createResource("test.txt");
		writeString(res, "Hello");

		commit();

		obj = readObject(obj.getId());
		res = obj.getResource("test.txt");
		writeString(res, " World!");

		commit();

		obj = readObject(obj.getId());
		res = obj.getResource("test.txt");
		assertEquals("Hello World!", readString(res));
	}

	@Test
	public void testEverythingUpdatesModifiedTime() throws Exception {

		final StorageObject obj = createObject();
		final Resource res = obj.createResource("test.txt");
		writeString(res, "Hello");

		Date mtime = obj.getLastModified();
		Date fmtime = res.getLastModified();

		Utils.sleepInterruptable(2);
		writeString(res, " World!");
		assertTrue(obj.getLastModified().after(mtime));
		assertTrue(res.getLastModified().after(fmtime));
		mtime = obj.getLastModified();
		fmtime = res.getLastModified();

		Utils.sleepInterruptable(2);
		res.setName("newname.txt");
		assertTrue(obj.getLastModified().after(mtime));
		assertFalse(res.getLastModified().after(fmtime));
		mtime = obj.getLastModified();
		fmtime = res.getLastModified();

		Utils.sleepInterruptable(2);
		res.setMediaType("new/mediatype");
		assertTrue(obj.getLastModified().after(mtime));
		assertFalse(res.getLastModified().after(fmtime));
		mtime = obj.getLastModified();
		fmtime = res.getLastModified();

		Utils.sleepInterruptable(2);
		res.setProperty("some.prop", "some value");
		assertTrue(obj.getLastModified().after(mtime));
		assertFalse(res.getLastModified().after(fmtime));
		mtime = obj.getLastModified();
		fmtime = res.getLastModified();

		rollback();
	}

	@Test
	public void testRepeatedWrite() throws Exception {

		StorageObject obj = createObject();
		Resource res = obj.createResource("test.txt");
		assertEquals(0, res.getSize());
		writeString(res, "Hello");
		assertEquals(5, res.getSize());
		writeString(res, " World!");
		assertEquals(12, res.getSize());
		assertEquals("Hello World!", readString(res));

		commit();

		obj = readObject(obj.getId());
		res = obj.getResource("test.txt");
		assertEquals("Hello World!", readString(res));
	}

	@Test
	public void testCloseStreamsAfterModify() throws Exception {

		final StorageObject obj = createObject();
		final Resource res = obj.createResource("test.txt");
		writeString(res, "Hello");

		final ReadableByteChannel rc1 = res.getReadChannel(0);
		assertTrue(rc1.isOpen());

		writeString(res, " World");
		final ReadableByteChannel rc2 = res.getReadChannel(0);
		assertFalse(rc1.isOpen());
		assertTrue(rc2.isOpen());
		assertEquals("Hello World", IOUtils.toString(Channels.newInputStream(rc2), StandardCharsets.UTF_8));
	}

	@Test
	public void testOpenWriteChannelPreventsCommit() throws Exception {
		final StorageObject obj = createObject();
		obj.createResource("test.txt").getWriteChannel(0);

		TARollbackException rollback = TestUtils.assertRaises(TARollbackException.class, this::commit);
		assertTrue(rollback.getCause() instanceof PoolError.WriteChannelNotClosed);
	}


	@Test
	public void testOpenRestoreChannelPreventsCommit() throws Exception {
		final StorageObject obj = createObject();
		Resource res = obj.createResource("test.txt");
		res.setExternalLocation("foo");
		res.getRestoreChannel();

		TARollbackException rollback = TestUtils.assertRaises(TARollbackException.class, this::commit);
		assertTrue(rollback.getCause() instanceof PoolError.WriteChannelNotClosed);
	}

	@Test
	public void testRollback() throws Exception {

		final StorageObject obj = createObject();
		final Resource res = obj.createResource("test.txt");
		writeString(res, "Hello");

		rollback();

		expect.expect(NotFound.class);
		readObject(obj.getId());
	}

	@Test
	public void testNotModified() throws Exception {

		final StorageObject obj = createObject();
		final Resource res = obj.createResource("test.txt");
		writeString(res, "Hello");

		commit();

		readObject(obj.getId());

		commit();
	}

	@Test
	public void testNotFound() throws Exception {
		expect.expect(NotFound.class);
		readObject("gskjfdhkjhasgkdjfsahgkfj");
	}

	@Test
	public void testInvalidShortID() throws Exception {
		expect.expect(NotFound.class);
		readObject("x");
	}

	@Test
	public void testInvalidLongId() throws Exception {
		expect.expect(NotFound.class);
		readObject(Utils.bytesToHex(Utils.randomBytes(1024)));
	}

	@Test
	public void testInvalidCharId() throws Exception {
		expect.expect(NotFound.class);
		readObject("\0xxxxxx");
	}

	@Test
	public void testNullId() throws Exception {
		expect.expect(NullPointerException.class);
		readObject(null);
	}

	@Test
	public void testListObjects() throws Exception {

		final List<String> allIDs = new ArrayList<>();
		for (int i = 0; i < 10; i++) {
			allIDs.add(createObject().getId());
		}

		commit();

		Collections.sort(allIDs);

		assertEquals(10, Utils.toArrayList(getPool().getObjectIDs()).size());
		assertEquals(allIDs, Utils.toArrayList(getPool().getObjectIDs()));
	}

	@Test
	public void testScrollObjects() throws Exception {

		final List<String> allIDs = new ArrayList<>();
		for (int i = 0; i < 10; i++) {
			allIDs.add(createObject().getId());
		}

		commit();

		Collections.sort(allIDs);

		for (int i = 0; i < allIDs.size(); i++) {
			assertEquals(allIDs.size() - i - 1, Utils.toArrayList(getPool().getObjectIDs(allIDs.get(i))).size());
			assertEquals(allIDs.subList(i + 1, allIDs.size()), Utils.toArrayList(getPool().getObjectIDs(allIDs.get(i))));
		}

	}

	@Test
	public void testMultipleObjects() throws Exception {

		for (int i = 0; i < 10; i++) {
			final StorageObject obj = createObject();
			for (int j = 0; j < 10; j++) {
				final Resource res = obj.createResource("test" + j + ".txt");
				writeString(res, "Hello");
			}
		}
		commit();
	}

	@Test
	public void testPrepareThenRollback() throws Exception {

		final StorageObject obj = createObject();
		final Resource res = obj.createResource("test.txt");
		writeString(res, "Hello");

		tx.bind(new TAResource() {

			@Override
			public void rollback(TransactionHandle t) {
			}

			@Override
			public void prepare(TransactionHandle t) throws Exception {
				throw new RuntimeException("I'm just a test");
			}

			@Override
			public void commit(TransactionHandle t) {
			}

			@Override
			public void bind(TransactionHandle t) {
			}
		});

		try {
			commit(); // Simulate a failed prepare in some other component.
			fail("Expected TARollbackException");
		} catch (final TARollbackException e) {
		}

		assertNull(tx);

		expect.expect(NotFound.class);
		getPool().open(newTx()).readObject(obj.getId());
	}

	@Test
	public void testPrepareParallelRead() throws Exception {
		final TestTransaction ta1 = newTx();
		final TestTransaction ta2 = newTx();
		final StorageObject obj = getPool().open(ta1).createObject();
		final Resource res = obj.createResource("test.txt");
		writeString(res, "Hello");
		ta1.testPrepare();

		expect.expect(NotFound.class);
		getPool().open(ta2).readObject(obj.getId());
	}

	@Test
	public void testDelete() throws Exception {

		final StorageObject obj = createObject();
		final Resource res = obj.createResource("test.txt");
		writeString(res, "Hello");

		commit();

		readObject(obj.getId()).remove();

		commit();

		Utils.sleepInterruptable(10);

		expect.expect(NotFound.class);
		readObject(obj.getId());
	}

	@Test
	public void testDeleteImmediately() throws Exception {

		final StorageObject obj = createObject();
		final Resource res = obj.createResource("test.txt");
		writeString(res, "Hello");
		obj.remove();

		commit();

		expect.expect(NotFound.class);
		readObject(obj.getId());
	}

	@Test
	public void testCrashRecoveryNoCommitPrePrepare() throws Exception {

		final StorageObject obj = createObject();
		final Resource res = obj.createResource("test.txt");
		writeString(res, "Hello");

		txm.simulateCrash();

		expect.expect(NotFound.class);
		getPool().open(newTx()).readObject(obj.getId());
	}

	@Test
	public void testCrashRecoveryNoCommit() throws Exception {

		final StorageObject obj = createObject();
		final Resource res = obj.createResource("test.txt");
		writeString(res, "Hello");

		tx.testHalfPrepare();
		txm.simulateCrash();

		expect.expect(NotFound.class);
		getPool().open(newTx()).readObject(obj.getId());
	}

	@Test
	public void testCrashRecoveryDoCommit() throws Exception {
		final StorageObject obj = createObject();
		final Resource res = obj.createResource("test.txt");
		writeString(res, "Hello");
		tx.testPrepare();
		txm.simulateCrash();

		assertEquals(obj.getId(), readObject(obj.getId()).getId());
	}

	/**
	 * Prepare should be exclusive per object.
	 */
	@Test
	public void testTransactionPrepareConflict() throws Exception {

		final String conflict = createObject().getId();

		commit();

		final TestTransaction t1 = newTx();
		final TestTransaction t2 = newTx();

		getPool().open(t1).readObject(conflict).createResource("t1");
		getPool().open(t2).readObject(conflict).createResource("t2");

		t1.testPrepare();

		try {
			t2.testPrepare();
			fail("This should throw PoolError.Conflict");
		} catch (final TARollbackException e) {
			assertTrue(e.getCause() instanceof PoolError.Conflict);
			t2.rollback();
		}

		t1.testCommit();

		assertTrue(readObject(conflict).hasResource("t1"));
	}

	/**
	 * If two transactions edit the same object, then the first one to commit should
	 * win and the second should fail. (Lost update / write-after-read conflict)
	 */
	@Test
	public void testTransactionWriteConflict() throws Exception {

		final String conflict = createObject().getId();
		commit();

		final TestTransaction t1 = newTx();
		final TestTransaction t2 = newTx();

		getPool().open(t1).readObject(conflict).createResource("t1");
		getPool().open(t2).readObject(conflict).createResource("t2");

		t1.commit();

		try {
			t2.commit();
			fail("This should throw PoolError.Conflict");
		} catch (final TARollbackException e) {
			assertTrue(e.getCause() instanceof PoolError.Conflict);
		}

		assertTrue(readObject(conflict).hasResource("t1"));
	}

	/**
	 * If one transaction reads an object and another changes it, the first
	 * transaction should not be able to commit.
	 *
	 * Example: T1 copies data from A to B. Expected: A and B contain the same data.
	 *
	 * Conflict: T2 commits changes to A during the lifetime if T1. Expected: T1
	 * should not be able to commit.
	 *
	 */
	@Test
	public void testTransactionReadConflict() throws Exception {

		final String conflict = createObject().getId();

		commit();

		final TestTransaction tSnapshot = newTx();
		final TestTransaction tSerailizable = newTx();
		tSerailizable.setMode(Mode.SERIALIZABILITY);
		final TestTransaction tConflict = newTx();

		getPool().open(tSnapshot).readObject(conflict);
		getPool().open(tSnapshot).createObject();

		getPool().open(tSerailizable).readObject(conflict);
		getPool().open(tSerailizable).createObject();

		getPool().open(tConflict).readObject(conflict).createResource("change");
		tConflict.commit();

		// In SNAPSHOT mode, read-write is not a conflict.
		tSnapshot.commit();

		try {
			// In SERIALIZABILITY mode, read-write IS a conflict.
			tSerailizable.testPrepare();
			fail("This should throw PoolError.Conflict");
		} catch (final TARollbackException e) {
			assertTrue(e.getCause() instanceof PoolError.Conflict);
			tSerailizable.rollback();
		}

		assertTrue(readObject(conflict).hasResource("change"));
	}

	/**
	 * For read-only transactions, commit() should have the same result as
	 * rollback(). This implies that commit() should never fail, and conflicts
	 * should be ignored.
	 */
	@Test
	public void testTransactionReadOnlyNoConflicht() throws Exception {

		final String conflict = createObject().getId();

		commit();

		// Required so TX and T1 have different timestamps
		tx();
		Utils.sleepInterruptable(100);

		final TestTransaction t1 = newTx();
		getPool().open(t1).readObject(conflict).createResource("t1");
		t1.commit();

		assertTrue(readObject(conflict).getResources().isEmpty());

		commit();

		assertTrue(readObject(conflict).hasResource("t1"));
	}

	@Test
	public void testProperties() throws Exception {

		StorageObject obj = createObject();
		Resource res = obj.createResource("test.txt");
		obj.setProperty("test.global", "test1");
		res.setProperty("test.res", "test2");

		assertEquals("test1", obj.getProperty("test.global"));
		assertEquals(null, obj.getProperty("test.res"));
		assertEquals("test2", res.getProperty("test.res"));
		assertEquals(null, res.getProperty("test.global"));

		commit();

		obj = readObject(obj.getId());
		res = obj.getResource("test.txt");
		res.setProperty("test.res", "rollback");
		res.setProperty("test.res2", "rollback");

		assertEquals("test1", obj.getProperty("test.global"));
		assertEquals(null, obj.getProperty("test.res"));
		assertEquals("rollback", res.getProperty("test.res"));
		assertEquals(null, res.getProperty("test.global"));

		rollback();

		obj = readObject(obj.getId());
		res = obj.getResource("test.txt");

		assertEquals("test1", obj.getProperty("test.global"));
		assertEquals(null, obj.getProperty("test.res"));
		assertEquals("test2", res.getProperty("test.res"));
		assertEquals(null, res.getProperty("test.global"));
		assertTrue(obj.getPropertyNames().contains("test.global"));
		assertTrue(res.getPropertyNames().contains("test.res"));
	}

	@Test
	public void testDeleteProperties() throws Exception {

		StorageObject obj = createObject();
		Resource res = obj.createResource("test.txt");
		obj.setProperty("prop", "1");
		obj.setProperty("no-prop", null);
		res.setProperty("prop", "2");
		res.setProperty("no-prop", null);

		commit();

		obj = readObject(obj.getId());
		res = obj.getResource("test.txt");
		obj.setProperty("prop", null);
		res.setProperty("prop", null);

		commit();

		obj = readObject(obj.getId());
		res = obj.getResource("test.txt");

		assertEquals(null, obj.getProperty("prop"));
		assertEquals(null, res.getProperty("prop"));
		assertEquals(0, obj.getPropertyNames().size());
		assertEquals(0, res.getPropertyNames().size());
	}

	@Test
	public void testEmptyWrite() throws Exception {
		final StorageObject obj = createObject();
		final Resource res = obj.createResource("test.txt");
		writeString(res, "");
		writeString(res, "Hello");
		writeString(res, "");
		assertEquals("Hello", readString(res));
	}

	@Test
	public void testChangeMetaOnly() throws Exception {

		StorageObject obj = createObject();
		Resource res = obj.createResource("test.txt");
		writeString(res, "Hello");

		commit();

		obj = readObject(obj.getId());
		res = obj.getResource("test.txt");

		// Change only some meta-data, not the actual resource content.
		res.setProperty("foo", "bar");

		commit();

		obj = readObject(obj.getId());
		res = obj.getResource("test.txt");
		assertEquals("bar", res.getProperty("foo"));
	}

	/**
	 * Make sure resources and orpperties are still present if none of both is
	 * changed.
	 */
	@Test
	public void testChangePropertyOnly() throws Exception {

		StorageObject obj = createObject();
		Resource res = obj.createResource("test.txt");
		writeString(res, "Hello");
		obj.setProperty("foo", "bar");
		res.setProperty("foo", "baz");
		commit();

		obj = readObject(obj.getId());
		obj.setType("some/other-type");
		commit();

		obj = readObject(obj.getId());
		res = obj.getResource("test.txt");
		assertEquals("bar", obj.getProperty("foo"));
		assertEquals("baz", res.getProperty("foo"));
	}

	@Test
	public void testExternalizeResource() throws Exception {

		StorageObject obj = createObject();
		Resource res = obj.createResource("test.txt");
		writeString(res, "Hello World?");
		res.setExternalLocation("testLocation");

		assertTrue(res.isExternal());
		assertEquals(12, res.getSize());
		assertEquals("testLocation", res.getExternalLocation());
		assertEquals(Utils.sha256("Hello World?"), res.getDigests().get(Resource.DIGEST_SHA256));

		try {
			res.getReadChannel(0);
			fail("Missing exception");
		} catch (final ExternalResourceException e) {
		}

		try {
			res.getWriteChannel(-1);
			fail("Missing exception");
		} catch (final ExternalResourceException e) {
		}

		commit();

		obj = readObject(obj.getId());
		res = obj.getResource("test.txt");

		assertTrue(res.isExternal());
		assertEquals(12, res.getSize());
		assertEquals("testLocation", res.getExternalLocation());
		assertEquals(Utils.sha256("Hello World?"), res.getDigests().get(Resource.DIGEST_SHA256));

		try {
			res.getReadChannel(0);
			fail("Missing exception");
		} catch (final ExternalResourceException e) {
		}

		try {
			res.getWriteChannel(-1);
			fail("Missing exception");
		} catch (final ExternalResourceException e) {
		}

	}

	@Test
	public void textRestoreExternalResource() throws Exception {

		StorageObject obj = createObject();
		Resource res = obj.createResource("test.txt");
		writeString(res, "Hello World?");
		res.setExternalLocation("testLocation");

		Resource res2 = obj.createResource("test2.txt");
		writeString(res2, "Hello World?");

		commit();

		obj = readObject(obj.getId());
		res = obj.getResource("test.txt");
		res2 = obj.getResource("test2.txt");

		try {
			res.restoreExternal(new ByteArrayInputStream("Hello World!?".getBytes()));
			fail("Restore with wrong size should throw");
		} catch (final IOException e) {
		}

		try {
			res.restoreExternal(new ByteArrayInputStream("HELLO WORLD?".getBytes()));
			fail("Restore with wrong content should throw");
		} catch (final IOException e) {
		}

		try {
			res2.restoreExternal(new ByteArrayInputStream("Hello World?".getBytes()));
			fail("Restore on a non-externalized resource should throw");
		} catch (final IllegalStateException e) {
		}

		// This should work
		res.restoreExternal(new ByteArrayInputStream("Hello World?".getBytes()));
		assertFalse(res.isExternal());
		assertEquals(12, res.getSize());
		assertNull(res.getExternalLocation());
		assertEquals(Utils.sha256("Hello World?"), res.getDigests().get(Resource.DIGEST_SHA256));
		assertEquals("Hello World?", readString(res));

		commit();

		obj = readObject(obj.getId());
		res = obj.getResource("test.txt");

		assertFalse(res.isExternal());
		assertEquals(12, res.getSize());
		assertNull(res.getExternalLocation());
		assertEquals(Utils.sha256("Hello World?"), res.getDigests().get(Resource.DIGEST_SHA256));
		assertEquals("Hello World?", readString(res));
	}

	@Test
	public void textRestoreAfterModify() throws Exception {

		StorageObject obj = createObject();
		Resource res = obj.createResource("test.txt");
		writeString(res, "Hello World?");
		res.setExternalLocation("testLocation");

		commit();

		obj = readObject(obj.getId());
		res = obj.getResource("test.txt");
		res.setProperty("foo", "bar");
		res.restoreExternal(new ByteArrayInputStream("Hello World?".getBytes()));
		assertFalse(res.isExternal());
		assertEquals(12, res.getSize());
		assertNull(res.getExternalLocation());
		assertEquals(Utils.sha256("Hello World?"), res.getDigests().get(Resource.DIGEST_SHA256));
		assertEquals("Hello World?", readString(res));

		tx.rollback();
	}

	@Test
	public void testIntermediateDigestComputation() throws Exception {
		final StorageObject obj = createObject();
		final Resource res = obj.createResource("test.txt");
		final String d1 = res.getDigests().get(Resource.DIGEST_SHA256);
		assertNotNull(d1);
		writeString(res, "töst");
		final String d2 = res.getDigests().get(Resource.DIGEST_SHA256);
		assertNotNull(d2);
		assertNotEquals(d1, d2);
		res.getWriteChannel(0).close();
		final String d3 = res.getDigests().get(Resource.DIGEST_SHA256);
		assertNotNull(d3);
		assertEquals(d1, d3);
	}

	@Test
	public void testRestoreFromEmpty() throws Exception {

		StorageObject obj = createObject();
		Resource res = obj.createResource("test.txt");
		res.setExternalLocation("testLocation");

		commit();

		// Restore with empty input stream
		obj = readObject(obj.getId());
		res = obj.getResource("test.txt");
		res.restoreExternal(new ByteArrayInputStream(new byte[] {}));
		assertFalse(res.isExternal());
		assertEquals(0, res.getSize());
		assertNull(res.getExternalLocation());

		res.setExternalLocation("testLocation");

		commit();

		// Restore with non-empty input stream
		obj = readObject(obj.getId());
		res = obj.getResource("test.txt");

		try {
			res.restoreExternal(new ByteArrayInputStream(new byte[] { 0 }));
			fail("restore should check input stream size");
		} catch (final IOException e) {
		}

	}

	@Test
	public void testCloneComittedResource() throws Exception {

		StorageObject obj = createObject();
		final Resource res = obj.createResource("test.txt");
		writeString(res, "Hello World?");

		commit();

		obj = readObject(obj.getId());
		final StorageObject obj2 = createObject();
		final Resource res2 = obj2.createResource("clone");
		res2.cloneFrom(obj.getResource("test.txt"));

		assertEquals(Utils.sha256("Hello World?"), res2.getDigests().get(Resource.DIGEST_SHA256));
		assertEquals("Hello World?", readString(res2));

		commit();
	}

	@Test
	public void testCloneComittedResourceIntoModified() throws Exception {

		StorageObject obj = createObject();
		final Resource res = obj.createResource("test.txt");
		writeString(res, "Hello World?");

		commit();

		obj = readObject(obj.getId());
		final StorageObject obj2 = createObject();
		final Resource res2 = obj2.createResource("clone");
		writeString(res2, "mod");
		// This should overwrite the old content.
		res2.cloneFrom(obj.getResource("test.txt"));

		assertEquals(Utils.sha256("Hello World?"), res2.getDigests().get(Resource.DIGEST_SHA256));
		assertEquals("Hello World?", readString(res2));

		commit();
	}

	@Test
	public void testCloneUncommittedResource() throws Exception {

		StorageObject obj = createObject();
		final Resource res = obj.createResource("test.txt");
		writeString(res, "Hello World?");

		obj = readObject(obj.getId());
		final StorageObject obj2 = createObject();
		final Resource res2 = obj2.createResource("clone");
		res2.cloneFrom(obj.getResource("test.txt"));

		assertEquals(Utils.sha256("Hello World?"), res2.getDigests().get(Resource.DIGEST_SHA256));
		assertEquals("Hello World?", readString(res2));

		commit();
	}

	@Test
	public void testCloneEmptyResource() throws Exception {

		StorageObject obj = createObject();
		obj.createResource("test.txt");

		commit();

		obj = readObject(obj.getId());
		final StorageObject obj2 = createObject();
		final Resource res2 = obj2.createResource("clone");
		res2.cloneFrom(obj.getResource("test.txt"));

		assertEquals(Utils.sha256(""), res2.getDigests().get(Resource.DIGEST_SHA256));
		assertEquals("", readString(res2));

		commit();
	}

}