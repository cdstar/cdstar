package de.gwdg.cdstar.pool.nio;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.ByteBuffer;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import de.gwdg.cdstar.GromitIterable;
import de.gwdg.cdstar.ta.TAJournal;
import de.gwdg.cdstar.ta.TAJournalReader;
import de.gwdg.cdstar.ta.TAJournalRecord;
import de.gwdg.cdstar.ta.TAListener;
import de.gwdg.cdstar.ta.TARecoveryHandler;
import de.gwdg.cdstar.ta.TAResource;
import de.gwdg.cdstar.ta.TAState;
import de.gwdg.cdstar.ta.TransactionHandle;
import de.gwdg.cdstar.ta.TransactionManager;
import de.gwdg.cdstar.ta.UserTransaction;
import de.gwdg.cdstar.ta.exc.TAFatalError;
import de.gwdg.cdstar.ta.exc.TARollbackException;

public class TestTransactionManager implements TransactionManager {

	Map<String, TestTransaction> allTransactions = new HashMap<>();

	@Override
	public UserTransaction begin() {
		final TestTransaction tx = new TestTransaction();
		allTransactions.put(tx.getId(), tx);
		return tx;
	}

	@Override
	public UserTransaction getTransaction(String txId) {
		return allTransactions.get(txId);
	}

	public void simulateCrash() throws Exception {
		for (final TestTransaction t : allTransactions.values()) {
			for (final ByteArrayOutputStream bos : t.recovery.values()) {
				final ByteArrayInputStream bis = new ByteArrayInputStream(bos.toByteArray());
				final ObjectInputStream in = new ObjectInputStream(bis);
				final TARecoveryHandler handler = (TARecoveryHandler) in.readObject();
				handler.recover(t.getId(), t.journal, t.state == TAState.PREPARED);
			}
		}
		allTransactions.clear();
	}

	public class TestTransaction implements UserTransaction, TransactionHandle {

		private final String id;
		private final Instant started;
		private Instant expires;
		private TAState state;
		private Mode mode;
		private final InMemoryJournal journal;

		private GromitIterable<TAListener> listeners = new GromitIterable<>();

		private final Set<TAResource> resources = new HashSet<>();
		private final Map<TARecoveryHandler, ByteArrayOutputStream> recovery = new HashMap<>();

		public TestTransaction() {
			id = UUID.randomUUID().toString();
			started = Instant.now();
			state = TAState.NEW;
			mode = Mode.SNAPSHOT;
			journal = new InMemoryJournal();
		}

		@Override
		public String getId() {
			return id;
		}

		@Override
		public Instant getStarted() {
			return started;
		}

		@Override
		public Instant getExpires() {
			return expires;
		}

		@Override
		public TAState getState() {
			return state;
		}

		@Override
		public Mode getMode() {
			return mode;
		}

		@Override
		public void bindRecoveryHandler(TARecoveryHandler handler) {
			final ByteArrayOutputStream bos = new ByteArrayOutputStream();
			ObjectOutputStream out;
			try {
				out = new ObjectOutputStream(bos);
				out.writeObject(handler);
				out.close();
			} catch (final IOException e) {
				throw new RuntimeException(e);
			}
			recovery.put(handler, bos);
		}

		@Override
		public TAJournal getJournal() {
			return journal;
		}

		@Override
		public void bind(TAResource resource) {
			resources.add(resource);
			resource.bind(this);
		}

		@Override
		public void commit() throws TARollbackException {
			testPrepare();
			testCommit();
		}

		/**
		 * Complete half-committed transaction (see {@link #testPrepare()}.
		 */
		public void testCommit() throws TAFatalError {
			assertEquals(TAState.PREPARED, state);

			try {
				for (final TAResource r : resources)
					r.commit(this);
			} catch (final Exception e) {
				throw new TAFatalError("Commit failed", e);
			}

			state = TAState.COMMITTED;

			listeners.forEach(l -> l.afterCommit(this), e -> {
				throw new RuntimeException(e);
			});
		}

		/**
		 * Perform only the prepare step of a commit.
		 */
		public void testPrepare() throws TARollbackException {
			assertEquals(TAState.NEW, state);

			listeners.forEach(l -> l.beforeCommit(this), e -> {
				throw new RuntimeException(e);
			});

			state = TAState.PREPARING;

			try {
				for (final TAResource r : resources)
					r.prepare(this);
			} catch (final Exception e) {
				rollback(e);
				throw new TARollbackException(e);
			}

			state = TAState.PREPARED;
		}

		/**
		 * Like {@link #testPrepare()} but leaves the transaction in PREPARING
		 * state.
		 */
		public void testHalfPrepare() throws TARollbackException {
			testPrepare();
			state = TAState.PREPARING;
		}

		@Override
		public void rollback(Throwable reason) {
			if (isClosed())
				return;

			assertTrue(state == TAState.NEW || state == TAState.PREPARING || state == TAState.ROLLBACKONLY);
			try {
				for (final TAResource r : resources)
					r.rollback(this);
			} catch (final Exception e) {
				throw new TAFatalError("Rollback failed", e);
			}

			listeners.forEach(l -> l.afterRollback(this), e -> {
				throw new RuntimeException(e);
			});
		}

		@Override
		public void setRollbackOnly() {
			assertEquals(TAState.NEW, state);
			state = TAState.ROLLBACKONLY;
		}

		@Override
		public void setMode(Mode mode) {
			this.mode = mode;
		}

		@Override
		public void setExpires(Instant instant) {
			expires = instant;
		}

		@Override
		public synchronized void addListener(TAListener listener) {
			if (listeners == null)
				listeners = new GromitIterable<>();
			listeners.add(listener);
		}

	}

	static class InMemoryJournal implements TAJournal, TAJournalReader {
		List<TAJournalRecord> entries = new ArrayList<>();
		int pos;

		@Override
		public void write(String name, ByteBuffer data) {
			entries.add(new InMemoryJournalRecord(name,
				Arrays.copyOfRange(data.array(),
					data.arrayOffset() + data.position(),
					data.arrayOffset() + data.limit())));
		}

		@Override
		public void flush() throws IOException {
		}

		@Override
		public TAJournalRecord next() throws IOException {
			return pos < entries.size() ? entries.get(pos++) : null;
		}

		@Override
		public void reset() throws IOException {
			pos = 0;
		}

		@Override
		public boolean isClosed() {
			return false;
		}

	}

	private static class InMemoryJournalRecord implements TAJournalRecord {
		private final String name;
		private final byte[] data;

		public InMemoryJournalRecord(String name, byte[] data) {
			this.name = name;
			this.data = data;
		}

		@Override
		public String getName() {
			return name;
		}

		@Override
		public byte[] getValue() {
			return data;
		}

	}

}
