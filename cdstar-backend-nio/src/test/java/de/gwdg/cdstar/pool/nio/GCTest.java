package de.gwdg.cdstar.pool.nio;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.WritableByteChannel;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Instant;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import de.gwdg.cdstar.pool.PoolError;
import de.gwdg.cdstar.pool.Resource;
import de.gwdg.cdstar.pool.StorageObject;
import de.gwdg.cdstar.pool.nio.TestTransactionManager.TestTransaction;
import de.gwdg.cdstar.ta.exc.TARollbackException;
import de.gwdg.cdstar.utils.test.TestLogger;
import de.gwdg.cdstar.utils.test.TestUtils;

public class GCTest {

	@Rule
	public TemporaryFolder tmp = new TemporaryFolder();

	@Rule
	public TestLogger log = new TestLogger("de.gwdg");

	private Path basePath;
	private TestTransactionManager txm;
	private NioPool pool;

	private TestTransaction tx;

	@Before
	public void setupStore() throws Exception {
		txm = new TestTransactionManager();
		basePath = tmp.newFolder().toPath();
		pool = new NioPool(basePath);
		tx = (TestTransaction) txm.begin();
	}

	@After
	public void closeTX() {
		tx.close();
	}

	void commit() throws TARollbackException {
		tx.commit();
		tx = (TestTransaction) txm.begin();
	}

	private GCTask gc(final String id) {
		return gc(id, null);
	}

	private GCTask gc(final String id, Instant keepNewerThan) {
		final GCTask gc = new GCTask(pool, id, false, keepNewerThan);
		gc.run();
		return gc;
	}

	private static void writeString(Resource file, String data) throws IOException {
		try (WritableByteChannel ch = file.getWriteChannel(file.getSize())) {
			ch.write(ByteBuffer.wrap(data.getBytes(StandardCharsets.UTF_8)));
		}
	}

	@Test
	public void testFullGC() throws Exception {
		StorageObject o = pool.open(tx).createObject();
		writeString(o.createResource("test1"), "AAA");
		writeString(o.createResource("test2"), "BBB");
		final String id = o.getId();
		commit();

		// Head, rev, test1, test2
		assertEquals(4, Files.list(pool.getObjectPath(id)).count());

		o = pool.open(tx).readObject(id);

		writeString(o.getResource("test1"), "CCC");
		o.getResource("test2").remove();
		writeString(o.createResource("test3"), "AAA"); // same as old "test1"
		commit();

		// Head, rev, rev , test1, old test2, test3 (equals old test1)
		assertEquals(6, Files.list(pool.getObjectPath(id)).count());

		final GCTask gc = gc(id);

		// Head, rev, test1, test3
		assertEquals(4, Files.list(pool.getObjectPath(id)).count());
		assertEquals(2, gc.getCollectedFiles());
	}

	@Test
	public void testDeletedObject() throws Exception {
		StorageObject o = pool.open(tx).createObject();
		final String id = o.getId();
		commit();

		o = pool.open(tx).readObject(id);
		o.remove();
		commit();

		Files.createFile(pool.getObjectPath(id).resolve("xxx.unknown"));
		Files.createFile(pool.getObjectPath(id).resolve("xxx.tmp"));


		final GCTask gc = gc(id);
		// HEAD link + 2 revisions (first + deleted)
		assertEquals(3, gc.getCollectedFiles());
	}

	@Test
	public void testKeepRecentEvenIfHeadWasRemoved() throws Exception {
		final StorageObject o = pool.open(tx).createObject();
		final String id = o.getId();
		commit();

		pool.open(tx).readObject(id).remove();
		commit();

		Instant keepAfter = o.getCreated().toInstant().minusSeconds(1);

		final GCTask gc = gc(id, keepAfter);
		assertEquals(0, gc.getCollectedFiles());
	}

	@Test
	public void testEmptyObject() throws Exception {
		final StorageObject o = pool.open(tx).createObject();
		final String id = o.getId();
		commit();
		final GCTask gc = gc(id);
		assertEquals(0, gc.getCollectedFiles());
	}

	@Test
	public void testKeepUnknownFiles() throws Exception {
		final StorageObject o = pool.open(tx).createObject();
		final String id = o.getId();
		commit();

		Files.createFile(pool.getObjectPath(id).resolve("xxx.tmp"));
		Files.createFile(pool.getObjectPath(id).resolve("xxx.temp"));
		Files.createFile(pool.getObjectPath(id).resolve("xxx.unknown"));
		Files.createFile(pool.getObjectPath(id).resolve("xxx"));

		final GCTask gc = gc(id);
		assertEquals(0, gc.getCollectedFiles());
	}

	@Test
	public void testCollectExternalFiles() throws Exception {
		StorageObject o = pool.open(tx).createObject();
		writeString(o.createResource("test1"), "data");
		writeString(o.createResource("test2"), "data");
		writeString(o.createResource("test3"), "data2");
		final String id = o.getId();

		commit();

		o = pool.open(tx).readObject(id);
		o.getResource("test2").setExternalLocation("XXX");
		o.getResource("test3").setExternalLocation("XXX");
		tx.commit();

		final GCTask gc = gc(id);
		// rev + test3, but not test2 as it is dedublicated into test1 already
		assertEquals(2, gc.getCollectedFiles());
	}

	@Test
	public void testFailIfLocked() throws Exception {
		final StorageObject o = pool.open(tx).createObject();
		final String id = o.getId();
		commit();

		pool.open(tx).readObject(id).createResource("XXX");
		tx.testPrepare();

		TestUtils.assertRaises(PoolError.Conflict.class, () -> {
			gc(id);
		});

		tx.testCommit();
	}

	@Test
	public void testFailIfNotFound() throws Exception {
		TestUtils.assertRaises(PoolError.NotFound.class, () -> {
			gc("xxx");
		});

		TestUtils.assertRaises(PoolError.NotFound.class, () -> {
			gc("1234567890abcdef");
		});
	}

}
