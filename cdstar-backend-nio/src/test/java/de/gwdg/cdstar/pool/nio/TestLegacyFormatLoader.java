package de.gwdg.cdstar.pool.nio;

import static org.junit.Assert.assertEquals;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.pool.nio.json.JsonFormat;
import de.gwdg.cdstar.pool.nio.json.JsonIndex;

public class TestLegacyFormatLoader {

	@Rule
	public TemporaryFolder tmp = new TemporaryFolder();

	@Test
	public void testLoadLegacyFile() throws Exception {
		final JsonFormat os = new JsonFormat();
		final JsonIndex x = os.load(Paths.get(getClass().getResource("./legacy/v3-pre.json").toURI()));
		assertEquals(1509035256292l, x.ctime);
		assertEquals(1509035256338l, x.mtime);
		assertEquals(-1, x.dtime);
		assertEquals(1509035256321l, x.resources.get(0).ctime);
		assertEquals(1509035256324l, x.resources.get(0).mtime);
		assertEquals("fXkwN6B2AYZXSwKC8vQ15w==", Utils.base64encode(x.resources.get(0).md5));
		assertEquals("fCEUM/AgcVl3Qeb/Wo6jR4mrv0M=", Utils.base64encode(x.resources.get(0).sha1));
		assertEquals("SG6kYiTRu0+2gPNPfJrZao8k7Ii+c+qOWmxlJg6cuKc=", Utils.base64encode(x.resources.get(0).sha256));
		assertEquals(5, x.resources.get(0).size);
		assertEquals("data/hello.txt", x.resources.get(0).name);

		// Test round-trip identity. This is not a requirement, but nice to
		// have.
		final Path file1 = tmp.newFile().toPath();
		final Path file2 = tmp.newFile().toPath();
		os.write(file1, x);
		os.write(file2, os.load(file1));

		assertEquals(Files.readAllLines(file1), Files.readAllLines(file2));
	}

}
