package de.gwdg.cdstar.pool.nio;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.utils.test.TestLogger;

public class FileSharderTest {

	@Rule
	public TestLogger log = new TestLogger("de.gwdg");

	@Rule
	public TemporaryFolder tmp = new TemporaryFolder();

	static List<String> exampleIDs;
	static {
		exampleIDs = Arrays.asList("aaaaaa", "aabbbb", "aabbcc", "bbbbcc", "aabbcd", "aabbca", "abcdefghij", "zzzzzz");
		exampleIDs.sort(null);
	}

	public FileSharder makeDirs(List<String> ids, int depth) throws IOException {
		final FileSharder fs = new FileSharder(tmp.newFolder().toPath(), depth);
		for (final String id : ids) {
			Files.createDirectories(fs.getPath(id));
		}
		return fs;
	}

	@Test
	public void testIterator() throws Exception {
		final FileSharder fs = makeDirs(exampleIDs, 2);
		assertEquals(exampleIDs, Utils.toArrayList(fs));
	}

	@Test
	public void testSkipIterator() throws Exception {
		final FileSharder fs = makeDirs(exampleIDs, 2);
		for (int i = 0; i < exampleIDs.size(); i++)
			assertEquals(exampleIDs.subList(i + 1, exampleIDs.size()),
				Utils.toArrayList(fs.iterator(exampleIDs.get(i))));
	}

	@Test
	public void testSkipIteratorMidStart() throws Exception {
		final List<String> realIds = Arrays.asList("ba0c14a8032a", "c19456f2c693");
		final FileSharder fs = makeDirs(realIds, 2);
		assertEquals(realIds.subList(1, realIds.size()),
			Utils.toArrayList(fs.iterator("bad")));
	}

	@Test
	public void testSkipIteratorShortIDs() throws Exception {
		final FileSharder fs = makeDirs(exampleIDs, 2);

		// For each suffix of each example ID, check that the iterator behaves exactly
		// as a string compare on the entire string.

		for (final String id : exampleIDs) {
			for (int prefix = 1; prefix < id.length(); prefix++) {
				final String cmp = id.substring(0, prefix);
				final List<String> expected = exampleIDs.stream()
					.filter(s -> s.compareTo(cmp) > 0)
					.collect(Collectors.toList());
				assertEquals(expected, Utils.toArrayList(fs.iterator(cmp)));
			}
		}
	}

	@Test
	public void testStream() throws Exception {
		final FileSharder fs = makeDirs(exampleIDs, 2);
		assertEquals(exampleIDs, Utils.toArrayList(fs.stream()));
	}

	@Test
	public void testVariableDepth() throws Exception {
		for (int depth = 0; depth <= 3; depth++) {
			final FileSharder fs = makeDirs(exampleIDs, depth);
			assertEquals(exampleIDs, Utils.toArrayList(fs));
		}
	}

	@Test
	public void testErrorIfIDTooShort() throws Exception {
		final FileSharder fs = new FileSharder(tmp.newFolder().toPath(), 4);
		fs.getPath("11223344");

		try {
			fs.getPath("1122334");
			fail("Expected error");
		} catch (final IndexOutOfBoundsException e) {
			// success
		}
	}

}
