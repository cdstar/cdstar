package de.gwdg.cdstar.pool.nio;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.pool.BackendError;
import de.gwdg.cdstar.pool.PoolError;
import de.gwdg.cdstar.pool.PoolError.Conflict;
import de.gwdg.cdstar.pool.PoolError.NameConflict;
import de.gwdg.cdstar.pool.PoolError.StaleHandle;
import de.gwdg.cdstar.pool.Resource;
import de.gwdg.cdstar.pool.StorageObject;
import de.gwdg.cdstar.pool.nio.json.JsonIndex;
import de.gwdg.cdstar.pool.nio.json.JsonResource;
import de.gwdg.cdstar.ta.TAState;
import de.gwdg.cdstar.ta.TransactionHandle;
import de.gwdg.cdstar.ta.TransactionInfo.Mode;

public class NioObject implements StorageObject {
	static final Logger log = LoggerFactory.getLogger(NioObject.class);

	final NioPool pool;

	private final JsonIndex source;

	// Shallow copy of source to hold modified fields.
	private JsonIndex dest;

	private List<NioResource> resources;

	// Nullable (if direct-loaded)
	private final TransactionHandle tx;

	private boolean lockCreated = false;

	private volatile String nextRevision;

	private SymlinkLock lock;

	/**
	 * Create a brand new object.
	 */
	public NioObject(NioPool nioPool, TransactionHandle t, String id) {
		tx = t;
		pool = nioPool;
		source = dest = new JsonIndex();
		dest.id = id;
		dest.ctime = System.currentTimeMillis();
		dest.mtime = dest.ctime;
		dest.dtime = -1;
	}

	/**
	 * Materialize from an existing json structure.
	 */
	public NioObject(NioPool nioPool, TransactionHandle t, JsonIndex json) {
		tx = t;
		pool = nioPool;
		source = json;
	}

	private boolean isStale() {
		return tx == null || !tx.isOpen();
	}

	/**
	 * Throw a {@link StaleHandle} if the transaction is no longer open.
	 */
	void ensureNotStale() throws StaleHandle {
		if (isStale())
			throw new StaleHandle();
	}

	@Override
	public String getId() {
		return source.id;
	}

	@Override
	public String getRevision() {
		return source.rev;
	}

	@Override
	public String getParentRevision() {
		return source.parent;
	}

	@Override
	public synchronized String getNextRevision() {
		if (nextRevision == null) {
			do nextRevision = PoolUtils.getNextRandomRevision();
			while(Files.exists(pool.getObjectFile(getId(), nextRevision)));
		}
		return nextRevision;
	}

	@Override
	public boolean isModified() {
		return dest != null;
	}

	@Override
	public void touch() throws StaleHandle {
		ensureNotStale();
		if (dest == null)
			dest = source.shallowCopy();
		dest.mtime = System.currentTimeMillis();
	}

	@Override
	public String getType() {
		return dest == null ? source.type : dest.type;
	}

	@Override
	public void setType(String type) throws StaleHandle {
		touch();
		dest.type = type;
	}

	@Override
	public Date getCreated() {
		return new Date(source.ctime);
	}

	@Override
	public Date getLastModified() {
		return new Date(dest == null ? source.mtime : dest.mtime);
	}

	@Override
	public synchronized List<Resource> getResources() {
		if (resources == null) {
			if (source.resources == null)
				resources = new ArrayList<>(4);
			else {
				resources = new ArrayList<>(source.resources.size());
				source.resources.forEach(r -> resources.add(new NioResource(this, r)));
			}
		}
		return Collections.unmodifiableList(resources);
	}

	@Override
	public int getResourceCount() {
		if (resources != null)
			return resources.size();
		if (source.resources != null)
			return source.resources.size();
		return 0;
	}

	@Override
	public synchronized Resource createResource(String name) throws Conflict, StaleHandle, NameConflict {
		touch();
		getResources(); // Ensure this.resources is populated.

		final NioResource r = new NioResource(this);
		r.setName(name); // May fail with a NameConflict
		resources.add(r);

		return r;
	}

	Path getPath() {
		return pool.getObjectPath(getId());
	}

	@Override
	public void remove() throws Conflict, StaleHandle {
		if (isRemoved())
			return;
		touch();
		dest.dtime = System.currentTimeMillis();
	}

	@Override
	public boolean isRemoved() {
		return (dest == null ? source.dtime : dest.dtime) != -1;
	}

	synchronized void onPrepare(Instant now) throws IOException {
		assert tx.getState() == TAState.PREPARING;
		log.debug("PREPARE {} (tx: {})", getId(), tx.getId());

		/*
		 * Early exit for unmodified objects in SNAPSHOT mode. In this mode,
		 * other transactions are allowed to modify objects that were not
		 * modified by us (write-read conflicts are okay).
		 */
		if (tx.getMode() == Mode.SNAPSHOT && !isModified())
			return;

		final Path headLink = pool.getHEAD(getId());

		/*
		 * We need a global lock to this object to prevent other transactions or GC runs
		 * from modifying it while our transaction is still incomplete.
		 *
		 * As a lockfile, we create a HEAD_LOCK link that points to the (to be created)
		 * revision file. This link is renamed to HEAD during commit.
		 */
		lock = pool.lockObject(getId(), getNextRevision());

		/*
		 * Ensure that HEAD still points to what we think is the newest
		 * revision. In SERIALIZABILITY mode we have to do this even for
		 * unmodified objects to maintain strong transaction isolation
		 * guarantees.
		 */
		if (source.rev != null) {
			final Path oldHead = pool.getObjectFile(getId(), getRevision());
			if (!Files.isSameFile(oldHead, headLink))
				throw new PoolError.Conflict("HEAD changed");
		}

		// Not-so-early exit for unmodified objects in SERIALIZABILITY mode.
		if (!isModified())
			return;

		// Objects that were deleted in the same transaction they were created
		// can be rolled back.
		if (source.rev == null && isRemoved()) {
			onRollback();
			return;
		}

		// This object was modified. Make a new revision
		dest.parent = source.rev;
		dest.rev = getNextRevision();
		dest.mtime = now.toEpochMilli();

		// Prepare resources and re-build the json.resources list, if
		// necessary.
		if (resources != null) {
			dest.resources = new ArrayList<>(resources.size());
			for (final NioResource r : resources) {
				final JsonResource rjson = r.onPrepare();
				if (!r.isRemoved())
					dest.resources.add(rjson);
			}
		} else {
			dest.resources = source.resources;
		}

		if (dest.attr == null)
			dest.attr = source.attr;

		// Actually persist this object. This will fail on (unlikely) revision string conflicts.
		pool.write(dest);

		// Done :) Any error will trigger a rollback.
	}

	void onCommit() {
		assert tx.getState() == TAState.PREPARED;
		log.debug("COMMIT {} (tx: {})", getId(), tx.getId());

		final Path headLock = pool.getHEAD_NEXT(getId());

		// Not modified, nothing to commit. Just delete the lock file.
		if (!isModified()) {
			if (lockCreated)
				Utils.deleteQuietly(headLock);
			lockCreated = false;
			return;
		}

		// Special case: Object created and immediately deleted. It was rolled
		// back already in onPrepare()
		if (dest.rev == null && isRemoved()) {
			return;
		}

		// Rename HEAD_LOCK to HEAD.
		try {
			lock.commit();
		} catch (final Exception e) {
			// We are screwed. This is a HARD error. If a simple rename of a
			// symbolic link fails, there is something seriously wrong with the
			// file system and we should shut down everything!
			throw new BackendError("Failed to commit", e);
		}
	}

	void onRollback() {
		log.debug("ROLLBACK {} (tx: {})", getId(), tx.getId());

		// Cleanup all temporary or modified resources (if present)
		if (resources != null)
			resources.forEach(NioResource::onRollback);

		// Clean up after a failed or partial prepare. Most importantly: Delete
		// the Lockfile.
		if (lock != null) {
			lock.rollback();
			Utils.deleteQuietly(lock.getTarget());
			lockCreated = false;
		}

		// For objects created during this transaction, we should also delete
		// the directory. It should be empty by now. If not, keep it. We should
		// never delete stuff we did not create ourself.
		if (dest != null && (dest.rev == null || Utils.equal(nextRevision, dest.rev))) {
			// TODO: Also delete intermediate folders if they are empty.
			Utils.deleteQuietly(pool.getObjectPath(getId()));
		}
	}

	/**
	 * The current implementation leaves garbage files in the file system and
	 * only fixes HEAD and HEAD_LOCK. This allows manual analysis after a crash
	 * and is probably a lot less likely to fail and destroy valuable data
	 * during recovery. Crashes should be rare and the disk space taken by
	 * garbage files should be minimal.
	 */
	static void onRecover(NioPool nioPool, String id, String rev, boolean doCommit) throws IOException {
		final Path headLink = nioPool.getHEAD(id);
		final Path headLock = nioPool.getHEAD_NEXT(id);
		final Path revFile = nioPool.getObjectFile(id, rev);

		// No lock file. Object already recovered, or crash happened before it
		// was created. Nothing to do.
		if (!Files.exists(headLock, LinkOption.NOFOLLOW_LINKS))
			return;

		// Not our lock file. A different transaction is responsible for
		// recovering this object.
		if (!Files.isSameFile(headLock, revFile)) {
			return;
		}

		// Update HEAD if required, then remove lockfile.
		if (doCommit && Files.exists(revFile)) {
			Files.move(headLock, headLink, StandardCopyOption.REPLACE_EXISTING);
			log.info("Recovered {}: COMMIT", id);
		} else if (doCommit) {
			// This should never happen. If it happens, we cannot do anything
			// about it :(
			log.error("Recovery failed for {}: Revision file not found at '{}'", id, revFile);
		} else {
			Utils.deleteQuietly(headLock);
			log.info("Recovered {}: ROLLBACK", id);
		}
	}

	@Override
	public synchronized Set<String> getPropertyNames() {
		if (dest != null && dest.attr != null)
			return Collections.unmodifiableSet(dest.attr.keySet());
		else if (source.attr != null)
			return Collections.unmodifiableSet(source.attr.keySet());
		return Collections.emptySet();
	}

	@Override
	public synchronized String getProperty(String name) {
		if (dest != null && dest.attr != null)
			return dest.attr.get(name);
		else if (source.attr != null)
			return source.attr.get(name);
		return null;
	}

	@Override
	public synchronized void setProperty(String name, String value) {
		touch();
		if (dest.attr == null)
			dest.attr = source.attr == null ? new HashMap<>() : new HashMap<>(source.attr);
		if (value == null)
			dest.attr.remove(name);
		else
			dest.attr.put(name, value);
	}

}
