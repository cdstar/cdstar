package de.gwdg.cdstar.pool.nio;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.FileStore;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.StandardOpenOption;
import java.nio.file.attribute.BasicFileAttributes;
import java.time.Instant;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;
import com.github.benmanes.caffeine.cache.stats.CacheStats;

import de.gwdg.cdstar.MultiDigest;
import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.pool.BackendError;
import de.gwdg.cdstar.pool.BackendError.DamagedDataError;
import de.gwdg.cdstar.pool.PoolError;
import de.gwdg.cdstar.pool.PoolError.Conflict;
import de.gwdg.cdstar.pool.PoolError.ExternalResourceException;
import de.gwdg.cdstar.pool.PoolError.NotFound;
import de.gwdg.cdstar.pool.Resource;
import de.gwdg.cdstar.pool.StorageObject;
import de.gwdg.cdstar.pool.StoragePool;
import de.gwdg.cdstar.pool.StorageSession;
import de.gwdg.cdstar.pool.nio.json.JsonFormat;
import de.gwdg.cdstar.pool.nio.json.JsonFormat.FormatError;
import de.gwdg.cdstar.pool.nio.json.JsonIndex;
import de.gwdg.cdstar.pool.nio.json.JsonResource;
import de.gwdg.cdstar.runtime.Config;
import de.gwdg.cdstar.runtime.ConfigException;
import de.gwdg.cdstar.runtime.Plugin;
import de.gwdg.cdstar.ta.UserTransaction;

@Plugin
public class NioPool implements StoragePool {

	/**
	 * Path to the storage base directory (required)
	 */
	public static final String PROP_PATH = "path";
	/**
	 * (experimental) Cache size for the in-memory manifest cache.
	 */
	public static final String PROP_CACHE_SIZE = "cacheSize";
	/**
	 * (experimental) If enabled, trigger a TRIM operation after each successful
	 * commit.
	 */
	public static final String PROP_AUTOTRIM = "autotrim";
	/**
	 * (experimental) Comma separated list of digests to calculate for new files.
	 */
	public static final String PROP_DIGESTS = "digests";

	static final String EXT_BLOB = ".bin";
	static final String EXT_REVISION = ".json";
	static final String FILENAME_NEXT = "HEAD_NEXT";
	static final String FILENAME_HEAD = "HEAD";

	public static Logger log = LoggerFactory.getLogger(NioPool.class);
	public static final int SHARD_DEPTH = 2;
	private static JsonFormat objectLoader = new JsonFormat();

	final Path basePath;
	private int cacheSize = 16;

	Map<String, NioSession> sessions = new HashMap<>();

	private final FileSharder pathSharder;
	final NioRecoveryHandler recoveryHandler;
	private LoadingCache<Path, JsonIndex> indexCache;

	private ObjectTrimer trimmer;
	private boolean autotrimEnabled;
	private Set<String> digestNames;

	public NioPool(Path base) throws IOException {
		basePath = base;
		pathSharder = new FileSharder(base, SHARD_DEPTH);
		recoveryHandler = new NioRecoveryHandler(this);
		trimmer = new ObjectTrimer(this);
		digestNames = new HashSet<String>(Arrays.asList(Resource.DIGEST_NAMES));

		if (!Files.isDirectory(basePath))
			Files.createDirectories(basePath);
		rebuildCache();
	}

	public NioPool(Config cfg) throws IOException, ConfigException {
		this(Paths.get(cfg.get(PROP_PATH)));
		cfg.setDefault(PROP_CACHE_SIZE, Integer.toString(cacheSize));

		setCacheSize(cfg.getInt(PROP_CACHE_SIZE));
		setAutotrim(cfg.getBool(PROP_AUTOTRIM));
		if (cfg.hasKey(PROP_DIGESTS))
			setDigests(cfg.getList(PROP_DIGESTS));
	}

	public void setDigests(List<String> list) {
		Set<String> values = new HashSet<>(list);
		for(String name : values)
			if(!Arrays.asList(Resource.DIGEST_NAMES).contains(name))
				throw new PoolError("Unsupported digest name: " + name);
		values.add(Resource.DIGEST_SHA256);
		this.digestNames = Collections.unmodifiableSet(values);
	}

	@Override
	public Set<String> getDefaultDigests() {
		return digestNames; // unmodifiable
	}

	public synchronized void setAutotrim(boolean enable) {
		autotrimEnabled = enable;
	}

	public boolean isAutotrimEnabled() {
		return autotrimEnabled;
	}

	public void setCacheSize(int cacheSize) {
		this.cacheSize = cacheSize;
		rebuildCache();
	}

	private void rebuildCache() {
		indexCache = Caffeine.newBuilder()
				.softValues() // prevent OOM
				.maximumSize(cacheSize)
				.expireAfterAccess(60, TimeUnit.SECONDS)
				.recordStats()
				.build(this::loadFromDisk);
	}

	@Override
	public StorageSession open(UserTransaction tx) {
		synchronized (sessions) {
			return sessions.computeIfAbsent(tx.getId(), tid -> {
				final NioSession scope = new NioSession(this);
				tx.bind(scope);
				return scope;
			});
		}
	}

	void closeScope(NioSession scope, boolean wasComitted) {
		synchronized (sessions) {
			sessions.remove(scope.tx.getId());
		}

		if (isAutotrimEnabled() && wasComitted)
			scope.getModifiedObjectIDs().forEach(trimmer::trimLater);
	}

	Optional<Instant> getOldestScopeStartTime() {
		synchronized (sessions) {
			return sessions
					.values()
					.stream()
					.map(s -> s.tx.getStarted())
					.min(Comparator.naturalOrder());
		}
	}

	/**
	 * Find the path to the current HEAD.
	 */
	Path findHead(String objectId) {
		// TODO: Try to load HEAD multiple times.
		try {
			final Path path = getHEAD(objectId);
			return path.resolveSibling(Files.readSymbolicLink(path));
		} catch (final NoSuchFileException e) {
			throw new PoolError.NotFound(objectId);
		} catch (final IOException e) {
			throw new PoolError(e);
		}
	}

	/**
	 * Actually load a JsonIndex from disk.
	 */
	JsonIndex loadFromDisk(Path path) {
		try {
			return objectLoader.load(path);
		} catch (final FormatError e) {
			log.error("Damaged storage object: {}", path, e);
			throw new DamagedDataError("Object metadata damaged or incomplete.", e);
		} catch (final NoSuchFileException e) {
			throw new PoolError.NotFound(path.toString());
		} catch (final IOException e) {
			throw new PoolError(e);
		}
	}

	/**
	 * Load a specific object revision, or HEAD if the second parameter is null.
	 */
	JsonIndex load(String objectId, String revision) {
		Path path;

		if (revision == null) {
			path = findHead(objectId);
		} else {
			path = getObjectFile(objectId, revision);
		}

		return indexCache.get(path); // triggers loadFromDisk()
	}

	/**
	 * Write this object to disk, or fail if it already exists.
	 */
	void write(JsonIndex obj) throws IOException {
		try (final OutputStream stream = Files.newOutputStream(getObjectFile(obj.id, obj.rev),
				StandardOpenOption.CREATE_NEW);
				OutputStream buffered = new BufferedOutputStream(stream)) {
			objectLoader.write(buffered, obj);
		}
	}

	@Override
	public StorageObject loadObjectDirect(String objectId, String revision) throws NotFound {
		Path path;

		if (revision == null) {
			path = findHead(objectId);
		} else {
			path = getObjectFile(objectId, revision);
		}

		final JsonIndex index = loadFromDisk(path);
		return new NioObject(this, null, index);
	}

	@Override
	public long objectSize(String objectId) throws NotFound {
		if (!objectExists(objectId))
			return 0;
		try {
			final AtomicLong size = new AtomicLong(0);
			Files.walkFileTree(getObjectPath(objectId), new SimpleFileVisitor<Path>() {
				@Override
				public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) {
					size.addAndGet(attrs.size());
					return FileVisitResult.CONTINUE;
				}
			});
			return size.get();
		} catch (final IOException e) {
			throw new BackendError(e);
		}
	}

	@Override
	public boolean objectExists(String objectId) {
		return Files.exists(getHEAD(objectId));
	}

	@Override
	public Date lastModified(String objectId) throws NotFound {
		try {
			return new Date(Files.getLastModifiedTime(getHEAD(objectId)).toMillis());
		} catch (final IOException e) {
			throw new NotFound(objectId, e);
		}
	}

	@Override
	public Iterable<String> getObjectIDs() {
		return () -> pathSharder.iterator();
	}

	@Override
	public Iterable<String> getObjectIDs(String skipUntil) {
		return () -> pathSharder.iterator(skipUntil);
	}

	@Override
	public void validateObject(String objectId) throws NotFound, ExternalResourceException, DamagedDataError {
		final JsonIndex json = load(objectId, null);

		final NioObject obj = new NioObject(this, null, json);

		for (final Resource r : obj.getResources()) {
			final NioResource rs = (NioResource) r;
			final MultiDigest md = new MultiDigest(Resource.DIGEST_SHA256);
			try (ReadableByteChannel data = rs.getReadChannelInternal(0)) {
				md.update(Channels.newInputStream(data));
			} catch (final IOException e) {
				throw new DamagedDataError("Failed to read resource data", e);
			}

			if (md.totalBytes() != rs.getSize()) {
				final String msg = Utils.format("Size mismatch.\n  Expected: {}\n  Computed: {}",
						rs.getSize(), md.totalBytes());
				throw new DamagedDataError(msg);
			}

			final String expected = rs.getDigests().get(Resource.DIGEST_SHA256);
			final String computed = Utils.bytesToHex(md.digest().get(Resource.DIGEST_SHA256));
			if (!expected.equalsIgnoreCase(computed)) {
				final String msg = Utils.format(
						"Digest mismatch.\n  Algorithm. {}\n  Expected: {}\n  Computed: {}", Resource.DIGEST_SHA256,
						expected, computed);
				throw new DamagedDataError(msg);
			}
		}
	}

	@Override
	public long trimObject(String objectId) throws NotFound, Conflict {
		GCTask gc = trimmer.trim(objectId);
		return gc.getCollectedBytes();
	}

	Path getObjectPath(String id) {
		try {
			return pathSharder.getPath(id);
		} catch (InvalidPathException | IndexOutOfBoundsException e) {
			log.error("Tried to resolve invalid object ID: {}", id != null ? id : "'null'");
			throw new NotFound(id);
		}
	}

	Path getHEAD(String id) {
		return getObjectPath(id).resolve(FILENAME_HEAD);
	}

	Path getHEAD_NEXT(String id) {
		return getObjectPath(id).resolve(FILENAME_NEXT);
	}

	Path getObjectFile(String id, String revisionId) {
		return getObjectPath(id).resolve(revisionId + EXT_REVISION);
	}

	Path getResourceBlob(String id, JsonResource dest) {
		Objects.requireNonNull(dest.sha256);
		return getObjectPath(id).resolve(Utils.bytesToHex(dest.sha256) + EXT_BLOB);
	}

	/**
	 * Create the object directory, if it does not exist already.
	 *
	 * @param id Object ID
	 * @return true on success, false otherwise
	 */
	boolean makeObjectDirectory(String id) {
		if (!pathSharder.isValidId(id))
			throw new PoolError.InvalidObjectId("Invalid ID format.");
		final Path fullPath = getObjectPath(id);
		try {
			Files.createDirectories(fullPath.getParent());
			Files.createDirectory(fullPath);
			return true;
		} catch (final FileAlreadyExistsException e) {
			return false;
		} catch (final IOException e) {
			throw new BackendError(e);
		}
	}

	/**
	 * Lock an object globally for writing (reading allowed).
	 *
	 * @param id           The object ID
	 * @param nextRevision The target revision, or null if the lock is not actually
	 *                     creating a new revision.
	 * @throws PoolError.Conflict if object is already locked.
	 * @throws PoolError          if lockfile could not be created for other
	 *                            reasons.
	 */
	SymlinkLock lockObject(String id, String nextRevision) throws PoolError {
		return new SymlinkLock(getHEAD(id), getObjectFile(id, nextRevision),
				getHEAD_NEXT(id));
	}

	String randomObjectId() {
		return pathSharder.getRandomId();
	}

	/**
	 * Return true if you believe that two files with the same size and SHA-256 hash
	 * may actually have different content. Hint: As to date, there is not a single
	 * known SHA-256 collision, even for inputs of different size. A same-size
	 * collision is so astronomical unlikely, that enabling this really qualifies as
	 * paranoid.
	 */
	boolean iAmParanoid() {
		return false;
	}

	/**
	 * Return a snapshot of the current pool statistics
	 */
	public PoolStats getStats() {
		return new PoolStats();
	}

	public class PoolStats {
		private final CacheStats cacheStats;
		private long fsFree = -1;
		private long fsTotal = -1;
		private final long cacheSize;

		public PoolStats() {
			indexCache.cleanUp();
			cacheStats = indexCache.stats();
			cacheSize = indexCache.estimatedSize();
			try {
				final FileStore fileStore = Files.getFileStore(basePath.toRealPath());
				fsFree = fileStore.getUsableSpace();
				fsTotal = fileStore.getTotalSpace();
			} catch (final IOException e) {
				log.warn("Unable to read file system statistics", e);
			}
		}

		/**
		 * Size (bytes) of the backing file store
		 */
		public long diskTotal() {
			return fsTotal;
		}

		/**
		 * Free space (bytes) in the backing file store
		 */
		public long diskFree() {
			return fsFree;
		}

		/**
		 * Number of objects currently present in the cache
		 */
		public long cacheSize() {
			return cacheSize;
		}

		/**
		 * Total number of cache hits that prevented a disk read
		 */
		public long cacheHitCount() {
			return cacheStats.hitCount();
		}

		/**
		 * Total number of cache misses that triggered a disk read
		 */
		public long cacheMissCount() {
			return cacheStats.missCount();
		}

		/**
		 * Number of open sessions (transactions)
		 */
		public int sessionCount() {
			return sessions.size();
		}

	}

	@Override
	public String toString() {
		return "<NioPool " + basePath + ">";
	}

	@Override
	public void close() {
		Utils.closeQuietly(trimmer);
		// Warn if sessions were not terminated properly.
		sessions.values().forEach(session -> {
			if (session.tx.isOpen())
				log.warn("Storage pool {} closed while transactions still open: {}", this, session.tx);
		});
	}

}
