package de.gwdg.cdstar.pool.nio;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.stream.Collectors;

import de.gwdg.cdstar.MultiDigest;
import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.pool.PoolError.ExternalResourceException;
import de.gwdg.cdstar.pool.PoolError.StaleHandle;
import de.gwdg.cdstar.pool.Resource;

/**
 * A {@link WritableByteChannel} to append data to a {@link NioResource},
 * optionally copying bytes from the original source first.
 */
class ResouceWriteChannel implements WritableByteChannel {

	private final FileChannel channel;
	private final Path path;
	private MultiDigest rollingDigest;
	private Map<String, byte[]> digestCache;
	private long size;
	private Collection<String> digestAlgos;

	/**
	 * Create a write target by copying (part of) the original source.
	 *
	 * @param src         Source to copy data from. May be null if maxCopy is zero.
	 * @param dstPath     Path of the temporary file. Must not exist.
	 * @param maxCopy     Number of bytes to copy from the original source. MUST be
	 *                    a positive number, may be 0.
	 * @param digestAlgos Digests to compute on the fly.
	 *
	 * @throws IOException If the target file cannot be created, or copying fails.
	 */
	ResouceWriteChannel(ReadableByteChannel src, Path dstPath, long maxCopy, Collection<String> digests)
			throws IOException {
		path = dstPath;
		channel = FileChannel.open(path, StandardOpenOption.CREATE_NEW, StandardOpenOption.WRITE);
		if (maxCopy > 0)
			channel.transferFrom(src, 0, maxCopy);
		channel.position(channel.size());
		size = channel.position();
		digestAlgos = digests;
		if (size == 0)
			rollingDigest = new MultiDigest(digests);
	}

	/**
	 * Create a write channel from an existing temporary file with pre-computed hashes.
	 */
	ResouceWriteChannel(Path path, Map<String, String> digests) throws IOException {
		this.path = path;
		digestAlgos = digests.keySet();
		digestCache = digests.entrySet().stream()
				.collect(Collectors.toMap(e->e.getKey(), e-> Utils.hexToBytes(e.getValue())));
		channel = FileChannel.open(path, StandardOpenOption.WRITE);
		channel.position(channel.size());
		size = channel.position();
	}

	/**
	 * Re-open an existing write target, saving as much state (digests, rolling
	 * digests) as possible.
	 */
	ResouceWriteChannel(ResouceWriteChannel old, long maxCopy) throws IOException {
		old.close();
		path = old.path;
		digestAlgos = old.digestAlgos;
		channel = FileChannel.open(path, StandardOpenOption.WRITE);
		channel.position(channel.size());
		channel.truncate(maxCopy);
		size = channel.position();
		if (size == old.size) {
			digestCache = old.digestCache;
			rollingDigest = old.rollingDigest;
			old.rollingDigest = null;
		} else if (size == 0) {
			rollingDigest = new MultiDigest(digestAlgos);
		}
	}

	/**
	 * A potentially very efficient way to copy data from a {@link FileChannel} to
	 * this resource. Downside is we won't be able to compute digests.
	 */
	public synchronized long transferFrom(ReadableByteChannel src, long count) throws IOException {
		final long n = channel.transferFrom(src, size, count);
		if (n > 0) {
			digestCache = null;
			rollingDigest = null;
			size += n;
		}
		return n;
	}

	/**
	 * A specialized version of {@link #transferFrom(ReadableByteChannel, long)}
	 * that copies all bytes from an existing {@link Resource} and benefits from
	 * known digests.
	 * @throws IOException
	 * @throws ExternalResourceException
	 * @throws StaleHandle
	 */
	public synchronized void copyFrom(Resource other) throws StaleHandle, ExternalResourceException, IOException {

		if(other.getSize() == 0)
			return;

		long copied;
		try(ReadableByteChannel src = other.getReadChannel(0)) {
			copied = transferFrom(src, other.getSize());
			if(copied != other.getSize())
				throw new IOException("Unexpected EOF");
		}

		// If size does not match (e.g. because write channel was not empty) then digests are useless.
		if(copied != size)
			return;

		// Copy known digests
		assert digestCache == null && rollingDigest == null;
		digestCache = new HashMap<String, byte[]>();
		other.getDigests().forEach((name, value) -> digestCache.put(name, Utils.hexToBytes(value)));

		// Calculate missing digests
		HashSet<String> missing = new HashSet<>(digestAlgos);
		missing.removeAll(digestCache.keySet());
		if(!missing.isEmpty()) {
			NioResource.log.debug("Calculating partial digest after clone ({} bytes)", getSize());
			digestCache.putAll(new MultiDigest(missing).update(path).digest());
		}
	}

	long getSize() {
		return size;
	}

	Path getPath() {
		return path;
	}

	/**
	 * Return a map of digests, potentially triggering an expensive computation.
	 * This will only work on a closed channel.
	 */
	synchronized Map<String, byte[]> getDigests() throws IOException {
		if (isOpen())
			throw new IllegalStateException("Digests not available on opened write channel");
		if (digestCache == null) {
			if (rollingDigest == null) {
				NioResource.log.debug("Calculating full digest ({} bytes, non-rolling)", getSize());
				rollingDigest = new MultiDigest(digestAlgos);
				rollingDigest.update(path);
			}
			digestCache = rollingDigest.digest();
			rollingDigest = null;
		}
		return digestCache;
	}

	@Override
	public boolean isOpen() {
		return channel.isOpen();
	}

	@Override
	public void close() throws IOException {
		channel.close();
	}

	@Override
	public synchronized int write(ByteBuffer src) throws IOException {
		if (!src.hasRemaining())
			return 0;

		try {
			final int n;

			if (rollingDigest == null) {
				n = channel.write(src);
			} else {
				final ByteBuffer buffCopy = src.asReadOnlyBuffer();
				n = channel.write(src);
				if (n > 0) {
					buffCopy.limit(buffCopy.position() + n);
					rollingDigest.update(buffCopy);
				}
			}

			size += n;
			return n;
		} catch (final ClosedChannelException e) {
			throw e; // digests still valid, if present
		} catch (final IOException e) {
			rollingDigest = null;
			digestCache = null;
			Utils.closeQuietly(channel);
			Utils.deleteQuietly(path);
			throw e;
		}
	}

}
