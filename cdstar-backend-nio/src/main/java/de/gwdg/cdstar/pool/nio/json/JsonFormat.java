package de.gwdg.cdstar.pool.nio.json;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import com.fasterxml.jackson.core.JsonEncoding;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import de.gwdg.cdstar.Utils;

/**
 * Helper class to load and store {@link JsonIndex} structures to/from json
 * files.
 */
public class JsonFormat {

	static final ObjectMapper orm = new ObjectMapper();
	static final JsonFactory jsonFactory = orm.getFactory();
	/*
	 * No need to configure this ObjectMapper. Serialization features only affect
	 * object serialization, which is not used here. We only use the low-level
	 * JsonGenerator.
	 */

	private boolean pretty = true;

	/**
	 * Enable or disable the output intention for generated json. Performance or
	 * size impact is small, so this is enabled by default.
	 */
	public void setIdentOutput(boolean pretty) {
		this.pretty = pretty;
	}

	public JsonIndex load(byte[] input) throws FormatError, IOException {
		try (final JsonParser p = jsonFactory.createParser(input)) {
			return readIndexDocument(p);
		} catch (final FormatError e) {
			final JsonNode tree = orm.readTree(input);
			migrate(tree);
			return readIndexDocument(tree.traverse());
		}
	}

	public JsonIndex load(Path revFile) throws FormatError, IOException {
		try (final JsonParser p = jsonFactory.createParser(revFile.toFile())) {
			return readIndexDocument(p);
		} catch (final FormatError e) {
			final JsonNode tree = orm.readTree(revFile.toFile());
			migrate(tree);
			return readIndexDocument(tree.traverse());
		}
	}

	public void write(Path target, JsonIndex obj) throws IOException {
		try (final OutputStream stream = Files.newOutputStream(target)) {
			write(stream, obj);
		}
	}

	public void write(OutputStream target, JsonIndex obj) throws IOException {
		final JsonGenerator g = jsonFactory.createGenerator(target, JsonEncoding.UTF8);
		writeIndexDocument(g, obj);
		g.close();
	}

	public byte[] write(JsonIndex obj) throws IOException {
		final ByteArrayOutputStream stream = new ByteArrayOutputStream();
		write(stream, obj);
		return stream.toByteArray();
	}

	private JsonIndex readIndexDocument(JsonParser p) throws IOException, FormatError {
		final JsonIndex doc = new JsonIndex();

		assertNextToken(p, JsonToken.START_OBJECT);
		String fieldName;
		while ((fieldName = p.nextFieldName()) != null) {
			switch (fieldName) {
			case ("id"):
				doc.id = readText(p);
			break;
			case ("v"):
				doc.v = (int) readLong(p);
			if (doc.v != JsonIndex.CURRENT_FORMAT_V)
				throw new FormatVersionError("Migration required: " + p.getLongValue());
			break;
			case ("rev"):
				doc.rev = readText(p);
			break;
			case ("parent"):
				doc.parent = readText(p);
			break;
			case ("type"):
				doc.type = readText(p);
			break;
			case ("ctime"):
				doc.ctime = readLong(p);
			break;
			case ("mtime"):
				doc.mtime = readLong(p);
			break;
			case ("dtime"):
				doc.dtime = readLong(p);
			break;
			case ("resources"):
				assertNextToken(p, JsonToken.START_ARRAY);
			doc.resources = new ArrayList<>();
			while (p.nextToken() == JsonToken.START_OBJECT)
				doc.resources.add(readResourceEntry(p));
			assertCurrentToken(p, JsonToken.END_ARRAY);
			break;
			default:
				if (fieldName.startsWith("x-")) {
					if (doc.attr == null)
						doc.attr = new HashMap<>(4);
					doc.attr.put(fieldName.substring(2), readText(p));
				} else {
					throw new FormatError("Unknown property: " + fieldName, p);
				}
			}
		}

		if (doc.resources == null)
			doc.resources = Collections.emptyList();

		if (doc.id == null)
			throw new FormatError("Missing property: id", p);
		if (doc.rev == null)
			throw new FormatError("Missing property: rev", p);
		if (doc.ctime == 0)
			throw new FormatError("Missing property: ctime", p);
		if (doc.mtime == 0)
			throw new FormatError("Missing property: mtime", p);

		assertCurrentToken(p, JsonToken.END_OBJECT);

		compress(doc);

		return doc;
	}

	/**
	 * Try to reduce document size by deduplicating certain strings.
	 */
	private void compress(JsonIndex doc) {
		// Resources often share the same 'type' and 'enc' values. Frozen objects likely
		// have the same 'src' value for all resources. To speed things up and not
		// produce more garbage than we save, we only compare consecutive resource here.

		String lastType = null;
		String lastSrc = null;
		String lastEnc = null;
		for (final JsonResource res : doc.resources) {

			if (res.type != null) {
				if (res.type.equals(lastType))
					res.type = lastType;
				else
					lastType = res.type;
			}

			if (res.src != null) {
				if (res.src.equals(lastSrc))
					res.src = lastSrc;
				else
					lastSrc = res.src;
			}

			if (res.enc != null) {
				if (res.enc.equals(lastSrc))
					res.enc = lastEnc;
				else
					lastEnc = res.enc;
			}
		}
	}

	private JsonResource readResourceEntry(JsonParser p) throws IOException, FormatError {
		final JsonResource jr = new JsonResource();
		jr.size = -1;

		assert p.currentToken() == JsonToken.START_OBJECT;
		String fieldName;
		while ((fieldName = p.nextFieldName()) != null) {
			switch (fieldName) {
			case ("id"):
				jr.id = readText(p);
			break;
			case ("src"):
				jr.src = readText(p);
			break;
			case ("name"):
				jr.name = readText(p);
			break;
			case ("type"):
				jr.type = readText(p);
			break;
			case ("size"):
				jr.size = readLong(p);
			break;
			case ("ctime"):
				jr.ctime = readLong(p);
			break;
			case ("mtime"):
				jr.mtime = readLong(p);
			break;
			case ("md5"):
				jr.md5 = readBytes(p);
			break;
			case ("sha1"):
				jr.sha1 = readBytes(p);
			break;
			case ("sha256"):
				jr.sha256 = readBytes(p);
			break;
			default:
				if (fieldName.startsWith("x-")) {
					if (jr.attr == null)
						jr.attr = new HashMap<>(4);
					jr.attr.put(fieldName.substring(2), readText(p));
				} else {
					throw new FormatError("Unknown property: " + fieldName, p);
				}
			}
		}

		if (jr.id == null)
			throw new FormatError("Missing property: id", p);
		if (jr.size == -1)
			throw new FormatError("Missing property: size", p);
		if (jr.ctime == 0)
			throw new FormatError("Missing property: ctime", p);
		if (jr.mtime == 0)
			throw new FormatError("Missing property: mtime", p);

		return jr;
	}

	private String readText(JsonParser p) throws IOException, FormatError {
		assertNextToken(p, JsonToken.VALUE_STRING);
		return p.getText();
	}

	private long readLong(JsonParser p) throws IOException, FormatError {
		assertNextToken(p, JsonToken.VALUE_NUMBER_INT);
		return p.getLongValue();
	}

	private byte[] readBytes(JsonParser p) throws IOException, FormatError {
		assertNextToken(p, JsonToken.VALUE_STRING);
		return p.getBinaryValue();
	}

	/*
	 * Helper methods
	 */

	private void assertNextToken(JsonParser p, JsonToken t) throws IOException, FormatError {
		if (p.nextToken() != t) {
			throw new FormatError("Error in json document. Expected token " + t + " but got " + p.getCurrentToken(), p);
		}
	}

	private void assertCurrentToken(JsonParser p, JsonToken t) throws IOException, FormatError {
		if (p.currentToken() != t) {
			throw new FormatError("Error in json document: Expected token " + t, p);
		}
	}

	private void writeIndexDocument(JsonGenerator g, JsonIndex obj) throws IOException {
		if (pretty)
			g.useDefaultPrettyPrinter();

		g.writeStartObject();
		g.writeNumberField("v", JsonIndex.CURRENT_FORMAT_V);
		g.writeStringField("id", obj.id);
		g.writeStringField("rev", obj.rev);
		if (obj.parent != null)
			g.writeStringField("parent", obj.parent);
		if (obj.type != null)
			g.writeStringField("type", obj.type);
		g.writeNumberField("ctime", obj.ctime);
		g.writeNumberField("mtime", obj.mtime);
		if (obj.dtime > -1)
			g.writeNumberField("dtime", obj.dtime);

		writeXAttrFields(g, obj.attr);

		if (obj.resources != null && !obj.resources.isEmpty()) {
			g.writeArrayFieldStart("resources");
			for (final JsonResource r : obj.resources)
				writeResourceEntry(g, r);
			g.writeEndArray();
		}

		g.writeEndObject();
	}

	private void writeResourceEntry(JsonGenerator g, JsonResource r) throws IOException {
		g.writeStartObject();

		g.writeStringField("id", r.id);
		if (r.src != null)
			g.writeStringField("src", r.src);
		if (r.name != null)
			g.writeStringField("name", r.name);
		if (r.type != null)
			g.writeStringField("type", r.type);
		if (r.enc != null)
			g.writeStringField("enc", r.enc);

		g.writeNumberField("size", r.size);
		g.writeNumberField("ctime", r.ctime);
		g.writeNumberField("mtime", r.mtime);

		if (r.md5 != null)
			g.writeBinaryField("md5", r.md5);
		if (r.sha1 != null)
			g.writeBinaryField("sha1", r.sha1);
		if (r.sha256 != null)
			g.writeBinaryField("sha256", r.sha256);

		writeXAttrFields(g, r.attr);

		g.writeEndObject();

	}

	private void writeXAttrFields(JsonGenerator g, Map<String, String> attrs) throws IOException {
		if (attrs == null || attrs.isEmpty())
			return;
		for (final Entry<String, String> e : attrs.entrySet()) {
			final String value = e.getValue();
			if (value != null && !value.isEmpty())
				g.writeStringField("x-" + e.getKey(), value);
		}
	}

	private void migrate(JsonNode node) throws IOException {
		if (!node.isObject())
			throw new FormatError("Not a json object at all.");

		final ObjectNode tree = (ObjectNode) node;
		final int v = tree.path("v").asInt(0);

		switch (v) {
		case (0):
			migrate_0(tree);
		case (1):
			migrate_1(tree);
		case (2): // Not used.
		case (3):
			migrate_3(tree);
		case (JsonIndex.CURRENT_FORMAT_V):
			break;
		default:
			throw new FormatError("Object loading failed. Unsupported format revision: " + v);
		}

		tree.put("v", JsonIndex.CURRENT_FORMAT_V); // Bump to current version
		// after migration is
		// complete

	}

	private void migrate_0(ObjectNode src) throws FormatError {
		if (src.has("rev") && !src.has("revision")) {
			// This is actually a version 1 (early 3) file.
			return;
		}

		// TODO: Load CDStar v1 objects
		throw new FormatError("Legacy CDSTar v2 format not supported.");
	}

	/*
	 * The first on-disk format in CDStar 3.0 did not have a 'v' attribute, stored
	 * digests in a sub-object called 'digest', attributes in a sub-document called
	 * 'attr' and dates as ISO8601 instead of unix epoch.
	 *
	 * There is no need to officially support the old format, but some test
	 * installations already exist and this would allow a smooth transition. Support
	 * can be removed later.
	 */
	private void migrate_1(ObjectNode node) {
		node.put("ctime", Utils.fromIsoDate(node.get("ctime").textValue()).getTime());
		node.put("mtime", Utils.fromIsoDate(node.get("mtime").textValue()).getTime());
		if (node.has("dtime"))
			node.put("dtime", Utils.fromIsoDate(node.get("dtime").textValue()).getTime());

		if (node.has("attr"))
			for (final Entry<String, JsonNode> attr : Utils.iter(node.remove("attr").fields()))
				node.put("x-" + attr.getKey(), attr.getValue().asText());

		for (final JsonNode sub : node.path("resources")) {
			final ObjectNode res = (ObjectNode) sub;
			res.put("md5", sub.path("digest").path("MD5").textValue());
			res.put("sha1", sub.path("digest").path("SHA-1").textValue());
			res.put("sha256", sub.path("digest").path("SHA-256").textValue());
			res.remove("digest");
			res.put("ctime", Utils.fromIsoDate(res.get("ctime").textValue()).getTime());
			res.put("mtime", Utils.fromIsoDate(res.get("mtime").textValue()).getTime());
			if (res.has("attr"))
				for (final Entry<String, JsonNode> attr : Utils.iter(res.remove("attr").fields()))
					res.put("x-" + attr.getKey(), attr.getValue().asText());
		}
	}

	/**
	 * Version 4 changed the meaning of `resource.src`. Previously, it would contain
	 * the file name, or a prefixed external source. Now it should be null for the
	 * default case and start with 'x-' instead of 'external:' for application
	 * defined locations.
	 */
	private void migrate_3(ObjectNode node) throws FormatError {
		for (final JsonNode sub : node.path("resources")) {
			final ObjectNode res = (ObjectNode) sub;
			final String src = res.path("src").asText(null);

			if (src == null)
				continue;

			if (src.startsWith("external:")) {
				res.put("src", "x-" + src.substring("external:".length()));
			} else if (src.endsWith(".bin")
					&& src.equals(Utils.bytesToHex(Utils.base64decode(res.get("sha256").asText())) + ".bin")) {
				res.remove("src");
			} else {
				throw new FormatError("Unrecognized value for 'resource.src' in version 3 format: " + Utils.repr(src));
			}
		}
	}

	public static class FormatError extends IOException {
		private static final long serialVersionUID = 721474532848470263L;

		public FormatError(String msg, JsonParser p) {
			super(msg + "(" + p.getCurrentLocation().getLineNr() + ":" + p.getCurrentLocation().getColumnNr() + ")");
		}

		public FormatError(String msg) {
			super(msg);
		}
	}

	public static class FormatVersionError extends FormatError {
		private static final long serialVersionUID = -8057269398973537921L;

		public FormatVersionError(String msg) {
			super(msg);
		}
	}

}
