package de.gwdg.cdstar.pool.nio;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.time.Duration;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.pool.PoolError;
import de.gwdg.cdstar.pool.nio.json.JsonIndex;
import de.gwdg.cdstar.pool.nio.json.JsonResource;

/**
 * Perform garbage collection or 'trimming' on a single storage pool object.
 */
public class GCTask implements Runnable {
	private static final Logger log = LoggerFactory.getLogger(GCTask.class);
	private final NioPool pool;
	private final String id;
	private Instant keepNewerThan;
	private final boolean dryRun;
	private int collectedFiles;
	private long collectedBytes;

	private class ScanResult {
		String head;
		Set<Path> found = new HashSet<Path>();
		Set<Path> keep = new HashSet<Path>();
		boolean isDeadObject = false;

		boolean remember(Path path) {
			return found.add(path);
		}

		void keep(Path path) {
			if(keep.add(path))
				found.add(path);
		}
	}

	/**
	 *
	 * @param pool          The pool in which the object is stored.
	 * @param id            id of the object to collect garbage from.
	 * @param dryRun        if true, do not actually delete any files.
	 * @param keepNewerThan Keep revisions newer than this point in time. If null,
	 *                      do not keep revisions based on time.
	 */
	public GCTask(NioPool pool, String id, boolean dryRun, Instant keepNewerThan) {
		this.pool = pool;
		this.id = id;
		this.keepNewerThan = keepNewerThan;
		this.dryRun = dryRun;
	}

	@Override
	public synchronized void run() {

		try {
			Instant gcStarted = Instant.now();

			log.debug("GC started: {} (path:{})", id, pool.getObjectPath(id));

			ScanResult result = scan();

			if(log.isDebugEnabled())
				log.debug("GC scan: {} of {} files can be removed. Scan took {}ms",
						result.found.size() - result.keep.size(), result.found.size(),
						Duration.between(gcStarted, Instant.now()).toMillis());
			if(result.isDeadObject)
				log.info("GC Scan: Object is dead (all reachable revision are marked as deleted)");

			collect(result);

			log.info("GC completed: {} (files:{}, bytes:{})", id, collectedFiles, collectedBytes);
		} catch (final PoolError e) {
			throw e;
		} catch (final Exception e) {
			throw new PoolError("GC Failed", e);
		}
	}

	/**
	 * Scan the object directory, keep HEAD revision and all revisions that match
	 * {@link #keepNewerThan}. Keep BLOBs referenced from these revisions. Always
	 * keep HEAD, HEAD_NEXT and unknown files.
	 *
	 * If HEAD as deleted before keepNewerThan, then the entire object is removed.
	 */
	private ScanResult scan() throws IOException {
		ScanResult result = new ScanResult();
		JsonIndex head = pool.load(id, null);
		result.head = head.rev;

		if(head.dtime > -1) {
			if(keepNewerThan == null || Instant.ofEpochMilli(head.dtime).isBefore(keepNewerThan)) {
				result.isDeadObject = true;
			}
		}

		try (DirectoryStream<Path> ds = Files.newDirectoryStream(pool.getObjectPath(id))) {
			for (Path file : ds) {
				file = normalize(file);
				if (!result.remember(file))
					continue; // already seen

				final String fname = file.getFileName().toString();
				if (fname.endsWith(NioPool.EXT_REVISION)) {
					if(!result.isDeadObject)
						scanRevision(result, file);
				} else if (fname.endsWith(NioPool.EXT_BLOB)) {
					// BLOBs not added by scanRevision() are garbage.
				} else if (fname.equals(NioPool.FILENAME_HEAD) || fname.equals(NioPool.FILENAME_NEXT)) {
					// Keep HEAD or HEAD_NEXT, unless object is dead
					if(!result.isDeadObject)
						result.keep(file);
				} else if (fname.endsWith(".tmp") || fname.endsWith(".temp")) {
					// Always keep temporary files
					result.keep(file);
				} else {
					// Always keep unrecognized files
					log.warn("GC found unrecognized file: {}", file);
					result.keep(file);
				}
			}
		}

		return result;
	}

	/**
	 * Scan a revision file. If it matches HEAD or is newer than
	 * {@link #keepNewerThan}, add all referenced BLOBs to the keep-list.
	 */
	private void scanRevision(ScanResult result, Path file) throws IOException {
		final JsonIndex idx = pool.loadFromDisk(file);

		// Keep current head, or if revision is newer than our GC deadline
		boolean keep = idx.rev.equals(result.head)
				|| (keepNewerThan != null && Instant.ofEpochMilli(idx.mtime).isAfter(keepNewerThan));

		if (log.isTraceEnabled())
			log.trace("rev {} head {} mtime {} knt {} -> keep {}",
					idx.rev, result.head, Instant.ofEpochMilli(idx.mtime), keepNewerThan, keep);

		if (keep) {
			result.keep.add(file);

			if(idx.dtime < 0)
				result.isDeadObject = false;

			if (idx.resources != null) {
				for (final JsonResource r : idx.resources) {
					if (r.size == 0)
						continue;
					if (NioResource.isExternal(r))
						continue;
					Path blobFile = normalize(pool.getResourceBlob(id, r));
					result.keep.add(blobFile);
				}
			}
		}

	}

	/**
	 * Lock the object, ensure that the scan-time head is still valid, then remove
	 * all files found during scan but not in keep-list.
	 */
	private void collect(ScanResult result) {

		SymlinkLock lock = pool.lockObject(id, result.head);

		try {
			// Check head revision after lock was created
			if (!result.head.equals(pool.load(id, null).rev))
				throw new PoolError.Conflict("Head revision changed");

			for (final Path file : result.found) {
				if (result.keep.contains(file)) {
					log.debug("Keeping: {}", file);
					continue;
				}

				try {
					collectedBytes += collectFile(file);
					collectedFiles += 1;
				} catch (IOException e) {
					log.warn("Failed to remove file: {}", file, e);
				}
			}

			if(result.isDeadObject && result.keep.size() > 0)
				log.warn("Object removed but stale files left in directory. Example: {}", Utils.first(result.keep));

		} finally {
			lock.rollback();
		}

	}

	private long collectFile(final Path file) throws IOException {
		final long size = Files.isRegularFile(file, LinkOption.NOFOLLOW_LINKS) ? Files.size(file) : 0;

		if (dryRun) {
			log.info("Collecting (dry run): {} ({} bytes)", file, size);
		} else {
			log.info("Collecting: {} ({} bytes)", file, size);
			Files.delete(file);
		}
		return size;
	}

	public int getCollectedFiles() {
		return collectedFiles;
	}

	public long getCollectedBytes() {
		return collectedBytes;
	}

	private Path normalize(Path path) throws IOException {
		return path.toRealPath(LinkOption.NOFOLLOW_LINKS);
	}

}
