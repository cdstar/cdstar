package de.gwdg.cdstar.pool.nio.json;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public final class JsonResource {
	public String id; // Unique tracking ID that never changes
	public String src; // Null or prefixed URI
	public String name; // Given name for this file
	public String type; // Content-Type compatible mime type
	public String enc; // Content-Encoding compatible encoding name. May be
	// null if no encoding is applied.
	public long size; // Size in bytes of the (encoded) binary stream.
	public long ctime;
	public long mtime;

	public byte[] md5;
	public byte[] sha1;
	public byte[] sha256;

	public Map<String, String> attr;

	public JsonResource copy() {
		final JsonResource copy = new JsonResource();
		copy.id = id;
		copy.src = src;
		copy.name = name;
		copy.type = type;
		copy.enc = enc;
		copy.size = size;
		copy.ctime = ctime;
		copy.mtime = mtime;
		copy.md5 = md5;
		copy.sha1 = sha1;
		copy.sha256 = sha256;
		if (attr != null)
			copy.attr = new HashMap<>(attr);

		return copy;
	}

}