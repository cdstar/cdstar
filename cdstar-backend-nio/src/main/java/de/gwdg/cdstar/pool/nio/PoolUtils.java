package de.gwdg.cdstar.pool.nio;

import java.nio.file.Path;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.time.Instant;

import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.pool.Resource;

class PoolUtils {
	static final SecureRandom random = new SecureRandom();

	static byte[] NULL_SHA1, NULL_SHA256, NULL_MD5;

	static {
		try {
			NULL_SHA256 = MessageDigest.getInstance(Resource.DIGEST_SHA256).digest();
			NULL_SHA1 = MessageDigest.getInstance(Resource.DIGEST_SHA1).digest();
			NULL_MD5 = MessageDigest.getInstance(Resource.DIGEST_MD5).digest();
		} catch (NoSuchAlgorithmException e) {
			throw Utils.wtf(e);
		}
	}

	/**
	 * Return a random resource id (64 bit, as hex).
	 *
	 * Can be relatively small because collisions are checked.
	 */
	static String getNextResourceId() {
		return randomString("0123456789abcdef", 16);
	}

	/**
	 * Return a random revision id. (64 bit, as hex)
	 *
	 * Can be relatively small because collisions will gracefully fail at
	 * prepare-time.
	 */
	static String getNextRandomRevision() {
		return randomString("0123456789abcdef", 16);
	}

	/**
	 * Return a temporary file name, which is extremely unlikely (but not checked)
	 * to already exist.
	 */
	static Path getTempPathIn(NioObject obj, String prefix) {
		return obj.getPath().resolve("t." + prefix + "." + randomHexString(32) + ".tmp");
	}

	static String randomHexString(int size) {
		return randomString("0123456789abcdef", size);
	}

	static boolean isLowercaseHexString(String str) {
		for (int i = 0; i < str.length(); i++) {
			final char c = str.charAt(i);
			if (c < '0' || c > 'f' || (c > '9' && c < 'a'))
				return false;
		}
		return true;
	}

	static String randomString(String alphabet, int size) {
		final char[] result = new char[size];
		final int alen = alphabet.length();
		for (int i = 0; i < size; i++) {
			result[i] = alphabet.charAt(random.nextInt(alen));
		}
		return new String(result);
	}

	/**
	 * Return {@link Instant#now()} but ensure that each return value is at least
	 * one millisecond apart from the last one returned, so that their exact ISO
	 * representations are different.
	 *
	 * This implementation blocks until a new value can be returned.
	 *
	 * TODO: Remove this, once we stop using timestamps to identify the correct pool
	 * state for a transaction.
	 */
	private static volatile long lastEpochInstant;
	private static Object instantLock = new Object();

	static Instant getUniqueInstant() {
		Instant now = Instant.now();
		long epoch = now.toEpochMilli();

		synchronized (instantLock) {
			while (epoch <= lastEpochInstant) {
				Utils.sleepInterruptable(1);
				now = Instant.now();
				epoch = now.toEpochMilli();
			}
			lastEpochInstant = epoch;
			return now;
		}
	}
}
