package de.gwdg.cdstar.pool.nio;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.SocketChannel;
import java.nio.channels.WritableByteChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.gwdg.cdstar.MultiDigest;
import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.pool.BackendError;
import de.gwdg.cdstar.pool.PoolError;
import de.gwdg.cdstar.pool.PoolError.ExternalResourceException;
import de.gwdg.cdstar.pool.PoolError.NameConflict;
import de.gwdg.cdstar.pool.PoolError.StaleHandle;
import de.gwdg.cdstar.pool.PoolError.WriteChannelNotClosed;
import de.gwdg.cdstar.pool.Resource;
import de.gwdg.cdstar.pool.StorageObject;
import de.gwdg.cdstar.pool.nio.json.JsonResource;
import de.gwdg.cdstar.pool.nio.util.ZeroByteChannel;

/**
 *
 * <h2>Implementation details:</h2>
 *
 * Resources are stored in files named after their SHA-256 hash. Committed files
 * cannot be modified, so the first modifying operation will create a copy in a
 * temporary file to work with. During prepare phase, this file is renamed to
 * match its SHA-256 hash, or removed if such a file already exists. Hash
 * collision are considered extremely unlikely and not handled. A rollback
 * simply removes the temporary file.
 *
 * Cloning a resource is optimized by hard-linking the original blob file to a
 * (read-only) temporary file in the target object and copying the already known
 * digests. During prepare, this file is simply renamed or removed if the target
 * exists. Hard-linking works only because blob files are considered immutable.
 *
 * Empty resources do not need a file on the file system and are removed during
 * prepare phase, or never created in the first place. A call to
 * {@link #getStream()} will return an empty {@link InputStream} and
 * never throw @link {@link ExternalResourceException} for empty resources.
 *
 * Calculating digests is a very expensive operation and only happens on demand,
 * which is during prepare phase or if the user explicitly calls
 * {@link #getDigests()} after a write operation. The most common case
 * (create/replace a resource completely) is optimized: The first write to an
 * empty resource creates a rolling {@link MultiDigest}, which is updated
 * in-place on each write. This instance can only be used once to calculate
 * digests and is destroyed after use. Moving backwards in the file (via
 * {@link #truncate(long)}) will also destroy the {@link MultiDigest}. To
 * benefit from this optimization, truncate non-empty resources to 0 before
 * writing and never call {@link #getDigests()} or {@link #truncate(long)} in
 * between writes.
 *
 */
public class NioResource implements Resource {

	static final Logger log = LoggerFactory.getLogger(NioResource.class);

	public static final String SRC_PREFIX = "x-";
	private static final String DEFAULT_MEDIA_TYPE = "application/octet-stream";

	final NioObject obj;

	final JsonResource source;
	JsonResource dest;

	private List<ReadableByteChannel> readChannels;
	private ResouceWriteChannel writeChannel;
	private RestoreChannel restoreChannel;
	// If set, then this is a temp-file hard-linking to a different BLOB.
	private Path clonedBlob;


	public NioResource(NioObject nioObject, JsonResource jres) {
		Utils.notNull(nioObject, jres);
		obj = nioObject;
		source = jres;
	}

	public NioResource(NioObject nioObject) {
		Utils.notNull(nioObject);
		obj = nioObject;
		source = new JsonResource();

		do
			source.id = PoolUtils.getNextResourceId();
		while (nioObject.getResourceByID(source.id) != null);

		source.ctime = System.currentTimeMillis();
		source.mtime = source.ctime;
		source.type = DEFAULT_MEDIA_TYPE;

		source.size = 0;
		source.sha256 = PoolUtils.NULL_SHA256;
		source.sha1 = PoolUtils.NULL_SHA1;
		source.md5 = PoolUtils.NULL_MD5;

		beforeModify();
	}

	private void beforeModify() {
		if (dest == null) {
			obj.ensureNotStale();
			dest = source.copy();
		}
		obj.touch();
	}

	@Override
	public StorageObject getObject() {
		return obj;
	}

	@Override
	public String getId() {
		return source.id;
	}

	@Override
	public String getName() {
		return dest != null ? dest.name : source.name;
	}

	@Override
	public void setName(String name) throws StaleHandle, NameConflict {
		beforeModify();

		if (name == null) {
			dest.name = null;
			return;
		}

		name = Utils.normalizeNFKC(name);

		// Renames must be checked for conflicts:
		synchronized (obj) {
			for (final Resource r : obj.getResources()) {
				if (name.equals(r.getName())) {
					throw new NameConflict(name);
				}
			}
			dest.name = name;
		}
	}

	@Override
	public String getMediaType() {
		return dest != null ? dest.type : source.type;
	}

	@Override
	public String getContentEncoding() {
		return dest != null ? dest.enc : source.enc;
	}

	@Override
	public void setMediaType(String type, String coding) throws StaleHandle {
		beforeModify();
		dest.type = type;
		dest.enc = coding;
	}

	@Override
	public long getSize() {
		if (writeChannel != null)
			return writeChannel.getSize();
		return dest != null ? dest.size : source.size;
	}

	@Override
	public Date getCreated() {
		return new Date(source.ctime);
	}

	@Override
	public Date getLastModified() {
		return new Date(dest != null ? dest.mtime : source.mtime);
	}

	@Override
	public Map<String, String> getDigests() {

		if(isWriteInProgress())
			throw new WriteChannelNotClosed("Digests not available as long as a write channel is open.");

		if (writeChannel != null) {
			// Modified resource. get digests from (closed) write channel.
			try {
				Map<String, String> results = new HashMap<>(3);
				writeChannel.getDigests().forEach((k, v) -> results.put(k, Utils.bytesToHex(v)));
				return results;
			} catch (IOException e) {
				throw new BackendError("Digest computation failed", e);
			}
		}

		// Unmodified (or new and empty, or cloned) resource.
		Map<String, String> results = new HashMap<>(3);
		final JsonResource entry = dest != null ? dest : source;
		if (entry.sha256 != null)
			results.put(DIGEST_SHA256, Utils.bytesToHex(entry.sha256));
		if (entry.sha1 != null)
			results.put(DIGEST_SHA1, Utils.bytesToHex(entry.sha1));
		if (entry.md5 != null)
			results.put(DIGEST_MD5, Utils.bytesToHex(entry.md5));
		return results;

	}

	@Override
	public boolean isExternal() {
		return isExternal(dest != null ? dest : source);
	}

	static boolean isExternal(JsonResource json) {
		return json.src != null && json.src.startsWith(SRC_PREFIX);
	}

	@Override
	public void setExternalLocation(String hint) {
		if (Utils.nullOrEmpty(hint))
			throw new IllegalArgumentException("Hint must be a non-empty string");
		beforeModify();
		dest.src = SRC_PREFIX + hint;
	}

	@Override
	public String getExternalLocation() {
		final String src = dest != null ? dest.src : source.src;
		if (src != null && src.startsWith(SRC_PREFIX))
			return src.substring(SRC_PREFIX.length());
		return null;
	}

	@Override
	public WritableByteChannel getRestoreChannel() throws StaleHandle, IOException {
		beforeModify();

		if (!isExternal())
			throw new IllegalStateException("Resource is not external");

		if (!getDigests().containsKey(DIGEST_SHA256))
			throw new IllegalStateException("Required digest of original resource not available: SHA-256");

		Path path = PoolUtils.getTempPathIn(obj, "restore");
		restoreChannel = new RestoreChannel(path, getSize(), DIGEST_SHA256, this::finishRestore);
		return restoreChannel;
	}

	/**
	 * Triggered by the {@link #getRestoreChannel()} on close.
	 */
	private synchronized void finishRestore(RestoreChannel rchannel) throws IOException {
		if (!isExternal())
			throw new IllegalStateException("Resource is not external");

		// Compare size
		if (Files.size(rchannel.getPath()) != getSize())
			throw new IOException("Resource size mismatch");

		// Compare hash
		String hash = Utils.bytesToHex(rchannel.getDigest().digest());
		String expected = getDigests().get(DIGEST_SHA256);
		if (!hash.equalsIgnoreCase(expected)) {
			throw new IOException("Digest mismatch: Expected " + expected
					+ " but got " + hash + " (" + DIGEST_SHA256 + ")");
		}

		// Done. We can use the temp file as the new write target. It will be
		// moved to the correct location during prepare.
		if (writeChannel != null) {
			Utils.closeQuietly(writeChannel);
			Utils.deleteQuietly(writeChannel.getPath());
		}
		writeChannel = new ResouceWriteChannel(rchannel.getPath(), getDigests());
		writeChannel.close();

		// Finally, remove external location marker.
		restoreChannel = null;
		dest.src = null;
	}

	@Override
	public ReadableByteChannel getReadChannel(long skip) throws StaleHandle, ExternalResourceException, IOException {
		obj.ensureNotStale();
		return getReadChannelInternal(skip);
	}

	/**
	 * Return a read channel, even if the transaction is stale.
	 */
	@SuppressWarnings("resource")
	synchronized ReadableByteChannel getReadChannelInternal(long skip)
			throws ExternalResourceException, IOException {

		if (isExternal())
			throw new ExternalResourceException();

		if (isWriteInProgress())
			throw new WriteChannelNotClosed("Resource opened for writing");

		final ReadableByteChannel readChannel;

		try {
			if (getSize() == 0) {
				readChannel = new ZeroByteChannel();
			} else if (writeChannel != null) {
				readChannel = FileChannel.open(writeChannel.getPath()).position(skip);
			} else if (clonedBlob != null) {
				readChannel = FileChannel.open(clonedBlob).position(skip);
			} else {
				// TODO: Add support for packs (zip://b00b5.zip!file.bin)
				// TODO: Add support for urls (http://s3.example.com/file.bin)
				readChannel = FileChannel.open(obj.pool.getResourceBlob(obj.getId(), source)).position(skip);
			}
		} catch (final FileNotFoundException e) {
			log.error("Failed to read resource file for: {}:{}", obj.getId(), getName(), e);
			throw new BackendError("Failed to open resource file for reading.", e);
		}

		if (readChannels == null)
			readChannels = new ArrayList<>(1);
		readChannels.add(readChannel);
		readChannels.removeIf(ch -> !ch.isOpen());

		return readChannel;
	}

	@Override
	public synchronized boolean isWriteInProgress() {
		return (restoreChannel != null && restoreChannel.isOpen())
				|| writeChannel != null && writeChannel.isOpen();
	}

	@Override
	public synchronized WritableByteChannel getWriteChannel(long truncateTo)
			throws StaleHandle, ExternalResourceException, IOException {
		obj.ensureNotStale();

		if (isExternal())
			throw new ExternalResourceException();

		beforeModify();
		dest.mtime = System.currentTimeMillis();

		closeReadChannels();

		if (truncateTo < 0 || truncateTo > getSize())
			truncateTo = getSize();

		// Temp file already exists, just re-open existing write channel.
		if (writeChannel != null) {
			writeChannel.close();
			writeChannel = new ResouceWriteChannel(writeChannel, truncateTo);
			return writeChannel;
		}

		// Create new temp file
		final Path tmp = PoolUtils.getTempPathIn(obj, "write");
		try (ReadableByteChannel src = getReadChannelInternal(0)) {
			writeChannel = new ResouceWriteChannel(src, tmp, truncateTo, obj.pool.getDefaultDigests());
		} catch (final IOException e) {
			Utils.closeQuietly(writeChannel);
			Utils.deleteQuietly(tmp);
			writeChannel = null;
			throw new BackendError(e);
		}

		// Cloned BLOB was already copied for writing and is now obsolete.
		if (clonedBlob != null) {
			Utils.deleteQuietly(clonedBlob);
			clonedBlob = null;
		}

		return writeChannel;
	}

	private void closeReadChannels() {
		if (readChannels != null) {
			readChannels.forEach(Utils::closeQuietly);
			readChannels = null;
		}
	}

	@Override
	public long transferFrom(ReadableByteChannel src, long truncateTo, long count)
			throws StaleHandle, ExternalResourceException, IOException {
		// Direct transfer will clear rolling digests, so only do it if there is a
		// clear benefit.
		if (src instanceof FileChannel || src instanceof SocketChannel) {
			try (ResouceWriteChannel target = (ResouceWriteChannel) getWriteChannel(truncateTo)) {
				return target.transferFrom(src, count);
			}
		}
		return Resource.super.transferFrom(src, truncateTo, count);
	}

	@Override
	public synchronized void cloneFrom(Resource other) throws StaleHandle, ExternalResourceException, IOException {
		beforeModify();

		synchronized (other) {
			// Skip cloning for empty resources
			if (other.getSize() == 0) {
				// TODO: No need for a write channel.
				getWriteChannel(0).close();
				return;
			}

			// Try to create a cheap clone by hard-linking original source.
			if (other instanceof NioResource && tryCloneFrom((NioResource) other))
				return;

			// Fallback to byte copy. We still may be able to save some digest calculation.
			try (ResouceWriteChannel target = (ResouceWriteChannel) getWriteChannel(0)) {
				target.copyFrom(other);
			}
		}
	}

	/**
	 * Try to hard-link from an existing resource, if possible. Update
	 * digests in {@link #dest}, create {@link #clonedBlob} and return true on
	 * success.
	 *
	 * This only works if the source resource is unmodified and backed by an
	 * (immutable) blob file stored on the same file system. If the source file is
	 * not immutable or hard-linking is not possible, simply return false.
	 */
	private boolean tryCloneFrom(NioResource src) {
		if (src.writeChannel != null)
			return false; // We cannot clone from mutable files
		if (src.getSize() == 0)
			return false; // No file to hard-link to

		Path srcFile = src.clonedBlob;
		if (srcFile == null)
			srcFile = obj.pool.getResourceBlob(src.obj.getId(), src.source);

		final Path tmp = PoolUtils.getTempPathIn(obj, "clone");
		try {
			Files.createLink(tmp, srcFile.toRealPath());
		} catch (IOException | UnsupportedOperationException | SecurityException e) {
			// Hard linking may fail if the file-system does not support it, the process is
			// not allowed to create hard-links, source and target are on different
			// file-systems, or if the target does not exists. The last case may be a race
			// condition with a GC.
			log.info("Hardlinking {} with {} failed. Fallback to copy.", tmp, srcFile, e);
			Utils.deleteQuietly(tmp);
			return false;
		}

		// Hard-linking succeeded. Copy known state.
		JsonResource entry = src.dest != null ? src.dest : src.source;
		dest.md5 = entry.md5;
		dest.sha1 = entry.sha1;
		dest.sha256 = entry.sha256;
		dest.size = entry.size;
		clonedBlob = tmp;

		// Close now obsolete write channel, if any
		if (writeChannel != null) {
			Utils.closeQuietly(writeChannel);
			Utils.deleteQuietly(writeChannel.getPath());
			writeChannel = null;
		}

		// Calculate missing digests
		Set<String> missing = new HashSet<>(obj.pool.getDefaultDigests());
		missing.removeAll(src.getDigests().keySet());
		if (!missing.isEmpty()) {
			try (ReadableByteChannel rbc = getReadChannelInternal(0)) {
				Map<String, byte[]> digests = new MultiDigest(missing).update(Channels.newInputStream(rbc)).digest();
				dest.sha256 = digests.getOrDefault(DIGEST_SHA256, dest.sha256);
				dest.sha1 = digests.getOrDefault(DIGEST_SHA1, dest.sha1);
				dest.md5 = digests.getOrDefault(DIGEST_MD5, dest.md5);
			} catch (Exception e) {
				// Source not readable? Then the clone cannot be trusted -> panic
				throw new PoolError("Failed to calculate digests after cloning resource", e);
			}
		}

		return true;
	}

	@Override
	public void remove() throws StaleHandle, ExternalResourceException {
		setName(null);
	}

	boolean isRemoved() {
		return getName() == null;
	}

	synchronized JsonResource onPrepare() throws IOException {

		if (readChannels != null)
			readChannels.forEach(Utils::closeQuietly);

		// Resource not modified
		if (dest == null)
			return source;

		if(isWriteInProgress())
			throw new PoolError.WriteChannelNotClosed("Incomplete write to this resource");

		Path tempFile;
		if (writeChannel != null) {
			dest.mtime = System.currentTimeMillis();
			dest.size = writeChannel.getSize();
			Map<String, byte[]> digests = writeChannel.getDigests();
			dest.md5 = digests.get(DIGEST_MD5);
			dest.sha1 = digests.get(DIGEST_SHA1);
			dest.sha256 = digests.get(DIGEST_SHA256);
			tempFile = writeChannel.getPath();
		} else if (clonedBlob != null) {
			dest.mtime = System.currentTimeMillis();
			// hashes and size are already up to date.
			tempFile = clonedBlob;
		} else {
			// Data not modified
			return dest;
		}

		// Blob file not required? Remove temp file and exit
		if (isRemoved() || isExternal() || dest.size == 0) {
			Utils.deleteQuietly(tempFile);
			return dest;
		}

		// Persist temp file to final destination (based on sha256 hash)
		final Path targetFile = obj.pool.getResourceBlob(obj.getId(), dest);

		if (Files.exists(targetFile)) {
			// Blob with same hash exists. Check if we have a hash collision.
			if (Files.size(tempFile) != Files.size(targetFile) ||
					(obj.pool.iAmParanoid() && !FileUtils.contentEquals(targetFile.toFile(), tempFile.toFile()))) {
				log.error("Hash collision: {} {} (SHA256={})",
						tempFile, targetFile, Utils.bytesToHex(dest.sha256));
				throw new BackendError("SHA-256 hash collision!");
			}
			// No collision. Just keep the existing blob.
			Utils.deleteQuietly(tempFile);
		} else {
			Files.move(tempFile, targetFile, StandardCopyOption.ATOMIC_MOVE);
		}

		return dest;
	}

	synchronized void onRollback() {
		if (readChannels != null)
			readChannels.forEach(Utils::closeQuietly);
		if(clonedBlob != null)
			Utils.deleteQuietly(clonedBlob);
		if (writeChannel != null) {
			Utils.closeQuietly(writeChannel);
			Utils.deleteQuietly(writeChannel.getPath());
		}
	}

	@Override
	public synchronized Set<String> getPropertyNames() {
		if (dest != null && dest.attr != null)
			return Collections.unmodifiableSet(dest.attr.keySet());
		else if (source.attr != null)
			return Collections.unmodifiableSet(source.attr.keySet());
		return Collections.emptySet();
	}

	@Override
	public synchronized String getProperty(String name) {
		if (dest != null && dest.attr != null)
			return dest.attr.get(name);
		else if (source.attr != null)
			return source.attr.get(name);
		return null;
	}

	@Override
	public synchronized void setProperty(String name, String value) {
		beforeModify();
		if (dest.attr == null)
			dest.attr = source.attr == null ? new HashMap<>() : new HashMap<>(source.attr);
		if (value == null)
			dest.attr.remove(name);
		else
			dest.attr.put(name, value);
	}

}
