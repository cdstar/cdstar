package de.gwdg.cdstar.pool.nio;

import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;

import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.pool.PoolError;

/**
 * Objects are globally locked for writing by creating a HEAD_NEXT symlink that
 * should point to the file which eventually would become the new HEAD.
 *
 * On commit, the HEAD_NEXT symlink is renamed to HEAD, overwriting the previous
 * HEAD link. On rollback, the HEAD_NEXT links is simply removed.
 *
 * File creation MUST be exclusive and atomic, even on network file systems, for
 * this to work properly.
 *
 * Whoever holds the lock MUST also release it, otherwise the file stays locked.
 */
class SymlinkLock {
	private final Path head;
	private Path lock;
	private final Path target;

	SymlinkLock(Path headLink, Path headTarget, Path lockFile) {
		head = headLink;
		lock = lockFile;
		target = headTarget;

		try {
			Utils.relativeSymlink(lockFile, headTarget);
		} catch (final FileAlreadyExistsException e) {
			throw new PoolError.Conflict("Object locked");
		} catch (IOException e) {
			throw new PoolError("Object cannot be locked", e);
		}
	}

	public synchronized boolean isLocked() {
		return lock != null;
	}

	/**
	 * Unlock the object and make sure HEAD points to the target this lock was
	 * created with.
	 *
	 * @throws IOException
	 *             if the lock could not be comitted.
	 */
	public synchronized void commit() throws IOException {
		Files.move(lock, head, StandardCopyOption.REPLACE_EXISTING);
		lock = null;
	}

	/**
	 * Unlock the object by simply removing the lock. HEASD is not updated.
	 */
	public synchronized void rollback() {
		Utils.deleteQuietly(lock);
		lock = null;
	}

	public Path getTarget() {
		return target;
	}

}
