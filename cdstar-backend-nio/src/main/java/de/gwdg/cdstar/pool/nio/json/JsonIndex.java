package de.gwdg.cdstar.pool.nio.json;

import java.util.List;
import java.util.Map;

public final class JsonIndex {
	public static final int CURRENT_FORMAT_V = 4;

	public int v = CURRENT_FORMAT_V;
	public String id;
	public String rev;
	public String parent;
	public String type;
	public long ctime;
	public long mtime;
	public long dtime = -1;
	public Map<String, String> attr;
	public List<JsonResource> resources;

	public JsonIndex shallowCopy() {
		final JsonIndex copy = new JsonIndex();
		copy.id = id;
		copy.rev = rev;
		copy.parent = parent;
		copy.type = type;
		copy.ctime = ctime;
		copy.mtime = mtime;
		copy.dtime = dtime;
		return copy;
	}
}