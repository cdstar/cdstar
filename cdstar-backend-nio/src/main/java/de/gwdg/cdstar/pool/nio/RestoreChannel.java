package de.gwdg.cdstar.pool.nio;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.WritableByteChannel;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import de.gwdg.cdstar.FailableConsumer;
import de.gwdg.cdstar.Utils;

/**
 * {@link WritableByteChannel} that writes to a file, computes a digest and triggers a callback on close.
 */
final class RestoreChannel implements WritableByteChannel {

	private FileChannel channel;
	private long maxSize;
	private MessageDigest digest;
	private FailableConsumer<RestoreChannel> onClose;
	private Path path;

	public RestoreChannel(Path path, long maxSize, String hashAlgo, FailableConsumer<RestoreChannel> onClose) throws IOException {
		this.path=path;
		channel = FileChannel.open(path, StandardOpenOption.CREATE_NEW, StandardOpenOption.WRITE);
		try {
			digest = MessageDigest.getInstance(hashAlgo);
		} catch (NoSuchAlgorithmException e) {
			Utils.wtf(e);
		}
		this.maxSize = maxSize;
		this.onClose = onClose;
	}

	@Override
	public int write(ByteBuffer src) throws IOException {
		digest.update(src.duplicate());
		int written = 0;
		while (src.hasRemaining())
			written += channel.write(src);
		if (channel.position() > maxSize)
			throw new IOException("Resource size exceeded");
		return written;
	}

	@Override
	public boolean isOpen() {
		return channel.isOpen();
	}

	@Override
	public void close() throws IOException {
		if (!isOpen())
			return;
		channel.close();

		try {
			onClose.accept(this);
		} catch (IOException e) {
			throw e;
		} catch (Exception e) {
			throw new IOException(e);
		}
	}

	public MessageDigest getDigest() {
		return digest;
	}

	public Path getPath() {
		return path;
	}

}