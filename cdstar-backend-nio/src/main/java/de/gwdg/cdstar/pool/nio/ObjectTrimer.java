package de.gwdg.cdstar.pool.nio;

import java.io.Closeable;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.gwdg.cdstar.NamedThreadFactory;

class ObjectTrimer implements Closeable {
	private static Logger log = LoggerFactory.getLogger(ObjectTrimer.class);

	static final ScheduledExecutorService autotrimScheduler;
	// Add a bit of leeway to compensate clock screw or other temporal anomalies.
	private static Duration leeway = Duration.ofSeconds(2);
	// Limit the frequency at which delayed tasks are scanned for runnable tasks.
	private static Duration runTasksInterval = Duration.ofSeconds(10);

	static {
		// TODO: Get rid of this static thread pool
		autotrimScheduler = Executors
				.newSingleThreadScheduledExecutor(new NamedThreadFactory("NioPool-GC").deamon(true));
		Runtime.getRuntime().addShutdownHook(new Thread(() -> autotrimScheduler.shutdownNow()));
	}

	private NioPool pool;
	// Maps object IDs to the time trimLater() was last triggered for that id.
	Map<String, Instant> delayedTasks = new ConcurrentHashMap<>();

	private final ScheduledFuture<?> autotrimFuture;

	public ObjectTrimer(NioPool pool) {
		this.pool = pool;
		this.autotrimFuture = autotrimScheduler.scheduleWithFixedDelay(this::runTasks, runTasksInterval.toMillis(),
				runTasksInterval.toMillis(), TimeUnit.MILLISECONDS);
	}

	@Override
	public void close() {
		autotrimFuture.cancel(true);
	}

	/**
	 * Trim this object now.
	 *
	 * @return the finished {@link GCTask}
	 */
	GCTask trim(String objectId) {
		Instant keepNewerThan = pool.getOldestScopeStartTime()
				.orElseGet(Instant::now)
				.minus(leeway);
		return trimNow(objectId, keepNewerThan);
	}

	private GCTask trimNow(String objectId, Instant keepNewerThan) {
		final GCTask gc = new GCTask(pool, objectId, false, keepNewerThan);
		gc.run();
		return gc;
	}

	/**
	 * Trim this object later. That is, after all currently running pool sessions
	 * ended, so that any resources that are obsolete at the time of this call can
	 * actually be collected.
	 */
	void trimLater(String id) {
		delayedTasks.put(id, Instant.now());
	}

	/**
	 * Run all delayed tasks that were scheduled before the oldest pool session
	 * started. This ensures that resources that were removed before the
	 * {@link #trimLater(String)} call can actually be collected, since no
	 * transaction is old enough to reference these anymore.
	 */
	private void runTasks() {
		Instant keepNewerThan = pool.getOldestScopeStartTime()
				.orElseGet(Instant::now)
				.minus(leeway);

		// Find and remove delayed tasks that were last triggered BEFORE the current
		// keepNewerThan barrier.
		List<String> objectsToTrim = new ArrayList<>();
		delayedTasks.entrySet().removeIf(e -> {
			if (e.getValue().isBefore(keepNewerThan)) {
				objectsToTrim.add(e.getKey());
				return true;
			}
			return false;
		});

		if(objectsToTrim.size() == 0)
			return;

		log.info("Auto-trimming {} objects in pool {}", objectsToTrim.size(), pool);
		for (String id : objectsToTrim) {
			try {
				trimNow(id, keepNewerThan);
			} catch (Exception e) {
				log.warn("Object not trimmed: {}", id, e);
			}
		}

	}
}
