package de.gwdg.cdstar.pool.nio;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.nio.file.Paths;

import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.ta.TAJournal;
import de.gwdg.cdstar.ta.TAJournalReader;
import de.gwdg.cdstar.ta.TAJournalRecord;
import de.gwdg.cdstar.ta.TARecoveryHandler;

public class NioRecoveryHandler implements TARecoveryHandler {
	private static final long serialVersionUID = 4838920553540471208L;

	private final String basePath;
	private final String journalKey;
	private transient NioPool pool;

	public NioRecoveryHandler(NioPool pool) {
		basePath = pool.basePath.toString();
		journalKey = "NIOPOOL:" + System.identityHashCode(this);
	}

	private void readObject(ObjectInputStream in) throws ClassNotFoundException, IOException {
		in.defaultReadObject();
		// TODO: Allow path override via System parameter.
		pool = new NioPool(Paths.get(basePath));
	}

	void writeWalEntry(TAJournal wal, String objectId, String nextRevision) {
		wal.write(journalKey, objectId + "\t" + nextRevision);
	}

	@Override
	public void recover(String id, TAJournalReader wal, boolean doCommit) throws IOException {
		NioPool.log.info("Recovery of transaction {} (commit: {})", id, Boolean.valueOf(doCommit));
		for (TAJournalRecord e; (e = wal.next(journalKey)) != null;) {
			final String line = e.getStringValue();
			final int index = line.indexOf('\t');
			if (index <= 0 || index == line.length() - 1)
				throw new IllegalArgumentException("Recovery failed: Corrupt journal entry: " + Utils.repr(line));
			NioObject.onRecover(pool, line.substring(0, index), line.substring(index + 1), doCommit);
		}
	}

}
