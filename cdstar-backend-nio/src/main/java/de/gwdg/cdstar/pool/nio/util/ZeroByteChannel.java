package de.gwdg.cdstar.pool.nio.util;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.ReadableByteChannel;

public class ZeroByteChannel implements ReadableByteChannel {
	boolean open = true;

	@Override
	public boolean isOpen() {
		return open;
	}

	@Override
	public void close() throws IOException {
		open = false;
	}

	@Override
	public int read(ByteBuffer dst) throws IOException {
		return -1;
	}

}
