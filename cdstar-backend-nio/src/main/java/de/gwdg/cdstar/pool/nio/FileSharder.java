package de.gwdg.cdstar.pool.nio;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Deque;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Spliterator;
import java.util.function.Consumer;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.gwdg.cdstar.Utils;

/**
 * This class maps IDs (string identifiers) to directories in a balanced
 * hierarchy of subfolders to reduce the total number of links per directory and
 * improve lookup and scan performance. It also allows to iterate over IDs in
 * lexicographical order and jump to specific IDs in an efficient way.
 *
 * Warning: We assume that IDs are random from start to end and do not contain
 * any characters that might be problematic in a file system path. Two
 * characters are used per hierarchy. If only hex characters are used, this
 * means 256 folders per level, and n/(256^depth) folders in the last level.
 *
 * Even with modern file systems (like XFS) where the total number of files per
 * directory is no longer an issue, iterating over IDs in lexicographical (or
 * any) order is a memory-bound problem.
 *
 */
public class FileSharder implements Iterable<String> {
	private static final Logger log = LoggerFactory.getLogger(FileSharder.class);

	private final int depth;
	private final Path base;
	private final int baseNameCount;

	public FileSharder(Path base, int depth) {
		this.base = base;
		baseNameCount = base.getNameCount();
		this.depth = depth;
	}

	public boolean isValidId(String id) {
		return id != null && id.length() >= depth * 2 && PoolUtils.isLowercaseHexString(id);
	}

	/**
	 * Return a random ID that does not exist yet.
	 */
	public String getRandomId() {
		String id;
		do {
			id = PoolUtils.randomHexString(depth * 2 + 8);
		} while (Files.exists(getPath(id)));
		return id;
	}

	/**
	 * Return the path responsible for a given ID.
	 *
	 * Warning: We assume that the ID is random from start to end and does not
	 * contain any characters that might be problematic in a file system path. Two
	 * characters are used per depth-level. If only hex characters are used, this
	 * means 256 folders per level.
	 */
	public Path getPath(String id) {
		Path path = base;
		for (int i = 0; i < depth; i++)
			path = path.resolve(id.substring(i * 2, i * 2 + 2));
		path = path.resolve(id);
		return path;
	}

	/**
	 * Return the ID represented by a given path.
	 *
	 * We assume that this path was created by this class, or that it is compatible.
	 * More precisely, it MUST start with {@link #base} and has the correct depth.
	 */
	String extractID(Path path) {
		if (!path.startsWith(base))
			throw new IllegalArgumentException("Path not within base path");
		if (path.getNameCount() != baseNameCount + depth + 1)
			throw new IllegalArgumentException("Path depth does not match");
		return Objects.requireNonNull(path.getFileName()).toString();
	}

	/**
	 * Iterate over all IDs in natural string order.
	 */
	@Override
	public Iterator<String> iterator() {
		return iterator(null);
	}

	/**
	 * Iterate over an ordered subset of all IDs, skipping all IDs up to (and
	 * including) the given ID. This is much more efficient than a full scan.
	 */
	public Iterator<String> iterator(String skipUntil) {
		return new ShardIterator(skipUntil);
	}

	public Stream<String> stream() {
		return StreamSupport.stream(new ShardIterator(null), false);
	}

	public Stream<String> stream(String skipUntil) {
		return StreamSupport.stream(new ShardIterator(skipUntil), false);
	}

	class ShardIterator implements Iterator<String>, Spliterator<String> {

		// Directories that need to be scanned, separated by depth.
		private final Deque<Deque<Path>> scanStack = new ArrayDeque<>(depth);
		// Found IDs (leaf directories of the deepest path level)
		private final Deque<String> nextIDs = new ArrayDeque<>();
		// The skipUntil string split into path segments.
		private String[] skipUntil;

		public ShardIterator(String skipUntil) {

			// Split the skipUntil string the same way directories are split. For depth=3,
			// the skip string 'aab' is split into 'aa','b','','aab'.
			if (Utils.notNullOrEmpty(skipUntil)) {
				this.skipUntil = new String[depth + 1];
				for (int i = 0; i < depth; i++) {
					final int from = i * 2;
					final int to = Math.min(skipUntil.length(), from + 2);
					this.skipUntil[i] = from >= to ? "" : skipUntil.substring(from, to);
				}
				this.skipUntil[depth] = skipUntil; // Last segment has the full id (or full skip string)
			}

			// Push first level of directories to stack
			scanStack.push(new ArrayDeque<>(Arrays.asList(base)));
		}

		@Override
		public synchronized boolean hasNext() {

			// Perform ordered depth-first search into the directory
			// hierarchy until we hit the maximum depth, at which point
			// we collect all leaf directory names as IDs.
			while (nextIDs.isEmpty() && !scanStack.isEmpty()) {
				final Path scanDir = scanStack.peek().poll();
				if (scanDir == null) {
					// Nothing left in this branch -> Move up
					scanStack.pop();
					continue;
				}

				// Level of the scanDir: 0=/base/, 1=/base/aa/, 2=/base/aa/bb/
				final int level = scanStack.size() - 1;

				// Ignore this directory if it cannot possibly contain any hits,
				// but never ignore the base directory (level=0)
				if (skipUntil != null && level > 0) {
					final String scanDirName = scanDir.getFileName().toString();
					final int cmp = scanDirName.compareTo(skipUntil[level - 1]);
					// Skip this directory if it is too small
					if (cmp < 0)
						continue;
					// Equal or greater -> go into this directory
					if (cmp > 0)
						skipUntil = null; // We can stop filtering
				}

				// Find all child directories
				try (Stream<Path> ds = Files.list(scanDir)) {
					final Stream<Path> childDirs = ds.filter(Files::isDirectory);
					if (level < depth) {
						// Go deeper -> Push child directories to stack (ordered)
						final Deque<Path> newDirs = new ArrayDeque<>();
						childDirs
							.sorted(Comparator.comparing(p -> p.getFileName().toString()))
							.forEachOrdered(newDirs::add);
						scanStack.push(newDirs);
					} else {
						// level==depth -> Sort and collect IDs from child directory names.
						childDirs
							.map(p -> p.getFileName().toString())
							.sorted().forEachOrdered(id -> {
								if (skipUntil != null) {
									final int cmp = id.compareTo(skipUntil[depth]);
									// Skip small IDs, but keep filtering
									if (cmp < 0)
										return;
									// Found equal or greater ID -> stop filtering
									skipUntil = null;
									// Skip equal ID
									if (cmp == 0)
										return;
								}
								nextIDs.add(id);
							});
					}
				} catch (final IOException e) {
					log.debug("Could not scan: {}", scanDir, e);
				}

				// Will break our of the loop if IDs were found or the stack is empty,
				// or continue the depth-first search.
			}

			return !nextIDs.isEmpty();
		}

		@Override
		public synchronized String next() {
			final String nxt = nextIDs.poll();
			if (nxt == null)
				throw new NoSuchElementException();
			return nxt;
		}

		@Override
		public boolean tryAdvance(Consumer<? super String> action) {
			if (!hasNext())
				return false;
			action.accept(next());
			return true;
		}

		@Override
		public void forEachRemaining(Consumer<? super String> action) {
			while (hasNext())
				action.accept(next());
		}

		@Override
		public Spliterator<String> trySplit() {
			return null;
		}

		@Override
		public long estimateSize() {
			return Long.MAX_VALUE;
		}

		@Override
		public int characteristics() {
			return Spliterator.DISTINCT | Spliterator.NONNULL | Spliterator.ORDERED | Spliterator.SORTED
				| Spliterator.CONCURRENT;
		}

		@Override
		public Comparator<? super String> getComparator() {
			return null; // natural order
		}

	}

}
