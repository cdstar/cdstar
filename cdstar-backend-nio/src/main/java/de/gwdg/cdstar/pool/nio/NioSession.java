package de.gwdg.cdstar.pool.nio;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.pool.PoolError;
import de.gwdg.cdstar.pool.PoolError.NotFound;
import de.gwdg.cdstar.pool.StorageObject;
import de.gwdg.cdstar.pool.StoragePool;
import de.gwdg.cdstar.pool.StorageSession;
import de.gwdg.cdstar.pool.nio.json.JsonIndex;
import de.gwdg.cdstar.ta.TAJournal;
import de.gwdg.cdstar.ta.TAResource;
import de.gwdg.cdstar.ta.TransactionHandle;
import de.gwdg.cdstar.ta.TransactionInfo.Mode;

/**
 * This class keeps track of all opened objects in a transaction (possibly
 * multiple revisions per object) and acts as a {@link TAResource}.
 */
class NioSession implements TAResource, StorageSession {

	static final int WARN_SIZE = 128;
	static final int MAX_SIZE = 1024;
	private final NioPool pool;
	private final List<NioObject> headObjects = new ArrayList<>(2);
	private boolean notModified = false;
	TransactionHandle tx;

	public NioSession(NioPool nioPool) {
		pool = nioPool;
	}

	@Override
	public void bind(TransactionHandle t) {
		Utils.assertTrue(tx == null, "Resource bound mutliple times");
		tx = t;
		tx.bindRecoveryHandler(pool.recoveryHandler);
	}

	@Override
	public void prepare(TransactionHandle t) throws Exception {
		/*
		 * Shortcut for transactions without any modifications in SNAPSHOT mode, which
		 * are basically no-ops. We cannot shortcut SERIALIZABILITY transactions because
		 * unmodified objects may conflict there.
		 */

		notModified = null == Utils.first(headObjects, NioObject::isModified);
		if (notModified && tx.getMode() == Mode.SNAPSHOT)
			return;

		/*
		 * Objects are sorted by their ID, and locking happens in that order. This way,
		 * if multiple transactions conflict, at least one of them will succeed and
		 * deadlocks are impossible. The first transaction that locks the first
		 * conflicting object wins.
		 */
		headObjects.sort(Comparator.comparing(NioObject::getId));

		/*
		 * Remember which objects were part of this transaction in case of
		 * crash/recovery before doing any actual work.
		 */
		final TAJournal wal = tx.getJournal();
		for (final NioObject obj : headObjects)
			if (obj.isModified())
				pool.recoveryHandler.writeWalEntry(wal, obj.getId(), obj.getNextRevision());

		/*
		 * Prepare all objects. This will lock each object and fail if the file lock
		 * cannot be acquired, the revision does not match the current HEAD, or an IO
		 * error occurs.
		 */
		final Instant prepareTime = PoolUtils.getUniqueInstant();
		for (final NioObject o : headObjects) {
			o.onPrepare(prepareTime);
		}
	}

	@Override
	public void commit(TransactionHandle t) {
		try {
			if (notModified)
				return;

			// Actually commit to disk.
			for (final NioObject o : headObjects)
				o.onCommit();

		} finally {
			pool.closeScope(this, !notModified);
		}
	}

	@Override
	public void rollback(TransactionHandle t) {
		assert tx == t;

		try {
			// Same shortcut as in prepare().
			if (notModified && tx.getMode() == Mode.SNAPSHOT)
				return;

			// Cleanup and optionally unlock prepared objects.
			for (final NioObject o : headObjects)
				o.onRollback();
		} finally {
			pool.closeScope(this, false);
		}
	}

	@Override
	public StoragePool getPool() {
		return pool;
	}

	@Override
	public synchronized StorageObject createObject() {
		if(tx.isClosed())
			throw new IllegalStateException("Transaction closed");
		while (true) {
			final String id = pool.randomObjectId();
			if (pool.makeObjectDirectory(id)) {
				final NioObject obj = new NioObject(pool, tx, id);
				addObjectToScope(obj, true);
				return obj;
			}
			NioPool.log.warn("Collision in ID namespace. If this happens a lot, then the ID namespace is to small.");
		}
	}

	@Override
	public synchronized StorageObject createObject(String id) {
		if(tx.isClosed())
			throw new IllegalStateException("Transaction closed");
		if (!pool.makeObjectDirectory(id))
			throw new PoolError.InvalidObjectId("Object with this ID already exists: " + id);
		final NioObject obj = new NioObject(pool, tx, id);
		addObjectToScope(obj, true);
		return obj;
	}

	@Override
	public synchronized StorageObject readObject(String objectId, String revision) throws NotFound {
		Objects.requireNonNull(objectId);
		if(tx.isClosed())
			throw new IllegalStateException("Transaction closed");
		final long referenceTime = tx.getStarted().toEpochMilli();

		// Check if we already know the current revision
		NioObject current = Utils.first(headObjects, obj -> objectId.equals(obj.getId()));

		// Find current revision by scanning backwards from HEAD
		if (current == null) {

			/*
			 * TODO: Since mtime is defined during prepare, not commit, we might read
			 * objects that were prepared before, but committed after the transaction was
			 * started (phantom read) or even get mixed results from before/after a parallel
			 * transaction is committed (inconsistent read). There is no easy workaround.
			 * One solution would be an in-memory MVCC-capable HEAD->revision cache that
			 * provides a consistent snapshot view based on commit-time (instead of
			 * prepare-time) for recent commits and only requires actual disk reads for
			 * objects not changed recently.
			 */

			JsonIndex json = pool.load(objectId, null);
			while (json.mtime > referenceTime) {
				if (json.parent == null || json.dtime > -1)
					throw new NotFound(objectId);
				json = pool.load(objectId, json.parent);
			}
			if (json.dtime > -1)
				throw new NotFound(objectId);

			current = new NioObject(pool, tx, json);
			addObjectToScope(current, true);
		}

		if (revision == null) {
			return current;
		} else {
			if (revision.equals(current.getRevision()))
				return current;
			// TODO: Implement persistent revisions
			throw new NotFound(objectId);
		}
	}

	private synchronized void addObjectToScope(NioObject obj, boolean isHead) {
		if (isHead)
			headObjects.add(obj);
		else
			;// TODO: Implement persistent revisions

		final int poolSize = headObjects.size();
		// TODO: Make this configurable
		if (poolSize > MAX_SIZE)
			throw new PoolError("Maximum transaction scope size exceeded");
		if (poolSize == WARN_SIZE) {
			NioPool.log.warn("Large transaction ({} more than objects in scope): {}", Integer.toString(poolSize),
					tx);
		}
	}

	Set<String> getModifiedObjectIDs() {
		return headObjects.stream()
				.filter(NioObject::isModified)
				.map(NioObject::getId)
				.collect(Collectors.toSet());
	}

}
