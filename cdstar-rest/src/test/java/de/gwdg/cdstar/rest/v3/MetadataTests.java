package de.gwdg.cdstar.rest.v3;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;

import org.junit.Test;

import de.gwdg.cdstar.rest.BaseRestTest;
import de.gwdg.cdstar.web.common.model.MetaMap;

public class MetadataTests extends BaseRestTest {

	@Test
	public void testCRUDArchiveMeta() throws Exception {
		target("/v3/test/").request().post(null);
		assertStatus(Status.CREATED);
		final String archiveId = getJsonString("id");

		testCRUDOnAnnotateableMeta("/v3/test/" + archiveId);
	}

	@Test
	public void testCRUDFileMeta() throws Exception {
		target("/v3/test/").request().post(null);
		assertStatus(Status.CREATED);
		final String archiveId = getJsonString("id");
		final String fileUri = "/v3/test/" + archiveId + "/test.txt";
		target(fileUri).request().put(Entity.entity("Hello!", "text/plain"));
		testCRUDOnAnnotateableMeta(fileUri);
	}

	private void testCRUDOnAnnotateableMeta(String resourcePath) throws Exception {
		final MetaMap meta = new MetaMap();
		meta.add("a", "a");
		meta.add("b", "b1", "", "b2");

		// Create
		target(resourcePath).queryParam("meta", "").request().put(Entity.json(meta));
		assertStatus(204);

		// Get
		final MetaMap stored = target(resourcePath).queryParam("meta", "").request().get().readEntity(MetaMap.class);
		assertEquals(meta.getMap(), stored.getMap());

		// Update
		meta.put("c", Arrays.asList("b1"));
		target(resourcePath).queryParam("meta", "").request().put(Entity.json(meta));
		assertStatus(204);

		Response result = target(resourcePath).queryParam("meta", "").request().get();
		assertStatus(200);
		assertEquals(meta.getMap(), result.readEntity(MetaMap.class).getMap());

		// Delete
		target(resourcePath).queryParam("meta", "").request().delete();
		assertStatus(204);

		result = target(resourcePath).queryParam("meta", "").request().get();
		assertStatus(200);
		assertEquals(new HashMap<>(), result.readEntity(MetaMap.class).getMap());
	}

	@Test
	public void testNormalizeInput() throws Exception {

		final Map<String, List<String>> meta = new HashMap<>();
		meta.put("a", Arrays.asList("a", null, "b", ""));
		meta.put("b", Arrays.asList());
		meta.put("c", null);

		target("/v3/test/").request().post(null);
		assertStatus(Status.CREATED);
		final String archiveId = getJsonString("id");

		target("/v3/test/" + archiveId).queryParam("meta", "").request().put(Entity.json(meta));
		assertStatus(204);

		// We expect the server to clean up these as follows:
		meta.put("a", Arrays.asList("a", "b", ""));
		meta.remove("b");
		meta.remove("c");

		final Response result = target("/v3/test/", archiveId).queryParam("meta", "").request().get();
		assertStatus(200);
		assertEquals(meta, result.readEntity(MetaMap.class).getMap());

	}

	@Test
	public void testInvalidFieldNames() throws Exception {
		final Map<String, List<String>> meta = new HashMap<>();
		meta.put("a:not:valid", Arrays.asList("a", null, "b", ""));
		meta.put("b:not:valid", null);

		target("/v3/test/").request().post(null);
		assertStatus(Status.CREATED);
		final String archiveId = getJsonString("id");

		target("/v3/test/" + archiveId).queryParam("meta", "").request().put(Entity.json(meta));
		assertStatus(400);
		assertError(400, "InvalidFieldName");
	}

	@Test
	public void testBadJson() throws Exception {

		target("/v3/test/").request().post(null);
		assertStatus(Status.CREATED);
		final String archiveId = getJsonString("id");

		target("/v3/test/" + archiveId).queryParam("meta", "").request()
				.put(Entity.entity("{", MediaType.APPLICATION_JSON));
		assertError(400, "JsonParseError");

		target("/v3/test/" + archiveId).queryParam("meta", "").request()
				.put(Entity.entity("{ \"key\" : {} }", MediaType.APPLICATION_JSON));
		assertError(400, "JsonMappingError");
	}

}
