package de.gwdg.cdstar.rest.v2.utils;

import static org.junit.Assert.*;

import org.junit.Test;

public class LegacyHelperTest {

	@Test
	public void testLegacyIDTranslator() {
		assertEquals("deadbeef01239", LegacyHelper.fromAnyID("EAEA0-DEAD-BEEF-0123-9"));
		assertEquals("deadbeef01239", LegacyHelper.fromAnyID("deadbeef01239"));
		assertEquals("something completely different", LegacyHelper.fromAnyID("something completely different"));
	}

}
