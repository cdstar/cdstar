package de.gwdg.cdstar.rest.v2;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.core.Response.Status;

import org.junit.Test;

public class MetadataTests extends ApiTestBase {

	@Test
	public void testUploadMetadata() throws NoSuchAlgorithmException, IOException {
		final String uid = POST("/v2/test/objects/", null).getJsonString("uid");

		GET("/v2/test/objects/" + uid);
		assertTrue(getJson("metadata").isNull());

		GET("/v2/test/metadata/" + uid);
		assertStatus(Status.NOT_FOUND);

		final String json = "{\"test\": \"value\", \"test2\": [1,2,\"3\"]}";

		resetTimer();
		PUT("/v2/test/metadata/" + uid, Entity.json(json));
		assertStatus(Status.CREATED);
		assertEquals(uid, getJsonString("uid"));
		assertTrue(getJsonBool("ok"));

		GET("/v2/test/objects/" + uid);
		assertTrue(getJson("metadata").isObject());
		assertEquals("application/json", getJsonString("metadata.content-type"));
		assertTrue(getJson("metadata.checksum").isTextual());
		assertTrue(getJson("metadata.checksum-algorithm").isTextual());
		assertAfterTimer(getJsonDate("metadata.last-modified"));

		final MessageDigest md = MessageDigest.getInstance(getJsonString("metadata.checksum-algorithm"));

		GET("/v2/test/metadata/" + uid);
		assertStatus(Status.OK);
		assertEquals("value", getJsonString("test"));
		assertEquals(1, getJsonLong("test2.0"));
		assertEquals(2, getJsonLong("test2.1"));
		assertEquals("3", getJsonString("test2.2"));

		md.update(getEntity().getBytes("utf-8"));
		// TODO: The MD5 hash has no useful meaning in CDStar v3, as long as
		// index documents do not preserve their source.
		// assertEquals(Hex.encodeHexString(md.digest()), checksum);

		DELETE("/v2/test/metadata/" + uid);
		assertStatus(Status.NO_CONTENT);
		assertFalse(getLastResponse().hasEntity());
	}
}
