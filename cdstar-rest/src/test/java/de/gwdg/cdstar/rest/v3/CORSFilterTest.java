package de.gwdg.cdstar.rest.v3;

import static org.junit.Assert.assertTrue;

import jakarta.ws.rs.core.Response.Status;

import org.junit.Test;

import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.rest.BaseRestTest;

public class CORSFilterTest extends BaseRestTest {

	@Test
	public void testNoOriginNoCors() throws Exception {
		target("/v3/test/").request().get();
		assertStatus(Status.OK);
		assertHeaderEquals("Access-Control-Allow-Origin", null);
	}

	@Test
	public void testSimpleRequestContainsCorsHeader() throws Exception {
		target("/v3/test/").request().header("Origin", "https://example.com").get();
		assertStatus(Status.OK);
		assertHeaderEquals("Access-Control-Allow-Origin", "https://example.com");
	}

	@Test
	public void testPreflightResponseHeaders() throws Exception {
		target("/v3/test/").request()
			.header("Origin", "https://example.com")
			.header("Access-Control-Request-Method", "DELETE")
			.options();
		assertStatus(Status.OK);
		assertHeaderEquals("Access-Control-Allow-Origin", "https://example.com");
		assertTrue(Utils.notNullOrEmpty(getLastResponse().getHeaderString("Access-Control-Max-Age")));
		assertTrue(Utils.notNullOrEmpty(getLastResponse().getHeaderString("Access-Control-Allow-Methods")));
		assertTrue(Utils.notNullOrEmpty(getLastResponse().getHeaderString("Access-Control-Allow-Headers")));
	}

}
