package de.gwdg.cdstar.rest.v2;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.core.Response.Status;

import org.junit.Test;

public class CollectionTests extends ApiTestBase {

	@Test
	public void testEmpty() throws NoSuchAlgorithmException, IOException {
		final String uid = POST("/v2/test/objects/?type=COLLECTION", null).getJsonString("uid");

		GET("/v2/test/objects/" + uid);
		assertTrue(getJson("collection").isArray());
		assertEquals(0, getJson("collection").size());
		GET("/v2/test/collections/" + uid);
		assertStatus(Status.OK);
		assertEquals("[ ]", getEntity());
	}

	@Test
	public void testPut() throws IOException {
		final String uid = POST("/v2/test/objects/?type=COLLECTION", null).getJsonString("uid");
		PUT("/v2/test/collections/" + uid, Entity.json("[\"a\", \"b\", \"c\"]"));
		assertStatus(Status.CREATED);

		GET("/v2/test/collections/" + uid);
		assertStatus(Status.OK);
		assertEquals("a", getJsonString("0"));
		assertEquals("b", getJsonString("1"));
		assertEquals("c", getJsonString("2"));

		GET("/v2/test/objects/" + uid);
		assertStatus(Status.OK);
		assertEquals("a", getJsonString("collection.0"));
		assertEquals("b", getJsonString("collection.1"));
		assertEquals("c", getJsonString("collection.2"));
	}
}
