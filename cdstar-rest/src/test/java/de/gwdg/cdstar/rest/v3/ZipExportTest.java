package de.gwdg.cdstar.rest.v3;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import jakarta.ws.rs.client.Entity;

import org.junit.Before;
import org.junit.Test;

import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.rest.BaseRestTest;

public class ZipExportTest extends BaseRestTest {

	private String id;
	private Map<String, String> files;

	@Before
	public void prepareArchive() {
		files = new HashMap<>();
		files.put("a.txt", "a");
		files.put("b.json", "{}");
		files.put("path/c.txt", "path/c");

		target("/v3/test/").request().post(null);
		id = getJsonString("id");
		for (final Entry<String, String> fp : files.entrySet()) {
			target("/v3/test/", id, "/", fp.getKey()).request().put(Entity.text(fp.getValue()));
		}
	}

	@Test
	public void testExportFull() throws IOException {
		final InputStream fp = target("/v3/test/", id).queryParam("export", "zip").request().get(InputStream.class);
		final Map<String, String> entries = zipToMap(fp);
		assertEquals(files, entries);
	}

	@Test
	public void testExportExclude() throws IOException {
		final InputStream fp = target("/v3/test/", id)
			.queryParam("export", "zip")
			.queryParam("exclude", "*.txt")
			.request().get(InputStream.class);
		final Map<String, String> entries = zipToMap(fp);
		assertEquals(1, entries.size());
		assertEquals("b.json", entries.keySet().iterator().next());
	}

	@Test
	public void testExportInclude() throws IOException {
		final InputStream fp = target("/v3/test/", id)
			.queryParam("export", "zip")
			.queryParam("include", "*.json")
			.request().get(InputStream.class);
		final Map<String, String> entries = zipToMap(fp);
		assertEquals(1, entries.size());
		assertEquals("b.json", entries.keySet().iterator().next());
	}

	@Test
	public void testExportExcludeInclude() throws IOException {
		final InputStream fp = target("/v3/test/", id)
			.queryParam("export", "zip")
			.queryParam("include", "*.txt")
			.queryParam("exclude", "path/*")
			.request().get(InputStream.class);
		final Map<String, String> entries = zipToMap(fp);
		assertEquals(1, entries.size());
		assertEquals("a.txt", entries.keySet().iterator().next());
	}

	@Test
	public void testExportUnknownType() throws IOException {
		target("/v3/test/", id).queryParam("export", "pixiedust").request().get();
		assertError(400, "ApiError");
	}

	private Map<String, String> zipToMap(final InputStream fp) throws IOException {
		final Map<String, String> entries = new HashMap<>();
		final File tmp = testFolder.newFile();
		try (OutputStream out = Files.newOutputStream(tmp.toPath())) {
			Utils.copy(fp, out);
		}
		try (ZipFile zip = new ZipFile(tmp)) {
			for (final ZipEntry e : Utils.iter(zip.entries())) {
				final byte[] buf = new byte[(int) e.getSize()];
				assertEquals(buf.length, Utils.readNBytes(zip.getInputStream(e), buf));
				entries.put(e.getName(), new String(buf));
			}
		}
		return entries;
	}

}
