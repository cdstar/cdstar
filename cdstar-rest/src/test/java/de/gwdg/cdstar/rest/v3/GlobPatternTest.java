package de.gwdg.cdstar.rest.v3;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import de.gwdg.cdstar.rest.utils.GlobPattern;

public class GlobPatternTest {

	public void assertMatch(String glob, String path) {
		assertTrue("Glob '" + glob + "' should match '" + path + "'.", new GlobPattern(glob).test(path));
	}

	public void assertNoMatch(String glob, String path) {
		assertFalse("Glob '" + glob + "' should not match '" + path + "'", new GlobPattern(glob).test(path));
	}

	@Test
	public void testGlobPatternSyntax() {
		assertMatch("*.pdf", "/file.pdf");
		assertMatch("*.pdf", "/folder/file.pdf"); // suffix match
		assertMatch("*.pdf", "/folder/subfolder/file.pdf"); // suffix match
		assertMatch("*.pdf", "/.pdf"); // empty match
		assertNoMatch("*.pdf", "/file.txt");
		assertNoMatch("*.pdf", "/file.pdf.txt");
		assertNoMatch("*.pdf", "/filepdf");

		assertMatch("/*.pdf", "/file.pdf");
		assertNoMatch("/*.pdf", "/folder/file.pdf");
		assertNoMatch("/*.pdf", "/folder/subfolder/file.pdf");
		assertMatch("/*.pdf", "/.pdf");
		assertNoMatch("/*.pdf", "/file.txt");
		assertNoMatch("/*.pdf", "/file.pdf.txt");
		assertNoMatch("/*.pdf", "/filepdf");

		assertMatch("folder/*.pdf", "/folder/file.pdf");
		assertMatch("folder/*.pdf", "/some/folder/file.pdf"); // suffix match
		assertMatch("folder/*.pdf", "/folder/subfolder/folder/file.pdf"); // suffix
																			// match
		assertMatch("folder/*.pdf", "/folder/.pdf"); // empty match
		assertNoMatch("folder/*.pdf", "/file.pdf");
		assertNoMatch("folder/*.pdf", "/folder/file.txt");
		assertNoMatch("folder/*.pdf", "/folder/file.pdf.txt");
		assertNoMatch("folder/*.pdf", "/folder/filepdf");

		assertMatch("/folder/*.pdf", "/folder/file.pdf");
		assertNoMatch("/folder/*.pdf", "/some/folder/file.pdf");
		assertNoMatch("/folder/*.pdf", "/folder/subfolder/folder/file.pdf");
		assertMatch("/folder/*.pdf", "/folder/.pdf"); // empty match
		assertNoMatch("/folder/*.pdf", "/file.pdf");
		assertNoMatch("/folder/*.pdf", "/folder/file.txt");
		assertNoMatch("/folder/*.pdf", "/folder/file.pdf.txt");
		assertNoMatch("/folder/*.pdf", "/folder/filepdf");

		assertMatch("/folder/*/*.pdf", "/folder/subfolder/file.pdf");
		assertMatch("/folder/*/*.pdf", "/folder/subfolder/.pdf");
		assertMatch("/folder/*/*.pdf", "/folder//.pdf");
		assertNoMatch("/folder/*/*.pdf", "/folder/subfolder/subsubfolder/file.pdf");

		assertMatch("201?.csv", "/2017.csv");
		assertMatch("201?.csv", "/reports/2017.csv");
		assertNoMatch("201?.csv", "/reports/2017/final.csv");
		assertNoMatch("201?.csv", "/201.csv");
		assertNoMatch("201?.csv", "/2021.csv");
		assertNoMatch("201?.csv", "/20171.csv");
		assertNoMatch("201?.csv", "/2017csv");
		assertNoMatch("201?.csv", "/201/.csv");

		assertMatch("target/**.jar", "/target/project.jar");
		assertMatch("target/**.jar", "/folder/target/project.jar");
		assertNoMatch("/target/**.jar", "/folder/target/project.jar");
		assertMatch("target/**.jar", "/target/lib/project.jar");
		assertMatch("target/**.jar", "/target/lib/lib/lib/project.jar");

		assertMatch("/**/test", "/foo/test");
		assertMatch("/**/test", "/foo/bar/test");
		assertNoMatch("/**/test", "/test"); // is this intuitive?

	}

}
