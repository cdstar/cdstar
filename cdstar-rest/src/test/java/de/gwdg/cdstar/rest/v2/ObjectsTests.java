package de.gwdg.cdstar.rest.v2;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.Arrays;

import jakarta.ws.rs.core.Response.Status;

import org.junit.Test;

import de.gwdg.cdstar.auth.simple.Account;

public class ObjectsTests extends ApiTestBase {

	@Test
	public void testCreateDefaultType() {
		POST("/v2/test/objects/", null);
		GET("/v2/test/objects/" + getJsonString("uid"));
		assertStatus(Status.OK);
		assertEquals("OBJECT", getJsonString("type"));
	}

	@Test
	public void testCreateObject() {
		POST("/v2/test/objects/?type=OBJECT", null);
		GET("/v2/test/objects/" + getJsonString("uid"));
		assertStatus(Status.OK);
		assertEquals("OBJECT", getJsonString("type"));
	}

	@Test
	public void testCreateCollection() {
		POST("/v2/test/objects/?type=COLLECTION", null);
		GET("/v2/test/objects/" + getJsonString("uid"));
		assertStatus(Status.OK);
		assertEquals("COLLECTION", getJsonString("type"));
	}

	@Test
	public void testCreateIllegalType() {
		POST("/v2/test/objects/?type=Garbage", null);
		assertStatus(Status.BAD_REQUEST);
		assertEquals("parameter", getJsonString("error"));
	}

	@Test
	public void testEmptyObject() {
		for (final String type : Arrays.asList("OBJECT", "COLLECTION")) {
			resetTimer();
			final String uid = POST("/v2/test/objects/?type=" + type, null).getJsonString("uid");
			GET("/v2/test/objects/" + uid);

			assertStatus(Status.OK);
			assertEquals(uid, getJsonString("uid"));
			assertEquals(type, getJsonString("type"));
			assertTrue(getJson("metadata").isNull());
			assertAfterTimer(getJsonDate("last-modified"));

			switch (type) {
			case "COLLECTION":
				assertTrue(getJson("collection").isArray());
				assertEquals(0, getJson("collection").size());
				assertTrue(findJson("bitstream").isMissingNode());
				break;
			default:
				assertTrue(getJson("bitstream").isArray());
				assertEquals(0, getJson("bitstream").size());
				assertTrue(findJson("collection").isMissingNode());
				break;
			}
		}
	}

	@Test
	public void testDefaultPermissions() throws IOException {
		final String uid = POST("/v2/test/objects/", null).getJsonString("uid");
		GET("/v2/test/objects/" + uid);
		final Account acc = getAuthRealm().account("test");
		assertEquals(acc.getFullId(), getJsonString("permissions.owner"));
		// assertEquals(Arrays.asList(acc.getFullId(), "admin"),
		// getJsonStrings("permissions.manage"));
		assertEquals(Arrays.asList(acc.getFullId()), getJsonStrings("permissions.read"));
		assertEquals(Arrays.asList(acc.getFullId()), getJsonStrings("permissions.write"));
	}

	@Test
	public void testDeleteObject() {
		final String uid = POST("/v2/test/objects/", null).getJsonString("uid");
		DELETE("/v2/test/objects/" + uid);
		assertStatus(Status.NO_CONTENT);
		GET("/v2/test/objects/" + uid);
		assertStatus(Status.NOT_FOUND);
	}
}
