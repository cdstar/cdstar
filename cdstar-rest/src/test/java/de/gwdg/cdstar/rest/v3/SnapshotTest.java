package de.gwdg.cdstar.rest.v3;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.core.Form;
import jakarta.ws.rs.core.Response.Status;

import org.junit.Before;
import org.junit.Test;

import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.rest.BaseRestTest;
import de.gwdg.cdstar.runtime.CDStarRuntime;
import de.gwdg.cdstar.runtime.lts.LTSConfig;
import de.gwdg.cdstar.runtime.lts.bagit.BagitTarget;
import de.gwdg.cdstar.runtime.profiles.ProfileRegistry;
import de.gwdg.cdstar.runtime.tasks.TaskService;
import de.gwdg.cdstar.web.common.model.ArchiveInfo;
import de.gwdg.cdstar.web.common.model.FileList;
import de.gwdg.cdstar.web.common.model.SnapshotInfo;
import de.gwdg.cdstar.web.common.model.SnapshotList;

public class SnapshotTest extends BaseRestTest {

	private String archiveId;

	@Before
	public void prepareArchive() throws IOException, URISyntaxException {
		target("/v3/test/").request().post(null);
		assertStatus(Status.CREATED);
		archiveId = getJsonString("id");
	}

	String makeSnapshot(String archiveId, String name) {
		target("/v3/test/", archiveId)
				.queryParam("snapshots", "").request()
				.post(Entity.form(new Form("name", name)));
		assertStatus(Status.CREATED);
		assertEquals(name, getJsonString("name"));
		assertEquals("test@system", getJsonString("creator"));
		return archiveId + "@" + name;
	}

	String getFileAsString(String archiveId, String name) {
		return target("/v3/test/", archiveId, name).request().get(String.class);
	}

	@Test
	public void testListSnapshots() throws Exception {
		makeSnapshot(archiveId, "v1");
		makeSnapshot(archiveId, "v2");
		makeSnapshot(archiveId, "v3");

		final SnapshotList snapshots = target("/v3/test/" + archiveId).queryParam("snapshots", "").request()
				.get(SnapshotList.class);
		assertEquals(3, snapshots.getCount());
		assertEquals(3, snapshots.getTotal());
		assertEquals("v1", snapshots.getItems().get(0).name);
		assertEquals("v2", snapshots.getItems().get(1).name);
		assertEquals("v3", snapshots.getItems().get(2).name);

		List<SnapshotInfo> snapList = target("/v3/test/" + archiveId).queryParam("with", "snapshots").request()
				.get(ArchiveInfo.class).snapshots;
		assertEquals(3, snapList.size());
		assertEquals("v1", snapList.get(0).name);
		assertEquals("v2", snapList.get(1).name);
		assertEquals("v3", snapList.get(2).name);
	}

	@Test
	public void testSnapshotHaveSpecialFields() throws Exception {
		final String v1 = makeSnapshot(archiveId, "v1");
		ArchiveInfo snap = target("/v3/test/", v1).request().get(ArchiveInfo.class);
		assertEquals(v1, snap.id);
	}

	@Test
	public void testSnapshotsPreserveFiles() throws Exception {
		target("/v3/test/", archiveId, "/test.txt").request().put(Entity.text("v1"));
		final String v1 = makeSnapshot(archiveId, "v1");
		target("/v3/test/", archiveId, "/test1.txt").request().put(Entity.text("v2"));
		target("/v3/test/", archiveId, "/test2.txt").request().put(Entity.text("v2"));

		// File Content
		assertEquals("v1", target("/v3/test/", v1, "/test.txt").request().get(String.class));
		assertEquals(404, target("/v3/test/", v1, "/test2.txt").request().get().getStatus());

		// File list (embedded)
		assertEquals(1, target("/v3/test/", v1)
				.queryParam("with", "files")
				.request().get(ArchiveInfo.class).files.size());

		// File list (separate)
		assertEquals(1, target("/v3/test/", v1)
				.queryParam("files", "")
				.request().get(FileList.class).getTotal());

	}

	@Test
	public void testSnapshotsPreserveMetadata() throws Exception {
		target("/v3/test/", archiveId, "/test.txt").request().put(Entity.text("v0"));
		assertStatus(201);
		target("/v3/test/", archiveId).request().post(Entity.form(new Form()
				.param("meta:foo", "foo")
				.param("meta:foo:/test.txt", "foo")));
		assertStatus(200);

		final String v1 = makeSnapshot(archiveId, "v1");
		target("/v3/test/", archiveId).request().post(Entity.form(new Form()
				.param("meta:foo", "bar")
				.param("meta:foo:/test.txt", "bar")));
		assertStatus(200);

		target("/v3/test/", archiveId).queryParam("with", "files,meta").request().get();
		assertEquals(Arrays.asList("bar"), getJsonStrings("meta.foo"));
		assertEquals(Arrays.asList("bar"), getJsonStrings("files.0.meta.foo"));

		target("/v3/test/", v1).queryParam("with", "files,meta").request().get();
		assertEquals(Arrays.asList("foo"), getJsonStrings("meta.foo"));
		assertEquals(Arrays.asList("foo"), getJsonStrings("files.0.meta.foo"));
	}

	@Override
	public void initRuntime(CDStarRuntime runtime) throws Exception {
		runtime.register(new BagitTarget("zip", runtime.getServiceDir("zip")));
	}

	@Test
	public void testSnapshotHaveTheirOwnProfile() throws Exception {
		final ProfileRegistry profiles = getRuntime().lookupRequired(ProfileRegistry.class);
		final Map<String, String> ltsConf = new HashMap<>();
		ltsConf.put(LTSConfig.PROP_NAME, "zip");
		profiles.defineProfile("test", "zip1", ltsConf);
		profiles.defineProfile("test", "zip2", ltsConf);

		final String v1 = makeSnapshot(archiveId, "v1");
		target("/v3/test/", archiveId).request().post(Entity.form(new Form()
				.param("profile", "zip1")));
		waitForTasks();

		target("/v3/test/", v1).request().post(Entity.form(new Form()
				.param("profile", "zip2")));
		waitForTasks();

		target("/v3/test/", archiveId).request().get();
		assertEquals("zip1", getJsonString("profile"));
		target("/v3/test/", v1).request().get();
		assertEquals("zip2", getJsonString("profile"));
	}

	public void waitForTasks() {
		getRuntime().lookupRequired(TaskService.class).allTasks().forEach(task -> {
			for (int i = 0; i < 10; i++) {
				if (task.isDone())
					return;
				if (!Utils.sleepInterruptable(100))
					break;
			}
			fail("Task did not finish: " + task);
		});
	}

	@Test
	public void testSnapshotInheritAclAndOwner() throws Exception {
		final String v1 = makeSnapshot(archiveId, "v1");

		// Change owner. Two steps required:
		target("/v3/test/", archiveId).request().post(Entity.form(new Form()
				.param("acl:test@system",
						"READ,read_acl")
				.param("acl:$owner",
						"OWNER,change_owner")));
		assertStatus(200);
		target("/v3/test/", archiveId).request().post(Entity.form(
				new Form().param("owner", "not_me")));
		assertStatus(200);

		// Ensure that snapshot ACL and owner are identical to HEAD
		ArchiveInfo snap = target("/v3/test/", v1).queryParam("with", "acl").request().get(ArchiveInfo.class);
		ArchiveInfo head = target("/v3/test/", archiveId).queryParam("with", "acl").request().get(ArchiveInfo.class);
		assertFalse(snap.acl.getMap().isEmpty());
		assertEquals(snap.owner, head.owner);
		assertEquals(snap.acl.getMap(), head.acl.getMap());
	}

}
