package de.gwdg.cdstar.rest.v2;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URI;
import java.util.Date;

import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.client.Invocation;
import jakarta.ws.rs.client.Invocation.Builder;
import jakarta.ws.rs.client.WebTarget;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

import org.junit.Before;

import de.gwdg.cdstar.rest.BaseRestTest;
import de.gwdg.cdstar.rest.v2.model.ObjectBean;
import de.gwdg.cdstar.rest.v2.model.Success.ObjectCreated;
import de.gwdg.cdstar.runtime.Config;

public class ApiTestBase extends BaseRestTest {

	private Date timer;
	private Response response;

	@Override
	public void initConfig(Config config) {
		super.initConfig(config);
		config.set("api.v2.enable", "true");
		config.set("api.dariah.enable", "true");
	}

	@Before
	public final void apiSetUp() throws Exception {
		resetTimer();
	}

	protected void resetTimer() {
		timer = new Date();
		timer.setTime(timer.getTime() - timer.getTime() % 1000);
	}

	public String lastLocationPathSegment() {
		final String[] paths = getLastResponse().getLocation().getPath().split("/");
		return paths[paths.length - 1];
	}

	protected Date getTimer() {
		return timer;
	}

	protected Builder prepare(final String path) {
		final URI x = URI.create(path);
		WebTarget t = target(x.getPath());

		if (x.getQuery() != null) {
			for (final String pair : x.getQuery().split("&")) {
				t = t.queryParam(pair.substring(0, pair.indexOf('=')), pair.substring(pair.indexOf('=') + 1));
			}
		}

		return t.request(MediaType.APPLICATION_JSON_TYPE, MediaType.TEXT_HTML_TYPE, MediaType.WILDCARD_TYPE);
	}

	protected ApiTestBase send(final Invocation request) {
		response = request.invoke();
		return this;
	}

	public ApiTestBase GET(final String path) {
		response = prepare(path).get();
		return this;
	}

	public ApiTestBase POST(final String path, final Entity<?> entity) {
		response = prepare(path).post(entity);
		return this;
	}

	public ApiTestBase PUT(final String path, final Entity<?> entity) {
		response = prepare(path).put(entity);
		return this;
	}

	public ApiTestBase DELETE(final String path) {
		response = prepare(path).delete();
		return this;
	}

	protected void assertInEntity(final String needle) throws IOException {
		assertTrue("Could not find '" + needle + "' in response entity.", getEntity().indexOf(needle) > -1);
	}

	protected void assertAfterTimer(final Date date) {
		assertFalse("Date " + date + " is to old (" + timer + ")", timer.after(date));
		assertTrue("Date " + date + " is in the future", new Date().after(date));
	}

	protected void assertWithinTimeFrame(final long millis) {
		assertTrue("More than " + millis + "ms elapsed.", new Date().getTime() - millis <= timer.getTime());
	}

	public ObjectBean newObject() {
		final String uid = POST("/v2/test/objects/", null).readEntity(ObjectCreated.class).getUid();
		return GET("/v2/test/objects/" + uid).readEntity(ObjectBean.class);
	}

	private <T> T readEntity(Class<T> class1) {
		return response.readEntity(class1);
	}

}
