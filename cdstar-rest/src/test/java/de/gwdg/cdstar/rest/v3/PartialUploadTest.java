package de.gwdg.cdstar.rest.v3;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;

import org.junit.Before;
import org.junit.Test;

import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.rest.BaseRestTest;
import de.gwdg.cdstar.rest.TestDataStream;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.core.Response.Status;

public class PartialUploadTest extends BaseRestTest {

	private String tx;
	private String id;
	private int uploadSize;
	private int targetSize;
	private String fileUrl;

	@Before
	public void uploadParial() throws UnknownHostException, IOException {
		// Make explicit transaction (implicit ones are always rolled back on errors)
		target("/v3/_tx").request().post(null);
		assertStatus(Status.CREATED);
		tx = getJsonString("id");

		target("/v3/test").request().header("X-Transaction", tx).post(null);
		assertStatus(Status.CREATED);
		id = getJsonString("id");

		// Actual upload must be larger than the largest internal buffer, or the upload
		// will fail before any bytes arrives at the application.
		uploadSize = 1024 * 64 + 1;
		targetSize = uploadSize + 10;
		fileUrl = "/v3/test/" + id + "/test";

		Socket sock = new Socket(getBaseUri().getHost(), getBaseUri().getPort());
		OutputStream out = sock.getOutputStream();
		OutputStreamWriter writer = new OutputStreamWriter(out, StandardCharsets.US_ASCII);
		writer.write("PUT " + fileUrl + " HTTP/1.1\r\n");
		writer.write("Host: " + getBaseUri().getHost() + "\r\n");
		writer.write("X-Transaction: " + tx + "\r\n");
		writer.write("Content-Length: " + targetSize + "\r\n");
		writer.write("\r\n");
		writer.flush();
		Utils.copy(new TestDataStream(uploadSize), sock.getOutputStream());
		out.flush();
		sock.shutdownOutput(); // This should trigger an unexpected end-of-stream on server side.
		BufferedReader response = new BufferedReader(
				new InputStreamReader(sock.getInputStream(), StandardCharsets.US_ASCII));
		while (response.readLine() != null) {
		}
		sock.close();
	}

	int getActualSize() {
		target(fileUrl).request().header("X-Transaction", tx).head();
		assertStatus(200);
		return getLastResponse().getLength();
	}

	@Test
	public void testHeadRequestReturnsPartialSize() throws IOException {
		// HEAD works
		target(fileUrl).request()
				.header("X-Transaction", tx)
				.head();
		assertStatus(200);
		int partialSize = getLastResponse().getLength();
		assertNotEquals(0, partialSize);
		assertTrue(partialSize > 0);
		assertTrue(partialSize <= uploadSize);
	}

	@Test
	public void testGetFails() throws IOException {
		target(fileUrl).request().header("X-Transaction", tx)
				.get();
		assertError(Status.CONFLICT, "IncompleteWrite");
	}

	@Test
	public void testGetFileInfoHasNoDigest() throws IOException {
		// GET info works, but no digests
		int partialSize = getActualSize();
		target(fileUrl).queryParam("info", "")
				.request().header("X-Transaction", tx)
				.get();
		assertEquals(partialSize, getJsonLong("size"));
		assertTrue(getJson("digests").isNull());
	}

	@Test
	public void testGetFileListHasNoDigest() throws IOException {
		// GET list also works, but no digests
		int partialSize = getActualSize();
		target("/v3/test/", id).queryParam("files", "")
				.request().header("X-Transaction", tx)
				.get();
		assertEquals(partialSize, getJsonLong("files.0.size"));
		assertTrue(getJson("files.0.digests").isNull());

		// Commit fails
		target("/v3/_tx/", tx).request().post(null);
		assertError(Status.CONFLICT, "IncompleteWrite");
	}

	@Test
	public void testUploadResume() throws UnknownHostException, IOException {
		int currentSize = getActualSize();

		target(fileUrl).request()
				.header("X-Transaction", tx)
				.header("Range", "bytes=" + currentSize + "-")
				.header("X-HTTP-Method-Override", "PATCH")
				.post(Entity.entity("0123456789", "application/vnd.cdstar.resume"));
		// Java 17+ broke PATCH requests with the default client connector, so we work around this here.

		// We added 10 bytes
		assertEquals(currentSize + 10, getActualSize());

		// Digests are now present
		target(fileUrl).queryParam("info", "")
				.request().header("X-Transaction", tx)
				.get();

		assertNotNull(getJsonString("digests.sha256"));

		// GET works
		target(fileUrl).request().header("X-Transaction", tx)
				.get();

		// Commit works
		target("/v3/_tx/", tx).request().post(null);
		assertStatus(200);
	}

}
