package de.gwdg.cdstar.rest.v3;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import jakarta.ws.rs.core.Response.Status;

import org.junit.Test;

import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.rest.BaseRestTest;
import de.gwdg.cdstar.runtime.client.CDStarSession;

public class ExplicitTransactionTest extends BaseRestTest {

	private String tx;
	private CDStarSession session;

	@Test
	public void makeTransaction() {
		target("/v3/_tx").request().post(null);
		assertStatus(Status.CREATED);
		tx = getJsonString("id");
		assertEquals("snapshot", getJsonString("isolation"));
		assertEquals(60, getJsonLong("timeout"));
		assertTrue(getJsonLong("timeout") >= getJsonLong("ttl"));
		assertTrue(getJsonLong("timeout") - 5 <= getJsonLong("ttl"));
		assertEquals(false, getJsonBool("readonly"));

		session = getRuntime().resumeSession(tx);
		assertNotNull(session);
	}

	@Test
	public void testTransactionInfo() {
		makeTransaction();
		session.setTimeout(100_000);

		target("/v3/_tx/", tx).request().get();

		assertEquals(tx, getJsonString("id"));
		assertEquals("snapshot", getJsonString("isolation"));
		assertEquals(100, getJsonLong("timeout"));
		assertTrue(getJsonLong("timeout") >= getJsonLong("ttl"));
		assertTrue(getJsonLong("timeout") - 5 <= getJsonLong("ttl"));
		assertEquals(false, getJsonBool("readonly"));
	}

	@Test
	public void testIsolation() {
		makeTransaction();

		target("/v3/test").request().header("X-Transaction", tx).post(null);
		assertStatus(Status.CREATED);
		final String id = getJsonString("id");

		// Archive should be invisible to other transactions
		target("/v3/test/", id).request().get();
		assertStatus(Status.NOT_FOUND);

		// Archive should be load-able by same transaction
		target("/v3/test/", id).request().header("X-Transaction", tx).get();
		assertStatus(Status.OK);
		assertEquals(id, getJsonString("id"));
	}

	@Test
	public void testRenewOfDeletedTransactionShouldFail() {
		target("/v3/_tx").request().post(null);
		assertStatus(Status.CREATED);
		tx = getJsonString("id");

		target("/v3/_tx/", tx).request().delete();
		assertStatus(Status.NO_CONTENT);

		// TODO: Retry until rollback thread was able to run
		Utils.sleepInterruptable(100);

		target("/v3/_tx/", tx).queryParam("renew", "true").request().get();
		assertStatus(Status.NOT_FOUND);

		target("/v3/_tx/", tx).request().get();
		assertStatus(Status.NOT_FOUND);
	}

}
