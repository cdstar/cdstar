package de.gwdg.cdstar.rest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

import org.junit.Rule;
import org.junit.Test;
import org.slf4j.Logger;

import ch.qos.logback.classic.Level;
import de.gwdg.cdstar.rest.api.AsyncContext;
import de.gwdg.cdstar.rest.api.HttpError;
import de.gwdg.cdstar.rest.api.HttpError.NotFound;
import de.gwdg.cdstar.rest.api.RestContext;
import de.gwdg.cdstar.utils.test.TestLogger;

public class FrameworkTest {

	@Rule
	public TestLogger logger = new TestLogger(Logger.ROOT_LOGGER_NAME);

	public static class TestContext extends RestContextBase {

		private final String method;
		private final String path;
		private final Map<String, String> headers = new HashMap<>();
		private int status;
		private final Map<String, String> sendHeaders = new HashMap<>();

		private final ByteArrayOutputStream outputBuffer = new ByteArrayOutputStream();

		public TestContext(String method, String path, String... headers) {
			super();
			this.method = method.toUpperCase();
			this.path = path;
			for (final String h : headers) {
				final String[] parts = h.split(":", 2);
				this.headers.put(parts[0].trim().toLowerCase(), parts[1].trim());
			}
		}

		@Override
		public String getQuery() {
			return null;
		}

		@Override
		public String header(String name) {
			return sendHeaders.get(name.toLowerCase());
		}

		@Override
		public String getMethod() {
			return method;
		}

		@Override
		public String getPath() {
			return path;
		}

		@Override
		public String getHeader(String name) {
			return headers.get(name.toLowerCase());
		}

		@Override
		public boolean hasEntity() {
			return false;
		}

		@Override
		public int read(byte[] buffer, int off, int len) throws IOException {
			return -1;
		}

		@Override
		public RestContext status(int code) {
			status = code;
			return this;
		}

		@Override
		public RestContext header(String name, String value) {
			sendHeaders.put(name, value);
			return this;
		}

		@Override
		public void write(byte[] buffer, int off, int len) throws IOException {
			outputBuffer.write(buffer, off, len);
		}

		@Override
		public boolean isCommitted() {
			return outputBuffer.size() > 0;
		}

		@Override
		public boolean isAsync() {
			return false;
		}

		@Override
		public AsyncContext startAsync() {
			throw new UnsupportedOperationException();
		}

		@Override
		public long getContentLength() {
			return -1;
		}

		@Override
		public String resolvePath(String path, boolean keepLastSegment) {
			throw new UnsupportedOperationException();
		}

	}

	public FrameworkTest() {
	}

	@Test
	public void testErrorMapper() throws Exception {
		final AtomicReference<HttpError> base = new AtomicReference<>();
		final AtomicReference<NotFound> notfound = new AtomicReference<>();
		final RestConfigImpl rc = new RestConfigImpl(null);
		rc.route("/base").GET(ctx -> {throw new HttpError(300, "halp");});
		rc.route("/404").GET(ctx -> {throw new NotFound();});
		rc.mapError(HttpError.class, (ctyx, e) -> base.set(e));
		rc.mapError(NotFound.class, (ctyx, e) -> notfound.set(e));
		final RequestDispatcher router = new RequestDispatcher(rc);
		router.dispatch(new TestContext("GET", "/base", "Accept: text/html"));
		assertNotNull(base.get());
		logger.setLevel("de.gwdg", Level.TRACE);
		router.dispatch(new TestContext("GET", "/404", "Accept: text/html"));
		assertNotNull(notfound.get());
	}

	@Test
	public void testResponseMapper() throws Exception {
		final AtomicReference<String> ref = new AtomicReference<>();
		final RestConfigImpl rc = new RestConfigImpl(null);
		rc.route("/").GET(ctx -> new Object());
		rc.mapResponse("text/plain; q=0.9", Object.class, (ctx, v) -> ref.set("plain"));
		rc.mapResponse("text/html", Object.class, (ctx, v) -> ref.set("html"));
		final RequestDispatcher router = new RequestDispatcher(rc);
		router.dispatch(new TestContext("GET", "/", "Accept: text/html"));
		assertEquals(ref.get(), "html");

		router.dispatch(new TestContext("GET", "/", "Accept: text/plain"));
		assertEquals(ref.get(), "plain");

		final TestContext tc = new TestContext("GET", "/", "Accept: image/png");
		router.dispatch(tc);
		assertEquals(406, tc.status);

		router.dispatch(new TestContext("GET", "/", "Accept: "));
		assertEquals(ref.get(), "html");

		router.dispatch(new TestContext("GET", "/"));
		assertEquals(ref.get(), "html");

		router.dispatch(new TestContext("GET", "/", "Accept: text/html;q=0.8, text/plain"));
		assertEquals(ref.get(), "plain");

		router.dispatch(new TestContext("GET", "/", "Accept: text/html;q=0.99, text/plain"));
		assertEquals(ref.get(), "html");
	}

}
