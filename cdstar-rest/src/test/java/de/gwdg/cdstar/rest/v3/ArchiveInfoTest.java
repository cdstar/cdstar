package de.gwdg.cdstar.rest.v3;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.client.WebTarget;
import jakarta.ws.rs.core.Response.Status;

import org.glassfish.jersey.media.multipart.FormDataMultiPart;
import org.junit.Before;
import org.junit.Test;

import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.rest.BaseRestTest;
import de.gwdg.cdstar.web.common.model.FileInfo;

public class ArchiveInfoTest extends BaseRestTest {

	private String archiveId;

	@Before
	public void prepareArchive() throws IOException, URISyntaxException {
		target("/v3/test/").request().post(null);
		assertStatus(Status.CREATED);
		archiveId = getJsonString("id");
	}

	@Test
	public void testNoParameter() {
		addFile("a.txt", "text/x-c", "x");
		addFile("b.txt", "text/x-b", "xxx");
		addFile("c.txt", "text/x-a", "xx");

		target("/v3/test/", archiveId).request().get();
		assertStatus(Status.OK);
		assertEquals(archiveId, getJsonString("id"));
		assertEquals("test", getJsonString("vault"));
		assertEquals(3, getJsonLong("file_count"));
		assertTrue(findJson("files").isMissingNode());
	}

	private FileInfo addFile(String name, String type, String content) {
		return target("/v3/test/", archiveId, "/", name).request().put(Entity.entity(content, type)).readEntity(
			FileInfo.class);
	}

	private void checkNames(String desiredOrder) {
		final String actualOrder = Utils.join(",",
			Utils.toArrayList(Utils.toArrayList(findJson("files")).stream().map(n -> n.path("name").textValue())));
		assertEquals(desiredOrder, actualOrder);
	}

	@Test
	public void testParamOrder() {
		// These files will be sorted differently for each supported mode.
		final FileInfo a = addFile("a", "text/x-c", "x");
		final FileInfo b = addFile("b", "text/x-b", "xxx");
		final FileInfo c = addFile("d", "text/x-d", "xxxx");
		final FileInfo d = addFile("c", "text/x-a", "xx");
		addFile("a", "text/x-c", "x"); // Bump lastModified

		target("/v3/test/", archiveId).queryParam("with", "files").request().get();
		checkNames("a,b,c,d");

		target("/v3/test/", archiveId).queryParam("order", "name").request().get();
		checkNames("a,b,c,d");

		target("/v3/test/", archiveId).queryParam("order", "type").request().get();
		checkNames("c,b,a,d");

		target("/v3/test/", archiveId).queryParam("order", "size").request().get();
		checkNames("a,c,b,d");

		target("/v3/test/", archiveId).queryParam("order", "created").request().get();
		checkNames("a,b,d,c");

		target("/v3/test/", archiveId).queryParam("order", "modified").request().get();
		checkNames("b,d,c,a");

		target("/v3/test/", archiveId).queryParam("order", "hash").request().get();
		checkNames("d,a,c,b");

		target("/v3/test/", archiveId).queryParam("order", "id").request().get();
		// IDs change very time so we have to control-sort them at runtime.
		final List<String> names = Arrays.asList(a, b, c, d)
			.stream()
			.sorted((f1, f2) -> f1.id.compareTo(f2.id))
			.map(f -> f.name)
			.collect(Collectors.toList());
		checkNames(Utils.join(",", names));

		target("/v3/test/", archiveId).queryParam("order", "notSupported").request().get();
		assertStatus(Status.BAD_REQUEST);

	}

	@Test
	public void testParamReversed() {
		// These files will be sorted differently for each supported mode.
		addFile("a", "text/x-c", "x");
		addFile("b", "text/x-b", "xxx");
		addFile("c", "text/x-a", "xx");
		addFile("d", "text/x-d", "xxxx");

		target("/v3/test/", archiveId).queryParam("reverse", "").request().get();
		checkNames("d,c,b,a");
	}

	@Test
	public void testParamLimitOffset() {
		// These files will be sorted differently for each supported mode.
		for (int i = 0; i < 10; i++)
			addFile("" + i, "text/plain", "text");

		final WebTarget baseTarget = target("/v3/test/", archiveId);

		baseTarget.queryParam("limit", "3").request().get();
		checkNames("0,1,2");
		baseTarget.queryParam("limit", "3").queryParam("offset", "2").request().get();
		checkNames("2,3,4");
		baseTarget.queryParam("limit", "5").queryParam("offset", "8").request().get();
		checkNames("8,9");

		baseTarget.queryParam("limit", "-3").request().get();
		assertStatus(Status.BAD_REQUEST);
		baseTarget.queryParam("offset", "-3").request().get();
		assertStatus(Status.BAD_REQUEST);
	}

	@Test
	public void testSilentlyCapLimitParam() {
		// These files will be sorted differently for each supported mode.
		addFile("foo", "text/plain", "text");

		final WebTarget baseTarget = target("/v3/test/", archiveId);

		baseTarget.queryParam("limit", "99999").request().get();
		checkNames("foo");
		baseTarget.queryParam("limit", Integer.toString(Integer.MAX_VALUE)).request().get();
		checkNames("foo");
	}

	@Test
	public void testGetEmbeddedSubresources() {
		addFile("foo", "text/plain", "text");

		final WebTarget baseTarget = target("/v3/test/", archiveId);

		baseTarget.queryParam("with", "acl").request().get();
		assertTrue(findJson("meta").isMissingNode());
		assertTrue(findJson("files").isMissingNode());
		assertEquals("OWNER", getJsonString("acl.$owner.0"));

		baseTarget.queryParam("with", "meta").request().get();
		assertTrue(findJson("acl").isMissingNode());
		assertTrue(findJson("files").isMissingNode());
		assertTrue(findJson("meta").isObject());

		baseTarget.queryParam("with", "files").request().get();
		assertTrue(findJson("acl").isMissingNode());
		assertTrue(findJson("meta").isMissingNode());
		assertTrue(findJson("files").isArray());

		baseTarget.queryParam("with", "acl", "meta", "files").request().get();
		assertEquals("OWNER", getJsonString("acl.$owner.0"));
		assertTrue(findJson("meta").isObject());
		assertTrue(findJson("files").isArray());

		baseTarget.queryParam("with", "acl,meta,files").request().get();
		assertEquals("OWNER", getJsonString("acl.$owner.0"));
		assertTrue(findJson("meta").isObject());
		assertTrue(findJson("files").isArray());
	}

	/**
	 * Requests for embedded sub-resources should not fail the entire request if
	 * only a sub-resource is unavailable.
	 */
	@Test
	public void testProtectedEmbeddedSubresources() {
		addFile("foo", "text/plain", "text");

		// Give "test" only load access
		final FormDataMultiPart formData = new FormDataMultiPart();
		formData.field("acl:test2", "load");
		target("/v3/test/", archiveId, "/").request().post(Entity.entity(formData, formData.getMediaType()));

		// Create test2 user and login
		getAuthRealm().account("test2").password("test2").withPermissions("vault:test:*");
		authBasic("test2", "test2");

		target("/v3/test/", archiveId)
			.queryParam("with", "acl", "meta", "files").request().get();
		assertStatus(200);
		assertTrue(findJson("acl").isMissingNode());
		assertTrue(findJson("meta").isMissingNode());
		assertTrue(findJson("files").isMissingNode());
	}

	@Test
	public void testGetACL() {
		final WebTarget baseTarget = target("/v3/test/", archiveId);
		baseTarget.queryParam("acl", "").request().get();
		assertEquals("OWNER", getJsonString("$owner.0"));
	}

}
