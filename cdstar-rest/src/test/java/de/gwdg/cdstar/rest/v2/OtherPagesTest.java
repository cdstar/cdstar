package de.gwdg.cdstar.rest.v2;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response.Status;

import org.junit.Test;

public class OtherPagesTest extends ApiTestBase {

	@Test
	public void testIndexPage() throws NoSuchAlgorithmException, IOException {
		GET("/v2/test/");
		assertStatus(Status.OK);
		assertEquals(MediaType.TEXT_HTML_TYPE.withCharset("utf-8"), getLastResponse().getMediaType());
		// TODO: Fill in a soon as the index page does something...
	}

}
