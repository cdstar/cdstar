package de.gwdg.cdstar.rest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;

import org.apache.commons.io.IOUtils;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.HttpUrlConnectorProvider;
import org.glassfish.jersey.logging.LoggingFeature;
import org.glassfish.jersey.logging.LoggingFeature.Verbosity;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.message.MessageUtils;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.rules.TemporaryFolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.jakarta.rs.json.JacksonJsonProvider;

import de.gwdg.cdstar.SharedObjectMapper;
import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.auth.simple.SimpleAuthorizer;
import de.gwdg.cdstar.config.MapConfig;
import de.gwdg.cdstar.rest.servlet.CDStarServletInitializer;
import de.gwdg.cdstar.runtime.CDStarRuntime;
import de.gwdg.cdstar.runtime.Config;
import de.gwdg.cdstar.server.jetty.JettyServer;
import de.gwdg.cdstar.utils.test.TestLogger;
import jakarta.ws.rs.Priorities;
import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.client.ClientRequestContext;
import jakarta.ws.rs.client.ClientRequestFilter;
import jakarta.ws.rs.client.ClientResponseContext;
import jakarta.ws.rs.client.ClientResponseFilter;
import jakarta.ws.rs.client.WebTarget;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response.Status;

/**
 * Test framework to test against the REST API, or test the API itself.
 *
 * This base class starts a fresh cdstar runtime (including HTTP REST transport)
 * for each test.
 */
public class BaseRestTest {

	private static final Logger log = LoggerFactory.getLogger(BaseRestTest.class);

	@Rule
	public TestLogger logger = new TestLogger("de.gwdg");

	private JettyServer server;
	private Client client;
	protected String authHeader;
	protected ClientRequestContext lastRequest;
	protected ClientResponseContext lastResponse;

	@Rule
	public TemporaryFolder testFolder = new TemporaryFolder();

	private File deployFolder;

	private CDStarRuntime runtime;

	private SimpleAuthorizer systemRealm;

	private static String oldValueJerseyFix;

	@BeforeClass
	public static void fixJerseyClient() {
		oldValueJerseyFix = System.getProperty("sun.net.http.allowRestrictedHeaders");
		System.setProperty("sun.net.http.allowRestrictedHeaders", "true");
	}

	@AfterClass
	public static void unfixJerseyClient() {
		if (oldValueJerseyFix == null)
			System.clearProperty("sun.net.http.allowRestrictedHeaders");
		else
			System.setProperty("sun.net.http.allowRestrictedHeaders", oldValueJerseyFix);
	}

	@Before
	public void setup() throws Exception {
		deployFolder = getNewTempFolder();

		final Config cfg = new MapConfig();
		cfg.set("http.host", "127.0.0.1");
		cfg.set("http.port", "0");
		cfg.set("path.home", deployFolder.toString());
		cfg.set("vault.test.create", "true");

		initConfig(cfg);

		runtime = CDStarRuntime.bootstrap(cfg);
		initRuntime(runtime);
		runtime.start();

		systemRealm = Utils.first(runtime.lookupAll(SimpleAuthorizer.class), a -> a.getDomain().equals("system"));
		assertNotNull("Runtime should install system realm", systemRealm);
		assertEquals(1, runtime.getAuthConfig().listRealms().size());
		assertEquals(systemRealm, runtime.getAuthConfig().listRealms().get(0));
		systemRealm.account("test").password("test").withPermissions("vault:test:*");

		authBasic("test", "test");

		server = new JettyServer(cfg.get("http.host"), cfg.getInt("http.port"));
		server.addComponent(new CDStarServletInitializer(runtime));
		server.start();
	}

	@After
	public void shutdownServer() throws Exception {
		server.stop();
		runtime.close();
		Utils.closeQuietly(runtime);
		if (client != null)
			client.close();
	}

	public void initRuntime(CDStarRuntime runtime) throws Exception {
	}

	public void initConfig(Config config) {
	}

	public CDStarRuntime getRuntime() {
		return runtime;
	}

	public SimpleAuthorizer getAuthRealm() {
		return systemRealm;
	}

	protected URI getBaseUri() {
		return server.getURI();
	}

	public File getNewTempFolder() {
		try {
			return testFolder.newFolder();
		} catch (final IOException e) {
			throw Utils.wtf(e);
		}
	}

	public void configureClient(ClientConfig config) {

		config.register(new ClientRequestFilter() {
			@Override
			public void filter(ClientRequestContext requestContext) throws IOException {
				if (authHeader != null) {
					assert !requestContext.getHeaders().containsKey("Authorization");
					requestContext.getHeaders().add("Authorization", authHeader);
				}
			}
		}, Priorities.HEADER_DECORATOR);

		config.register(new ClientRequestFilter() {
			@Override
			public void filter(ClientRequestContext requestContext) throws IOException {
				// Prevent java.net.URLConnection (used as jax-rs backend) to
				// add a default accept header, which contains "text/html".
				requestContext.getHeaders().putIfAbsent("Accept", Arrays.asList());
			}
		}, Priorities.HEADER_DECORATOR);

		config.register(new ClientRequestFilter() {
			@Override
			public void filter(ClientRequestContext requestContext) throws IOException {
				lastRequest = requestContext;
				lastResponse = null;
			}
		});

		config.register(new ClientResponseFilter() {
			@Override
			public void filter(ClientRequestContext requestContext, ClientResponseContext responseContext)
				throws IOException {
				if (!lastRequest.equals(requestContext)) {
					throw new IllegalStateException("Race condition in test framework.");
				}
				lastResponse = responseContext;
			}
		});

		config.register(new ClientResponseFilter() {
			@Override
			public void filter(ClientRequestContext requestContext, ClientResponseContext responseContext)
				throws IOException {
				if (!responseContext.hasEntity())
					return;
				final File tmp = testFolder.newFile();
				try (OutputStream target = new FileOutputStream(tmp)) {
					IOUtils.copy(responseContext.getEntityStream(), target);
				}
				final MarkableFileInputStream buff = new MarkableFileInputStream(new FileInputStream(tmp));
				buff.mark(-1);
				responseContext.setEntityStream(buff);
			}
		});

		config.register(JacksonJsonProvider.class);
		config.register(MultiPartFeature.class);
		config.register(
			new LoggingFeature(java.util.logging.Logger.getLogger(BaseRestTest.class.getName()),
				Level.INFO, Verbosity.PAYLOAD_TEXT, Integer.valueOf(10240)));
	}

	public Client client() {

		if (client != null)
			return client;

		final ClientConfig clientConfig = new ClientConfig();
		clientConfig.property(HttpUrlConnectorProvider.SET_METHOD_WORKAROUND, Boolean.TRUE);
		configureClient(clientConfig);

		return client = ClientBuilder.newClient(clientConfig);
	}

	public WebTarget target() {
		return client().target(getBaseUri());
	}

	public WebTarget target(String path) {
		return target().path(path);
	}

	public WebTarget target(String... parts) {
		return target(Utils.join(null, parts));
	}

	public void authBasic(String name, String password) {
		authHeader = "Basic "
			+ Base64.getEncoder().encodeToString((name + ":" + password).getBytes(StandardCharsets.UTF_8));
	}

	public void logout() {
		authHeader = null;
	}

	public void authBearer(String token) {
		authHeader = "Bearer " + token;
	}

	protected ClientResponseContext getLastResponse() {
		return lastResponse;
	}

	protected String getEntity() {
		assertNotNull(getLastResponse());
		return new String(getEntityBytes(), MessageUtils.getCharset(getLastResponse().getMediaType()));
	}

	protected <T> T getEntity(Class<T> class1) {
		assertEquals(MediaType.APPLICATION_JSON_TYPE, getLastResponse().getMediaType());
		try {
			return SharedObjectMapper.json.readValue(getEntityBytes(), class1);
		} catch (final JsonProcessingException e) {
			fail("Response parsing failed:" + e.getMessage());
		} catch (final IOException e) {
			fail("Some IO error bubbled up? Strange...");
		}
		return null;
	}

	protected byte[] getEntityBytes() {
		try {
			final InputStream stream = getLastResponse().getEntityStream();
			try {
				stream.reset();
			} catch (final IOException e) {
				log.error("Could not reset response test stream. Not buffered?", e);
			}
			return IOUtils.toByteArray(stream);
		} catch (final IOException e) {
			if (getLastResponse().hasEntity())
				log.error("Failed to read entity stream.", e);
			return new byte[] {};
		}
	}

	protected void assertStatus(int code) {
		assertEquals(code, getLastResponse().getStatus());
	}

	protected void assertStatus(Status status) {
		assertStatus(status.getStatusCode());
	}

	protected void assertError(int code, String errName) {
		assertStatus(code);
		assertEquals(errName, getJsonString("error"));
	}

	protected void assertError(Status status, String errName) {
		assertError(status.getStatusCode(), errName);
	}

	protected JsonNode getJson() {
		assertEquals(MediaType.APPLICATION_JSON_TYPE, getLastResponse().getMediaType());

		try {
			return SharedObjectMapper.json.readTree(getEntity());
		} catch (final JsonProcessingException e) {
			fail("Response parsing failed:" + e.getMessage());
		}
		return null;
	}

	protected JsonNode findJson(final String path) {
		JsonNode node = getJson();
		for (final String part : path.split("(?<!\\.)\\.(?!\\.)")) {
			if (part.matches("[0-9]+")) {
				node = node.path(Integer.parseInt(part));
			} else {
				node = node.path(part.replace("..", "."));
			}
		}
		return node;
	}

	protected JsonNode getJson(final String path) {
		final JsonNode node = findJson(path);
		if (node.isMissingNode()) {
			fail("Requested node " + path + " does not exist. Full object:\n" + getJson().toString());
		}
		return node;
	}

	public String getJsonString(final String path) {
		final JsonNode node = getJson(path);
		assertTrue("Node " + path + " not a string: " + node, node.isTextual());
		return node.textValue();
	}

	public List<String> getJsonStrings(final String path) {
		final JsonNode node = getJson(path);
		assertTrue("Node " + path + " not an array", node.isArray());
		final List<String> results = new ArrayList<>();
		for (final JsonNode item : node) {
			assertTrue(item.isTextual());
			results.add(item.textValue());
		}
		return results;
	}

	protected Boolean getJsonBool(final String path) {
		final JsonNode node = getJson(path);
		assertTrue("Node " + path + " not a string", node.isBoolean());
		return node.booleanValue();
	}

	protected long getJsonLong(final String path) {
		final JsonNode node = getJson(path);
		assertTrue("Node " + path + " not an integer or long", node.isInt() | node.isLong());
		return node.longValue();
	}

	protected double getJsonDouble(final String path) {
		final JsonNode node = getJson(path);
		assertTrue("Node " + path + " not an integer or long", node.isFloatingPointNumber());
		return node.doubleValue();
	}

	final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");

	protected Date getJsonDate(final String path) {
		try {
			return sdf.parse(getJsonString(path));
		} catch (final ParseException e) {
			throw new RuntimeException(e);
		}
	}

	protected long getJsonArraySize(final String path) {
		final JsonNode node = getJson(path);
		assertTrue("Node " + path + " not an array type", node.isArray());
		return node.size();
	}

	protected List<String> getJsonKeys(final String path) {
		final JsonNode node = getJson(path);
		assertTrue("Node " + path + " not an object", node.isObject());
		return Utils.toArrayList(node.fieldNames());
	}

	protected void assertHeaderEquals(String header, Object expected) {
		if (expected == null)
			assertNull(getLastResponse().getHeaderString(header));
		else
			assertEquals(expected.toString(), getLastResponse().getHeaderString(header));
	}

	public long consumeStream() throws IOException {
		long bytes = 0;
		int read = 0;
		final byte[] b = new byte[1024 * 1024];
		final InputStream stream = getLastResponse().getEntityStream();
		while ((read = stream.read(b)) >= 0) {
			bytes += read;
		}
		return bytes;
	}
}
