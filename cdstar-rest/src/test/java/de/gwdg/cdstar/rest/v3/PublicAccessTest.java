package de.gwdg.cdstar.rest.v3;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response.Status;

import org.glassfish.jersey.media.multipart.FormDataMultiPart;
import org.glassfish.jersey.media.multipart.file.StreamDataBodyPart;
import org.junit.Before;
import org.junit.Test;

import de.gwdg.cdstar.rest.BaseRestTest;
import de.gwdg.cdstar.runtime.Config;

/**
 * Ensure that public vaults can be accessed by unauthorized users.
 */
public class PublicAccessTest extends BaseRestTest {

	@Override
	public void initConfig(Config config) {
		config.set("vault.pub.create", "true");
		config.set("vault.pub.public", "true");
	}

	@Before
	public void assMoreAccounts() {
		getAuthRealm().account("test").withPermissions("vault:pub:create");
		getAuthRealm().account("otherUser").password("otherPassword");
	}

	@Test
	public void testReadAccess() {
		final FormDataMultiPart formData = new FormDataMultiPart();
		formData.field("acl:$any", "READ");
		formData.bodyPart(
			new StreamDataBodyPart("/pub.txt", new ByteArrayInputStream("public".getBytes(StandardCharsets.UTF_8)),
				"example.txt", MediaType.valueOf("text/plain")));
		target("/v3/pub/").request().post(Entity.entity(formData, formData.getMediaType()));
		assertStatus(201);
		final String id = getJsonString("id");

		logout();
		target("/v3/pub/", id, "/pub.txt").request().get();
		assertStatus(200);
		assertEquals("public", getEntity());

		authBasic("otherUser", "otherPassword");
		target("/v3/pub/", id, "/pub.txt").request().get();
		assertStatus(200);
		assertEquals("public", getEntity());
	}

	@Test
	public void testVisibility() {
		target("/v3/").request().get();
		assertStatus(Status.OK);
		assertEquals(2, getJsonArraySize("vaults"));
		assertTrue(getJsonStrings("vaults").containsAll(Arrays.asList("pub", "test")));
	}

	@Test
	public void testWrongPassword() {
		authBasic("test", "wrong!");
		target("/v3/").request().get();
		assertStatus(Status.UNAUTHORIZED);
	}

}
