package de.gwdg.cdstar.rest.v3;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.core.Form;
import jakarta.ws.rs.core.Response.Status;

import org.junit.Test;

import de.gwdg.cdstar.rest.BaseRestTest;

public class ScrollVaultTest extends BaseRestTest {

	public List<String> makeArchives(int n) {
		final List<String> IDs = new ArrayList<>(n);
		while (n-- > 0) {
			target("/v3/test").request().post(null);
			assertStatus(Status.CREATED);
			IDs.add(getJsonString("id"));
		}
		return IDs;
	}

	@Test
	public void testScroll() {
		final List<String> allIDs = makeArchives(10);
		allIDs.sort(null);

		// Test small limit
		target("/v3/test").queryParam("scroll", "").queryParam("limit", "5").request().get();
		assertStatus(200);
		assertEquals(5, getJsonLong("limit"));
		assertEquals(allIDs.subList(0, 5), getJsonStrings("results"));

		// Test large limit (capped by server)
		target("/v3/test").queryParam("scroll", allIDs.get(4)).queryParam("limit", "9999999").request().get();
		assertStatus(200);
		assertEquals(VaultEndpoint.SCROLL_LIMIT, getJsonLong("limit"));
		assertEquals(allIDs.subList(5, 10), getJsonStrings("results"));
	}

	@Test
	public void testScrollStrict() {
		final List<String> allIDs = makeArchives(3);
		allIDs.sort(null);

		String deleted = allIDs.get(0);
		target("/v3/test/", deleted).request().delete();

		String noAccess = allIDs.get(1);
		target("/v3/test/", noAccess).request().post(Entity.form(new Form("acl:$owner", "")));

		// In default mode, these are still visible
		target("/v3/test").queryParam("scroll", "").request().get();
		assertEquals(allIDs, getJsonStrings("results"));

		// In strict mode, these should disappear
		allIDs.remove(deleted);
		allIDs.remove(noAccess);
		target("/v3/test").queryParam("scroll", "").queryParam("strict", "true").request().get();
		assertEquals(allIDs, getJsonStrings("results"));
	}

}
