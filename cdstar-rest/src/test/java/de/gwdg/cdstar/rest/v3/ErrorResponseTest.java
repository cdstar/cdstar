package de.gwdg.cdstar.rest.v3;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;

import de.gwdg.cdstar.pool.PoolError;
import de.gwdg.cdstar.rest.BaseRestTest;
import de.gwdg.cdstar.runtime.CDStarRuntime;
import de.gwdg.cdstar.runtime.client.exc.ConstraintError;
import de.gwdg.cdstar.runtime.client.exc.TemporaryError;
import de.gwdg.cdstar.runtime.listener.SessionListener;

/**
 * Ensure that certain errors produce certain error responses.
 */
public class ErrorResponseTest extends BaseRestTest {
	SessionListener mockedSessionListener;

	@Override
	public void initRuntime(CDStarRuntime runtime) throws Exception {
		mockedSessionListener = Mockito.mock(SessionListener.class);
		runtime.register(mockedSessionListener);
	}

	@Test
	public void testTemporaryError() {
		Mockito.doThrow(new TemporaryError("testMsg", null))
			.when(mockedSessionListener)
			.onPrepare(
				ArgumentMatchers.any());
		target("/v3/test/").request().post(null);
		assertStatus(503);
		assertEquals("TemporaryError", getJsonString("error"));

		// The error message should not be visible to the user
		assertNotEquals("testMsg", getJsonString("message"));
	}

	@Test
	public void testConstraintError() {
		Mockito.doThrow(new ConstraintError("testConstraint"))
			.when(mockedSessionListener)
			.onPrepare(
				ArgumentMatchers.any());
		target("/v3/test/").request().post(null);
		assertStatus(400);
		assertEquals("ConstraintViolation", getJsonString("error"));
		assertEquals("testConstraint", getJsonString("message"));
	}

	@Test
	public void testConflictError() {
		Mockito.doThrow(new PoolError.Conflict("testError"))
			.when(mockedSessionListener)
			.onPrepare(
				ArgumentMatchers.any());
		target("/v3/test/").request().post(null);
		assertStatus(409);
		assertEquals("Conflict", getJsonString("error"));
		assertNotEquals("testError", getJsonString("message"));
	}

}
