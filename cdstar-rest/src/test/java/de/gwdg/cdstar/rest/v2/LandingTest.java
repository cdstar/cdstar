package de.gwdg.cdstar.rest.v2;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response.Status;

import org.junit.Test;

public class LandingTest extends ApiTestBase {

	@Test
	public void testNotFound() throws NoSuchAlgorithmException, IOException {
		target("/v2/test/landing/some-random-stuff").request(MediaType.TEXT_HTML_TYPE).get();
		assertStatus(Status.NOT_FOUND);
		assertTrue(MediaType.TEXT_HTML_TYPE.isCompatible(getLastResponse().getMediaType()));
	}

	static MediaType HTML_MIME = MediaType.TEXT_HTML_TYPE.withCharset("utf-8");

	@Test
	public void testWithBitsreams() throws NoSuchAlgorithmException, IOException {
		final String uid = POST("/v2/test/objects/", null).getJsonString("uid");

		GET("/v2/test/landing/" + uid);
		assertStatus(Status.OK);
		assertEquals(HTML_MIME, getLastResponse().getMediaType());

		POST("/v2/test/bitstreams/" + uid, Entity.entity("foo,bar,baz", MediaType.valueOf("text/csv")));
		final String bitid = getJsonString("bitstreamid");

		PUT("/v2/test/metadata/" + uid, Entity.json("{\"test\": \"valuexxx\", \"test2\": [1,2,\"3\"]}"));

		GET("/v2/test/landing/" + uid);
		assertStatus(Status.OK);
		assertEquals(HTML_MIME, getLastResponse().getMediaType());
		assertInEntity(uid);
		assertInEntity(bitid);
		assertInEntity("test2");
		assertInEntity("valuexxx");
	}

	@Test
	public void testWithCollection() throws NoSuchAlgorithmException, IOException {
		final String uid = POST("/v2/test/objects/?type=COLLECTION", null).getJsonString("uid");

		GET("/v2/test/landing/" + uid);
		assertStatus(Status.OK);
		assertEquals(HTML_MIME, getLastResponse().getMediaType());

		PUT("/v2/test/collections/" + uid, Entity.json("[\"coll-1\", \"coll-2\", \"coll-3\"]"));
		assertStatus(Status.CREATED);

		GET("/v2/test/landing/" + uid);
		assertStatus(Status.OK);
		assertEquals(HTML_MIME, getLastResponse().getMediaType());
		assertInEntity(uid);
		assertInEntity("coll-1");
		assertInEntity("coll-2");
	}
}
