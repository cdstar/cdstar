package de.gwdg.cdstar.rest.v2;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;

import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.core.EntityTag;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response.Status;

import org.apache.commons.codec.binary.Hex;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataMultiPart;
import org.glassfish.jersey.media.multipart.MultiPart;
import org.junit.Test;

public class BitstreamTests extends ApiTestBase {

	@Test
	public void testNotFound() throws NoSuchAlgorithmException, IOException {
		final String uid = POST("/v2/test/objects/", null).getJsonString("uid");

		GET("/v2/test/objects/" + uid);
		assertTrue(getJson("bitstream").isArray());
		assertEquals(0, getJson("bitstream").size());

		for (final String id : "0 1 test".split(" ")) {
			GET("/v2/test/bytestreams/" + uid + "/" + id);
			assertStatus(Status.NOT_FOUND);
		}
	}

	@Test
	public void testUploadRaw() {
		final String uid = POST("/v2/test/objects/", null).getJsonString("uid");
		POST("/v2/test/bitstreams/" + uid, Entity.entity("foo,bar,baz", "text/csv"));
		final String bitid = getJsonString("bitstreamid");
		assertEquals(getLastResponse().getLocation().getPath(), "/v2/test/bitstreams/" + uid + "/" + bitid);

		GET("/v2/test/bitstreams/" + uid + "/" + bitid);
		assertStatus(Status.OK);
		assertEquals(MediaType.valueOf("text/csv"), getLastResponse().getMediaType());
		assertEquals("foo,bar,baz", getEntity());
	}

	@Test
	public void testUploadMultipart() throws UnsupportedEncodingException {
		final String uid = POST("/v2/test/objects/", null).getJsonString("uid");

		final String bitid = uploadFile(uid, "bitstreams", "test.bin", "text/csv", "foo,bar,baz");

		assertStatus(Status.CREATED);
		assertEquals(uid, getJsonString("uid"));
		assertTrue(getJsonBool("ok"));
		assertEquals(getLastResponse().getLocation().getPath(), "/v2/test/bitstreams/" + uid + "/" + bitid);

		GET("/v2/test/bitstreams/" + uid + "/" + bitid);
		assertStatus(Status.OK);
		assertEquals(MediaType.valueOf("text/csv"), getLastResponse().getMediaType());
		assertEquals("foo,bar,baz", getEntity());
	}

	@Test
	public void testUploadMultipartNoValidField() throws UnsupportedEncodingException {
		final String uid = POST("/v2/test/objects/", null).getJsonString("uid");

		uploadFile(uid, "WRONG_FIELD_NAME", "test.bin", "text/cvs", "foo,bar,baz");
		assertEquals(400, getLastResponse().getStatus());
	}

	@Test
	public void testUpdateBitsream() throws NoSuchAlgorithmException, IOException {
		final String uid = POST("/v2/test/objects/", null).getJsonString("uid");

		final String bitid = uploadFile(uid, "bitstreams", "test.bin", "text/cvs", "foo,bar,baz");

		resetTimer();
		PUT("/v2/test/bitstreams/" + uid + "/" + bitid, Entity.text("updated data"));
		assertStatus(Status.OK);

		GET("/v2/test/bitstreams/" + uid + "/" + bitid);
		assertStatus(Status.OK);
		assertEquals(MediaType.TEXT_PLAIN_TYPE, getLastResponse().getMediaType());
		assertEquals("updated data", getEntity());

		GET("/v2/test/objects/" + uid);
		assertEquals(1, getJson("bitstream").size());
		assertEquals("text/plain", getJsonString("bitstream." + bitid + ".content-type"));
		assertAfterTimer(getJsonDate("bitstream." + bitid + ".last-modified"));

		final MessageDigest md = MessageDigest.getInstance(getJsonString("bitstream." + bitid + ".checksum-algorithm"));
		final String checksum = getJsonString("bitstream." + bitid + ".checksum");

		GET("/v2/test/bitstreams/" + uid + "/" + bitid);
		md.update(getEntity().getBytes());
		assertEquals(Hex.encodeHexString(md.digest()), checksum);
	}

	@Test
	public void testDeleteBitstream() {
		final String uid = POST("/v2/test/objects/", null).getJsonString("uid");

		final String bitid = uploadFile(uid, "bitstreams", "test.bin", "text/cvs", "foo,bar,baz");

		DELETE("/v2/test/bitstreams/" + uid + "/" + bitid);
		assertStatus(Status.NO_CONTENT);

		GET("/v2/test/bitstreams/" + uid + "/" + bitid);
		assertStatus(Status.NOT_FOUND);

		GET("/v2/test/objects/" + uid);
		assertEquals(0, getJson("bitstream").size());
	}

	@Test
	public void testMetaData() throws NoSuchAlgorithmException, IOException {
		final String uid = POST("/v2/test/objects/", null).getJsonString("uid");

		GET("/v2/test/objects/" + uid);
		assertStatus(Status.OK);
		assertTrue(getJson("bitstream").isArray());
		assertEquals(0, getJson("bitstream").size());

		resetTimer();
		final String bitid = uploadFile(uid, "bitstreams", "test.bin", "text/test", "foo,bar,baz");

		GET("/v2/test/objects/" + uid);
		assertStatus(Status.OK);
		assertTrue(getJson("bitstream").isArray());
		assertEquals(1, getJson("bitstream").size());
		assertEquals("text/test", getJsonString("bitstream." + bitid + ".content-type"));
		assertTrue(getJson("bitstream." + bitid + ".checksum").isTextual());
		assertTrue(getJson("bitstream." + bitid + ".checksum-algorithm").isTextual());
		assertAfterTimer(getJsonDate("bitstream." + bitid + ".last-modified"));

		final MessageDigest md = MessageDigest.getInstance(getJsonString("bitstream." + bitid + ".checksum-algorithm"));
		final String checksum = getJsonString("bitstream." + bitid + ".checksum");

		GET("/v2/test/bitstreams/" + uid + "/" + bitid);
		md.update(getEntity().getBytes());
		assertEquals(Hex.encodeHexString(md.digest()), checksum);
	}

	@Test
	public void testGet() throws NoSuchAlgorithmException, IOException, InterruptedException {
		final String uid = POST("/v2/test/objects/", null).getJsonString("uid");

		final String bitid = uploadFile(uid, "bitstreams", "test.bin", "text/test", "foo,bar,baz");

		GET("/v2/test/bitstreams/" + uid + "/" + bitid);
		assertStatus(Status.OK);
		assertEquals("text/test", getLastResponse().getMediaType().toString());
		assertEquals("foo,bar,baz", getEntity());
	}

	@Test
	public void testGetNotModified() throws NoSuchAlgorithmException, IOException {
		final String uid = POST("/v2/test/objects/", null).getJsonString("uid");

		final String bitid = uploadFile(uid, "bitstreams", "test.bin", "text/test", "abc,def,ghi");

		GET("/v2/test/bitstreams/" + uid + "/" + bitid);
		final EntityTag etag = getLastResponse().getEntityTag();
		final Date mtime = getLastResponse().getLastModified();

		send(prepare("/v2/test/bitstreams/" + uid + "/" + bitid).header("If-None-Match", etag).buildGet());
		assertStatus(Status.NOT_MODIFIED);

		send(prepare("/v2/test/bitstreams/" + uid + "/" + bitid).header("If-Modified-Since", mtime).buildGet());
		assertStatus(Status.NOT_MODIFIED);
	}

	private String uploadFile(final String uid, final String fieldname, final String filename, final String contentType,
			final String content) {
		final FormDataBodyPart file = new FormDataBodyPart();
		file.setEntity(content.getBytes());
		file.setMediaType(MediaType.valueOf(contentType));
		file.setFormDataContentDisposition(FormDataContentDisposition.name(fieldname).fileName(filename).build());
		@SuppressWarnings("resource")
		final MultiPart payload = new FormDataMultiPart().bodyPart(file);
		POST("/v2/test/bitstreams/" + uid, Entity.entity(payload, payload.getMediaType()));
		if (getLastResponse().getStatusInfo().getFamily() == Status.Family.SUCCESSFUL) {
			return lastLocationPathSegment();
		}
		return null;
	}
}
