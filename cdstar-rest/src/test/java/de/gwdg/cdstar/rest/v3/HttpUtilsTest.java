package de.gwdg.cdstar.rest.v3;

import static org.junit.Assert.assertEquals;

import java.util.List;
import java.util.Map;

import org.junit.Test;

import de.gwdg.cdstar.rest.utils.RangeHeader;
import de.gwdg.cdstar.rest.utils.RestUtils;

public class HttpUtilsTest {

	@Test
	public void testRangeParser() {
		RangeHeader range;
		range = new RangeHeader("bytes=3-6", 100);
		assertEquals(3, range.getStartIndex());
		assertEquals(7, range.getEndIndex());
		assertEquals(4, range.getRangeLength());
		assertEquals(100, range.getTotalLength());
		assertEquals("bytes 3-6/100", range.toString());

		range = new RangeHeader("bytes=-6", 100);
		assertEquals(94, range.getStartIndex());
		assertEquals(100, range.getEndIndex());
		assertEquals(6, range.getRangeLength());
		assertEquals(100, range.getTotalLength());
		assertEquals("bytes 94-99/100", range.toString());

		range = new RangeHeader("bytes=3-", 100);
		assertEquals(3, range.getStartIndex());
		assertEquals(100, range.getEndIndex());
		assertEquals(97, range.getRangeLength());
		assertEquals(100, range.getTotalLength());
		assertEquals("bytes 3-99/100", range.toString());
	}

	@Test
	public void runQueryParserTest() {
		Map<String, List<String>> params = RestUtils.splitQuery("=5&=6&foo&bar=7&foo=8");
		assertEquals("[5, 6]", params.get("").toString());
		assertEquals("[, 8]", params.get("foo").toString());
		assertEquals("[7]", params.get("bar").toString());

		params = RestUtils.splitQuery("=5");
		assertEquals("[5]", params.get("").toString());

		params = RestUtils.splitQuery("foo");
		assertEquals("[]", params.get("foo").toString());

		params = RestUtils.splitQuery("foo=");
		assertEquals("[]", params.get("foo").toString());

		params = RestUtils.splitQuery("");
		assertEquals(0, params.size());
	}

}
