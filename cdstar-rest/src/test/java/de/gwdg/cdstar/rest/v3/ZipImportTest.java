package de.gwdg.cdstar.rest.v3;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.apache.commons.compress.archivers.ArchiveOutputStream;
import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveOutputStream;
import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.compress.archivers.zip.ZipArchiveOutputStream;
import org.junit.AssumptionViolatedException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import de.gwdg.cdstar.rest.BaseRestTest;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.core.Response.Status;

@RunWith(Parameterized.class)
public class ZipImportTest extends BaseRestTest {

	@Parameters(name = "{0}")
	public static Collection<String> mediaType() {
		return Arrays.asList(new String[] { "application/zip", "application/x-tar" });
	}

	private final String mediaType;
	private List<FileEntry> testArchive;

	private static class FileEntry {
		String name;
		byte[] content;
		private final String meidaType;

		public FileEntry(String name, String mediaType, byte[] content) {
			this.name = name;
			meidaType = mediaType;
			this.content = content;
		}

		public long getSize() {
			return content.length;
		}

	}

	public ZipImportTest(String meidaType) throws IOException, URISyntaxException {
		mediaType = meidaType;
	}

	@Before
	public void makeImportFile() throws IOException, URISyntaxException {
		testArchive = new ArrayList<>();
		testArchive.add(new FileEntry("example.txt", "text/plain", "example".getBytes()));
		testArchive.add(new FileEntry("folder/file.json", "application/json", "{\"test\": \"value\"}".getBytes()));
		testArchive.add(new FileEntry("i18n/測試.txt", "text/plain", "測試".getBytes()));
	}

	File makeZip() throws IOException, URISyntaxException {
		if (mediaType.equals("application/zip")) {
			return makeZipFile();
		} else if (mediaType.equals("application/x-tar")) {
			return makeTarFile();
		} else {
			fail("Unsupported media type: " + mediaType);
			return null;
		}
	}

	File makeTarFile() throws IOException, URISyntaxException {
		final File out = testFolder.newFile();
		try (ArchiveOutputStream zip = new TarArchiveOutputStream(new FileOutputStream(out))) {
			for (final FileEntry file : testArchive) {
				final TarArchiveEntry e = new TarArchiveEntry(file.name);
				e.setSize(file.getSize());
				zip.putArchiveEntry(e);
				zip.write(file.content);
				zip.closeArchiveEntry();
			}
		}
		return out;
	}

	File makeZipFile() throws IOException, URISyntaxException {
		final File out = testFolder.newFile();
		try (ZipArchiveOutputStream zip = new ZipArchiveOutputStream(new FileOutputStream(out))) {
			for (final FileEntry file : testArchive) {
				final ZipArchiveEntry e = new ZipArchiveEntry(file.name);
				e.setSize(file.getSize());
				zip.putArchiveEntry(e);
				zip.write(file.content);
				zip.closeArchiveEntry();
			}
		}
		return out;
	}

	private void assertPresent(FileEntry file) {
		assertEquals(testArchive.size(), getJsonLong("file_count"));
		for (int i = 0; i < getJsonLong("file_count"); i++) {
			if (getJsonString("files." + i + ".name").equals(file.name)) {
				assertEquals(file.getSize(), getJsonLong("files." + i + ".size"));
				assertEquals(file.meidaType, getJsonString("files." + i + ".type"));
				return;
			}
		}
		fail("Could not find: " + file.name);
	}

	@Test
	public void testImport() throws IOException, URISyntaxException {
		target("/v3/test/").request().post(Entity.entity(makeZip(), mediaType));
		assertStatus(Status.CREATED);
		final String id = getJsonString("id");
		assertEquals("/v3/test/" + id, getLastResponse().getLocation().getPath());

		target("/v3/test/" + id).queryParam("order", "name").request().get();
		assertStatus(Status.OK);
		assertEquals(id, getJsonString("id"));
		assertEquals(3, getJsonLong("file_count"));
		testArchive.forEach(this::assertPresent);
	}

	@Test
	public void testUpdate() throws IOException, URISyntaxException {
		target("/v3/test/").request().post(null);
		final String id = getJsonString("id");

		target("/v3/test/", id).request().post(Entity.entity(makeZip(), mediaType));
		assertStatus(Status.OK);

		target("/v3/test/" + id).queryParam("order", "name").request().get();
		assertStatus(Status.OK);
		assertEquals(id, getJsonString("id"));
		assertEquals(3, getJsonLong("file_count"));
		testArchive.forEach(this::assertPresent);
	}

	@Test
	public void testUpdateFailsIfFilesExist() throws IOException, URISyntaxException {
		target("/v3/test/").request().post(null);
		final String id = getJsonString("id");
		target("/v3/test/", id, "/example.txt").request().put(Entity.text("hello"));
		target("/v3/test/", id).request().post(Entity.entity(makeZip(), mediaType));
		assertStatus(Status.BAD_REQUEST);
	}

	@Test
	public void testInvalidFilenames() throws IOException, URISyntaxException {
		target("/v3/test/").request().post(null);
		final String id = getJsonString("id");

		testArchive.clear();
		testArchive.add(new FileEntry("", "text/plain", "example".getBytes()));
		target("/v3/test/", id).request().post(Entity.entity(makeZip(), mediaType));
		assertStatus(Status.BAD_REQUEST);

		testArchive.clear();
		testArchive.add(new FileEntry("../foo", "text/plain", "example".getBytes()));
		target("/v3/test/", id).request().post(Entity.entity(makeZip(), mediaType));
		assertStatus(Status.BAD_REQUEST);
	}

	@Test
	public void testBadUnicodeZip() throws IOException, URISyntaxException {
		final File out = testFolder.newFile();

		if (mediaType.equals("application/zip")) {
			try (ZipArchiveOutputStream zip = new ZipArchiveOutputStream(new FileOutputStream(out))) {
				zip.setEncoding("latin1");
				zip.setFallbackToUTF8(false);
				zip.putArchiveEntry(new ZipArchiveEntry("schöner_mist.txt"));
				zip.closeArchiveEntry();
			}
		} else if (mediaType.equals("application/x-tar")) {
			try (TarArchiveOutputStream zip = new TarArchiveOutputStream(new FileOutputStream(out), "latin1")) {
				zip.putArchiveEntry(new TarArchiveEntry("schöner_mist.txt"));
				zip.closeArchiveEntry();
			}
			throw new AssumptionViolatedException("TAR implementation cannot me bade to fail on encoding errors...");
		} else {
			fail("Unsupported media type: " + mediaType);
		}

		target("/v3/test/").request().post(Entity.entity(out, mediaType));
		assertError(Status.BAD_REQUEST, "InvalidImportFormat");

	}

	@Test
	public void testInclude() throws IOException, URISyntaxException {
		testArchive.clear();
		testArchive.add(new FileEntry("foo.pdf", "application/pdf", "example".getBytes()));
		testArchive.add(new FileEntry("bar.png", "image/png", "example".getBytes()));
		testArchive.add(new FileEntry("baz.txt", "text/plain", "example".getBytes()));

		target("/v3/test/").queryParam("include", "*.pdf").request().post(Entity.entity(makeZip(), mediaType));
		assertStatus(Status.CREATED);
		final String id = getJsonString("id");

		target("/v3/test/" + id).queryParam("order", "name").queryParam("with", "files").request().get();
		assertStatus(Status.OK);
		assertEquals(1, getJsonLong("file_count"));
		assertEquals("foo.pdf", getJsonString("files.0.name"));
	}

	@Test
	public void testExclude() throws IOException, URISyntaxException {
		testArchive.clear();
		testArchive.add(new FileEntry("foo.pdf", "application/pdf", "example".getBytes()));
		testArchive.add(new FileEntry("bar.png", "image/png", "example".getBytes()));
		testArchive.add(new FileEntry("baz.txt", "text/plain", "example".getBytes()));

		target("/v3/test/").queryParam("exclude", "*.pdf").request().post(Entity.entity(makeZip(), mediaType));
		assertStatus(Status.CREATED);
		final String id = getJsonString("id");

		target("/v3/test/" + id).queryParam("order", "name").queryParam("with", "files").request().get();
		assertStatus(Status.OK);
		assertEquals(2, getJsonLong("file_count"));
		assertEquals("bar.png", getJsonString("files.0.name"));
		assertEquals("baz.txt", getJsonString("files.1.name"));
	}

	@Test
	public void testIncludeExclude() throws IOException, URISyntaxException {
		testArchive.clear();
		testArchive.add(new FileEntry("foo.pdf", "application/pdf", "example".getBytes()));
		testArchive.add(new FileEntry("bar.png", "image/png", "example".getBytes()));
		testArchive.add(new FileEntry("baz.txt", "text/plain", "example".getBytes()));

		target("/v3/test/").queryParam("include", "ba*").queryParam("exclude", "*.txt").request()
			.post(Entity.entity(makeZip(), mediaType));
		assertStatus(Status.CREATED);
		final String id = getJsonString("id");

		target("/v3/test/" + id).queryParam("order", "name").queryParam("with", "files").request().get();
		assertStatus(Status.OK);
		assertEquals(1, getJsonLong("file_count"));
		assertEquals("bar.png", getJsonString("files.0.name"));
	}

}
