package de.gwdg.cdstar.rest.v3;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Map;

import org.junit.Test;

import de.gwdg.cdstar.rest.BaseRestTest;
import de.gwdg.cdstar.runtime.services.FeatureRegistry;
import jakarta.ws.rs.core.Response.Status;

public class ServiceApiTest extends BaseRestTest {

	@Test
	public void testFeatureFlags() {
		getRuntime().lookupRequired(FeatureRegistry.class).addFeature("testfeature", Map.of("foo", "bar"));
		target("/v3/").request().get();
		assertStatus(Status.OK);
		assertEquals("bar", getJsonString("features.testfeature.foo"));
	}


	@Test
	public void testListVaults() {
		target("/v3/").request().get();
		assertStatus(Status.OK);
		assertEquals(1, getJsonArraySize("vaults"));
		assertEquals("test", getJsonString("vaults.0"));
	}

	@Test
	public void testServiceHealthAsGuest() {
		target("/v3/_health").request().get();
		assertStatus(Status.OK);
		assertEquals("OK", getJsonString("status"));
		assertTrue(getJson().path("checks").isMissingNode());
	}

	@Test
	public void testServiceHealthAsUser() {
		getAuthRealm().account("health").password("health").withPermissions("service:health");
		authBasic("health", "health");
		target("/v3/_health").request().get();
		assertStatus(Status.OK);
		assertEquals("OK", getJsonString("status"));
		assertFalse(getJson().path("checks").isMissingNode());
	}



}
