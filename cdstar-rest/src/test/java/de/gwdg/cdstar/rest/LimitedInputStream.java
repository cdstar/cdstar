package de.gwdg.cdstar.rest;

import java.io.IOException;
import java.io.InputStream;

public class LimitedInputStream extends InputStream {

	private final InputStream original;
	private final long limit;
	private long consumed;

	public LimitedInputStream(InputStream original, long maxSize) {
		this.original = original;
		limit = maxSize;
	}

	@Override
	public int read() throws IOException {
		final byte[] buff = new byte[1];
		return read(buff) > 0 ? buff[0] + 128 : -1;
	}

	@Override
	public int read(byte b[]) throws IOException {
		return read(b, 0, b.length);
	}

	@Override
	public synchronized int read(byte b[], int off, int len) throws IOException {
		int maxRead = len - off;
		if (maxRead > limit - consumed)
			maxRead = (int) (limit - consumed);
		if (maxRead == 0)
			return -1;
		final int read = original.read(b, off, maxRead);
		if (read <= 0)
			return -1;
		consumed += read;
		return read;
	}

}
