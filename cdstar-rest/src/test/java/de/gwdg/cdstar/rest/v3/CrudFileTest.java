package de.gwdg.cdstar.rest.v3;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import org.junit.Test;

import com.fasterxml.jackson.databind.JsonNode;

import de.gwdg.cdstar.pool.nio.NioPool;
import de.gwdg.cdstar.rest.BaseRestTest;
import de.gwdg.cdstar.runtime.client.CDStarFile;
import de.gwdg.cdstar.runtime.services.VaultRegistry;
import de.gwdg.cdstar.web.common.model.FileInfo;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.core.Form;
import jakarta.ws.rs.core.Response.Status;

public class CrudFileTest extends BaseRestTest {

	private String makeArchive() {
		target("/v3/test/").request().post(Entity.form(new Form()));
		assertStatus(Status.CREATED);
		return getJsonString("id");
	}

	private FileInfo putFile(String id, String name, String content, String type) {
		target("/v3/test/", id, "/" + name).request().put(Entity.entity(content, type));
		final FileInfo entity = getEntity(FileInfo.class);
		assertEquals(name, entity.name);
		return entity;
	}

	@Test
	public void testFileInfoEqualsEmbedded() {
		final String id = makeArchive();
		putFile(id, "test/file.txt", "Hello World", "text/plain");
		assertStatus(Status.CREATED);

		// No Meta
		target("/v3/test/", id).queryParam("with", "files").request().get();
		JsonNode embedded = getJson("files.0");
		target("/v3/test/", id, "/test/file.txt").queryParam("info", "").request().get();
		JsonNode raw = getJson();
		assertEquals(embedded, raw);

		// With Meta
		final Map<String, String> meta = new HashMap<>();
		meta.put("dc:title", "hello");
		target("/v3/test/", id, "/test/file.txt").queryParam("meta", "").request().put(Entity.json(meta));
		target("/v3/test/", id).queryParam("with", "files,meta").request().get();
		embedded = getJson("files.0");
		target("/v3/test/", id, "/test/file.txt").queryParam("info", "").queryParam("with", "meta").request().get();
		raw = getJson();
		assertEquals(embedded, raw);
	}

	@Test
	public void testOverwriteFile() {
		final String id = makeArchive();
		putFile(id, "test/file.txt", "Hello World", "text/plain");
		assertStatus(Status.CREATED);
		putFile(id, "test/file.txt", "Hello World 2", "text/plain");
		assertStatus(Status.OK);
		final String content = target("/v3/test/", id, "/test/file.txt").request().get().readEntity(String.class);
		assertEquals("Hello World 2", content);
	}

	@Test
	public void testNameConflict() {
		final String id = makeArchive();
		putFile(id, "test", "Hello World", "text/plain");
		assertStatus(Status.CREATED);
		target("/v3/test/", id, "/test/conflict.txt").request().put(Entity.text(""));
		assertError(Status.CONFLICT, "FileNameConflict");

		putFile(id, "conflict/test", "Hello World", "text/plain");
		assertStatus(Status.CREATED);
		target("/v3/test/", id, "/conflict").request().put(Entity.text(""));
		assertError(Status.CONFLICT, "FileNameConflict");

		target("/v3/test/", id).request().get();
		assertEquals(2, getJsonLong("file_count"));
	}

	@Test
	public void testUnicodeNames() {
		var id = makeArchive();
		var name = "測試";
		putFile(id, name, "Hello World", "text/plain");
		assertStatus(Status.CREATED);
		putFile(id, name, "Hello World 2", "text/plain");
		assertStatus(Status.OK);

		target("/v3/test/", id, "/", name).request().get();
		assertStatus(Status.OK);
		target("/v3/test/", id, "/", name).queryParam("info", "true").request().get();
		assertStatus(Status.OK);
		assertEquals("測試", getJsonString("name"));
	}

	@Test
	public void testPreventOverwrite() {
		final String id = makeArchive();
		target("/v3/test/", id, "/test").request().header("If-None-Match", "*").put(Entity.text(""));
		assertStatus(Status.CREATED);
		target("/v3/test/", id, "/test").request().header("If-None-Match", "*").put(Entity.text(""));
		assertError(Status.PRECONDITION_FAILED, "PreconditionFailed");
	}

	@Test
	public void testRequireOverwrite() {
		final String id = makeArchive();
		target("/v3/test/", id, "/test").request().header("If-Match", "*").put(Entity.text(""));
		assertError(Status.PRECONDITION_FAILED, "PreconditionFailed");
		target("/v3/test/", id, "/test").request().put(Entity.text(""));
		assertStatus(Status.CREATED);
		target("/v3/test/", id, "/test").request().header("If-Match", "*").put(Entity.text(""));
		assertStatus(Status.OK);
	}

	@Test
	public void testMissingDigests() throws Exception {
		final String id = makeArchive();

		// Create file with all digests enabled
		putFile(id, "test.txt", "foo", "plain/text");

		// Disable all but SHA256
		VaultRegistry vr = getRuntime().lookupRequired(VaultRegistry.class);
		NioPool pool = (NioPool) vr.getVault("test").map(vr::getPoolFor).get();
		pool.setDigests(Arrays.asList());

		// Assume available digests are shown, even if disabled
		target("/v3/test/", id, "/test.txt").queryParam("info", "").request().get();
		assertStatus(Status.OK);
		assertEquals(new HashSet<>(Arrays.asList("md5", "sha1", "sha256")),
			new HashSet<>(getJsonKeys("digests")));

		// Re-create file with only SHA256 enabled
		putFile(id, "test.txt", "bar", "plain/text");

		// Enable MD5
		pool.setDigests(Arrays.asList(CDStarFile.DIGEST_MD5));

		// Assume only the known digests are displayed
		target("/v3/test/", id, "/test.txt").queryParam("info", "").request().get();
		assertStatus(Status.OK);
		assertEquals(Arrays.asList("sha256"), getJsonKeys("digests"));
	}

}
