package de.gwdg.cdstar.rest.utils.form;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class UrlEncodedParserTest {

	ByteBuffer toBuffer(String str) {
		return ByteBuffer.wrap(str.getBytes(StandardCharsets.UTF_8));
	}

	void assertParsed(List<FormPart> parts, String... expected) {
		assertEquals("Not enough elemts parsed", expected.length, parts.size() * 2);
		for (int i = 0; i + 1 < expected.length; i += 2) {
			final FormPart part = parts.get(i / 2);
			assertTrue(part.isComplete());
			assertEquals(expected[i], part.getName());
			assertEquals(expected[i + 1], part.drainToString(StandardCharsets.UTF_8));
		}
	}

	List<FormPart> parseChunks(String... chunks) throws FormParserException {
		final UrlEncodedParser parser = new UrlEncodedParser(1024, true, StandardCharsets.UTF_8);
		final List<FormPart> parts = new ArrayList<>();
		for (final String chunk : chunks)
			parts.addAll(parser.parse(toBuffer(chunk)));
		parts.addAll(parser.finish());
		return parts;
	}

	@Test
	public void testParseChunked() throws FormParserException {
		assertParsed(parseChunks("foo&bar=xxx"), "foo", "", "bar", "xxx");
		assertParsed(parseChunks("foo", "&bar=xxx"), "foo", "", "bar", "xxx");
		assertParsed(parseChunks("foo&", "bar=xxx"), "foo", "", "bar", "xxx");
		assertParsed(parseChunks("foo", "&", "bar", "=", "xxx"), "foo", "", "bar", "xxx");
	}

	@Test
	public void testEmptyParts() throws FormParserException {
		assertParsed(parseChunks("foo&&bar=xxx"), "foo", "", "bar", "xxx");
		assertParsed(parseChunks("foo&", "&bar=xxx"), "foo", "", "bar", "xxx");
	}

	@Test
	public void testEmptyKeyParameters() throws FormParserException {
		assertParsed(parseChunks("=foo&="), "", "foo", "", "");
	}

	@Test
	public void testParseEscaped() throws FormParserException {
		assertParsed(parseChunks("foo%20"), "foo ", "");
		assertParsed(parseChunks("foo%20+"), "foo  ", "");
	}

}
