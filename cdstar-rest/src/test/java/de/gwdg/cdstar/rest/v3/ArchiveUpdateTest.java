package de.gwdg.cdstar.rest.v3;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.function.BiConsumer;
import java.util.function.BooleanSupplier;

import org.glassfish.jersey.media.multipart.FormDataMultiPart;
import org.glassfish.jersey.media.multipart.file.StreamDataBodyPart;
import org.junit.Before;
import org.junit.Test;

import de.gwdg.cdstar.MimeUtils;
import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.rest.BaseRestTest;
import de.gwdg.cdstar.rest.v3.async.UrlFetchService;
import de.gwdg.cdstar.rest.v3.async.UrlFetchService.FetchHandle;
import de.gwdg.cdstar.runtime.CDStarRuntime;
import de.gwdg.cdstar.runtime.lts.LTSConfig;
import de.gwdg.cdstar.runtime.lts.bagit.BagitTarget;
import de.gwdg.cdstar.runtime.profiles.ProfileRegistry;
import de.gwdg.cdstar.web.common.model.ErrorResponse;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.core.Form;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response.Status;

public class ArchiveUpdateTest extends BaseRestTest {

	private final class TestFetchService implements UrlFetchService {
		@Override
		public boolean canHandle(URI uri) {
			return "base64".equals(uri.getScheme());
		}

		@Override
		public FetchHandle resolve(URI uri) {
			ByteBuffer data;
			try {
				data = ByteBuffer.wrap(Utils.base64decode(uri.getSchemeSpecificPart()));
			} catch (IllegalArgumentException e) {
				throw new ErrorResponse(400, "TusNotFound", "This is not even a TUS id");
			}

			var handle = new TestFetchHandle(data);

			fetchCache.put(uri.getSchemeSpecificPart(), handle);
			return handle;
		}
	}

	private final class TestFetchHandle implements FetchHandle {
		private final ByteBuffer data;
		private boolean closed;

		private TestFetchHandle(ByteBuffer data) {
			this.data = data;
		}

		@Override
		public long size() {
			return data.limit();
		}

		@Override
		public void read(ByteBuffer dst, BiConsumer<Integer, Throwable> handler) {
			var available = data.remaining();
			if (available == 0)
				handler.accept(-1, null);
			else {
				dst.put(data);
				handler.accept(available - data.remaining(), null);
			}
		}

		@Override
		public void close() {
			if(closed)
				throw new AssertionError("Fetch ahndle closed twice");
			data.position(data.limit());
			closed = true;
		}

		boolean isClosed() {
			return closed;
		}
	}

	private FormDataMultiPart formData;
	private Map<String, TestFetchHandle> fetchCache = new HashMap<>();

	@Before
	public void prepareForm() {
		formData = new FormDataMultiPart();
		fetchCache.clear();
	}

	@Override
	public void initRuntime(CDStarRuntime runtime) throws Exception {
		runtime.register(new BagitTarget("zip", runtime.getServiceDir("zip")));
		runtime.register(new TestFetchService());
	}

	@Before
	public void addProfilesRuntime() throws Exception {
		final ProfileRegistry profiles = getRuntime().lookupRequired(ProfileRegistry.class);
		// Add some profiles and the zip mirror adapter to test profile changes
		final Map<String, String> conf = new HashMap<>();
		conf.put(LTSConfig.PROP_NAME, "zip");
		conf.put(LTSConfig.PROP_MODE, LTSConfig.MODE_COLD);
		profiles.defineProfile("test", "zip-cold", conf);

		conf.put(LTSConfig.PROP_MODE, LTSConfig.MODE_HOT);
		profiles.defineProfile("test", "zip-hot", conf);

		conf.clear();
		profiles.defineProfile("test", "local", conf);
	}

	private void field(String name, String value) {
		formData.field(name, value);
	}

	private void file(String name, byte[] bytes, String filename, String contentType) {
		formData.bodyPart(new StreamDataBodyPart("/" + name, new ByteArrayInputStream(bytes),
				filename, MediaType.valueOf(contentType)));
	}

	private void file(String name) {
		file(name, name.getBytes(StandardCharsets.UTF_8), name, "text/plain");
	}

	private String create() {
		if (formData.getBodyParts().size() > 0)
			target("/v3/test/").request().post(Entity.entity(formData, formData.getMediaType()));
		else
			target("/v3/test/").request().post(null);

		assertStatus(Status.CREATED);
		formData = new FormDataMultiPart();
		return getJsonString("id");
	}

	private void update(String id) {
		updateNoCheck(id);
		assertStatus(Status.OK);
	}

	private void updateNoCheck(String id) {
		target("/v3/test/", id).request().post(Entity.entity(formData, formData.getMediaType()));
		formData = new FormDataMultiPart();
	}

	private void load(String id) {
		target("/v3/test/", id).queryParam("order", "name").queryParam("with", "files,meta,acl").request().get();
		assertStatus(Status.OK);
	}

	@Test
	public void testCreateComplex() throws InterruptedException, ExecutionException, IOException, URISyntaxException {
		file("example.txt");
		field("meta:dc:title", "Archive Title");
		field("meta:dc:title:/example.txt", "File Title");
		field("acl:bob", "READ");
		final String id = create();

		load(id);
		assertEquals("example.txt", getJsonString("files.0.name"));
		assertEquals("File Title", getJsonString("files.0.meta.dc:title.0"));
		assertEquals("Archive Title", getJsonString("meta.dc:title.0"));
		assertEquals("READ", getJsonString("acl.bob.0"));
	}

	@Test
	public void testAutodetectMime() throws InterruptedException, ExecutionException, IOException, URISyntaxException {
		file("test.png", new byte[] {}, "test.png", MimeUtils.APPLICATION_X_AUTODETECT);
		final String id = create();
		load(id);
		assertEquals("image/png", getJsonString("files.0.type"));
	}

	@Test
	public void testUpdateComplex() throws InterruptedException, ExecutionException, IOException, URISyntaxException {
		final String id = create();
		file("example.txt");
		field("meta:dc:title", "Archive Title");
		field("meta:dc:title:/example.txt", "File Title");
		field("acl:bob", "READ");
		update(id);

		load(id);
		assertEquals("example.txt", getJsonString("files.0.name"));
		assertEquals("File Title", getJsonString("files.0.meta.dc:title.0"));
		assertEquals("Archive Title", getJsonString("meta.dc:title.0"));
		assertEquals("READ", getJsonString("acl.bob.0"));
	}

	@Test
	public void testUploadUnicodeFileName()
			throws InterruptedException, ExecutionException, IOException, URISyntaxException {
		final String id = create();
		file("t💩 st.txt");
		update(id);

		load(id);
		assertEquals("t💩 st.txt", getJsonString("files.0.name"));
	}

	@Test
	public void testCreateWithProfile() throws Exception {
		field("profile", "zip-cold");
		final String id = create();
		load(id);
		assertEquals("zip-cold", getJsonString("profile"));
		final String state = getJsonString("state");
		assertTrue(Arrays.asList("archived", "pending-archive").contains(state));
	}

	@Test
	public void testUpdateProfile() throws Exception {
		final String id = create();
		load(id);
		assertEquals("default", getJsonString("profile"));
		assertEquals("open", getJsonString("state"));
		final String rev = getJsonString("revision");
		final Date modified = getJsonDate("modified");

		field("profile", "zip-cold");
		update(id);
		assertEquals(rev, getJsonString("revision"));
		load(id);

		// profile changes do not change revision or modified date
		assertEquals(rev, getJsonString("revision"));
		assertEquals(modified, getJsonDate("modified"));

		assertEquals("zip-cold", getJsonString("profile"));
		assertTrue(Arrays.asList("archived", "pending-archive").contains(getJsonString("state")));

		assertTrueEventually(() -> {
			load(id);
			return getJsonString("state").equals("archived");
		}, 10, TimeUnit.SECONDS);

	}

	/**
	 * Test something repeatedly until it returns true. There is a short wait time
	 * between tests (defaults to 100th of maxTime) but other than that, this is a
	 * hot wait. Use with care.
	 */
	private void assertTrueEventually(BooleanSupplier test, long maxTime, TimeUnit unit) {
		long now = System.currentTimeMillis();
		final long timeout = now + unit.toMillis(maxTime);
		final long sleepMillis = (timeout - now) / 100;
		while ((now = System.currentTimeMillis()) < timeout) {
			if (test.getAsBoolean())
				return;
			if (!Utils.sleepInterruptable(sleepMillis)) {
				fail("Test thread interrupted");
				return;
			}
		}
		fail("Test failed within given time frame of " + maxTime + unit);
	}

	@Test
	public void testUpdateTypeOnly() throws InterruptedException, ExecutionException, IOException, URISyntaxException {
		final String id = create();
		file("example.txt");
		update(id);
		load(id);
		assertEquals("text/plain", getJsonString("files.0.type"));

		field("type:/example.txt", "image/png");
		update(id);
		load(id);
		assertEquals("image/png", getJsonString("files.0.type"));

		field("type:/example.txt", MimeUtils.APPLICATION_X_AUTODETECT);
		update(id);
		load(id);
		assertEquals("text/plain", getJsonString("files.0.type"));
	}

	@Test
	public void testUploadViaTextField() {
		final String id = create();
		field("/example.txt", "Hello World");
		update(id);
		load(id);
		assertEquals("example.txt", getJsonString("files.0.name"));
		assertEquals("text/plain", getJsonString("files.0.type"));
		assertEquals("Hello World", target("/v3/test/", id, "/example.txt").request().get(String.class));
	}

	@Test
	public void testUploadExisting()
			throws InterruptedException, ExecutionException, IOException, URISyntaxException {
		file("example.txt");
		final String id = create();
		load(id);
		assertEquals(1, getJsonLong("file_count"));

		file("example.txt");
		updateNoCheck(id);
		assertError(Status.CONFLICT, "FileExists");
	}

	@Test
	public void testDelete()
			throws InterruptedException, ExecutionException, IOException, URISyntaxException {
		file("example.txt");
		final String id = create();
		load(id);
		assertEquals(1, getJsonLong("file_count"));

		field("delete:/example.txt", "/example.txt");
		update(id);
		load(id);
		assertEquals(0, getJsonLong("file_count"));
	}

	@Test
	public void testDeletePath()
			throws InterruptedException, ExecutionException, IOException, URISyntaxException {
		file("a/example.txt");
		file("a/example2.txt");
		file("b/example.txt");
		file("ab/example.txt");
		final String id = create();

		field("delete:/a/", "/a/");
		update(id);
		load(id);
		assertEquals(2, getJsonLong("file_count"));
		assertEquals("ab/example.txt", getJsonString("files.0.name"));
		assertEquals("b/example.txt", getJsonString("files.1.name"));
	}

	@Test
	public void testDeleteNotFound()
			throws InterruptedException, ExecutionException, IOException, URISyntaxException {

		final String id = create();

		field("delete:notfound", "notfound");
		updateNoCheck(id);
		assertStatus(Status.BAD_REQUEST);

		assertEquals("delete:notfound", getJsonString("detail.form"));
		assertEquals("notfound", getJsonString("detail.value"));
	}

	@Test
	public void testCopy() {
		file("example.txt");
		field("meta:dc:title:/example.txt", "File Title");
		final String id = create();

		field("copy:/target.txt", "/example.txt");
		update(id);
		load(id);

		assertEquals("example.txt", getJsonString("files.0.name"));
		assertEquals("target.txt", getJsonString("files.1.name"));
		assertEquals(getJsonString("files.0.digests.sha256"), getJsonString("files.1.digests.sha256"));
		assertTrue(getJson("files.1.meta").path("dc:title").isMissingNode());
	}

	@Test
	public void testFetch() {
		final String id = create();
		var payload = "test";
		var tudId = Utils.base64encode(payload);
		field("fetch:/target.txt", "base64:" + tudId);
		update(id);
		load(id);

		assertEquals("target.txt", getJsonString("files.0.name"));
		assertEquals(Utils.sha256("test"), getJsonString("files.0.digests.sha256"));
		assertTrue(fetchCache.containsKey(tudId));
		assertTrue(fetchCache.get(tudId).isClosed());
	}

	@Test
	public void testFetchError() {
		final String id = create();

		field("fetch:/target.txt", "http://foo");
		updateNoCheck(id);
		assertError(501, "NotImplemented");

		field("fetch:/target.txt", "base64:bad.input");
		updateNoCheck(id);
		assertError(400, "TusNotFound");
	}

	@Test
	public void testClone() {
		file("example.txt");
		field("meta:dc:title:/example.txt", "File Title");
		final String id = create();

		field("clone:/target.txt", "/example.txt");
		update(id);
		load(id);

		assertEquals("example.txt", getJsonString("files.0.name"));
		assertEquals("target.txt", getJsonString("files.1.name"));
		assertEquals(getJsonString("files.0.digests.sha256"), getJsonString("files.1.digests.sha256"));
		assertEquals(getJsonString("files.0.meta.dc:title.0"), getJsonString("files.1.meta.dc:title.0"));

	}

	@Test
	public void testMove() {
		file("example.txt");
		field("meta:dc:title:/example.txt", "File Title");
		final String id = create();

		field("move:/target.txt", "/example.txt");
		update(id);
		load(id);

		assertEquals("target.txt", getJsonString("files.0.name"));
		assertEquals("File Title", getJsonString("files.0.meta.dc:title.0"));
	}

	@Test
	public void testMissingSource() {
		final String id = create();
		for (final String cmd : new String[] { "copy", "clone", "move" }) {
			field(cmd + ":/target.txt", "/missing.txt");
			updateNoCheck(id);
			assertStatus(Status.BAD_REQUEST);
			assertEquals(cmd + ":/target.txt", getJsonString("detail.form"));
			assertEquals("/missing.txt", getJsonString("detail.value"));
		}
	}

	@Test
	public void testTargetExists() {
		file("source.txt");
		file("exists.txt");
		final String id = create();
		for (final String cmd : new String[] { "copy", "clone", "move" }) {
			field(cmd + ":/exists.txt", "/source.txt");
			updateNoCheck(id);
			assertError(Status.CONFLICT, "FileExists");
		}
	}

	@Test
	public void testFileNameConflict() {
		file("exists");
		file("source.txt");
		final String id = create();

		file("exists/conflict.txt");
		updateNoCheck(id);
		assertError(Status.CONFLICT, "FileNameConflict");

		for (final String cmd : new String[] { "copy", "clone", "move" }) {
			field(cmd + ":/exists/conflict.txt", "/source.txt");
			updateNoCheck(id);
			assertError(Status.CONFLICT, "FileNameConflict");
		}
	}

	@Test
	public void testUrlEncoded() {
		for (final String cmd : new String[] { "copy", "clone", "move" }) {
			file("source.txt");
			final String id = create();

			final Form form = new Form();
			form.param(cmd + ":/target.txt", "/source.txt");
			target("/v3/test/", id).request().post(Entity.form(form));
			assertStatus(Status.OK);

			load(id);
			if (cmd.equals("move"))
				assertEquals("target.txt", getJsonString("files.0.name"));
			else
				assertEquals("target.txt", getJsonString("files.1.name"));
		}
	}

	@Test
	public void testNoChange() {
		final String id = create();
		load(id);
		final String rev = getJsonString("revision");

		target("/v3/test/", id).request().post(null);
		assertStatus(Status.OK);
		assertEquals(rev, getJsonString("revision"));

		load(id);
		assertEquals(rev, getJsonString("revision"));
	}

	@Test
	public void testACL() {
		field("acl:$any", "READ");
		field("acl:$user", "READ");
		field("acl:$owner", "OWNER");
		field("acl:@group", "READ");
		field("acl:subject", "READ,change_acl");
		field("acl:subject", "read_acl");
		final String id = create();
		load(id);

		assertEquals("READ", getJsonString("acl.$any.0"));
		assertEquals("READ", getJsonString("acl.$user.0"));
		assertEquals("OWNER", getJsonString("acl.$owner.0"));
		assertEquals("READ", getJsonString("acl.@group.0"));
		assertTrue(getJsonStrings("acl.subject").contains("READ"));
		assertTrue(getJsonStrings("acl.subject").contains("change_acl"));
		assertTrue(getJsonStrings("acl.subject").contains("read_acl"));
		assertEquals(5, getJson("acl").size());
	}

	@Test
	public void testChangeOwner() {
		field("acl:$any", "READ,read_acl");
		field("acl:$owner", "ADMIN"); // OWNER is not enough to change_owner
		final String id = create();
		field("owner", "some.dude@company");
		update(id);
		load(id);

		assertEquals("READ", getJsonString("acl.$any.0"));
		assertEquals("ADMIN", getJsonString("acl.$owner.0"));
		assertEquals("some.dude@company", getJsonString("owner"));
	}

	@Test
	public void testMetaAddValue() {
		field("meta:foo", "a");
		field("meta:foo", "b");
		field("meta:foo", "c");
		final String id = create();
		load(id);

		assertEquals("a", getJsonString("meta.foo.0"));
		assertEquals("b", getJsonString("meta.foo.1"));
		assertEquals("c", getJsonString("meta.foo.2"));

		field("meta:foo", "a");
		field("meta:foo", "b");
		field("meta:foo", "c"); // There was a bug that caused the 'c' to be
								// missing after the update.
		field("meta:foo", "d");

		update(id);
		load(id);

		assertEquals("a", getJsonString("meta.foo.0"));
		assertEquals("b", getJsonString("meta.foo.1"));
		assertEquals("c", getJsonString("meta.foo.2"));
		assertEquals("d", getJsonString("meta.foo.3"));

	}

}
