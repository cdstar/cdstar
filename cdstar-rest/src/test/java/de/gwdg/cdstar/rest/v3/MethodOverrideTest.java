package de.gwdg.cdstar.rest.v3;

import static org.junit.Assert.assertFalse;

import jakarta.ws.rs.core.Response.Status;

import org.junit.Test;

import de.gwdg.cdstar.rest.BaseRestTest;
import de.gwdg.cdstar.rest.servlet.MethodOverrideFilter;

public class MethodOverrideTest extends BaseRestTest {

	@Test
	public void testDeleteOverride() throws Exception {
		target("/v3/test/").request().post(null);
		assertStatus(Status.CREATED);
		final String id = getJsonString("id");

		target("/v3/test/", id).request()
			.header(MethodOverrideFilter.HTTP_OVERRIDE_HEADER, "DELETE")
			.post(null);

		assertStatus(Status.NO_CONTENT);
		target("/v3/test/", id).request().get();
		assertStatus(Status.NOT_FOUND);
	}

	@Test
	public void testIgnoreIfNotPost() throws Exception {
		target("/v3/test/").request()
			.header(MethodOverrideFilter.HTTP_OVERRIDE_HEADER, "DELETE")
			.get();
		assertStatus(Status.OK);
		assertFalse(getJson("public").isMissingNode()); // should be a GetVault
														// response
	}

	@Test
	public void testIgnoreIfUnsupportedOverride() throws Exception {
		target("/v3/test/").request()
			.header(MethodOverrideFilter.HTTP_OVERRIDE_HEADER, "GET")
			.post(null);
		assertStatus(Status.CREATED); // Should stay POST and create an archive
										// instead
	}
}
