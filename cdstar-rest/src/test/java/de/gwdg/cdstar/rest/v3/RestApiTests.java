package de.gwdg.cdstar.rest.v3;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.ExecutionException;

import org.apache.commons.io.input.NullInputStream;
import org.glassfish.jersey.client.ClientProperties;
import org.glassfish.jersey.media.multipart.FormDataMultiPart;
import org.glassfish.jersey.media.multipart.file.StreamDataBodyPart;
import org.junit.Assume;
import org.junit.Test;
import org.slf4j.LoggerFactory;

import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.rest.BaseRestTest;
import de.gwdg.cdstar.rest.TestDataStream;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;
import jakarta.ws.rs.core.Variant;

public class RestApiTests extends BaseRestTest {

	private String currentArchive;

	protected String makeArchive() {
		target("/v3/test/").request().post(null);
		assertStatus(Status.CREATED);
		currentArchive = getJsonString("id");
		return currentArchive;
	}

	protected void uploadFile(String name, InputStream input, String mtype) {
		target("/v3/test/", currentArchive, "/", name).request().put(
			Entity.entity(input, mtype != null ? mtype : MediaType.APPLICATION_OCTET_STREAM));
		assertStatus(Status.CREATED);
	}

	/*
	 * Vault level APIs
	 */

	@Test
	public void testCreateEmptyArchive() {
		target("/v3/test/").request().post(null);
		assertStatus(Status.CREATED);
		final String id = getJsonString("id");
		assertEquals(getBaseUri().resolve("v3/test/" + id).getPath(), getLastResponse().getLocation().getPath());

		target("/v3/test/" + id).request().get();
		assertStatus(Status.OK);
		assertEquals(id, getJsonString("id"));
		assertEquals(0, getJsonLong("file_count"));
	}

	@Test
	public void testCreateArchiveWithSetId() {
		target("/v3/test/").queryParam("_setid", "0123456789abcdef").request().post(null);
		assertStatus(Status.CREATED);
		final String id = getJsonString("id");
		assertEquals("0123456789abcdef", id);

		target("/v3/test/").queryParam("_setid", "invalid id").request().post(null);
		assertError(400, "ConstraintViolation");
	}

	@Test
	public void testRemoveEmptyArchive() {
		target("/v3/test/").request().post(null);
		assertStatus(Status.CREATED);
		final String id = getJsonString("id");

		target("/v3/test/" + id).request().delete();
		assertStatus(Status.NO_CONTENT);
	}

	@Test
	public void testCreateFromForm() throws InterruptedException, ExecutionException {

		final FormDataMultiPart formData = new FormDataMultiPart();
		formData.bodyPart(new StreamDataBodyPart("/example.txt", new ByteArrayInputStream("example".getBytes()),
			"example.txt", MediaType.TEXT_PLAIN_TYPE));
		formData.bodyPart(new StreamDataBodyPart("/folder/", new ByteArrayInputStream("file".getBytes()), "file.txt",
			MediaType.TEXT_PLAIN_TYPE));

		target("/v3/test/").request().post(Entity.entity(formData, MediaType.MULTIPART_FORM_DATA));

		assertStatus(Status.CREATED);
		final String id = getJsonString("id");
		assertEquals("/v3/test/" + id, getLastResponse().getLocation().getPath());

		target("/v3/test/" + id).queryParam("order", "name").request().get();
		assertStatus(Status.OK);
		assertEquals(id, getJsonString("id"));
		assertEquals(2, getJsonLong("file_count"));
		assertEquals("example.txt", getJsonString("files.0.name"));
		assertEquals("folder/file.txt", getJsonString("files.1.name"));

		target("/v3/test/" + id + "/folder/file.txt").request().get();
		assertEquals("file", getEntity());
	}

	@Test
	public void testCreateFromGarbage() throws InterruptedException, ExecutionException {
		target("/v3/test/").request()
			.post(Entity.entity("%xx".getBytes(), MediaType.APPLICATION_FORM_URLENCODED));
		assertStatus(Status.BAD_REQUEST);
		target("/v3/test/").request()
			.post(Entity.entity("not-multipart".getBytes(), "multipart/form-data; no-boundary=xxx"));
		assertStatus(Status.BAD_REQUEST);
		target("/v3/test/").request()
			.post(Entity.entity("not-multipart".getBytes(), "multipart/form-data; boundary=xxx"));
		assertStatus(Status.BAD_REQUEST);
	}

	/*
	 * Archive level APIs
	 */

	@Test
	public void testNotFound() {
		target("/v3/test/", "x").request().get();
		assertStatus(Status.NOT_FOUND);

		target("/v3/test/", Utils.bytesToHex(Utils.randomBytes(1024))).request().get();
		assertStatus(Status.NOT_FOUND);

		target("/v3/test/", "qwertzqwertzqwertzqwertz").request().get();
		assertStatus(Status.NOT_FOUND);

		target("/v3/test/", "../../etc/").request().get();
		assertStatus(Status.NOT_FOUND);
	}

	@Test
	public void testDirectUpload()
			throws InterruptedException, ExecutionException, IOException, NoSuchAlgorithmException {
		final long fileSize = 1024l * 1024 * 10 + 13; // ~ 10MB

		client().property(ClientProperties.CHUNKED_ENCODING_SIZE, Integer.valueOf(10240));
		client().property(ClientProperties.REQUEST_ENTITY_PROCESSING, "CHUNKED");

		target("/v3/test/").request().post(null);
		assertStatus(Status.CREATED);
		final String id = getJsonString("id");

		final TestDataStream bigfile = new TestDataStream(fileSize);

		target("/v3/test/", id, "/bigfile.bin").request().put(
			Entity.entity(bigfile, MediaType.APPLICATION_OCTET_STREAM));

		assertStatus(Status.CREATED);
		target("/v3/test/" + id).queryParam("order", "name").request().get();
		assertStatus(Status.OK);
		assertEquals(id, getJsonString("id"));
		assertEquals(1, getJsonLong("file_count"));
		assertEquals("bigfile.bin", getJsonString("files.0.name"));
		assertEquals(fileSize, getJsonLong("files.0.size"));
		assertEquals(Utils.bytesToHex(bigfile.digest("SHA-256")), getJsonString("files.0.digests.sha256"));

		final Response rs = target("/v3/test/", id, "/bigfile.bin").request().get();
		assertEquals(fileSize, Long.valueOf(rs.getHeaderString("Content-Length")).longValue());

		assertEquals(fileSize, consumeStream());
	}

	@Test
	public void testBigDirectUpload() throws NoSuchAlgorithmException {
		Assume.assumeTrue(System.getProperty("testMode", "").contains("stress"));

		final long size = 1024l * 1024 * 1024 * 4 + 13;
		target("/v3/test/").request().post(null);
		assertStatus(Status.CREATED);
		final String id = getJsonString("id");
		final TestDataStream bigfile = new TestDataStream(size);

		client().property(ClientProperties.CHUNKED_ENCODING_SIZE, Integer.valueOf(10240));
		client().property(ClientProperties.REQUEST_ENTITY_PROCESSING, "CHUNKED");

		final long t1 = System.nanoTime();
		target("/v3/test/", id, "/bigfile.bin").request().put(
			Entity.entity(bigfile, MediaType.APPLICATION_OCTET_STREAM));
		final double s = 0.000000001 * (System.nanoTime() - t1);
		final double mb = (float) size / 1024 / 1024;
		System.out.println("MB/s: " + (mb / s));
	}

	private void testDirectUpload(String id, long size) throws NoSuchAlgorithmException {
		final TestDataStream bigfile = new TestDataStream(size);
		target("/v3/test/", id, "/bigfile.bin").request().put(
			Entity.entity(bigfile, MediaType.APPLICATION_OCTET_STREAM));
		assertEquals(size, getJsonLong("size"));
		assertEquals(Utils.bytesToHex(bigfile.digest("SHA-256")), getJsonString("digests.sha256"));
	}

	@Test
	public void testBufferBorders() throws NoSuchAlgorithmException {
		target("/v3/test/").request().post(null);
		assertStatus(Status.CREATED);
		final String id = getJsonString("id");

		testDirectUpload(id, 1);
		testDirectUpload(id, 0);
		testDirectUpload(id, 1);
		testDirectUpload(id, 2);
		testDirectUpload(id, 3);

		for (int i = -3; i <= 3; i++)
			testDirectUpload(id, 1024 + i);

		for (int i = -3; i <= 3; i++)
			testDirectUpload(id, 1024 * 8 + i);

	}

	@Test
	public void testDirectUploadCompressed() throws InterruptedException, ExecutionException, IOException {
		// These are 10240 \NULL values, gzip compressed.
		final ByteArrayInputStream gzip = new ByteArrayInputStream(Utils.base64decode("H4sIAAAAAAAAAA=="));

		target("/v3/test/").request().post(null);
		assertStatus(Status.CREATED);
		final String id = getJsonString("id");
		target("/v3/test/", id, "/bigfile.bin").request().put(
			Entity.entity(gzip, new Variant(MediaType.APPLICATION_OCTET_STREAM_TYPE, (String) null, "gzip")));
		assertStatus(Status.UNSUPPORTED_MEDIA_TYPE);
	}

	@Test
	public void testDownload() throws InterruptedException, ExecutionException, IOException {
		final int fileSize = 1024 * 1024;
		makeArchive();
		uploadFile("file.bin", new NullInputStream(fileSize), null);

		LoggerFactory.getLogger(getClass()).warn("XXXXXXXXXXXXXXXXXXXXXXXXXXX");

		target("/v3/test/", currentArchive, "/file.bin").request().get();
		assertHeaderEquals("Content-Length", String.valueOf(fileSize));
		assertEquals(fileSize, consumeStream());
	}

	@Test
	public void testHead() throws InterruptedException, ExecutionException, IOException {
		final int fileSize = 1024 * 1024;
		makeArchive();
		uploadFile("file.bin", new NullInputStream(fileSize), null);

		target("/v3/test/", currentArchive, "/file.bin").request().head();
		assertHeaderEquals("Content-Length", String.valueOf(fileSize));
		assertEquals(0, consumeStream());
	}

	@Test
	public void testDelete() throws InterruptedException, ExecutionException, IOException {
		final int fileSize = 1024 * 1024;
		makeArchive();
		uploadFile("file.bin", new NullInputStream(fileSize), null);

		target("/v3/test/", currentArchive, "/file.bin").request().delete();
		assertStatus(Status.NO_CONTENT);

		target("/v3/test/", currentArchive, "/file.bin").request().head();
		assertStatus(Status.NOT_FOUND);
	}

	@Test
	public void testPartialDownload() throws InterruptedException, ExecutionException, IOException {
		final int fileSize = 100;
		makeArchive();
		uploadFile("file.bin", new NullInputStream(fileSize), null);

		target("/v3/test/", currentArchive, "/file.bin").request().header("Range", "bytes=3-6").get();
		assertStatus(Status.PARTIAL_CONTENT);
		assertHeaderEquals("Content-Range", "bytes 3-6/100");
		assertHeaderEquals("Content-Length", "4");
		assertEquals(4, consumeStream());

		target("/v3/test/", currentArchive, "/file.bin").request().header("Range", "bytes=-6").get();
		assertStatus(Status.PARTIAL_CONTENT);
		assertHeaderEquals("Content-Range", "bytes 94-99/100");
		assertHeaderEquals("Content-Length", "6");
		assertEquals(6, consumeStream());

		target("/v3/test/", currentArchive, "/file.bin").request().header("Range", "bytes=XXX").get();
		assertStatus(Status.REQUESTED_RANGE_NOT_SATISFIABLE);
		target("/v3/test/", currentArchive, "/file.bin").request().header("Range", "apples=3-6").get();
		assertStatus(Status.REQUESTED_RANGE_NOT_SATISFIABLE);
		target("/v3/test/", currentArchive, "/file.bin").request().header("Range", "bytes=103-106").get();
		assertStatus(Status.REQUESTED_RANGE_NOT_SATISFIABLE);
	}

}
