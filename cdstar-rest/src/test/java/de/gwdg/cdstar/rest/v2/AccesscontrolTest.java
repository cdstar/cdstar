package de.gwdg.cdstar.rest.v2;

import java.io.IOException;

import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.core.Response.Status;

import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;

public class AccesscontrolTest extends ApiTestBase {

	/**
	 * Authorization for the read case has to work like this: If a user occupies
	 * a role that is part of the AC "read"-field, he can read the object,
	 * HTTP-Code 200 Everyone else, who is not part of the "read"-field will get
	 * an HTTP-Code 401 Unauthorized
	 */
	@Test
	public void testReadPermissions() throws JsonProcessingException, IOException {
		// Set Authorization Stuff
		final String uid = POST("/v2/test/objects/", null).getJsonString("uid");

		// As Owner, we can read the object - HTTP Status code 200
		GET("/v2/test/objects/" + uid);
		assertStatus(Status.OK);

		// As anyone else, we are not allowed to read the object
		getAuthRealm().account("BadGuy").password("123456").withRoles("SomeOtherRole");
		authBasic("BadGuy", "123456");

		GET("/v2/test/objects/" + uid);
		assertStatus(Status.FORBIDDEN);
	}

	/**
	 * Authorization for the write case has to work like this: If a user
	 * occupies a role that is part of the AC "write"-field, he can write the
	 * object, HTTP-Code 201 The write operation will be tested with metadata.
	 * Everyone else, who is not part of the "write"-field will get an HTTP-Code
	 * 401 Unauthorized
	 */
	@Test
	public void testWritePermissions() throws JsonProcessingException, IOException {

		// Set Authorization Stuff
		final String uid = POST("/v2/test/objects/", null).getJsonString("uid");

		// As anyone else, we are not allowed to read the object
		getAuthRealm().account("BadGuy").password("123456").withRoles("SomeOtherRole");
		authBasic("BadGuy", "123456");

		final String json = "{\"test\": \"value\", \"test2\": [1,2,\"3\"]}";
		PUT("/v2/test/metadata/" + uid, Entity.json(json));
		assertStatus(Status.FORBIDDEN);
	}

	/**
	 * Authorization for the change owner case has to work like this: Owner can
	 * change the permissions of the object Owners are not allowed to change the
	 * owner - only managed can do that If a user occupies the role that is part
	 * of the AC "owner"-field, he can change the object permissions, HTTP-Code
	 * 200. Everyone else, who is not in the "owner"-field will get an HTTP-Code
	 * 401 Unauthorized Authorization for the the role manage looks like this
	 * Managers can change the permissions of the object and the ownership of it
	 * If a user occupies the role that is part of the AC "manage"-field, he can
	 * change the object permissions, HTTP-Code 200. If a user occupies the role
	 * that is part of the AC "manage"-field, he can change the object
	 * ownership, HTTP-Code 200.
	 */
	@Test
	public void testChangePermissions() throws JsonProcessingException, IOException {

		// Set Authorization Stuff
		final String uid = POST("/v2/test/objects/", null).getJsonString("uid");

		// By default the creator is also manager.
		// Since 3.0: By default we are only OWNER and cannot change the
		// owner.

		// No we can no longer change the owner
		String json = "{\"owner\": \"elvis@static\", \"manage\": [\"admin@static\"], \"read\": [\"TestUser@static\"], \"write\": []}";
		PUT("/v2/test/accesscontrol/" + uid, Entity.json(json));
		assertStatus(Status.FORBIDDEN);

		// Or the manager
		json = "{\"owner\": \"TestUser@static\", \"manage\": [\"TestUser@static\"], \"read\": [\"TestUser@static\"], \"write\": []}";
		PUT("/v2/test/accesscontrol/" + uid, Entity.json(json));
		assertStatus(Status.FORBIDDEN);

		// Or any other permission (this was allowed in CDStar-v2)
		json = "{\"owner\": \"TestUser@static\", \"manage\": [\"admin@static\"], \"read\": [\"TestUser@static\"], \"write\": []}";
		PUT("/v2/test/accesscontrol/" + uid, Entity.json(json));
		assertStatus(Status.FORBIDDEN);

		// The manager, however, can change everything
		getAuthRealm().account("admin").password("123456").withPermissions("vault:*:read", "archive:*:*:*");
		authBasic("admin", "123456");

		json = "{\"owner\": \"elvis@static\", \"manage\": [\"elvis@static\"], \"read\": [\"elvis@static\"], \"write\": [\"The king@static\"]}";
		PUT("/v2/test/accesscontrol/" + uid, Entity.json(json));
		assertStatus(Status.OK);
	}

}
