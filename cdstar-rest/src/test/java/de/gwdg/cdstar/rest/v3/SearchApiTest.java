package de.gwdg.cdstar.rest.v3;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Arrays;

import jakarta.ws.rs.core.Response.Status;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;

import de.gwdg.cdstar.Promise;
import de.gwdg.cdstar.rest.BaseRestTest;
import de.gwdg.cdstar.runtime.CDStarRuntime;
import de.gwdg.cdstar.runtime.search.SearchException;
import de.gwdg.cdstar.runtime.search.SearchHit;
import de.gwdg.cdstar.runtime.search.SearchProvider;
import de.gwdg.cdstar.runtime.search.SearchResult;

public class SearchApiTest extends BaseRestTest {

	private SearchProvider mockedSearchProvider;

	@Override
	public void initRuntime(CDStarRuntime runtime) throws Exception {
		mockedSearchProvider = Mockito.mock(SearchProvider.class);
		getRuntime().register(mockedSearchProvider);
	}

	@Before
	public void prepareArchive() throws IOException, URISyntaxException {
		target("/v3/test/").request().post(null);
		assertStatus(Status.CREATED);
		getJsonString("id");
	}

	SearchResult mockResult(int count, long total, SearchHit... hits) {
		final SearchResult mockResult = Mockito.mock(SearchResult.class);
		Mockito.when(mockResult.getSize()).thenReturn(Integer.valueOf(count));
		Mockito.when(mockResult.getTotal()).thenReturn(Long.valueOf(total));
		Mockito.when(mockResult.hits()).thenReturn(Arrays.asList(hits));
		return mockResult;
	}

	SearchHit mockHit(String id, String type, double score) {
		final SearchHit mockHit = Mockito.mock(SearchHit.class);
		Mockito.when(mockHit.getId()).thenReturn(id);
		Mockito.when(mockHit.getType()).thenReturn(type);
		Mockito.when(mockHit.getScore()).thenReturn(score);
		return mockHit;
	}

	@Test
	public void testSearchProviderDiscovery() throws SearchException {
		final SearchResult mockedResult = mockResult(2, 3, mockHit("id1", "type1", 3.14),
			mockHit("id2", "type2", 4.14));

		Mockito.when(mockedSearchProvider.search(ArgumentMatchers.any())).thenReturn(Promise.of(mockedResult));

		target("/v3/test").queryParam("q", "foo").request().get();
		assertStatus(Status.OK);
		assertEquals(2, getJsonLong("count"));
		assertEquals(3, getJsonLong("total"));
		assertEquals("id1", getJsonString("hits.0.id"));
		assertEquals("type1", getJsonString("hits.0.type"));
		assertEquals(3.14, getJsonDouble("hits.0.score"), 0);

		assertEquals("id2", getJsonString("hits.1.id"));
		assertEquals("type2", getJsonString("hits.1.type"));
		assertEquals(4.14, getJsonDouble("hits.1.score"), 0);

	}

}
