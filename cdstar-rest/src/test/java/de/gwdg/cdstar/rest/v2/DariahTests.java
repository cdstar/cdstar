package de.gwdg.cdstar.rest.v2;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response.Status;

import org.junit.Test;

public class DariahTests extends ApiTestBase {

	private static final String document = "<xml>boooring</xml>";

	public String makeObject() {
		POST("/dariah/test/", Entity.xml(document));
		assertStatus(Status.CREATED);
		return lastLocationPathSegment();
	}

	@Test
	public void testUploadBasic() throws NoSuchAlgorithmException, IOException {

		POST("/dariah/test/", Entity.xml(document));

		assertStatus(Status.CREATED);
		assertFalse(getLastResponse().hasEntity());
		assertAfterTimer(getLastResponse().getLastModified());
		final String uid = lastLocationPathSegment();
		assertEquals("/dariah/test/" + uid, getLastResponse().getLocation().getPath());
	}

	@Test
	public void testUploadBasicWithPidAndVersion() throws NoSuchAlgorithmException, IOException {

		send(prepare("/dariah/test/").header("PID", "EhhPID").header("Version", "13").buildPost(Entity.xml(document)));
		assertStatus(Status.CREATED);
		assertEquals("", getEntity());
		assertAfterTimer(getLastResponse().getLastModified());
		assertEquals("/dariah/test/" + lastLocationPathSegment(), getLastResponse().getLocation().getPath());

		GET("/v2/test/metadata/" + lastLocationPathSegment());
		assertStatus(Status.OK);
		assertEquals("EhhPID", getJsonString("PID"));
		assertEquals("13", getJsonString("Version"));
	}

	@Test
	public void testHead() throws NoSuchAlgorithmException, IOException {
		final String uid = makeObject();

		send(prepare("/dariah/test/" + uid).build("HEAD"));
		assertStatus(Status.OK);
		assertFalse(getLastResponse().hasEntity());
		assertEquals(MediaType.APPLICATION_XML_TYPE, getLastResponse().getMediaType());
		assertEquals(document.length(), getLastResponse().getLength());
		assertAfterTimer(getLastResponse().getLastModified());
	}

	@Test
	public void testGet() throws NoSuchAlgorithmException, IOException {
		final String uid = makeObject();

		GET("/dariah/test/" + uid);
		assertStatus(Status.OK);
		assertEquals(document, getEntity());
		assertEquals(MediaType.APPLICATION_XML_TYPE, getLastResponse().getMediaType());
		assertEquals(document.length(), getLastResponse().getLength());
		assertAfterTimer(getLastResponse().getLastModified());
	}

	@Test
	public void testPut() throws NoSuchAlgorithmException, IOException {
		final String uid = makeObject();

		resetTimer();
		PUT("/dariah/test/" + uid, Entity.json("{'json':'rocks'}"));

		assertStatus(Status.CREATED);
		assertFalse(getLastResponse().hasEntity());
		assertAfterTimer(getLastResponse().getLastModified());

		GET("/dariah/test/" + uid);
		assertStatus(Status.OK);
		assertEquals("{'json':'rocks'}", getEntity());
		assertEquals(MediaType.APPLICATION_JSON_TYPE, getLastResponse().getMediaType());
		assertEquals("{'json':'rocks'}".length(), getLastResponse().getLength());
		assertAfterTimer(getLastResponse().getLastModified());
	}

	@Test
	public void testPutWithPidAndVersion() throws NoSuchAlgorithmException, IOException {
		final String uid = makeObject();

		send(prepare("/dariah/test/" + uid).header("PID", "EhhPID")
										.header("Version", "13")
										.buildPut(Entity.json("{'json':'rocks'}")));

		GET("/dariah/test/" + uid);
		assertStatus(Status.OK);
		assertEquals("{'json':'rocks'}", getEntity());

		GET("/v2/test/metadata/" + uid);
		assertStatus(Status.OK);
		assertEquals("EhhPID", getJsonString("PID"));
		assertEquals("13", getJsonString("Version"));
	}

	@Test
	public void testDelete() throws NoSuchAlgorithmException, IOException {
		final String uid = makeObject();

		resetTimer();
		DELETE("/dariah/test/" + uid);
		assertStatus(Status.NO_CONTENT);

		GET("/dariah/test/" + uid);
		assertStatus(Status.NOT_FOUND);
	}
}
