package de.gwdg.cdstar.rest.v3;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.concurrent.ExecutionException;

import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.core.Form;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;

import org.junit.Test;

import com.fasterxml.jackson.databind.JsonNode;

import de.gwdg.cdstar.rest.BaseRestTest;
import de.gwdg.cdstar.web.common.model.AclMap;

public class CrudAclTest extends BaseRestTest {

	private String makeArchive() {
		target("/v3/test/").request().post(Entity.form(new Form()));
		assertStatus(Status.CREATED);
		return getJsonString("id");
	}

	@Test
	public void testACLEqualsEmbedded()
			throws InterruptedException, ExecutionException, IOException, URISyntaxException {
		final String id = makeArchive();

		target("/v3/test/", id).queryParam("with", "acl").request().get();
		final JsonNode acl = getJson("acl");

		target("/v3/test/", id).queryParam("acl", "").request().get();
		final JsonNode acl2 = getJson();
		assertEquals(acl, acl2);
	}

	@Test
	public void testAclExplode()
		throws InterruptedException, ExecutionException, IOException, URISyntaxException {
		final String id = makeArchive();

		final AclMap acl = new AclMap().with("$owner", "OWNER").with("$any", "READ");
		target("/v3/test/", id).queryParam("acl", "").request().put(Entity.json(acl));

		AclMap acl2 = target("/v3/test/", id).queryParam("acl", "").request().get().readEntity(AclMap.class);
		assertStatus(Status.OK);
		assertTrue(acl2.getMap().get("$any").contains("READ"));

		acl2 = target("/v3/test/", id).queryParam("acl", "group").request().get().readEntity(AclMap.class);
		assertStatus(Status.OK);
		assertTrue(acl2.getMap().get("$any").contains("READ"));

		acl2 = target("/v3/test/", id).queryParam("acl", "explode").request().get().readEntity(AclMap.class);
		assertStatus(Status.OK);
		assertFalse(acl2.getMap().get("$any").contains("READ"));
		assertTrue(acl2.getMap().get("$any").contains("read_files"));
	}

	@Test
	public void testPutACL() throws InterruptedException, ExecutionException, IOException, URISyntaxException {
		final String id = makeArchive();

		final AclMap acl = new AclMap().with("$any", "READ", "read_acl", "change_acl");

		target("/v3/test/", id).queryParam("acl", "").request().put(Entity.json(acl));
		assertStatus(Status.OK);

		final Response r = target("/v3/test/", id).queryParam("acl", "").request().get();
		assertStatus(Status.OK);
		final AclMap acl2 = r.readEntity(AclMap.class);
		assertEquals(acl.getMap(), acl2.getMap());
	}

	@Test
	public void testPutACLBadPermission()
			throws InterruptedException, ExecutionException, IOException, URISyntaxException {
		final String id = makeArchive();

		final AclMap acl = new AclMap().with("$any", "whatever");
		target("/v3/test/", id).queryParam("acl", "").request().put(Entity.json(acl));
		assertStatus(Status.BAD_REQUEST);
		assertEquals("UnknownPermission", getJsonString("error"));
	}

}
