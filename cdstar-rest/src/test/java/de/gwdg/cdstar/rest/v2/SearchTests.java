package de.gwdg.cdstar.rest.v2;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;

import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response.Status;

import org.junit.After;
import org.junit.Assume;
import org.junit.Before;
import org.junit.Test;

public class SearchTests extends ApiTestBase {

	private String bitsreamid;
	private String uid;
	// private Node esNode;

	@Before
	public void startSearch() throws IOException {
		Assume.assumeTrue("Search is not implemented", false);
		// final Settings.Builder elasticsearchSettings = Settings
		// .settingsBuilder()
		// .put("http.enabled", "false")
		// .put("path.data",
		// testFolder.newFolder().getAbsolutePath())
		// .put("index.store.type", "memory");

		// esNode =
		// NodeBuilder.nodeBuilder().local(true).settings(elasticsearchSettings.build()).node();
	}

	@Before
	public void storeTestData() throws InterruptedException {
		uid = POST("/v2/test/objects/", null).getJsonString("uid");
		assertStatus(Status.CREATED);

		PUT("/v2/test/metadata/" + uid, Entity.json("{\"test\": \"value\", \"test2\": [1,2,\"3\"]}"));
		assertStatus(Status.CREATED);

		POST("/v2/test/bitstreams/" + uid, Entity.entity("a,b,c\nfoo,bar,baz", MediaType.TEXT_PLAIN));
		assertStatus(Status.CREATED);
		bitsreamid = getJsonString("bitstreamid");
		assertHitcount("/v2/test/search/", "{\"match_all\" : {}}", 3);
	}

	@After
	public void stopSearch() {
		// esNode.close();
	}

	private void search(final String url, final String q) {
		POST(url, Entity.json(q));
	}

	private void assertHitcount(final String url, final String q, final long c) throws InterruptedException {
		/*
		 * syncSearch(); for (int i = 0; i < 100; i++) { Thread.sleep(100);
		 * search(url, q); assertStatus(Status.OK); if (getLong("hitcount") ==
		 * c) { return; } } assertEquals(c, getLong("hitcount"));
		 */
	}

	@Test
	public void testQueryparameterSanity() throws IOException, InterruptedException {
		final String query = "{\"term\": {\"field\": \"value\"}}";

		search("/v2/test/search/", query);
		assertStatus(Status.OK);
		assertEquals(0, getJsonLong("hitcount"));
		assertEquals(0.0, getJsonDouble("maxscore"), 0.0);

		// Invalid limit size
		search("/v2/test/search/?limit=0", query);
		assertStatus(Status.BAD_REQUEST);

		// To big limit size
		search("/v2/test/search/?limit=1000", query);
		assertStatus(Status.BAD_REQUEST);

		// Invalid offset
		search("/v2/test/search/?offset=-2", query);
		assertStatus(Status.BAD_REQUEST);

		// Invalid index selection
		search("/v2/test/search/?index=INVALID", query);
		assertStatus(Status.BAD_REQUEST);
	}

	@Test
	public void testSearch() throws IOException, InterruptedException {
		search("/v2/test/search/", "{\"term\": {\"test\": \"value\"}}");
		assertStatus(Status.OK);
		assertEquals(1, getJsonLong("hitcount"));
		assertTrue(0.0 < getJsonDouble("maxscore"));
	}

	@Test
	public void testIndexSelect() throws IOException, InterruptedException {
		search("/v2/test/search/", "{\"match_all\" : {}}");
		assertStatus(Status.OK);
		assertEquals(3, getJsonLong("hitcount"));

		search("/v2/test/search/?index=all", "{\"match_all\" : {}}");
		assertStatus(Status.OK);
		assertEquals(3, getJsonLong("hitcount"));

		search("/v2/test/search/?index=metadata", "{\"match_all\" : {}}");
		assertStatus(Status.OK);
		assertEquals(1, getJsonLong("hitcount"));
		assertEquals("metadata", getJsonString("hits.0.type"));

		search("/v2/test/search/?index=fulltext", "{\"match_all\" : {}}");
		assertStatus(Status.OK);
		assertEquals(1, getJsonLong("hitcount"));
		assertEquals("fulltext", getJsonString("hits.0.type"));
		assertEquals(bitsreamid, getJsonString("hits.0.bitstreamid"));

		search("/v2/test/search/?index=object", "{\"match_all\" : {}}");
		assertStatus(Status.OK);
		assertEquals(1, getJsonLong("hitcount"));
		assertEquals("object", getJsonString("hits.0.type"));
	}

	@Test
	public void testUpdatedBistream() throws IOException, InterruptedException {
		PUT("/v2/test/bitstreams/" + uid + "/" + bitsreamid, Entity.entity("xyz,xyz", "text/svc"));
		assertStatus(Status.OK);

		assertHitcount("/v2/test/search/", "{\"match_all\" : {}}", 3);
		assertHitcount("/v2/test/search/?index=fulltext", "{\"term\": {\"data\": \"foo\"}}", 0);
		assertHitcount("/v2/test/search/?index=fulltext", "{\"term\": {\"data\": \"xyz\"}}", 1);
	}

	@Test
	public void testDeletedBistream() throws IOException, InterruptedException {
		DELETE("/v2/test/bitstreams/" + uid + "/" + bitsreamid);
		assertStatus(Status.NO_CONTENT);

		assertHitcount("/v2/test/search/", "{\"match_all\" : {}}", 2);
		assertHitcount("/v2/test/search/?index=fulltext", "{\"match_all\" : {}}", 0);
	}

	@Test
	public void testUpdatedMetadata() throws IOException, InterruptedException {
		PUT("/v2/test/metadata/" + uid, Entity.json("{\"test\": \"xyz\"}"));
		assertStatus(Status.CREATED);

		assertHitcount("/v2/test/search/?index=metadata", "{\"term\": {\"test\": \"value\"}}", 0);
		assertHitcount("/v2/test/search/?index=metadata", "{\"term\": {\"test\": \"xyz\"}}", 1);
	}

	@Test
	public void testDeletedMetadata() throws IOException, InterruptedException {
		DELETE("/v2/test/metadata/" + uid);
		assertStatus(Status.NO_CONTENT);

		assertHitcount("/v2/test/search/", "{\"match_all\" : {}}", 2);
		assertHitcount("/v2/test/search/?index=metadata", "{\"match_all\" : {}}", 0);
	}

	@Test
	public void testDeletedObject() throws IOException, InterruptedException {
		DELETE("/v2/test/objects/" + uid);
		assertStatus(Status.NO_CONTENT);
		assertHitcount("/v2/test/search/", "{\"match_all\" : {}}", 0);
	}

}
