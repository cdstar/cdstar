package de.gwdg.cdstar.rest;

import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import de.gwdg.cdstar.Utils;

/**
 * Produce test data with some alternating but reproducible pattern.
 */
public final class TestDataStream extends InputStream implements Cloneable {
	volatile long size;
	volatile long consumed = 0;

	public TestDataStream(long size) {
		this.size = size;
	}

	@Override
	public int available() {
		return (int) Math.min(Integer.MAX_VALUE, size - consumed);
	}

	@Override
	public TestDataStream clone() {
		final TestDataStream tdd = new TestDataStream(size);
		tdd.consumed = consumed;
		return tdd;
	}

	public byte[] digest(String algo) throws NoSuchAlgorithmException {
		final MessageDigest md = MessageDigest.getInstance(algo);
		final TestDataStream tdd = clone();
		tdd.consumed = 0;
		final byte[] chunk = new byte[1024 * 8];
		int read;
		while ((read = tdd.read(chunk)) > -1)
			md.update(chunk, 0, read);
		return md.digest();
	}

	@Override
	public int read(byte[] b) {
		return read(b, 0, b.length);
	}

	@Override
	public int read(byte[] b, int off, int len) {
		int toRead = len - off;
		if (toRead > size - consumed)
			toRead = (int) (size - consumed);

		consumed += toRead;

		if (toRead == 0)
			return -1;
		for (int i = 0; i < toRead; i++)
			b[off + i] = (byte) (consumed - toRead + i);
		return toRead;
	}

	@Override
	public int read() {
		throw Utils.wtf("Do not do this");
	}

	public long bytesLeft() {
		return size - consumed;
	}

	public long bytesConsumed() {
		return consumed;
	}
}