package de.gwdg.cdstar.rest.v3;

import java.util.Map;
import java.util.stream.Collectors;

import de.gwdg.cdstar.rest.api.RestBlueprint;
import de.gwdg.cdstar.rest.api.RestConfig;
import de.gwdg.cdstar.rest.api.RestContext;
import de.gwdg.cdstar.rest.utils.SessionHelper;
import de.gwdg.cdstar.runtime.RuntimeContext;
import de.gwdg.cdstar.runtime.client.CDStarSession;
import de.gwdg.cdstar.runtime.services.FeatureRegistry;
import de.gwdg.cdstar.runtime.services.FeatureRegistry.FeatureEntry;
import de.gwdg.cdstar.web.common.model.ServiceInfo;
import de.gwdg.cdstar.web.common.model.VersionInfo.SourceInfo;

public class ServiceEndpoint implements RestBlueprint {

	@Override
	public void configure(RestConfig cfg) {
		cfg.route("").GET(this::handleGet);
	}

	public ServiceInfo handleGet(RestContext ctx) {
		final ServiceInfo info = new ServiceInfo();
		if (SessionHelper.getSubject(ctx).hasPrincipal())
			info.version.source = new SourceInfo();

		info.feature = ctx.getService(RuntimeContext.class)
				.lookup(FeatureRegistry.class).map(
						fr -> fr.getFeatures().values().stream()
								.collect(Collectors.toMap(FeatureEntry::getFeature, FeatureEntry::getInfo)))
				.orElse(Map.of());

		final CDStarSession session = SessionHelper.getCDStarSession(ctx, true);
		info.vaults = session.getVaultNames();
		return info;
	}

}
