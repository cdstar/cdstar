package de.gwdg.cdstar.rest;

import de.gwdg.cdstar.rest.api.ResponseMapper;
import de.gwdg.cdstar.rest.utils.MimeType;

public class ResponseMapperHolder {
	private final MimeType mime;
	private final ResponseMapper<Object> mapper;
	private final Class<? extends Object> type;

	@SuppressWarnings("unchecked")
	public <T extends Object> ResponseMapperHolder(String mimetype, Class<T> klass, ResponseMapper<T> mapper) {
		mime = MimeType.getCached(mimetype);
		this.mapper = (ResponseMapper<Object>) mapper;
		this.type = klass;
	}

	public ResponseMapper<Object> getMapper() {
		return mapper;
	}

	public Class<? extends Object> getMappedType() {
		return type;
	}

	public MimeType getMime() {
		return mime;
	}

	@Override
	public String toString() {
		return "ResponseMapper(mime=" + mime + ", type=" + type + ", mapper=" + mapper + ")";
	}

}