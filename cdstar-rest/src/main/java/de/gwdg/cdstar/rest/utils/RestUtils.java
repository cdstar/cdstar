package de.gwdg.cdstar.rest.utils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import de.gwdg.cdstar.SharedObjectMapper;
import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.rest.api.RestContext;
import de.gwdg.cdstar.web.common.model.ErrorResponse;

public class RestUtils {
	private static final Logger log = LoggerFactory.getLogger(RestUtils.class);

	private static DateTimeFormatter RFC1123 = DateTimeFormatter.RFC_1123_DATE_TIME.withZone(ZoneId.of("GMT"));

	public static String httpDate(Date date) {
		return RFC1123.format(date.toInstant());
	}

	public static Date httpDate(String date) throws DateTimeParseException {
		return Date.from(RFC1123.parse(date, Instant::from));
	}

	/**
	 * Set `ETag` and `Last-Modified` response headers and check them against
	 * `If-Match`, `If-None-Match`, `If-Modified-Since` and
	 * `If-Unmodified-Since` request headers. Return true if this request should
	 * be answered with a full response, false if no response body is required.
	 *
	 * In the later case, the response status code is already set to either 403
	 * or 412.
	 */
	public static boolean checkConditional(RestContext ctx, Date lastModified, String etag) {
		ctx.header("ETag", etag);
		ctx.header("Last-Modified", httpDate(lastModified));

		// Check if we can return 304 NOT MODIFIED

		final String ifNoneMatch = ctx.getHeader("If-None-Match");
		if (ifNoneMatch != null && (ifNoneMatch.equals(etag) || ifNoneMatch.equals("*"))) {
			ctx.status(304);
			return false;
		}
		final long ifModifiedSince = getDateHeader(ctx, "If-Modified-Since");
		if (ifNoneMatch == null && ifModifiedSince > -1 && ifModifiedSince + 1000 > lastModified.getTime()) {
			ctx.status(304);
			return false;
		}

		// Check if we have to return 412 PRECONDITION FAILED

		final String ifMatch = ctx.getHeader("If-Match");
		if (ifMatch != null && (ifMatch.equals(etag) || ifMatch.equals("*"))) {
			ctx.status(412);
			return false;
		}

		final long ifUnmodifiedSince = getDateHeader(ctx, "If-Unmodified-Since");
		if (ifUnmodifiedSince > -1 && ifUnmodifiedSince + 1000 <= lastModified.getTime()) {
			ctx.status(412);
			return false;
		}

		return true;
	}

	public static long getDateHeader(RestContext ctx, String name) {
		final String header = ctx.getHeader(name);
		if (header == null)
			return -1;
		try {
			return httpDate(header).getTime();
		} catch (final DateTimeParseException e) {
			return -1;
		}
	}

	public static RangeHeader prepareRangeRequest(RestContext ctx, Date lastModified, String etag, long totalSize) {
		// Range header support.
		ctx.header("Accept-Ranges", "bytes");

		final String rangeHeader = ctx.getHeader("Range");
		RangeHeader range = null;

		if (rangeHeader != null) {

			try {
				range = new RangeHeader(rangeHeader, totalSize);
			} catch (final IllegalArgumentException e) {
				range = new RangeHeader(totalSize);
				if (ctx.getHeader("If-Range") == null) {
					ctx.header("Content-Range", range.toString());
					throw new ErrorResponse(416, "RangeNotSatisfiable", "Range not satisfiable");
				}
			}

			// If-Range may contain an e-tag or last-modified date. If it does
			// not match, we have to return the full range.
			final String ifRange = ctx.getHeader("If-Range");
			if (ifRange != null && !ifRange.equals(etag) && !ifRange.startsWith("\"")) {
				final long ifRangeTime = getDateHeader(ctx, "If-Range");
				if (ifRangeTime != -1 && ifRangeTime + 1000 < lastModified.getTime()) {
					range = new RangeHeader(totalSize);
				}
			}

			if (range.getRangeLength() != totalSize)
				ctx.status(206);

		} else {
			// Still advertise range support even if client did not ask for it.
			range = new RangeHeader(totalSize);
		}

		ctx.header("Content-Range", range.toString());
		ctx.header("Content-Length", range.getRangeLength());
		return range;
	}

	private static Charset utf8 = Charset.forName("UTF-8");

	public static String urlDecode(String encoded) {
		try {
			return URLDecoder.decode(encoded, utf8.name());
		} catch (final UnsupportedEncodingException e) {
			throw Utils.wtf();
		}
	}

	public static String urlEncode(String raw) {
		try {
			return URLEncoder.encode(raw, utf8.name());
		} catch (final UnsupportedEncodingException e) {
			throw Utils.wtf();
		}
	}

	/**
	 * Parse an URL-encoded query string (e.g. 'key=value&amp;...') into a
	 * multi-valued map. Order of values for a given key is preserved, but keys
	 * are not ordered (since the returned map is not ordered).
	 *
	 * Parameter names and values may be empty strings. The '=' separator is
	 * optional if the value is empty, but required if the key is also empty.
	 */
	public static Map<String, List<String>> splitQuery(String rawQuery) {
		final Map<String, List<String>> params = new HashMap<>();
		final int len = rawQuery.length();
		String key = null;
		int o = 0;

		for (int i = 0; i <= len; i++) {
			// After end of string, loop once more with c == '&' to collect the
			// last parameter.
			final char c = i < len ? rawQuery.charAt(i) : '&';

			if (c == '=' && key == null) {
				key = rawQuery.substring(o, i);
				o = i + 1;
			} else if (c == '&') {
				if (key != null) {
					params.computeIfAbsent(urlDecode(key), k -> new ArrayList<>(1))
						.add(urlDecode(rawQuery.substring(o, i)));
					key = null;
				} else if (i > o) {
					params.computeIfAbsent(urlDecode(rawQuery.substring(o, i)), k -> new ArrayList<>(1)).add("");
				}
				o = i + 1;
			}
		}

		return params;

	}

	public static String escapeHTML(String s) {
		final StringBuilder out = new StringBuilder(Math.max(16, s.length()));
		for (int i = 0; i < s.length(); i++) {
			final char c = s.charAt(i);
			if (c > 127 || c == '"' || c == '<' || c == '>' || c == '&') {
				out.append("&#");
				out.append((int) c);
				out.append(';');
			} else {
				out.append(c);
			}
		}
		return out.toString();
	}

	/**
	 * Read a JSON request body, or raise an appropriate {@link ErrorResponse}
	 */
	public static <T> T parseJsonBody(RestContext ctx, Class<T> klass) {
		final String ctype = ctx.getContentType();
		if (ctype == null || ctype.equals("application/json")) {
			try {
				return SharedObjectMapper.json.readValue(ctx.getInputStream(), klass);
			} catch (final JsonParseException e) {
				log.debug("Failed to parse request body", e);
				throw new ErrorResponse(400, "JsonParseError", "Invalid JSON response body")
					.detail("line", e.getLocation().getLineNr())
					.detail("column", e.getLocation().getColumnNr())
					.cause(e);
			} catch (final JsonMappingException e) {
				log.debug("Failed to map request body (target={})", klass, e);
				throw new ErrorResponse(400, "JsonMappingError", "JSON request did not match expected structure")
				.detail("line", e.getLocation().getLineNr())
				.detail("column", e.getLocation().getColumnNr())
				.cause(e);
			} catch (final IOException e) {
				log.debug("JsonMappingException", e);
				throw new ErrorResponse(400, "IOError", "Failed to read request body")
				.cause(e);
			}
		} else {
			throw new ErrorResponse(415, "UnsupportedMediaType", "Expected application/json request");
		}
	}

}
