package de.gwdg.cdstar.rest.v2;

import java.io.IOException;
import java.nio.channels.Channels;
import java.nio.channels.WritableByteChannel;
import java.util.Date;

import de.gwdg.cdstar.Promise;
import de.gwdg.cdstar.rest.api.RestBlueprint;
import de.gwdg.cdstar.rest.api.RestConfig;
import de.gwdg.cdstar.rest.api.RestContext;
import de.gwdg.cdstar.rest.utils.RangeHeader;
import de.gwdg.cdstar.rest.utils.RestUtils;
import de.gwdg.cdstar.rest.utils.SessionHelper;
import de.gwdg.cdstar.rest.utils.form.MultipartParser;
import de.gwdg.cdstar.rest.v2.model.Success.BitstreamResponse;
import de.gwdg.cdstar.rest.v2.utils.LegacyHelper;
import de.gwdg.cdstar.rest.v3.async.ArchiveUpdaterLegacy;
import de.gwdg.cdstar.rest.v3.async.AsyncDownload;
import de.gwdg.cdstar.rest.v3.async.AsyncUpload;
import de.gwdg.cdstar.runtime.client.CDStarArchive;
import de.gwdg.cdstar.runtime.client.CDStarFile;
import de.gwdg.cdstar.runtime.client.CDStarSession;
import de.gwdg.cdstar.runtime.client.exc.ArchiveNotFound;
import de.gwdg.cdstar.runtime.client.exc.FileNotFound;
import de.gwdg.cdstar.runtime.client.exc.VaultNotFound;
import de.gwdg.cdstar.ta.exc.TARollbackException;
import de.gwdg.cdstar.web.common.model.ErrorResponse;

public class BitstreamResource implements RestBlueprint {

	@Override
	public void configure(RestConfig cfg) {
		cfg.route("/<vault>/bitstreams/<uid>").POST(this::createBitsream);
		cfg.route("/<vault>/bitstreams/<uid>/<bitid:path>")
			.PUT(this::createBitsream)
			.GET(this::getBitstream)
			.DELETE(this::deleteBitstream);
	}

	public Void createBitsream(RestContext ctx) throws ArchiveNotFound, VaultNotFound, IOException {

		final CDStarArchive archive = SessionHelper.getCDStarSession(ctx, false)
			.getVault(ctx.getPathParam("vault"))
			.loadArchive(LegacyHelper.fromAnyID(ctx.getPathParam("uid")));

		String bitid = ctx.getPathParam("bitid");
		if (bitid == null) {
			bitid = "0";
			while (archive.hasFile(bitid)) {
				bitid = Integer.toString(Integer.parseInt(bitid) + 1);
			}
		}

		updateBitstream(ctx, archive, bitid);
		return null;

	}

	public void updateBitstream(RestContext ctx, final CDStarArchive archive, String bitid) throws IOException {

		final boolean isNew = !archive.hasFile(bitid);
		final CDStarFile file = archive.getOrCreateFile(bitid);

		Promise<?> p;

		if (ctx.getMethod().equals("POST") && ctx.isMultipart()) {
			p = Promise.wrap(new ArchiveUpdaterLegacy(ctx,
				new MultipartParser(ctx.getHeader("Content-Type"), ctx.getHeader("Content-Encoding"), 1024 * 8),
				file).dispatch());
		} else {
			file.setMediaType(ctx.getContentType());
			file.truncate(0);
			final WritableByteChannel writeChannel = file.getWriteChannel();

			p = Promise.wrap(new AsyncUpload(ctx, writeChannel).dispatch());
			p.then(r -> writeChannel.close()); // Close only on success
		}

		p.then(x -> {
			try {
				archive.getSession().commit();
				final BitstreamResponse entity = new BitstreamResponse(LegacyHelper.toLegacyID(archive.getId()), bitid);
				if (isNew) {
					ctx.status(201);
					ctx.header("Location", ctx.resolvePath("./" + bitid, true));
				}
				ctx.write(entity);
				ctx.close();
			} catch (final Exception e) {
				ctx.abort(e);
			}
		}, ctx::abort);

	}

	public Void deleteBitstream(RestContext ctx) throws TARollbackException, ArchiveNotFound, VaultNotFound {
		try {
			final CDStarSession session = SessionHelper.getCDStarSession(ctx, false);
			session
				.getVault(ctx.getPathParam("vault"))
				.loadArchive(LegacyHelper.fromAnyID(ctx.getPathParam("uid")))
				.getFile(ctx.getPathParam("bitid"))
				.remove();
			session.commit();
		} catch (final FileNotFound e) {
			throw new ErrorResponse(404, "BitstreamNotFound", "Bitstream not found");
		}
		ctx.status(204);
		return null;
	}

	public Void getBitstream(RestContext ctx) throws ArchiveNotFound, VaultNotFound, IOException {
		CDStarFile file;
		try {
			file = SessionHelper.getCDStarSession(ctx, true)
				.getVault(ctx.getPathParam("vault"))
				.loadArchive(LegacyHelper.fromAnyID(ctx.getPathParam("uid")))
				.getFile(ctx.getPathParam("bitid"));
		} catch (final FileNotFound e) {
			throw new ErrorResponse(404, "BitstreamNotFound", "Bitstream not found");
		}

		// Start serving actual file content

		final String etag = "\"" + file.getDigests().get(CDStarFile.DIGEST_SHA256) + "\"";
		final Date lastModified = file.getLastModified();

		if (!RestUtils.checkConditional(ctx, lastModified, etag))
			return null;

		final RangeHeader range = RestUtils.prepareRangeRequest(ctx, lastModified, etag, file.getSize());
		if (range.isZeroLength())
			return null;

		if (ctx.getMethod().equals("HEAD")) {
			ctx.header("Content-Length", range.getRangeLength());
			return null;
		}

		// TODO: Backwards compatible, but dangerous for stuff like javascript.
		ctx.header("Content-Type", file.getMediaType());

		new AsyncDownload(ctx,
			Channels.newInputStream(file.getReadChannel(range.getStartIndex())), 0, range.getRangeLength())
			.dispatch();

		return null;
	}

}
