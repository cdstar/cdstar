package de.gwdg.cdstar.rest.utils.form;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.synchronoss.cloud.nio.multipart.Multipart;
import org.synchronoss.cloud.nio.multipart.MultipartContext;
import org.synchronoss.cloud.nio.multipart.NioMultipartParser;
import org.synchronoss.cloud.nio.multipart.NioMultipartParserListener;
import org.synchronoss.cloud.nio.multipart.PartBodyStreamStorageFactory;
import org.synchronoss.cloud.nio.stream.storage.StreamStorage;

import de.gwdg.cdstar.Utils;

public class MultipartParser implements FormParser {

	static final ByteBuffer nullBuffer = ByteBuffer.allocate(0);

	volatile List<MultipartPart> finishedParts = new ArrayList<>();
	volatile MultipartPart currentPart;
	private boolean multipartMessageComplete;

	private FormParserException error;
	private final NioMultipartParser multipartParser;
	private final MultipartContext multipartContext;

	private long drainWatermark;

	/**
	 * Parse chunks of data and return (possibly empty) lists of
	 * {@link MultipartPart} instances.
	 *
	 * If a part grows larger than the drainWatermark, it is returned even if it is
	 * not complete. Incomplete parts are returned again every time drainWatermark
	 * is reached and a last time once they are finally complete. A consumer should
	 * drain as much as possible from incomplete parts to free up buffer space.
	 *
	 * @param drainWatermark Number of bytes an incomplete part should buffer before
	 *                       it is returned by the parser.
	 */
	public MultipartParser(String contentType, String charEncoding, long drainWatermark) {
		Utils.notNullOrEmpty(contentType);
		Utils.notNullOrEmpty(charEncoding);
		this.drainWatermark = drainWatermark;
		multipartContext = new MultipartContext(contentType, -1, charEncoding);
		final MultipartListener listener = new MultipartListener();
		multipartParser = Multipart.multipart(multipartContext)
			.usePartBodyStreamStorageFactory(listener)
			.forNIO(listener);
	}

	public long getDrainWatermark() {
		return drainWatermark;
	}

	public void setDrainWatermark(long drainWatermark) {
		this.drainWatermark = drainWatermark;
	}

	@Override
	public List<FormPart> parse(ByteBuffer bytes) throws FormParserException {
		if (bytes.hasArray()) {
			final List<FormPart> result = parse(bytes.array(), bytes.arrayOffset() + bytes.position(),
				bytes.remaining());
			bytes.position(bytes.limit());
			return result;
		} else {
			final byte[] copy = new byte[bytes.remaining()];
			bytes.get(copy);
			return parse(copy, 0, copy.length);
		}
	}

	private synchronized List<FormPart> parse(byte[] bytes, int offset, int len) throws FormParserException {
		multipartParser.write(bytes, offset, len);

		if (error != null)
			throw error;

		if (currentPart == null && finishedParts.isEmpty())
			return Collections.emptyList();

		final List<MultipartPart> result = finishedParts;
		finishedParts = new ArrayList<>(1);

		if (currentPart != null && currentPart.getBuffered() > drainWatermark) {
			result.add(currentPart);
		}

		return Collections.unmodifiableList(result);
	}

	@Override
	public List<FormPart> finish() throws FormParserException {
		if (!multipartMessageComplete)
			throw new FormParserException("Unexpected end of stream.");
		return Collections.emptyList();
	}

	private class MultipartListener implements NioMultipartParserListener, PartBodyStreamStorageFactory {

		@Override
		public StreamStorage newStreamStorageForPartBody(Map<String, List<String>> partHeaders, int partIndex) {
			currentPart = new MultipartPart(partHeaders, partIndex);
			return new StreamStorageWrapper(currentPart);
		}

		@Override
		public void onPartFinished(StreamStorage store, Map<String, List<String>> headersFromPart) {
			assert ((StreamStorageWrapper) store).getPart() == currentPart;
			currentPart.done = true;
			if (finishedParts == null)
				finishedParts = new ArrayList<>();
			finishedParts.add(currentPart);
			currentPart = null;
		}

		@Override
		public void onAllPartsFinished() {
			multipartMessageComplete = true;
		}

		@Override
		public void onNestedPartStarted(Map<String, List<String>> headersFromParentPart) {
			throw new UnsupportedOperationException("Nested multipart is not supported.");
		}

		@Override
		public void onNestedPartFinished() {
		}

		@Override
		public void onError(String message, Throwable cause) {
			error = new FormParserException(message, cause);
		}
	}

	private static class StreamStorageWrapper extends StreamStorage {

		private final MultipartPart part;

		StreamStorageWrapper(MultipartPart part) {
			this.part = part;
		}

		public MultipartPart getPart() {
			return part;
		}

		@Override
		public void write(int b) throws IOException {
			throw new UnsupportedOperationException("This is ineffictend. Do not do that.");
		}

		@Override
		public void write(byte[] b) throws IOException {
			part.write(b, 0, b.length);
		}

		@Override
		public void write(byte[] b, int off, int len) throws IOException {
			part.write(b, off, len);
		}

		@Override
		public boolean dispose() {
			return false;
		}

		@Override
		public InputStream getInputStream() {
			throw new UnsupportedOperationException("Not used.");
		}
	}
}
