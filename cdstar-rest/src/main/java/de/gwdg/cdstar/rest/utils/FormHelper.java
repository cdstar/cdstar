package de.gwdg.cdstar.rest.utils;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.rest.api.RestContext;
import de.gwdg.cdstar.rest.utils.form.FormParser;
import de.gwdg.cdstar.rest.utils.form.FormParserException;
import de.gwdg.cdstar.rest.utils.form.FormPart;
import de.gwdg.cdstar.rest.utils.form.MultipartParser;
import de.gwdg.cdstar.rest.utils.form.UrlEncodedParser;

/**
 * Synchronous form parser for small forms with no file uploads.
 */
public class FormHelper {

	private final Map<String, String> parts;

	private FormHelper(Map<String, String> parts) {
		this.parts = parts;
	}

	public Optional<String> get(String name) {
		return Optional.ofNullable(parts.get(name));
	}

	public String get(String name, String defaultValue) {
		return get(name).orElse(defaultValue);
	}

	/**
	 * Parse the entire request in a blocking fashion.
	 *
	 * @param ctx            request context
	 * @param maxElementSize throw an error if a single form element is larger than
	 *                       this.
	 * @param maxRead        throw an error if the entire form is larger than this.
	 * @return a readily parsed {@link FormHelper}
	 * @throws EntityTooLarge
	 * @throws UnsupportedContentType
	 * @throws FormFieldTooLarge
	 * @throws FormParserException
	 * @throws IOException
	 */
	public static FormHelper parse(RestContext ctx, int maxElementSize, int maxRead)
		throws FormParserException {
		if (!ctx.hasEntity())
			return new FormHelper(Collections.emptyMap());

		final long clen = ctx.getContentLength();
		if (clen > 0) {
			if (clen > maxRead)
				throw new EntityTooLarge(maxRead);
			maxRead = (int) Math.min(maxRead, clen);
		}

		FormParser form;
		if (ctx.isMultipart()) {
			form = new MultipartParser(ctx.getHeader("Content-Type"), "UTF-8", maxElementSize);
		} else if (ctx.isForm()) {
			form = new UrlEncodedParser(maxElementSize, true, StandardCharsets.UTF_8);
		} else {
			throw new UnsupportedContentType(ctx.getContentType());
		}

		final Map<String, String> result = new HashMap<>();
		final int bufferSize = Utils.gate(maxElementSize, 1024 * 64, maxRead);
		final ByteBuffer buffer = ByteBuffer.allocate(bufferSize);
		int total = 0;
		boolean eof = false;
		do {
			int chunk;
			try {
				chunk = ctx.read(buffer);
			} catch (final IOException e) {
				throw new FormParserException("Unexpected EOF or read error", e);
			}
			if ((total += chunk) > maxRead)
				throw new EntityTooLarge(maxRead);
			buffer.flip();

			final List<FormPart> parts;
			if (buffer.hasRemaining()) {
				parts = form.parse(buffer);
				buffer.clear();
			} else {
				parts = form.finish();
				eof = true;
			}

			for (final FormPart part : parts) {
				if (!part.isComplete()) {
					if (part.getBuffered() >= maxElementSize)
						throw new FormFieldTooLarge(part.getName());
					continue;
				}
				if (part.getName() == null)
					throw new FormParserException("Unnamed field");
				result.put(part.getName(), part.drainToString(StandardCharsets.UTF_8));
			}
		} while (!eof);

		return new FormHelper(result);
	}

	public static class EntityTooLarge extends FormParserException {
		private static final long serialVersionUID = 4874186929008883638L;

		public EntityTooLarge(int limit) {
			super("Larger than " + limit);
		}
	}

	public static class UnsupportedContentType extends FormParserException {
		private static final long serialVersionUID = -432047215473060595L;

		public UnsupportedContentType(String type) {
			super(type);
		}
	}

	public static class FormFieldTooLarge extends FormParserException {
		private static final long serialVersionUID = 6187007509004252521L;
		private final String fieldName;

		public FormFieldTooLarge(String name) {
			super(name);
			fieldName = name;
		}

		public String getFieldName() {
			return fieldName;
		}
	}

}
