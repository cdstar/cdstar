package de.gwdg.cdstar.rest.v2.utils;

import java.util.function.Function;
import java.util.stream.Collectors;

import de.gwdg.cdstar.rest.v2.model.ObjectBean.PermissionBean;
import de.gwdg.cdstar.runtime.client.CDStarACLEntry;
import de.gwdg.cdstar.runtime.client.CDStarArchive;
import de.gwdg.cdstar.runtime.client.auth.ArchivePermissionSet;

public class LegacyHelper {

	public static String fromAnyID(String id) {
		if (id.startsWith("EAEA0-"))
			return id.substring(5).replace("-", "").toLowerCase();
		return id;
	}

	public static String toLegacyID(String id) {
		final StringBuilder sb = new StringBuilder("EAEA0");
		int i;
		for (i = 0; i < id.length() - 4; i += 4) {
			sb.append('-').append(id.substring(i, i + 4).toUpperCase());
		}
		if (i < id.length())
			sb.append('-').append(id.substring(i).toUpperCase());
		return sb.toString();
	}

	public static void translatePermissions(PermissionBean perm, CDStarArchive ar) {

		final Function<CDStarACLEntry, String> permissionMapper = p -> {
			// CDStar-v2 does not know group or special subjects, but the only one that can
			// be clearly translated is $owner.
			if (p.isOwnerSubject())
				return ar.getOwner();
			return p.getSubject().toString();
		};

		// CDSTAR v2 permissions cannot be mapped to v3 without loss of information.
		// This mapping simply hides permissions that are not mappable.
		perm.setOwner(ar.getOwner());
		perm.setManage(ar.getACL().getAccessList().stream()
				.filter(p -> p.isPermitted(ArchivePermissionSet.MANAGE.getPermissions())).map(permissionMapper)
				.collect(Collectors.toList()));
		perm.setRead(ar.getACL().getAccessList().stream()
				.filter(p -> p.isPermitted(ArchivePermissionSet.READ.getPermissions())).map(permissionMapper)
				.collect(Collectors.toList()));
		perm.setWrite(ar.getACL().getAccessList().stream()
				.filter(p -> p.isPermitted(ArchivePermissionSet.WRITE.getPermissions())).map(permissionMapper)
				.collect(Collectors.toList()));
	}

}
