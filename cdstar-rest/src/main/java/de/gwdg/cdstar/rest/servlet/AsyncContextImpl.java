package de.gwdg.cdstar.rest.servlet;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.gwdg.cdstar.BufferPool;
import de.gwdg.cdstar.NamedThreadFactory;
import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.rest.api.AsyncCloseListener;
import de.gwdg.cdstar.rest.api.AsyncContext;
import de.gwdg.cdstar.rest.api.AsyncResultCallback;
import de.gwdg.cdstar.rest.api.RestContext;
import jakarta.servlet.AsyncEvent;
import jakarta.servlet.ReadListener;
import jakarta.servlet.ServletInputStream;
import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.WriteListener;
import jakarta.servlet.http.HttpServletRequest;

public class AsyncContextImpl implements AsyncContext, WriteListener, ReadListener {
	private static final Logger log = LoggerFactory.getLogger(AsyncContextImpl.class);

	private static final ByteBuffer nullBuffer = ByteBuffer.allocate(0);
	private static final BufferPool bufferPool = new BufferPool(16, 1024 * 64);

	private static final ScheduledExecutorService scheduler;
	private static final Set<AsyncContextImpl> asyncRequests = ConcurrentHashMap.newKeySet();

	/*
	 * A client connected via first generation mobile internet (or a 56k modem)
	 * could fill a default buffer (64k) in under 10 seconds. We allow 30 seconds
	 * (17kbit/s).
	 */
	static final long defaultReadTimeout = 30 * 1000;
	static final long defaultWriteTimeout = 30 * 1000;
	static final long defaultIdleTimeout = 30 * 60 * 1000;

	static {
		scheduler = new ScheduledThreadPoolExecutor(1, new NamedThreadFactory("async-request-reaper").deamon(true));
		scheduler.scheduleWithFixedDelay(() -> {
			final long now = System.currentTimeMillis();
			for (final AsyncContextImpl ac : asyncRequests)
				ac.enforceTimeout(now);
			if (asyncRequests.size() > 1000) {
				log.warn("Possible async request leak (or DoS attack). Found {} open async requests.",
						Integer.toString(asyncRequests.size()));
			}
		}, 1, 1, TimeUnit.SECONDS);
	}

	private final RestContext ctx;
	private final jakarta.servlet.AsyncContext servletContext;
	private ServletInputStream input;
	private ServletOutputStream output;

	private Throwable error;

	private AsyncResultCallback readCallback;
	private ByteBuffer readBuffer;
	private long readExpires;
	private boolean readEndOfStream;
	private boolean readPartial = false;

	private AsyncResultCallback writeCallback;
	private ByteBuffer writeBuffer;
	private long writeExpires;

	// Tracks the last time an IO task completed, or #keepAlive() was called.
	private long lastActivity;
	private long readTimeout;
	private long writeTimeout;
	private long idleTimeout;

	private boolean closed;
	private List<AsyncCloseListener> closeListeners;

	/**
	 * Check if there is a timeout and abort the request with an error if necessary.
	 *
	 * The current time is passed in as a value so only one
	 * {@link System#currentTimeMillis()} call per round is required to check all
	 * requests.
	 *
	 * @return true if the request was reaped, false otherwise.
	 */
	private synchronized boolean enforceTimeout(long now) {

		if (readCallback != null && now > readExpires) {
			onError(new TimeoutException("Request timed out (reading)"));
			return true;
		}

		if (writeCallback != null && now > writeExpires) {
			onError(new TimeoutException("Request timed out (writing)"));
			return true;
		}

		if (readCallback == null && writeCallback == null && now - lastActivity > idleTimeout) {
			log.warn("Async request starved on unresponsive application. No activity for {}/{} ms",
					now - lastActivity, idleTimeout);
			onApplicationError(new TimeoutException("Request timed out (idle)"));
			return true;
		}

		return false;
	}

	/**
	 * Set read, write and idle timeouts for this context.
	 *
	 * Read or write timeouts limit the time to wait for a read or write request to
	 * complete. The idle timeout limits the time the context is kept alive without
	 * waiting for read or write requests.
	 *
	 * Note that slow clients need longer to fill large buffers, so make sure that
	 * the timeouts are larger than {@code bufferSize/minRate}.
	 *
	 * @param read  Timeout for read requests.
	 * @param write Timeout for write requests.
	 * @param idle  Idle timeout. The context is considered idle while no read or
	 *              write requests are issued.
	 */
	void setTimeouts(long read, long write, long idle, TimeUnit unit) {
		readTimeout = unit.toMillis(read);
		writeTimeout = unit.toMillis(write);
		idleTimeout = unit.toMillis(idle);
	}

	/**
	 * If true, read requests are completed as soon as at least one byte could be
	 * read. If false, the buffer is filled.
	 */
	void setReadPartial(boolean readPartial) {
		this.readPartial = readPartial;
	}

	public AsyncContextImpl(RestContext ctx, HttpServletRequest request) {
		this.ctx = ctx;

		servletContext = request.startAsync();

		// Servlet API does not allow to change the timeout at runtime, so we
		// have to do our own (more dynamic) timeout handling.
		servletContext.setTimeout(-1);

		setTimeouts(defaultReadTimeout, defaultWriteTimeout, defaultIdleTimeout, TimeUnit.MILLISECONDS);
		keepAlive();

		// Keep track of open requests and warn if there is a leak and not all
		// request are closed properly.
		asyncRequests.add(this);

		// If a handler forgets to call close() or abort(), then this listener
		// does it and issues a warning.
		servletContext.addListener(new jakarta.servlet.AsyncListener() {

			void ensureClose() {
				asyncRequests.remove(AsyncContextImpl.this);
				if (!ctx.isClosed()) {
					log.warn("Async context was not closed explicitly: {}", ctx);
					ctx.close();
				}
			}

			@Override
			public void onStartAsync(AsyncEvent event) throws IOException {
				throw new IOException("Replacing async context not supported.");
			}

			@Override
			public void onTimeout(AsyncEvent event) throws IOException {
				ensureClose();
				Utils.wtf("We disabled the timeout...");
			}

			@Override
			public void onError(AsyncEvent event) throws IOException {
				log.info("Async request failed: {}", ctx);
				ensureClose();
			}

			@Override
			public void onComplete(AsyncEvent event) throws IOException {
				ensureClose();
			}
		});
	}

	@Override
	public synchronized void addCloseListener(AsyncCloseListener listener) {
		if (closed)
			throw new IllegalStateException("Context already closed");
		if (closeListeners == null)
			closeListeners = new ArrayList<>(1);
		closeListeners.add(listener);
	}

	@Override
	public ByteBuffer getBuffer() {
		return bufferPool.getBuffer();
	}

	@Override
	public boolean recycleBuffer(ByteBuffer buf) {
		if (buf == nullBuffer)
			return false;
		return bufferPool.recycleBuffer(buf);
	}

	@Override
	public int getBufferSize() {
		return bufferPool.getBufferSize();
	}

	@Override
	public RestContext getRequest() {
		return ctx;
	}

	private long getExpireTime(Duration userTimeout, long defaultTimeout) {
		if (userTimeout == null || userTimeout.isNegative())
			return System.currentTimeMillis() + defaultTimeout;
		return System.currentTimeMillis() + userTimeout.toMillis();
	}

	@Override
	public synchronized void asyncWrite(ByteBuffer buffer, AsyncResultCallback callback, Duration timeout) {
		if (error != null)
			throw new IllegalStateException("Context in error state.");
		if (writeCallback != null)
			throw new IllegalStateException("Write request still pending");

		writeCallback = callback;
		writeBuffer = buffer;
		writeExpires = getExpireTime(timeout, writeTimeout);

		if (output == null) {
			try {
				output = servletContext.getResponse().getOutputStream();
				output.setWriteListener(this);
			} catch (final IOException e) {
				onError(e);
				return;
			}
		} else if (output.isReady()) {
			// Wake up, data is already write-able. The container is waiting for us.
			onWritePossible();
		}
	}

	@Override
	public synchronized void asyncRead(ByteBuffer buffer, AsyncResultCallback callback, Duration timeout) {
		if (error != null)
			throw new IllegalStateException("Context in error state.");
		if (readCallback != null)
			throw new IllegalStateException("Read request still pending.");
		if (readEndOfStream)
			throw new IllegalStateException("Cannot read past end of stream.");
		if (buffer == null || !buffer.hasArray() || !buffer.hasRemaining())
			throw new IllegalStateException("Read buffer must be backed by an array and have remaining space.");

		readCallback = callback;
		readBuffer = buffer;
		readExpires = getExpireTime(timeout, readTimeout);

		if (input == null) {
			try {
				input = servletContext.getRequest().getInputStream();
				input.setReadListener(this);
			} catch (final IOException e) {
				onError(e);
				return;
			}
		} else if (input.isReady()) {
			// Wake up, data is already read-able. The container is waiting for us.
			onDataAvailable();
		}

	}

	@Override
	public boolean endOfStream() {
		return readEndOfStream;
	}

	/**
	 * Called on read- or write errors or IO timeouts. All three situations are
	 * non-recoverable and no attempt is made to send an error response, as these
	 * errors usually mean that the client disconnected already. The request is just
	 * closed. Pending callbacks are notified about the error, though.
	 *
	 * TODO: Read timeouts or errors are a situation where a sensible response could
	 * be sent. We currently only set an appropriate status code (408 Request
	 * Timeout).
	 */
	@Override
	public synchronized void onError(Throwable t) {
		log.debug("Async request failed", t);
		if (error == null) {
			error = t;

			final AsyncResultCallback rcb = readCallback;
			final AsyncResultCallback wcb = writeCallback;
			final ByteBuffer wbuff = writeBuffer;

			readCallback = writeCallback = null;
			readBuffer = writeBuffer = null;

			if (rcb != null)
				runInPool(rcb, nullBuffer, t);
			if (wcb != null)
				runInPool(wcb, wbuff, t);

			ctx.status(error instanceof TimeoutException ? 408 : 500);
			ctx.close();
		} else {
			error.addSuppressed(t);
		}
	}

	/**
	 * Called on idle timeouts (application dropped the ball) or if the error
	 * handler of a read/write task failed. We have no one to ask for help now. Try
	 * to send an error response and close the request.
	 */
	public synchronized void onApplicationError(Throwable t) {
		if (readCallback != null || writeCallback != null)
			Utils.wtf();

		if (error == null) {
			error = t;

			// Error handlers may override this, but it's nice to set a sensible default
			ctx.status(error instanceof TimeoutException ? 408 : 500);

			if (!ctx.isCommitted())
				ctx.abort(t);

			ctx.close();
		} else {
			error.addSuppressed(t);
		}
	}

	/**
	 * Triggered by the container once data is available, or by us if we already
	 * know for sure that data is available, or by us if we reached EOF and did not
	 * already knew that (see {@link #onAllDataRead()}).
	 */
	@Override
	public synchronized void onDataAvailable() {
		if (readCallback != null) {
			try {
				final ByteBuffer buf = readBuffer;

				do {
					final int n;
					if (input.isFinished()
							|| (n = input.read(buf.array(), buf.arrayOffset() + buf.position(), buf.remaining())) < 0) {
						readEndOfStream = true;
					} else {
						buf.position(buf.position() + n);
					}

					if (!buf.hasRemaining() || readEndOfStream || readPartial) {
						buf.flip();
						final AsyncResultCallback cb = readCallback;

						readCallback = null;
						readBuffer = null;

						keepAlive();
						runInPool(cb, buf, null);
						break;
					}

				} while (input.isReady());

			} catch (final Exception e) {
				onError(e);
			}
		}
	}

	@Override
	public synchronized void onAllDataRead() {
		/*
		 * There is an edge-case where onDataAvailable() will read the last byte, but
		 * not detect that EOL was reached. Both input.isReady() and input.isFinished()
		 * will return false, but onDataAvailable() will not be triggered again. I guess
		 * this happens with jetty and chunked transfer encoding if the terminating
		 * empty chunk arrived in a separate TCP packet.
		 *
		 * To handle this special case (and not duplicate code), we trigger a final
		 * onDataAvailable() manually.
		 */
		if (readCallback != null && !readEndOfStream)
			onDataAvailable();
	}

	private void runInPool(AsyncResultCallback callback, ByteBuffer buffer, Throwable error) {
		ctx.runInPool(() -> {
			try {
				callback.done(this, buffer, error);
			} catch (final Exception e) {
				if (error == null) {
					runInPool(callback, buffer, e);
				} else {
					e.addSuppressed(error);
					log.warn("Async callback failed to handle error", e);
					// Send the original error to the client, as it usually makes more sense
					onApplicationError(error);
				}
			}
		});
	}

	/**
	 * Triggered by the container once data can be written to the output buffer, or
	 * by us if we already know for sure that there is some space available on the
	 * output buffer to write to.
	 */
	@Override
	public synchronized void onWritePossible() {
		if (writeCallback == null)
			return;

		final AsyncResultCallback callback = writeCallback;
		final ByteBuffer b = writeBuffer;

		try {
			if (b.hasRemaining()) {
				if (b.hasArray()) {
					output.write(b.array(), b.arrayOffset() + b.position(), b.remaining());
					b.position(b.limit());
				} else {
					final byte[] copy = new byte[b.remaining()];
					b.get(copy);
					output.write(copy);
				}
			}

			if (b.hasArray() && !output.isReady()) {
				// Write was incomplete and the container may still hold a
				// reference to the data buffer.
				// We have to wait for onWritePossible() to be triggered one
				// more time before we can close the task and recycle the
				// buffer.
			} else {
				// All data sent.
				writeCallback = null;
				writeBuffer = null;

				keepAlive();
				runInPool(callback, b, null);
			}
		} catch (final IOException e) {
			onError(e);
		}
	}

	@Override
	public void keepAlive() {
		lastActivity = System.currentTimeMillis();
	}

	@Override
	public CompletableFuture<Void> consumeRequest() {
		if (endOfStream())
			return CompletableFuture.completedFuture(null);

		// TODO: Switch to input.seek() and bypass the readTask creation?
		final CompletableFuture<Void> cf = new CompletableFuture<>();
		try {
			asyncRead(getBuffer(), new AsyncResultCallback() {
				@Override
				public void done(AsyncContext ac, ByteBuffer buffer, Throwable error) throws Exception {
					try {
						if (error != null) {
							cf.completeExceptionally(error);
						} else if (ac.endOfStream()) {
							ac.recycleBuffer(buffer);
							cf.complete(null);
						} else {
							buffer.clear();
							ac.asyncRead(buffer, this, null);
						}
					} catch (final Exception e) {
						cf.completeExceptionally(e);
					}
				}
			}, null);
		} catch (final Exception e) {
			cf.completeExceptionally(e);
		}
		return cf;
	}

	/**
	 * {@link AsyncContext#close()} calls {@link RestContext#close()} which then
	 * calls {@link #consumeThenClose()}. This is why this method is not just called
	 * 'close'.
	 */
	synchronized void consumeThenClose() {
		if (closed)
			return;
		closed = true;

		// Ensure that the entire request is consumed before the context is
		// closed. Closing the response early while the client still sends data
		// confuses some clients, including firefox/chrome and
		// their XmlHttpRequest implementations :(
		consumeRequest().whenComplete((e, r) -> servletContext.complete());

		if (closeListeners != null) {
			for (final AsyncCloseListener listener : closeListeners) {
				try {
					listener.closed(this, error);
				} catch (final Exception e) {
					log.warn("Close listener failed with an excepion.", e);
				}
			}
		}
	}

}
