package de.gwdg.cdstar.rest.v3;

import java.io.IOException;

import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.Snapshot;

import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.rest.api.RestBlueprint;
import de.gwdg.cdstar.rest.api.RestConfig;
import de.gwdg.cdstar.rest.api.RestContext;
import de.gwdg.cdstar.rest.utils.QueryHelper;
import de.gwdg.cdstar.rest.utils.SessionHelper;
import de.gwdg.cdstar.runtime.RuntimeContext;
import de.gwdg.cdstar.runtime.client.auth.ServicePermission;
import de.gwdg.cdstar.web.common.model.ServiceMetricsReport;

public class ServiceMetricsEndpoint implements RestBlueprint {
	private MetricRegistry metrics;

	@Override
	public void configure(RestConfig cfg) {
		cfg.route("/_metrics").GET(this::handleGetMetrics);

		metrics = cfg.lookup(RuntimeContext.class).lookupRequired(MetricRegistry.class);
	}

	public Void handleGetMetrics(RestContext ctx) throws IOException {
		SessionHelper.ensurePermitted(ctx, ServicePermission.HEALTH.asStringPermission());

		final QueryHelper qh = new QueryHelper(ctx);
		qh.setDefault("format", "json");
		final String format = qh.getOption("format", "json", "prometheus");

		if (Utils.equalNotNull(format, "prometheus")) {
			ctx.header("Contet-Type", "text/plain");
			ctx.write(metricsToText());
		} else {
			ctx.write(metricsToJson());
		}
		return null;
	}

	private void addPrometheusMetrics(StringBuilder sb, String name, String type, Object value) {
		final String pname = name.replace(".", "_").replace("-", "_").toLowerCase();
		sb.append("# HELP ").append(pname).append(" ").append(name).append("\n");
		sb.append("# TYPE ").append(pname).append(" ").append(type).append("\n");
		sb.append(pname).append(" ").append(value).append("\n");
	}

	private String metricsToText() {
		final StringBuilder sb = new StringBuilder();

		metrics.getCounters().forEach((name, c) -> {
			addPrometheusMetrics(sb, name, "counter", c.getCount());
		});
		metrics.getGauges().forEach((name, c) -> {
			addPrometheusMetrics(sb, name, "gauge", c.getValue());
		});
		metrics.getTimers().forEach((name, c) -> {
			addPrometheusMetrics(sb, name, "gauge", c.getOneMinuteRate());
		});

		return sb.toString();
	}

	public ServiceMetricsReport metricsToJson() {

		final ServiceMetricsReport result = new ServiceMetricsReport();

		metrics.getCounters().forEach((name, c) -> {
			result.setMetric(name, c.getCount());
		});
		metrics.getGauges().forEach((name, c) -> {
			result.setMetric(name, c.getValue());
		});
		metrics.getTimers().forEach((name, c) -> {
			result.setMetric(name + ".count", c.getCount());
			result.setMetric(name + ".rate", c.getMeanRate());
			result.setMetric(name + ".rate15m", c.getFifteenMinuteRate());
			result.setMetric(name + ".rate5m", c.getFiveMinuteRate());
			result.setMetric(name + ".rate1m", c.getOneMinuteRate());
			final Snapshot sample = c.getSnapshot();
			result.setMetric(name + ".min", sample.getMin() / 1000000);
			result.setMetric(name + ".max", sample.getMax() / 1000000);
			result.setMetric(name + ".mean", sample.getMean() / 1000000);
			result.setMetric(name + ".median", sample.getMedian() / 1000000);
			result.setMetric(name + ".p75", sample.get75thPercentile() / 1000000);
			result.setMetric(name + ".p95", sample.get95thPercentile() / 1000000);
			result.setMetric(name + ".p99", sample.get99thPercentile() / 1000000);
		});

		return result;
	}

}
