package de.gwdg.cdstar.rest.v3.async;

import java.net.URI;
import java.nio.ByteBuffer;
import java.util.function.BiConsumer;

import de.gwdg.cdstar.web.common.model.ErrorResponse;

public interface UrlFetchService {

	boolean canHandle(URI uri);

	FetchHandle resolve(URI uri) throws ErrorResponse;

	interface FetchHandle extends AutoCloseable {
		long size();

		/**
		 * Read some data into a {@link ByteBuffer}.
		 */
		void read(ByteBuffer dst, BiConsumer<Integer, Throwable> handler);

		@Override
		void close();
	}

}
