package de.gwdg.cdstar.rest.v3;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;

import de.gwdg.cdstar.runtime.client.CDStarAnnotateable;
import de.gwdg.cdstar.runtime.client.CDStarAttribute;
import de.gwdg.cdstar.runtime.client.CDStarAttributeHelper;
import de.gwdg.cdstar.web.common.model.ErrorResponse;
import de.gwdg.cdstar.web.common.model.MetaMap;

public class MetadataHelper {

	public static MetaMap getMetadata(CDStarAnnotateable src) {
		final MetaMap mmap = new MetaMap();
		src.getAttributes().stream().forEach(
			p -> mmap.put(p.getName(), p.values()));
		return mmap;
	}

	public static void setMetadata(CDStarAnnotateable src, MetaMap meta) {
		final Set<String> defined = src.getAttributeNames();
		final Map<String, List<String>> mmap = meta.getMap();

		// Insert/replace values defined by user
		for(Entry<String, List<String>> e: mmap.entrySet()) {
			String name = e.getKey();
			List<String> values = e.getValue();
			if(name == null)
				continue;
			if(!CDStarAttributeHelper.isValidAttributeName(name))
				throw new ErrorResponse(400, "InvalidFieldName", "Invalid metadata field name")
				.detail("field", name)
				.detail("pattern", CDStarAttribute.NAME_PATTERN.pattern());
			
			if (values != null)
				values.removeIf(Objects::isNull);
			if (values == null || values.isEmpty())
				src.getAttribute(name).clear();
			else
				src.getAttribute(name).set(values);
		}

		// Remove values not mentioned by user but present in source
		defined.forEach(name -> {
			if (!mmap.containsKey(name))
				src.getAttribute(name).clear();
		});
	}

	public static void clearMetadata(CDStarAnnotateable src) {
		src.getAttributes().forEach(CDStarAttribute::clear);
	}

}
