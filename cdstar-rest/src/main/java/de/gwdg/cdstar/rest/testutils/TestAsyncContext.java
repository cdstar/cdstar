package de.gwdg.cdstar.rest.testutils;

import java.nio.ByteBuffer;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import de.gwdg.cdstar.rest.api.AsyncCloseListener;
import de.gwdg.cdstar.rest.api.AsyncContext;
import de.gwdg.cdstar.rest.api.AsyncResultCallback;
import de.gwdg.cdstar.rest.api.RestContext;

public class TestAsyncContext implements AsyncContext {

	private final TestContext ctx;
	private volatile boolean endOfFile;
	private volatile boolean readPending = false;
	private volatile boolean writePending = false;
	private final List<AsyncCloseListener> onClose = new ArrayList<>(1);
	private Throwable error;

	public TestAsyncContext(TestContext ctx) {
		this.ctx = ctx;
	}

	@Override
	public synchronized void addCloseListener(AsyncCloseListener listener) {
		onClose.add(listener);
	}

	synchronized void runCloseListeners() {
		while (!onClose.isEmpty())
			onClose.remove(0).closed(this, error);
	}

	@Override
	public int getBufferSize() {
		return 1024 * 8;
	}

	@Override
	public boolean recycleBuffer(ByteBuffer buf) {
		return false;
	}

	@Override
	public ByteBuffer getBuffer() {
		return ByteBuffer.allocate(getBufferSize());
	}

	@Override
	public synchronized void asyncWrite(ByteBuffer buffer, AsyncResultCallback callback, Duration timeout) {
		if (ctx.isClosed())
			throw new IllegalStateException("Context closed");
		if (writePending)
			throw new IllegalStateException("Write task in progress");
		writePending = true;

		ctx.runInPool(() -> {
			try {
				ctx.write(buffer);
				writePending = false;
				callDone(callback, buffer, null);
			} catch (final Exception e) {
				writePending = false;
				callDone(callback, buffer, e);
			}
		});
	}

	@Override
	public synchronized void asyncRead(ByteBuffer buffer, AsyncResultCallback callback, Duration timeout) {
		if (!buffer.hasRemaining())
			throw new IllegalStateException("Buffer has no space left for reading");
		if (readPending)
			throw new IllegalStateException("Read task in progress");
		readPending = true;

		ctx.runInPool(() -> {
			try {
				if (!ctx.hasEntity())
					endOfFile = true;
				while (!endOfFile && buffer.hasRemaining()) {
					final int n = ctx.read(buffer.array(), buffer.arrayOffset() + buffer.position(),
						buffer.remaining());
					if (n > 0)
						buffer.position(buffer.position() + n);
					else if (n < 0)
						endOfFile = true;
				}
				buffer.flip();
				readPending = false;
				callDone(callback, buffer, null);
			} catch (final Exception e) {
				readPending = false;
				callDone(callback, buffer, e);
			}
		});
	}

	private void callDone(AsyncResultCallback callback, ByteBuffer buffer, Throwable e) {
		if (e != null && error == null)
			error = e;
		try {
			callback.done(this, buffer, e);
		} catch (final Exception e2) {
			if (e == null)
				callDone(callback, buffer, e2);
		}
	}

	@Override
	public boolean endOfStream() {
		return endOfFile;
	}

	@Override
	public RestContext getRequest() {
		return ctx;
	}

	@Override
	public CompletableFuture<Void> consumeRequest() {
		return CompletableFuture.completedFuture(null);
	}

	@Override
	public void keepAlive() {
		// ...
	}

}
