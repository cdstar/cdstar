package de.gwdg.cdstar.rest;

import java.util.HashMap;
import java.util.Map;

import de.gwdg.cdstar.rest.api.RequestHandler;
import de.gwdg.cdstar.rest.api.RestRoute;

/**
 * Note: The natural ordering of this class is based on its 'order' field. Since
 * non-equal routes may have the same priority, ordering is inconsistent with
 * {@link #equals(Object)}. When sorting a collection of routes, make sure to
 * use a stable sorting algorithm, so insertion order is preserved for routes
 * with the same priority.
 */

class RestRouteImpl implements RestRoute {
	private final RestConfigImpl config;
	final RouteTemplate route;
	Map<String, RequestHandler> methodHandler = new HashMap<>();
	int order = 0;

	String getRoute() {
		return route.template;
	}

	RestRouteImpl(RestConfigImpl config, String rule) {
		route = new RouteTemplate(rule, true);
		this.config = config;
	}

	@Override
	public String toString() {
		return "Route(" + route + ")";
	}

	@Override
	public RestRoute order(int order) {
		this.order = order;
		return this;
	}

	@Override
	public int getOrder() {
		return order;
	}

	@Override
	public String getRule() {
		return route.template;
	}

	@Override
	public Map<String, RequestHandler> getTargets() {
		return methodHandler;
	}

	@Override
	public RestRoute target(String method, RequestHandler target) {
		methodHandler.put(method.trim().toUpperCase(), target);
		config.reset();
		return this;
	}

}
