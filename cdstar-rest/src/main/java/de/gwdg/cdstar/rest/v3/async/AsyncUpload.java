package de.gwdg.cdstar.rest.v3.async;

import java.nio.ByteBuffer;
import java.nio.channels.WritableByteChannel;
import java.util.concurrent.CompletableFuture;

import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.rest.api.AsyncContext;
import de.gwdg.cdstar.rest.api.RestContext;

/**
 * Copy the content of a request (probably PUT) into an open
 * {@link WritableByteChannel}. The channel is NOT closed by this
 * handler.
 * 
 * Incomplete uploads can only be detected as an error if content-length
 * is known or chunked transfer encoding is used.
 */
public class AsyncUpload {
	private final RestContext ctx;
	private AsyncContext ac;
	private final CompletableFuture<Long> future;
	private final WritableByteChannel target;
	private long bytesWritten;

	public AsyncUpload(RestContext ctx, WritableByteChannel target) {
		this.ctx = ctx;
		this.target = target;
		future = new CompletableFuture<>();
	}

	public final CompletableFuture<Long> dispatch() {
		Utils.assertTrue(ac == null, "Already started");
		ac = ctx.startAsync();
		ac.asyncRead(ac.getBuffer(), this::onRead, future::completeExceptionally);
		return future;
	}

	protected AsyncContext getAsyncContext() {
		return ac;
	}

	public long getBytesWritten() {
		return bytesWritten;
	}

	private final void onRead(ByteBuffer buffer) throws Exception {
		while (buffer.hasRemaining())
			bytesWritten += target.write(buffer);

		if (ac.endOfStream()) {
			ac.recycleBuffer(buffer);
			future.complete(getBytesWritten());
		} else {
			buffer.clear();
			ac.asyncRead(buffer, this::onRead, future::completeExceptionally);
		}
	}

}
