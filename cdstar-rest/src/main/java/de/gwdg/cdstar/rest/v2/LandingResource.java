package de.gwdg.cdstar.rest.v2;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import javax.naming.NamingException;

import org.apache.commons.io.IOUtils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

import de.gwdg.cdstar.rest.api.RestConfig;
import de.gwdg.cdstar.rest.api.RestContext;
import de.gwdg.cdstar.rest.api.RestBlueprint;
import de.gwdg.cdstar.rest.utils.SessionHelper;
import de.gwdg.cdstar.rest.v2.model.ApiErrorResponse;
import de.gwdg.cdstar.rest.v2.utils.LegacyHelper;
import de.gwdg.cdstar.rest.v2.utils.Template;
import de.gwdg.cdstar.runtime.client.CDStarArchive;
import de.gwdg.cdstar.runtime.client.CDStarSession;
import de.gwdg.cdstar.runtime.client.exc.ArchiveNotFound;
import de.gwdg.cdstar.runtime.client.exc.VaultNotFound;
import de.gwdg.cdstar.web.common.model.ErrorResponse;

public class LandingResource implements RestBlueprint {

	@Override
	public void configure(RestConfig cfg) {
		cfg.route("/_assets/<asset:path>").GET(this::getAsset);
		cfg.route("/<vault>").GET(this::getIndexHtml);
		cfg.route("/<vault>/landing/<uid>").GET(this::getLandingPage);
	}

	static final ObjectMapper objectMapper = new ObjectMapper();
	static final ObjectWriter jsonFormater = objectMapper.writerWithDefaultPrettyPrinter();

	public Template getIndexHtml(RestContext ctx) throws IOException, NamingException {
		final String vault = ctx.getPathParam("vault");

		final Template tpl = new Template("index.html");
		tpl.put("vault", vault);

		try {
			SessionHelper.getCDStarSession(ctx, true).getVault(vault);
			return tpl;
		} catch (final VaultNotFound e) {
			throw new ErrorResponse(404, "NotFound", "Vault not found");
		}
	}

	public Template getLandingPage(RestContext ctx) throws IOException {
		final String vault = ctx.getPathParam("vault");
		final String uid = LegacyHelper.fromAnyID(ctx.getPathParam("uid"));

		final Template tpl = new Template("landing.html");
		tpl.put("vault", vault);

		try {
			final CDStarSession session = SessionHelper.getCDStarSession(ctx, true);
			final CDStarArchive ar = session.getVault(vault).loadArchive(uid);
			tpl.put("obj", ObjectsResource.makeObjectBean(ar));
			if (ar.hasFile(ApiV2Module.v2metaFile)) {
				try (InputStream io = ar.getFile(ApiV2Module.v2metaFile).getStream()) {
					tpl.put("metadata", IOUtils.toString(io, StandardCharsets.UTF_8));
				}
			} else {
				tpl.put("metadata", "");
			}
			return tpl;
		} catch (VaultNotFound | ArchiveNotFound e) {
			throw new ApiErrorResponse(404, "NotFound", "Object not found");
		}
	}

	public Void getAsset(RestContext ctx) throws IOException, NamingException {
		final String assetPath = ctx.getPathParam("asset");
		if (assetPath.contains("./") || assetPath.contains("../"))
			throw new ErrorResponse(404, "AssetNotFound", "Unable to serve asset").detail("path", assetPath);

		final String fullPath = "/de/gwdg/cdstar/web/v2/public/" + assetPath;
		try (InputStream io = getClass().getResourceAsStream(fullPath)) {
			if (io == null) {
				throw new ErrorResponse(404, "AssetNotFound", "Unable to serve asset").detail("path", assetPath);
			}
			ctx.write(io);
		}

		return null;
	}

}
