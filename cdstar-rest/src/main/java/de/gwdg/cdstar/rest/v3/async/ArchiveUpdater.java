package de.gwdg.cdstar.rest.v3.async;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.ByteBuffer;
import java.nio.channels.WritableByteChannel;
import java.nio.charset.StandardCharsets;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.CancellationException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import java.util.function.BiConsumer;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.gwdg.cdstar.MimeUtils;
import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.rest.api.AsyncContext;
import de.gwdg.cdstar.rest.api.RestContext;
import de.gwdg.cdstar.rest.utils.form.FormParser;
import de.gwdg.cdstar.rest.utils.form.FormParserException;
import de.gwdg.cdstar.rest.utils.form.FormPart;
import de.gwdg.cdstar.rest.v3.async.UrlFetchService.FetchHandle;
import de.gwdg.cdstar.rest.v3.errors.ApiErrors;
import de.gwdg.cdstar.runtime.RuntimeContext;
import de.gwdg.cdstar.runtime.client.CDStarACLEntry;
import de.gwdg.cdstar.runtime.client.CDStarArchive;
import de.gwdg.cdstar.runtime.client.CDStarAttribute;
import de.gwdg.cdstar.runtime.client.CDStarFile;
import de.gwdg.cdstar.runtime.client.CDStarProfile;
import de.gwdg.cdstar.runtime.client.auth.ArchivePermission;
import de.gwdg.cdstar.runtime.client.auth.ArchivePermissionSet;
import de.gwdg.cdstar.runtime.client.auth.StringSubject;
import de.gwdg.cdstar.runtime.client.exc.FileExists;
import de.gwdg.cdstar.runtime.client.exc.FileNotFound;
import de.gwdg.cdstar.runtime.client.exc.InvalidFileName;
import de.gwdg.cdstar.runtime.client.exc.ProfileNotDefined;
import de.gwdg.cdstar.runtime.client.utils.ArchiveOrSnapshot;
import de.gwdg.cdstar.web.common.model.ArchiveUpdated;
import de.gwdg.cdstar.web.common.model.ErrorResponse;

/**
 * Asynchronously parse the request body with the help of a {@link FormParser}.
 * Form fields are interpreted according to the form-based cdstar mini-language.
 *
 */
public class ArchiveUpdater {

	private volatile CDStarFile currentUpload;
	private WritableByteChannel currentUploadChannel;

	Set<String> clearedSchemata = new HashSet<>();
	Set<StringSubject> clearedACL = new HashSet<>();
	final ReportBuilder report;
	private AsyncContext ac;
	protected final FormParser parser;
	protected final Deque<FormPart> parts = new ArrayDeque<>();
	protected final RestContext ctx;
	CompletableFuture<ArchiveUpdated> future;
	private ArchiveOrSnapshot target;

	private static final Logger log = LoggerFactory.getLogger(ArchiveUpdater.class);

	public ArchiveUpdater(RestContext ctx, FormParser parser, ArchiveOrSnapshot target) {
		this.ctx = ctx;
		this.target = target;
		this.parser = parser;
		report = new ReportBuilder();
		future = new CompletableFuture<>();
	}

	public CompletableFuture<ArchiveUpdated> dispatch() {
		Utils.assertTrue(ac == null, "Already started");
		ac = ctx.startAsync();
		ac.asyncRead(ac.getBuffer(), this::onRead, this::onError, null);
		return future;
	}

	/**
	 * Called by {@link #start()} once, or {@link #process()} if it runs out of
	 * parts to process. Triggers {@link #process()} if new parts are available, or
	 * continues reading if not.
	 *
	 * @throws Exception
	 */
	private void onRead(ByteBuffer buffer) {
		try {
			if (buffer.hasRemaining())
				parts.addAll(parser.parse(buffer));
			if (ac.endOfStream())
				parts.addAll(parser.finish());
			process();
		} catch (final FormParserException e) {
			log.warn("Error while parsing update request.", e);
			final ErrorResponse error = new ErrorResponse(400, "BadRequest", "Error while parsing request.")
					.detail("content_type", ctx.getContentType()).detail("message", e.getMessage());
			onError(error);
		} finally {
			ac.recycleBuffer(buffer);
		}
	}

	/**
	 * Called by {@link #onRead(ByteBuffer)} or after a {@link #handlePart()} task
	 * completes. Consumes a single part (if available) and starts a new
	 * {@link #handlePart()} task. If no part is available, starts reading instead,
	 * which triggers {@link #onRead(ByteBuffer)} eventually.
	 */
	private void process() {
		try {
			if (parts.isEmpty()) {
				if (ac.endOfStream()) {
					future.complete(buildReport());
				} else {
					ac.asyncRead(ac.getBuffer(), this::onRead, this::onError, null);
				}
				return;
			}

			handlePart(parts.poll()).thenRun(() -> ctx.runInPool(this::process)).exceptionally(t -> {
				onError(t);
				return null;
			});

		} catch (final Exception e) {
			onError(e);
		}
	}

	private ArchiveUpdated buildReport() {
		// TODO: What about a SnapshotUpdated response?
		return report.build(target.map(a -> new ArchiveUpdated(a.getVault().getName(), a.getId(), a.getNextRev()),
				s -> new ArchiveUpdated(s.getSource().getVault().getName(), s.getSource().getId(), s.getRevision())));
	}

	private void onError(Throwable t) {
		while (t instanceof CompletionException)
			t = t.getCause();
		future.completeExceptionally(t);
	}

	/**
	 * Handle a single part and return a {@link CompletableFuture} or throw an
	 * exception.
	 *
	 * This method is allowed to block for a short time.
	 *
	 * @throws IOException
	 */
	private CompletableFuture<Void> handlePart(FormPart part) throws IOException {
		if (part.getName() == null)
			throw new ErrorResponse(400, "BadRequest", "Missing field name.");

		if (part.getName().startsWith("/")) {
			return handleUpload(part);
		} else {
			return handleTextCommand(part);
		}
	}

	private CompletableFuture<Void> handleUpload(FormPart part) throws IOException {

		if (currentUpload == null) {
			if (!part.getContentEncoding().equals("identity"))
				throw new ErrorResponse(415, "NotImplemented", "Compressed uploads are currently not supported.");

			String targetName = part.getName().substring(1);
			if (targetName.endsWith(CDStarFile.PATH_SEP) || targetName.isEmpty()) {
				// Upload into directory: append client-side filename
				if (part.getFileName() == null)
					throw new ErrorResponse(400, "BadRequest", "Missing filename header option.");
				targetName = targetName + part.getFileName();
			}

			String mediaType = part.getContentType();
			if (mediaType == null || mediaType.equals(MimeUtils.APPLICATION_X_AUTODETECT)) {
				// may return null, which is okay
				mediaType = MimeUtils.guess(targetName, false).getMime();
			}

			currentUpload = getFileForWriting(targetName);
			currentUpload.setMediaType(mediaType);
			currentUploadChannel = currentUpload.getWriteChannel();
		}

		assert part.getDrained() == currentUpload.getSize();

		final ByteBuffer chunk = part.drain();
		while (chunk.hasRemaining())
			currentUploadChannel.write(chunk);

		if (part.isComplete()) {
			currentUploadChannel.close();
			report.rememberFile(currentUpload);
			currentUploadChannel = null;
			currentUpload = null;
		}

		return CompletableFuture.completedFuture(null);
	}

	private CDStarFile getFileForWriting(String fileName) throws FileExists, InvalidFileName {
		return assumeArchive().createFile(Objects.requireNonNull(fileName));
	}

	private CompletableFuture<Void> handleTextCommand(FormPart part) {
		if (!part.isComplete())
			throw new ErrorResponse(400, "BadRequest", "Form field too long.");

		int i;
		String cmd;
		final String fieldName = part.getName();
		final String fieldValue = part.drainToString(StandardCharsets.UTF_8);
		String fileName = null;
		if ((i = fieldName.indexOf(":/")) != -1) {
			cmd = fieldName.substring(0, i);
			fileName = fieldName.substring(i + 2);
		} else {
			cmd = fieldName;
		}
		ErrorResponse error;

		try {
			if ("profile".equals(cmd)) {
				return handleProfile(fieldValue);
			}
			if ("copy".equals(cmd)) {
				return handleCopy(fileName, fixFilename(fieldValue));
			}
			if ("clone".equals(cmd)) {
				return handleClone(fileName, fixFilename(fieldValue));
			}
			if ("move".equals(cmd)) {
				return handleMove(fileName, fixFilename(fieldValue));
			}
			if ("delete".equals(cmd)) {
				return handleDelete(fileName, fixFilename(fieldValue));
			}
			if ("fetch".equals(cmd)) {
				return handleFetch(fileName, fieldValue);
			}
			if ("type".equals(cmd)) {
				return handleType(fileName, fieldValue);
			}
			if ("owner".equals(cmd)) {
				return handleOwner(fieldValue);
			}
			if (cmd.startsWith("meta:")) {
				final String attr = cmd.substring("meta:".length());
				return handleMeta(attr, fileName, fieldValue);
			}
			if (cmd.startsWith("acl:")) {
				final String subject = fieldName.substring("acl:".length());
				return handleAcl(subject, fieldValue);
			}

			error = new ErrorResponse(400, "UnrecognizedParameter", "Unrecognized parameter");
		} catch (final FileExists e) {
			// TODO: Keep all this error handling better in sync with stuff in
			// CDSTarRootModule
			if (Utils.equal(e.getConflict(), e.getName()))
				error = new ErrorResponse(409, "FileExists", "File name conflicts with existing file.").detail("file",
						e.getName());
			else
				error = new ErrorResponse(409, "FileNameConflict", "File name conflicts with an existing file.")
						.detail("file", e.getName()).detail("conflict", e.getConflict());
		} catch (final FileNotFound e) {
			error = new ErrorResponse(400, "FileNotFound", "File not found").detail("file", e.getName());
		} catch (final InvalidFileName e) {
			error = new ErrorResponse(400, "InvalidFileName", e.getDetail()).detail("file", e.getName());
		} catch (final ProfileNotDefined e) {
			error = new ErrorResponse(400, "UnknownProfile", "Unknown Profile").detail("profile", e.getName());
		} catch (final ErrorResponse e) {
			error = e;
		}

		error.detail("form", fieldName).detail("value", fieldValue);
		throw error;
	}

	private String fixFilename(String fname) {
		return fname.startsWith("/") ? fname.substring(1) : fname;
	}

	private CompletableFuture<Void> handleProfile(String fieldValue) throws ProfileNotDefined {
		final CDStarProfile profile = target.getSourceArchive().getVault().getProfileByName(fieldValue);
		target.setProfile(profile);
		return CompletableFuture.completedFuture(null);
	}

	private CompletableFuture<Void> handleCopy(String fileName, String fieldValue)
			throws FileNotFound, FileExists, InvalidFileName {
		CDStarArchive archive = assumeArchive();
		final CDStarFile source = archive.getFile(fieldValue);
		final CDStarFile target = getFileForWriting(fileName);

		target.setMediaType(source.getMediaType(), source.getContentEncoding());
		try {
			target.transferFrom(source);
		} catch (IOException e) {
			throw new ErrorResponse(500, "CopyFailed", "File copy operation failed");
		}

		report.rememberFile(target);
		return CompletableFuture.completedFuture(null);
	}

	private CDStarArchive assumeArchive() {
		return target.asArchive().orElseThrow(
				() -> new ErrorResponse(400, "UnsupporedOperation", "This operation is not allowed on snapshots."));
	}

	private CompletableFuture<Void> handleClone(String fileName, String fieldValue)
			throws FileNotFound, FileExists, InvalidFileName {
		CDStarArchive archive = assumeArchive();
		final CDStarFile source = archive.getFile(fieldValue);
		final CDStarFile target = getFileForWriting(fileName);

		target.setMediaType(source.getMediaType(), source.getContentEncoding());
		try {
			target.transferFrom(source);
		} catch (IOException e) {
			throw new ErrorResponse(500, "CloneFailed", "File clone operation failed");
		}

		target.getAttributes().forEach(CDStarAttribute::clear);
		source.getAttributes().forEach(attr -> {
			target.getAttribute(attr.getName()).set(attr.values());
			report.addMeta(attr, target);
		});

		report.rememberFile(target);
		return CompletableFuture.completedFuture(null);
	}

	private CompletableFuture<Void> handleMove(String fileName, String fieldValue)
			throws FileNotFound, FileExists, InvalidFileName {

		var source = assumeArchive().getFile(fieldValue);
		source.setName(fileName);
		report.rememberFile(source);

		return CompletableFuture.completedFuture(null);
	}

	private CompletableFuture<Void> handleDelete(String fileName, String fieldValue) throws FileNotFound {
		CDStarArchive archive = assumeArchive();
		if (fileName.endsWith(CDStarFile.PATH_SEP)) {
			archive.getFiles().stream().filter(f -> f.getName().startsWith(fileName)).collect(Collectors.toList())
					.forEach(file -> {
						report.rememberFile(file);
						file.remove();
					});
		} else {
			final CDStarFile file = archive.getFile(fileName);
			report.rememberFile(file);
			file.remove();
		}

		return CompletableFuture.completedFuture(null);
	}

	private CompletableFuture<Void> handleFetch(String fileName, String fieldValue) throws FileExists, InvalidFileName {
		URI uri;
		try {
			uri = new URI(fieldValue);
		} catch (final URISyntaxException e) {
			throw ApiErrors.badRequest("Bad fetch URI.");
		}

		UrlFetchService fetchHandler = ctx.getConfig()
				.lookup(RuntimeContext.class)
				.lookupAll(UrlFetchService.class)
				.stream()
				.filter(c -> c.canHandle(uri))
				.findFirst()
				.orElseThrow(() -> ApiErrors.notImplemented("Fetch handler not found for the given URI."));

		FetchHandle fetch = fetchHandler.resolve(uri); // May throw ErrorResponse

		CDStarFile target;
		WritableByteChannel writeChannel;
		try {
			target = getFileForWriting(fileName);
			writeChannel = Utils.wrapError(target::getWriteChannel);
		} catch (Exception e) {
			Utils.closeQuietly(fetch);
			// Keep writeChannel open on errors, so it triggers a rollback later!
			throw e;
		}

		var buf = ac.getBuffer();
		var cf = new CompletableFuture<Void>();
		var fetchReadHandler = new BiConsumer<Integer, Throwable>() {

			long bytesFetched = 0;

			@Override
			public void accept(Integer result, Throwable err) {
				if (cf.isDone())
					return;

				if (err != null) {
					cf.completeExceptionally(err);
					return;
				}

				try {
					buf.flip();
					bytesFetched += buf.remaining();
					while (buf.remaining() > 0)
						writeChannel.write(buf);
					if (bytesFetched < fetch.size()) {
						ac.keepAlive();
						buf.clear();
						fetch.read(buf, this);
					} else {
						cf.complete(null);
					}
				} catch (Exception e) {
					cf.completeExceptionally(e);
				}
			}
		};

		try {
			fetch.read(buf, fetchReadHandler);
		} catch (Exception e) {
			cf.completeExceptionally(e);
		}

		return cf.handle((res, err) -> {
			ac.recycleBuffer(buf);
			Utils.closeQuietly(fetch);
			if (err instanceof CancellationException)
				throw new ErrorResponse(500, "FetchFailed", "Fetch operation canceled.");
			if (err != null) {
				log.warn("Fetch operation failed", err);
				throw new ErrorResponse(500, "FetchFailed", "Fetch operation failed.")
						.detail("error", err.getMessage());
			}
			Utils.closeQuietly(writeChannel); // Keep open on errors
			report.rememberFile(target);
			return res;
		});
	}

	private CompletableFuture<Void> handleType(String fileName, String fieldValue) throws FileNotFound {
		CDStarArchive archive = assumeArchive();
		final CDStarFile target = archive.getFile(fileName);
		if (fieldValue == null || fieldValue.isEmpty() || fieldValue.equals(MimeUtils.APPLICATION_X_AUTODETECT))
			fieldValue = MimeUtils.guess(fileName, false).getMimeDefault(MimeUtils.OCTET_STREAM);
		target.setMediaType(fieldValue);
		report.rememberFile(target);
		return CompletableFuture.completedFuture(null);
	}

	private CompletableFuture<Void> handleMeta(String attrName, String fileName, String fieldValue)
			throws FileNotFound {
		CDStarArchive archive = assumeArchive();
		CDStarAttribute prop;
		CDStarFile file = null;
		try {
			if (fileName != null) {
				file = archive.getFile(fileName);
				prop = file.getAttribute(attrName);
			} else {
				prop = archive.getAttribute(attrName);
			}
		} catch (final IllegalArgumentException e) {
			throw new ErrorResponse(400, "InvalidAttributeName", "Could not parse meta attribute name.");
		}

		if (clearedSchemata.add(fileName == null ? attrName : attrName + ":/" + fileName)) {
			if (Utils.nullOrEmpty(fieldValue))
				prop.clear();
			else
				prop.set(fieldValue);
			report.addMeta(prop, file);
		} else {
			prop.append(fieldValue);
		}

		return CompletableFuture.completedFuture(null);
	}

	private CompletableFuture<Void> handleOwner(String newOwner) throws ProfileNotDefined {
		CDStarArchive archive = assumeArchive();
		archive.setOwner(newOwner);
		return CompletableFuture.completedFuture(null);
	}

	private CompletableFuture<Void> handleAcl(String subject, String fieldValue) {
		CDStarArchive archive = assumeArchive();
		CDStarACLEntry acl;
		try {
			acl = archive.getACL().forSubject(StringSubject.fromString(subject));
		} catch (final IllegalArgumentException e) {
			throw new ErrorResponse(400, "InvalidSubject", "Subject not recognized").detail("subject", subject);
		}

		if (clearedACL.add(acl.getSubject())) {
			acl.revokeAll();
		}

		if (fieldValue.isEmpty()) {
			// Done. keep all ACLs for this subject revoked.
			return CompletableFuture.completedFuture(null);
		}

		for (final String grant : fieldValue.split("\\s*,\\s*")) {
			try {
				if (Utils.isUpperCase(grant)) {
					acl.permit(ArchivePermissionSet.valueOf(grant));
				} else {
					acl.permit(ArchivePermission.valueOf(grant.toUpperCase()));
				}
			} catch (final IllegalArgumentException e) {
				throw new ErrorResponse(400, "UnknownPermission", "Invalid permission name").detail("permission",
						grant);
			}
		}

		return CompletableFuture.completedFuture(null);
	}

}
