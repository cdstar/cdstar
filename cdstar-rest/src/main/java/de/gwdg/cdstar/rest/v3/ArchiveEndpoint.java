package de.gwdg.cdstar.rest.v3;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.codahale.metrics.MetricRegistry;

import de.gwdg.cdstar.Promise;
import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.pool.PoolError.NotFound;
import de.gwdg.cdstar.pool.StoragePool;
import de.gwdg.cdstar.rest.api.AsyncContext;
import de.gwdg.cdstar.rest.api.HttpError.NotAcceptable;
import de.gwdg.cdstar.rest.api.RestBlueprint;
import de.gwdg.cdstar.rest.api.RestConfig;
import de.gwdg.cdstar.rest.api.RestContext;
import de.gwdg.cdstar.rest.utils.FormHelper;
import de.gwdg.cdstar.rest.utils.FormHelper.EntityTooLarge;
import de.gwdg.cdstar.rest.utils.FormHelper.FormFieldTooLarge;
import de.gwdg.cdstar.rest.utils.FormHelper.UnsupportedContentType;
import de.gwdg.cdstar.rest.utils.GlobPattern;
import de.gwdg.cdstar.rest.utils.IncludeExcludeFilter;
import de.gwdg.cdstar.rest.utils.QueryHelper;
import de.gwdg.cdstar.rest.utils.RequestThrottle;
import de.gwdg.cdstar.rest.utils.RestUtils;
import de.gwdg.cdstar.rest.utils.SessionHelper;
import de.gwdg.cdstar.rest.utils.form.FormParserException;
import de.gwdg.cdstar.rest.utils.form.MultipartParser;
import de.gwdg.cdstar.rest.utils.form.UrlEncodedParser;
import de.gwdg.cdstar.rest.v3.async.ArchiveImporter;
import de.gwdg.cdstar.rest.v3.async.ArchiveUpdater;
import de.gwdg.cdstar.rest.v3.async.AsyncExportBase;
import de.gwdg.cdstar.rest.v3.async.AsyncZipExport;
import de.gwdg.cdstar.rest.v3.async.ModelHelper;
import de.gwdg.cdstar.runtime.RuntimeContext;
import de.gwdg.cdstar.runtime.client.CDStarACL;
import de.gwdg.cdstar.runtime.client.CDStarACLEntry;
import de.gwdg.cdstar.runtime.client.CDStarArchive;
import de.gwdg.cdstar.runtime.client.CDStarAttribute;
import de.gwdg.cdstar.runtime.client.CDStarFile;
import de.gwdg.cdstar.runtime.client.CDStarSnapshot;
import de.gwdg.cdstar.runtime.client.CDStarVault;
import de.gwdg.cdstar.runtime.client.auth.ArchivePermission;
import de.gwdg.cdstar.runtime.client.auth.ArchivePermissionSet;
import de.gwdg.cdstar.runtime.client.auth.StringSubject;
import de.gwdg.cdstar.runtime.client.exc.AccessError;
import de.gwdg.cdstar.runtime.client.exc.ArchiveNotFound;
import de.gwdg.cdstar.runtime.client.exc.InvalidSnapshotName;
import de.gwdg.cdstar.runtime.client.exc.SnapshotNotFound;
import de.gwdg.cdstar.runtime.client.exc.VaultNotFound;
import de.gwdg.cdstar.runtime.client.utils.ArchiveOrSnapshot;
import de.gwdg.cdstar.runtime.services.VaultRegistry;
import de.gwdg.cdstar.ta.exc.TARollbackException;
import de.gwdg.cdstar.web.common.model.AclMap;
import de.gwdg.cdstar.web.common.model.ArchiveInfo;
import de.gwdg.cdstar.web.common.model.ArchiveUpdated;
import de.gwdg.cdstar.web.common.model.ErrorResponse;
import de.gwdg.cdstar.web.common.model.FileInfo;
import de.gwdg.cdstar.web.common.model.FileList;
import de.gwdg.cdstar.web.common.model.MetaMap;
import de.gwdg.cdstar.web.common.model.SnapshotInfo;
import de.gwdg.cdstar.web.common.model.SnapshotList;

public class ArchiveEndpoint implements RestBlueprint {

	private static final Set<String> withOptions = new HashSet<>(Arrays.asList("files", "meta", "acl", "snapshots"));
	private RequestThrottle zipBreaker;

	@Override
	public void configure(RestConfig cfg) {
		cfg.route("/<vault:re:[^_][^/]*>/<archive:re:[^_][^/]*>")
			.GET(this::handleGet)
			.POST(this::handlePost)
			.PUT(this::handlePut)
			.DELETE(this::handleDelete);

		// Limit parallel zip exports to an arbitrary number
		zipBreaker = new RequestThrottle(16);

		cfg.lookup(RuntimeContext.class).lookup(MetricRegistry.class).ifPresent(metrics -> {
			metrics.gauge("rest.zipExport.slots", () -> () -> zipBreaker.getSlots() - zipBreaker.getSlotsUsed());
		});
	}

	private static Map<String, Comparator<CDStarFile>> comparators = new HashMap<>();
	static {
		comparators.put("name", Comparator.comparing(CDStarFile::getName));
		comparators.put("type", Comparator.comparing(CDStarFile::getMediaType));
		comparators.put("size", Comparator.comparing(CDStarFile::getSize));
		comparators.put("created", Comparator.comparing(CDStarFile::getCreated));
		comparators.put("modified", Comparator.comparing(CDStarFile::getLastModified));
		comparators.put("hash",
				Comparator.comparing(f -> f.getDigests().get(CDStarFile.DIGEST_SHA256)));
		comparators.put("id", Comparator.comparing(CDStarFile::getID));
	}

	/**
	 * Load an archive or snapshot for the given archive reference string.
	 *
	 * @throws ArchiveNotFound
	 * @throws SnapshotNotFound
	 */
	static ArchiveOrSnapshot resolveId(CDStarVault vault, String archiveRef) throws ArchiveNotFound, SnapshotNotFound {
		final int i = archiveRef.indexOf(CDStarSnapshot.ID_SEP);
		if (i > 0) {
			final String snapName = archiveRef.substring(i + 1);
			String archiveId = archiveRef.substring(0, i);
			CDStarArchive archive = vault.loadArchive(archiveId);
			CDStarSnapshot snapshot = archive.getSnapshot(snapName)
					.orElseThrow(() -> new SnapshotNotFound(vault.getName(), archiveRef));
			return new ArchiveOrSnapshot(archive, snapshot);
		}
		return new ArchiveOrSnapshot(vault.loadArchive(archiveRef));
	}

	public Object handleGet(RestContext ctx)
		throws NotAcceptable, IOException, ArchiveNotFound, VaultNotFound, SnapshotNotFound {

		final QueryHelper qh = new QueryHelper(ctx);
		final String subResource = qh.getExclusiveParam("acl", "meta", "files", "export", "snapshots");

		if ("export".equals(subResource)) {
			zipBreaker.handleThrottled(ctx.startAsync(), 1, this::handleZipExport);
			return null;
		}

		final CDStarVault vault = SessionHelper.getCDStarSession(ctx, true).getVault(ctx.getPathParam("vault"));
		ArchiveOrSnapshot source = resolveId(vault, ctx.getPathParam("archive"));

		if ("acl".equals(subResource)) {
			final boolean groupBySet = !qh.getOption("acl", "", "group", "explode").equals("explode");
			qh.ensureNoUnusedParameters();
			return getArchiveAcl(source.getSourceArchive(), groupBySet);
		}

		if ("meta".equals(subResource)) {
			qh.ensureNoUnusedParameters();
			Set<CDStarAttribute> attrs = source.getAttributes();
			return getArchiveMeta(attrs);
		}

		if ("files".equals(subResource)) {
			final List<CDStarFile> files = source.getFiles();
			final int total = files.size();
			final Stream<FileInfo> filtered = getFilesFiltered(files, qh);
			qh.ensureNoUnusedParameters();
			return new FileList(filtered.collect(Collectors.toList()), total);
		}

		if ("snapshots".equals(subResource)) {
			final List<SnapshotInfo> snapshots = source.getSourceArchive().getSnapshots().stream()
					.map(this::snapshotInfo).collect(Collectors.toList());
			qh.ensureNoUnusedParameters();
			return new SnapshotList(snapshots, snapshots.size());
		}

		// No subresource request

		final Set<String> with = qh.getCsvOptions("with", withOptions);
		final boolean showFiles = with.contains("files")
			|| qh.has("include") || qh.has("exclude") || qh.has("order")
			|| qh.has("reverse") || qh.has("offset") || qh.has("limit");
		final boolean showMeta = with.contains("meta");
		final boolean showACL = with.contains("acl");
		final boolean showSnaps = with.contains("snapshots");

		final ArchiveInfo info = new ArchiveInfo();
		info.id = source.getId(); // id@name for snapshots
		info.vault = vault.getName();
		info.revision = source.getRev();
		info.created = source.getSourceArchive().getCreated();
		info.modified = source.map(CDStarArchive::getContentModified, CDStarSnapshot::getCreated);
		info.owner = source.getSourceArchive().getOwner();
		info.file_count = source.getFileCount();

		// TODO: The 'default' snapshots profile inherits the archive profile?
		info.profile = source.getProfile().getName();
		info.state = ModelHelper.getArchiveState(source.getMirrorState());

		ctx.header("Last-Modified", source.getModified()); // NOT getContentModified

		if (showACL) {
			try {
				info.acl = getArchiveAcl(source.getSourceArchive(), true);
			} catch (final AccessError e) {
				// Just keep it blank
			}
		}

		if (showMeta) {
			try {
				info.meta = getArchiveMeta(source.getAttributes());
			} catch (final AccessError e) {
				// Just keep it blank
			}
		}

		if (showFiles) {
			try {
				info.files = getFilesFiltered(source.getFiles(), qh).collect(Collectors.toList());
			} catch (final AccessError e) {
				// Just keep it blank
			}
		}

		if (showSnaps) {
			try {
				info.snapshots = source.getSourceArchive().getSnapshots().stream()
						.map(this::snapshotInfo).collect(Collectors.toList());
			} catch (final AccessError e) {
				// Just keep it blank
			}
		}

		return info;
	}

	SnapshotInfo snapshotInfo(final CDStarSnapshot snap) {
		final SnapshotInfo info = new SnapshotInfo();
		info.name = snap.getName();
		info.revision = snap.getRevision();
		info.creator = snap.getCreator();
		info.created = snap.getCreated();
		info.profile = snap.getProfile().getName();
		return info;
	}

	private Void handleZipExport(AsyncContext ac) throws ArchiveNotFound, VaultNotFound, SnapshotNotFound {

		final QueryHelper qh = new QueryHelper(ac.getRequest());

		// TODO: Add more modes (e.g. bagit, tar, tgz, ...)
		// TODO: Allow an empty 'export' parameter and read the desired format from the
		// `Accept` header.
		AsyncExportBase export;
		String ctype;
		String ext;
		switch (qh.getOption("export", "zip")) {
		case ("zip"):
			ctype = "application/zip";
			ext = "zip";
			export = new AsyncZipExport();
			break;
		default:
			throw Utils.wtf();
		}

		final RestContext ctx = ac.getRequest();
		final CDStarVault vault = SessionHelper.getCDStarSession(ctx, true).getVault(ctx.getPathParam("vault"));
		ArchiveOrSnapshot source = resolveId(vault, ctx.getPathParam("archive"));
		final List<CDStarFile> files = Utils.toArrayList(source.getFiles());

		// Handle include and exclude rules
		final List<GlobPattern> include = qh.getFiltered("include", (s) -> new GlobPattern(s));
		final List<GlobPattern> exclude = qh.getFiltered("exclude", (s) -> new GlobPattern(s));
		final IncludeExcludeFilter<String> filter = new IncludeExcludeFilter<>(include, exclude);
		files.removeIf(f -> !filter.test("/" + f.getName()));

		final String filename = source.getSourceArchive().getId()
				+ source.asSnapshot().map(s -> "-" + s.getName()).orElse("") + "." + ext;

		ctx.header("Content-Type", ctype);
		ctx.header("Content-Disposition", "attachment; filename=\"" + filename + "\"");

		export.setWatermark(1024 * 32);
		export.addFiles(files);
		export.dispatch(ac);

		return null;
	}

	public Object handlePost(RestContext ctx) throws Exception {
		final CDStarVault vault = SessionHelper.getCDStarSession(ctx, false).getVault(ctx.getPathParam("vault"));
		ArchiveOrSnapshot source = resolveId(vault, ctx.getPathParam("archive"));

		final QueryHelper qh = new QueryHelper(ctx);
		final String action = qh.getExclusiveParam("trim", "snapshots");

		if ("trim".equals(action)) {
			handleTrim(ctx, ctx.getPathParam("vault"), ctx.getPathParam("archive"));
			return null;
		}

		if ("snapshots".equals(action))
			return createSnapshot(vault, assumeArchive(source), ctx);

		ctx.status(200);
		return createOrUpdate(ctx, source);
	}

	private Void handleTrim(RestContext ctx, String vault, String archive) throws VaultNotFound, ArchiveNotFound {
		VaultRegistry vr = ctx.getConfig().lookup(RuntimeContext.class).lookupRequired(VaultRegistry.class);
		StoragePool pool = vr.getVault(vault).map(vr::getPoolFor).orElseThrow(() -> new VaultNotFound(vault));
		SessionHelper.ensurePermitted(ctx, ArchivePermission.TRIM.toStringPermission(vault, archive));
		try {
			pool.trimObject(archive);
		} catch (NotFound e) {
			throw new ArchiveNotFound(vault, archive);
		}
		return null;
	}

	static CDStarArchive assumeArchive(ArchiveOrSnapshot source) {
		return source.asArchive()
				.orElseThrow(() -> new ErrorResponse(400, "ApiError", "This operation is not suppoted on snapshots."));
	}

	private SnapshotInfo createSnapshot(CDStarVault vault, CDStarArchive archive, RestContext ctx)
		throws IOException, TARollbackException {
		final FormHelper form = parseForm(ctx);

		final String name = form.get("name")
			.orElseThrow(() -> new ErrorResponse(400, "ApiError", "Missing form parameter")
				.detail("field", "name"));

		try {
			ctx.status(201);
			final SnapshotInfo info = snapshotInfo(archive.createSnapshot(name));
			SessionHelper.commitOrSuspend(ctx);
			return info;
		} catch (final InvalidSnapshotName e) {
			throw new ErrorResponse(400, "InvalidSnapshotName", "Invalid snapshot name");
		}
	}

	static FormHelper parseForm(RestContext ctx) throws IOException {
		try {
			return FormHelper.parse(ctx, 1024, 1024 * 8);
		} catch (final EntityTooLarge e) {
			throw new ErrorResponse(413, "FormTooLarge", "Form request larger than expected.");
		} catch (final UnsupportedContentType e) {
			throw new ErrorResponse(415, "FormRequired", "Form request expected.");
		} catch (final FormFieldTooLarge e) {
			throw new ErrorResponse(422, "FormFieldTooLarge", "An element of the form request was too large.");
		} catch (final FormParserException e) {
			throw new ErrorResponse(400, "FormParserError", "Unable to parse form request.");
		}
	}

	/**
	 * Handle PUT requests for ?acl and ?meta
	 */
	public Void handlePut(RestContext ctx) throws Exception {

		final CDStarVault vault = SessionHelper.getCDStarSession(ctx, false).getVault(ctx.getPathParam("vault"));
		ArchiveOrSnapshot source = resolveId(vault, ctx.getPathParam("archive"));

		final QueryHelper qh = new QueryHelper(ctx);
		final String subResource = qh.getExclusiveParam("acl", "meta");

		if ("acl".equals(subResource)) {
			return handlePutAcl(assumeArchive(source), ctx);
		}

		if ("meta".equals(subResource)) {
			MetadataHelper.setMetadata(assumeArchive(source), RestUtils.parseJsonBody(ctx, MetaMap.class));
			SessionHelper.commitOrSuspend(ctx);
			ctx.status(204);
			return null;
		}

		throw new ErrorResponse(400, "ApiError", "Either 'acl' or 'meta' subresource specifier is required");
	}

	public Void handleDelete(RestContext ctx) throws Exception {

		final CDStarVault vault = SessionHelper.getCDStarSession(ctx, false).getVault(ctx.getPathParam("vault"));
		ArchiveOrSnapshot source = resolveId(vault, ctx.getPathParam("archive"));

		final QueryHelper qh = new QueryHelper(ctx);
		final String subResource = qh.getExclusiveParam("meta");

		// Handle DELETE ?meta. This is undocumented, but a safeguard against
		// clients that assume full CRUD support for the ?meta subresource.
		if ("meta".equals(subResource)) {
			MetadataHelper.clearMetadata(assumeArchive(source));
			SessionHelper.commitOrSuspend(ctx);
			ctx.status(204);
			return null;
		}

		source.remove();

		// Remove entire archive
		SessionHelper.commitOrSuspend(ctx);
		ctx.status(204); // No Content
		return null;
	}

	private static Stream<FileInfo> getFilesFiltered(List<CDStarFile> files, QueryHelper qh) {
		qh.setDefault("limit", "25");
		qh.setDefault("offset", "0");
		qh.setDefault("order", "name");

		final boolean withMeta = qh.getCsvOptions("with", withOptions).contains("meta");
		final List<GlobPattern> include = qh.getFiltered("include", (s) -> new GlobPattern(s));
		final List<GlobPattern> exclude = qh.getFiltered("exclude", (s) -> new GlobPattern(s));

		final Comparator<CDStarFile> order = qh.getMap("order", comparators);
		final boolean reverse = qh.getBoolean("reverse");
		final int offset = qh.getInt("offset", 0, Integer.MAX_VALUE);
		final int limit = Utils.gate(0, qh.getInt("limit", 0, Integer.MAX_VALUE), 1000);

		Stream<CDStarFile> stream = files.stream();
		stream = stream.sorted(reverse ? Collections.reverseOrder(order) : order);

		if (include.size() + exclude.size() != 0) {
			final IncludeExcludeFilter<String> filter = new IncludeExcludeFilter<>(include, exclude);
			stream = stream.filter(file -> filter.test("/" + file.getName()));
		}

		if (offset > 0)
			stream = stream.skip(offset);
		if (limit < files.size() - offset)
			stream = stream.limit(limit);

		return stream.map(f -> {
			return ModelHelper.makeFileInfo(f, withMeta);
		});
	}

	static MetaMap getArchiveMeta(Set<CDStarAttribute> attributes) {
		final MetaMap mmap = new MetaMap();
		attributes.forEach(p -> mmap.put(p.getName(), p.values()));
		return mmap;
	}

	public static AclMap getArchiveAcl(CDStarArchive archive, boolean groupBySet) {
		final AclMap acl = new AclMap();
		for (final CDStarACLEntry entry : archive.getACL().getAccessList()) {
			final List<String> values = new ArrayList<>();
			final Set<ArchivePermission> loosePermissions = entry.getPermissions();

			if (groupBySet) {
				ArchivePermissionSet.group(loosePermissions).forEach(set -> {
					values.add(set.name().toUpperCase());
					loosePermissions.removeAll(set.getPermissions());
				});
			}

			// Add remaining loose permissions to result.
			loosePermissions.forEach(p -> values.add(p.name().toLowerCase()));
			acl.put(entry.getSubject().toString(), values);
		}
		return acl;
	}

	static Void createOrUpdate(RestContext ctx, ArchiveOrSnapshot source) {

		Promise<ArchiveUpdated> p;

		if (ctx.isMultipart()) {
			try {
				// TODO: Do not hard-code "UTF-8" as encoding...
				final MultipartParser form = new MultipartParser(ctx.getHeader("Content-Type"), "UTF-8", 1024 * 64);
				p = Promise.wrap(new ArchiveUpdater(ctx, form, source).dispatch());
			} catch (final IllegalStateException e) {
				throw new ErrorResponse(400, "InvalidRequest",
						"Request detected as multipart/form+data, but failed to parse.");
			}
		} else if (ctx.isForm() || Utils.nullOrEmpty(ctx.getContentType())) {
			final UrlEncodedParser form = new UrlEncodedParser(1024 * 8, true, StandardCharsets.UTF_8);
			p = Promise.wrap(new ArchiveUpdater(ctx, form, source).dispatch());
		} else if (ArchiveImporter.isSupported(ctx.getContentType())) {
			p = new ArchiveImporter(ctx, assumeArchive(source), -1).dispatch();
		} else {
			throw new ErrorResponse(415, "ApiError", "Unsupported content type");
		}

		p.then(report -> {
			try {
				// Status and location header already set by caller.
				SessionHelper.commitOrSuspend(ctx);
				ctx.write(report);
				ctx.close();
			} catch (final Exception e) {
				ctx.abort(e);
			}
		}, error -> {
			if (!ctx.isCommitted())
				ctx.abort(error);
		});

		return null;
	}

	private static Void handlePutAcl(CDStarArchive archive, RestContext ctx) throws IOException, TARollbackException {
		final CDStarACL acl = archive.getACL();

		final Map<String, List<String>> aclMap = RestUtils.parseJsonBody(ctx, AclMap.class).getMap();
		acl.getAccessList().forEach(e -> e.revokeAll());

		for (final Entry<String, List<String>> e : aclMap.entrySet()) {
			final CDStarACLEntry en = acl.forSubject(StringSubject.fromString(e.getKey()));
			for (final String permit : e.getValue()) {
				try {
					if (Utils.isLowerCase(permit))
						en.permit(ArchivePermission.valueOf(permit.toUpperCase()));
					else
						en.permit(ArchivePermissionSet.valueOf(permit));
				} catch (final IllegalArgumentException err) {
					throw new ErrorResponse(400, "UnknownPermission", "Invalid permission name").cause(err)
						.detail("permission", permit);
				}
			}
		}

		SessionHelper.commitOrSuspend(ctx);
		ctx.status(200);
		return null;
	}

}
