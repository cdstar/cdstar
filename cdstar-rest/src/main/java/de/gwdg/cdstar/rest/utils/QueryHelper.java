package de.gwdg.cdstar.rest.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import de.gwdg.cdstar.rest.api.RestContext;
import de.gwdg.cdstar.web.common.model.ErrorResponse;

public class QueryHelper {
	private final RestContext ctx;
	private final Map<String, String[]> defaults = new HashMap<>();
	private final Set<String> visted = new HashSet<>();

	public QueryHelper(RestContext ctx) {
		this.ctx = ctx;
	}

	public ErrorResponse error(String name, String value, String message) {
		return new ErrorResponse(400, "ApiError", message).detail("param", name).detail("value", value);
	}

	public void setDefault(String name, String... values) {
		if (values.length == 0)
			throw new IllegalArgumentException("Must define at least one default value");
		defaults.put(name, values);
	}

	public boolean has(String name) {
		return ctx.getQueryParams(name).length > 0;
	}

	/**
	 * Get a list of values for this parameter. If no value is present, an error
	 * is returned.
	 */
	public String[] getAll(String name) {
		final String[] values = getAny(name);
		if (values.length == 0)
			throw error(name, null, "Missing query parameter");
		return values;
	}

	/**
	 * Get a (possibly empty) list of values for this parameter.
	 */
	public String[] getAny(String name) {
		String[] values = ctx.getQueryParams(name);
		visted.add(name);
		if (values.length == 0)
			values = defaults.getOrDefault(name, new String[] {});
		return values;
	}

	public String get(String name) {
		String value = ctx.getQueryParam(name);
		visted.add(name);
		if (value == null && defaults.containsKey(name))
			value = defaults.get(name)[0];
		if (value == null)
			throw error(name, null, "Missing query parameter");
		return value;
	}

	public boolean getBoolean(String name) {
		if (!has(name))
			return false;
		final String value = get(name);
		if (value.isEmpty() || value.equalsIgnoreCase("true"))
			return true;
		else if (value.equalsIgnoreCase("false"))
			return false;
		else
			throw error(name, value, "Boolean parameters should be 'true' or 'false' only");
	}

	public long getLong(String name, long min, long max) {
		final String value = get(name);
		long parsed;
		try {
			parsed = Long.parseLong(value);
		} catch (final NumberFormatException e) {
			throw error(name, value, "Not a number");
		}
		if (parsed < min || parsed > max)
			throw error(name, value, "Number out of range").detail("min", min).detail("max", max);
		return parsed;
	}

	public long getLong(String name) {
		return getLong(name, Long.MIN_VALUE, Long.MAX_VALUE);
	}

	public int getInt(String name, int min, int max) {
		// Cast is save since we are guaranteed within integer range.
		return (int) getLong(name, min, max);
	}

	public int getInt(String name) {
		return getInt(name, Integer.MIN_VALUE, Integer.MAX_VALUE);
	}

	public double getDouble(String name, double min, double max) {
		final String value = get(name);
		double parsed;
		try {
			parsed = Double.parseDouble(value);
		} catch (final NumberFormatException e) {
			throw error(name, value, "Not a number");
		}
		if (Double.isFinite(parsed) || parsed < min || parsed > max)
			throw error(name, value, "Number out of range").detail("min", min).detail("max", max);
		return parsed;
	}

	public double getDouble(String name) {
		return getDouble(name, -Double.MAX_VALUE, Double.MAX_VALUE);
	}

	@FunctionalInterface
	public interface Mapper<T> {
		T map(String value) throws Exception;
	}

	/**
	 * Return all values for which a mapping filter does not return null.
	 * Exceptions raised from the mapping function are handled as parameter
	 * parser errors.
	 */
	public <T> List<T> getFiltered(String name, Mapper<T> mapper) {
		final String[] values = getAny(name);
		if (values.length == 0)
			return Collections.emptyList();
		final ArrayList<T> results = new ArrayList<>(values.length);
		for (final String val : values) {
			try {
				final T mapped = mapper.map(val);
				if (mapped == null)
					continue;
				results.add(mapped);
			} catch (final IllegalArgumentException e) {
				throw error(name, val, e.getMessage());
			} catch (final Exception e) {
				throw error(name, val, "Could not parse parameter").cause(e);
			}
		}
		return results;
	}

	/**
	 * Expect a parameter to be defined, and its value to be in a set of allowed
	 * options. Return the value.
	 */
	public String getOption(String name, String... options) {
		final String value = get(name);
		for (final String v : options)
			if (value.equals(v))
				return value;
		throw error(name, value, "Value does not match any of the available options")
			.detail("options", Arrays.asList(options));
	}

	/**
	 * Expect a parameter to be defined, and its value to be in a set of allowed
	 * options. Return the value.
	 */
	public String getOption(String name, Set<String> options) {
		final String value = get(name);
		if (options.contains(value))
			return value;
		throw error(name, value, "Value does not match any of the available options").detail("options", options);
	}

	public Set<String> getAnyOptions(String name, Set<String> options) {
		final Set<String> result = new HashSet<>();
		for (final String value : getAny(name)) {
			if (options.contains(value))
				result.add(value);
			else
				throw error(name, value, "Unepected value").detail("options", options);
		}
		return result;
	}

	/**
	 * Look for multiple boolean parameters and expect exactly one of them to be
	 * true. Return the true parameters name, or null if none of the parameters
	 * were true. Raise an error if more than one was found to be true.
	 */
	@Deprecated
	public String getExclusiveFlag(String... possibilities) {
		final Set<String> trueValues = getFlags(possibilities);
		if (trueValues.isEmpty())
			return null;
		final String found = trueValues.iterator().next();
		if (trueValues.size() == 1)
			return found;
		throw error(found, get(found), "Only one of these parameters should be set").detail("conflict", trueValues);
	}

	/**
	 * Look for multiple parameters and expect exactly one of them to be defined.
	 * Return the parameter name, or null if none of the parameters were found.
	 * Raise an error if more than one was found.
	 */
	public String getExclusiveParam(String... possibilities) {
		String found = null;
		for (final String p : possibilities) {
			if (getAny(p).length > 0) {
				if (found == null)
					found = p;
				else
					throw error(p, get(p), "Only one of these parameters should be set").detail("conflict",
						possibilities);
			}
		}
		return found;
	}

	/**
	 * For multiple boolean parameters, return all parameter names that are
	 * true.
	 */
	public Set<String> getFlags(String... possibilities) {
		final Set<String> trueValues = new HashSet<>(possibilities.length);
		for (final String p : possibilities) {
			if (getBoolean(p)) {
				trueValues.add(p);
			}
		}
		return trueValues;
	}

	public <T> T getMap(String name, Map<String, T> options) {
		final String value = get(name);
		final T match = options.get(value);
		if (match == null) {
			throw error(name, value, "Value does not match any of the available options").detail("options",
				options.keySet());
		}
		return match;
	}

	public <T> List<T> getAnyMap(String name, Map<String, T> options) {
		final String[] values = getAny(name);
		if (values.length == 0)
			return Collections.emptyList();
		final List<T> results = new ArrayList<>(values.length);
		for (final String val : values) {
			final T match = options.get(val);
			if (match == null)
				continue;
			results.add(match);
		}
		return results;
	}

	public void ensureNoUnusedParameters() {
		for (final String param : ctx.getQueryParamNames()) {
			if (!visted.contains(param)) {
				throw error(param, get(param), "Unused parameter");
			}
		}
	}

	/**
	 * Similar to {@link #getAnyOptions(String, Set)}, but allows single values
	 * to contain multiple comma separated options. Example: with=meta,info
	 */
	public Set<String> getCsvOptions(String name, Set<String> options) {
		final Set<String> result = new HashSet<>();
		for (final String v : getAnyCsv(name)) {
			if (options.contains(v))
				result.add(v);
			else
				throw error(name, v, "Unepected value").detail("options", options);
		}
		return result;
	}

	public List<String> getAnyCsv(String name) {
		final List<String> results = new ArrayList<>();
		for (final String val : getAny(name)) {
			for (final String v2 : val.split(",")) {
				results.add(v2);
			}
		}
		return results;
	}
}
