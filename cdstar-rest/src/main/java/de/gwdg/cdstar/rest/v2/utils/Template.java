package de.gwdg.cdstar.rest.v2.utils;

import java.util.HashMap;
import java.util.Map;

public class Template {

	Map<String, Object> namespace = new HashMap<>();
	String template;

	public Template(String name) {
		template = name;
	}

	public String getTemplate() {
		return template;
	}

	public Map<String, Object> getNamespace() {
		return namespace;
	}

	public void put(String key, Object value) {
		namespace.put(key, value);
	}

}
