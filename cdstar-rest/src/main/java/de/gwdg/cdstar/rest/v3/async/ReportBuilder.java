package de.gwdg.cdstar.rest.v3.async;

import java.util.HashMap;
import java.util.Map;

import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.runtime.client.CDStarAttribute;
import de.gwdg.cdstar.runtime.client.CDStarFile;
import de.gwdg.cdstar.web.common.model.ArchiveUpdated;

public class ReportBuilder {

	Map<String, CDStarFile> files = new HashMap<>();
	Map<String, String> fileNames = new HashMap<>();
	Map<String, Map<String, CDStarAttribute>> meta = new HashMap<>();

	public void rememberFile(CDStarFile file) {
		files.put(file.getID(), file);
		fileNames.putIfAbsent(file.getID(), file.getName());
	}

	public void addMeta(CDStarAttribute p, CDStarFile target) {
		meta.computeIfAbsent(target == null ? "" : target.getID(), key -> new HashMap<>()).put(p.getName(), p);
	}

	public ArchiveUpdated build(ArchiveUpdated au) {
		for (final CDStarFile fc : files.values()) {
			final String origName = fileNames.get(fc.getID());
			final String fileId = fc.getID();
			if (fc.getName() == null) {
				au.addChange(new ArchiveUpdated.FileChange(fileId, origName, null));
			} else {
				au.addChange(new ArchiveUpdated.FileChange(fileId, origName, ModelHelper.makeFileInfo(fc, false)));
			}
		}

		meta.forEach((target, attrs) -> {
			String ref = Utils.notNullOrEmpty(target) && files.containsKey(target) ? files.get(target).getName() : null;
			attrs.values().forEach(attr -> {
				au.addChange(new ArchiveUpdated.MetaChange(attr.getName(), attr.values(), ref));
			});
		});

		return au;
	}

}
