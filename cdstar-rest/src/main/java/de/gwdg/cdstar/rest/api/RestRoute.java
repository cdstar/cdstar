package de.gwdg.cdstar.rest.api;

import java.util.Map;

public interface RestRoute extends Comparable<RestRoute> {

	/**
	 * Routes are checked based on their order setting. Routes with a low value are
	 * checked first. Routes with identical values are checked in the order they
	 * were defined.
	 */
	RestRoute order(int order);

	int getOrder();

	String getRule();

	Map<String, RequestHandler> getTargets();

	RestRoute target(String method, RequestHandler target);

	default RestRoute HEAD(RequestHandler target) {
		return target("HEAD", target);
	}

	default RestRoute GET(RequestHandler target) {
		return target("GET", target);
	}

	default RestRoute POST(RequestHandler target) {
		return target("POST", target);
	}

	default RestRoute PUT(RequestHandler target) {
		return target("PUT", target);
	}

	default RestRoute DELETE(RequestHandler target) {
		return target("DELETE", target);
	}

	default RestRoute PATCH(RequestHandler target) {
		return target("PATCH", target);
	}

	@Override
	default int compareTo(RestRoute o) {
		return getOrder() - o.getOrder();
	}

}