package de.gwdg.cdstar.rest.v3.ingest;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream;
import org.apache.commons.compress.compressors.CompressorException;
import org.apache.commons.compress.compressors.CompressorStreamFactory;

import de.gwdg.cdstar.MimeUtils;
import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.web.common.model.ErrorResponse;

public class TarImporter implements Runnable {

	private static final Map<String, String> allowedContentEncodings = new HashMap<>();
	private static final String acceptEncodingHeader;
	static {
		allowedContentEncodings.put("gzip", "gz"); // Different name in
													// apache-compress
		allowedContentEncodings.put("deflate", "deflate");
		allowedContentEncodings.put("bzip2", "bzip2");
		allowedContentEncodings.put("lz4", "lz4");
		acceptEncodingHeader = Utils.join(", ", allowedContentEncodings.keySet());
	}

	public static boolean isTypeSupported(String contentType) {
		return "application/x-tar".equals(contentType) || "application/tar".equals(contentType);
	}

	private final ImportTarget target;
	private final Path source;
	private final String encodingHint;

	/**
	 *
	 * @param target
	 *               to import files to.
	 * @param source
	 *               to read from. The stream is closed after calling run().
	 */

	public TarImporter(ImportTarget target, Path source, String encodingHint) {
		this.target = target;
		this.source = source;
		this.encodingHint = Utils.nullOrEmpty(encodingHint) ? "identity" : encodingHint;
	}

	private InputStream decodeStream() throws IOException {
		final InputStream stream = Files.newInputStream(source, StandardOpenOption.READ);
		if (encodingHint.equals("identity"))
			return stream;

		final String algoName = allowedContentEncodings.get(encodingHint);
		if (algoName == null)
			throw new ErrorResponse(400, "UnsupportedEncoding",
				"Compression algorithm not supported for import.")
					.detail("encoding", encodingHint)
					.detail("allowed", acceptEncodingHeader);
		try {
			return new CompressorStreamFactory(true).createCompressorInputStream(algoName, stream);
		} catch (final CompressorException e) {
			throw Utils.wtf("Commons-compress does not support " + algoName + "?", e);
		}
	}

	@Override
	public void run() {
		TarArchiveInputStream tar = null;

		try {
			tar = new TarArchiveInputStream(decodeStream(), "UTF-8");

			TarArchiveEntry entry;
			while ((entry = tar.getNextTarEntry()) != null) {
				if (!entry.isFile())
					continue;

				final String name = entry.getName();
				final String type = MimeUtils.guess(name, false).getMimeDefault(MimeUtils.OCTET_STREAM);

				if (!target.newFile(name, type))
					continue;

				final ByteBuffer buffer = ByteBuffer.allocate(1024 * 8);
				int read;
				while (-1 < (read = tar.read(buffer.array(), buffer.arrayOffset(), buffer.capacity()))) {
					buffer.clear().limit(read);
					target.writeFile(buffer);
				}
			}
			target.close();
		} catch (final IOException e) {
			// TODO: File name encoding errors are currently silently ignored (chars replaced with ?)
			throw new ErrorResponse(400, "InvalidImportFormat", "Failed to extract archive: " + e.getMessage());
		} catch (final ErrorResponse e) {
			throw e;
		} catch (final Exception e) {
			throw new ErrorResponse(e);
		} finally {
			Utils.closeQuietly(tar);
		}

	}

}
