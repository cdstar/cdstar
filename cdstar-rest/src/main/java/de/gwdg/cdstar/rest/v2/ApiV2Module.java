package de.gwdg.cdstar.rest.v2;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;

import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.rest.api.Blueprint;
import de.gwdg.cdstar.rest.api.RestBlueprint;
import de.gwdg.cdstar.rest.api.RestConfig;
import de.gwdg.cdstar.rest.api.RestContext;
import de.gwdg.cdstar.rest.v2.model.ApiErrorResponse;
import de.gwdg.cdstar.rest.v2.utils.Template;
import de.gwdg.cdstar.runtime.Plugin;
import de.gwdg.cdstar.web.common.model.ErrorResponse;
import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapperBuilder;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModelException;

@Plugin(name = "cdstar-api-2")
@Blueprint(path = "/v2", name = "cdstar-api-2")
public class ApiV2Module implements RestBlueprint {

	public static String v2TypeKey = "cdstar2:type";
	public static String v2metaFile = ".cdstar2/metadata.json";

	private Configuration tpl;
	private final String baseUrl;

	public ApiV2Module(String baseUrl) {
		this.baseUrl = baseUrl;
	}

	@Override
	public void configure(RestConfig conf) {
		tpl = new Configuration(Configuration.VERSION_2_3_23);
		tpl.setObjectWrapper(new DefaultObjectWrapperBuilder(Configuration.VERSION_2_3_23).build());
		tpl.loadBuiltInEncodingMap();
		tpl.setDefaultEncoding("utf-8");
		tpl.setClassForTemplateLoading(getClass(), "/de/gwdg/cdstar/web/v2/templates");
		try {
			tpl.setSharedVariable("baseurl", baseUrl + "v2/");
		} catch (final TemplateModelException e) {
			throw Utils.wtf(e);
		}

		conf.install(new AccessControlResource());
		conf.install(new BitstreamResource());
		conf.install(new CollectionResource());
		conf.install(new LandingResource());
		conf.install(new MetadataResource());
		conf.install(new ObjectsResource());
		conf.install(new SearchResource());

		conf.mapError(ApiErrorResponse.class, (ctx, e) -> {
			ctx.status(e.getStatus());
			ctx.write(e);
		});

		conf.mapResponse("text/html", Template.class, this::mapToTemplate);
		conf.mapResponse("*;q=0.1", Template.class, this::mapToTemplate);

	}

	private void mapToTemplate(RestContext ctx, Template val) throws IOException {
		final ByteArrayOutputStream out = new ByteArrayOutputStream();
		try {
			tpl.getTemplate(val.getTemplate())
				.process(val.getNamespace(), new OutputStreamWriter(out, StandardCharsets.UTF_8));
		} catch (TemplateException | IOException e) {
			throw new ErrorResponse(e);
		}
		ctx.header("Content-Type", "text/html;charset=utf-8");
		ctx.write(out.toByteArray());
	}

}
