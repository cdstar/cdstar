package de.gwdg.cdstar.rest.v3;

import java.time.Duration;

import de.gwdg.cdstar.rest.api.RestBlueprint;
import de.gwdg.cdstar.rest.api.RestConfig;
import de.gwdg.cdstar.rest.api.RestContext;
import de.gwdg.cdstar.rest.utils.QueryHelper;
import de.gwdg.cdstar.rest.utils.SessionHelper;
import de.gwdg.cdstar.rest.v3.async.ModelHelper;
import de.gwdg.cdstar.runtime.CDStarRuntime;
import de.gwdg.cdstar.runtime.client.CDStarSession;
import de.gwdg.cdstar.runtime.client.exc.AccessError;
import de.gwdg.cdstar.ta.TransactionInfo.Mode;
import de.gwdg.cdstar.web.common.model.ErrorResponse;
import de.gwdg.cdstar.web.common.model.TransactionInfo;

public class TransactionEndpoint implements RestBlueprint {

	@Override
	public void configure(RestConfig cfg) {
		cfg.route("/_tx/<tx>").GET(this::handleGet).POST(this::handlePost).DELETE(this::handleDelete);
		cfg.route("/_tx").POST(this::newTransaction);
	}

	public TransactionInfo newTransaction(RestContext ctx) {
		final QueryHelper qh = new QueryHelper(ctx);
		qh.setDefault("isolation", "snapshot");
		qh.setDefault("timeout", "60");

		if (SessionHelper.getSubject(ctx).isAnonymous())
			throw new AccessError();

		final String isolation = qh.getOption("isolation", "snapshot", "full");
		final Duration timeout = Duration.ofSeconds(qh.getInt("timeout", 1, 60 * 10));
		final boolean readOnly = qh.getBoolean("readOnly");
		final Mode mode = isolation.equals("full") ? Mode.SERIALIZABILITY : Mode.SNAPSHOT;

		final CDStarSession session = SessionHelper.createDurableSession(ctx, readOnly, mode, timeout);

		ctx.status(201);
		ctx.header("Location", ctx.resolvePath("./" + session.getSessionId(), true));
		return ModelHelper.makeTransactionInfo(session);
	}

	public TransactionInfo handleGet(RestContext ctx) throws Exception {
		return ModelHelper.makeTransactionInfo(getTxOr404(ctx));
	}

	/**
	 * POST /v3/_tx/{txid}
	 *
	 * POST /v3/_tx/{txid}?renew
	 */
	public TransactionInfo handlePost(RestContext ctx) throws Exception {
		final CDStarSession session = getTxOr404(ctx);
		if (new QueryHelper(ctx).getBoolean("renew")) {
			session.refreshTimeout();
			return ModelHelper.makeTransactionInfo(session);
		} else {
			session.commit();
			return null;
		}
	}

	public Void handleDelete(RestContext ctx) throws Exception {
		CDStarSession tx = getTxOr404(ctx);
		ctx.runInPool(tx::rollback); // Close in background
		ctx.status(204);
		return null;
	}

	/**
	 * Get a specific session NOT USING the {@link SessionHelper}, as that would
	 * have unwanted side-effects (e.g. binding the session, refreshing or suspending
	 * session timeouts, ...)
	 */
	private CDStarSession getTxOr404(RestContext ctx) {
		final String txid = ctx.getPathParam("tx");
		if (txid == null || txid.isEmpty())
			throw new ErrorResponse(404, "TransactonNotFound", "No transaction specified");

		final CDStarSession session = ctx.getService(CDStarRuntime.class).resumeSession(txid);
		if (session == null || session.isExpired())
			throw new ErrorResponse(404, "TransactonNotFound", "Unknown or expired transaction").detail("transaction",
				txid);
		return session;
	}

}
