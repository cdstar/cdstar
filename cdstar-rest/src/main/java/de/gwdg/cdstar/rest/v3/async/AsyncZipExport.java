package de.gwdg.cdstar.rest.v3.async;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.attribute.FileTime;
import java.util.zip.Deflater;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import de.gwdg.cdstar.MimeUtils;
import de.gwdg.cdstar.runtime.client.CDStarFile;

/**
 * Export an archive as a ZIP files.
 */
public class AsyncZipExport extends AsyncExportBase {

	ZipOutputStream zipOutput;

	@Override
	protected void startExport(OutputStream output) {
		zipOutput = new ZipOutputStream(output, StandardCharsets.UTF_8);
	}

	@Override
	protected void finishExport() throws IOException {
		zipOutput.finish();
	}

	@Override
	protected void startFile(CDStarFile file) throws IOException {
		final ZipEntry ze = new ZipEntry(file.getName());
		ze.setSize(file.getSize());
		ze.setCreationTime(FileTime.from(file.getCreated().toInstant()));
		ze.setLastModifiedTime(FileTime.from(file.getLastModified().toInstant()));

		zipOutput.setMethod(ZipOutputStream.DEFLATED);
		if (MimeUtils.isUncompressed(file.getMediaType()) && file.getContentEncoding() == null)
			zipOutput.setLevel(Deflater.DEFAULT_COMPRESSION);
		else
			zipOutput.setLevel(0);

		zipOutput.putNextEntry(ze);
	}

	@Override
	protected void writeBytes(byte[] bytes, final int off, int len) throws IOException {
		zipOutput.write(bytes, 0, len);
	}

	@Override
	protected void closeFile() throws IOException {
		zipOutput.closeEntry(); // This flushes the deflater
	}

}
