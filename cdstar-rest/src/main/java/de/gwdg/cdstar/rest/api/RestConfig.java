package de.gwdg.cdstar.rest.api;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.List;

import de.gwdg.cdstar.Utils;

public interface RestConfig {

	/**
	 * Install a blueprint directly into this configuration, ignoring any
	 * {@link Blueprint} annotation.
	 */
	default void installDirect(RestBlueprint blueprint) {
		try {
			blueprint.configure(this);
		} catch (final RuntimeException e) {
			throw e;
		} catch (final Exception e) {
			throw new RuntimeException("Failed to install REST blueprint", e);
		}
	}

	/**
	 * Install a {@link RestBlueprint} into a new child-configuration with the
	 * settings defined by a {@link Blueprint} annotation. If no annotation is
	 * present, then the blueprint is installed into a sister-configuration (no
	 * prefix, inherit).
	 */
	default RestConfig install(RestBlueprint blueprint) {
		String prefix = "/";
		boolean inherit = true;
		String name = blueprint.getClass().getSimpleName();

		for (final Blueprint anno : blueprint.getClass().getAnnotationsByType(Blueprint.class)) {
			prefix = anno.path();
			if (Utils.notNullOrEmpty(anno.name()))
				name = anno.name();
			inherit = anno.inherit();
			break;
		}
		final RestConfig cc = createChild(prefix, inherit);
		cc.setName(name);
		cc.installDirect(blueprint);
		return cc;
	}

	void setName(String name);

	String getName();

	RestRoute route(String rule);

	/**
	 * Return the configured route prefix, which is either an empty string (no
	 * prefix) or a string starting with slash but not ending in '/'.
	 */
	String getPrefix();

	void setPrefix(String prefix);

	void setInterhit(boolean inherit);

	RestConfig getParent();

	boolean getInherit();

	List<RestConfig> getChildren();

	/**
	 * Create and return a nested {@link RestConfig}.
	 *
	 * @param Listen  to requests for this path (relativ e to the parent
	 *                configuration). Null or an empty string are translated to '/'.
	 * @param inherit If true, inherit response and error mappers as well as
	 *                services from the parent.
	 */
	RestConfig createChild(String preifx, boolean inherit);

	/**
	 * Register a response mapper for a specific mime- and java type.
	 *
	 * The correct mapper is selected as follows:
	 *
	 * For each mime-type in the clients Accept header and each mapper that can
	 * handle the given mime-type and java-type, find the match with the highest
	 * combined score. The score is calculated by multiplying the clients and the
	 * mappers `q=` parameter, which defaults to `1.0` and must be between 0
	 * (exclusive) and 1 (inclusive).
	 *
	 * If multiple candidates have the same score, the mapper with the shortest
	 * inheritance-distance to the actual java-type is preferred. If there are still
	 * multiple matches with the same distance, the one with the least amount of
	 * wildcards in its mime-type is preferred. As a last resort, mappers registered
	 * to the current config are preferred over mappers from parent configs.
	 *
	 * If no suitable mapper could be found, a 415 error is returned to the client.
	 *
	 * Tipps:
	 *
	 * If you have a mapper that can produce some format (e.g. json) out of
	 * virtually any object, map it to `Object.class` and a low quality score.
	 *
	 * If you want to make sure that ExampleType.class is always mapped to
	 * 'text/html' but there is a global mapper for `Object.class` and
	 * 'application/json', register am additional mapper for `ExampleType.class` and
	 * '*' which either returns html anyway or aborts the request with a 415 error
	 * response. This mapper will have the highest score possible (since '*'
	 * defaults to '*;q=1') whenever `ExampleType.class` or a subclass should be
	 * mapped. It will still loose against your regular `ExampleType.class`
	 * 'text/html' mapper for the same type because of the wildcard-rule.
	 */
	<T extends Object> void mapResponse(String contentType, Class<T> klass, ResponseMapper<T> mapper);

	/**
	 * Register a handler for a specific exception class, or subclasses of that
	 * exception.
	 *
	 * More specific handlers are always favored over broader exception types. For
	 * example, if you have a handler for {@link Exception} and a handler for
	 * {@link IOException}, and a {@link FileNotFoundException} is thrown, then the
	 * {@link IOException} handler will be invoked since it is closer to the actual
	 * exception.
	 */
	<T extends Throwable> void mapError(Class<T> klass, ErrorMapper<T> mapper);

	/**
	 * Like {@link #mapError(Class, ErrorMapper)}, but automatically detects the
	 * exception class through reflection. This does work nicely for
	 * {@link ErrorMapper} subclasses (including anonymous subclasses) and method
	 * references, but not for lambdas. Use {@link #mapError(Class, ErrorMapper)}
	 * with an explicit class parameter for lambda-based error mappers.
	 */
	@SuppressWarnings("unchecked")
	default <T extends Throwable> void mapError(ErrorMapper<T> mapper) {
		// Find the generic type T. This does work for real or anonymous
		// subclasses, but not for lambdas.
		for (final Method m : mapper.getClass().getMethods()) {
			if (ErrorMapper.class.isAssignableFrom(m.getDeclaringClass())) {
				if (!m.getName().equals("handle") || m.getParameterCount() != 2)
					continue;
				final Class<?> klass = (Class<?>) m.getGenericParameterTypes()[1];
				if (!Throwable.class.isAssignableFrom(klass))
					continue;
				mapError((Class<T>) klass, mapper);
				return;
			}
		}

		throw new IllegalArgumentException("Could not find mapped class through reflection. Probably a lambda?");
	}

	/**
	 * Return a registered service by class. This returns the first matching
	 * service, or null.
	 */
	<T> T lookup(Class<T> type);

	/**
	 * Return all registered service that implement a given class.
	 */
	<T> List<T> lookupAll(Class<T> type);

	void register(Object instance);

	List<RestRoute> getRoutes();

	/**
	 * Return the full path of this config, as seen from the root config.
	 */
	default String getPath() {
		final RestConfig parent = getParent();
		final String prefix = getPrefix();
		if (parent == null)
			return prefix.isEmpty() ? "/" : prefix;

		final String parentPath = parent.getPath();
		if (prefix.isEmpty())
			return parentPath;
		if (parentPath.equals("/"))
			return prefix;
		return parentPath + prefix;
	}

}
