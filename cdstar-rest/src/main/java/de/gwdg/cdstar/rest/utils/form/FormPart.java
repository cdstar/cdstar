package de.gwdg.cdstar.rest.utils.form;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;

/**
 * A single form part. My be text field or a file upload field.
 */
public interface FormPart {

	/**
	 * Return true if the part was fully received and will not be returned
	 * another time.
	 */
	boolean isComplete();

	/**
	 * Return the name of the form field. May be null or empty.
	 */
	String getName();

	/**
	 * Return content encoding header if present, or "identity".
	 */
	String getContentEncoding();

	/**
	 * Return the 'filename' option of the Content-Disposition header, if
	 * present. May return null.
	 */
	String getFileName();

	/**
	 * Return the value of the Content-type header, if present. May be null or
	 * empty.
	 */
	String getContentType();

	/**
	 * Write all buffered bytes to the given output stream.
	 *
	 * The stream is not flushed or closed. This operation may block if the
	 * {@link OutputStream} blocks. If the {@link OutputStream} throws an
	 * exception, some bytes may or may not have been written and are lost.
	 *
	 * @return Number of bytes copied.
	 */
	default long drainTo(OutputStream stream) throws IOException {
		long drained = 0;
		byte[] chunk;
		while ((chunk = drainChunk()) != null) {
			stream.write(chunk);
			drained += chunk.length;
		}
		return drained;
	}

	/**
	 * Get and remove a chunk of bytes from the in-memory buffer. If no data is
	 * available, the chunk may be empty.
	 */
	byte[] drainChunk();

	/**
	 * Return true if is buffered data that can be drained.
	 */
	boolean isBuffered();

	/**
	 * Return the number of bytes that can be drained from this part.
	 */
	int getBuffered();

	/**
	 * Return the total size of this part so far. This is only accurate if
	 * {@link #isComplete()} returns true.
	 */
	long getTotal();

	/**
	 * Return the total number of bytes drained from this part so far.
	 */
	long getDrained();

	/**
	 * Drain the entire buffer and return a {@link ByteBuffer};
	 */
	ByteBuffer drain();

	/**
	 * Drain the entire buffer into a string.
	 */
	String drainToString(Charset charset);

	/**
	 * Throw away the current part content.
	 */
	void clear();

}
