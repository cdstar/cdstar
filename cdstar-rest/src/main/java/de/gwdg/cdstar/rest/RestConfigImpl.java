package de.gwdg.cdstar.rest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.rest.api.ErrorMapper;
import de.gwdg.cdstar.rest.api.ResponseMapper;
import de.gwdg.cdstar.rest.api.RestConfig;
import de.gwdg.cdstar.rest.api.RestRoute;
import de.gwdg.cdstar.rest.utils.MimeType;

public class RestConfigImpl implements RestConfig {

	private static final Logger log = LoggerFactory.getLogger(RestConfigImpl.class);
	private static final Comparator<MimeType> BY_QUALITY_DESC = Comparator.<MimeType>comparingDouble(
		m -> m.getQuality()).reversed();

	// Configuration
	final List<RestRouteImpl> routes = new ArrayList<>();
	final List<Object> services = new ArrayList<>();
	final List<ResponseMapperHolder> responseMappers = new ArrayList<>();
	final List<ErrorMapperHolder> errorMappers = new ArrayList<>();

	// Caches
	private final Map<Class<? extends Object>, List<ResponseMapperHolder>> responseMapperCache = new ConcurrentHashMap<>();
	private final Map<Class<? extends Object>, List<Object>> lookupCache = new ConcurrentHashMap<>();
	private final Map<Class<? extends Throwable>, ErrorMapper<Throwable>> errorMapperCache = new ConcurrentHashMap<>();

	// Hierarchy
	private String prefix = "";
	private boolean inherit = true;
	private String name;
	private final RestConfigImpl parent;
	final List<RestConfigImpl> children = new ArrayList<>();

	public RestConfigImpl(RestConfigImpl parent) {
		this.parent = parent;
		if (parent == null)
			setName("ROOT");
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String getPrefix() {
		return prefix;
	}

	@Override
	public void setPrefix(String prefix) {
		if (Utils.nullOrEmpty(prefix))
			prefix = "/";
		if (!prefix.startsWith("/"))
			prefix = "/" + prefix;
		if (prefix.endsWith("/"))
			prefix = prefix.substring(0, prefix.length() - 1);
		this.prefix = prefix;
		reset();
	}

	@Override
	public boolean getInherit() {
		return inherit;
	}

	@Override
	public void setInterhit(boolean inherit) {
		this.inherit = inherit;
		reset();
	}

	@Override
	public RestConfig getParent() {
		return parent;
	}

	@Override
	public List<RestConfig> getChildren() {
		return Collections.unmodifiableList(children);
	}

	@Override
	public RestConfig createChild(String prefix, boolean inherit) {
		final RestConfigImpl child = new RestConfigImpl(this);
		child.setPrefix(prefix);
		child.setInterhit(inherit);
		children.add(child);
		reset();
		return child;
	}

	/**
	 * Mark this config as changed, which should also invalidate caches in its
	 * inheriting children.
	 */
	void reset() {
		errorMapperCache.clear();
		lookupCache.clear();
		responseMapperCache.clear();
		children.forEach(child -> {
			if (child.inherit)
				child.reset();
		});
	}

	@Override
	public RestRoute route(String rule) {
		for (final RestRouteImpl route : routes) {
			if (route.getRoute().equals(rule))
				return route;
		}
		final RestRouteImpl route = new RestRouteImpl(this, rule);
		routes.add(route);
		reset();
		return route;
	}

	/**
	 * Find the closest error mapper for a given exception and cache the result.
	 *
	 * Return {@link FallbackErrorMapper} if no matching mapper was found.
	 */
	ErrorMapper<Throwable> getErrorMapper(Class<? extends Throwable> t) {
		return errorMapperCache.computeIfAbsent(t, this::findErrorMapperInternal);
	}

	@SuppressWarnings("unchecked")
	private ErrorMapper<Throwable> findErrorMapperInternal(Class<? extends Throwable> klass) {
		log.debug("Searching for best error mapper for exception class {}", klass.getName());
		int bestDistance = Integer.MAX_VALUE;
		ErrorMapper<? extends Throwable> mapper = new FallbackErrorMapper();

		for (RestConfigImpl current = this; current != null; current = current.inherit ? current.parent : null) {
			for (final ErrorMapperHolder candidate : current.errorMappers) {
				if (!candidate.getMappedException().isAssignableFrom(klass))
					continue;
				final int dist = Utils.getClassDistance(candidate.getMappedException(), klass);
				if (log.isTraceEnabled())
					log.trace("Checking mapper for {} -> distance {}", candidate.getMappedException().getName(),
						dist);
				if (dist == bestDistance) {
					// This should be impossible as long as java does not start
					// supporting multiple inheritance.
					log.warn("Found two exception mappers for exception type {} with same distance {}: {} and {}",
						klass.getName(), dist, mapper, candidate.getMapper());
				}
				if (dist < bestDistance) {
					bestDistance = dist;
					mapper = candidate.getMapper();
				}
			}
		}
		return (ErrorMapper<Throwable>) mapper;
	}

	@Override
	public <T extends Throwable> void mapError(Class<T> klass, ErrorMapper<T> mapper) {
		log.debug("Mapping exception {} to handler: {}", klass, mapper);
		if (errorMappers.removeIf(m -> m.getMappedException().equals(klass))) {
			log.warn("Error mapper for type {} will replace existing mapper: {}", klass, mapper);
		}
		errorMappers.add(new ErrorMapperHolder(klass, mapper));
		reset();
	}

	@Override
	public <T> void mapResponse(String contentType, Class<T> klass, ResponseMapper<T> mapper) {
		log.debug("Mapping response {} ({}) to handler: {}", klass, contentType, mapper);
		responseMappers.add(new ResponseMapperHolder(contentType, klass, mapper));
		reset();
	}

	/**
	 * Get all {@link ResponseMapper}s suitable for a given class, ordered by their
	 * relevance (see {@link ResponseMapperComparator}).
	 */
	List<ResponseMapperHolder> getResponseMappers(Class<? extends Object> type) {
		return responseMapperCache.computeIfAbsent(type, this::findResponseMappers);
	}

	private List<ResponseMapperHolder> findResponseMappers(Class<? extends Object> type) {
		final List<ResponseMapperHolder> all = new ArrayList<>();
		for (RestConfigImpl current = this; current != null; current = current.inherit ? current.parent : null) {
			for (final ResponseMapperHolder m : current.responseMappers) {
				if (m.getMappedType().isAssignableFrom(type))
					all.add(m);
			}
		}
		all.sort(new ResponseMapperComparator(type));
		return all;
	}

	/**
	 * Return the best {@link ResponseMapper} for a given type and the clients
	 * 'Accept' header, following the rules defined in
	 * {@link RestConfig#mapResponse(String, Class, ResponseMapper)}.
	 */
	ResponseMapper<Object> findBestResponseMapperFor(String acceptHeader, Class<?> type) {
		// Find a list of mappers that support the given type. We do NOT cache
		// depending on the clients accept header, because this would allow
		// clients to inflate the cache by supplying random mime-types. The
		// returned list is ordered, to allow early-exists in the inner loop.

		final List<ResponseMapperHolder> mappers = getResponseMappers(type);

		// No mapper for that type :(
		if (mappers.isEmpty())
			return null;

		final List<MimeType> clientAccepts;

		// Client does not care, so just return the best mapper we have.
		if (Utils.nullOrEmpty(acceptHeader))
			return mappers.get(0).getMapper();

		// Client actually does care. Order by preference.
		clientAccepts = MimeType.parseAcceptHeader(acceptHeader);
		if (clientAccepts.size() > 1)
			clientAccepts.sort(BY_QUALITY_DESC);

		ResponseMapperHolder bestMatch = null;
		float bestQuality = 0;

		mapperloop: for (final ResponseMapperHolder mapper : mappers) {
			final float mapperQuality = mapper.getMime().getQuality();

			// Mappers are ordered by quality (desc), so there cannot be any better match.
			if (bestQuality >= mapperQuality)
				break;

			for (final MimeType accept : clientAccepts) {
				final float combinedQuality = accept.getQuality() * mapperQuality;

				// Accept headers are ordered by quality, so any remaining header will be worse
				// for the current mapper. The next mapper may still match a better accept
				// header and beat the score, so only break out of the inner loop.
				if (bestQuality >= combinedQuality)
					break;

				// A better match was found
				if (accept.accepts(mapper.getMime())) {
					bestQuality = combinedQuality;
					bestMatch = mapper;

					// Perfect match. No need to continue search.
					if (combinedQuality >= 1.0)
						break mapperloop;

					// Accept headers are ordered by quality, so any remaining header will be worse
					// for the current mapper.
					break;
				}
			}
		}

		if (bestMatch != null) {
			if (log.isDebugEnabled())
				log.debug("Content negotion for [{}]: Best match {} -> {} (score: {})", acceptHeader,
					bestMatch.getMime(), bestMatch.getMapper(), bestQuality);
			return bestMatch.getMapper();
		}

		if (log.isDebugEnabled())
			log.warn("Content negotion for [{}]: No match", acceptHeader);

		// Not a single match :(
		return null;
	}

	@Override
	public void register(Object instance) {
		if (instance == null)
			return;
		services.add(instance);
		reset();
	}

	@Override
	@SuppressWarnings("unchecked")
	public <T> List<T> lookupAll(Class<T> type) {
		Utils.notNull(type);
		return (List<T>) lookupCache.computeIfAbsent(type, t -> {
			final List<Object> all = new ArrayList<>(1);
			for (RestConfigImpl current = this; current != null; current = current.inherit ? current.parent : null) {
				for (final Object service : current.services) {
					if (type.isAssignableFrom(service.getClass()))
						all.add(service);
				}
			}
			return all;
		});
	}

	@Override
	public <T> T lookup(Class<T> type) {
		Utils.notNull(type);
		final List<T> result = lookupAll(type);
		if (result.isEmpty())
			return null;
		return result.get(0);
	}

	@Override
	public List<RestRoute> getRoutes() {
		return Collections.unmodifiableList(routes);
	}

}
