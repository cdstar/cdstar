package de.gwdg.cdstar.rest.utils;

import java.util.Collection;
import java.util.function.Predicate;

/**
 * A compound predicate that returns true if at least one include predicate, but
 * no exclude predicate returns true. In absence of include predicates (empty
 * list or null), a single include predicate that always returns true is
 * assumed.
 *
 * In other words: If an exclude predicate matches, return false. If an include
 * predicate matches, return true. If there were no include predicates, return
 * true. Otherwise, return false.
 */
public class IncludeExcludeFilter<T> implements Predicate<T> {

	private final Collection<? extends Predicate<T>> include;
	private final Collection<? extends Predicate<T>> exclude;

	public IncludeExcludeFilter(Collection<? extends Predicate<T>> include,
		Collection<? extends Predicate<T>> exclude) {
		this.include = include;
		this.exclude = exclude;
	}

	@Override
	public boolean test(T test) {
		if (exclude != null)
			for (final Predicate<T> e : exclude)
				if (e.test(test))
					return false;
		if (include != null)
			for (final Predicate<T> e : include)
				if (e.test(test))
					return true;
		return include == null || include.isEmpty();
	}

}
