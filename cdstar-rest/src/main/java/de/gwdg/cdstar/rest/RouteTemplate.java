package de.gwdg.cdstar.rest;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class RouteTemplate {
	static Pattern syntax = Pattern
		.compile("<([a-zA-Z_][a-zA-Z_0-9]*)?(?::([a-zA-Z_]*)(?::((?:\\.|[^\\>]+)+)?)?)?>");
	private final Set<String> names = new HashSet<>();

	private final Pattern pattern; // null for static rules
	private final String prefix;
	final String template;
	private final boolean stripSlash;

	RouteTemplate(String template, boolean stripSlash) {
		this.stripSlash = stripSlash;
		if (template == null || template.isEmpty())
			template = "/";
		if (stripSlash && template.endsWith("/"))
			template = template.substring(0, template.length() - 1);

		this.template = template;
		final StringBuilder patternBuilder = new StringBuilder();
		patternBuilder.append("^");

		final Matcher m = syntax.matcher(template);
		int lastFind = 0;
		String staticPrefix = null;
		while (m.find()) {
			if (m.start() > lastFind) {
				final String staticPart = template.substring(lastFind, m.start());
				if (staticPrefix == null)
					staticPrefix = staticPart;
				else
					patternBuilder.append(Pattern.quote(staticPart));
			}
			final String name = m.group(1);
			String filter = m.group(2);
			String conf = m.group(3);
			if (filter == null)
				filter = "re";
			if (filter.equals("re")) {
				conf = conf == null ? "[^/]+" : conf;
			} else if (filter.equals("path")) {
				conf = conf == null ? ".+" : conf;
			} else {
				throw new IllegalArgumentException("Not a valid route filter: " + filter);
			}

			patternBuilder.append("(?<" + name + ">(?>" + conf + "))");
			names.add(name);
			lastFind = m.end();
		}
		if (template.length() > lastFind)
			patternBuilder.append(Pattern.quote(template.substring(lastFind)));
		if (this.stripSlash)
			patternBuilder.append("/?");
		patternBuilder.append("$");

		// Compile pattern, but only if there is any dynamic part
		if (lastFind > 0) {
			pattern = Pattern.compile(patternBuilder.toString());
			prefix = staticPrefix;
		} else {
			pattern = null;
			prefix = template;
		}
	}

	/**
	 * Match a given path and return a map of path parameters, or null if not
	 * matched.
	 */
	public Map<String, String> match(String path) {
		if (pattern == null)
			return matchStatic(path);

		final Matcher m;
		if (prefix != null) {
			if (!path.startsWith(prefix))
				return null;
			m = pattern.matcher(path.substring(prefix.length()));
		} else {
			m = pattern.matcher(path);
		}
		if (!m.matches())
			return null;

		final Map<String, String> params = new HashMap<>(names.size());
		for (final String name : names) {
			params.put(name, m.group(name));
		}
		return params;
	}

	private Map<String, String> matchStatic(String path) {
		if (template.equals(path))
			return Collections.emptyMap();
		if (stripSlash && path.length() == template.length() + 1 && path.startsWith(template) && path.endsWith("/"))
			return Collections.emptyMap();
		return null;
	}

	@Override
	public String toString() {
		return template;
	}
}