package de.gwdg.cdstar.rest.v2.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonAutoDetect(getterVisibility = Visibility.NONE, isGetterVisibility = Visibility.NONE)
public class ApiErrorResponse extends RuntimeException {
	private static final long serialVersionUID = 2772614386994025138L;

	int status;
	String error;
	String reason;

	public ApiErrorResponse(final int status, final String err, final String msg) {
		setStatus(status);
		setError(err);
		setReason(msg);
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	@JsonProperty("error")
	public String getError() {
		return error;
	}

	public void setError(final String error) {
		this.error = error;
	}

	@JsonProperty("reason")
	public String getReason() {
		return reason;
	}

	public void setReason(final String reason) {
		this.reason = reason;
	}

}
