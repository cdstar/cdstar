package de.gwdg.cdstar.rest.v3.async;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayDeque;
import java.util.Collection;
import java.util.Deque;
import java.util.zip.Deflater;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import de.gwdg.cdstar.ByteBufferOutputStream;
import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.rest.api.AsyncContext;
import de.gwdg.cdstar.runtime.client.CDStarFile;

/**
 * Base class for archive exporters. This approach is designed to work well with
 * {@link ZipOutputStream} style APIs that write to a {@link OutputStream} and
 * are blocking by nature.
 *
 * This implementation uses a memory-buffered {@link OutputStream} instead of a
 * temporary file to reduce IO and latency.
 *
 * TODO: Support ETag, If-*-Match headers, and stuff.
 *
 * TODO: Support Range headers? Might be dangerous (easy to craft very small but
 * expensive requests)
 *
 * TODO: Compress the next chunk while the current chunk is sent to the client?
 * Do IO and compression in a limited thread pool, instead of the unbounded web
 * server pool?
 *
 * TODO: Limit the total number of concurrent exports to avoid OOM. Large ZIP
 * files, for example, require a significant amount of memory per
 * {@link ZipEntry}. It is better to serve a few clients fast, than a lot of
 * clients slowly and let them hog memory.
 */
public abstract class AsyncExportBase {

	private AsyncContext ac;
	protected final Deque<CDStarFile> filesToExport = new ArrayDeque<>();
	private CDStarFile currentFile;
	private InputStream currentStream;
	private byte[] readBuffer;
	protected int lowWatermark = 1024 * 8;
	private long bytesWritten;
	ByteBufferOutputStream outputBuffer;

	/**
	 * Add files to export. Files are exported in-order. Duplicates are not removed.
	 */
	public final void addFiles(Collection<CDStarFile> files) {
		filesToExport.addAll(files);
	}

	/**
	 * Set the low watermark for the send-buffer (default = 8KB). If less than this
	 * amount of bytes are available to be sent to the client, more bytes are
	 * collected first. A large send-buffer improves throughput, but also increases
	 * memory usage.
	 *
	 * The high watermark (maximum amount of buffered bytes) is two times the low
	 * watermark. In parallel mode, two buffers may exists, so the total number of
	 * buffered bytes is four times the watermark, plus the size of the read buffer.
	 * Note that the actual exporter implementation may have internal buffers, too
	 * (e.g. block size of a {@link Deflater}).
	 */
	public final void setWatermark(int bytes) {
		Utils.assertTrue(ac == null, "Already started");
		lowWatermark = Math.max(bytes, 1);
	}

	public final void dispatch(AsyncContext ctx) {
		Utils.assertTrue(ac == null, "Already started");
		ac = ctx;

		readBuffer = new byte[lowWatermark];
		outputBuffer = new ByteBufferOutputStream(lowWatermark);

		try {
			startExport(outputBuffer);
			writeChunk();
		} catch (final Exception e) {
			onError(e);
		}
	}

	/**
	 * This is called at the beginning of an export. Whatever is written to the
	 * {@link OutputStream} is sent to the client. The given stream is
	 * memory-buffered and non-blocking.
	 */
	protected abstract void startExport(OutputStream output);

	/**
	 * Called once for each {@link CDStarFile} to export.
	 */
	protected abstract void startFile(CDStarFile file) throws IOException;

	/**
	 * Called once for each chunk of data in a file. Not called for empty files.
	 */
	protected abstract void writeBytes(byte[] bytes, final int off, int len) throws IOException;

	/**
	 * Called once after the last chunk of data was written to a file.
	 */
	protected abstract void closeFile() throws IOException;

	/**
	 * Called after the last file was exported. This should flush any buffers and
	 * finish writing to the supplied {@link OutputStream}.
	 *
	 * To clean up after errors, implementations may also overwrite {@link #close()}
	 */
	protected abstract void finishExport() throws IOException;

	/**
	 * Compress chunk after chunk, file after file, until the output buffer contains
	 * more than minWatermark bytes, or there is nothing left to compress.
	 *
	 * This may block on Disk IO (reading from the archive vile) or CPU
	 * (compression).
	 *
	 * This method is synchronized to prevent stale memory reads.
	 *
	 * @return the total size of the write buffer
	 */
	private synchronized final int getMoreBytes() throws IOException {

		while (outputBuffer.getBufferSize() < lowWatermark) {

			if (currentStream == null) {

				if (filesToExport.isEmpty()) {
					finishExport();
					break;
				}

				currentFile = filesToExport.pop();
				currentStream = currentFile.getStream();

				startFile(currentFile);
			}

			final int bytes = currentStream.read(readBuffer);
			if (bytes > 0) {
				writeBytes(readBuffer, 0, bytes);
			} else {
				// End of file.
				Utils.closeQuietly(currentStream);
				currentStream = null;
				closeFile();
				continue;
			}
		}

		return outputBuffer.getBufferSize();
	}

	private synchronized final void writeChunk() throws Exception {
		if (getMoreBytes() > 0) {
			ac.asyncWrite(outputBuffer.popBytes(), buffer -> {
				bytesWritten += buffer.position();
				writeChunk();
			}, this::onError);
		} else {
			// Done.
			Utils.closeQuietly(ac);
		}
	}

	private synchronized final void onError(Throwable e) {
		if (bytesWritten > 0) {
			// No way to signal the client that the download failed, so we just
			// close the connection and hope that all bytes were already transmitted.
			Utils.closeQuietly(ac);
		} else {
			ac.getRequest().abort(e);
		}
	}

}