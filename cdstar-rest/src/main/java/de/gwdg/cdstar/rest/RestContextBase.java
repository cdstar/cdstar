package de.gwdg.cdstar.rest;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import de.gwdg.cdstar.FastUniquePNRG;
import de.gwdg.cdstar.NamedThreadFactory;
import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.rest.api.ErrorMapper;
import de.gwdg.cdstar.rest.api.HttpError;
import de.gwdg.cdstar.rest.api.ResponseMapper;
import de.gwdg.cdstar.rest.api.RestConfig;
import de.gwdg.cdstar.rest.api.RestContext;
import de.gwdg.cdstar.rest.utils.RestUtils;

/**
 * Back-end independent base class for all {@link RestContext} implementations.
 */

public abstract class RestContextBase implements RestContext {
	private static final String[] emptyStringArray = new String[] {};

	public static final Logger log = LoggerFactory.getLogger(RestContext.class);
	private static final FastUniquePNRG requestIdSource = new FastUniquePNRG();
	private static final boolean mdcAware = MDC.getMDCAdapter() != null;
	private final List<Runnable> cleanupCallables = new ArrayList<>();

	private boolean closed;
	private Map<String, Object> attributes;

	// TODO: Add metric for the size of this queue
	// TODO: Add a way to shutdown this thread pool
	private static final ThreadPoolExecutor pool;

	static {
		final int tCount = Math.max(4, Runtime.getRuntime().availableProcessors() * 2);
		pool = new ThreadPoolExecutor(tCount, tCount, 60, TimeUnit.SECONDS,
			new LinkedBlockingQueue<>(), new NamedThreadFactory("rest"));
		pool.allowCoreThreadTimeOut(true);
	}

	long requestId = requestIdSource.next();
	private Map<String, List<String>> query;

	public RestContextBase() {
		if (mdcAware)
			MDC.put("request.id", String.valueOf(requestId));
	}

	@Override
	public RestConfig getConfig() {
		return getAttribute(RestContext.ATTR_MATCH_CONF, RestConfig.class);
	}

	private RestConfigImpl getConfigInternal() {
		return (RestConfigImpl) getConfig();
	}

	@Override
	public <T> T getService(Class<T> t) {
		return getConfig().lookup(t);
	}

	@Override
	public boolean hasEntity() {
		return getContentLength() != 0;
	}

	synchronized Map<String, List<String>> getQueryMap() {
		if (query != null)
			return query;
		final String q = getQuery();
		query = q != null ? RestUtils.splitQuery(q) : Collections.emptyMap();
		return query;
	}

	@Override
	public String[] getQueryParams(String name) {
		final List<String> values = getQueryMap().get(name);
		return values == null ? emptyStringArray : values.toArray(emptyStringArray);
	}

	@Override
	public Set<String> getQueryParamNames() {
		return new HashSet<>(getQueryMap().keySet());
	}

	@Override
	public String getPathParam(String name) {
		return (String) getAttribute(RestContext.ATTR_MATCH_PARAMS, Map.class).get(name);
	}

	@Override
	public Future<?> runInPool(Runnable runnable) {
		if (!mdcAware) {
			return pool.submit(runnable);
		} else {
			final Map<String, String> mdc = MDC.getCopyOfContextMap();
			return pool.submit(() -> {
				if (Utils.notNullOrEmpty(mdc))
					MDC.setContextMap(mdc);
				runnable.run();
				MDC.clear();
			});
		}
	}

	@Override
	public void runAfterRequest(Runnable runnable) {
		cleanupCallables.add(runnable);
	}

	@Override
	public void abort(Throwable err) {

		if (isCommitted()) {
			log.error("Cannot write error to committed or closed response.", new IllegalStateException(err));
			close();
			return;
		}

		ErrorMapper<Throwable> mapper = getConfigInternal().getErrorMapper(err.getClass());

		try {
			mapper.handle(this, err);
			return;
		} catch (final Exception e) {
			log.error("Error while handling error.", e);
			status(500);
			return;
		} finally {
			close();
		}
	}

	@Override
	public void close() {
		if (isClosed())
			return;

		closed = true;

		while (!cleanupCallables.isEmpty()) {
			runInPool(cleanupCallables.remove(0));
		}
	}

	@Override
	public boolean isClosed() {
		return closed;
	}

	@Override
	public void write(Object entity) throws IOException {
		// Upcast to a more suitable write method, if present.
		if (entity instanceof InputStream) {
			write((InputStream) entity);
		} else if (entity instanceof String) {
			write((String) entity);
		} else if (entity instanceof ByteBuffer) {
			write((ByteBuffer) entity);
		} else if (entity instanceof byte[]) {
			write((byte[]) entity);
		} else {
			// Actually search for a mapper
			final ResponseMapper<Object> mapper = getConfigInternal()
				.findBestResponseMapperFor(getHeader("Accept"), entity.getClass());
			if (mapper == null)
				throw new HttpError.NotAcceptable();
			try {
				mapper.handle(this, entity);
			} catch (final Exception e) {
				if (e instanceof IOException)
					throw (IOException) e;
				throw new IOException("Response mapper failed", e);
			}
		}
	}

	@Override
	public InputStream getInputStream() {
		return new RestInputStream(this);
	}

	public static class RestInputStream extends InputStream {

		private final RestContext ctx;

		public RestInputStream(RestContext ctx) {
			this.ctx = ctx;
		}

		@Override
		public int read() throws IOException {
			final byte[] b = new byte[1];
			if (read(b) > 0)
				return b[0];
			else
				return -1;
		}

		@Override
		public int read(byte[] b) throws IOException {
			return read(b, 0, b.length);
		}

		@Override
		public int read(byte[] b, int off, int len) throws IOException {
			return ctx.read(b, off, len);
		}

	}

	@Override
	public <T> T getAttribute(String name, Class<T> type) {
		if (attributes == null)
			return null;
		return type.cast(attributes.get(name));
	}

	@Override
	public void setAttribute(String name, Object value) {
		if (attributes == null)
			attributes = new HashMap<>();
		attributes.put(name, value);
	}

	@Override
	public long getRequestId() {
		return requestId;
	}

	@Override
	public String toString() {
		return "[" + getMethod() + " " + Utils.repr(getPath()) + " #" + Long.toHexString(requestId) + "]";
	}

}
