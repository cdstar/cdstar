package de.gwdg.cdstar.rest.api;

import java.io.Closeable;
import java.nio.ByteBuffer;
import java.time.Duration;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeoutException;
import java.util.function.Consumer;

import de.gwdg.cdstar.FailableConsumer;

/**
 * Non-blocking IO handling without back-pressure or hidden unbounded buffers.
 *
 * The standard Servlet API for asynchronous IO is pretty complex and hard to
 * get right. This is an attempt to reduce the number of rules and states one
 * has to keep in mind and prevent the most common errors, while keeping all the
 * nice properties of asynchronous IO.
 *
 * Goals:
 *
 * Build-in ByteBuffer pool to recycle unused buffers and reduce allocation
 * overhead. The recycling happens explicitly and is totally optional. If you
 * are unsure if you still need that buffer, keep it as long as you want. You
 * own it.
 *
 * Events are handled via lambda-friendly callbacks or
 * {@link CompletableFuture}s. No anonymous listener subclasses needed.
 *
 * Event callbacks are executed in a thread pool so they do not block the web
 * server IO thread. Since this pool is fed via a queue, and each Servlet
 * request has at most one pending IO requests per type (read or write) in this
 * queue, you get basic traffic shaping (round-robin handling of requests) for
 * free.
 *
 * As mentioned already, you cannot issue a read or write request while there is
 * still a pending request of the same type. This prevents most race condition
 * or out-of-memory bugs and makes reasoning about process flow a lot easier.
 *
 * Each read/write request will result in the callback to be called exactly
 * once. There is no back-pressure (the server pushing data faster than you can
 * handle).
 */

public interface AsyncContext extends Closeable {

	/**
	 * {@link AsyncContext}s may be closed for various reasons, explicitly or
	 * implicitly due to timeouts or server shutdown events. Installing a listener
	 * may help cleaning up resources or abort expensive operations if the client
	 * disconnects.
	 *
	 * @throws IllegalStateException if the context is already closed.
	 */
	void addCloseListener(AsyncCloseListener listener);

	/**
	 * @return Size (capacity) of buffers allocated by this context or acceptable
	 *         for recycling (see {@link #recycleBuffer(ByteBuffer)}).
	 */
	int getBufferSize();

	/**
	 * Donate a buffer to be used for future read requests. The buffer must have an
	 * array associated with it, an arrayOffset of 0 and a capacity of exactly
	 * {@link #getBufferSize()} to be considered.
	 *
	 * Recycled buffers are cleared but not zeroed out. Buffers that contain
	 * sensitive information or may still have references to it after recycling,
	 * should not be recycled.
	 *
	 * You should always recycle your read- or write buffers after use, if possible.
	 * If the recycle pool runs dry, new buffers are allocated.
	 *
	 * @param buf Buffer to recycle.
	 *
	 * @return true if the buffer was accepted for recycling, false otherwise.
	 */
	boolean recycleBuffer(ByteBuffer buf);

	/**
	 * Return a buffer from the recycle pool or allocate a new one. The returned
	 * buffer will have an array associated with it, an arrayOffset of 0 and a
	 * capacity of {@link #getBufferSize()}.
	 *
	 * Recycled buffers are cleared but not zeroed out. They may contain stale data
	 * from a previous request. If used as a write buffer, make sure that no stale
	 * data remains between 'position' and 'limit'.
	 *
	 * If you need a buffer larger or significantly smaller than
	 * {@link #getBufferSize()}, or if you already have a byte array, it is usually
	 * faster to allocate a new buffer or using {@link ByteBuffer#wrap(byte[])}.
	 *
	 */
	ByteBuffer getBuffer();

	/**
	 * Write the content of the given buffer to the stream. The callback is called
	 * in a managed thread-pool after all bytes were successfully written to the
	 * socket or copied to a (bounded) intermediate buffer, or an error occurred. In
	 * any case, the write buffer is no longer needed and can be recycled or re-used
	 * for the next write request.
	 *
	 * If the task takes longer than a given {@code timeout}, it is aborted with a
	 * {@link TimeoutException}. Null or negative durations will not disable the
	 * timeout, but set it to a sensible default value.
	 *
	 * @throws IllegalStateException if there is a pending write request.
	 */
	void asyncWrite(ByteBuffer buffer, AsyncResultCallback callback, Duration timeout);

	/**
	 * @see AsyncContext#asyncWrite(ByteBuffer, AsyncResultCallback, Duration)
	 */
	default void asyncWrite(ByteBuffer buffer, AsyncResultCallback callback) {
		asyncWrite(buffer, callback, null);
	}

	/**
	 * Same as {@link #asyncWrite(ByteBuffer, AsyncResultCallback, Duration)} but
	 * accepts two separate callbacks for success and error. If the success callback
	 * throws an exception, the error callback is also called.
	 */
	default void asyncWrite(ByteBuffer buffer, FailableConsumer<ByteBuffer> onSuccess,
			Consumer<Throwable> onError, Duration timeout) {
		asyncWrite(buffer, (a, buff, error) -> {
			if (error == null)
				onSuccess.accept(buffer);
			else
				onError.accept(error);
		}, timeout);
	}

	/**
	 * @see AsyncContext#asyncWrite(ByteBuffer, FailableConsumer, Consumer, Duration)
	 */
	default void asyncWrite(ByteBuffer buffer, FailableConsumer<ByteBuffer> onSuccess,
			Consumer<Throwable> onError) {
		asyncWrite(buffer, onSuccess, onError, null);
	}

	/**
	 * Read data from the request entity into a {@link ByteBuffer} until the buffer
	 * is either full or the end of stream is reached. The callback is then called
	 * in a managed thread-pool with a flipped buffer, ready for reading.
	 *
	 * Read buffers received from {@link #getBuffer()} should be recycled via
	 * {@link #recycleBuffer(ByteBuffer)} after use, or re-used for the next read
	 * operation.
	 *
	 * The end of stream can be detected by a non-full (limit < capacity) read
	 * buffer, or by checking {@link #endOfStream()}. Issuing read requests after
	 * {@link #endOfStream()} returns true, will raise an
	 * {@link IllegalStateException}.
	 *
	 * If the task takes longer than a given {@code timeout}, it is aborted with a
	 * {@link TimeoutException}. Null or negative durations will not disable the
	 * timeout, but set it to a sensible default value.
	 *
	 * @throws IllegalStateException if there is a pending read request, if the
	 *                               stream is already closed, or if
	 *                               {@link #endOfStream()} returns true.
	 */
	void asyncRead(ByteBuffer buffer, AsyncResultCallback callback, Duration timeout);

	/**
	 * @see AsyncContext#asyncRead(ByteBuffer, AsyncResultCallback, Duration)
	 */
	default void asyncRead(ByteBuffer buffer, AsyncResultCallback callback) {
		asyncRead(buffer, callback, null);
	}

	/**
	 * Same as {@link #asyncRead(ByteBuffer buffer, AsyncResultCallback)} but
	 * accepts two separate callbacks for success/error. If the success callback
	 * throws an exception, the error callback is also called.
	 */
	default void asyncRead(ByteBuffer buffer, final FailableConsumer<ByteBuffer> onSuccess,
			final Consumer<Throwable> onError, Duration timeout) {
		asyncRead(buffer, (a, b, error) -> {
			if (error == null)
				onSuccess.accept(b);
			else
				onError.accept(error);
		}, timeout);
	}

	/**
	 * @see AsyncContext#asyncRead(ByteBuffer, FailableConsumer, Consumer, Duration)
	 */
	default void asyncRead(ByteBuffer buffer, FailableConsumer<ByteBuffer> onSuccess,
			Consumer<Throwable> onError) {
		asyncRead(buffer, onSuccess, onError, null);
	}

	/**
	 * Return true if the last read request reached the end of stream, and the next
	 * read request will fail.
	 */
	boolean endOfStream();

	RestContext getRequest();

	/**
	 * Silently consume the entire request.
	 */
	CompletableFuture<Void> consumeRequest();

	/**
	 * Reset the last activity time for this request to prevent timeouts. The
	 * default idle timeout is very generous (30 Minutes) so this should not be
	 * necessary most of the time.
	 */
	void keepAlive();

	/**
	 * Calls {@link RestContext#close()}.
	 */
	@Override
	default void close() {
		getRequest().close();
	}

}