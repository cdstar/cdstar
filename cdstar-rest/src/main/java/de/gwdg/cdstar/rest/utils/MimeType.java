package de.gwdg.cdstar.rest.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.gwdg.cdstar.Utils;

public class MimeType {
	public static MimeType ANY = new MimeType("*", "*");
	public static Map<String, MimeType> cache = new HashMap<>();

	public String major;
	public String minor;
	public float q = 1;

	private MimeType(String value) {
		// text/plain; q=0.5
		// TODO: This parser breaks if quoted strings are involved, but that
		// is usually not the case for accept headers.
		final String[] vp = value.split(";");
		final String[] mp = vp[0].trim().split("/", 2);
		major = mp[0];
		minor = mp.length == 2 ? mp[1] : "*";
		q = 1;
		for (int i = 1; i < vp.length; i++) {
			final String[] kv = vp[i].split("=", 2);
			if (kv[0].trim().equals("q") && kv.length == 2) {
				q = Utils.gate(0, Float.parseFloat(kv[1]), 1);
			}
		}
	}

	private MimeType(String minor, String major) {
		this.minor = minor;
		this.major = major;
	}

	/**
	 * @return number of wildcards in mime-type.
	 */
	public int getWildcardCount() {
		if (!minor.equals("*"))
			return 0;
		if (!major.equals("*"))
			return 1;
		return 2;
	}

	public String getPrimary() {
		return major;
	}

	public String getSecondary() {
		return minor;
	}

	public float getQuality() {
		return q;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder();
		sb.append(major);
		sb.append("/");
		sb.append(minor);
		if (q != 1) {
			sb.append("; q=");
			sb.append(Float.toString(q));
		}
		return sb.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((major == null) ? 0 : major.hashCode());
		result = prime * result + ((minor == null) ? 0 : minor.hashCode());
		result = prime * result + Float.floatToIntBits(q);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof MimeType))
			return false;
		final MimeType other = (MimeType) obj;
		if (major == null) {
			if (other.major != null)
				return false;
		} else if (!major.equals(other.major))
			return false;
		if (minor == null) {
			if (other.minor != null)
				return false;
		} else if (!minor.equals(other.minor))
			return false;
		if (Float.floatToIntBits(q) != Float.floatToIntBits(other.q))
			return false;
		return true;
	}

	public boolean accepts(MimeType other) {
		if (major.equals(other.major)) {
			if (minor.equals(other.minor))
				return true;
			if (minor.equals("*") || other.minor.equals("*"))
				return true;
		}
		if (major.equals("*") || other.major.equals("*"))
			return true;
		return false;
	}

	public synchronized static MimeType getCached(String mimeType) {
		return cache.computeIfAbsent(mimeType, v -> new MimeType(mimeType));
	}

	public synchronized static MimeType get(String mimeType) {
		final MimeType result = cache.get(mimeType);
		return result != null ? result : new MimeType(mimeType);
	}

	public static List<MimeType> parseAcceptHeader(String value) {
		// text/plain; q=0.5, text/html, text/x-dvi; q=0.8, text/x-c
		// TODO: This parser breaks if quoted strings are involved, but that is
		// usually not the case for accept headers.
		final List<MimeType> results = new ArrayList<>();
		for (final String element : value.split(",")) {
			results.add(MimeType.get(element));
		}
		return results;
	}

}