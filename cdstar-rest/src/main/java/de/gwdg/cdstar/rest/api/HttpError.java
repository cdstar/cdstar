package de.gwdg.cdstar.rest.api;

import java.io.IOException;
import java.util.Iterator;

/**
 * Basic HTTP errors triggered by the REST framework. The application should
 * register 'nice' handlers for these errors.
 */
public class HttpError extends IOException {

	private static final long serialVersionUID = 3467778144280283338L;
	private final int status;
	private final String help;

	public static class NotAcceptable extends HttpError {
		private static final long serialVersionUID = 8911703875360551353L;

		public NotAcceptable() {
			super(406, "NotAcceptable");
		}

	}

	public static class UnsupportedMediaType extends HttpError {
		private static final long serialVersionUID = -7768957925790754117L;

		public UnsupportedMediaType() {
			super(415, "UnsupportedMediaType");
		}

	}

	public static class MethodNotAllowed extends HttpError {

		private static final long serialVersionUID = -418918166496944560L;
		private String allowed;

		public MethodNotAllowed(Iterable<String> allowed) {
			super(405, "MethodNotAllowed");

			final Iterator<String> ai = allowed.iterator();

			if (ai.hasNext()) {
				final StringBuilder sb = new StringBuilder();
				sb.append(ai.next());
				while (ai.hasNext())
					sb.append(", ").append(ai.next());

				this.allowed = sb.toString();
			} else {
				this.allowed = "";
			}
		}

		public String getAllowedHeader() {
			return allowed;
		}
	}

	public static class NotFound extends HttpError {
		private static final long serialVersionUID = -3039132532848462261L;
		public NotFound() {
			super(404, "NotFound");
		}
	}

	public HttpError(int status, String help) {
		super(status + ": " + help);
		this.status = status;
		this.help = help;
	}

	public int getStatus() {
		return status;
	}

	public String getHelp() {
		return help;
	}

	@Override
	public String toString() {
		return getMessage();
	}

}
