package de.gwdg.cdstar.rest.api;

@FunctionalInterface
public interface AsyncRequestHandler {
	void handle(AsyncContext ctx) throws Exception;
}