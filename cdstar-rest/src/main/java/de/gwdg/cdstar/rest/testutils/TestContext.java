package de.gwdg.cdstar.rest.testutils;

import java.io.IOException;
import java.net.URI;

import de.gwdg.cdstar.rest.RestContextBase;
import de.gwdg.cdstar.rest.api.AsyncContext;
import de.gwdg.cdstar.rest.api.RestContext;

public class TestContext extends RestContextBase {

	private final TestRequest rq;
	private final TestResponse rs;
	private TestAsyncContext async;

	public TestContext(TestRequest rq) {
		this.rq = rq;
		rs = new TestResponse();
	}

	@Override
	public String getMethod() {
		return rq.getMethod();
	}

	@Override
	public String getPath() {
		return rq.getPath();
	}

	@Override
	public String getHeader(String name) {
		return rq.getHeaders().get(name.toLowerCase());
	}

	@Override
	public String getQuery() {
		return rq.getQueryString();
	}

	@Override
	public String resolvePath(String path, boolean keepLastSegment) {
		String totalPath = rq.getPath();
		if (keepLastSegment && !totalPath.endsWith("/"))
			totalPath += "/";
		return URI.create(totalPath).resolve(path).toString();
	}

	@Override
	public int read(byte[] buffer, int off, int len) throws IOException {
		return rq.read(buffer, off, len);
	}

	@Override
	public RestContext status(int code) {
		rs.status(code);
		return this;
	}

	@Override
	public RestContext header(String name, String value) {
		rs.header(name, value);
		return this;
	}

	@Override
	public String header(String name) {
		return rs.getHeader(name);
	}

	@Override
	public void write(byte[] buffer, int off, int len) throws IOException {
		rs.write(buffer, off, len);
	}

	@Override
	public boolean isCommitted() {
		return rs.isCommitted() || isClosed();
	}

	@Override
	public AsyncContext startAsync() {
		async = new TestAsyncContext(this);
		return async;
	}

	@Override
	public boolean isAsync() {
		return async != null;
	}

	@Override
	public long getContentLength() {
		return Long.valueOf(rq.getHeaders().getOrDefault("content-length", rq.getEntity() != null ? "-1" : "0"));
	}

	TestResponse getResponse() {
		return rs;
	}

	@Override
	public void close() {
		if (isClosed())
			return;
		if (async != null)
			async.runCloseListeners();
		rs.close();
		super.close();
	}

}
