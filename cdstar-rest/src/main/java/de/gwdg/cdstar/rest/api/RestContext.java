package de.gwdg.cdstar.rest.api;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.Set;
import java.util.concurrent.Future;

import de.gwdg.cdstar.rest.api.HttpError.NotAcceptable;
import de.gwdg.cdstar.rest.utils.RestUtils;

/**
 * A single interface to represent an HTTP request as well as an API to define
 * the response.
 *
 * Inspired by HttpServletRequest, but can also be implemented natively on
 * jetty/netty/undertow without a servlet layer in between.
 *
 */
public interface RestContext extends AutoCloseable {

	String ATTR_MATCH_PARAMS = "cdstar-match-parsms";
	String ATTR_MATCH_CONF = "cdstar-match-conf";
	String ATTR_MATCH_ROUTE = "cdstar-match-route";
	String ATTR_MATCH_HANDLER = "cdstar-match-handler";

	RestConfig getConfig();

	String getMethod();

	String getPath();

	/**
	 * Return the given header as a string, or null if the header is not set.
	 */
	String getHeader(String name);

	/**
	 * Return the content type (all lower-case, options removed)
	 */
	default String getContentType() {
		final String value = getHeader("Content-Type");
		if (value == null)
			return null;
		final int i = value.indexOf(';');
		return i > -1 ? value.substring(0, i).toLowerCase() : value.toLowerCase();
	}

	/**
	 * Return the first path parameter with this name, or null if no such path
	 * parameter exists.
	 *
	 * Path parameters are extracted from the request path if regular expressions
	 * were used to match this request to a handler.
	 */
	String getPathParam(String name);

	/**
	 * Return the values of all query parameters with this name as an (possibly
	 * empty) array of strings. The array may contain empty strings for parameters
	 * with no value.
	 */
	String[] getQueryParams(String name);

	/**
	 * Return the first query parameter with this name, or null if no such query
	 * parameter exists. Parameters with no value are returned as empty strings.
	 */
	default String getQueryParam(String name) {
		final String[] values = getQueryParams(name);
		if (values != null && values.length > 0)
			return values[0];
		return null;
	}

	/**
	 * Get the names of all query parameters.
	 */
	Set<String> getQueryParamNames();

	String getQuery();

	/**
	 * Resolve a relative path against the current request path. Example: /script +
	 * /path/info + ./relative/path = /script/path/relative/path Example: /script +
	 * /path/info/ + ./relative/path = /script/path/info/relative/path
	 *
	 * @param path            Path relative to the current request path.
	 * @param keepLastSegment If true and the current path does not end in a slash,
	 *                        it is appended before the path is resolved..
	 */

	String resolvePath(String path, boolean keepLastSegment);

	/**
	 * Same as {@link #getConfig()} followed by {@link RestConfig#lookup(Class)}
	 */
	default <T> T getService(Class<T> t) {
		return getConfig().lookup(t);
	}

	/**
	 * Returns true if this request has an entity.
	 */
	boolean hasEntity();

	InputStream getInputStream();

	/**
	 * Read bytes from the request body to a byte buffer. It is an error to call
	 * this method if {@link #hasEntity()} returns false.
	 *
	 * @param buffer Byte buffer to copy bytes to.
	 * @param off    Offset within the byte buffer.
	 * @param len    Maximum number of bytes to read.
	 * @return Actual number of bytes copied to the buffer.
	 * @throws IOException if an IO error occurred.
	 */

	int read(byte[] buffer, int off, int len) throws IOException;

	/**
	 * Read bytes from the request body to a byte buffer. It is an error to call
	 * this method if {@link #hasEntity()} returns false.
	 *
	 * @param buffer Byte buffer to copy bytes to.
	 * @return Actual number of bytes copied to the buffer.
	 * @throws IOException if an IO error occurred.
	 */
	default int read(byte[] buffer) throws IOException {
		return read(buffer, 0, buffer.length);
	}

	/**
	 * Read bytes from the request body to a byte buffer. It is an error to call
	 * this method if {@link #hasEntity()} returns false.
	 *
	 * @param buffer Byte buffer to copy bytes to.
	 * @return Actual number of bytes copied to the buffer.
	 * @throws IOException if an IO error occurred.
	 */
	default int read(ByteBuffer buffer) throws IOException {
		if (buffer.hasArray()) {
			final int nb = read(buffer.array(), buffer.arrayOffset() + buffer.position(), buffer.remaining());
			if (nb > 0)
				buffer.position(buffer.position() + nb);
			return nb;
		}

		final byte[] buff = new byte[buffer.remaining()];
		final int i = read(buff);
		buffer.put(buff, 0, i);
		return i;
	}

	/**
	 * Change the response status code.
	 *
	 * This method has no effect if the response is already closed or committed.
	 */

	RestContext status(int code);

	/**
	 * Set a header for the response.
	 *
	 * This method has no effect if the response is already closed or committed.
	 */
	RestContext header(String name, String value);

	/**
	 * See {@link #header(String, String)}
	 */
	default RestContext header(String name, long l) {
		header(name, Long.toString(l));
		return this;
	}

	/**
	 * See {@link #header(String, String)}
	 */
	default RestContext header(String name, int i) {
		header(name, Integer.toString(i));
		return this;
	}

	/**
	 * See {@link #header(String, String)}
	 */
	default RestContext header(String name, Date date) {
		header(name, RestUtils.httpDate(date));
		return this;
	}

	/**
	 * Return a previously defined response header, or null;
	 */

	String header(String name);

	/**
	 * Write bytes to the response buffer.
	 *
	 * The ownership of the buffer is transfered to the container. Changing the
	 * buffers content while the container owns it may result in garbage sent to the
	 * client.
	 *
	 * @param buffer Byte buffer to copy bytes from.
	 * @param off    Offset within the byte buffer.
	 * @param len    Maximum number of bytes to send.
	 * @throws IOException if an IO error occurred.
	 */
	void write(byte[] buffer, int off, int len) throws IOException;

	/**
	 * Write an entity to the response buffer. This will find the most suitable
	 * response mapper based on the clients 'Accept' header and call it.
	 *
	 * The context is not {@link #close()} by calling this method.
	 *
	 * @throws UnsupportedOperationException if none of the registered response
	 *                                       mappers are able to encode the given
	 *                                       object type.
	 * @throws NotAcceptable                 if none of the content-types in the
	 *                                       clients Accept header can be produced.
	 *                                       The status code of this error will be
	 *                                       406.
	 */
	void write(Object entity) throws IOException, NotAcceptable;

	/**
	 * Write all bytes from a stream synchronously. The stream is NOT closed.
	 */
	default void write(InputStream stream) throws IOException {
		final byte[] buffer = new byte[1024 * 46];
		int bytes;
		while ((bytes = stream.read(buffer)) > -1) {
			write(buffer, 0, bytes);
		}
	}

	/**
	 * @see #write(byte[], int, int)
	 */
	default void write(byte[] bytes) throws IOException {
		write(bytes, 0, bytes.length);
	}

	default void write(ByteBuffer buffer) throws IOException {
		if (!buffer.hasRemaining())
			return;

		if (buffer.hasArray()) {
			write(buffer.array(), buffer.arrayOffset() + buffer.position(), buffer.remaining());
		} else {
			final byte[] copy = new byte[buffer.remaining()];
			buffer.get(copy);
			write(copy, 0, copy.length);
		}
	}

	/**
	 * Write text (encoded as UTF-8) to the response buffer.
	 */
	default void write(String text) throws IOException {
		write(text.getBytes(StandardCharsets.UTF_8));
	}

	/**
	 * Return true if status and headers have already been sent, or the context was
	 * closed. Calling {@link #status(int)} or {@link #header(String, String)} after
	 * this has no effect.
	 */
	boolean isCommitted();

	/**
	 * Start async mode and return an async context.
	 *
	 * The container will not call {@link #close()} for async requests and the
	 * context will stay alive until {@link #close()} or {@link #abort(Throwable)}
	 * are called explicitly, or a timeout occurs.
	 */
	AsyncContext startAsync();

	/**
	 * Return true if the context is in asynchronous mode and not closed yet.
	 */
	boolean isAsync();

	/**
	 * Closes the context and all associated resources. This has to be called
	 * explicitly if the request was detached for asynchronous handling. In
	 * synchronous mode, the container closes contexts automatically.
	 */
	@Override
	void close();

	/**
	 * Returns true if either {@link #close()} or {@link #abort(Throwable)} was
	 * called on this context.
	 */

	boolean isClosed();

	/**
	 * Close this context with an error.
	 *
	 * This calls the suitable error handler, if any, and then closes the context.
	 * If there is no error handler registered, of if the error handler fails with a
	 * new exception, the problem is logged and the response is closed as an empty
	 * 500 error. In any case, this method never raises an exception itself.
	 *
	 * For synchronous contexts, this method is called automatically if any
	 * exception is thrown from a request handler. Asynchronous contexts may call
	 * this method at any time to close a connection.
	 */
	void abort(Throwable t);

	/**
	 * Run this runnable in a container-managed thread pool.
	 */
	Future<?> runInPool(Runnable runnable);

	/**
	 * Registers a runnable to be run after the request has finished. If the request
	 * has finished already, it is run immediately.
	 */
	void runAfterRequest(Runnable runnable);

	default boolean isForm() {
		return "application/x-www-form-urlencoded".equals(getContentType());
	}

	default boolean isMultipart() {
		return "multipart/form-data".equals(getContentType());
	}

	/**
	 * Get the value of a context-local attribute.
	 */
	<T> T getAttribute(String name, Class<T> type);

	/**
	 * Set the value of a context-local attribute.
	 */
	void setAttribute(String name, Object value);

	/**
	 * Return the request content length (as specified by the client), or -1 if not
	 * specified.
	 */
	long getContentLength();

	/**
	 * Return a request ID for logging and diagnostics. The ID is usually generated
	 * by a counter or PRNG and should be unique for the lifetime of an instance.
	 */
	long getRequestId();

}
