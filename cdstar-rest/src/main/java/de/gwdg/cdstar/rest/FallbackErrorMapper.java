package de.gwdg.cdstar.rest;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.gwdg.cdstar.rest.api.ErrorMapper;
import de.gwdg.cdstar.rest.api.HttpError;
import de.gwdg.cdstar.rest.api.RestContext;

public class FallbackErrorMapper implements ErrorMapper<Throwable> {
	private static final Logger log = LoggerFactory.getLogger(FallbackErrorMapper.class);

	@Override
	public void handle(RestContext ctx, Throwable e) throws IOException {
		if (e instanceof HttpError) {
			ctx.status(((HttpError) e).getStatus());
			ctx.header("Content-Type", "text/plain");
			ctx.write(e.getMessage());
			return;
		}
		log.warn("No mapper for exception: {}", e.getClass().getName());
		ctx.status(500);
		ctx.header("Content-Type", "text/plain");
		ctx.write("Internal Server Error");

	}

}
