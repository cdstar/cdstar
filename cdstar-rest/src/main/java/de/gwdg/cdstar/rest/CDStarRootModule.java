package de.gwdg.cdstar.rest;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.auth.Subject;
import de.gwdg.cdstar.pool.PoolError;
import de.gwdg.cdstar.rest.api.HttpError;
import de.gwdg.cdstar.rest.api.RestBlueprint;
import de.gwdg.cdstar.rest.api.RestConfig;
import de.gwdg.cdstar.rest.api.RestContext;
import de.gwdg.cdstar.rest.utils.RestObjectMapper;
import de.gwdg.cdstar.rest.utils.RestUtils;
import de.gwdg.cdstar.rest.utils.SessionHelper;
import de.gwdg.cdstar.rest.v2.ApiV2Module;
import de.gwdg.cdstar.rest.v2.DariahBlueprint;
import de.gwdg.cdstar.rest.v3.ApiV3Module;
import de.gwdg.cdstar.runtime.Config;
import de.gwdg.cdstar.runtime.RuntimeContext;
import de.gwdg.cdstar.runtime.client.exc.AccessError;
import de.gwdg.cdstar.runtime.client.exc.ArchiveLocked;
import de.gwdg.cdstar.runtime.client.exc.ArchiveNotAvailable;
import de.gwdg.cdstar.runtime.client.exc.ArchiveNotFound;
import de.gwdg.cdstar.runtime.client.exc.ConstraintError;
import de.gwdg.cdstar.runtime.client.exc.FileExists;
import de.gwdg.cdstar.runtime.client.exc.FileNotFound;
import de.gwdg.cdstar.runtime.client.exc.InvalidFileName;
import de.gwdg.cdstar.runtime.client.exc.SnapshotLocked;
import de.gwdg.cdstar.runtime.client.exc.SnapshotNotFound;
import de.gwdg.cdstar.runtime.client.exc.TemporaryError;
import de.gwdg.cdstar.runtime.client.exc.VaultNotFound;
import de.gwdg.cdstar.runtime.search.SearchProvider;
import de.gwdg.cdstar.ta.exc.TARollbackException;
import de.gwdg.cdstar.web.common.model.ErrorResponse;

public class CDStarRootModule implements RestBlueprint {

	private static final Logger log = LoggerFactory.getLogger(CDStarRootModule.class);

	private final RuntimeContext runtime;

	private String baseUrl;
	public Map<String, String> links = new HashMap<>();

	public CDStarRootModule(RuntimeContext runtime) {
		this.runtime = runtime;
	}

	@Override
	public void configure(RestConfig cfg) {
		cfg.register(runtime);

		final Config conf = runtime.getConfig();
		conf.setDefault("api.v2.enable", "false");
		conf.setDefault("api.v3.enable", "true");
		conf.setDefault("api.dariah.enable", "false");
		conf.setDefault("api.context", "/");

		baseUrl = conf.get("api.context", "/");

		links.put("help", "https://cdstar.gwdg.de/");

		cfg.route("/").GET(ctx -> new Hello(links));

		if (conf.getBool("api.v3.enable")) {
			cfg.install(new ApiV3Module());
			links.put("v3", baseUrl + "v3/");
		}

		if (conf.getBool("api.v2.enable")) {
			cfg.install(new ApiV2Module(baseUrl));
			links.put("v2", baseUrl + "v2/");
		}

		if (conf.getBool("api.dariah.enable")) {
			cfg.install(new DariahBlueprint());
			links.put("dariah", baseUrl + "dariah/");
		}

		runtime.lookupAll(SearchProvider.class).forEach(cfg::register);
		runtime.lookupAll(RestBlueprint.class).forEach(cfg::install);

		cfg.mapError(TARollbackException.class, (ctx, e) -> {
			final Throwable reason = e.getCause();
			if (reason instanceof PoolError.Conflict)
				ctx.abort(new ErrorResponse(409, "Conflict", "Transaction rolled back because of a conflict"));
			else if (reason instanceof PoolError.WriteChannelNotClosed)
				ctx.abort(new ErrorResponse(409, "IncompleteWrite",
						"Detected an incompletely written file resource. Transaction cannot be committed."));
			else
				ctx.abort(reason);
		});

		cfg.mapError(PoolError.WriteChannelNotClosed.class, (ctx, e) -> {
			ctx.abort(new ErrorResponse(409, "IncompleteWrite",
					"Write operation in progress."));
		});

		cfg.mapError(VaultNotFound.class, (ctx, e) -> {
			ctx.abort(new ErrorResponse(404, "VaultNotFound",
				"Vault not found").detail("vault", e.getName()));
		});

		cfg.mapError(ArchiveNotFound.class, (ctx, e) -> {
			ctx.abort(new ErrorResponse(404, "ArchiveNotFound", "Archive not found")
				.detail("vault", e.getVault())
				.detail("archive", e.getArchive()));
		});

		cfg.mapError(SnapshotNotFound.class, (ctx, e) -> {
			ctx.abort(new ErrorResponse(404, "ArchiveNotFound", "Archive not found")
					.detail("vault", e.getVault())
					.detail("snapshot", e.getSnapsotId()));
		});

		cfg.mapError(SnapshotLocked.class, (ctx, e) -> {
			ctx.abort(new ErrorResponse(401, "SnapshotLocked", "Snapshots are read-only"));
		});

		cfg.mapError(FileNotFound.class, (ctx, e) -> {
			ctx.abort(
				new ErrorResponse(404, "FileNotFound", "File not found")
					.detail("vault", e.getVault())
					.detail("archive", e.getArchive())
					.detail("file", e.getName()));
		});

		cfg.mapError(FileExists.class, (ctx, e) -> {
			if (Utils.equal(e.getConflict(), e.getName()))
				ctx.abort(new ErrorResponse(409, "FileExists", "File name conflicts with existing file.")
					.detail("file", e.getName()));
			else
				ctx.abort(new ErrorResponse(409, "FileNameConflict", "File name conflicts with an existing file.")
					.detail("file", e.getName()).detail("conflict", e.getConflict()));
		});

		cfg.mapError(InvalidFileName.class, (ctx, e) -> {
			ctx.abort(
				new ErrorResponse(400, "InvalidFileName", e.getDetail())
					.detail("file", e.getName()));
		});

		cfg.mapError(ArchiveNotAvailable.class, (ctx, e) -> {
			ctx.abort(new ErrorResponse(409, "ArchiveNotAvailable",
				"Archive not available.")
					.detail("vault", e.getVault())
					.detail("archive", e.getArchive()));
		});

		cfg.mapError(ArchiveLocked.class, (ctx, e) -> {
			ctx.abort(new ErrorResponse(409, "ArchiveLocked",
				"Archive locked. Write operations are not permitted.")
					.detail("vault", e.getVault())
					.detail("archive", e.getArchive()));
		});

		cfg.mapError(AccessError.class, (ctx, e) -> {
			final Subject subject = SessionHelper.getSubject(ctx);
			if (subject.isAnonymous()) {
				ctx.header("WWW-Authenticate", "BASIC realm=\"cdstar\"");
				ctx.abort(new ErrorResponse(401, "unauthorized", "Authentication required"));
			} else {
				ctx.abort(new ErrorResponse(403, "forbidden", "Permission denied"));
			}
		});

		cfg.mapError(ConstraintError.class, (ctx, e) -> {
			ctx.abort(new ErrorResponse(400, "ConstraintViolation", e.getMessage(), e.getDetails(), null));
		});

		cfg.mapError(TemporaryError.class, (ctx, e) -> {
			ctx.abort(
				new ErrorResponse(503, "TemporaryError",
					"The request could not be processed at this very moment," +
						" but might succeed if tried again.",
					null, null));
		});

		cfg.mapError(Exception.class, (ctx, e) -> {
			log.error("Unhandled exception {}: {}", e.getClass().getSimpleName(), e.getMessage(), e);
			ctx.abort(new ErrorResponse(500, "ServerError", "Internal server error."));
		});

		cfg.mapError(ErrorResponse.class, (ctx, e) -> {
			ctx.status(e.getStatus());
			ctx.write(e);
		});

		cfg.mapError(HttpError.class, (ctx, e) -> {
			ctx.abort(new ErrorResponse(
				e.getStatus(), "HttpError", e.getHelp()));
		});

		cfg.mapResponse("application/json; q=0.2", Object.class, this::mapToJson);
		cfg.mapResponse("text/html; q=0.1", Object.class, this::mapToHTML);
		cfg.mapResponse("*", ByteBuffer.class, this::mapToBuffer);

	}

	public static class Hello {
		public String hello = "Hello! This is a cdstar instance.";
		public Map<String, String> links;

		public Hello(Map<String, String> links) {
			this.links = links;
		}
	}

	private void mapToJson(RestContext ctx, Object value) throws IOException {
		final byte[] json = RestObjectMapper.json.writeValueAsBytes(value);
		ctx.header("Content-Type", "application/json");
		ctx.header("Content-Length", json.length);
		ctx.write(json);
	}

	private void mapToHTML(RestContext ctx, Object value) throws IOException {
		final String json = RestObjectMapper.json.writeValueAsString(value);
		ctx.header("Content-Type", "text/html");
		ctx.write("<pre>");
		ctx.write(RestUtils.escapeHTML(json));
		ctx.write("</pre>");
	}

	private void mapToBuffer(RestContext ctx, ByteBuffer val) throws IOException {
		ctx.write(val.array(), val.arrayOffset() + val.position(), val.remaining());
	}

}
