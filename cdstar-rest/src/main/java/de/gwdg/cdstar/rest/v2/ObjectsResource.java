package de.gwdg.cdstar.rest.v2;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.pool.Resource;
import de.gwdg.cdstar.rest.api.RestConfig;
import de.gwdg.cdstar.rest.api.RestContext;
import de.gwdg.cdstar.rest.api.RestBlueprint;
import de.gwdg.cdstar.rest.utils.SessionHelper;
import de.gwdg.cdstar.rest.v2.model.ApiErrorResponse;
import de.gwdg.cdstar.rest.v2.model.ObjectBean;
import de.gwdg.cdstar.rest.v2.model.ObjectBean.BitstreamBean;
import de.gwdg.cdstar.rest.v2.model.ObjectBean.MetadataBean;
import de.gwdg.cdstar.rest.v2.model.ObjectBean.PermissionBean;
import de.gwdg.cdstar.rest.v2.model.Success.ObjectCreated;
import de.gwdg.cdstar.rest.v2.utils.LegacyHelper;
import de.gwdg.cdstar.runtime.client.CDStarArchive;
import de.gwdg.cdstar.runtime.client.CDStarFile;
import de.gwdg.cdstar.runtime.client.CDStarSession;
import de.gwdg.cdstar.runtime.client.exc.ArchiveNotFound;
import de.gwdg.cdstar.runtime.client.exc.FileNotFound;
import de.gwdg.cdstar.runtime.client.exc.VaultNotFound;
import de.gwdg.cdstar.ta.exc.TARollbackException;

public class ObjectsResource implements RestBlueprint {

	@Override
	public void configure(RestConfig cfg) {
		cfg.route("/<vault>/objects").POST(this::newObject);
		cfg.route("/<vault>/objects/<uid>").GET(this::getObject).DELETE(this::deleteObject);
	}

	public ObjectCreated newObject(RestContext ctx) throws TARollbackException, VaultNotFound {
		final String vaultName = ctx.getPathParam("vault");
		String typeName = ctx.getQueryParam("type");

		if (typeName == null || "object".equalsIgnoreCase(typeName))
			typeName = "object";
		else if ("collection".equalsIgnoreCase(typeName))
			typeName = "collection";
		else
			throw new ApiErrorResponse(400, "parameter", "Invalid object type");

		final CDStarSession session = SessionHelper.getCDStarSession(ctx, false);
		final CDStarArchive a = session.getVault(vaultName).createArchive();
		a.setProperty(ApiV2Module.v2TypeKey, typeName);
		session.commit();

		ctx.status(201);
		ctx.header("Location", ctx.resolvePath(LegacyHelper.toLegacyID(a.getId()), true));
		return new ObjectCreated(LegacyHelper.toLegacyID(a.getId()));
	}

	static ObjectBean makeObjectBean(CDStarArchive ar) throws FileNotFound {
		final ObjectBean bean = new ObjectBean();

		bean.setUid(LegacyHelper.toLegacyID(ar.getId()));
		bean.setRevision(ar.getRev());
		bean.setType(ar.getProperty(ApiV2Module.v2TypeKey, "object").toUpperCase());
		bean.setCreated(ar.getCreated());
		bean.setLastModified(ar.getContentModified());

		final PermissionBean perm = new PermissionBean();
		LegacyHelper.translatePermissions(perm, ar);
		bean.setPermissions(perm);

		if (ar.hasFile(ApiV2Module.v2metaFile) && ar.getFile(ApiV2Module.v2metaFile).getSize() > 0) {
			final CDStarFile doc = ar.getFile(ApiV2Module.v2metaFile);
			final MetadataBean meta = new MetadataBean();
			meta.setChecksum(doc.getDigests().get(CDStarFile.DIGEST_SHA256));
			meta.setChecksumAlgorithm(Resource.DIGEST_SHA256);
			meta.setContentType("application/json");
			meta.setLastModified(doc.getLastModified());
			bean.setMetadata(meta);
		}

		if ("object".equals(ar.getProperty(ApiV2Module.v2TypeKey, "object"))) {
			bean.setBitstream(new ArrayList<BitstreamBean>());

			// Sort the access keys
			final List<CDStarFile> sortedFiles = Utils.toArrayList(ar.getFiles());
			java.util.Collections.sort(sortedFiles, new Comparator<CDStarFile>() {
				@Override
				public int compare(CDStarFile o1, CDStarFile o2) {
					return o1.getName().compareTo(o2.getName());
				}
			});

			for (final CDStarFile af : sortedFiles) {
				if (ApiV2Module.v2metaFile.equals(af.getName()))
					continue;
				final BitstreamBean bs = new BitstreamBean();
				bs.setBitstreamid(af.getName());
				bs.setChecksum(af.getDigests().get(CDStarFile.DIGEST_SHA256));
				bs.setChecksumAlgorithm(Resource.DIGEST_SHA256);
				bs.setFilesize(af.getSize());
				bs.setLastModified(af.getLastModified());
				bs.setCreated(af.getCreated());
				bs.setContentType(af.getMediaType());
				bean.getBitstream().add(bs);
			}
		} else {
			bean.setCollection(ar.getAttribute(CollectionResource.linksKey).values());
		}

		return bean;

	}

	public ObjectBean getObject(RestContext ctx) throws FileNotFound, ArchiveNotFound, VaultNotFound {
		final String vaultName = ctx.getPathParam("vault");
		final String uid = LegacyHelper.fromAnyID(ctx.getPathParam("uid"));

		final CDStarSession session = SessionHelper.getCDStarSession(ctx, false);
		final CDStarArchive ar = session.getVault(vaultName).loadArchive(uid);
		final ObjectBean bean = makeObjectBean(ar);

		return bean;
	}

	public Void deleteObject(RestContext ctx) throws TARollbackException, ArchiveNotFound, VaultNotFound {
		final String vaultName = ctx.getPathParam("vault");
		final String uid = LegacyHelper.fromAnyID(ctx.getPathParam("uid"));
		final CDStarSession session = SessionHelper.getCDStarSession(ctx, false);
		session.getVault(vaultName).loadArchive(uid).remove();
		session.commit();
		ctx.status(204);
		return null;
	}

}
