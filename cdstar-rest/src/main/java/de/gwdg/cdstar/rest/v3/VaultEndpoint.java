package de.gwdg.cdstar.rest.v3;

import java.util.ArrayList;
import java.util.List;

import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.rest.api.RestBlueprint;
import de.gwdg.cdstar.rest.api.RestConfig;
import de.gwdg.cdstar.rest.api.RestContext;
import de.gwdg.cdstar.rest.utils.QueryHelper;
import de.gwdg.cdstar.rest.utils.SessionHelper;
import de.gwdg.cdstar.rest.v3.async.SearchHandler;
import de.gwdg.cdstar.runtime.client.CDStarArchive;
import de.gwdg.cdstar.runtime.client.CDStarVault;
import de.gwdg.cdstar.runtime.client.exc.VaultNotFound;
import de.gwdg.cdstar.runtime.client.utils.ArchiveOrSnapshot;
import de.gwdg.cdstar.web.common.model.ScrollResult;
import de.gwdg.cdstar.web.common.model.VaultInfo;

public class VaultEndpoint implements RestBlueprint {

	private static final String PARAM_SCROLL = "scroll";
	public static final int SCROLL_LIMIT = 128;

	@Override
	public void configure(RestConfig cfg) {
		cfg.route("/<vault:re:[^_][^/]*>").GET(this::handleGet).POST(this::handlePost);
	}

	public Object handleGet(RestContext ctx) throws Exception {
		if (ctx.getQueryParam(SearchHandler.PARAM_Q) != null) {
			new SearchHandler(ctx).dispatch();
			return null;
		}

		// Beta interface
		if (ctx.getQueryParam(PARAM_SCROLL) != null)
			return handleScroll(ctx);

		final CDStarVault vault = SessionHelper.getCDStarSession(ctx, true).getVault(ctx.getPathParam("vault"));
		return new VaultInfo(vault.isPublic());
	}

	private ScrollResult handleScroll(RestContext ctx) throws VaultNotFound {
		final QueryHelper qh = new QueryHelper(ctx);
		qh.setDefault("limit", "25");
		qh.setDefault("strict", "false");

		final String scanOffset = qh.get(PARAM_SCROLL); // may be empty
		final int limit = Utils.gate(0, qh.getInt("limit", 0, Integer.MAX_VALUE), SCROLL_LIMIT);
		final boolean strict = qh.getBoolean("strict");
		qh.ensureNoUnusedParameters();

		final CDStarVault vault = SessionHelper.getCDStarSession(ctx, true).getVault(ctx.getPathParam("vault"));

		final List<String> result = new ArrayList<>(limit);
		for (final String id : vault.getArchiveIterable(scanOffset, strict)) {
			result.add(id);
			if (result.size() >= limit)
				break;
		}

		return new ScrollResult(limit, result);
	}

	public Void handlePost(RestContext ctx) throws Exception {
		final CDStarVault vault = SessionHelper.getCDStarSession(ctx, false).getVault(ctx.getPathParam("vault"));

		// Experimental (undocumented) setid support to aid migrations of V2 instances.
		final String setid = ctx.getQueryParam("_setid");
		final CDStarArchive archive = Utils.notNullOrEmpty(setid) ? vault.createArchive(setid) : vault.createArchive();

		ctx.status(201);
		ctx.header("Location", ctx.resolvePath("./" + archive.getId(), true));
		return ArchiveEndpoint.createOrUpdate(ctx, new ArchiveOrSnapshot(archive));
	}

}
