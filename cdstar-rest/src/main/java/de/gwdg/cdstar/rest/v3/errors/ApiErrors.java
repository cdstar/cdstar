package de.gwdg.cdstar.rest.v3.errors;

import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.web.common.model.ErrorResponse;

public class ApiErrors {

	public static ErrorResponse vaultNotFound(String vaultName) {
		return new ErrorResponse(404, "VaultNotFound", "The requested vault does not exist or is not accessible.")
			.detail("vault", vaultName);
	}

	public static ErrorResponse archvieNotFound(String vaultName, String archiveId) {
		return new ErrorResponse(404, "ArchiveNotFound", "The requested archive does not exist or is not accessible.")
			.detail("vault", vaultName)
			.detail("archive", archiveId);
	}

	public static ErrorResponse permissionDenied(String permissionName, String... resource) {
		return new ErrorResponse(401, "PermissionDenied", "Infuccicient permissions to perform the operation.")
			.detail("permission", permissionName)
			.detail("resource", Utils.join(":", resource));
	}

	public static ErrorResponse notImplemented(String msg) {
		return new ErrorResponse(501, "NotImplemented", msg);
	}

	public static ErrorResponse badRequest(String msg) {
		return new ErrorResponse(400, "BadRequest", msg);
	}

}
