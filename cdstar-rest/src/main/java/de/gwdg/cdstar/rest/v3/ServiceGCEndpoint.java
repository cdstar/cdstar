package de.gwdg.cdstar.rest.v3;

import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.lang.management.MemoryMXBean;
import java.lang.management.MemoryUsage;

import de.gwdg.cdstar.rest.api.RestBlueprint;
import de.gwdg.cdstar.rest.api.RestConfig;
import de.gwdg.cdstar.rest.api.RestContext;
import de.gwdg.cdstar.rest.utils.SessionHelper;
import de.gwdg.cdstar.runtime.client.auth.ServicePermission;

/**
 * Undocumented API to trigger a full GC run.
 */
public class ServiceGCEndpoint implements RestBlueprint {
	@Override
	public void configure(RestConfig cfg) {
		cfg.route("/_gc").POST(this::handlePostGC);
	}

	public GCResult handlePostGC(RestContext ctx) throws IOException {
		SessionHelper.ensurePermitted(ctx, ServicePermission.HEALTH.asStringPermission());
		return new GCResult(ManagementFactory.getMemoryMXBean());
	}

	public class GCResult {
		public final MemoryUsage before;
		public final MemoryUsage after;
		public final double time;

		public GCResult(MemoryMXBean mem) {
			before = mem.getHeapMemoryUsage();
			final long t1 = System.nanoTime();
			System.gc();
			time = ((double) (System.nanoTime() - t1)) / 1000000000;
			after = mem.getHeapMemoryUsage();
		}
	}
}
