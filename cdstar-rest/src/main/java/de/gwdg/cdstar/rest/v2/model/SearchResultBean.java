package de.gwdg.cdstar.rest.v2.model;

import java.util.List;
import java.util.Vector;

import com.fasterxml.jackson.annotation.JsonInclude;

public class SearchResultBean {

	long		hitcount;
	float		maxscore;
	List<Hit>	hits	= new Vector<Hit>();

	public long getHitcount() {
		return hitcount;
	}

	public void setHitcount(long hitcount) {
		this.hitcount = hitcount;
	}

	public float getMaxscore() {
		return maxscore;
	}

	public void setMaxscore(float maxscore) {
		this.maxscore = maxscore;
	}

	public List<Hit> getHits() {
		return hits;
	}

	public void setHits(List<Hit> hits) {
		this.hits = hits;
	}

	public static class Hit {
		String	source;

		String	type;

		float	score;

		String	uid;

		@JsonInclude(value = JsonInclude.Include.NON_NULL)
		String	bitstreamid;

		public String getSource() {
			return source;
		}

		public void setSource(String source) {
			this.source = source;
		}

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}

		public float getScore() {
			return score;
		}

		public void setScore(float score) {
			this.score = score;
		}

		public String getUid() {
			return uid;
		}

		public void setUid(String uid) {
			this.uid = uid;
		}

		public String getBitstreamid() {
			return bitstreamid;
		}

		public void setBitstreamid(String bitstreamid) {
			this.bitstreamid = bitstreamid;
		}
	}
}
