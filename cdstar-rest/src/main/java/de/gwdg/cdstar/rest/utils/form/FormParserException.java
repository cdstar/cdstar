package de.gwdg.cdstar.rest.utils.form;

import java.io.IOException;

public class FormParserException extends IOException {
	private static final long serialVersionUID = -3006800522476991884L;

	public FormParserException(String message) {
		super(message);
	}

	public FormParserException(String message, Throwable cause) {
		super(message, cause);
	}
}
