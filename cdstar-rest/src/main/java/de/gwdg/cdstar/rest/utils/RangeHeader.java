package de.gwdg.cdstar.rest.utils;

/**
 * A simple HTTP Range header parser or Content-Range header generator.
 */

public class RangeHeader {
	final long start;
	final long end;
	final long total;

	public RangeHeader(long start, long end, long total) {
		if (end > total)
			end = total;
		if (start < 0)
			start = 0;
		else if (start > end)
			start = end;
		this.start = start;
		this.end = end;
		this.total = total;
	}

	public RangeHeader(String header, long total) {
		if (!header.startsWith("bytes=")) {
			throw new IllegalArgumentException("Range header must start with 'bytes='.");
		}
		this.total = total;
		final int commaPos = header.indexOf(',');
		final String range = header.substring(6 /* bytes= */, commaPos != -1 ? commaPos : header.length());
		final int i = range.indexOf('-');
		if (i == -1) {
			if (range.equals("*")) {
				start = 0;
				end = total;
			} else {
				throw new IllegalArgumentException("Invalid range: " + range);
			}
		} else if (i == 0) {
			// "-5" --> "95-100"
			start = total + Long.parseLong(range);
			end = total;
		} else if (i == range.length() - 1) {
			// "5-" --> "5-100"
			start = Long.parseLong(range.substring(0, i));
			end = total;
		} else {
			// "5-9" --> "5-10" (End index in Range headers is inclusive,
			// but we want the index after the last byte.)
			start = Long.parseLong(range.substring(0, i));
			end = Math.min(Long.parseLong(range.substring(i + 1)) + 1, total);
			if (start > end)
				throw new IllegalArgumentException("Start index out of bounds.");
		}
	}

	public RangeHeader(long total) {
		this(0, total, total);
	}

	public long getStartIndex() {
		return start;
	}

	public long getEndIndex() {
		return end;
	}

	public long getTotalLength() {
		return total;
	}

	public long getRangeLength() {
		return end - start;
	}

	@Override
	public String toString() {
		if (start == 0 && end == total)
			return "bytes */" + total;
		else
			return "bytes " + start + "-" + (end - 1) + "/" + total;
	}

	public boolean isFullRange() {
		return start == 0 && end == total;
		
	}

	public boolean isZeroLength() {
		return start == end;
	}

}