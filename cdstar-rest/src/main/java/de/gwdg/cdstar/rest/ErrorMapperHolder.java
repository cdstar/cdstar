package de.gwdg.cdstar.rest;

import de.gwdg.cdstar.rest.api.ErrorMapper;
import de.gwdg.cdstar.rest.api.RestContext;

public class ErrorMapperHolder {
	private final Class<?> klass;
	private final ErrorMapper<Throwable> mapper;

	@SuppressWarnings("unchecked")
	public <T extends Throwable> ErrorMapperHolder(Class<T> klass, ErrorMapper<T> mapper) {
		this.klass = klass;
		this.mapper = (ErrorMapper<Throwable>) mapper;
	}

	public void handle(RestContext ctx, Throwable t) throws Exception {
		mapper.handle(ctx, t);
	}

	public boolean handles(Class<? extends Throwable> candidate) {
		return klass.isAssignableFrom(candidate);
	}

	public Class<?> getMappedException() {
		return klass;
	}

	public ErrorMapper<? extends Throwable> getMapper() {
		return mapper;
	}
}