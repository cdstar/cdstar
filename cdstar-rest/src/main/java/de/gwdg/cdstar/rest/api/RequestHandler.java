package de.gwdg.cdstar.rest.api;

@FunctionalInterface
public interface RequestHandler {
	public Object handle(RestContext ctx) throws Exception;
}