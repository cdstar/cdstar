package de.gwdg.cdstar.rest.testutils;

import java.util.HashMap;

import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.rest.RequestDispatcher;
import de.gwdg.cdstar.rest.api.RestConfig;

/**
 * A helper class to test rest applications without any network overhead.
 */
public class TestClient {

	private final RestConfig cfg;
	final HashMap<String, String> headers;
	private final RequestDispatcher dispatcher;

	public TestClient(RestConfig cfg) {
		this.cfg = cfg;
		headers = new HashMap<>();
		setAgent(getClass().getCanonicalName());
		dispatcher = new RequestDispatcher(cfg);
	}

	public RestConfig getConfig() {
		return cfg;
	}

	public void reset() {
		dispatcher.reset();
	}

	void setAuth(String user, String password) {
		throw new UnsupportedOperationException();
	}

	void setAgent(String agent) {
		headers.put("agent", agent);
	}

	public TestRequest prepare() {
		return new TestRequest(this);
	}

	public TestRequest prepare(String method, String... path) {
		String joined = Utils.join("/", path);
		if (!joined.startsWith("/"))
			joined = "/" + joined;
		return new TestRequest(this).method(method).path(joined);
	}

	public TestRequest HEAD(String... path) {
		return prepare("HEAD", path);
	}

	public TestRequest GET(String... path) {
		return prepare("GET", path);
	}

	public TestRequest POST(String... path) {
		return prepare("POST", path);
	}

	public TestRequest PUT(String... path) {
		return prepare("PUT", path);
	}

	public TestRequest DELETE(String... path) {
		return prepare("DELETE", path);
	}

	public TestRequest PATCH(String... path) {
		return prepare("PATCH", path);
	}

	public TestResponse dispatch(TestRequest rq) {
		final TestContext ctx = new TestContext(rq);
		dispatcher.dispatch(ctx);
		return ctx.getResponse();

	}


}
