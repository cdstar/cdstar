package de.gwdg.cdstar.rest.v3.async;

import java.util.List;

import de.gwdg.cdstar.Promise;
import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.auth.Subject;
import de.gwdg.cdstar.rest.api.AsyncContext;
import de.gwdg.cdstar.rest.api.RestContext;
import de.gwdg.cdstar.rest.utils.QueryHelper;
import de.gwdg.cdstar.rest.utils.SessionHelper;
import de.gwdg.cdstar.runtime.search.SearchException;
import de.gwdg.cdstar.runtime.search.SearchProvider;
import de.gwdg.cdstar.runtime.search.SearchQueryBuilder;
import de.gwdg.cdstar.runtime.search.SearchResult;
import de.gwdg.cdstar.runtime.search.SplitQuery;
import de.gwdg.cdstar.web.common.model.ErrorResponse;
import de.gwdg.cdstar.web.common.model.SearchHits;
import de.gwdg.cdstar.web.common.model.SearchHits.Hit;

public class SearchHandler {

	public  static final String PARAM_FIELDS = "fields";
	public  static final String PARAM_GROUP = "group";
	public  static final String PARAM_SCROLL = "scroll";
	public  static final String PARAM_LIMIT = "limit";
	public  static final String PARAM_ORDER = "order";
	public static final String PARAM_Q = "q";

	private final RestContext ctx;
	private static final int MAX_RESULTS = 1024;
	private SearchQueryBuilder query;

	public SearchHandler(RestContext ctx) {
		this.ctx = ctx;
	}

	/**
	 * Prepare a {@link SearchQueryBuilder} from query parameters and Auth information.
	 * May be re-used by other routes that use the same query parameters as the v3 search API.
	 */
	public static SearchQueryBuilder prepareQuery(String vault, QueryHelper qh, Subject subject) {
		qh.setDefault(PARAM_LIMIT, "25");
		qh.setDefault(PARAM_ORDER, "-score");

		var query = new SearchQueryBuilder();
		query.vault(vault);
		query.query(qh.get(PARAM_Q));
		query.limit(Utils.gate(0, qh.getInt(PARAM_LIMIT, 0, Integer.MAX_VALUE), MAX_RESULTS));
		query.order(qh.getAnyCsv(PARAM_ORDER).toArray(new String[] {}));
		query.fields(qh.getAnyCsv(PARAM_FIELDS).toArray(new String[] {}));

		if (qh.has(PARAM_SCROLL))
			query.scrollId(qh.get(PARAM_SCROLL));
		for (final String group : qh.getAnyCsv(PARAM_GROUP)) {
			if (!subject.isMemberOf(group))
				throw new ErrorResponse(403, "InvalidGroupClaim",
					"One of the claimed group memberships could not be verified.").detail(PARAM_GROUP, group);
			query.group(group);
		}
		if (!subject.isAnonymous())
			query.principal(subject.getPrincipal().getFullId());

		qh.ensureNoUnusedParameters();

		applySearchQueryMarkers(query);
		return query;
	}


	/**
	 * Find, apply and remove special markers that can be embedded into the search
	 * string to control aspects of the search. Namely: "limit:*" and "order:*"
	 */
	private static void applySearchQueryMarkers(final SearchQueryBuilder qb) {
		final List<String> tokens = new SplitQuery(qb.getQuery()).split();
		if (tokens.size() > 0 && tokens.removeIf(token -> {
			if (token.startsWith("limit:")) {
				try {
					qb.limit(Utils.gate(0, Integer.parseInt(token.substring(6)), MAX_RESULTS));
				} catch (final NumberFormatException e) {
					// Eat it
				}
				return true;
			} else if (token.startsWith("order:")) {
				qb.order(token.substring(6).split(","));
				return true;
			}
			return false;
		})) {
			qb.query(Utils.join(" ", tokens));
		}
	}

	public void dispatch() {
		final SearchProvider sp = ctx.getService(SearchProvider.class);
		if (sp == null)
			throw new ErrorResponse(501, "SearchDisabled", "Search is not available in this instance");

		final AsyncContext async = ctx.startAsync();
		query = prepareQuery(ctx.getPathParam("vault"), new QueryHelper(ctx), SessionHelper.getSubject(ctx));
		final Promise<SearchResult> queryResult = sp.search(query.build());

		// TODO: Implement search timeouts (currently only the async idle
		// timeout ensures some upper bound in search time)

		async.addCloseListener((ac, err) -> {
			if (err != null)
				queryResult.cancel();
		});

		queryResult.map(this::buildSearchQueryResponse).mapVoid(sr -> {
			ctx.write(sr);
			ctx.close();
		}).then(null, e -> {
			// TODO: Handle different errors (400, 404, 50[1234])
			if (e instanceof SearchException) {
				final SearchException se = (SearchException) e;
				ctx.abort(new ErrorResponse(400, "SearchFailed", se.getMessage()));
			}
			ctx.abort(e);
		});
	}

	private SearchHits buildSearchQueryResponse(SearchResult rs) {
		List<Hit> items = Utils.map(rs.hits(), h -> {
			final Hit hit = new Hit();
			hit.id = h.getId();
			hit.type = h.getType();
			hit.score = h.getScore();
			hit.name = h.getName();
			hit.fields = h.getFields();
			return hit;
		});
		return new SearchHits(items, rs.getTotal(), rs.getScrollID());
	}

}
