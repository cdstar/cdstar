package de.gwdg.cdstar.rest.v3.async;

import java.util.Map;

import de.gwdg.cdstar.pool.PoolError.WriteChannelNotClosed;
import de.gwdg.cdstar.runtime.client.CDStarFile;
import de.gwdg.cdstar.runtime.client.CDStarMirrorState;
import de.gwdg.cdstar.runtime.client.CDStarSession;
import de.gwdg.cdstar.runtime.lts.LTSConfig;
import de.gwdg.cdstar.web.common.model.ArchiveState;
import de.gwdg.cdstar.web.common.model.DigestInfo;
import de.gwdg.cdstar.web.common.model.FileInfo;
import de.gwdg.cdstar.web.common.model.MetaMap;
import de.gwdg.cdstar.web.common.model.TransactionInfo;

public class ModelHelper {

	public static ArchiveState getArchiveState(CDStarMirrorState mirror) {
		final LTSConfig ltsConf = LTSConfig.fromProfile(mirror.getProfile()).orElse(null);

		if (mirror.isMigrationPending()) {
			return (ltsConf != null && ltsConf.isCold())
					? ArchiveState.PENDING_ARCHIVE
					: ArchiveState.PENDING_RECOVER;
		}

		if (mirror.isMirrored())
			return mirror.isAvailable() ? ArchiveState.LOCKED : ArchiveState.ARCHIVED;

		return ArchiveState.OPEN;
	}

	public static FileInfo makeFileInfo(CDStarFile file, boolean withMeta) {
		final FileInfo fi = new FileInfo();
		fi.id = file.getID();
		fi.name = file.getName();
		fi.type = file.getMediaType();
		fi.size = file.getSize();
		fi.created = file.getCreated();
		fi.modified = file.getLastModified();
		fi.digests = makeDigestInfo(file);
		if (withMeta)
			fi.meta = getFileMeta(file);
		return fi;
	}

	public static MetaMap getFileMeta(CDStarFile file) {
		final MetaMap mmap = new MetaMap();
		file.getAttributes()
			.stream()
			.forEach(
				p -> mmap.put(p.getName(), p.values()));
		return mmap;
	}

	public static DigestInfo makeDigestInfo(CDStarFile file) {
		try {
			final DigestInfo di = new DigestInfo();
			Map<String, String> digests = file.getDigests();
			di.sha256 = digests.get(CDStarFile.DIGEST_SHA256);
			di.sha1 = digests.get(CDStarFile.DIGEST_SHA1);
			di.md5 = digests.get(CDStarFile.DIGEST_MD5);
			return di;
		} catch (WriteChannelNotClosed e) {
			return null;
		}
	}

	public static TransactionInfo makeTransactionInfo(CDStarSession session) {
		final TransactionInfo ti = new TransactionInfo();
		ti.id = session.getSessionId();
		ti.isolation = session.getMode().toString().toLowerCase();
		ti.readonly = session.isReadOnly();
		ti.ttl = session.getRemainingTime() / 1000;
		ti.timeout = session.getTimeout() / 1000;
		return ti;
	}

}
