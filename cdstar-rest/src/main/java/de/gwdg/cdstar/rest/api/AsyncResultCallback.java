package de.gwdg.cdstar.rest.api;

import java.nio.ByteBuffer;

/**
 * Functional interface for an event listener for asynchronous read or write operations.
 * 
 * If an implementation throws an exception (checked or unchecked) and was
 * called with error=null, it is called a second time with error set to the
 * exception that was just thrown. Otherwise, the new exception is
 * suppressed.
 */

@FunctionalInterface
public interface AsyncResultCallback {
	void done(AsyncContext ac, ByteBuffer buffer, Throwable error) throws Exception;
}