package de.gwdg.cdstar.rest.v2;

import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.databind.JsonNode;

import de.gwdg.cdstar.rest.api.RestConfig;
import de.gwdg.cdstar.rest.api.RestContext;
import de.gwdg.cdstar.rest.api.RestBlueprint;
import de.gwdg.cdstar.rest.utils.RestUtils;
import de.gwdg.cdstar.rest.utils.SessionHelper;
import de.gwdg.cdstar.rest.v2.model.ApiErrorResponse;
import de.gwdg.cdstar.rest.v2.model.Success;
import de.gwdg.cdstar.rest.v2.model.Success.OkResponse;
import de.gwdg.cdstar.rest.v2.utils.LegacyHelper;
import de.gwdg.cdstar.runtime.client.CDStarArchive;
import de.gwdg.cdstar.runtime.client.CDStarAttribute;
import de.gwdg.cdstar.runtime.client.CDStarAttributeHelper;
import de.gwdg.cdstar.runtime.client.CDStarSession;
import de.gwdg.cdstar.runtime.client.exc.ArchiveNotFound;
import de.gwdg.cdstar.runtime.client.exc.VaultNotFound;
import de.gwdg.cdstar.ta.exc.TARollbackException;

public class CollectionResource implements RestBlueprint {

	static final String linksKey = CDStarAttributeHelper.join("sys", "cdstar2_links");

	@Override
	public void configure(RestConfig cfg) {
		cfg.route("/<vault>/collections/<uid>").GET(this::getObject).PUT(this::updateCollection);
	}

	public List<String> getObject(RestContext ctx) throws ArchiveNotFound, VaultNotFound {
		final CDStarSession session = SessionHelper.getCDStarSession(ctx, true);
		final CDStarArchive ar = session.getVault(ctx.getPathParam("vault")).loadArchive(LegacyHelper.fromAnyID(ctx.getPathParam("uid")));
		return ar.getAttribute(linksKey).values();
	}

	public OkResponse updateCollection(RestContext ctx)
			throws TARollbackException, IOException, ArchiveNotFound, VaultNotFound {
		final CDStarSession session = SessionHelper.getCDStarSession(ctx, false);
		final CDStarArchive ar = session.getVault(ctx.getPathParam("vault")).loadArchive(LegacyHelper.fromAnyID(ctx.getPathParam("uid")));
		final CDStarAttribute prop = ar.getAttribute(linksKey);

		final JsonNode values = RestUtils.parseJsonBody(ctx, JsonNode.class);
		if (!values.isArray())
			throw new ApiErrorResponse(400, "Not an array", "Must be array of strings");
		prop.clear();
		for (final JsonNode item : values)
			prop.append(item.asText());
		session.commit();

		ctx.status(201); // yes, this is wrong
		ctx.header("Location", ctx.resolvePath("", true));
		return new Success.OkResponse(LegacyHelper.toLegacyID(ar.getId()));

	}
}
