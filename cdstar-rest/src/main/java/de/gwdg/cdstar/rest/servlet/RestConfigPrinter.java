package de.gwdg.cdstar.rest.servlet;

import java.util.Map.Entry;

import de.gwdg.cdstar.rest.RestConfigImpl;
import de.gwdg.cdstar.rest.api.RequestHandler;
import de.gwdg.cdstar.rest.api.RestConfig;
import de.gwdg.cdstar.rest.api.RestRoute;

/**
 * Utility class do visualize a {@link RestConfig} tree.
 */
public class RestConfigPrinter {

	private final RestConfigImpl root;

	public RestConfigPrinter(RestConfigImpl rootConfig) {
		root = rootConfig;
	}

	private String indent(int n) {
		final StringBuilder sb = new StringBuilder(n * 2);
		for (int i = 0; i < n * 2; i++) {
			sb.append(' ');
		}
		return sb.toString();
	}

	private void writeTree(StringBuilder sb, RestConfig cfg, int n) {
		final String indent = indent(n);

		sb.append(indent);
		sb.append("[");
		sb.append("path=").append(cfg.getPath()).append(" ");
		sb.append("name=").append(cfg.getName()).append(" ");
		if (cfg.getInherit())
			sb.append("inherit=").append(cfg.getInherit());
		sb.append("]\n");
		for (final RestRoute route : cfg.getRoutes()) {
			for (final Entry<String, RequestHandler> r : route.getTargets().entrySet()) {
				sb.append(indent).append(" ")
					.append(cfg.getPath()).append(route.getRule()).append(' ')
					.append(r.getKey()).append(" -> ")
					.append(r.getValue())
					.append("\n");
			}
		}

		for (final RestConfig c : cfg.getChildren()) {
			writeTree(sb, c, n + 1);
		}
	}

	public String render(boolean indent) {
		final StringBuilder sb = new StringBuilder();
		writeTree(sb, root, indent ? 1 : 0);
		return sb.toString();
	}

	@Override
	public String toString() {
		return render(false);
	}

}
