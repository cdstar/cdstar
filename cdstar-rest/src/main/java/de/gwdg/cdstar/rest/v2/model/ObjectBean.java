package de.gwdg.cdstar.rest.v2.model;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({ "uid", "revision", "type", "created", "last-modified" })
public class ObjectBean {

	String uid;

	String revision;

	String type;

	PermissionBean permissions;

	MetadataBean metadata;

	@JsonInclude(value = Include.NON_NULL)
	List<BitstreamBean> bitstream;

	@JsonInclude(value = Include.NON_NULL)
	List<String> collection;

	Date created;

	@JsonProperty("last-modified")
	Date lastModified;

	public String getUid() {
		return uid;
	}

	public void setUid(String uuid) {
		uid = uuid;
	}

	public String getRevision() {
		return revision;
	}

	public void setRevision(String revision) {
		this.revision = revision;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public PermissionBean getPermissions() {
		return permissions;
	}

	public void setPermissions(PermissionBean permissions) {
		this.permissions = permissions;
	}

	public MetadataBean getMetadata() {
		return metadata;
	}

	public void setMetadata(MetadataBean metadata) {
		this.metadata = metadata;
	}

	public List<BitstreamBean> getBitstream() {
		return bitstream;
	}

	public BitstreamBean getBitstream(String name) {
		for (final BitstreamBean bs : getBitstream()) {
			if (bs.getBitstreamid().equals(name))
				return bs;
		}
		return null;
	}

	public void setBitstream(List<BitstreamBean> bitstream) {
		this.bitstream = bitstream;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getLastModified() {
		return lastModified;
	}

	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	public List<String> getCollection() {
		return collection;
	}

	public void setCollection(List<String> collection) {
		this.collection = collection;
	}

	public static class MetadataBean {
		String checksum;

		@JsonProperty("checksum-algorithm")
		String checksumAlgorithm;

		@JsonProperty("last-modified")
		Date lastModified;

		@JsonProperty("content-type")
		String contentType;

		public String getChecksum() {
			return checksum;
		}

		public void setChecksum(String checksum) {
			this.checksum = checksum;
		}

		public String getChecksumAlgorithm() {
			return checksumAlgorithm;
		}

		public void setChecksumAlgorithm(String checksumAlgorithm) {
			this.checksumAlgorithm = checksumAlgorithm;
		}

		public Date getLastModified() {
			return lastModified;
		}

		public void setLastModified(Date lastModified) {
			this.lastModified = lastModified;
		}

		public String getContentType() {
			return contentType;
		}

		public void setContentType(String contentType) {
			this.contentType = contentType;
		}
	}

	@JsonPropertyOrder({ "owner", "manager", "read", "write" })
	public static class PermissionBean {
		String owner;
		List<String> manage;
		List<String> read;
		List<String> write;

		public String getOwner() {
			return owner;
		}

		public void setOwner(String owner) {
			this.owner = owner;
		}

		public List<String> getManage() {
			return manage;
		}

		public void setManage(List<String> manager) {
			manage = manager;
		}

		public List<String> getRead() {
			return read;
		}

		public void setRead(List<String> read) {
			this.read = read;
		}

		public List<String> getWrite() {
			return write;
		}

		public void setWrite(List<String> write) {
			this.write = write;
		}

	}

	@JsonPropertyOrder({ "bitstreamid", "contentType", "filesize" })
	public static class BitstreamBean {
		String bitstreamid;

		@JsonProperty("content-type")
		String contentType;

		long filesize;

		String checksum;

		@JsonProperty("checksum-algorithm")
		String checksumAlgorithm;

		@JsonProperty("last-modified")
		Date lastModified;

		Date created;

		public String getBitstreamid() {
			return bitstreamid;
		}

		public void setBitstreamid(String bitstreamid) {
			this.bitstreamid = bitstreamid;
		}

		public String getContentType() {
			return contentType;
		}

		public void setContentType(String contentType) {
			this.contentType = contentType;
		}

		public long getFilesize() {
			return filesize;
		}

		public void setFilesize(long filesize) {
			this.filesize = filesize;
		}

		public String getChecksum() {
			return checksum;
		}

		public void setChecksum(String checksum) {
			this.checksum = checksum;
		}

		public String getChecksumAlgorithm() {
			return checksumAlgorithm;
		}

		public void setChecksumAlgorithm(String checksumAlgorithm) {
			this.checksumAlgorithm = checksumAlgorithm;
		}

		public Date getLastModified() {
			return lastModified;
		}

		public void setLastModified(Date lastModified) {
			this.lastModified = lastModified;
		}

		public Date getCreated() {
			return created;
		}

		public void setCreated(Date created) {
			this.created = created;
		}
	}

}
