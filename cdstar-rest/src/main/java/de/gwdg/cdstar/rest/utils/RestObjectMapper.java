package de.gwdg.cdstar.rest.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

public class RestObjectMapper {
	public static ObjectMapper json = new ObjectMapper();
	static {
		json.configure(SerializationFeature.INDENT_OUTPUT, true);
		json.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
		json.registerModule(new JavaTimeModule());
	}

}
