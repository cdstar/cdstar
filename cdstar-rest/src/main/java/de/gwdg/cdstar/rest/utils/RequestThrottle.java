package de.gwdg.cdstar.rest.utils;

import java.util.Deque;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.function.Function;

import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.rest.api.AsyncContext;
import de.gwdg.cdstar.rest.api.AsyncRequestHandler;
import de.gwdg.cdstar.rest.api.RequestHandler;
import de.gwdg.cdstar.rest.api.RestContext;

/**
 * Limit the resources consumed by async requests, by pausing new requests in
 * high-load situations and only handling requests that fit a given resource
 * constraint.
 *
 * Each request registered to this class is associated with an estimate cost and
 * a continuation handler. The handler is called as soon as enough resources are
 * available to run the request, and is expected to serve and eventually close
 * the {@link AsyncContext}.
 */
public class RequestThrottle {

	long used;
	long max;

	Deque<DelayedRequest> waitQueue = new ConcurrentLinkedDeque<>();

	private static class DelayedRequest {
		final AsyncContext ctx;
		final long cost;
		final AsyncRequestHandler handler;

		public DelayedRequest(AsyncContext ctx, long cost, AsyncRequestHandler handler) {
			this.ctx = ctx;
			this.cost = cost;
			this.handler = handler;
		}
	}

	/**
	 * Create a new circuit breaker.
	 *
	 * @param name    Circuit breaker name. Only used for logging.
	 * @param maxCost Maximum cost for a single request, and for the sum of all
	 *                currently running requests.
	 */
	public RequestThrottle(long maxCost) {
		max = maxCost;
	}

	/**
	 * Pause this {@link AsyncContext} until enough resources are available to fit
	 * the estimated costs, and then call the {@link AsyncRequestHandler} in a
	 * separate thread. The handler must serve and later close the
	 * {@link AsyncContext} to free up the resources bound by this request.
	 */

	public void handleThrottled(AsyncContext ctx, long cost, AsyncRequestHandler handler) {
		if (cost <= 0)
			throw new IllegalArgumentException("Cost must be positive");

		if (cost > max)
			cost = max;

		if (!tryRunNow(ctx, cost, handler)) {
			final DelayedRequest sl = new DelayedRequest(ctx, cost, handler);
			waitQueue.addLast(sl);
			tryWakeup();
		}
	}

	public void handleThrottled(RestContext ctx, long cost, AsyncRequestHandler handler) {
		handleThrottled(ctx.startAsync(), cost, handler);
	}

	/**
	 * Creates a {@link RequestHandler} that calls
	 * {@link #handleThrottled(RestContext, long, AsyncRequestHandler)} with the
	 * calculated cost.
	 */
	public RequestHandler makeWrapper(AsyncRequestHandler handler, Function<RestContext, Long> costFunction) {
		return new RequestHandler() {
			@Override
			public Object handle(RestContext ctx) throws Exception {
				handleThrottled(ctx, costFunction.apply(ctx), handler);
				return null;
			}
		};
	}

	public RequestHandler makeWrapper(AsyncRequestHandler handler, long fixedCost) {
		return makeWrapper(handler, ctx -> fixedCost);
	}

	private synchronized boolean tryAllocate(long cost) {
		if (max - used >= cost) {
			used += cost;
			return true;
		}
		return false;
	}

	private synchronized void free(long cost) {
		used -= cost;
	}

	/**
	 * Try allocate enough tokens and immediately run the handler. Return true on
	 * success (or if the request was closed and the handler call was skipped), or
	 * false if not enough tokens were available.
	 */
	private boolean tryRunNow(AsyncContext ctx, long cost, AsyncRequestHandler handler) {

		if (!tryAllocate(cost))
			return false;

		try {
			ctx.addCloseListener((ac, error) -> {
				free(cost);
				tryWakeup();
			});
		} catch (final IllegalStateException e) {
			if (ctx.getRequest().isClosed()) {
				free(cost);
				return true;
			}
			throw e;
		}

		ctx.getRequest().runInPool(() -> {
			try {
				handler.handle(ctx);
			} catch (final Exception e) {
				// this triggers the closeListener with the free(cost) call registered above.
				ctx.getRequest().abort(e);
			}
		});

		return true;
	}

	/**
	 * Try to wake-up and run as many sleeping task as possible.
	 */
	private synchronized void tryWakeup() {
		for (DelayedRequest next = null; (next = waitQueue.peek()) != null;) {
			if (tryRunNow(next.ctx, next.cost, next.handler))
				Utils.assertTrue(next == waitQueue.poll(), "");
			else
				break;
		}
	}

	public long getSlots() {
		return max;
	}

	public long getSlotsUsed() {
		return used;
	}

}
