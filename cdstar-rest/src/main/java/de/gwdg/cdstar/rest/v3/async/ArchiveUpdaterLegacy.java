package de.gwdg.cdstar.rest.v3.async;

import java.nio.ByteBuffer;
import java.nio.channels.WritableByteChannel;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.rest.api.AsyncContext;
import de.gwdg.cdstar.rest.api.RestContext;
import de.gwdg.cdstar.rest.utils.form.FormParser;
import de.gwdg.cdstar.rest.utils.form.FormPart;
import de.gwdg.cdstar.rest.v2.model.ApiErrorResponse;
import de.gwdg.cdstar.runtime.client.CDStarFile;

/**
 * This class has a rather complex process flow.
 *
 * It starts with start(), which asks the client for data and registers onRead()
 * as a callback.
 *
 * onRead() parses multi-part parts from a chunk of client data and either
 * schedules itself to wait for more data, or calls handlePart().
 *
 * handlePart() consumes a single multi-part part and returns either DONE or
 * ASYNC. If DONE is returned, the next part is scheduled. If ASYNC is called,
 * then some other mechanism MUST call reschedule() some time in the future.
 *
 * reschedule() checks if more parts are available. If yes, it calls
 * handlePart() in a separate thread. If no, it asks the client for more data,
 * which results in onRead() to be called.
 *
 */
public class ArchiveUpdaterLegacy {
	private AsyncContext ac;
	protected final FormParser parser;
	protected final RestContext ctx;
	private final CDStarFile file;
	private final CompletableFuture<Void> future;
	private WritableByteChannel channel;

	public ArchiveUpdaterLegacy(RestContext ctx, FormParser parser, CDStarFile file) {
		this.ctx = ctx;
		this.file = file;
		this.parser = parser;
		future = new CompletableFuture<>();
	}

	public CompletableFuture<Void> dispatch() {
		Utils.assertTrue(ac == null, "Already started");
		ac = ctx.startAsync();
		ac.asyncRead(ac.getBuffer(), this::onRead, future::completeExceptionally);
		return future;
	}

	private void onRead(ByteBuffer buffer) throws Exception {
		final List<FormPart> parts = new ArrayList<>(1);

		if (buffer.hasRemaining())
			parts.addAll(parser.parse(buffer));

		if (ac.endOfStream())
			parts.addAll(parser.finish());

		for (final FormPart part : parts) {
			if (handlePart(part)) {
				future.complete(null);
				return; // success
			}
		}

		if (ac.endOfStream()) {
			ac.recycleBuffer(buffer);
			throw new ApiErrorResponse(400, "parameter", "Missing parameter: bitstream");
		} else {
			buffer.clear();
			ac.asyncRead(buffer, this::onRead, future::completeExceptionally);
		}
	}

	private boolean handlePart(FormPart part) throws Exception {
		if (part.getName() == null)
			throw new ApiErrorResponse(400, "parameter", "Form upload witout field name.");

		if (!part.getName().equals("bitstreams"))
			throw new ApiErrorResponse(400, "parameter", "Unsupported form parameter: " + part.getName());

		if (channel == null) {
			file.setMediaType(part.getContentType());
			file.truncate(0);
			channel = file.getWriteChannel();
		}

		final ByteBuffer buffer = part.drain();
		while (buffer.hasRemaining())
			channel.write(buffer);

		if (part.isComplete())
			channel.close();

		return part.isComplete();
	}

}