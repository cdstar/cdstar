package de.gwdg.cdstar.rest.servlet;

import java.io.IOException;
import java.net.URI;

import de.gwdg.cdstar.rest.RestContextBase;
import de.gwdg.cdstar.rest.api.AsyncContext;
import de.gwdg.cdstar.rest.api.RestContext;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public class ServletRestContext extends RestContextBase {
	private final HttpServletRequest rq;
	private final HttpServletResponse rs;
	private AsyncContextImpl async;

	public ServletRestContext(HttpServletRequest rq, HttpServletResponse rs) {
		super();
		this.rq = rq;
		this.rs = rs;
		rq.setAttribute(getClass().getName(), this);
	}

	@Override
	public long getContentLength() {
		return rq.getContentLengthLong();
	}

	@Override
	public String getPath() {
		return rq.getPathInfo();
	}

	@Override
	public String getHeader(String name) {
		return rq.getHeader(name);
	}

	@Override
	public RestContext status(int status) {
		rs.setStatus(status);
		return this;
	}

	@Override
	public RestContext header(String name, String value) {
		rs.setHeader(name, value);
		return this;
	}

	@Override
	public String header(String name) {
		return rs.getHeader(name);
	}

	@Override
	public String getMethod() {
		return rq.getMethod();
	}

	@Override
	public String getQuery() {
		return rq.getQueryString();
	}

	@Override
	public int read(byte[] buffer, int off, int len) throws IOException {
		if (len > 0)
			return rq.getInputStream().read(buffer, off, len);
		return 0;
	}

	@Override
	public void write(byte[] buffer, int off, int len) throws IOException {
		if (len > 0)
			rs.getOutputStream().write(buffer, off, len);
	}

	@Override
	public boolean isCommitted() {
		return isClosed() || rs.isCommitted();
	}

	@Override
	public synchronized AsyncContext startAsync() {
		if (async != null)
			throw new IllegalStateException("Async already started");
		async = new AsyncContextImpl(this, rq);
		return async;
	}

	@Override
	public boolean isAsync() {
		return async != null;
	}

	@Override
	public void close() {
		super.close();
		if (isAsync()) {
			async.consumeThenClose();
			async = null;
		}
	}

	@Override
	public String resolvePath(String path, boolean keepLastSegment) {
		String totalPath = rq.getRequestURI();
		if (keepLastSegment && !totalPath.endsWith("/"))
			totalPath += "/";
		return URI.create(totalPath).resolve(path).toString();
	}

}
