package de.gwdg.cdstar.rest.v3.async;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SeekableByteChannel;
import java.nio.channels.WritableByteChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.List;
import java.util.concurrent.CancellationException;
import java.util.concurrent.Future;

import de.gwdg.cdstar.Promise;
import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.pool.BackendError;
import de.gwdg.cdstar.rest.api.AsyncContext;
import de.gwdg.cdstar.rest.api.RestContext;
import de.gwdg.cdstar.rest.utils.GlobPattern;
import de.gwdg.cdstar.rest.utils.IncludeExcludeFilter;
import de.gwdg.cdstar.rest.utils.QueryHelper;
import de.gwdg.cdstar.rest.v3.ingest.ImportTarget;
import de.gwdg.cdstar.rest.v3.ingest.TarImporter;
import de.gwdg.cdstar.rest.v3.ingest.ZipImporter;
import de.gwdg.cdstar.runtime.client.CDStarArchive;
import de.gwdg.cdstar.runtime.client.CDStarFile;
import de.gwdg.cdstar.runtime.client.exc.FileExists;
import de.gwdg.cdstar.runtime.client.exc.InvalidFileName;
import de.gwdg.cdstar.web.common.model.ArchiveUpdated;
import de.gwdg.cdstar.web.common.model.ErrorResponse;

/**
 * Import files from any of the supported import formats (e.g.
 * {@link TarImporter} or {@link ZipImporter}) into an existing archive.
 */
public class ArchiveImporter {

	protected final RestContext ctx;
	private long readLimit;
	private Path tmp;
	private SeekableByteChannel out;
	private int tempSize;

	private final CDStarArchive archive;
	private AsyncContext ac;
	private final Promise<ArchiveUpdated> promise;
	private String prefix;
	ReportBuilder report;
	private final IncludeExcludeFilter<String> includeFilter;

	public class ImportJob implements ImportTarget {
		CDStarFile currentFile;
		WritableByteChannel currentChannel;

		@Override
		public boolean newFile(String name, String mimeType) {
			if (promise.isCompleted())
				return false;

			name = normalizeName(name);

			if (!includeFilter.test(name))
				return false;

			try {
				Utils.closeQuietly(currentChannel);

				currentFile = archive.createFile(name);
				report.rememberFile(currentFile);
				if (mimeType != null)
					currentFile.setMediaType(mimeType);
				currentChannel = currentFile.getWriteChannel();

				ac.keepAlive();

				return true;
			} catch (final FileExists e) {
				throw new ErrorResponse(400, "FileExists", "File with this name").detail("name", name);
			} catch (final InvalidFileName e) {
				throw new ErrorResponse(400, "FileInvalid", "File name not valid").detail("name", name);
			} catch (final IOException e) {
				throw new BackendError(e);
			}
		}

		@Override
		public void writeFile(ByteBuffer data) throws IOException {
			if (promise.isCompleted())
				throw new IOException("Copy operation was interrupted");
			currentChannel.write(data);
		}

		@Override
		public void setMeta(String name, String value) {
			currentFile.setAttribute(name, value);
		}

		@Override
		public void close() {
			Utils.closeQuietly(currentChannel);
			promise.resolve(report.build(
					new ArchiveUpdated(archive.getVault().getName(), archive.getId(), archive.getNextRev())));
		}

	}

	public ArchiveImporter(RestContext ctx, CDStarArchive archive, long readLimit) {
		this.ctx = ctx;
		this.archive = archive;
		report = new ReportBuilder();
		promise = Promise.empty();

		if (readLimit == -1) {
			this.readLimit = ctx.getContentLength();
		} else if (ctx.getContentLength() == -1) {
			this.readLimit = readLimit;
		} else {
			this.readLimit = Math.min(readLimit, ctx.getContentLength());
		}

		final QueryHelper q = new QueryHelper(ctx);
		q.setDefault("prefix", "/");
		prefix = q.get("prefix");
		final List<GlobPattern> include = q.getFiltered("include", (s) -> new GlobPattern(s));
		final List<GlobPattern> exclude = q.getFiltered("exclude", (s) -> new GlobPattern(s));
		includeFilter = new IncludeExcludeFilter<>(include, exclude);
		q.ensureNoUnusedParameters();

		while (prefix.startsWith(CDStarFile.PATH_SEP))
			prefix = prefix.substring(1);
		if (!prefix.isEmpty() && !prefix.endsWith("/"))
			prefix = prefix + "/";

		promise.then((resul, error) -> {
			Utils.closeQuietly(out);
			Utils.deleteQuietly(tmp);
		});

		try {
			tmp = Files.createTempFile("cdstar-upload", ".zip");
			out = Files.newByteChannel(tmp, StandardOpenOption.WRITE);
		} catch (final IOException e) {
			onError(e);
		}
	}

	private String normalizeName(String name) {
		// Silently remove common unwanted prefixes found in archives.
		// Any other strange stuff ( e.g. /../ in path) is filtered out downstream
		if (name.startsWith("./"))
			name = name.substring(2);
		if (name.startsWith("/"))
			name = name.substring(1);
		name = prefix + name;
		return name;
	}

	public Promise<ArchiveUpdated> dispatch() {
		Utils.assertTrue(ac == null, "Already started");
		ac = ctx.startAsync();
		ac.asyncRead(ac.getBuffer(), this::onRead, this::onError, null);
		return promise;
	}

	private final void onRead(ByteBuffer buffer) throws Exception {
		// Bail out if promise was canceled from the outside.
		if (promise.isCompleted())
			return;

		if (buffer.hasRemaining()) {
			tempSize += out.write(buffer);
			if (readLimit > -1 && tempSize > readLimit) {
				onError(new IOException("File upload limit reached: " + readLimit));
			}
		}

		if (ac.endOfStream()) {
			ac.recycleBuffer(buffer);
			onAllBytesRecieved();
		} else {
			buffer.clear();
			ac.asyncRead(buffer, this::onRead, this::onError);
		}
	}

	private synchronized void onError(Throwable err) {
		promise.tryReject(err);
	}

	/**
	 * Start import job.
	 */
	private void onAllBytesRecieved() {
		final String ctype = ctx.getContentType();
		final ImportJob job = new ImportJob();

		Runnable task;

		if (TarImporter.isTypeSupported(ctype)) {
			task = new TarImporter(job, tmp, ctx.getHeader("Content-Encoding"));
		} else if (ZipImporter.isTypeSupported(ctype)) {
			task = new ZipImporter(job, tmp);
		} else {
			throw new ErrorResponse(415, "UnsupportedImportFormat", "The supplied import format is not supported.");
		}

		final Future<?> future = ctx.runInPool(() -> {
			try {
				task.run();
			} catch (final Exception e) {
				onError(e);
			}
		});
		promise.then(null, e -> {
			if (e instanceof CancellationException)
				future.cancel(true);
		});

	}

	public static boolean isSupported(String contentType) {
		return TarImporter.isTypeSupported(contentType) || ZipImporter.isTypeSupported(contentType);
	}

}
