package de.gwdg.cdstar.rest.v2;

import java.io.IOException;
import java.io.InputStream;
import java.nio.channels.Channels;
import java.nio.channels.WritableByteChannel;
import java.util.Date;

import com.fasterxml.jackson.databind.node.ObjectNode;

import de.gwdg.cdstar.Promise;
import de.gwdg.cdstar.SharedObjectMapper;
import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.rest.api.Blueprint;
import de.gwdg.cdstar.rest.api.RestBlueprint;
import de.gwdg.cdstar.rest.api.RestConfig;
import de.gwdg.cdstar.rest.api.RestContext;
import de.gwdg.cdstar.rest.utils.RangeHeader;
import de.gwdg.cdstar.rest.utils.RestUtils;
import de.gwdg.cdstar.rest.utils.SessionHelper;
import de.gwdg.cdstar.rest.v2.model.ApiErrorResponse;
import de.gwdg.cdstar.rest.v2.utils.LegacyHelper;
import de.gwdg.cdstar.rest.v3.async.AsyncDownload;
import de.gwdg.cdstar.rest.v3.async.AsyncUpload;
import de.gwdg.cdstar.runtime.Plugin;
import de.gwdg.cdstar.runtime.client.CDStarArchive;
import de.gwdg.cdstar.runtime.client.CDStarFile;
import de.gwdg.cdstar.runtime.client.CDStarSession;
import de.gwdg.cdstar.runtime.client.exc.ArchiveNotFound;
import de.gwdg.cdstar.runtime.client.exc.FileNotFound;
import de.gwdg.cdstar.runtime.client.exc.InvalidFileName;
import de.gwdg.cdstar.runtime.client.exc.VaultNotFound;
import de.gwdg.cdstar.ta.exc.TARollbackException;

// TODO: Make this a plugin
@Plugin(name = "dariah-api")
@Blueprint(path = "/dariah", name = "dariah-api")
public class DariahBlueprint implements RestBlueprint {

	private static final String DARIAH_CONTENT_NAME = "data.bin";

	@Override
	public void configure(RestConfig cfg) {
		cfg.route("/<vault>").POST(this::newObject);
		cfg.route("/<vault>/<uid>").GET(this::getObject).PUT(this::putObject).DELETE(this::deleteObject);
	}

	public Void newObject(RestContext ctx) throws VaultNotFound, IOException {
		final String ctype = ctx.getContentType();

		if (ctx.isMultipart()) {
			throw new ApiErrorResponse(501, "no paos", "PAOS support is currently not implemented");
		}

		final String pid = ctx.getHeader("PID");
		final String version = ctx.getHeader("Version");

		final ObjectNode meta = SharedObjectMapper.json.createObjectNode();
		if (Utils.notNullOrEmpty(pid))
			meta.put("PID", pid);
		if (Utils.notNullOrEmpty(version))
			meta.put("Version", version);

		final CDStarSession session = SessionHelper.getCDStarSession(ctx, false);
		final CDStarArchive ar = session.getVault(ctx.getPathParam("vault")).createArchive();
		MetadataResource.putMetadata(ar, meta);
		final CDStarFile file = ar.createFile(DARIAH_CONTENT_NAME, ctype.toString());
		ar.setProperty(ApiV2Module.v2TypeKey, "object");

		final WritableByteChannel writeChannel = file.getWriteChannel();

		Promise.wrap(new AsyncUpload(ctx, writeChannel).dispatch())
				.then(x -> {
					try {
						writeChannel.close();
						session.commit();
						ctx.status(201);
						ctx.header("Location", ctx.resolvePath(ar.getId(), true));
						ctx.header("Last-Modified", ar.getContentModified());
						ctx.close();
					} catch (final Exception e) {
						ctx.abort(e);
					}
				}, ctx::abort);

		return null;
	}

	public Void putObject(RestContext ctx) throws ArchiveNotFound, VaultNotFound, IOException {
		final String ctype = ctx.getContentType();

		if (ctx.isMultipart()) {
			throw new ApiErrorResponse(501, "no paos", "PAOS support is currently not implemented");
		}

		final String pid = ctx.getHeader("PID");
		final String version = ctx.getHeader("Version");

		final ObjectNode meta = SharedObjectMapper.json.createObjectNode();
		if (Utils.notNullOrEmpty(pid))
			meta.put("PID", pid);
		if (Utils.notNullOrEmpty(version))
			meta.put("Version", version);

		final CDStarSession session = SessionHelper.getCDStarSession(ctx, false);
		final CDStarArchive ar = session.getVault(ctx.getPathParam("vault"))
			.loadArchive(LegacyHelper.fromAnyID(ctx.getPathParam("uid")));
		MetadataResource.putMetadata(ar, meta);
		final CDStarFile file = ar.getFile(DARIAH_CONTENT_NAME);
		file.setMediaType(ctype);
		final WritableByteChannel writeChannel = file.truncate(0).getWriteChannel();

		Promise.wrap(new AsyncUpload(ctx, writeChannel).dispatch())
				.then(x -> {
					try {
						writeChannel.close();
						session.commit();
						ctx.status(201);
						ctx.header("Location", ctx.resolvePath(ar.getId(), true));
						ctx.header("Last-Modified", ar.getContentModified());
						ctx.close();
					} catch (final Exception e) {
						ctx.abort(e);
					}
				}, ctx::abort);

		return null;

	}

	public Void getObject(RestContext ctx) throws ArchiveNotFound, VaultNotFound, IOException {
		final CDStarSession session = SessionHelper.getCDStarSession(ctx, true);
		final CDStarArchive ar = session.getVault(ctx.getPathParam("vault"))
			.loadArchive(LegacyHelper.fromAnyID(ctx.getPathParam("uid")));

		CDStarFile file;
		try {
			file = ar.getFile(DARIAH_CONTENT_NAME);
		} catch (final FileNotFound e) {
			throw new ApiErrorResponse(404, "not-found", "No such file");
		}

		final String etag = "\"" + file.getDigests().get(CDStarFile.DIGEST_SHA256) + "\"";
		final Date lastModified = file.getLastModified();

		if (!RestUtils.checkConditional(ctx, lastModified, etag))
			return null;

		final RangeHeader range = RestUtils.prepareRangeRequest(ctx, lastModified, etag, file.getSize());
		if (range.isZeroLength())
			return null;

		ctx.header("Content-Type", file.getMediaType());
		ctx.header("Content-Length", range.getRangeLength());

		if (ctx.getMethod().equals("HEAD")) {
			return null;
		}

		final InputStream io = Channels.newInputStream(file.getReadChannel(range.getStartIndex()));
		new AsyncDownload(ctx, io, 0, range.getRangeLength()).dispatch();

		return null;
	}

	public Void deleteObject(RestContext ctx)
		throws InvalidFileName, FileNotFound, TARollbackException, ArchiveNotFound, VaultNotFound {
		final CDStarSession session = SessionHelper.getCDStarSession(ctx, false);
		session.getVault(ctx.getPathParam("vault")).loadArchive(LegacyHelper.fromAnyID(ctx.getPathParam("uid")))
			.remove();
		session.commit();
		ctx.status(204);
		return null;
	}

}
