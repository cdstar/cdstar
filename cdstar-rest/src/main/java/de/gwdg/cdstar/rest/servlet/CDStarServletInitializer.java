package de.gwdg.cdstar.rest.servlet;

import java.util.Arrays;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.Set;

import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.rest.utils.SessionHelper;
import de.gwdg.cdstar.runtime.RuntimeContext;
import jakarta.servlet.DispatcherType;
import jakarta.servlet.FilterRegistration;
import jakarta.servlet.ServletContainerInitializer;
import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRegistration;
import jakarta.servlet.annotation.WebListener;

@WebListener
public class CDStarServletInitializer implements ServletContainerInitializer {

	private static final String CDSTAR_SERVLET_NAME = "CDSTAR: Servlet";
	private RuntimeContext runtime;
	public static final String INI_CDSTAR_CONFIG = "cdstar.config";
	public static final String INI_CDSTAR_RUNTIME = "cdstar.runtime";

	public CDStarServletInitializer() {
	}

	public CDStarServletInitializer(RuntimeContext runtime) {
		Utils.notNull(runtime);
		this.runtime = runtime;
	}

	@Override
	public void onStartup(Set<Class<?>> c, ServletContext ctx) throws ServletException {
		if (runtime == null)
			throw new ServletException("WAR deployment currently not supported");

		ctx.setAttribute(INI_CDSTAR_RUNTIME, runtime);

		final ServletRegistration.Dynamic servlet = ctx.addServlet(
			CDSTAR_SERVLET_NAME, new ApiRootServlet(runtime));
		servlet.addMapping("/*");
		servlet.setAsyncSupported(true);

		setupMethodOverride(ctx);
		setupCORS(ctx);
	}

	private void setupMethodOverride(ServletContext ctx) {
		FilterRegistration.Dynamic conf = ctx.addFilter("CDSTAR: Method Override Filter", MethodOverrideFilter.class);
		conf.setAsyncSupported(true);
		conf.addMappingForServletNames(EnumSet.of(DispatcherType.REQUEST), true, CDSTAR_SERVLET_NAME);
	}

	private void setupCORS(ServletContext ctx) {
		Set<String> allow = new HashSet<>();
		Set<String> expose = new HashSet<>();

		// Default headers
		allow.addAll(Arrays.asList("Cache-Control", "Content-Type", "Authorization"));
		allow.add(SessionHelper.HEADER_TRANSACTION);
		allow.add(MethodOverrideFilter.HTTP_OVERRIDE_HEADER);
		expose.add("Location");

		// TUS headers (make this configurable)
		allow.addAll(Arrays.asList("Upload-Offset", "Upload-Length", "Tus-Resumable", "Upload-Metadata"));
		expose.addAll(Arrays.asList("Upload-Offset", "Upload-Length", "Tus-Version", "Tus-Resumable", "Tus-Max-Size", "Tus-Extension", "Upload-Metadata"));

		// Add CORS filter
		FilterRegistration.Dynamic conf = ctx.addFilter("CDSTAR: CORS  Filter", CORSFilter.class);
		conf.setAsyncSupported(true);
		conf.addMappingForServletNames(EnumSet.of(DispatcherType.REQUEST), true, CDSTAR_SERVLET_NAME);
		conf.setInitParameter(CORSFilter.ALLOWED_ORIGINS_PARAM, "*");
		conf.setInitParameter(CORSFilter.ALLOWED_HEADERS_PARAM, String.join(",", allow));
		conf.setInitParameter(CORSFilter.EXPOSED_HEADERS_PARAM, String.join(",", expose));
		conf.setInitParameter(CORSFilter.ALLOWED_METHODS_PARAM, "HEAD,GET,POST,PUT,DELETE,PATCH");
		conf.setInitParameter(CORSFilter.PREFLIGHT_MAX_AGE_PARAM, "3600");
	}

}
