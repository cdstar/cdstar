package de.gwdg.cdstar.rest.testutils;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import de.gwdg.cdstar.Utils;

public class TestRequest {

	private final TestClient client;
	private String method = "GET";
	private String path = "/";
	private String queryString;
	private final Map<String, String> headers = new HashMap<>();
	private InputStream entity;

	public TestRequest(TestClient client) {
		this.client = client;
		headers.putAll(client.headers);
	}

	public TestRequest path(String path) {
		if (path.contains("?")) {
			final String[] parts = path.split("\\?", 1);
			this.path = parts[0];
			queryString = parts[1];
		} else {
			this.path = path;
		}
		return this;
	}

	public TestRequest method(String method) {
		this.method = method.toUpperCase();
		return this;
	}

	public TestRequest query(String name, String value) {
		final String pair = name + "=" + value;
		queryString = queryString == null ? pair : "&" + pair;
		return this;
	}

	public TestRequest entity(String contentType, InputStream entity) {
		if (contentType != null)
			header("content-type", contentType);
		this.entity = entity;
		return this;
	}

	public TestRequest entity(String contentType, byte[] entity) {
		if (contentType != null)
			header("content-type", contentType);
		header("content-length", Integer.toString(entity.length));
		this.entity = new ByteArrayInputStream(entity);
		return this;
	}

	public TestRequest header(String name, Object value) {
		headers.put(name.toLowerCase(), value.toString());
		return this;
	}

	public TestClient getClient() {
		return client;
	}

	public synchronized TestResponse submit() {
		final TestResponse rs = client.dispatch(this);
		try {
			rs.waitAsync();
		} catch (final InterruptedException e) {
			throw Utils.toUnchecked(e);
		}
		return rs;
	}

	public String getMethod() {
		return method;
	}

	public String getPath() {
		return path;
	}

	public String getQueryString() {
		return queryString;
	}

	public Map<String, String> getHeaders() {
		return headers;
	}

	public InputStream getEntity() {
		return entity;
	}

	int read(byte[] buffer, int off, int len) throws IOException {
		if (entity == null)
			return -1;
		return entity.read(buffer, off, len);
	}

}
