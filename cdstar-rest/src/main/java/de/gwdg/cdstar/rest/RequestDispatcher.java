package de.gwdg.cdstar.rest;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import de.gwdg.cdstar.rest.api.HttpError;
import de.gwdg.cdstar.rest.api.HttpError.MethodNotAllowed;
import de.gwdg.cdstar.rest.api.RequestHandler;
import de.gwdg.cdstar.rest.api.RestConfig;
import de.gwdg.cdstar.rest.api.RestContext;
import de.gwdg.cdstar.rest.api.RestRoute;

public class RequestDispatcher {

	private final RestConfig rootConfig;
	private RouteMatchingTree matchTree;

	public RequestDispatcher(RestConfig root) {
		rootConfig = root;
		reset();
	}

	private static class MatchResult {
		private final RestConfig config;
		private final RestRoute route;
		private final Map<String, String> urlParams;

		MatchResult(RestConfig config, RestRoute route, Map<String, String> urlParams) {
			super();
			this.config = config;
			this.route = route;
			this.urlParams = urlParams;
		}

	}

	private static class RouteMatchingTree {

		private final RestConfig conf;
		private final String prefix;
		private final List<RouteTreeNode> routes;
		private final List<RouteMatchingTree> children;

		public RouteMatchingTree(RestConfig conf) {
			this.conf = conf;
			prefix = conf.getPrefix();

			routes = new ArrayList<>(conf.getRoutes().size());
			conf.getRoutes().forEach(r -> routes.add(new RouteTreeNode(r)));
			routes.sort(null);

			children = new ArrayList<>(conf.getChildren().size());
			conf.getChildren().forEach(c -> children.add(new RouteMatchingTree(c)));
		}

		private MatchResult match(String path) {
			if (!prefix.isEmpty()) {
				if (!path.startsWith(prefix))
					return null;
				path = path.substring(prefix.length());
			}

			for (final RouteTreeNode node : routes) {
				Map<String, String> params;
				if ((params = node.template.match(path)) != null) {
					return new MatchResult(conf, node.route, params);
				}
			}

			for (final RouteMatchingTree child : children) {
				MatchResult result;
				if ((result = child.match(path)) != null)
					return result;
			}
			return null;
		}
	}

	private static class RouteTreeNode implements Comparable<RouteTreeNode> {
		private final RestRoute route;
		private final RouteTemplate template;

		RouteTreeNode(RestRoute route) {
			this.route = route;
			template = new RouteTemplate(route.getRule(), true);
		}

		@Override
		public int compareTo(RouteTreeNode o) {
			return route.compareTo(o.route);
		}
	}

	public void reset() {
		matchTree = new RouteMatchingTree(rootConfig);
	}

	public void dispatch(RestContext ctx) {
		final MatchResult match = matchTree.match(ctx.getPath());
		if (match == null) {
			ctx.setAttribute(RestContext.ATTR_MATCH_CONF, rootConfig);
			ctx.abort(new HttpError.NotFound());
			return;
		}

		ctx.setAttribute(RestContext.ATTR_MATCH_CONF, match.config);
		ctx.setAttribute(RestContext.ATTR_MATCH_PARAMS, match.urlParams);
		ctx.setAttribute(RestContext.ATTR_MATCH_ROUTE, match.route);

		final String method = ctx.getMethod();
		final Map<String, RequestHandler> targets = match.route.getTargets();

		RequestHandler target = targets.get(method);
		if (target == null && method.equals("HEAD"))
			target = targets.get("GET");
		if (target == null)
			target = targets.get("*");
		if (target == null) {
			final MethodNotAllowed err = new HttpError.MethodNotAllowed(targets.keySet());
			ctx.header("Allow", err.getAllowedHeader());
			ctx.abort(err);
			return;
		}
		ctx.setAttribute(RestContext.ATTR_MATCH_HANDLER, target);

		try {
			final Object result = target.handle(ctx);
			if (result != null)
				ctx.write(result);
		} catch (final Exception e) {
			ctx.abort(e);
		} finally {
			if (!ctx.isAsync() && !ctx.isClosed())
				ctx.close();
		}

	}
}
