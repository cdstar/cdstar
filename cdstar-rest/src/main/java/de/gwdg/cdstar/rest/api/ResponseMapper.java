package de.gwdg.cdstar.rest.api;

public interface ResponseMapper<T extends Object> {

	default boolean canHandle(String contentType, Class<T> type) {
		return true;
	}

	void handle(RestContext ctx, T value) throws Exception;
}
