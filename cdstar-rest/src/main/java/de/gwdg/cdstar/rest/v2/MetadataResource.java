package de.gwdg.cdstar.rest.v2;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.WritableByteChannel;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import de.gwdg.cdstar.SharedObjectMapper;
import de.gwdg.cdstar.rest.api.RestConfig;
import de.gwdg.cdstar.rest.api.RestContext;
import de.gwdg.cdstar.rest.api.RestBlueprint;
import de.gwdg.cdstar.rest.utils.RestUtils;
import de.gwdg.cdstar.rest.utils.SessionHelper;
import de.gwdg.cdstar.rest.v2.model.ApiErrorResponse;
import de.gwdg.cdstar.rest.v2.model.Success;
import de.gwdg.cdstar.rest.v2.model.Success.MetadataUpdated;
import de.gwdg.cdstar.rest.v2.utils.LegacyHelper;
import de.gwdg.cdstar.runtime.client.CDStarArchive;
import de.gwdg.cdstar.runtime.client.CDStarFile;
import de.gwdg.cdstar.runtime.client.CDStarSession;
import de.gwdg.cdstar.runtime.client.exc.ArchiveNotFound;
import de.gwdg.cdstar.runtime.client.exc.FileNotFound;
import de.gwdg.cdstar.runtime.client.exc.VaultNotFound;
import de.gwdg.cdstar.ta.exc.TARollbackException;

public class MetadataResource implements RestBlueprint {

	@Override
	public void configure(RestConfig cfg) {
		cfg.route("/<vault>/metadata/<uid>")
			.GET(this::getMetadata)
			.POST(this::createMetadata)
			.PUT(this::updateMetadata)
			.DELETE(this::deleteMetadata);
	}

	public MetadataUpdated updateMetadata(RestContext ctx)
			throws TARollbackException, IOException, ArchiveNotFound, VaultNotFound {
		return createMetadata(ctx);
	}

	static void putMetadata(CDStarArchive ar, ObjectNode doc) throws IOException {
		final CDStarFile m = ar.getOrCreateFile(ApiV2Module.v2metaFile);
		m.truncate(0);
		m.setMediaType("application/json");
		try (WritableByteChannel writeChannel = m.getWriteChannel()) {
			writeChannel.write(ByteBuffer.wrap(SharedObjectMapper.json.writeValueAsBytes(doc)));
		} catch (final JsonProcessingException e) {
			throw new RuntimeException("Jackson cannot serialize its own ObjectNode data type???", e);
		}
	}

	public MetadataUpdated createMetadata(RestContext ctx)
			throws TARollbackException, IOException, ArchiveNotFound, VaultNotFound {
		final CDStarSession session = SessionHelper.getCDStarSession(ctx, false);
		final CDStarArchive ar = session
				.getVault(ctx.getPathParam("vault"))
				.loadArchive(LegacyHelper.fromAnyID(ctx.getPathParam("uid")));
		final JsonNode is = RestUtils.parseJsonBody(ctx, JsonNode.class);

		if (!is.isObject() || !(is instanceof ObjectNode)) {
			throw new ApiErrorResponse(400, "bad-input", "Expected JSON object as input.");
		}
		putMetadata(ar, (ObjectNode) is);
		session.commit();

		ctx.status(201);
		ctx.header("Location", ctx.resolvePath("", true));
		return new Success.MetadataUpdated(LegacyHelper.toLegacyID(ar.getId()));
	}

	public Void getMetadata(RestContext ctx) throws IOException, ArchiveNotFound, VaultNotFound {
		final CDStarSession session = SessionHelper.getCDStarSession(ctx, true);
		final CDStarArchive ar = session.getVault(ctx.getPathParam("vault")).loadArchive(LegacyHelper.fromAnyID(ctx.getPathParam("uid")));
		final CDStarFile m;
		try {
			m = ar.getFile(ApiV2Module.v2metaFile);
		} catch (final FileNotFound e) {
			throw new ApiErrorResponse(404, "NotFound", "The object is not associated with a metadata structure");
		}

		ctx.header("Content-Type", "application/json");
		ctx.write(m.getStream());
		return null;
	}

	public Void deleteMetadata(RestContext ctx)
			throws FileNotFound, TARollbackException, ArchiveNotFound, VaultNotFound {
		final CDStarSession session = SessionHelper.getCDStarSession(ctx, false);
		final CDStarArchive ar = session.getVault(ctx.getPathParam("vault"))
			.loadArchive(LegacyHelper.fromAnyID(ctx.getPathParam("uid")));
		final CDStarFile m = ar.getFile(ApiV2Module.v2metaFile);
		if (m != null) {
			m.remove();
			session.commit();
		}
		ctx.status(204);
		return null;
	}
}
