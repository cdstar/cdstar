package de.gwdg.cdstar.rest.servlet;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.gwdg.cdstar.Utils;
import jakarta.servlet.Filter;
import jakarta.servlet.FilterChain;
import jakarta.servlet.FilterConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletRequestWrapper;

/**
 * Filter to honor X-HTTP-Method-Override headers for POST requests.
 */
public class MethodOverrideFilter implements Filter {

	private static final Logger log = LoggerFactory.getLogger(MethodOverrideFilter.class);

	public static final String HTTP_OVERRIDE_HEADER = "X-HTTP-Method-Override";
	private static final String[] allowedOverrides = { "PATCH", "DELETE" };

	public static final class MethodOverrideRequest extends HttpServletRequestWrapper {
		private final String method;

		public MethodOverrideRequest(HttpServletRequest delegate, String method) {
			super(delegate);
			this.method = method.toUpperCase();
		}

		@Override
		public String getMethod() {
			return method;
		}
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		final HttpServletRequest rq = (HttpServletRequest) request;
		final String override = rq.getHeader(HTTP_OVERRIDE_HEADER);

		if (override != null) {
			if (rq.getMethod().equals("POST") && Utils.arrayContains(allowedOverrides, override)) {
				chain.doFilter(new MethodOverrideRequest(rq, override), response);
				return;
			}

			if (log.isDebugEnabled()) {
				log.debug("Ignored invalid method override (override={}, client={}, method={}, path={})",
					Utils.repr(override), rq.getRemoteAddr(), rq.getMethod(), rq.getPathInfo());
			}
		}

		chain.doFilter(request, response);
	}

	@Override
	public void destroy() {
	}

}
