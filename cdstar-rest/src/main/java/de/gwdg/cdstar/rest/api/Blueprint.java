package de.gwdg.cdstar.rest.api;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation for {@link RestBlueprint}s to control how these blueprints are
 * installed by default (if no explicit configuration is provided).
 */
@Target({ ElementType.TYPE, ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
public @interface Blueprint {
	/**
	 * Path prefix to attach this blueprint to. A value of "/" install all routes
	 * alongside of the parent config, but still into a child configuration. A value
	 * of "" (empty string) or null will prevent the creation of a child
	 * configuration.
	 */
	String path() default "/";

	/**
	 * Name of this blueprint. Defaults to the class name of the annotated class.
	 */
	String name() default "";

	/**
	 * If true (default), inherit response and error mappers as well as services
	 * from the parent config.
	 */
	boolean inherit() default true;
}