package de.gwdg.cdstar.rest.v3;

import de.gwdg.cdstar.rest.api.Blueprint;
import de.gwdg.cdstar.rest.api.RestBlueprint;
import de.gwdg.cdstar.rest.api.RestConfig;
import de.gwdg.cdstar.runtime.Plugin;

@Plugin(name = "cdstar-api-3")
@Blueprint(path = "/v3", name = "cdstar-api-3")
public class ApiV3Module implements RestBlueprint {

	@Override
	public void configure(RestConfig cfg) {
		cfg.install(new ServiceEndpoint());
		cfg.install(new ServiceHealthEndpoint());
		cfg.install(new ServiceMetricsEndpoint());
		cfg.install(new ServiceGCEndpoint());
		cfg.install(new TransactionEndpoint());
		cfg.install(new VaultEndpoint());
		cfg.install(new ArchiveEndpoint());
		cfg.install(new FileEndpoint());
	}

}
