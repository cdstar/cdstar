package de.gwdg.cdstar.rest.v3.ingest;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import de.gwdg.cdstar.MimeUtils;
import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.web.common.model.ErrorResponse;

public class ZipImporter implements Runnable {
	private static final Set<String> mimeTypes = new HashSet<>(
			Arrays.asList("application/zip", "application/zip-compressed", "application/x-zip-compressed"));

	public static boolean isTypeSupported(String contentType) {
		return mimeTypes.contains(contentType);
	}

	private final ImportTarget target;
	private final Path source;

	/**
	 *
	 * @param target
	 *            to import files to.
	 * @param source
	 *            to read from. The stream is closed after calling run().
	 */

	public ZipImporter(ImportTarget target, Path source) {
		this.target = target;
		this.source = source;
	}

	@Override
	public void run() {
		ZipFile zip = null;
		try {
			zip = new ZipFile(source.toFile(), StandardCharsets.UTF_8);
			for (final ZipEntry entry : Utils.iter(zip.entries())) {
				if (entry.isDirectory())
					continue;

				final String name = entry.getName();
				final String type = MimeUtils.guess(name, false).getMimeDefault(MimeUtils.OCTET_STREAM);

				if (!target.newFile(name, type))
					continue;

				try (InputStream data = zip.getInputStream(entry)) {
					final ByteBuffer buffer = ByteBuffer.allocate(1024 * 8);
					int read;
					while (-1 < (read = data.read(buffer.array(), buffer.arrayOffset(), buffer.capacity()))) {
						buffer.clear().limit(read);
						target.writeFile(buffer);
					}
				}
			}
			target.close();
		} catch (final NullPointerException e) {
			// This happens for example if the name of a zip-entry is empty.
			throw new ErrorResponse(400, "InvalidImportFormat", "Invalid zip file content");
		} catch (final IOException e) {
			throw new ErrorResponse(400, "InvalidImportFormat", "Failed to extract archive: "+e.getMessage());
		} catch (final ErrorResponse e) {
			throw e;
		} catch (final Exception e) {
			throw new ErrorResponse(e);
		} finally {
			Utils.closeQuietly(zip);
		}
	}

}
