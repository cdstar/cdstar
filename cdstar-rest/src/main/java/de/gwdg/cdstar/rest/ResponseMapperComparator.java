package de.gwdg.cdstar.rest;

import java.util.Comparator;

import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.rest.api.ResponseMapper;
import de.gwdg.cdstar.rest.api.RestConfig;

/**
 * Order {@link ResponseMapperHolder} instances by their mime-type quality
 * (descending), their class distance to the reference type, and finally the
 * wildcard count in their mime-type.
 *
 * This matches the order defined in
 * {@link RestConfig#mapResponse(String, Class, ResponseMapper)} for any given
 * client accept header.
 *
 * In other words: The best mime-type quality wins. On a draw, the most suitable
 * mapper for a given reference type wins. On a draw, the mapper with fewer
 * wildcards in their mime-type wins. Finally, the mapper that was added first,
 * wins (stable sort).
 *
 */
final class ResponseMapperComparator implements Comparator<ResponseMapperHolder> {
	private final Class<?> refType;

	public ResponseMapperComparator(Class<?> referenceType) {
		refType = referenceType;
	}

	@Override
	public int compare(ResponseMapperHolder o1, ResponseMapperHolder o2) {
		// quality (DESC)
		int r = Double.compare(o1.getMime().getQuality(), o2.getMime().getQuality());
		if (r != 0)
			return -r;

		// distance
		r = Integer.compare(
			Utils.getClassDistance(o1.getMappedType(), refType),
			Utils.getClassDistance(o2.getMappedType(), refType));
		if (r != 0)
			return r;

		// wildcardCount
		r = Integer.compare(o1.getMime().getWildcardCount(), o2.getMime().getWildcardCount());
		return r;
	}

}