package de.gwdg.cdstar.rest.testutils;

import java.io.ByteArrayOutputStream;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

public class TestResponse {

	private int status = 200;
	private final Map<String, String> headers = new HashMap<>();
	private final ByteArrayOutputStream out = new ByteArrayOutputStream();
	private boolean comitted;
	private boolean closed;

	public TestResponse() {
	}

	void status(int code) {
		status = code;
	}

	void header(String name, String value) {
		if (!comitted)
			headers.put(name.toLowerCase(), value);
	}

	public String getHeader(String name) {
		return headers.get(name.toLowerCase());
	}

	public void write(byte[] buffer, int off, int len) {
		comitted = true;
		out.write(buffer, off, len);
	}

	public boolean isCommitted() {
		return comitted;
	}

	public void assertStatus(int code) {
		if (status != code)
			throw new AssertionError("Wrong status (expected=" + code + ", was=" + status + ")");
	}

	public void assertHeaderEquals(String name, String expected) {
		final String value = getHeader(name);
		if (value == null || !value.equals(expected))
			throw new AssertionError("Wrong header (expected=" + expected + ", was=" + value + ")");
	}

	synchronized void waitAsync() throws InterruptedException {
		while (!closed) {
			wait(1000);
		}
	}

	public synchronized void close() {
		closed = true;
		notifyAll();
	}

	public String getBodyString() {
		return new String(getBody(), StandardCharsets.UTF_8);
	}

	public byte[] getBody() {
		return out.toByteArray();
	}

	public int getStatus() {
		return status;
	}

}
