package de.gwdg.cdstar.rest.v3.ingest;

import java.io.IOException;
import java.nio.ByteBuffer;

public interface ImportTarget {

	/**
	 * Start writing to a new file. Close the currently opened file if
	 * necessary. return false if this file should be skipped.
	 */
	boolean newFile(String name, String mimeType);

	/**
	 * Write a chunk of data to the currently opened file.
	 * 
	 * @throws IOException
	 */
	void writeFile(ByteBuffer data) throws IOException;

	/**
	 * Set meta data on the currently opened file.
	 */
	void setMeta(String name, String value);

	/**
	 * Finish writing the last file and commit changes.
	 */
	void close();

}
