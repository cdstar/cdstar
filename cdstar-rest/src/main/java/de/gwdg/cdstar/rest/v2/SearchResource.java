package de.gwdg.cdstar.rest.v2;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.Vector;

import com.fasterxml.jackson.databind.JsonNode;

import de.gwdg.cdstar.Promise;
import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.rest.api.RestConfig;
import de.gwdg.cdstar.rest.api.RestContext;
import de.gwdg.cdstar.rest.api.RestBlueprint;
import de.gwdg.cdstar.rest.utils.QueryHelper;
import de.gwdg.cdstar.rest.utils.RestUtils;
import de.gwdg.cdstar.rest.v2.model.ApiErrorResponse;
import de.gwdg.cdstar.rest.v2.model.SearchResultBean;
import de.gwdg.cdstar.rest.v2.model.SearchResultBean.Hit;
import de.gwdg.cdstar.runtime.search.SearchHit;
import de.gwdg.cdstar.runtime.search.SearchQueryBuilder;
import de.gwdg.cdstar.runtime.search.SearchResult;

public class SearchResource implements RestBlueprint {
	@Override
	public void configure(RestConfig cfg) {
		cfg.route("/<vault>/search").POST(this::doSearch);
	}

	private static final Set<String> validSelections = new HashSet<>(
		Arrays.asList("fulltext", "metadata", "object", "all"));

	public SearchResultBean doSearch(RestContext ctx) throws IOException {
		final QueryHelper qh = new QueryHelper(ctx);

		qh.setDefault("index", "all");
		qh.setDefault("limit", "100");
		qh.setDefault("offset", "0");
		qh.setDefault("scroll", "");

		final String index = qh.getOption("index", validSelections);
		final int limit = qh.getInt("limit", 1, 500);
		final int offset = qh.getInt("offset", 0, Integer.MAX_VALUE - limit);
		final JsonNode query = RestUtils.parseJsonBody(ctx, JsonNode.class);

		if (offset > 0)
			throw new ApiErrorResponse(501, "not-implemented",
				"Search result offsets are no longer supported. Use result scrolling instead.");

		final SearchQueryBuilder q = new SearchQueryBuilder();
		q.limit(limit);
		q.scrollId(qh.get("scroll"));

		String queryString = query.path("query_string").path("query").asText();
		if (queryString.isEmpty())
			throw new ApiErrorResponse(501, "not-implemented",
				"Arbritrary search queries no longer supported. Query must have the form: '{\"query_string\":{\"query\":\"...\"}}'");

		switch (index) {
		case ("all"):
			break;
		case ("object"):
		case ("metadata"):
			queryString += "-is:file";
			break;
		case ("fulltext"):
			queryString += "+is:file";
			break;
		default:
			Utils.wtf();
		}

		q.query(queryString);

		// TODO: Actually search something here
		final SearchResult r = Promise.<SearchResult>empty().value();

		final SearchResultBean bean = new SearchResultBean();
		bean.setHitcount(r.getTotal());
		bean.setMaxscore(1);
		bean.setHits(new Vector<SearchResultBean.Hit>(r.getSize()));
		for (final SearchHit sr : r.hits()) {
			final Hit hit = new Hit();
			bean.getHits().add(hit);
			hit.setUid(sr.getId());
			hit.setScore((float) sr.getScore());
			hit.setType(sr.getType());
		}

		return bean;
	}
}
