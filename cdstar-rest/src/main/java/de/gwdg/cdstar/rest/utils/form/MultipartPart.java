package de.gwdg.cdstar.rest.utils.form;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Deque;
import java.util.List;
import java.util.Map;

import org.synchronoss.cloud.nio.multipart.MultipartUtils;

public class MultipartPart implements FormPart {

	Deque<byte[]> chunks = new ArrayDeque<>(1);

	long drained = 0;
	long total = 0;

	public boolean done;

	private final Map<String, List<String>> headers;

	public MultipartPart(Map<String, List<String>> partHeaders, int partIndex) {
		headers = partHeaders;
	}

	void write(byte[] b, int off, int len) {
		// TODO: Check if we really have to copy here.
		chunks.add(Arrays.copyOfRange(b, off, off + len));
		total += len;
	}

	@Override
	public boolean isComplete() {
		return done;
	}

	@Override
	public synchronized byte[] drainChunk() {
		final byte[] chunk = chunks.pollFirst();
		if (chunk != null)
			drained += chunk.length;
		return chunk;
	}

	@Override
	public boolean isBuffered() {
		return total > drained;
	}

	@Override
	public int getBuffered() {
		return (int) (total - drained);
	}

	@Override
	public long getTotal() {
		return total;
	}

	@Override
	public long getDrained() {
		return drained;
	}

	@Override
	public String getName() {
		return MultipartUtils.getFieldName(headers);
	}

	@Override
	public String getContentEncoding() {
		final String ce = MultipartUtils.getHeader("Content-Encoding", headers);
		return ce == null ? "identity" : ce;
	}

	@Override
	public String getFileName() {
		return MultipartUtils.getFileName(headers);
	}

	@Override
	public String getContentType() {
		return MultipartUtils.getContentType(headers);
	}

	@Override
	public ByteBuffer drain() {
		if (chunks.size() == 1)
			return ByteBuffer.wrap(drainChunk());

		final ByteBuffer bb = ByteBuffer.allocate(getBuffered());
		byte[] chunk;
		while ((chunk = drainChunk()) != null)
			bb.put(chunk);
		bb.flip();
		return bb;
	}

	@Override
	public String drainToString(Charset charset) {
		return new String(drain().array(), charset);
	}

	@Override
	public void clear() {
		byte[] chunk;
		while ((chunk = chunks.pollFirst()) != null)
			drained += chunk.length;
	}

}
