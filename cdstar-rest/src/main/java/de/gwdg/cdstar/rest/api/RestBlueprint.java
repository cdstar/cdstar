package de.gwdg.cdstar.rest.api;

/**
 * Blueprints are used to configure a {@link RestConfig} by adding routes,
 * request- or error-mappers and services. If annotated with {@link Blueprint},
 * the parent creates a child configuration with the specified settings.
 * Otherwise, blueprints are installed directly and no child-configuration is
 * created.
 */
public interface RestBlueprint {
	/**
	 * Called by {@link RestConfig#install(RestBlueprint)} with a {@link RestConfig}
	 * to configure.
	 */
	void configure(RestConfig cfg) throws Exception;
}
