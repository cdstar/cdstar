package de.gwdg.cdstar.rest.utils;

import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.gwdg.cdstar.LRUCache;

public class GlobPattern implements Predicate<String> {

	// Find '**', '*' or '?' in a quoted (escaped) regular expression.
	static final Pattern tokenPattern = Pattern.compile("(\\*\\*|\\*|\\?)");
	static final LRUCache<String, Pattern> patternCache = new LRUCache<>(1024);

	private Pattern pattern;
	private final String glob;

	public GlobPattern(String glob) {
		this.glob = glob;
	}

	public synchronized Pattern getPattern() {
		if (pattern == null)
			pattern = compile(glob);
		return pattern;
	}

	@Override
	public boolean test(String path) {
		return getPattern().matcher(path).matches();
	}

	@Override
	public String toString() {
		return glob;
	}

	public static Pattern compile(String glob) {
		Pattern cached;

		synchronized (patternCache) {
			cached = patternCache.get(glob);
		}

		if (cached == null) {
			final StringBuilder sb = new StringBuilder(glob.length() * 2);

			if (!glob.startsWith("/"))
				sb.append(".*");

			final Matcher m = tokenPattern.matcher(glob);
			int lastTokenEnd = 0;

			while (m.find()) {
				if (lastTokenEnd < m.start(1))
					sb.append(Pattern.quote(glob.substring(lastTokenEnd, m.start(1))));
				final String token = m.group(1);
				if (token.equals("**"))
					sb.append("(.*)");
				else if (token.equals("*"))
					sb.append("([^/]*)");
				else // token = "?"
					sb.append("([^/])");
				lastTokenEnd = m.end(1);
			}
			if (lastTokenEnd < glob.length())
				sb.append(Pattern.quote(glob.substring(lastTokenEnd)));

			sb.append("$");
			cached = Pattern.compile(sb.toString());

			synchronized (patternCache) {
				patternCache.put(glob, cached);
			}
		}

		return cached;
	}

}
