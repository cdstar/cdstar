package de.gwdg.cdstar.rest.v2.model;

public class Success {
	public boolean	ok	= true;

	public static class OkResponse extends Success {
		String	uid	= null;

		public OkResponse(String uid) {
			this.uid = uid;
		}

		public String getUid() {
			return uid;
		}

		public void setUid(String uid) {
			this.uid = uid;
		}
	}

	public static class BitstreamResponse extends OkResponse {
		String	bitstreamid	= null;

		public BitstreamResponse(String uid, String bitstreamid) {
			super(uid);
			this.bitstreamid = bitstreamid;
		}

		public String getBitstreamid() {
			return bitstreamid;
		}

		public void setBitstreamid(String bitstreamid) {
			this.bitstreamid = bitstreamid;
		}
	}

	public static class ObjectCreated extends OkResponse {

		public ObjectCreated(String uid) {
			super(uid);
		}
	}

	public static class MetadataUpdated extends OkResponse {

		public MetadataUpdated(String uid) {
			super(uid);
		}
	}
}
