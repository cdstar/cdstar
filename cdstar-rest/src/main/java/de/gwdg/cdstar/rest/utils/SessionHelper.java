package de.gwdg.cdstar.rest.utils;

import java.nio.charset.Charset;
import java.time.Duration;
import java.util.Base64;
import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.auth.Permission;
import de.gwdg.cdstar.auth.Subject;
import de.gwdg.cdstar.auth.TokenCredentials;
import de.gwdg.cdstar.auth.UsernamePasswordCredentials;
import de.gwdg.cdstar.rest.api.RestContext;
import de.gwdg.cdstar.runtime.RuntimeContext;
import de.gwdg.cdstar.runtime.client.CDStarSession;
import de.gwdg.cdstar.runtime.client.exc.AccessError;
import de.gwdg.cdstar.ta.TransactionInfo.Mode;
import de.gwdg.cdstar.ta.exc.TARollbackException;
import de.gwdg.cdstar.web.common.model.ErrorResponse;

public class SessionHelper {

	public static final String QUERY_TRANSACTION = "_ta";
	public static final String HEADER_TRANSACTION = "X-Transaction";
	private static final Logger log = LoggerFactory.getLogger(SessionHelper.class);

	/**
	 * Initialize or continue a cdstar session bound to the current request.
	 * Non-durable sessions are automatically rolled back at the end of the request
	 * if not committed manually.
	 *
	 * @param readOnly If True, new sessions are created as read-only. Otherwise, a
	 *                 write-able session is returned. If the session was resumed
	 *                 and the resumed session was read-only, an error is thrown.
	 */
	public static CDStarSession getCDStarSession(RestContext ctx, boolean readOnly) {
		CDStarSession session = ctx.getAttribute("cdstar.session", CDStarSession.class);
		if (session != null)
			return session;

		final Subject subject = getSubject(ctx);
		final RuntimeContext runtime = ctx.getService(RuntimeContext.class);

		final String explicitTransactionID = getExplicitTransactionID(ctx);

		if (explicitTransactionID != null) {
			session = runtime.resumeSession(explicitTransactionID);
			if (session == null || session.isExpired())
				throw new ErrorResponse(400, "TxError", "Transaction invalid or timed out.").detail("transaction",
					explicitTransactionID);
			// Check if transaction belongs to current subject.
			// TODO: Anonymous clients can resume any transaction if they
			// know the ID. Is this a feature or a bug?
			if (!subject.isAnonymous()) {
				final String sessionOwner = session.getClient().getSubject().getPrincipal().getFullId();
				final String currentUser = subject.getPrincipal().getFullId();
				if (!sessionOwner.equals(currentUser))
					throw new ErrorResponse(400, "TxError", "Transaction invalid or timed out.")
						.detail("transaction", explicitTransactionID);
			}
			if (session.isReadOnly() && !readOnly)
				throw new ErrorResponse(400, "TxError", "Transaction is read-only.")
					.detail("transaction", explicitTransactionID);
			session.refreshTimeout();
		} else {
			session = runtime.getClient(subject).begin(readOnly);

			// Auto-rollback one-time sessions
			final CDStarSession sessionClosure = session;
			ctx.runAfterRequest(() -> {
				if (!sessionClosure.isClosed())
					sessionClosure.rollback();
			});
		}

		// Ensure that sessions to not timeout while we are using it.
		suspendSessionTimeout(ctx, session);

		ctx.setAttribute("cdstar.session", session);
		return session;
	}

	/**
	 * Prevent {@link CDStarSession} timeouts while this {@link RestContext} is
	 * alive. Enable this for requests that potentially take a long time to process.
	 *
	 * Other than using {@link CDStarSession#setTimeoutSuspended(boolean)} directly,
	 * this is thread-safe and reentrant. Multiple {@link RestContext} can suspend
	 * the same {@link CDStarSession} multiple times, and only the last closing
	 * {@link RestContext} will re-enable the session timeout again.
	 */
	private static void suspendSessionTimeout(RestContext ctx, CDStarSession session) {
		AtomicInteger refcount;
		/*
		 * Increment a counter for each suspend call on this session. Disable and
		 * remember the original timeout if we are the first.
		 */
		synchronized (session) {
			refcount = (AtomicInteger) session.getContext().computeIfAbsent("suspendCounter", k -> new AtomicInteger());
			if (refcount.getAndIncrement() == 0)
				session.setTimeoutSuspended(true);
		}

		/**
		 * Register a close handler that decrements the counter and re-enables the
		 * timeout if it reaches zero again.
		 */
		ctx.runAfterRequest(() -> {
			synchronized (session) {
				if (refcount.decrementAndGet() == 0)
					session.setTimeoutSuspended(false);
			}
		});
	}

	public static CDStarSession createDurableSession(RestContext ctx, boolean readOnly, Mode mode, Duration timeout) {
		final Subject subject = getSubject(ctx);
		final RuntimeContext runtime = ctx.getService(RuntimeContext.class);
		final CDStarSession session = runtime.getClient(subject).begin(readOnly, mode);
		session.setTimeout(timeout.toMillis());
		return session;
	}

	public static boolean hasDurableSessionID(RestContext ctx) {
		return getExplicitTransactionID(ctx) != null;
	}

	private static String getExplicitTransactionID(RestContext ctx) {
		String tx = ctx.getHeader(HEADER_TRANSACTION);
		if (tx == null)
			tx = ctx.getQueryParam(QUERY_TRANSACTION);
		return tx;
	}

	/**
	 * Commit changes made to the session, or suspend it if is a durable session.
	 */
	public static void commitOrSuspend(RestContext ctx) throws TARollbackException {
		final CDStarSession session = ctx.getAttribute("cdstar.session", CDStarSession.class);

		if (session == null)
			throw new IllegalStateException("No session to commit");

		synchronized (session) {
			if (session.isClosed())
				throw new IllegalStateException("Session already closed.");

			final boolean isDurableTransaction = getExplicitTransactionID(ctx) != null;
			if (isDurableTransaction)
				return; // suspend

			session.commit();
		}
	}

	private static final String AUTHORIZATION_HEADER = "Authorization";
	private static final String TOKEN_HEADER = "X-Token";
	private static final String TOKEN_PARAMETER = "token";

	/**
	 * Return the authenticated or anonymous subject responsible for the current
	 * request.
	 *
	 * If the request contained authorization or token information, and the
	 * authorization failed, then an {@link ErrorResponse} is thrown.
	 *
	 * @param ctx current rest context.
	 * @return authenticated {@link Subject}. May be anonymous.
	 */
	public static Subject getSubject(RestContext ctx) {
		Subject subject = ctx.getAttribute("cdstar.subject", Subject.class);
		if (subject != null)
			return subject;

		subject = ctx.getService(RuntimeContext.class).createSubject();

		// Try X-Token header.
		final String tokenHeader = ctx.getHeader(TOKEN_HEADER);
		if (tokenHeader != null) {
			loginToken(subject, tokenHeader);
		}

		// Try Authorization header (basic, token or bearer)
		final String authHeader = ctx.getHeader(AUTHORIZATION_HEADER);
		if (authHeader != null) {
			final int i = authHeader.indexOf(' ');
			if (i < 0)
				throw new ErrorResponse(400, "BadRequest", "Invalid authorization header");
			final String scheme = authHeader.substring(0, i);
			final String value = authHeader.substring(i + 1);

			if ("BASIC".equalsIgnoreCase(scheme)) {
				loginBasic(subject, value);
			} else if ("BEARER".equalsIgnoreCase(scheme) || "TOKEN".equalsIgnoreCase(scheme)) {
				loginToken(subject, value);
			}
		}

		// Try GET parameter
		final String tokenParameter = ctx.getQueryParam(TOKEN_PARAMETER);
		if (tokenParameter != null) {
			loginToken(subject, tokenParameter);
		}

		return subject;
	}

	private static void loginToken(Subject subject, final String value) {
		if (!subject.tryLogin(new TokenCredentials(value))) {
			// Only log 5 characters to prevent leaks of sensitive information.
			log.warn("Login failed: token={}", Utils.repr(value.substring(0, Math.min(value.length(), 5)) + "***"));
			throw new ErrorResponse(401, "Unauthorized", "Invalid or expired token");
		}
	}

	private static void loginBasic(Subject subject, final String value) {
		final String creds = new String(Base64.getDecoder().decode(value), Charset.forName("UTF-8"));
		final int i = creds.indexOf(':');
		if (i < 0)
			throw new ErrorResponse(400, "BadRequest", "Invalid authorization header");

		String username = creds.substring(0, i);
		String domain = null;
		final String password = creds.substring(i + 1);

		// Fully qualified user@realm logins: The realm name must not contain a dot,
		// or it will be interpreted as an e-mail address instead.
		final int i2 = username.lastIndexOf('@');
		if (i2 > 0 && i2 < username.length() - 1 && username.indexOf('.', i2 + 1) == -1) {
			domain = username.substring(i2 + 1);
			username = username.substring(0, i2);
		}

		if (!subject.tryLogin(new UsernamePasswordCredentials(username, domain, password.toCharArray()))) {
			// Never log passwords
			log.warn("Login failed: user={} domain={} password=***", Utils.repr(username), Utils.repr(domain));
			throw new ErrorResponse(401, "Unauthorized", "Invalid credentials");
		}
	}

	public static boolean isPermitted(RestContext ctx, Permission p) {
		return getSubject(ctx).isPermitted(p);
	}

	public static void ensurePermitted(RestContext ctx, Permission p) {
		if (!isPermitted(ctx, p))
			throw new AccessError(p);
	}

}
