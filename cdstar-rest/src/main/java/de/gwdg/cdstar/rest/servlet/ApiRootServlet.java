package de.gwdg.cdstar.rest.servlet;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.Timer;
import com.codahale.metrics.Timer.Context;

import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.rest.CDStarRootModule;
import de.gwdg.cdstar.rest.RequestDispatcher;
import de.gwdg.cdstar.rest.RestConfigImpl;
import de.gwdg.cdstar.runtime.CDStarRuntime;
import de.gwdg.cdstar.runtime.RuntimeContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public class ApiRootServlet extends HttpServlet {
	private static final long serialVersionUID = 5564539332278878726L;
	RestConfigImpl rootConfig;
	RuntimeContext runtime;
	private RequestDispatcher router;
	private Timer timer;

	private static final Logger log = LoggerFactory.getLogger(ApiRootServlet.class);

	public ApiRootServlet(RuntimeContext runtime) {
		this.runtime = runtime;
	}

	@Override
	public void init() throws ServletException {
		if (runtime == null)
			runtime = (CDStarRuntime) getServletContext().getAttribute(CDStarServletInitializer.INI_CDSTAR_RUNTIME);

		Utils.notNull(runtime);
		rootConfig = new RestConfigImpl(null);
		rootConfig.install(new CDStarRootModule(runtime));
		router = new RequestDispatcher(rootConfig);
		if (log.isDebugEnabled())
			log.debug("REST Configuration:\n{}", new RestConfigPrinter(rootConfig).render(true));

		final MetricRegistry metrics = runtime.lookupRequired(MetricRegistry.class);
		timer = metrics.timer("rest.rq");

		super.init();
	}

	@Override
	protected void service(HttpServletRequest rq, HttpServletResponse rs) throws ServletException, IOException {
		if (log.isDebugEnabled()) {
			log.debug("Request from {} for {} {}{}",
					rq.getRemoteAddr(),
					rq.getMethod(),
					rq.getPathInfo(),
					rq.getQueryString() == null ? "" : "?" + rq.getQueryString());
		}
		final ServletRestContext ctx = new ServletRestContext(rq, rs);
		final Context t1 = timer.time();
		ctx.runAfterRequest(() -> t1.stop());
		router.dispatch(ctx);
	}

}
