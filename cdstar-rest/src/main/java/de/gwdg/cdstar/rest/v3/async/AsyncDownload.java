package de.gwdg.cdstar.rest.v3.async;

import java.io.InputStream;
import java.nio.ByteBuffer;

import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.rest.api.AsyncContext;
import de.gwdg.cdstar.rest.api.RestContext;

public class AsyncDownload {

	private final InputStream input;
	private final long maxRead;
	private volatile long skipBytes;
	private volatile long totalBytes = 0;
	private final RestContext ctx;
	private AsyncContext ac;

	/**
	 * Create a new download. Do not forget to call {@link #dispatch()}. The input
	 * stream is closed after the download finished or failed.
	 */
	public AsyncDownload(RestContext ctx, InputStream io, long skip, long readLimit) {
		this.ctx = ctx;
		input = io;
		maxRead = readLimit < 0 ? Long.MAX_VALUE : readLimit;
		skipBytes = skip <= 0 ? 0 : skip;
	}

	public void dispatch() {
		Utils.assertTrue(ac == null, "Already started");
		ac = ctx.startAsync();
		try {
			writeChunk(ac.getBuffer());
		} catch (final Exception e) {
			onError(e);
		}
	}

	private synchronized void writeChunk(ByteBuffer buffer) throws Exception {
		if (skipBytes > 0) {
			input.skip(skipBytes);
			skipBytes = 0;
		}
		final int readLimit = (int) Math.min(buffer.capacity(), maxRead - totalBytes);
		final int nbytes = input.read(buffer.array(), buffer.arrayOffset(), readLimit);
		if (nbytes > 0) {
			totalBytes += nbytes;
			buffer.position(nbytes);
			buffer.flip();
			ac.asyncWrite(buffer, this::writeChunk, this::onError);
		} else {
			Utils.closeQuietly(input);
			ctx.close();
			ac.recycleBuffer(buffer);
		}
	}

	private synchronized void onError(Throwable e) {
		Utils.closeQuietly(input);
		if (totalBytes > 0) {
			// No way to signal the client that the download failed, so we just
			// close the connection.
			ctx.close();
		} else {
			ctx.abort(e);
		}
	}

}
