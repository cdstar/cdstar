package de.gwdg.cdstar.rest.servlet;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.servlet.Filter;
import jakarta.servlet.FilterChain;
import jakarta.servlet.FilterConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public class CORSFilter implements Filter {

	private static final Logger log = LoggerFactory.getLogger(CORSFilter.class);

	public static final String ALLOW_CREDENTIALS_PARAM = "allowCredentials";
	public static final String EXPOSED_HEADERS_PARAM = "exposedHeaders";
	public static final String PREFLIGHT_MAX_AGE_PARAM = "preflightMaxAge";
	public static final String ALLOWED_HEADERS_PARAM = "allowedHeaders";
	public static final String ALLOWED_METHODS_PARAM = "allowedMethods";
	public static final String ALLOWED_ORIGINS_PARAM = "allowedOrigins";
	// Request headers
	private static final String ORIGIN_HEADER = "Origin";
	private static final String ACCESS_CONTROL_REQUEST_METHOD_HEADER = "Access-Control-Request-Method";
	private static final String ACCESS_CONTROL_REQUEST_HEADERS_HEADER = "Access-Control-Request-Headers";
	// Response headers
	private static final String ACCESS_CONTROL_ALLOW_ORIGIN_HEADER = "Access-Control-Allow-Origin";
	private static final String ACCESS_CONTROL_ALLOW_METHODS_HEADER = "Access-Control-Allow-Methods";
	private static final String ACCESS_CONTROL_ALLOW_HEADERS_HEADER = "Access-Control-Allow-Headers";
	private static final String ACCESS_CONTROL_MAX_AGE_HEADER = "Access-Control-Max-Age";
	private static final String ACCESS_CONTROL_ALLOW_CREDENTIALS_HEADER = "Access-Control-Allow-Credentials";
	private static final String ACCESS_CONTROL_EXPOSE_HEADERS_HEADER = "Access-Control-Expose-Headers";
	private static final Set<String> SAVE_HEADERS = new HashSet<>(
			Arrays.asList("accept", "accept-language", "content-language", "content-fype"));
	@SuppressWarnings("unused")
	private static final Set<String> AUTO_EXPOSED = new HashSet<>(
			Arrays.asList("cache-control", "content-language", "content-length", "content-type", "expires",
					"last-modified", "pragma"));

	private boolean allOriginsAllowed;
	private Set<String> allowedOrigins = new HashSet<>(0);
	private Set<Pattern> allowedOriginPatterns = new HashSet<>(0);

	private String allowedMethodsHeader;
	private boolean allMethodsAllowed;
	private Set<String> allowedMethods = new HashSet<>(8);

	private String allowedHeadersHeader;
	private boolean allHeadersAllowed;
	private Set<String> allowedHeaders = new HashSet<>(0);

	private boolean allowCredentials = false;
	private String preflightMaxAge;
	private String exposedHeadersHeader;

	private String getRequiredInitParam(FilterConfig config, String name) {
		return Objects.requireNonNull(config.getInitParameter(name), "Init parameter "+name+ "required");
	}

	@Override
	public void init(FilterConfig config) throws ServletException {

		// Access-Control-Allow-Origin
		for (String origin : hlistSplit(getRequiredInitParam(config, ALLOWED_ORIGINS_PARAM))) {
			if (origin.equals("*")) {
				allOriginsAllowed = true;
			} else if (origin.startsWith("^") && origin.endsWith("$")) {
				Pattern rx = Pattern.compile(origin);
				allowedOriginPatterns.add(rx);
			} else {
				allowedOrigins.add(origin);
			}
		}

		// Access-Control-Allow-Methods
		for (String method : hlistSplit(getRequiredInitParam(config, ALLOWED_METHODS_PARAM))) {
			if (method.equals("*")) {
				allMethodsAllowed = true;
			} else {
				allowedMethods.add(method.toUpperCase());
			}
		}
		allowedMethodsHeader = allMethodsAllowed ? "*" : String.join(", ", allowedMethods);

		// Access-Control-Allow-Headers
		for (String header : hlistSplit(getRequiredInitParam(config, ALLOWED_HEADERS_PARAM))) {
			if (header.equals("*")) {
				allHeadersAllowed = true;
			} else {
				allowedHeaders.add(header);
			}
		}
		allowedHeadersHeader = allHeadersAllowed ? "*" : String.join(", ", allowedHeaders);
		// Some headers are always allowed, but NOT included in the
		// Access-Control-Allow-Headers by default, as that would bypass
		// default value restrictions for these headers. Allow them explicitly
		// to remove the default restrictions.
		allowedHeaders.addAll(SAVE_HEADERS);

		// Access-Control-Allow-Credentials
		allowCredentials = "true".equalsIgnoreCase(config.getInitParameter(ALLOW_CREDENTIALS_PARAM));

		// Access-Control-Expose-Headers
		exposedHeadersHeader = config.getInitParameter(EXPOSED_HEADERS_PARAM);
		if (exposedHeadersHeader != null)
			exposedHeadersHeader = String.join(", ", new HashSet<String>(hlistSplit(exposedHeadersHeader)));

		// Access-Control-MAx-Age
		preflightMaxAge = config.getInitParameter(PREFLIGHT_MAX_AGE_PARAM);
		if (preflightMaxAge == null)
			preflightMaxAge = "1800"; // Default is 30 minutes
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		handle((HttpServletRequest) request, (HttpServletResponse) response, chain);
	}

	private void handle(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		String origin = request.getHeader(ORIGIN_HEADER);
		if (origin == null) {
			// Not a CORS request
			chain.doFilter(request, response);
			return;
		}

		if (!originAllowed(origin)) {
			handleError(request, response, 403, "CORS failed. Origin not allowed.");
			return;
		}

		if (isPreflightRequest(request)) {
			handlePreflightResponse(request, response, origin);
		} else {
			handleSimpleRequest(request, response, origin);
			chain.doFilter(request, response);
		}
	}

	private boolean originAllowed(String origin) {
		if (allOriginsAllowed)
			return true;

		origin = origin.toLowerCase();
		if (allowedOrigins.contains(origin))
			return true;

		for (Pattern p : allowedOriginPatterns)
			if (p.matcher(origin).matches())
				return true;

		return false;
	}

	private boolean isPreflightRequest(HttpServletRequest request) {
		return "OPTIONS".equalsIgnoreCase(request.getMethod())
				&& request.getHeader(ACCESS_CONTROL_REQUEST_METHOD_HEADER) != null;
	}

	private void handlePreflightResponse(HttpServletRequest request, HttpServletResponse response, String origin) {
		String accessControlRequestMethod = request.getHeader(ACCESS_CONTROL_REQUEST_METHOD_HEADER).toUpperCase();
		if (!(allMethodsAllowed || allowedMethods.contains(accessControlRequestMethod))) {
			log.debug("Preflight failed. Method not allowed: {} {}", accessControlRequestMethod, allowedMethods);
			handleError(request, response, 403, "CORS failed. Requested method not allowed.");
			return;
		}

		String accessControlRequestHeaders = request.getHeader(ACCESS_CONTROL_REQUEST_HEADERS_HEADER);
		if (!allHeadersAllowed && accessControlRequestHeaders != null) {
			for (String header : hlistSplit(accessControlRequestHeaders)) {
				if (!(allowedHeaders.contains(header))) {
					log.debug("Preflight failed. Header not allowed: {}", accessControlRequestHeaders);
					handleError(request, response, 403, "CORS failed: Requested header not allowed.");
					return;
				}
			}
		}

		response.setHeader(ACCESS_CONTROL_ALLOW_ORIGIN_HEADER, origin);
		response.addHeader("Vary", ORIGIN_HEADER);
		response.setHeader(ACCESS_CONTROL_MAX_AGE_HEADER, preflightMaxAge);
		response.setHeader(ACCESS_CONTROL_ALLOW_METHODS_HEADER, allowedMethodsHeader);
		response.setHeader(ACCESS_CONTROL_ALLOW_HEADERS_HEADER, allowedHeadersHeader);
		if (allowCredentials)
			response.setHeader(ACCESS_CONTROL_ALLOW_CREDENTIALS_HEADER, "true");
	}

	private void handleSimpleRequest(HttpServletRequest request, HttpServletResponse response, String origin) {
		response.setHeader(ACCESS_CONTROL_ALLOW_ORIGIN_HEADER, origin);
		response.addHeader("Vary", ORIGIN_HEADER);
		if (allowCredentials)
			response.setHeader(ACCESS_CONTROL_ALLOW_CREDENTIALS_HEADER, "true");
		if (exposedHeadersHeader != null)
			response.setHeader(ACCESS_CONTROL_EXPOSE_HEADERS_HEADER, exposedHeadersHeader);
	}

	private void handleError(HttpServletRequest request, HttpServletResponse response, int status, String string) {
		response.setStatus(status);
		response.setContentType("text/plain");

		try (ServletOutputStream body = response.getOutputStream()) {
			body.write("CORS failed. Requested method not allowed.\n".getBytes(StandardCharsets.US_ASCII));
		} catch (IOException e) {
			// Who cares?
		}
	}

	/**
	 * Same as {@code new ArrayList(str.trim().toLowerCase().split("(,|\\s)+"));}
	 * but faster. Also, returns an empty list if the parameter is null.
	 */
	private List<String> hlistSplit(String str) {
		if (str == null)
			return Collections.emptyList();
		List<String> results = new ArrayList<>(8);
		StringBuilder sb = new StringBuilder(str.length());
		for (int i = 0; i < str.length(); i++) {
			char c = str.charAt(i);
			if (c == ',' || Character.isWhitespace(c)) {
				if (sb.length() > 0) {
					results.add(sb.toString());
					sb.setLength(0);
				}
				continue;
			} else {
				sb.append(Character.toLowerCase(c));
			}
		}
		if (sb.length() > 0)
			results.add(sb.toString());
		return results;
	}

	@Override
	public void destroy() {

	}

}
