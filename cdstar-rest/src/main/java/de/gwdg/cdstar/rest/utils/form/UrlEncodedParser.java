package de.gwdg.cdstar.rest.utils.form;

import java.nio.BufferOverflowException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class UrlEncodedParser implements FormParser {

	volatile List<UrlFormPart> parts = new ArrayList<>();
	private final ByteBuffer buffer;

	// Position of first '=' in input
	private int keyLen = -1;

	// -2: not in escape sequence; -1: next byte is first half-byte; 0-155: next
	// byte is second half-byte
	private int escapedByte = -2;
	private final boolean decodePlus;
	private long offset = -1;
	private final Charset charset;

	/**
	 * Create a new query-string or application/x-www-form-urlencoded parser. The
	 * parser has a static memory footprint of {@link #getMaxElementSize()} bytes.
	 *
	 * @param maxElementSize Size of the key and value buffer. Note that there is a
	 *                       single buffer for the key and value of each element.
	 *                       Both key and value must fit into the buffer.
	 * @param decodePlus     Whether or not to translate '+' to a single space.
	 * @param charset        Charset to use.
	 */
	public UrlEncodedParser(int maxElementSize, boolean decodePlus, Charset charset) {
		buffer = ByteBuffer.allocate(maxElementSize);
		this.decodePlus = decodePlus;
		this.charset = charset;
	}

	private void emit() {
		// Ignore empty parameters (e.g. &&) but keep stuff like '&=&'
		// (key=value='')
		if (buffer.position() == 0 && keyLen < 0)
			return;
		// No '=' means: Whole parameter is key, value is empty.
		if (keyLen < 0)
			keyLen = buffer.position();
		final byte[] key = new byte[keyLen];
		final byte[] value = new byte[buffer.position() - keyLen];
		buffer.flip();
		buffer.get(key);
		buffer.get(value);
		parts.add(new UrlFormPart(new String(key, charset), value));
		keyLen = -1;
		buffer.clear();
	}

	public int getMaxElementSize() {
		return buffer.capacity();
	}

	/**
	 * Parse a chunk of input and return a list of {@link UrlFormPart}s.
	 *
	 * @throws FormParserException on protocol errors (e.g. invalid characters after
	 *                             '%' escape sequence or if a part (key + value) is
	 *                             larger than {@link #getMaxElementSize()})
	 */
	@Override
	public List<FormPart> parse(ByteBuffer bytes) throws FormParserException {

		while (bytes.hasRemaining()) {
			offset++;
			final byte b = bytes.get();
			if (escapedByte != -2) {
				final int v;

				if (b >= '0' && b <= '9')
					v = b - '0';
				else if (b >= 'A' && b <= 'F')
					v = 10 + b - 'A';
				else if (b >= 'a' && b <= 'f')
					v = 10 + b - 'a';
				else
					throw new FormParserException(
						"Invalid byte in hex escape sequence: offset=" + offset + ", byte=" + (b & 0xFF));

				if (escapedByte == -1) {
					escapedByte = v;
				} else {
					buffer.put((byte) (escapedByte * 16 + v));
					escapedByte = -2;
				}
			} else if (b == '%') {
				escapedByte = -1;
			} else if (b == '&') {
				emit();
			} else if (b == '=' && keyLen < 0) {
				keyLen = buffer.position();
			} else if (b == '+' && decodePlus) {
				put((byte) ' ');
			} else {
				put(b);
			}
		}
		return flush();
	}

	private void put(byte b) throws FormParserException {
		try {
			buffer.put(b);
		} catch (final BufferOverflowException e) {
			throw new FormParserException("Maximum element size for url-encoded form data exceeded.", e);
		}
	}

	@Override
	public List<FormPart> finish() {
		emit();
		return flush();
	}

	private List<FormPart> flush() {
		if (parts.isEmpty())
			return Collections.emptyList();

		final List<FormPart> result = Collections.unmodifiableList(parts);
		parts = new ArrayList<>();
		return result;
	}

	public static class UrlFormPart implements FormPart {
		private final String name;
		private final byte[] value;
		boolean drained = false;

		public UrlFormPart(String name, byte[] value) {
			this.name = name;
			this.value = value;
		}

		@Override
		public boolean isComplete() {
			return true;
		}

		@Override
		public byte[] drainChunk() {
			if (drained)
				return null;
			drained = true;
			return value;
		}

		@Override
		public boolean isBuffered() {
			return !drained;
		}

		@Override
		public int getBuffered() {
			return drained ? 0 : value.length;
		}

		@Override
		public long getTotal() {
			return value.length;
		}

		@Override
		public long getDrained() {
			return !drained ? 0 : value.length;
		}

		@Override
		public String getName() {
			return name;
		}

		@Override
		public String getContentEncoding() {
			return "identity";
		}

		@Override
		public String getFileName() {
			return null;
		}

		@Override
		public String getContentType() {
			return "text/plain";
		}

		@Override
		public ByteBuffer drain() {
			if (drained)
				return ByteBuffer.allocate(0);
			drained = true;
			return ByteBuffer.wrap(value);
		}

		@Override
		public String drainToString(Charset charset) {
			drained = true;
			return new String(value, charset);
		}

		@Override
		public void clear() {
			drained = true;
		}

	}

	public static boolean isForm(String contentType) {
		return contentType.equalsIgnoreCase("application/x-www-form-urlencoded");
	}

}
