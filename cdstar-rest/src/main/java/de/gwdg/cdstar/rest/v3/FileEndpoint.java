package de.gwdg.cdstar.rest.v3;

import java.io.IOException;
import java.nio.channels.Channels;
import java.nio.channels.WritableByteChannel;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.gwdg.cdstar.MimeUtils;
import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.pool.PoolError.WriteChannelNotClosed;
import de.gwdg.cdstar.rest.api.RestBlueprint;
import de.gwdg.cdstar.rest.api.RestConfig;
import de.gwdg.cdstar.rest.api.RestContext;
import de.gwdg.cdstar.rest.utils.QueryHelper;
import de.gwdg.cdstar.rest.utils.RangeHeader;
import de.gwdg.cdstar.rest.utils.RestUtils;
import de.gwdg.cdstar.rest.utils.SessionHelper;
import de.gwdg.cdstar.rest.v3.async.AsyncDownload;
import de.gwdg.cdstar.rest.v3.async.AsyncUpload;
import de.gwdg.cdstar.rest.v3.async.ModelHelper;
import de.gwdg.cdstar.runtime.client.CDStarArchive;
import de.gwdg.cdstar.runtime.client.CDStarFile;
import de.gwdg.cdstar.runtime.client.CDStarSession;
import de.gwdg.cdstar.runtime.client.CDStarVault;
import de.gwdg.cdstar.runtime.client.exc.AccessError;
import de.gwdg.cdstar.runtime.client.exc.ArchiveNotFound;
import de.gwdg.cdstar.runtime.client.exc.FileNotFound;
import de.gwdg.cdstar.runtime.client.exc.SnapshotNotFound;
import de.gwdg.cdstar.runtime.client.exc.VaultNotFound;
import de.gwdg.cdstar.runtime.client.utils.ArchiveOrSnapshot;
import de.gwdg.cdstar.ta.exc.TARollbackException;
import de.gwdg.cdstar.web.common.model.ErrorResponse;
import de.gwdg.cdstar.web.common.model.MetaMap;

public class FileEndpoint implements RestBlueprint {

	private static final String MTYPE_CDSTAR_RESUME = "application/vnd.cdstar.resume";

	@Override
	public void configure(RestConfig cfg) {
		cfg.route("/<vault:re:[^_][^/]*>/<archive:re:[^_][^/]*>/<file:path>")
				.GET(this::handleGet)
				.PUT(this::handlePut)
				.PATCH(this::handlePatch)
				.DELETE(this::handleDelete);
	}

	/**
	 * Return the file itself, the FileInfo (?info) with or without meta
	 * (?with=meta) or the MetaMap (?meta).
	 */
	public Object handleGet(RestContext ctx)
		throws AccessError, ArchiveNotFound, VaultNotFound, IOException, SnapshotNotFound {

		CDStarVault vault = SessionHelper.getCDStarSession(ctx, true).getVault(ctx.getPathParam("vault"));
		ArchiveOrSnapshot source = ArchiveEndpoint.resolveId(vault, ctx.getPathParam("archive"));

		String fileName = ctx.getPathParam("file");
		CDStarFile file = source.getFile(fileName);

		final QueryHelper qh = new QueryHelper(ctx);
		final String sub = qh.getExclusiveParam("info", "meta");

		// Handle GET ?meta

		if ("meta".equals(sub)) {
			return MetadataHelper.getMetadata(file);
		}

		// Handle GET ?info&[with=meta]

		if ("info".equals(sub)) {
			qh.setDefault("with", "");
			final boolean withMeta = qh.get("with").equals("meta");
			return ModelHelper.makeFileInfo(file, withMeta);
		}

		// Handle GET of actual file

		final String etag = etag(file);
		final Date lastModified = file.getLastModified();
		ctx.header("Content-Type", file.getMediaType());

		/**
		 * XSS Protection.
		 *
		 * Prevent embedding by default. If 'inline' is requested, make sure that
		 * embedding is only allowed for 'save' content types and force browsers to
		 * honor the content-type (prevent sniffing).
		 */
		ctx.header("X-Content-Type-Options", "nosniff");
		if (!(qh.getBoolean("inline") && isSaveContentType(file)))
			ctx.header("Content-Disposition", "attachment");

		if (!RestUtils.checkConditional(ctx, lastModified, etag))
			return null;

		final RangeHeader range = RestUtils.prepareRangeRequest(ctx, lastModified, etag, file.getSize());

		if (range.isZeroLength() || ctx.getMethod().equals("HEAD"))
			return null;

		new AsyncDownload(ctx,
				Channels.newInputStream(file.getReadChannel(range.getStartIndex())),
				0, range.getRangeLength()).dispatch();

		return null;
	}

	static Set<String> saveMimeTypes = new HashSet<>(Arrays.asList(
		"image/jpeg", "image/png", "image/gif",
		"audio/x-wav", "audio/mpeg", "audio/ogg"));

	private boolean isSaveContentType(CDStarFile file) {
		final String mtype = file.getMediaType();
		return saveMimeTypes.contains(mtype.split(";", 2)[0]);
	}

	/**
	 * Create or replace the file if it passes the optional If-Match or
	 * If-None-Match tests. `If-Match: *` prevents the creation of a new file.
	 * `If-None-Match: *` prevents overwriting existing files.
	 */
	public Void handlePut(RestContext ctx) throws AccessError, ArchiveNotFound, VaultNotFound, FileNotFound,
			IOException, TARollbackException, SnapshotNotFound {

		CDStarVault vault = SessionHelper.getCDStarSession(ctx, false).getVault(ctx.getPathParam("vault"));
		ArchiveOrSnapshot source = ArchiveEndpoint.resolveId(vault, ctx.getPathParam("archive"));
		CDStarArchive archive = ArchiveEndpoint.assumeArchive(source);

		final String fileName = ctx.getPathParam("file");

		// Handle PUT ?meta

		final QueryHelper qh = new QueryHelper(ctx);
		final String subResource = qh.getExclusiveParam("meta");

		if ("meta".equals(subResource)) {
			MetadataHelper.setMetadata(archive.getFile(fileName), RestUtils.parseJsonBody(ctx, MetaMap.class));
			SessionHelper.commitOrSuspend(ctx);
			ctx.status(204);
			return null;
		}

		// Handle PUT to actual file resource

		final boolean fileExists = archive.hasFile(fileName);
		final CDStarFile file;

		if (fileExists) {
			file = archive.getFile(fileName);
			checkConditional(ctx, etag(file));
			file.truncate(0);
		} else {
			checkConditional(ctx, null);
			file = archive.createFile(fileName);
		}

		String mediaType = ctx.getContentType();
		if (mediaType == null || mediaType.equals(MimeUtils.APPLICATION_X_AUTODETECT))
			mediaType = MimeUtils.guess(fileName, false).getMime();
		if (mediaType != null)
			file.setMediaType(mediaType);

		final String contentEncoding = ctx.getHeader("Content-Encoding");
		if (contentEncoding != null && !contentEncoding.equals("identity"))
			throw new ErrorResponse(415, "UnsupportedMediaType", "Compressed uploads are currently not supported.");

		final WritableByteChannel writeChannel = file.getWriteChannel();

		new AsyncUpload(ctx, writeChannel).dispatch().whenComplete((written, err) -> {
			try {
				if (err != null)
					throw err;
				writeChannel.close();
				SessionHelper.commitOrSuspend(ctx);
				ctx.status(fileExists ? 200 : 201);
				ctx.write(ModelHelper.makeFileInfo(file, false));
				ctx.close();
			} catch (final Throwable e) {
				// Keep write channel open in case this is a resume-able upload. See PATCH.
				ctx.abort(e);
			}
		});

		return null;
	}

	private static Pattern PATCH_RANGE = Pattern.compile("^bytes=(\\d+)-(\\d+)?$");

	/**
	 * Resumes an incomplete file upload or appends to an existing file.
	 * 
	 * @throws VaultNotFound
	 * @throws SnapshotNotFound
	 * @throws ArchiveNotFound
	 * @throws IOException
	 */
	public Void handlePatch(RestContext ctx) throws VaultNotFound, ArchiveNotFound, SnapshotNotFound, IOException {
		if (!SessionHelper.hasDurableSessionID(ctx))
			throw new ErrorResponse(400, "TransactionRequired",
					"Resuming uploads are only possible within a transaction.");

		if (!Utils.equal(ctx.getContentType(), MTYPE_CDSTAR_RESUME))
			throw new ErrorResponse(415, "UnsupportedMediaType", "Expected \"" + MTYPE_CDSTAR_RESUME + "\"");

		CDStarSession session = SessionHelper.getCDStarSession(ctx, false);
		CDStarVault vault = session.getVault(ctx.getPathParam("vault"));
		ArchiveOrSnapshot source = ArchiveEndpoint.resolveId(vault, ctx.getPathParam("archive"));
		CDStarArchive archive = ArchiveEndpoint.assumeArchive(source);
		CDStarFile file = archive.getFile(ctx.getPathParam("file"));

		String rangeHeader = ctx.getHeader("Range");
		if (rangeHeader == null)
			throw new ErrorResponse(416, "RangeNotSatisfiable", "Missing required request header: Range");

		Matcher m = PATCH_RANGE.matcher(rangeHeader);
		if (!m.matches())
			throw new ErrorResponse(416, "RangeNotSatisfiable", "Invalid or unparseable range header");
		long rangeStart = Long.valueOf(m.group(1));
		long rangeEnd = m.group(2) != null ? Long.valueOf(m.group(2)) : -1;

		// Check ranges AFTER creating the write channel to prevent races with parallel
		// uploads.

		// TODO: Fail if there is no write in progress (to prevent appending to finished
		// files).

		final WritableByteChannel writeChannel = file.getWriteChannel();

		if (rangeStart != file.getSize())
			throw new ErrorResponse(416, "RangeNotSatisfiable", "Current resource size does not match supplied range.")
					.detail("size", file.getSize());
		if (rangeEnd > 0 && rangeEnd <= rangeStart)
			throw new ErrorResponse(416, "RangeNotSatisfiable", "Invalid range end.");

		new AsyncUpload(ctx, writeChannel).dispatch().whenComplete((written, err) -> {
			try {
				if (err != null)
					throw err;
				if (rangeEnd > -1 && file.getSize() != rangeEnd)
					throw new ErrorResponse(416, "RangeNotSatisfiable", "Upload size and range did not match");
				writeChannel.close();
				SessionHelper.commitOrSuspend(ctx);
				ctx.status(200);
				ctx.write(ModelHelper.makeFileInfo(file, false));
				ctx.close();
			} catch (final Throwable e) {
				// Keep write-channel open
				ctx.abort(e);
			}
		});

		return null;
	}

	/**
	 * Throw a `412 PreconditionFailed` {@link ErrorResponse} if the given ETag does
	 * not pass the If-Match or If-None-Match tests (if any).
	 */
	private void checkConditional(RestContext ctx, String etag) throws ErrorResponse {
		final String ifMatch = ctx.getHeader("If-Match");
		if (ifMatch != null && !matchEtag(ifMatch, etag))
			throw new ErrorResponse(412, "PreconditionFailed", "If-Match condition not satisfied.");

		final String ifNoneMatch = ctx.getHeader("If-None-Match");
		if (ifNoneMatch != null && matchEtag(ifNoneMatch, etag))
			throw new ErrorResponse(412, "PreconditionFailed", "If-None-Match condition not satisfied.");
	}

	private boolean matchEtag(String header, String etag) {
		return etag != null && (header.equals("*") || header.equals(etag));
	}

	/**
	 * Return a proper ETag header value (including quotes) for a given file.
	 */
	public static String etag(CDStarFile file) {
		try {
			return "\"" + file.getDigests().get(CDStarFile.DIGEST_SHA256) + "\"";
		} catch (WriteChannelNotClosed e) {
			return "W/\"" + file.getID() + "-" + file.getSize() + "\"";
		}
	}

	/**
	 * Delete the file if it exists and passes the optional If-Match or
	 * If-None-Match tests.
	 */
	public Void handleDelete(RestContext ctx)
			throws FileNotFound, ArchiveNotFound, VaultNotFound, TARollbackException, SnapshotNotFound {

		CDStarVault vault = SessionHelper.getCDStarSession(ctx, false).getVault(ctx.getPathParam("vault"));
		ArchiveOrSnapshot source = ArchiveEndpoint.resolveId(vault, ctx.getPathParam("archive"));
		CDStarArchive archive = ArchiveEndpoint.assumeArchive(source);
		CDStarFile file = archive.getFile(ctx.getPathParam("file"));

		final QueryHelper qh = new QueryHelper(ctx);
		final String subResource = qh.getExclusiveParam("meta");

		// Handle DELETE ?meta. This is undocumented, but a safeguard against
		// clients that assume full CRUD support for the ?meta subresource.
		if ("meta".equals(subResource)) {
			MetadataHelper.clearMetadata(file);
			SessionHelper.commitOrSuspend(ctx);
			ctx.status(204);
			return null;
		}

		// Handle DELETE of entire file
		checkConditional(ctx, etag(file));

		file.remove();
		SessionHelper.commitOrSuspend(ctx);
		ctx.status(204);
		return null;
	}

}
