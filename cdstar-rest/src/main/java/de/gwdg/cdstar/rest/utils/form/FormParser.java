package de.gwdg.cdstar.rest.utils.form;

import java.nio.ByteBuffer;
import java.util.List;

/**
 * An incremental non-blocking form parser.
 */
public interface FormParser {

	ByteBuffer EOF = ByteBuffer.allocate(0);

	/**
	 * Parse all bytes from the given buffer and return a list of zero or more
	 * {@link FormPart} instances. The last element in a list may be incomplete, so
	 * always check {@link FormPart#isComplete()}. Incomplete parts are returned
	 * repeatedly until they are complete.
	 */

	List<FormPart> parse(ByteBuffer bytes) throws FormParserException;

	/**
	 * Return any buffered {@link FormPart}s and close the parser.
	 */
	List<FormPart> finish() throws FormParserException;

}
