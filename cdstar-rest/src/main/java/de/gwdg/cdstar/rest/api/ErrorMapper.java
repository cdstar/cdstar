package de.gwdg.cdstar.rest.api;

import java.io.IOException;

@FunctionalInterface
public interface ErrorMapper<T extends Throwable> {

	/**
	 * Handle the given error. Any exception thrown from within an error handler
	 * are fatal and not handled in any way (only logged).
	 */
	void handle(RestContext ctx, T e) throws IOException;
}
