package de.gwdg.cdstar.rest.v2;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.rest.api.RestConfig;
import de.gwdg.cdstar.rest.api.RestContext;
import de.gwdg.cdstar.rest.api.RestBlueprint;
import de.gwdg.cdstar.rest.utils.RestUtils;
import de.gwdg.cdstar.rest.utils.SessionHelper;
import de.gwdg.cdstar.rest.v2.model.ObjectBean.PermissionBean;
import de.gwdg.cdstar.rest.v2.utils.LegacyHelper;
import de.gwdg.cdstar.runtime.client.CDStarArchive;
import de.gwdg.cdstar.runtime.client.CDStarSession;
import de.gwdg.cdstar.runtime.client.auth.ArchivePermissionSet;
import de.gwdg.cdstar.runtime.client.exc.ArchiveNotFound;
import de.gwdg.cdstar.runtime.client.exc.VaultNotFound;
import de.gwdg.cdstar.ta.exc.TARollbackException;

public class AccessControlResource implements RestBlueprint {
	
	@Override
	public void configure(RestConfig cfg) {
		cfg.route("/<vault>/accesscontrol/<uid>").GET(this::getACL).PUT(this::setACL);
	}

	PermissionBean getACL(RestContext ctx) throws ArchiveNotFound, VaultNotFound {
		final CDStarSession session = SessionHelper.getCDStarSession(ctx, true);
		final CDStarArchive ar = session.getVault(ctx.getPathParam("vault")).loadArchive(LegacyHelper.fromAnyID(ctx.getPathParam("uid")));
		final PermissionBean perm = new PermissionBean();
		LegacyHelper.translatePermissions(perm, ar);
		return perm;
	}

	PermissionBean setACL(RestContext ctx) throws IOException, TARollbackException, ArchiveNotFound, VaultNotFound {
		final CDStarSession session = SessionHelper.getCDStarSession(ctx, false);
		final CDStarArchive ar = session.getVault(ctx.getPathParam("vault")).loadArchive(LegacyHelper.fromAnyID(ctx.getPathParam("uid")));

		final PermissionBean perm = RestUtils.parseJsonBody(ctx, PermissionBean.class);

		// Change owner
		if (!Utils.equal(perm.getOwner(), ar.getOwner()))
			ar.setOwner(perm.getOwner());

		// Clear all permissions:
		ar.getACL().getAccessList().forEach(f -> f.revokeAll());

		final Map<ArchivePermissionSet, List<String>> pairs = new HashMap<>();
		pairs.put(ArchivePermissionSet.READ, perm.getRead());
		pairs.put(ArchivePermissionSet.WRITE, perm.getWrite());
		pairs.put(ArchivePermissionSet.MANAGE, perm.getManage());

		for (final Entry<ArchivePermissionSet, List<String>> kv : pairs.entrySet()) {
			final ArchivePermissionSet grant = kv.getKey();
			for (final String ident : kv.getValue()) {
				if (ident.startsWith("@")) {
					ar.getACL().forGroup(ident.substring(1)).permit(grant);
				} else if (ident.startsWith("$")) {
					switch (ident) {
					case "$owner":
						ar.getACL().forOwner().permit(grant);
						break;
					case "$known":
						ar.getACL().forKnown().permit(grant);
						break;
					case "$any":
						ar.getACL().forAny().permit(grant);
						break;
					}
				} else {
					ar.getACL().forPrincipal(ident).permit(grant);
				}
			}
		}

		session.commit();
		return perm;
	}

}
