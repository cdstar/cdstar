package de.gwdg.cdstar.rest.api;

/**
 * Functional interface for an event listener for asynchronous contexts.
 */

@FunctionalInterface
public interface AsyncCloseListener {
	/**
	 * The async context was closed for whatever reason.
	 *
	 * @param ac
	 *            the closed {@link AsyncContext}
	 * @param error
	 *            The error that caused the {@link AsyncContext} to be closed,
	 *            or null if the context was closed gracefully.
	 */
	void closed(AsyncContext ac, Throwable error);
}