package de.gwdg.cdstar.rest.v3;

import de.gwdg.cdstar.rest.api.RestConfig;
import de.gwdg.cdstar.rest.api.RestContext;
import de.gwdg.cdstar.rest.api.RestBlueprint;
import de.gwdg.cdstar.rest.utils.SessionHelper;
import de.gwdg.cdstar.runtime.RuntimeContext;
import de.gwdg.cdstar.runtime.client.auth.ServicePermission;
import de.gwdg.cdstar.runtime.services.health.HealthLevel;
import de.gwdg.cdstar.runtime.services.health.HealthMonitor;
import de.gwdg.cdstar.runtime.services.health.HealthService;
import de.gwdg.cdstar.runtime.services.health.HealthStatus;
import de.gwdg.cdstar.web.common.model.ServiceStatusReport;

public class ServiceHealthEndpoint implements RestBlueprint {
	private HealthService health;

	@Override
	public void configure(RestConfig cfg) {
		cfg.route("/_health").GET(this::handleGetStatus);

		health = cfg.lookup(RuntimeContext.class).lookupRequired(HealthService.class);
	}

	public ServiceStatusReport handleGetStatus(RestContext ctx) {
		final ServiceStatusReport status = new ServiceStatusReport();

		final boolean canSeeChecks = SessionHelper.getSubject(ctx)
			.isPermitted(ServicePermission.HEALTH.asStringPermission());

		int warn = 0;
		int error = 0;
		for (final HealthMonitor m : health.getMonitors()) {
			final HealthStatus h = m.check();
			if (canSeeChecks)
				status.checks.put(m.getName(), h.toString());
			switch (h.getHealth()) {
			case OK:
				break;
			case WARN:
				warn++;
				break;
			case ERROR:
				error++;
				break;
			}
		}

		if (error > 0) {
			ctx.status(503);
			status.status = HealthLevel.ERROR.name();
		} else if (warn > 0) {
			status.status = HealthLevel.WARN.name();
		} else {
			status.status = HealthLevel.OK.name();
		}

		return status;
	}

}
