package de.gwdg.cdstar.config;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;

import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.runtime.Config;
import de.gwdg.cdstar.runtime.ConfigException;

public class ConfigLoader {
	private static Logger log = LoggerFactory.getLogger(ConfigLoader.class);

	private static ObjectMapper defaultMapper = new ObjectMapper();
	private static Map<String, ObjectMapper> mapperMap = new HashMap<>();

	static {
		addMapper(defaultMapper, "json");
		addMapper(new YAMLMapper(), "yaml", "yml", "");
	}

	private static void addMapper(ObjectMapper mapper, String... extentions) {
		mapper.enable(SerializationFeature.INDENT_OUTPUT);
		mapper.enable(SerializationFeature.ORDER_MAP_ENTRIES_BY_KEYS);
		for (final String ext : extentions) {
			mapperMap.put(ext, mapper);
		}
	}

	public static Config fromFile(File configFile) throws IOException {
		final ObjectMapper mapper = getObjectMapperFor(configFile);
		JsonNode node = mapper.readTree(configFile);

		if (node == null) {
			log.warn("Config file empty: {}", configFile);
			node = mapper.createObjectNode();
		}

		final MapConfig cfg = new MapConfig();
		flatten(cfg, (ObjectNode) node);

		return cfg;
	}

	public static Config fromArguments(String optParam, String fileParam, String[] args)
			throws ConfigException, IOException {
		final MapConfig cfg = new MapConfig();

		String param = null;
		for (String arg : args) {
			for (final String test : new String[] { optParam, fileParam }) {
				if (arg.equals(test)) {
					param = test;
					continue;
				} else if (arg.startsWith(test)) {
					param = test;
					arg = arg.substring(test.length());
				} else {
					// Unrecognized parameter
					continue;
				}
			}

			if (optParam.equals(param)) {
				final int i = arg.indexOf('=');
				if (i > 0 && i < arg.length()) {
					cfg.set(arg.substring(0, i), arg.substring(i + 1));
				} else {
					throw new ConfigException("Argument did not contain '=': " + arg);
				}
				param = null;
			} else if (fileParam.equals(param)) {
				cfg.setAll(fromFile(new File(arg)));
				param = null;
			}
		}
		return cfg;
	}

	private static Config flatten(Config cfg, ObjectNode node) {
		for (final Entry<String, JsonNode> e : Utils.iter(node.fields())) {
			final JsonNode value = e.getValue();
			if (value.isObject())
				flatten(cfg.with(e.getKey()), (ObjectNode) value);
			else if (value.isArray())
				flatten(cfg, e.getKey(), (ArrayNode) value);
			else
				flatten(cfg, e.getKey(), value.asText());
		}
		return cfg;
	}

	private static void flatten(Config cfg, String key, ArrayNode node) {
		for (final JsonNode value : node) {
			if (value.isObject()) {
				flatten(cfg.with(key), (ObjectNode) value);
			} else if (value.isArray()) {
				flatten(cfg, key, (ArrayNode) value);
			} else {
				flatten(cfg, key, value.asText());
			}
		}
	}

	private static void flatten(Config cfg, String key, String value) {
		if (cfg.hasKey(key))
			value = cfg.get(key, null) + ", " + value;
		cfg.set(key, value);
	}

	public static void saveAs(File targetFile, Config config) throws IOException {
		final ObjectMapper mapper = getObjectMapperFor(targetFile);
		mapper.writeValue(targetFile, config.toMap());
	}

	private static ObjectMapper getObjectMapperFor(File fname) throws IOException {
		ObjectMapper mapper;
		String ext = fname.getName();
		final int i = ext.lastIndexOf('.');
		ext = (i == -1) ? "(no extention)" : ext.substring(i + 1, ext.length()).toLowerCase();

		mapper = mapperMap.get(ext);
		if (mapper != null)
			return mapper;
		throw new IOException("Unknown/unsupported file type: " + ext);
	}

	public static ObjectNode asTree(Config config) {
		final ObjectNode node = defaultMapper.createObjectNode();
		asTree(node, config);
		return node;
	}

	private static void asTree(ObjectNode node, Config config) {
		for (final Entry<String, Config> e : config.getTable().entrySet()) {
			final String key = e.getKey();
			final Config value = e.getValue();
			if (value.isEmpty())
				continue; // cannot happen
			else if (value.size() == 1 && value.hasKey(""))
				node.put(key, value.get("", null));
			else
				asTree(node.with(key), value);
		}
	}
}
