package de.gwdg.cdstar.config;

import java.util.AbstractSet;
import java.util.Iterator;
import java.util.Objects;
import java.util.Set;

import de.gwdg.cdstar.runtime.Config;
import de.gwdg.cdstar.runtime.ConfigException;

/**
 * A namespaced view on a {@link Config}, as it would be returned by
 * {@link Config#with(String)}.
 *
 */
public class ConfigView implements Config {

	private final Config source;
	private final String prefix;
	private final String namespace;

	public ConfigView(Config source, String namespace) {
		if (namespace == null || namespace.isEmpty())
			throw new IllegalArgumentException("Namespace cannot be null or empty.");
		this.source = source;
		this.namespace = namespace;
		prefix = namespace + SEP;
	}

	@Override
	public boolean hasKey(String key) {
		return source.hasKey(fullKey(key));
	}

	@Override
	public String get(String key) throws ConfigException {
		return source.get(fullKey(key));
	}

	@Override
	public ConfigView with(String namespace) {
		return new ConfigView(source, prefix + namespace);
	}

	@Override
	public int size() {
		int c = 0;
		for (final String key : source.keySet())
			if (mapKey(key) != null)
				c++;
		return c;
	}

	@Override
	public Set<String> keySet() {
		return new AbstractSet<String>() {

			@Override
			public Iterator<String> iterator() {
				return ConfigView.this.keyIterator();
			}

			@Override
			public int size() {
				return ConfigView.this.size();
			}

			@Override
			public boolean contains(Object o) {
				if (o instanceof String)
					return ConfigView.this.hasKey((String) o);
				return false;
			}
		};
	}

	private Iterator<String> keyIterator() {
		return source.keySet().stream().map(this::mapKey).filter(Objects::nonNull).iterator();
	}

	private String mapKey(String full) {
		if (full.startsWith(prefix))
			return full.substring(prefix.length());
		else if (full.equals(namespace))
			return "";
		else
			return null;
	}

	private String fullKey(String key) {
		if (key == null || key.isEmpty())
			return namespace;
		return prefix + key;
	}

	@Override
	public Config set(String key, String value) {
		source.set(fullKey(key), value);
		return this;
	}
}
