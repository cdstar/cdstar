package de.gwdg.cdstar.config;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import de.gwdg.cdstar.runtime.Config;
import de.gwdg.cdstar.runtime.ConfigException;

public class MapConfig implements Config {

	private final Map<String, String> values = new HashMap<>();

	public MapConfig() {
	}

	public MapConfig(Config other) {
		setAll(other);
	}

	public MapConfig(Map<String, String> other) {
		setAll(other);
	}

	@Override
	public MapConfig set(String key, String value) {
		if (value == null)
			values.remove(key);
		else
			values.put(key, value);
		return this;
	}

	@Override
	public boolean hasKey(String key) {
		return values.containsKey(key);
	}

	@Override
	public String get(String key) throws ConfigException {
		if (!values.containsKey(key))
			throw new ConfigException("Missing Key: " + key);
		return values.get(key);
	}

	public Map<String, String> getMap() {
		return values;
	}

	@Override
	public Config with(String name) {
		if (name == null || name.isEmpty())
			return this;
		return new ConfigView(this, name);
	}

	@Override
	public Set<String> keySet() {
		return values.keySet();
	}

	@Override
	public String toString() {
		return getMap().toString();
	}

}
