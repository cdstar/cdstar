package de.gwdg.cdstar.config;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.runtime.Config;
import de.gwdg.cdstar.runtime.ConfigException;

public class WildcardConfig implements Config {

	static Pattern replacePattern = Pattern.compile("\\$\\{([^}:]+)(?::([^}:]+))?\\}");
	private final Map<String, String> cache = new HashMap<>();
	private final Map<String, String> defaults = new HashMap<>();
	private final Config src;
	private final Function<String, String> getEnv;

	public WildcardConfig(Config src) {
		this(src, key -> System.getenv(key.toUpperCase()));
	}

	public WildcardConfig(Config src, Function<String, String> envResolve) {
		this.src = src;
		getEnv = envResolve;
	}

	public Config getSrc() {
		return src;
	}

	public static Config resolveAll(Config cfg) {
		final MapConfig copy = new MapConfig();
		copy.setAll(new WildcardConfig(cfg));
		return copy;
	}

	private String resolve(String key, List<String> stack) throws ConfigException {
		String value = src.get(key);

		if (!value.contains("${"))
			return value;

		final Matcher matcher = replacePattern.matcher(value);
		if (!matcher.find())
			return value;

		if (stack == null)
			stack = new ArrayList<>(1);
		else if (stack.contains(key))
			throw new ConfigException(
					"Variable resolution detected a circular reference: " + Utils.join("->", stack) + "->" + key);
		stack.add(key);

		final StringBuffer result = new StringBuffer(value.length());
		do {
			final String q = matcher.group(1);
			final String def = matcher.group(2);
			String envValue;
			if (defaults.containsKey(q)) {
				matcher.appendReplacement(result, defaults.get(q));
			} else if (q.toUpperCase().equals(q) && (envValue = getEnv.apply(q)) != null) {
				matcher.appendReplacement(result, envValue);
			} else if (src.hasKey(q)) {
				matcher.appendReplacement(result, resolve(q, stack));
			} else if (def != null) {
				matcher.appendReplacement(result, def);
			} else {
				throw new ConfigException("Failed to resolve: ${" + q + "}");
			}
		} while (matcher.find());
		matcher.appendTail(result);
		value = result.toString();
		return value;
	}

	@Override
	public boolean hasKey(String key) {
		return src.hasKey(key);
	}

	@Override
	public String get(String key) throws ConfigException {
		if (!cache.containsKey(key))
			cache.put(key, resolve(key, null));
		return cache.get(key);
	}

	@Override
	public Config set(String key, String value) {
		src.set(key, value);
		cache.clear();
		return this;
	}

	@Override
	public Set<String> keySet() {
		return src.keySet();
	}

	@Override
	public Config with(String name) {
		if (name == null || name.isEmpty())
			return this;
		return new ConfigView(this, name);
	}
}
