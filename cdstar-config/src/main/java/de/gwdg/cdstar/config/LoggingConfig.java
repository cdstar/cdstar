package de.gwdg.cdstar.config;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.gwdg.cdstar.runtime.Config;
import de.gwdg.cdstar.runtime.ConfigException;

/**
 * {@link Config} wrapper that records keys for which values were requested, and
 * can be asked for keys that were never used.
 *
 * Note that only {@link #get(String)} (and methods using it) will add a key to
 * the set of used keys. Bulk retrieval methods like {@link #toMap()} will most
 * likely add ALL keys to the set.
 *
 */
public class LoggingConfig implements Config {
	private static final Logger log = LoggerFactory.getLogger(LoggingConfig.class);

	private final Config src;
	private final Set<String> accessed = new HashSet<>();

	public LoggingConfig(Config src) {
		this.src = src;
	}

	public Config getSrc() {
		return src;
	}

	/**
	 * Return all keys for which values were accessed through this wrapper.
	 */
	public Set<String> getUsedKeys() {
		return Collections.unmodifiableSet(accessed);
	}

	/**
	 * Return all keys that are defined, but where their value was never accessed.
	 */
	public Set<String> getUnusedKeys() {
		final HashSet<String> all = new HashSet<>(keySet());
		all.removeAll(accessed);
		return all;
	}

	@Override
	public boolean hasKey(String key) {
		return src.hasKey(key);
	}

	@Override
	public String get(String key) throws ConfigException {
		final String val = src.get(key);
		if (accessed.add(key))
			log.debug("Read config key: {}", key);
		return val;
	}

	@Override
	public Config set(String key, String value) {
		accessed.add(key);
		src.set(key, value);
		return this;
	}

	@Override
	public Set<String> keySet() {
		return src.keySet();
	}

	@Override
	public Config with(String name) {
		if (name == null || name.isEmpty())
			return this;
		return new ConfigView(this, name);
	}
}
