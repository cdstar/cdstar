[[config]]
# Configuration

CDStar is configured via configuration files (`YAML` or `json`), command-line arguments or environment variables, or a combination thereof. In any cases, configuration is treated as a flat list of dot-separated keys and plain string values (e.g. `key.name=value`). File formats that support advanced data types and nesting (namely json an yaml) are flattened automatically when loaded. Arrays or multiple values for the same key are simply joined into a comma-separated list.

[source,yaml]
.Example: Nested documents are flattened automatically.
---
# Nested document
path:
  home: "/mnt/vault"
vault.demo:
  create: True
---
# Flattened form
path.home: "/mnt/vault"
vault.demo.create: "True"

.Value references
Values may contain references to other keys (e.g. `${path.home}`) or environment variables (e.g. `${ENV_NAME}`). The latter is recommended for sensitive information that should not appear in config files or command line arguments (e.g. passwords). A cololon (`:`) is used to separate the reference from an optional default value.

For example, `${CDSATR_HOME:/var/lib/cdstar}` would be replaced by the content of the `CDSTAR_HOME` environment variable, or the default path if the environment variable is not defined.


== Disk Storage

CDStar stores all its data and internal state on the file system. You usually only need to set set `path.home`, as all other parameters default to subdirectories under the `path.home` directory.

path.home:: This directory is used as as base directory for the other paths. (default: `${CDSTAR_HOME:/var/lib/cdstar/}`)
path.data:: Storage location for archive-data and runtime information. CDStar creates a subdirectory for each vault and follows symlinks, which makes it easy to split the storage across several mounted disks. (default: `${path.home}/data`)
path.var:: Storage location for short-lived temporary data. Do NOT use a ramdisk or other volatile storage, as transaction and crash-recovery data will also be stored here. (default: `${path.home}/var`)
path.lib:: Plugins and extensions are searched for in this directory, if they are not found on the default java classpath. (default: `${path.home}/lib`)


== Transports

CDStar supports `http` and `https` transports out of the box. By default, only the unencrypted `http` transport is enabled and binds to localhost port `8080`. The high port number allows CDStar to run as non-root, which is the recommended mode of operation.

External access should be encrypted and off-loaded to a reverse proxy (e.g. https://docs.nginx.com/nginx/admin-guide/web-server/reverse-proxy/[nginx]) for security and performance reasons. Only enable the build-in `https` transport for testing or if you know what you are doing.

http.host:: IP address to bind to. A value of `0.0.0.0` will bind to all available interfaces at the same time. (default: `127.0.0.1`).
http.port:: Network port to bind to. Ports below `1024` require root privileges (not recommended). A value of `0` will bind to a random free port. A value if `-1` will disable this transport. (default: `8080`)

https.host:: IP address to bind to. (default: `${http.port}`).
https.port:: Network port to listen to. (default: `8433`)
https.certfile:: Path to a `*.pem` file containing the certificate chain and private key. (required)
https.h2:: Enable `HTTP/2`. This requires Java 9+ and should be considered experimental. (default: `false`)

== Public REST API

The REST API is exposed over all configured transports.

api.dariah.enable:: Enable or disable the dariah REST API. (default: `False`)
api.v2.enable:: Enable or disable the legacy v2 REST API. (default: `False`)
api.v3.enable:: Enable or disable the current v3 REST API. (default: `True`)
api.context:: Provide the public service URL. This is required if cdstar runs behind a reverse proxy or load balancer and cannot detect its public URL automatically. (default: `/`)


== Vaults

Vaults are usually created at runtime via the management API, but can also be be bootstrapped from configuration. Statically configured vaults are created at startup if they do not exist, and ignored otherwise. It is not possible to change the parameters of a vault via configuration after they were created.

vault.<name>.create:: If true, create this vault on startup if it does not exist already.
vault.<name>.public:: If true, allow public (non-authenticated) read access to this vault. Archive permissions are still checked.

Each vault is backed by a storage pool, which can be configured as part of the vault configuration. The default pool configuration looks like this, and may be overwritten if needed (experimental, not recommended).

vault.<name>.pool.class:: Storage pool class or name. Defaults to the `NioPool` class.
vault.<name>.pool.name:: Storage pool name. Defaults to the vault name.
vault.<name>.pool.path:: Data path for this storage pool. Defaults to `${path.data}/${name}`:

Other `StoragePool` implementations may accept additional parameters.

Plugins may also read vault-level configuration to control vault-specific behavior. The `DefaultPermissions` feature for example controls the permissions defined on newly created archives and can be configured differently for each vault.


== Realms
Realms manage authentication and authorization in CDStar.
For a simple setup with only a hand full of users, you usually only need a single 'default' realm (e.g. `StaticRealm`) with everything configured within the same config file.
More complex scenarios (e.g. LDAP, JWT or SAML auth) are supported via specialized implementations of the `Realm` interface (e.g. `StaticRealm`, `JWTRealm` or `LdapRealm`) and can be combined in many ways.

realm.<name>.class:: Realm implementation to use. Either a simple class name or a fully qualified java class. (required)
realm.<name>.<field>:: Additional realm configuration.

See <<realms>> for a list of available realm implementations and their configuration options.

WARNING: If no realm is configured, cdstar adds an 'admin' user with a randomly generated password to the implicit 'system' realm. The password is logged to the console on startup and changes every restart.

TIP: Realms are no different from plugins. They are only configured in a separate `reaml.*` name-space to avoid accidental misconfiguration.


== Plugins and Extentions
CDSTAR can be extended with custom implementations for event listeners, storage pools, long-term storage adapters and many other interfaces. These can be referenced by name, simple class-name or fully qualified java class name.

plugin.<name>.class:: Plugin to load. Either a name, java class name or a fully qualified java class path.
plugin.<name>.<field>:: Additional plugin configuration.


.Example
```yaml
plugin.ui:
   class: UIBlueprint
plugin.bagit:
   class: de.gwdg.cdstar.runtime.lts.bagit.BagitTarget
   path: ${path.home}/bagit/
```
