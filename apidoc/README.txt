API Documentation Generator
===========================

This is a tool to automatically generate excessive documentation from a YAML based REST api description. The output format is asciidoc, which can be translated into HTML, PDF and lots of other document formats.

This approach is very similar to Swagger (based on OpenAPI 2.0) and should be replaced by a standard solution as soon as the OpenAPI 3.0 spec is released. We cannot use Swagger at the moment due to missing support for Content-Type based routing in OpenAPI 2.0. Multiple endpoints that listen to the same method and path, but different Content-Type headers, cannot be described in OpenAPI 2.0 format. Unfortunately, we use this kind of routing in our JAX-RS based API as it is a common practice in REST APIs.
