from jinja2 import Environment, FileSystemLoader, Template
import yaml, json
import yaml.constructor
from collections import OrderedDict
import sys
import re

aslist = lambda x: x if isinstance(x, (list, tuple)) else ([x] if x else [])

class OrderedDictYAMLLoader(yaml.Loader):
    def __init__(self, *args, **kwargs):
        yaml.Loader.__init__(self, *args, **kwargs)
        self.add_constructor(u'tag:yaml.org,2002:map', type(self).construct_yaml_map)
        self.add_constructor(u'tag:yaml.org,2002:omap', type(self).construct_yaml_map)

    def construct_yaml_map(self, node):
        data = OrderedDict()
        yield data
        value = self.construct_mapping(node)
        data.update(value)

    def construct_mapping(self, node, deep=False):
        if isinstance(node, yaml.MappingNode):
            self.flatten_mapping(node)
        else:
            raise yaml.constructor.ConstructorError(None, None,
                'expected a mapping node, but found %s' % node.id, node.start_mark)

        mapping = OrderedDict()
        for key_node, value_node in node.value:
            key = self.construct_object(key_node, deep=deep)
            try:
                hash(key)
            except TypeError as exc:
                raise yaml.constructor.ConstructorError('while constructing a mapping',
                    node.start_mark, 'found unacceptable key (%s)' % exc, key_node.start_mark)
            value = self.construct_object(value_node, deep=deep)
            mapping[key] = value
        return mapping


context = {}

with open("src/api.yaml") as fp:
    context['api'] = yaml.load(fp, OrderedDictYAMLLoader)

if __name__ == '__main__':
    ENV = Environment(loader=FileSystemLoader('./'))
    ENV.filters['json'] = json.dumps
    ENV.filters['list'] = aslist
    def sub(s, find, replace):
        return re.sub(find, replace, s)
    ENV.filters['sub'] = sub

    if len(sys.argv) > 1:
        template = ENV.get_template(sys.argv[1])
        print(template.render(**context))
    else:
        print(json.dumps(context))
