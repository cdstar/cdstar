import requests
import sys
import os.path

if __name__ != '__main__':
    raise Exception("Do not import this as a library")


ROOT = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'samples')
def save_as(name, method, url, **args):
    res = requests.request(method, url, **args)
    req = res.request
    with open(os.path.join(ROOT, name+'.json'), 'w') as fp:
        fp.write(res.text)
        
    with open(os.path.join(ROOT, name+'.txt'), 'w') as fp:
        prefix = ''
        fp.write(prefix + req.method + ' ' + req.url.replace(baseurl, '/v3') + '\n')
        for k, v in req.headers.items():
            if k in {'Content-Type', 'If-Match'}:
                fp.write(prefix + k+': '+v+'\n')
        if req.body:
            fp.write(prefix + '\n')
            for line in req.body.splitlines():
                fp.write(prefix + line + '\n')
        elif req.method in {'POST', 'PUT'}:
            fp.write(prefix + '(empty body)\n')

        fp.write('\n')

        fp.write(prefix + str(res.status_code) + ' ' + res.reason + '\n')
        for k, v in res.headers.items():
            if k in {'Content-Type', 'Location'}:
                fp.write(prefix + k+': '+v+'\n')
        if 'json' in res.headers.get('Content-Type'):
            for line in res.text.splitlines():
                fp.write(prefix + line + '\n')
        elif res.content:
            fp.write(prefix + '[... %d bytes ...]' % len(res.content) + '\n')
            
    with open(os.path.join(ROOT, name+'.txt'), 'r') as fp:
        print()
        print(name)
        print('='*80)
        print(fp.read())

    return res

'''
  call ${prog} http://localhost:8080/v3/ myVault
'''
baseurl = sys.argv[1].rstrip('/')
vault = sys.argv[2]

ar = save_as('create_archive', 'POST', baseurl + '/' + vault).json()['id']
save_as('put_file', 'PUT', baseurl + '/' + vault + '/' + ar + '/test.txt', data="Hello World")
save_as('get_file', 'GET', baseurl + '/' + vault + '/' + ar + '/test.txt')
save_as('set_meta_archvie', 'POST', baseurl + '/' + vault + '/' + ar, data={
    'meta:dc:title': ['First title', 'Second title']
})



    
    
    


