MVN ?= ./mvnw
VERSION := ${shell (test .version -nt pom.xml || ($(MVN) -q -Dexec.executable='echo' -Dexec.args='$${project.version}' --non-recursive exec:exec > .version))&& cat .version || (rm -f .version; echo "UNKNOWN")}
ALLSRC := $(shell find . -type f -path '*/src/*' -o -name pom.xml -not -path '*/target/*')

.SECONDARY:

all: build

_PHONY: version docs test build clean push demo

version:
	@echo $(VERSION)

docs:
	$(MAKE) -C apidoc

test:
	$(MVN) clean test verify

test_clean:
	TMP=`mktemp -d`; \
	git clone --depth=1 file://`pwd` $$TMP ;\
	$(MAKE) -C $$TMP test

build:
	$(MVN) package -DskipTests=true

clean:
	$(MVN) -q clean
	rm -rf dist cdstar.jar

push: test_clean
	git push origin HEAD

demo: cdstar.jar dist/plugins
	java -jar cdstar.jar -c cdstar-demo.yaml --debug de.gwdg run

cdstar.jar: dist/bin/cdstar.jar
	ln -fs $< $@


PLUGINS := $(shell find cdstar-plugins-pom/ -mindepth 2 -maxdepth 2 -name 'pom.xml' | sed 's,.*/\(.*\)/pom.xml,\1,')
APPS := cdstar cdstar-elastic-ingest

.PHONY: dist dist/bin dist/plugins dist/docs dist/maven dist/javadoc
dist: dist/bin dist/plugins dist/docs dist/maven dist/javadoc

dist/plugins: $(foreach plugin,$(PLUGINS),dist/plugins/$(plugin).jar)

dist/plugins/%.jar: $(ALLSRC)
	@mkdir -p dist/plugins
	$(MVN) -pl cdstar-plugins-pom/$* -am package -DskipTests=true
	cp cdstar-plugins-pom/$*/target/$*-$(VERSION)-shaded.jar dist/plugins/$*.jar

dist/bin: $(foreach app,$(APPS),dist/bin/$(app).jar)

dist/bin/%.jar: $(ALLSRC)
	$(MVN) -pl $* -am package -DskipTests=true -Pshaded
	@mkdir -p dist/bin/
	cp $*/target/$*-$(VERSION)-shaded.jar $@

dist/bin/cdstar.jar: dist/bin/cdstar-cli.jar
	mv $< $@

dist/maven: $(ALLSRC)
	@mkdir -p dist/maven
	$(MVN) package javadoc:jar source:jar deploy -DskipTests=true \
	  "-DaltDeploymentRepository=snapshot-repo::default::file:`pwd`/dist/maven"

dist/docs: docs
	cp -a apidoc/build dist/docs

dist/javadoc: $(ALLSRC)
	$(MVN) javadoc:javadoc javadoc:aggregate
	@mkdir -p dist
	cp -a cdstar-api/target/site/apidocs dist/javadoc
	cp -a target/site/apidocs dist/javadoc-all

