package de.gwdg.cdstar.tm;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.gwdg.cdstar.GromitIterable;
import de.gwdg.cdstar.ta.TAJournal;
import de.gwdg.cdstar.ta.TAListener;
import de.gwdg.cdstar.ta.TARecoveryHandler;
import de.gwdg.cdstar.ta.TAResource;
import de.gwdg.cdstar.ta.TAState;
import de.gwdg.cdstar.ta.TransactionHandle;
import de.gwdg.cdstar.ta.UserTransaction;
import de.gwdg.cdstar.ta.exc.TAFatalError;
import de.gwdg.cdstar.ta.exc.TARollbackException;

public class DiskTransaction implements UserTransaction, TransactionHandle {
	private static Logger log = LoggerFactory.getLogger(DiskTransaction.class);

	static final String DTA_BIND = "#bind";
	static final String DTA_SHOULD_COMMIT = "#commit";

	private final String id;
	private final Instant started;
	private Instant expires;
	private static int defaultTimeout = 60 * 60 * 24;
	private volatile TAState txState = TAState.NEW;

	List<TAResource> boundResources = new ArrayList<>();
	Set<String> boundRecoveryHandlers = new HashSet<>();

	private Mode mode = Mode.SNAPSHOT;
	private final DiskTransactionManager manager;
	private final DiskJournal journal;
	private final GromitIterable<TAListener> listeners = new GromitIterable<>();

	DiskTransaction(DiskTransactionManager manager) {
		this.manager = manager;
		id = UUID.randomUUID().toString();
		started = Instant.now();
		expires = started.plusSeconds(defaultTimeout);
		journal = manager.getJournalFor(this);
	}

	private synchronized void changeState(TAState newState) {
		txState = txState.changeTo(newState);
	}

	private void ensureState(TAState expected) {
		if (txState != expected)
			throw new IllegalStateException("Unexpected transaction state: " + this + " (expected " + expected + ")");
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public Instant getStarted() {
		return started;
	}

	@Override
	public Instant getExpires() {
		return expires;
	}

	@Override
	public void setExpires(Instant instant) {
		expires = instant;
	}

	@Override
	public synchronized void bind(TAResource resource) {
		if (!isOpen())
			throw new IllegalStateException("Invalid transaction state: " + getState());
		if (boundResources.contains(resource))
			return;
		if (boundResources.add(resource)) {
			try {
				resource.bind(this);
			} catch (final Exception e) {
				boundResources.remove(resource);
				throw e;
			}
		}
	}

	@Override
	public synchronized void bindRecoveryHandler(TARecoveryHandler handler) {
		final String handlerId = manager.registerRecoveryHandler(handler);
		if (boundRecoveryHandlers.add(handlerId))
			journal.writePrivileged(DTA_BIND, ByteBuffer.wrap(handlerId.getBytes(StandardCharsets.UTF_8)));
	}

	@Override
	public synchronized void addListener(TAListener listener) {
		listeners.add(listener);
	}

	@Override
	public void commit() throws TARollbackException {
		ensureState(TAState.NEW);
		synchronized (this) {
			ensureState(TAState.NEW);

			try {
				for (final TAListener l : listeners)
					l.beforeCommit(this);

				changeState(TAState.PREPARING);

				// Actually prepare resources
				for (final TAResource resource : boundResources) {
					try {
						resource.prepare(this);
					} catch (final Exception e) {
						log.debug("Failed to prepare resource {}", resource, e);
						throw e;
					}
				}

				journal.writePrivileged(DTA_SHOULD_COMMIT, null);
				journal.close();

			} catch (final Exception e) {
				rollback(e);
				throw new TARollbackException(e);
			}

			// From now on, errors are fatal and cannot be resolved by a rollback
			changeState(TAState.PREPARED);
			final List<Exception> errors = new ArrayList<>(0);

			for (final TAResource resource : boundResources) {
				try {
					resource.commit(this);
				} catch (final Exception e) {
					errors.add(e);
					log.error("Failed to commit after successfull prepare (tx={}). "
							+ "Resource {} remains in locked state.", id, resource, e);
				}
			}

			if (!errors.isEmpty()) {
				// PANIC! We have a partially committed transaction and no way
				// to solve this problem automatically. Ask for human help. Keep
				// writeAheadLog so recovery may be possible. Keep transaction in
				// memory.
				throw new TAFatalError("Failed commit after successfull prepare. See logs for details.", errors.get(0));
			}

			changeState(TAState.COMMITTED);
			manager.forget(this);
			journal.remove();

			for (final TAListener l : listeners) {
				try {
					l.afterCommit(this);
				} catch (final Exception e) {
					log.warn("Error on after-commit listener (ignored)", e);
				}
			}
		}
	}

	@Override
	public void rollback(Throwable reason) {
		if (txState == TAState.CLOSING || txState == TAState.CLOSED)
			return; // NOP (closed or already closing in different thread)

		synchronized (this) {
			changeState(TAState.CLOSING);

			journal.remove();

			/*
			 * No need to persist the log here. The previous state is either
			 * NEW/ROLLBACKONLY or PREPARING. The log is only guaranteed to be persisted
			 * after PREPARED or if the resource flush()ed the log explicitly. If no
			 * resource did this, and a prepare() call failed, then recovery is optional.
			 */

			final List<Exception> errors = new ArrayList<>(0);
			for (final TAResource resource : boundResources) {
				try {
					resource.rollback(this);
				} catch (final Exception e) {
					log.error("Rollback failed. Affected resources are probably in a dirty state.", e);
					// Best we can do is to rollback as much as possible and then
					// ask for help.
					errors.add(e);
				}
			}

			if (!errors.isEmpty()) {
				// Not as fatal as a failing commit, because MVCC somewhat
				// guarantees that other transactions do not see the dirty state, it
				// is still there and should be cleaned up somehow. We leave the
				// transaction in a closing state, the writeAheadLog alive and exit here.
				// This WILL leak memory :/
				throw new TAFatalError("Rollback failed. Affected resources are probably in a dirty state.",
						errors.get(0));
			}

			changeState(TAState.CLOSED);
			manager.forget(this);
		}

		for (final TAListener l : listeners) {
			try {
				l.afterRollback(this);
			} catch (final Exception e) {
				log.warn("Error in after-rollback listener (ignored)", e);
			}
		}
	}

	@Override
	public synchronized void close() {
		switch (getState()) {
		case NEW:
		case ROLLBACKONLY:
			rollback();
			break;
		case COMMITTED:
		case CLOSED:
			break; // Nothing to do
		case PREPARED:
		case PREPARING:
		case CLOSING:
			// This might happen if a commit() or rollback() call
			// exited with an exception, leaving the transaction in an undefined
			// state. Recovery is required in this case.
			throw new IllegalStateException("Cannot close transaction in " + getState() + " state.");
		}
	}

	@Override
	public TAState getState() {
		return txState;
	}

	@Override
	public String toString() {
		return getId();
	}

	@Override
	public void setRollbackOnly() {
		changeState(TAState.ROLLBACKONLY);
	}

	@Override
	public Mode getMode() {
		return mode;
	}

	@Override
	public void setMode(Mode mode) {
		this.mode = mode;
	}

	@Override
	public TAJournal getJournal() {
		return journal;
	}

}
