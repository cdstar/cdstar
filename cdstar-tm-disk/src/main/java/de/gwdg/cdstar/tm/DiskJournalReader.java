package de.gwdg.cdstar.tm;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SeekableByteChannel;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.ta.TAJournalReader;
import de.gwdg.cdstar.ta.TAJournalRecord;

public class DiskJournalReader implements TAJournalReader, AutoCloseable {

	private final Path logFile;
	private final SeekableByteChannel channel;
	private final ByteBuffer readBuffer;
	private final List<String> idCache = new ArrayList<>();
	private boolean eofReached;

	public DiskJournalReader(DiskJournal src) throws IOException {
		this(src.logFile);
	}

	public DiskJournalReader(Path file) throws IOException {
		logFile = file;
		channel = Files.newByteChannel(logFile);
		readBuffer = ByteBuffer.allocate(DiskJournal.WRITE_BUFFER_SIZE);
		readBuffer.limit(0); // Mark buffer as empty
	}

	@Override
	public TAJournalRecord next() throws IOException {

		// Read enough to extract a header
		while (!eofReached && readBuffer.remaining() < 8) {
			readBuffer.compact();
			eofReached = (channel.read(readBuffer) <= 0);
			readBuffer.flip();
		}

		if (eofReached && readBuffer.remaining() == 0)
			return null;

		if (readBuffer.remaining() < 8)
			throw new IOException("Unepected end of file");

		// Read header
		final int keyId = readBuffer.getInt();
		final int dataSize = readBuffer.getInt();
		final byte[] payload;

		// Read payload
		if (dataSize == 0) {
			payload = new byte[0];
		} else if (dataSize <= readBuffer.remaining()) {
			payload = new byte[dataSize];
			readBuffer.get(payload);
		} else {
			final ByteBuffer readInto = ByteBuffer.allocate(dataSize);
			readInto.put(readBuffer);
			while (readInto.hasRemaining()) {
				if (channel.read(readInto) <= 0)
					throw new IOException("Unepected end of file");
			}
			payload = readInto.array();
		}

		if (keyId < idCache.size() && keyId >= 0) {
			// Known key -> return record
			return new DiskJournalRecord(idCache.get(keyId), payload);
		} else if (keyId == idCache.size()) {
			// Unknown key -> Learn it and read next record
			idCache.add(new String(payload, StandardCharsets.UTF_8));
			return next();
		} else {
			throw new IOException("Unexpected key ID: " + keyId);
		}

	}

	@Override
	public void reset() throws IOException {
		channel.position(0);
		readBuffer.clear().limit(0);
		idCache.clear();
		eofReached=false;
	}

	@Override
	public void close() {
		Utils.closeQuietly(channel);
	}

	public static void main(String[] args) throws IOException {

		if (args.length == 0 || Utils.arrayContains(args, "-h"))
			System.out.println("Usage: $prog /path/to/logfile.wal");

		try (final DiskJournalReader dr = new DiskJournalReader(Paths.get(args[0]))) {
			dr.forEach(r -> {
				System.out.println(r.getName() + " -> " + Utils.repr(r.getStringValue()));
			});
		}
	}

}