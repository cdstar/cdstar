package de.gwdg.cdstar.tm;

import de.gwdg.cdstar.ta.TAJournalRecord;

class DiskJournalRecord implements TAJournalRecord {

	private final String name;
	private final byte[] data;

	public DiskJournalRecord(String name, byte[] data) {
		this.name = name;
		this.data = data;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public byte[] getValue() {
		return data;
	}
}