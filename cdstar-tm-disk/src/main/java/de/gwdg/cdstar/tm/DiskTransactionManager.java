package de.gwdg.cdstar.tm;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.runtime.Config;
import de.gwdg.cdstar.runtime.ConfigException;
import de.gwdg.cdstar.runtime.RuntimeContext;
import de.gwdg.cdstar.runtime.listener.RuntimeListener;
import de.gwdg.cdstar.ta.TARecoveryHandler;
import de.gwdg.cdstar.ta.TransactionManager;
import de.gwdg.cdstar.ta.UserTransaction;

public class DiskTransactionManager implements TransactionManager, RuntimeListener {
	private static Logger log = LoggerFactory.getLogger(DiskTransactionManager.class);

	private static final String SERVICES_EXT = ".services";
	private static final String TRANSACTION_EXT = ".wal";

	private final Map<TARecoveryHandler, String> recoveryHandlerIDs = new ConcurrentHashMap<>();
	private final Map<String, DiskTransaction> runningTransactions = new ConcurrentHashMap<>();

	private final Path logPath;
	volatile boolean started;

	// TODO: Close expired transactions.
	// TODO: Extend expiration date of transactions in use.

	public DiskTransactionManager(Config properties) throws IOException, ConfigException {
		this(Paths.get(properties.get("path")));
	}

	public DiskTransactionManager(Path path) {
		logPath = path;
	}

	@Override
	public void onStartup(RuntimeContext ctx) throws Exception {
		start();
	}

	public void start() throws Exception {
		Files.createDirectories(logPath);
		new RecoveryTask(logPath).startRecovery();
		started = true;
	}

	@Override
	public UserTransaction begin() {
		if (!started)
			throw new IllegalStateException("Transaction manager not started");

		final DiskTransaction t = new DiskTransaction(this);
		runningTransactions.put(t.getId(), t);

		log.debug("Transaction started: {}", t);
		return t;
	}

	@Override
	public UserTransaction getTransaction(String transactionId) {
		if (!started)
			throw new IllegalStateException("Transaction manager not started");
		return runningTransactions.get(transactionId);
	}

	void forget(DiskTransaction t) {
		runningTransactions.remove(t.getId());
	}

	@Override
	public synchronized void onShutdown(RuntimeContext ctx) {
		shutdown();
	}

	public synchronized void shutdown() {
		log.debug("Stopping transaction manager");
		int errors = 0;
		for (final DiskTransaction t : Utils.toArrayList(runningTransactions.values())) {
			try {
				t.close();
			} catch (final Exception e) {
				log.error("Failed to close transaction: {}", t, e);
				errors++;
			}
		}

		if (errors == 0 && runningTransactions.isEmpty()) {
			recoveryHandlerIDs.values().forEach(id -> Utils.deleteQuietly(logPath.resolve(id + SERVICES_EXT)));
			recoveryHandlerIDs.clear();
		} else {
			log.error("Some transactions failed to close properly. Keeping journal to enable recovery.");
		}

		log.debug("Shutdown complete");
	}

	public DiskJournal getJournalFor(DiskTransaction tx) {
		return new DiskJournal(logPath.resolve(tx.getId() + TRANSACTION_EXT));
	}

	public String registerRecoveryHandler(TARecoveryHandler handler) {
		return recoveryHandlerIDs.computeIfAbsent(handler, s -> {
			final String id = UUID.randomUUID().toString();
			final String filename = id + SERVICES_EXT;
			log.debug("Persisting transactional service recovery handler: {} (implemented by {})", id, handler);
			RecoveryTask.persistRecoveryHandler(logPath.resolve(filename), id, handler);
			return id;
		});
	}

}
