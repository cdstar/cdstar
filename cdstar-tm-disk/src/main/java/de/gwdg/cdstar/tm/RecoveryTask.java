package de.gwdg.cdstar.tm;

import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.IOError;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.ta.TAJournalReader;
import de.gwdg.cdstar.ta.TAJournalRecord;
import de.gwdg.cdstar.ta.TARecoveryHandler;

/**
 * Facility class to persist {@link RecoveryHandler} and transactions to enable
 * crash recovery.
 *
 * An instance of this class is used to persist {@link RecoveryHandler}s and
 * transactions to disk.
 */
class RecoveryTask {
	private static final String SERVICES_EXT = ".services";
	private static final String TRANSACTION_EXT = ".wal";
	private static Logger log = LoggerFactory.getLogger(RecoveryTask.class);
	private final Path journalDirectory;

	final List<Path> serviceFiles = new ArrayList<>();
	final List<Path> transactionFiles = new ArrayList<>();
	final Map<String, TARecoveryHandler> handlers = new HashMap<>();
	final List<Throwable> errors = new ArrayList<>();

	/**
	 * Start a journal in the given path. If the path contains artifacts of an
	 * unclean shutdown, recovery is initiated. If that fails, an exception is
	 * thrown.
	 *
	 * @param path
	 *            Journal directory.
	 *
	 * @throws IllegalStateException
	 *             Journal folder is not clean and recovery failed.
	 */

	public RecoveryTask(Path path) throws IllegalStateException {
		journalDirectory = path;
	}

	/**
	 * Inspect a journal directory and recover any transactions found there
	 * using serialized {@link TARecoveryHandler} instances.
	 *
	 * Return true if recovery was successful or there was nothing to recover,
	 * false otherwise.
	 *
	 * This implementation tries to recover as much as possible. Failing
	 * {@link TARecoveryHandler#recover(String, TAJournalReader, boolean)} calls
	 * do not stop the process. Errors are logged, then result in a single
	 * Exception at the end.
	 */
	public void startRecovery() {

		log.debug("Starting recovery...");

		if (!Files.isDirectory(journalDirectory)) {
			log.error("Journal directory not found: {}", journalDirectory);
			return;
		}

		try {
			collectFiles();
		} catch (final IOException e) {
			log.error("Unable to access journal directory: {}", e);
			return;
		}

		if (transactionFiles.isEmpty()) {
			log.info("No journals found: Assuming clean state.");
			serviceFiles.forEach(Utils::deleteQuietly);
			return;
		}

		log.info("Unfinished journals found. Recovery required. Loading recovery handlers...");

		// Load as many RecoveryHandler instances as possible.
		// Missing or broken recovery handlers are acceptable if no transaction
		// actually depends on them.
		for (final Path serviceFile : serviceFiles) {
			loadRecoveryHandler(serviceFile);
		}

		// Try and fix as many half-opened transactions as possible.
		for (final Path transactionFile : transactionFiles) {
			recoverTransaction(transactionFile);
		}

		// Done.
		if (errors.isEmpty()) {
			log.info("Recovery completed.");
			serviceFiles.forEach(Utils::deleteQuietly);
			transactionFiles.forEach(Utils::deleteQuietly);
		} else {
			log.error("Recovery failed! See logs and remaining files in {}", journalDirectory);
			throw new IllegalStateException("Recovery failed.");
		}
	}

	private void collectFiles() throws IOException {
		Files.list(journalDirectory).forEach(p -> {
			final String fname = p.getFileName().toString();
			if (fname.endsWith(SERVICES_EXT))
				serviceFiles.add(p);
			else if (fname.endsWith(TRANSACTION_EXT))
				transactionFiles.add(p);
			else
				log.warn("Unrecognized file: {}", p.toAbsolutePath());
		});
	}

	private void loadRecoveryHandler(final Path serviceFile) {
		log.debug("Loading recovery handler from file: {}", serviceFile);
		try (FileInputStream fileIn = new FileInputStream(serviceFile.toFile());
				ObjectInputStream in = new ObjectInputStream(fileIn)) {
			final String name = (String) in.readObject();
			final TARecoveryHandler serviceRecovery = (TARecoveryHandler) in.readObject();
			handlers.put(name, serviceRecovery);
		} catch (final Exception e) {
			log.error("Failed to load recovery handler", e);
		}
	}

	static void persistRecoveryHandler(final Path file, String id, TARecoveryHandler handler) {
		try (ObjectOutputStream out = new ObjectOutputStream(
			new BufferedOutputStream(Files.newOutputStream(file, StandardOpenOption.CREATE_NEW)))) {
			out.writeObject(id);
			out.writeObject(handler);
			out.flush();
		} catch (final IOException e) {
			throw new IOError(e);
		}
	}

	private void recoverTransaction(final Path transactionFile) {

		final String fname = transactionFile.getFileName().toString();
		final String id = fname.substring(0, fname.length() - TRANSACTION_EXT.length());

		log.debug("Loading transaction journal: {}", transactionFile);
		DiskJournalReader journalReader;
		final Set<String> boundRecoveryHandlers = new HashSet<>();
		boolean shouldCommit = false;

		try {
			journalReader = new DiskJournalReader(transactionFile);
			for (TAJournalRecord record; (record = journalReader.next()) != null;) {
				if (DiskTransaction.DTA_BIND.equals(record.getName())) {
					boundRecoveryHandlers.add(record.getStringValue());
				} else if (DiskTransaction.DTA_SHOULD_COMMIT.equals(record.getName())) {
					shouldCommit = true;
					break;
				}
			}
		} catch (final IOException e) {
			log.error("Error while reading from transaction journal: {}", transactionFile, e);
			errors.add(e);
			return;
		}

		boolean hasErrors = false;
		for (final String recoveryHandlerID : boundRecoveryHandlers) {
			try {
				log.debug("Running recovery handler: {}", recoveryHandlerID);
				final TARecoveryHandler handler = handlers.get(recoveryHandlerID);
				if (handler == null)
					throw new IllegalStateException("Missing service: " + recoveryHandlerID);
				journalReader.reset();
				handler.recover(id, journalReader, shouldCommit);
			} catch (final Exception e) {
				log.error("Recovery failed for handler {} ({})", Utils.repr(recoveryHandlerID),
					transactionFile, e);
				hasErrors = true;
				errors.add(e);
			}
		}

		journalReader.close();

		if (!hasErrors)
			log.info("Sucessfully {} journal: {}", shouldCommit ? "committed" : "rolled back", transactionFile);

	}
}
