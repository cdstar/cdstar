package de.gwdg.cdstar.tm;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SeekableByteChannel;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.ta.TAJournal;

/**
 * Writer for a simple binary write-ahead journal. The journal consists of a
 * sequence of variable length entries:
 *
 * <pre>
 *   4 bytes: Name ID (signed int, big endian).
 *   4 bytes: Size of payload (signed int, big endian).
 *   * bytes: Either a new name (utf-8 encoded) or a record payload.
 * </pre>
 *
 * Each entry represents either a unique record name (UTF-8 encoded string), or
 * a record. The first time a record with a new name is written to the log, two
 * entries are persisted: One associating the unique record name with a new
 * sequential Name ID (starting at 0), and one with the actual record payload.
 * Additional records with the same name are stored as a single entry.
 *
 * A reader would recognize entries defining new names and build an array with
 * known record names while iterating through the entries.
 *
 */
public class DiskJournal implements TAJournal {
	public static final int MAX_ID_SIZE = 512;

	// Should be big enough to fit most records.
	public static final int WRITE_BUFFER_SIZE = 1024 * 8;

	private static Set<String> privilegedIDs = new HashSet<>(
		Arrays.asList(DiskTransaction.DTA_BIND, DiskTransaction.DTA_SHOULD_COMMIT));

	private final Map<String, Integer> keyIDs = new HashMap<>();

	List<LogRecord> bufferedEntries = new ArrayList<>();
	final Path logFile;
	private boolean fileCreated = false;
	boolean closed;
	private IOException error;

	static class LogRecord {
		private final int key;
		private final ByteBuffer data;

		public LogRecord(int key, ByteBuffer data) {
			this.key = key;
			this.data = data;
		}
	}

	public DiskJournal(Path logFile) {
		this.logFile = logFile;
	}

	@Override
	public void write(String key, ByteBuffer data) {
		if (privilegedIDs.contains(key))
			throw new IllegalArgumentException("Log record ID is reserved: " + key);
		if (key.length() > MAX_ID_SIZE)
			throw new IllegalArgumentException(
				"Record key larger than " + MAX_ID_SIZE + " characters.");
		writePrivileged(key, data);
	}

	synchronized void writePrivileged(String key, ByteBuffer data) {
		bufferRecord(getKeyId(key), data);
	}

	private void bufferRecord(int keyId, ByteBuffer data) {
		if (closed)
			throw new IllegalStateException("Journal closed");
		bufferedEntries.add(new LogRecord(keyId, data));
	}

	private int getKeyId(String key) {
		return keyIDs.computeIfAbsent(key, k -> {
			final int keyId = keyIDs.size();
			bufferRecord(keyId, ByteBuffer.wrap(key.getBytes(StandardCharsets.UTF_8)));
			return Integer.valueOf(keyId);
		}).intValue();
	}

	@Override
	public synchronized void flush() throws IOException {
		if (bufferedEntries.isEmpty())
			return;
		if (error != null)
			throw new IllegalStateException("Log file compromised because of a previous error", error);

		// TODO: Keep channel open and just flush it? Tradeoff between open file
		// descriptors and file open/close overhead.
		try (final SeekableByteChannel channel = Files.newByteChannel(logFile, StandardOpenOption.WRITE,
			fileCreated ? StandardOpenOption.APPEND : StandardOpenOption.CREATE_NEW)) {
			fileCreated = true;

			final ByteBuffer writeBuffer = ByteBuffer.allocateDirect(WRITE_BUFFER_SIZE);

			for (final LogRecord entry : bufferedEntries) {
				writeLogEntry(entry, writeBuffer, channel);
			}

			// Flush remaining bytes in buffer, if any
			if (writeBuffer.position() > 0)
				flushBuffer(channel, writeBuffer);

		} catch (final IOException e) {
			error = e;
			throw e;
		}

		bufferedEntries.clear();
	}

	private void writeLogEntry(final LogRecord entry, final ByteBuffer writeBuffer, final SeekableByteChannel channel)
			throws IOException {
		// Make room for a header
		if (writeBuffer.remaining() < 8)
			flushBuffer(channel, writeBuffer);

		// Write header
		writeBuffer.putInt(entry.key);
		writeBuffer.putInt(entry.data != null ? entry.data.remaining() : 0);

		// Write payload (if any)
		if (entry.data != null) {
			if (writeBuffer.remaining() >= entry.data.remaining()) {
				writeBuffer.put(entry.data);
			} else {
				// Not enough room in write buffer
				flushBuffer(channel, writeBuffer);
				while (entry.data.hasRemaining())
					channel.write(entry.data);
			}
		}
	}

	/**
	 * Flip, copy to a channel, then clear a buffer.
	 */
	private void flushBuffer(SeekableByteChannel channel, final ByteBuffer writeBuffer) throws IOException {
		writeBuffer.flip();
		while (writeBuffer.hasRemaining())
			channel.write(writeBuffer);
		writeBuffer.clear();
	}

	/**
	 * Flush and close the journal, preventing future writes.
	 */
	void close() throws IOException {
		closed = true;
		flush();
	}

	@Override
	public boolean isClosed() {
		return closed;
	}

	/**
	 * Remove and close the journal, preventing future writes.
	 */
	void remove() {
		closed = true;
		if (fileCreated)
			Utils.deleteQuietly(logFile);
	}

}
