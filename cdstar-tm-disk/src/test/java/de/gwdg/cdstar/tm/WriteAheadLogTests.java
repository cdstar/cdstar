package de.gwdg.cdstar.tm;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import de.gwdg.cdstar.ta.TAJournalRecord;
import de.gwdg.cdstar.utils.test.TestUtils;

public class WriteAheadLogTests {

	@Rule
	public TemporaryFolder tmp = new TemporaryFolder();
	private Path walFile;
	private DiskJournal wal;

	@Before
	public void setUp() throws Exception {
		walFile = new File(tmp.newFolder(), "test.wal").toPath();
		wal = new DiskJournal(walFile);
	}

	@Test
	public void testFileCreatedOnDemand() throws Exception {
		assertFalse(Files.exists(walFile));
		wal.flush();
		assertFalse(Files.exists(walFile));
		wal.write("test");
		assertFalse(Files.exists(walFile));
		wal.flush();
		assertTrue(Files.exists(walFile));
		assertTrue(Files.size(walFile) > 0);
		wal.remove();
		assertFalse(Files.exists(walFile));
	}

	@Test
	public void testWriteMethods() throws Exception {
		final byte[] bytes = "Hello World".getBytes(StandardCharsets.UTF_8);
		wal.write("no body");
		wal.write("byte array", bytes);
		wal.write("byte array offset", bytes, 2, 2);
		wal.write("byte buffer", ByteBuffer.wrap(bytes));
		wal.write("string", "Hello World");
		wal.flush();

		try (final DiskJournalReader reader = new DiskJournalReader(walFile)) {
			assertLog(reader.next(), "no body", new byte[] {});
			assertLog(reader.next(), "byte array", bytes);
			assertLog(reader.next(), "byte array offset", Arrays.copyOfRange(bytes, 2, 2 + 2));
			assertLog(reader.next(), "byte buffer", bytes);
			assertLog(reader.next(), "string", bytes);
		}
	}

	@Test
	public void testLargeEntry() throws IOException {
		final byte[] largeKey = new byte[DiskJournal.MAX_ID_SIZE];
		Arrays.fill(largeKey, (byte) 'x');
		final byte[] largeMessage = new byte[1024 * 1024];
		wal.write(new String(largeKey, StandardCharsets.UTF_8), largeMessage);
		wal.flush();

		try (final DiskJournalReader reader = new DiskJournalReader(walFile)) {
			assertLog(reader.next(), new String(largeKey, StandardCharsets.UTF_8), largeMessage);
		}
	}

	@Test
	public void testWriteAfterClose() throws IOException {
		wal.close();
		TestUtils.assertRaises(IllegalStateException.class, () -> wal.write("foo"));
	}

	private void assertLog(TAJournalRecord record, final String key, final byte[] message) throws IOException {
		assertNotNull("Expected entry, got null", record);
		assertEquals(key, record.getName());
		assertArrayEquals(message, record.getValue());
	}

	@Test(expected = IllegalArgumentException.class)
	public void testKeyToLarge() {
		final byte[] largeKey = new byte[DiskJournal.MAX_ID_SIZE + 1];
		Arrays.fill(largeKey, (byte) 'x');
		wal.write(new String(largeKey, StandardCharsets.UTF_8));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testReservedKey() {
		wal.write(DiskTransaction.DTA_BIND);
	}

}
