package de.gwdg.cdstar.tm;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.mockito.ArgumentMatchers;
import org.mockito.InOrder;
import org.mockito.Mockito;

import de.gwdg.cdstar.config.MapConfig;
import de.gwdg.cdstar.ta.TAJournalReader;
import de.gwdg.cdstar.ta.TARecoveryHandler;
import de.gwdg.cdstar.ta.TAResource;
import de.gwdg.cdstar.ta.TransactionHandle;
import de.gwdg.cdstar.ta.UserTransaction;
import de.gwdg.cdstar.ta.exc.TAFatalError;
import de.gwdg.cdstar.ta.exc.TARollbackException;
import de.gwdg.cdstar.utils.test.TestLogger;

public class DiskTransactionTests {

	@Rule
	public TestLogger log = new TestLogger("de.gwdg");

	@Rule
	public TemporaryFolder tmp = new TemporaryFolder();

	public DiskTransactionManager tm;

	private MapConfig config;

	@Before
	public void setUp() throws Exception {
		config = new MapConfig().set("path", tmp.newFolder().toPath().resolve("tx").toString());
		tm = new DiskTransactionManager(config);
		tm.start();
	}

	@After
	public void tearDown() {
		tm.shutdown();
	}

	@Test
	public void testCommitLifecycle() throws Exception {
		final TAResource mocked = Mockito.mock(TAResource.class);

		final UserTransaction ut = tm.begin();
		ut.bind(mocked);
		ut.commit();

		final InOrder inOrder = Mockito.inOrder(mocked);
		inOrder.verify(mocked, Mockito.times(1)).bind((TransactionHandle) ut);
		inOrder.verify(mocked, Mockito.times(1)).prepare((TransactionHandle) ut);
		inOrder.verify(mocked, Mockito.times(1)).commit((TransactionHandle) ut);
		inOrder.verifyNoMoreInteractions();
	}

	@Test
	public void testRollbackLifecycle() throws Exception {
		final TAResource mocked = Mockito.mock(TAResource.class);

		final UserTransaction ut = tm.begin();
		ut.bind(mocked);
		ut.rollback();

		final InOrder inOrder = Mockito.inOrder(mocked);
		inOrder.verify(mocked, Mockito.times(1)).bind((TransactionHandle) ut);
		inOrder.verify(mocked, Mockito.times(1)).rollback((TransactionHandle) ut);
		inOrder.verifyNoMoreInteractions();
	}

	@Test
	public void testFailedPrepareLifecycle() throws Exception {
		final TAResource mocked = Mockito.mock(TAResource.class);
		Mockito.doThrow(new RuntimeException("I'm just a test")).when(mocked).prepare(ArgumentMatchers.any());

		final UserTransaction ut = tm.begin();
		ut.bind(mocked);

		try {
			ut.commit();
			fail("Exception not propagated.");
		} catch (final TARollbackException e) {
		}

		final InOrder inOrder = Mockito.inOrder(mocked);
		inOrder.verify(mocked, Mockito.times(1)).bind((TransactionHandle) ut);
		inOrder.verify(mocked, Mockito.times(1)).prepare((TransactionHandle) ut);
		inOrder.verify(mocked, Mockito.times(1)).rollback((TransactionHandle) ut);
		inOrder.verifyNoMoreInteractions();
	}

	@Test
	public void testJounralClosedAfterPrepare() throws Exception {

		final TAResource res = new TAResource() {

			@Override
			public void rollback(TransactionHandle t) {
				assertTrue("Jounral should be closed during rollback", t.getJournal().isClosed());
			}

			@Override
			public void prepare(TransactionHandle t) throws Exception {
				assertFalse("Jounral should be open during prepare", t.getJournal().isClosed());
			}

			@Override
			public void commit(TransactionHandle t) {
				assertTrue("Jounral should be closed during commit", t.getJournal().isClosed());
			}

			@Override
			public void bind(TransactionHandle t) {
				assertFalse("Jounral should be open during bind", t.getJournal().isClosed());
			}
		};

		UserTransaction ut = tm.begin();
		ut.bind(res);
		ut.commit();

		ut = tm.begin();
		ut.bind(res);
		ut.rollback();
	}

	public static class TestRecoveryHandler implements TARecoveryHandler {
		private static final long serialVersionUID = 547032274505184013L;

		static String tx;
		static TAJournalReader log;
		static Boolean doCommit;

		@Override
		public void recover(String tx, TAJournalReader log, boolean doCommit) {
			assertNull("Recovery handler should only be called once", TestRecoveryHandler.tx);
			TestRecoveryHandler.tx = tx;
			TestRecoveryHandler.log = log;
			TestRecoveryHandler.doCommit = Boolean.valueOf(doCommit);
		}

		static void reset() {
			tx = null;
			log = null;
			doCommit = null;
		}

	}

	/**
	 * Ensure that any registered recovery handlers are called after a fatal
	 * error.
	 */
	@Test
	public void testRecoveryBehaviour() throws Exception {
		final TAResource mocked = Mockito.mock(TAResource.class);
		final TestRecoveryHandler mRecover = new TestRecoveryHandler();
		Mockito.doThrow(new RuntimeException("I'm just a test")).when(mocked).commit(ArgumentMatchers.any());
		Mockito.doAnswer(invocation -> {
			invocation.getArgument(0, TransactionHandle.class).bindRecoveryHandler(mRecover);
			return null;
		}).when(mocked).bind(ArgumentMatchers.any());

		// No jump head-first into a disaster!
		final UserTransaction ut = tm.begin();
		ut.bind(mocked);
		try {
			ut.commit();
			fail("We failed at failing...");
		} catch (final TAFatalError e) {
			// Muhaha!
		}

		// The old transaction manager is now in a dirty state. We do not stop
		// or otherwise touch it to simulate a hard crash.
		assertNull(TestRecoveryHandler.tx);

		// Create a new instance and let it clean up the mess
		final DiskTransactionManager tm2 = new DiskTransactionManager(config);
		tm2.start();
		// Transaction should now be recovered fully.

		try {
			assertEquals(ut.getId(), TestRecoveryHandler.tx);
			assertNotNull(TestRecoveryHandler.log);
			assertEquals(Boolean.TRUE, TestRecoveryHandler.doCommit);
		} finally {
			tm2.shutdown();
			tm.forget((DiskTransaction) ut); // this skips a stacktrace
		}
	}
}
