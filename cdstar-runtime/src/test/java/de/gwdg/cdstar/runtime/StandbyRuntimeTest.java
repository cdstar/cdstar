package de.gwdg.cdstar.runtime;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import de.gwdg.cdstar.config.MapConfig;
import de.gwdg.cdstar.utils.test.TestLogger;

/**
 * The runtime may be initialized, but not started for some functionality.
 */
public class StandbyRuntimeTest {

	@Rule
	public TestLogger log = new TestLogger("de.gwdg");

	@Rule
	public TemporaryFolder tmp = new TemporaryFolder();

	@Test
	public void testClosingUnstartedRuntime() throws Exception {
		final Config cfg = new MapConfig();
		cfg.set("path.home", tmp.newFolder().toString());

		final CDStarRuntime runtime = CDStarRuntime.bootstrap(cfg);

		runtime.close();
	}

}
