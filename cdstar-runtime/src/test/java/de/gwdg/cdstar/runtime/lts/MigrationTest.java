package de.gwdg.cdstar.runtime.lts;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

import de.gwdg.cdstar.runtime.RuntimeBaseTest;
import de.gwdg.cdstar.runtime.RuntimeContext;
import de.gwdg.cdstar.runtime.client.CDStarArchive;
import de.gwdg.cdstar.runtime.client.CDStarMirrorState;
import de.gwdg.cdstar.runtime.client.CDStarProfile;
import de.gwdg.cdstar.runtime.profiles.ProfileRegistry;
import de.gwdg.cdstar.runtime.tasks.TaskService;
import de.gwdg.cdstar.runtime.tasks.TaskServiceTest;

/**
 * Very high-level tests to verify that profile changes have the desired effect.
 */
@RunWith(Parameterized.class)
public class MigrationTest extends RuntimeBaseTest {

	private static final String MORE_FILES = "Multiple files";
	private static final String ONE_FILE = "Single file";
	private static final String EMPTY_FILE = "Empty file";
	private static final String NO_FILES = "No files";

	@Parameters(name = "files={4} snap={5} start={0}({1}) target={2}({3})")
	public static Collection<Object[]> data() {
		final List<String> mirrors = Arrays.asList(null, "m1", "m2");
		final List<String> modes = Arrays.asList(LTSConfig.MODE_HOT, LTSConfig.MODE_COLD);
		final List<String> files = Arrays.asList(NO_FILES, EMPTY_FILE, ONE_FILE, MORE_FILES);
		final List<Boolean> snaps = Arrays.asList(Boolean.TRUE, Boolean.FALSE);

		final List<Object[]> result = new ArrayList<>();
		for (final String fileState : files) {
			for (final String startMirror : mirrors) {
				for (final String startMode : modes) {
					for (final String targetMirror : mirrors) {
						for (final String targetMode : modes) {
							for (Boolean snap : snaps) {
								if (startMirror == null && startMode == "cold")
									continue;
								if (targetMirror == null && targetMode == "cold")
									continue;
								result.add(new Object[] {
										startMirror, startMode, targetMirror, targetMode, fileState, snap });
							}
						}
					}
				}
			}
		}
		return result;
	}

	@Parameter(0)
	public String startMirror;

	@Parameter(1)
	public String startMode;

	@Parameter(2)
	public String targetMirror;

	@Parameter(3)
	public String targetMode;

	@Parameter(4)
	public String filesMode;

	@Parameter(5)
	public Boolean snapshotMode;

	private CDStarProfile startProfile;
	private CDStarProfile targetProfile;
	private FakeTarget startMirrorAdapter;
	private FakeTarget targetMirrorAdapter;

	@Override
	protected void initRuntime(RuntimeContext runtime) throws IOException {
		if (startMirror != null) {
			startMirrorAdapter = new FakeTarget(startMirror);
			runtime.register(startMirrorAdapter);
		}

		if (targetMirror != null && !targetMirror.equals(startMirror)) {
			targetMirrorAdapter = new FakeTarget(targetMirror);
			runtime.register(targetMirrorAdapter);
		}
	}

	@Before
	public void defineProfiles() {
		final ProfileRegistry pr = getRuntime().lookupRequired(ProfileRegistry.class);

		Map<String, String> conf = new HashMap<>();
		if (startMirror != null) {
			conf.put(LTSConfig.PROP_NAME, startMirror);
			conf.put(LTSConfig.PROP_MODE, startMode);
		}
		startProfile = pr.defineProfile(TEST_VAULT, startMirror + startMode, conf);

		conf = new HashMap<>();
		if (targetMirror != null) {
			conf.put(LTSConfig.PROP_NAME, targetMirror);
			conf.put(LTSConfig.PROP_MODE, targetMode);
		}
		targetProfile = pr.defineProfile(TEST_VAULT, targetMirror + targetMode, conf);
	}

	public TaskService getTaskService() {
		return getRuntime().lookupRequired(TaskService.class);
	}

	private void waitForTasks() {
		TaskServiceTest.waitForBackgroundTasks(getTaskService());
	}

	@Test
	public void testProfileChange() throws Exception {
		CDStarArchive ar = makeArchive();

		switch (filesMode) {
		case NO_FILES:
			break;
		case EMPTY_FILE:
			ar.createFile("test.txt");
			break;
		case ONE_FILE:
			writeString(ar.createFile("test.txt"), "Hello World");
			break;
		case MORE_FILES:
			writeString(ar.createFile("test.txt"), "Hello World");
			writeString(ar.createFile("foo/bar/test.txt"), "Hello World");
			break;
		default:
			fail("Invalid parameter: filesMode=" + filesMode);
			break;
		}

		// Set start state
		if (snapshotMode) {
			commit();
			ar = loadArchive(ar.getId());
			ar.createSnapshot("v1").setProfile(startProfile);
		} else {
			ar.getMirrorState().setProfile(startProfile);
		}
		commit();
		waitForTasks();
		ar = loadArchive(ar.getId());
		checkState(ar);

		// Change profile
		if (snapshotMode) {
			ar.getSnapshot("v1").get().setProfile(targetProfile);
		} else {
			ar.getMirrorState().setProfile(targetProfile);
		}
		commit();
		waitForTasks();
		ar = loadArchive(ar.getId());
		checkState(ar);
	}


	private void checkState(CDStarArchive ar) {
		CDStarProfile profile = snapshotMode
				? ar.getSnapshot("v1").get().getProfile()
				: ar.getMirrorState().getProfile();
		CDStarMirrorState state = snapshotMode
				? ar.getSnapshot("v1").get().getMirrorState()
				: ar.getMirrorState();
		boolean isAvailable = snapshotMode
				? ar.getSnapshot("v1").get().isAvailable()
				: ar.getMirrorState().isAvailable();
		long fileCount = snapshotMode
				? ar.getSnapshot("v1").get().getFileCount()
				: ar.getFileCount();

		assertFalse(state.isMigrationPending());

		final LTSConfig mt = LTSConfig.fromProfile(profile).orElse(null);

		if (mt != null && mt.isCold())
			assertFalse("Archive should not be available", isAvailable);
		else
			assertTrue("Archive should be available", isAvailable);

		if (mt != null)
			assertEquals(mt.getName(), state.getMirrorName());
		else
			assertFalse(state.isMirrored());
	}

}
