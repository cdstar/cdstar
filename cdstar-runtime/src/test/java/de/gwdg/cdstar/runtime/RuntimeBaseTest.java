package de.gwdg.cdstar.runtime;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.WritableByteChannel;
import java.nio.charset.StandardCharsets;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.rules.TemporaryFolder;

import de.gwdg.cdstar.auth.Subject;
import de.gwdg.cdstar.auth.UsernamePasswordCredentials;
import de.gwdg.cdstar.auth.simple.Account;
import de.gwdg.cdstar.auth.simple.SimpleAuthorizer;
import de.gwdg.cdstar.config.MapConfig;
import de.gwdg.cdstar.runtime.client.CDStarArchive;
import de.gwdg.cdstar.runtime.client.CDStarFile;
import de.gwdg.cdstar.runtime.client.CDStarSession;
import de.gwdg.cdstar.runtime.client.CDStarVault;
import de.gwdg.cdstar.runtime.client.exc.ArchiveNotFound;
import de.gwdg.cdstar.runtime.client.exc.VaultNotFound;
import de.gwdg.cdstar.ta.exc.TARollbackException;
import de.gwdg.cdstar.utils.test.TestLogger;

public abstract class RuntimeBaseTest {
	public static final String TEST_USER = "TestUser";
	public static final String TEST_GROUP = "TestGroup";
	public static final String TEST_USER2 = "TestUser2";
	public static final String TEST_VAULT = "test";
	private CDStarRuntime runtime;

	@Rule
	public TestLogger log = new TestLogger("de.gwdg");

	@Rule
	public TemporaryFolder tmp = new TemporaryFolder();
	private Subject subject;
	private CDStarSession session;
	private SimpleAuthorizer staticRealm;

	@Before
	final public void setUp() throws Exception {
		runtime = makeRuntime();
	}

	@After
	final public void tearDown() {
		rollback();
		runtime.close();
	}

	CDStarRuntime makeRuntime() throws Exception {
		final Config cfg = new MapConfig();
		cfg.set("path.home", tmp.newFolder().toString());
		cfg.set("vault." + TEST_VAULT + ".create", "true");

		initConfig(cfg);

		final CDStarRuntime runtime = CDStarRuntime.bootstrap(cfg);
		initRuntime(runtime);
		runtime.start();

		staticRealm = runtime.lookupRequired(SimpleAuthorizer.class);
		assertNotNull("Runtime should install default realm", staticRealm);
		assertEquals(1, runtime.getAuthConfig().listRealms().size());
		assertEquals(staticRealm, runtime.getAuthConfig().listRealms().get(0));

		staticRealm.addRole("test_role", "vault:" + TEST_VAULT + ":create", "vault:" + TEST_VAULT + ":read");
		staticRealm.account(TEST_USER).password(TEST_USER).withGroups(TEST_GROUP).withRoles("test_role");
		staticRealm.account(TEST_USER2).password(TEST_USER2).withGroups(TEST_GROUP).withRoles("test_role");

		initAuthRealm(staticRealm);

		return runtime;
	}

	protected void initConfig(Config cfg) throws Exception {
	}

	protected void initRuntime(RuntimeContext runtime) throws Exception {
	}

	protected void initAuthRealm(SimpleAuthorizer realm) throws Exception {
	}

	public CDStarRuntime getRuntime() {
		return runtime;
	}

	protected Account getAccount(String name) {
		return staticRealm.account(name);
	}

	final Subject getDefaultSubject() {
		if (subject == null) {
			subject = runtime.getAuthConfig().getSubject();
			assertTrue(subject.tryLogin(new UsernamePasswordCredentials(TEST_USER, TEST_USER.toCharArray())));
		}
		return subject;
	}

	final void auth(String user, String password) {
		assertTrue(getDefaultSubject().tryLogin(new UsernamePasswordCredentials(user, password.toCharArray())));
	}

	final CDStarSession getDefaultSession() {
		if (session == null)
			session = runtime.getClient(getDefaultSubject()).begin(false);
		return session;
	}

	final CDStarVault getVault() throws VaultNotFound {
		return getDefaultSession().getVault(TEST_VAULT);
	}

	protected final CDStarArchive makeArchive() throws VaultNotFound {
		return getVault().createArchive();
	}

	protected final CDStarArchive loadArchive(String objectId) throws ArchiveNotFound, VaultNotFound {
		return getVault().loadArchive(objectId);
	}

	protected final void commit() throws TARollbackException {
		getDefaultSession().commit();
		session = null;
	}

	protected final void rollback() {
		getDefaultSession().rollback();
		session = null;
	}

	public static void writeString(CDStarFile file, String data) throws IOException {
		try (WritableByteChannel ch = file.getWriteChannel()) {
			ch.write(ByteBuffer.wrap(data.getBytes(StandardCharsets.UTF_8)));
		}
	}

}
