package de.gwdg.cdstar.runtime.search;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;

import org.junit.Test;

public class SplitQueryTest {

	@Test
	public void testTokenizer() {
		assertTokens("a'b'c", "a'b'c");
		assertTokens("a'b c'd", "a'b c'd");
		assertTokens("a 'b c'd", "a", "'b c'd");
		assertTokens("a 'b c' d", "a", "'b c'", "d");
		assertTokens("a b(c(d e) f)d e", "a", "b(c(d e) f)d", "e");
		assertTokens("a b(c[d{ } e] f)d e", "a", "b(c[d{ } e] f)d", "e");
		// Ignore wrong closing parenthesis
		assertTokens("a(b] )c", "a(b] )c");
		// Ignore opening parenthesis in quotes
		assertTokens("a'( ' )", "a'( '", ")");
		// Ignore whitespace prefix/suffix
		assertTokens(" a b c ", "a", "b", "c");
		// Ignore escaped whitespace
		assertTokens("a\\ b c", "a\\ b", "c");
		// Ignore escaped quotes (also, allow quotes that are never closed)
		assertTokens("a\\'b 'c ", "a\\'b", "'c ");
		// Ignore escaped parenthesis (also, allow parenthesis that are never
		// closed)
		assertTokens("a\\(b (c ", "a\\(b", "(c ");
	}

	private void assertTokens(String query, String... tokens) {
		assertEquals(Arrays.asList(tokens), new SplitQuery(query).split());
	}

}
