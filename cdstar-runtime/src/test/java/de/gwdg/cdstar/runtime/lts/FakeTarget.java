package de.gwdg.cdstar.runtime.lts;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.io.IOUtils;

import de.gwdg.cdstar.Utils;

/**
 * A simple {@link LTSTarget} that stores files as byte arrays in memory.
 */
public class FakeTarget implements LTSTarget {

	private final String name;
	private volatile MockableMirrorAdapter mock;
	private final ConcurrentHashMap<String, byte[]> blobs = new ConcurrentHashMap<>();

	private final class FakeImportContext implements ImportContext {
		private final ArchiveReference archive;
		private final LTSLocation location;

		private FakeImportContext(ArchiveReference archive, LTSLocation location) {
			this.archive = archive;
			this.location = location;
			mock.startImport(location, archive);
		}

		@Override
		public CompletableFuture<InputStream> importFile(FileHandle file) {
			return CompletableFuture.supplyAsync(() -> {
				mock.importFile(file);
				return new ByteArrayInputStream(blobs.get(getBlobKey(location.getLocation(), file)));
			});
		}

		@Override
		public void close() {
			mock.closeImport();
		}
	}

	private final class FakeExportContext implements ExportContext {
		String hint = Utils.bytesToHex(Utils.randomBytes(16));

		public FakeExportContext(ArchiveReference archive) {
			mock.startExport(archive);
		}

		@Override
		public CompletableFuture<Void> exportFile(FileHandle file, InputStream stream) {
			return CompletableFuture.runAsync(() -> {
				mock.exportFile(file);
				final byte[] buffer = new byte[(int) file.getSize()];
				blobs.put(getBlobKey(hint, file), buffer);
				try {
					IOUtils.readFully(stream, buffer);
				} catch (final IOException e) {
					throw new MigrationFailedException(e);
				}
			});
		}

		@Override
		public CompletableFuture<LTSLocation> complete() {
			mock.completeExport();
			return CompletableFuture.completedFuture(new LTSLocation(getName(), hint));
		}

		@Override
		public void close() {
			mock.closeExport();
		}
	}

	/**
	 * A synchronous observer that is easier to mock than a {@link LTSTarget}.
	 *
	 */
	public interface MockableMirrorAdapter {

		default void startExport(ArchiveReference archive) {
		}

		default void exportFile(FileHandle file) {
		}

		default void completeExport() {
		}

		default void closeExport() {
		}

		default void startImport(LTSLocation location, ArchiveReference archive) {
		}

		default void removeImport() {
		}

		default void closeImport() {
		}

		default void importFile(FileHandle file) {
		}

	}

	public FakeTarget(String name) throws IOException {
		this.name = name;
		mock = new MockableMirrorAdapter() {
		};
	}

	void setMock(MockableMirrorAdapter mock) {
		this.mock = mock;
	}

	private String getBlobKey(String hint, FileHandle file) {
		return hint + "/" + file.getId();
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public CompletableFuture<ExportContext> startExport(ArchiveHandle archive) {
		return CompletableFuture.supplyAsync(() -> {
			return new FakeExportContext(archive);
		});
	}

	@Override
	public CompletableFuture<ImportContext> startImport(ArchiveReference archive, LTSLocation location) {
		return CompletableFuture.supplyAsync(() -> {
			return new FakeImportContext(archive, location);
		});
	}

	@Override
	public CompletableFuture<Void> markObsolete(ArchiveReference archive, LTSLocation location) {
		return CompletableFuture.runAsync(() -> {
			mock.removeImport();
			blobs.entrySet().removeIf(e -> e.getKey().startsWith(location.getLocation()));
		});

	}
}