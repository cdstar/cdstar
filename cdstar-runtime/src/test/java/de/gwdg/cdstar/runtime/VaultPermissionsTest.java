package de.gwdg.cdstar.runtime;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;

import de.gwdg.cdstar.auth.Credentials;
import de.gwdg.cdstar.auth.simple.SimpleAuthorizer;
import de.gwdg.cdstar.runtime.client.CDStarArchive;
import de.gwdg.cdstar.runtime.client.CDStarSession;
import de.gwdg.cdstar.runtime.client.auth.ArchivePermissionSet;

public class VaultPermissionsTest extends RuntimeBaseTest {

	@Override
	protected void initConfig(Config cfg) {
		cfg.set("vault.public.create", "true");
		cfg.set("vault.public.public", "true");
		cfg.set("vault.private.create", "true");
		cfg.set("vault.privateButReadable.create", "true");
	}

	@Override
	protected void initAuthRealm(SimpleAuthorizer realm) {
		realm.account(TEST_USER)
			.withPermissions("vault:public:create")
			.withPermissions("vault:privateButReadable:read");
	}

	@Test
	public void testVaultPublicState() throws Exception {
		assertFalse(getDefaultSession().getVault(TEST_VAULT).isPublic());
		assertTrue(getDefaultSession().getVault("public").isPublic());
	}

	@Test
	public void testVaultPublicAccess() throws Exception {
		final CDStarArchive ar = getDefaultSession().getVault("public").createArchive();
		ar.getACL().forAny().permit(ArchivePermissionSet.READ);
		commit();

		final CDStarSession anon = getRuntime().getClient((Credentials) null).begin();
		anon.getVault("public").loadArchive(ar.getId());
	}

	@Test
	public void testPublicVaultVisibility() throws Exception {
		final CDStarSession sess = getDefaultSession();
		final List<String> visible = sess.getVaultNames();
		assertTrue(visible.contains("public"));
		assertTrue(visible.contains("privateButReadable"));
		assertFalse(visible.contains("private"));
	}

}
