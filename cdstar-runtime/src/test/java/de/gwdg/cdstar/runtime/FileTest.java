package de.gwdg.cdstar.runtime;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import de.gwdg.cdstar.runtime.client.CDStarArchive;

public class FileTest extends RuntimeBaseTest {

	@Test
	public void removeFileWithMetadata() throws Exception {
		CDStarArchive ar = makeArchive();
		ar.createFile("test").getAttribute("dc:title").append("foo");
		commit();

		ar = loadArchive(ar.getId());
		ar.getFile("test").remove();
		commit();

		ar = loadArchive(ar.getId());
		// triggers attribute loading, which should not fail with a damaged data error.
		ar.getAttribute("what");
	}

	@Test
	public void removeThenCreateFile() throws Exception {
		CDStarArchive ar = makeArchive();
		ar.createFile("test").getAttribute("dc:title").append("foo");
		commit();

		ar = loadArchive(ar.getId());
		ar.getFile("test").remove();
		ar.createFile("test").getAttribute("dc:title").append("bar");
		commit();

		ar = loadArchive(ar.getId());
		assertEquals("bar", ar.getFile("test").getAttribute("dc:title").getFirst());
	}

}
