package de.gwdg.cdstar.runtime.services;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.Collections;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.mockito.Mockito;

import de.gwdg.cdstar.config.MapConfig;
import de.gwdg.cdstar.runtime.RuntimeContext;
import de.gwdg.cdstar.runtime.VaultConfig;
import de.gwdg.cdstar.runtime.listener.VaultConfigListener;
import de.gwdg.cdstar.utils.test.TestLogger;

public class VaultRegistryTest {
	@Rule
	public TestLogger log = new TestLogger("de.gwdg");

	@Rule
	public TemporaryFolder tmp = new TemporaryFolder();

	@Test
	public void testCreateVaultsonDemand() throws Exception {
		RuntimeContext runtimeMock = Mockito.mock(RuntimeContext.class);
		Mockito.when(runtimeMock.getConfig()).thenReturn(new MapConfig().set("vault.test.create", "true"));
		Mockito.when(runtimeMock.lookupAll(VaultConfigListener.class)).thenReturn(Collections.emptyList());
		File data = tmp.newFolder();

		VaultRegistry vr = new VaultRegistry(data.toPath(), this.getClass().getClassLoader());
		vr.onInit(runtimeMock);
		vr.onStartup(runtimeMock);
		assertTrue(vr.getVaultNames().contains("test"));
	}

	@Test
	public void testDiscoverExistingVaults() throws Exception {
		RuntimeContext runtimeMock = Mockito.mock(RuntimeContext.class);
		Mockito.when(runtimeMock.getConfig()).thenReturn(new MapConfig());
		Mockito.when(runtimeMock.lookupAll(VaultConfigListener.class)).thenReturn(Collections.emptyList());

		File data = tmp.newFolder();

		VaultRegistry vr = new VaultRegistry(data.toPath(), this.getClass().getClassLoader());
		vr.onInit(runtimeMock);
		vr.onStartup(runtimeMock);
		vr.createVault("test", new MapConfig());

		VaultRegistry vr2 = new VaultRegistry(data.toPath(), this.getClass().getClassLoader());
		vr2.onInit(runtimeMock);
		vr2.onStartup(runtimeMock); // Does discovery
		assertTrue(vr2.getVaultNames().contains("test"));
	}

	@Test
	public void testKeepVaultsRelocateableByDefault() throws Exception {
		RuntimeContext runtimeMock = Mockito.mock(RuntimeContext.class);
		Mockito.when(runtimeMock.getConfig()).thenReturn(new MapConfig());
		Mockito.when(runtimeMock.lookupAll(VaultConfigListener.class)).thenReturn(Collections.emptyList());

		File data = tmp.newFolder();

		VaultRegistry vr = new VaultRegistry(data.toPath(), this.getClass().getClassLoader());
		vr.onInit(runtimeMock);
		vr.onStartup(runtimeMock);
		VaultConfig cfg = vr.createVault("test", new MapConfig());
		VaultConfig cfg2 = vr.createVault("test2", new MapConfig().set("pool.path", "../"));

		// Make sure all code that may run is run
		vr.getPoolFor(cfg);
		vr.getPoolFor(cfg2);

		// Ensure that only the vault with the explicit path has a property.
		assertNull(cfg.getProperty("pool.path"));
		assertNotNull(cfg2.getProperty("pool.path"));
	}


}
