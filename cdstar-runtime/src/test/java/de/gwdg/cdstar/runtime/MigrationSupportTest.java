package de.gwdg.cdstar.runtime;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

import de.gwdg.cdstar.runtime.client.CDStarArchive;
import de.gwdg.cdstar.runtime.client.LTSMigrationSupport;
import de.gwdg.cdstar.runtime.client.auth.ArchivePermission;
import de.gwdg.cdstar.runtime.client.exc.ArchiveNotAvailable;
import de.gwdg.cdstar.utils.test.TestUtils;

/**
 * Tests for {@link LTSMigrationSupport} for archive migration/recovery
 * functionality.
 *
 * These tests ensure functionality the migration/mirror support services build
 * upon.
 */
public class MigrationSupportTest extends RuntimeBaseTest {

	@Before
	final public void addPermissions() throws Exception {
		getAccount(TEST_USER).withPermissions(
				ArchivePermission.CHANGE_MIRROR.toStringPermission(null, null));
	}

	@Test
	public void testMigrationPendingState() throws Exception {
		// Initial state
		CDStarArchive ar = makeArchive();
		assertTrue(ar.isAvailable());
		assertFalse(ar.isLocked());
		assertFalse(ar.getMirrorState().isMirrored());
		assertFalse(ar.getMirrorState().isMigrationPending());
		commit();

		// Set migration task ID
		ar = loadArchive(ar.getId());
		assertNull(LTSMigrationSupport.getMigrationTaskId(ar.getMirrorState()));
		assertFalse(ar.getMirrorState().isMigrationPending());
		LTSMigrationSupport.setMigrationTaskPending(ar.getMirrorState(), "task-1234");
		assertTrue(ar.isAvailable());
		assertFalse(ar.isLocked());
		assertFalse(ar.getMirrorState().isMirrored());
		assertTrue(ar.getMirrorState().isMigrationPending());
		assertEquals("task-1234", LTSMigrationSupport.getMigrationTaskId(ar.getMirrorState()));
		commit();

		// Clear migration task ID
		ar = loadArchive(ar.getId());
		assertEquals("task-1234", LTSMigrationSupport.getMigrationTaskId(ar.getMirrorState()));
		LTSMigrationSupport.clearMigrationTask(ar.getMirrorState());
		assertTrue(ar.isAvailable());
		assertFalse(ar.isLocked());
		assertFalse(ar.getMirrorState().isMirrored());
		assertFalse(ar.getMirrorState().isMigrationPending());
		commit();

		// Normal state
		ar = loadArchive(ar.getId());
		assertFalse(ar.getMirrorState().isMigrationPending());
		rollback();
	}

	@Test
	public void testArchiveMirrorState() throws Exception {
		CDStarArchive ar = makeArchive();
		LTSMigrationSupport.setMirror(ar.getMirrorState(), "my-mirror", "/dev/null");
		commit();

		ar = loadArchive(ar.getId());
		assertTrue(ar.isAvailable());
		assertTrue(ar.isLocked());
		assertTrue(ar.getMirrorState().isMirrored());
		assertFalse(ar.getMirrorState().isMigrationPending());

		LTSMigrationSupport.clearMirror(ar.getMirrorState());
		commit();

		ar = loadArchive(ar.getId());
		assertTrue(ar.isAvailable());
		assertFalse(ar.isLocked());
		assertFalse(ar.getMirrorState().isMirrored());
		assertFalse(ar.getMirrorState().isMigrationPending());
		rollback();
	}

	@Test
	public void testExternalFiles() throws Exception {
		CDStarArchive ar = makeArchive();
		writeString(ar.createFile("test"), "foo");
		LTSMigrationSupport.setMirror(ar.getMirrorState(), "my-mirror", "/dev/null");
		ar.getFiles().forEach(LTSMigrationSupport::freezeFile);
		commit();

		ar = loadArchive(ar.getId());
		assertFalse(ar.isAvailable());
		assertTrue(ar.isLocked());
		assertTrue(ar.getMirrorState().isMirrored());
		assertFalse(ar.getMirrorState().isMigrationPending());

		LTSMigrationSupport.unfreezeFile(ar.getFile("test"), new ByteArrayInputStream("foo".getBytes()));
		commit();
		ar = loadArchive(ar.getId());
		assertTrue(ar.isAvailable());
		assertTrue(ar.isLocked());
		assertTrue(ar.getMirrorState().isMirrored());
		assertFalse(ar.getMirrorState().isMigrationPending());
	}

	@Test
	public void testNoClearMirrorWhileUnavailable() throws Exception {
		final CDStarArchive ar = makeArchive();
		writeString(ar.createFile("test"), "foo");
		LTSMigrationSupport.setMirror(ar.getMirrorState(), "my-mirror", "123");
		ar.getFiles().forEach(LTSMigrationSupport::freezeFile);
		commit();

		final CDStarArchive ar2 = loadArchive(ar.getId());
		TestUtils.assertRaises(ArchiveNotAvailable.class, () -> {
			LTSMigrationSupport.clearMirror(ar2.getMirrorState());
		});
	}

	@Test
	public void testChecksumsDuringRecovery() throws Exception {
		final CDStarArchive ar = makeArchive();
		writeString(ar.createFile("test"), "foo");
		LTSMigrationSupport.setMirror(ar.getMirrorState(), "my-mirror", "/dev/null");
		ar.getFiles().forEach(LTSMigrationSupport::freezeFile);
		commit();

		final CDStarArchive ar2 = loadArchive(ar.getId());
		assertFalse(ar2.isAvailable());
		TestUtils.assertRaises(IOException.class, () -> {
			LTSMigrationSupport.unfreezeFile(ar2.getFile("test"), new ByteArrayInputStream("fooX".getBytes()));
		});
		LTSMigrationSupport.unfreezeFile(ar2.getFile("test"), new ByteArrayInputStream("foo".getBytes()));
		LTSMigrationSupport.clearMirror(ar2.getMirrorState());
		commit();
	}

}
