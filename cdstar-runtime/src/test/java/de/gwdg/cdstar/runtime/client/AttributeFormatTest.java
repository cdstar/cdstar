package de.gwdg.cdstar.runtime.client;

import static org.junit.Assert.assertEquals;

import java.io.StringWriter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.databind.ObjectMapper;

import de.gwdg.cdstar.runtime.client.AttributeJsonFormat;

public class AttributeFormatTest {

	JsonFactory jsonFactory = new ObjectMapper().getFactory();

	@Test
	public void roundTripTest() throws Exception {
		final AttributeJsonFormat<List<String>> jsonFormat = new AttributeJsonFormat<List<String>>() {

			@Override
			public List<String> makeAttr(String fileId, String attrName, List<String> values) {
				return values;
			}

			@Override
			public List<String> getValues(List<String> attr) {
				return attr;
			}

		};

		final Map<String, Map<String, List<String>>> fooMap = new HashMap<>();
		fooMap.put("foo", new HashMap<>());
		fooMap.get("foo").put("bar", Arrays.asList("baz1", "baz2"));

		final StringWriter sw = new StringWriter();

		jsonFormat.serialize(jsonFactory.createGenerator(sw), fooMap);
		sw.flush();
		final String json = sw.getBuffer().toString();

		final Map<String, Map<String, List<String>>> result = jsonFormat
			.parse(jsonFactory.createParser(json));

		assertEquals(fooMap, result);
	}

}
