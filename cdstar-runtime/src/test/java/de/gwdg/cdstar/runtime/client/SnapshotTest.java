package de.gwdg.cdstar.runtime.client;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;

import de.gwdg.cdstar.pool.PoolError;
import de.gwdg.cdstar.runtime.RuntimeBaseTest;
import de.gwdg.cdstar.runtime.client.exc.InvalidSnapshotName;
import de.gwdg.cdstar.runtime.listener.ArchiveListener;
import de.gwdg.cdstar.utils.test.TestUtils;

public class SnapshotTest extends RuntimeBaseTest {

	@Test
	public void testEmptySnapshot() throws Exception {
		CDStarArchive ar = makeArchive();
		commit();

		ar = loadArchive(ar.getId());
		ar.createSnapshot("v1");
		commit();

		ar = loadArchive(ar.getId());
		CDStarSnapshot snap = ar.getSnapshot("v1").get();
		assertEquals("v1", snap.getName());
		assertEquals(ar, snap.getSource());
	}

	@Test
	/**
	 * Snapshots that are never committed should not appear in the snapshot list.
	 */
	public void testDeleteNewSnapshot() throws Exception {
		CDStarArchive ar = makeArchive();
		commit();

		ar = loadArchive(ar.getId());
		ar.createSnapshot("v1").remove();
		commit();

		ar = loadArchive(ar.getId());
		assertFalse(ar.getSnapshot("v1").isPresent());
		ar.createSnapshot("v1"); // should not fail
	}

	@Test
	public void testDeletedSnapshotsStillVisible() throws Exception {
		CDStarArchive ar = makeArchive();
		commit();

		ar = loadArchive(ar.getId());
		ar.createSnapshot("v1");
		commit();

		ar = loadArchive(ar.getId());
		ar.getSnapshot("v1").get().remove();
		commit();

		ar = loadArchive(ar.getId());
		assertTrue(ar.getSnapshot("v1").get().isRemoved());
	}

	@Test
	public void failSnapshotIfSourceModified() throws Exception {
		CDStarArchive ar = makeArchive();
		TestUtils.assertRaises(IllegalStateException.class, () -> ar.createSnapshot("v1"));
	}

	@Test
	public void failOnBadNames() throws Exception {
		String id = makeArchive().getId();
		commit();
		CDStarArchive ar = loadArchive(id);
		TestUtils.assertRaises(InvalidSnapshotName.class, () -> ar.createSnapshot("/bad/name"));
		TestUtils.assertRaises(InvalidSnapshotName.class, () -> ar.createSnapshot(" badname "));
		TestUtils.assertRaises(InvalidSnapshotName.class, () -> ar.createSnapshot("bäd"));
		TestUtils.assertRaises(InvalidSnapshotName.class, () -> ar.createSnapshot(""));
		TestUtils.assertRaises(NullPointerException.class, () -> ar.createSnapshot(null));

		String duplicate = "v1.0-beta";
		ar.createSnapshot(duplicate);
		TestUtils.assertRaises(InvalidSnapshotName.class, () -> ar.createSnapshot(duplicate));
	}

	@Test
	public void testSnapshotPayload() throws Exception {
		CDStarArchive ar = makeArchive();
		commit();
		ar = loadArchive(ar.getId());
		ar.createSnapshot("v1");
		ar.createFile("test1").getAttribute("v").set("v1");
		ar.getAttribute("v").set("v1");
		commit();
		ar = loadArchive(ar.getId());
		ar.createSnapshot("v2");
		commit();
		ar = loadArchive(ar.getId());
		ar.getFile("test1").remove();
		ar.getAttributes().forEach(a -> a.clear());
		commit();
		ar = loadArchive(ar.getId());
		assertEquals(0, ar.getFileCount());
		assertEquals(0, ar.getAttributeNames().size());
		CDStarSnapshot snap = ar.getSnapshot("v1").get();
		assertEquals(0, snap.getFileCount());
		assertEquals(0, snap.getAttributeNames().size());
		snap = ar.getSnapshot("v2").get();
		assertEquals(1, snap.getFileCount());
		assertEquals("v1", snap.getFile("test1").getAttribute("v").get(0));
		assertEquals("v1", snap.getAttribute("v").get(0));
	}

	@Test
	public void testSnapBeforeEdit() throws Exception {
		CDStarArchive ar = makeArchive();
		ar.createFile("test.txt");
		commit();
		ar = loadArchive(ar.getId());
		ar.createSnapshot("v1");
		ar.createFile("test2.txt");
		commit();
		ar = loadArchive(ar.getId());
		assertEquals(2, ar.getFileCount());
		assertEquals(1, ar.getSnapshot("v1").get().getFileCount());
	}

	@Test
	public void testListeners() throws Exception {
		ArchiveListener listenerMock = Mockito.mock(ArchiveListener.class);

		CDStarArchive ar = makeArchive();
		commit();

		ar = loadArchive(ar.getId());
		ar.addListener(listenerMock);
		CDStarSnapshot snap = ar.createSnapshot("v1");
		Mockito.verify(listenerMock).snapshotCreated(ArgumentMatchers.eq(snap));
		commit();

		ar = loadArchive(ar.getId());
		ar.addListener(listenerMock);
		snap = ar.getSnapshot("v1").get();
		snap.remove();
		Mockito.verify(listenerMock).snapshotRemoved(ArgumentMatchers.eq(snap));
		commit();
	}

	@Test
	public void testDedicatedCleanedUpAfterRemove() throws Exception {
		CDStarArchive ar = makeArchive();
		String arId = ar.getId();
		commit();

		ar = loadArchive(arId);
		CDStarSnapshot snap1 = ar.createSnapshot("v1");
		CDStarSnapshot snap2 = ar.createSnapshot("v2");
		String stoRef1 = ((SnapshotImpl) snap1).getMeta().ref;
		String stoRef2 = ((SnapshotImpl) snap2).getMeta().ref;
		commit();

		// Removing a snapshot also removes its storage object
		ar = loadArchive(arId);
		ar.getSnapshot("v1").get().remove();
		commit();
		VaultImpl vault = ((VaultImpl) loadArchive(arId).getVault());
		TestUtils.assertRaises(PoolError.NotFound.class, () -> vault.loadDirect(stoRef1));

		// Removing the archive removes all snapshots
		ar = loadArchive(arId);
		ar.remove();
		commit();
		VaultImpl vault2 = ((VaultImpl) makeArchive().getVault());
		TestUtils.assertRaises(PoolError.NotFound.class, () -> vault2.loadDirect(stoRef2));
		TestUtils.assertRaises(PoolError.NotFound.class, () -> vault2.loadDirect(arId));
	}

}