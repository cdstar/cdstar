package de.gwdg.cdstar.runtime.tasks;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.function.LongSupplier;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;

import com.codahale.metrics.MetricRegistry;

import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.runtime.RuntimeContext;
import de.gwdg.cdstar.runtime.services.CronFeature.CronServiceImpl;
import de.gwdg.cdstar.runtime.services.CronService;
import de.gwdg.cdstar.runtime.services.PoolService;
import de.gwdg.cdstar.runtime.services.PoolServiceImpl;
import de.gwdg.cdstar.runtime.tasks.TaskIOHelper.TaskModel;
import de.gwdg.cdstar.ta.UserTransaction;
import de.gwdg.cdstar.tm.DiskJournal;
import de.gwdg.cdstar.tm.DiskJournalReader;
import de.gwdg.cdstar.tm.DiskTransactionManager;
import de.gwdg.cdstar.utils.test.TestLogger;

public class TaskServiceTest {

	@Rule
	public TestLogger log = new TestLogger("de.gwdg");

	@Rule
	public TemporaryFolder tmp = new TemporaryFolder();
	private Path taskPath;
	private CronServiceImpl cronService;
	private PoolServiceImpl poolService;

	@Before
	public void makePersistFolder() throws IOException {
		taskPath = tmp.newFolder().toPath();
		cronService = new CronServiceImpl();
		poolService = new PoolServiceImpl();
	}

	@After
	public void shutdwn() {
		Utils.closeQuietly(cronService);
		Utils.closeQuietly(poolService);
	}

	TaskServiceImpl initWithMockedRuntime() throws IOException, Exception {
		final TaskServiceImpl ts = new TaskServiceImpl();

		// Reduce delays for testing
		ts.setDelay(100, 1000, TimeUnit.MILLISECONDS);

		final TaskRunner dummyRunner = new TaskRunner() {
			@Override
			public boolean canRun(Task task) {
				return task.getName().equals("dummy");
			}

			@Override
			public CompletableFuture<Void> run(Task task) throws Exception {
				return CompletableFuture.completedFuture(null);
			}
		};

		final RuntimeContext mCtx = Mockito.mock(RuntimeContext.class);
		Mockito.when(mCtx.getServiceDir(ArgumentMatchers.anyString())).thenReturn(taskPath);
		Mockito.when(mCtx.lookupRequired(PoolService.class)).thenReturn(poolService);
		Mockito.when(mCtx.lookupRequired(CronService.class)).thenReturn(cronService);
		Mockito.when(mCtx.lookupRequired(MetricRegistry.class)).thenReturn(new MetricRegistry());
		Mockito.when(mCtx.lookupAll(TaskRunner.class)).thenReturn(Arrays.asList(dummyRunner));

		ts.onInit(mCtx);
		ts.onStartup(mCtx);
		Mockito.verify(mCtx, Mockito.times(1)).getServiceDir(ArgumentMatchers.anyString());
		Mockito.verify(mCtx, Mockito.times(1)).lookupAll(TaskRunner.class);
		Mockito.verify(mCtx, Mockito.times(1)).lookupRequired(PoolService.class);
		Mockito.verify(mCtx, Mockito.times(1)).lookupRequired(CronService.class);
		Mockito.verify(mCtx, Mockito.times(1)).lookupRequired(MetricRegistry.class);
		Mockito.verifyNoMoreInteractions(mCtx);

		return ts;
	}

	/**
	 * Wait for all scheduled {@link TaskService} background tasks to complete
	 * (no longer than 10 seconds).
	 */
	public static void waitForBackgroundTasks(TaskService tasks) {
		TaskServiceImpl ts;
		if (tasks instanceof TaskServiceImpl)
			ts = (TaskServiceImpl) tasks;
		else
			throw new IllegalArgumentException("Task service must implement: " + TaskServiceImpl.class.getName());

		final int n = 1000, sleep = 10;

		final LongSupplier unfinishedTasks = () -> ts.getTasksTotal() - ts.getTasksFinished() - ts.getTasksFailed();

		for (int i = 0; i < n && unfinishedTasks.getAsLong() > 0; i++) {
			if (!Utils.sleepInterruptable(sleep))
				break;
		}

		assertEquals("Background tasks took longer than " + n * sleep + "ms to complete.", 0,
			unfinishedTasks.getAsLong());
	}

	TaskModel mkTask(String name) {
		final Map<String, String> params = new HashMap<>();
		params.put("param1", "value1");
		params.put("param2", "value2");
		return new TaskIOHelper.TaskModel(UUID.randomUUID().toString(), name, params);
	}

	@Test
	public void testTaskIoHelper() throws IOException {
		final TaskIOHelper io = new TaskIOHelper(taskPath);
		final TaskModel preparedTask = mkTask("dummy");
		final TaskModel submittedTask = mkTask("dummy");

		io.prepare(preparedTask.getId(), preparedTask.getName(), preparedTask.getParameterMap());
		assertTrue(io.isPrepared(preparedTask.getId()));

		final List<String> tasks = new ArrayList<>();
		io.findCommittedTaskIDs(tasks::add);
		assertTrue(tasks.isEmpty());

		io.prepare(submittedTask.getId(), submittedTask.getName(), submittedTask.getParameterMap());
		io.submit(submittedTask.getId());
		assertFalse(io.isPrepared(submittedTask.getId()));

		io.findCommittedTaskIDs(tasks::add);
		assertEquals(Arrays.asList(submittedTask.getId()), tasks);

		final TaskModel loaded = io.loadTask(submittedTask.getId());
		assertEquals(submittedTask.getId(), loaded.getId());
		assertEquals(submittedTask.getName(), loaded.getName());
		assertEquals(submittedTask.getParameterMap(), loaded.getParameterMap());

		io.forget(submittedTask.getId(), true);
		io.forget(preparedTask.getId(), false);

		assertFalse(io.isPrepared(submittedTask.getId()));
		assertFalse(io.isPrepared(preparedTask.getId()));
		assertEquals(0, Files.list(taskPath).count());
	}

	@Test
	public void testPickupSubmittedTasks() throws Exception {
		final TaskIOHelper io = new TaskIOHelper(taskPath);
		final TaskModel task = mkTask("dummy");
		io.prepare(task.getId(), task.getName(), task.getParameterMap());
		io.submit(task.getId());

		final TaskServiceImpl ts = initWithMockedRuntime();
		waitForBackgroundTasks(ts);

		assertEquals(1, ts.getTasksTotal());
		assertEquals(1, ts.getTasksFinished());
	}

	@Test
	public void testRepeatFailedTest() throws Exception {
		final TaskServiceImpl ts = initWithMockedRuntime();
		final CountDownLatch latch = new CountDownLatch(2);

		final TaskRunner runner = new TaskRunner() {
			@Override
			public CompletableFuture<Void> run(Task task) throws Exception {
				if (latch.getCount() > 1) {
					latch.countDown();
					throw new IllegalStateException("Will fail once, then succeed");
				}
				assertEquals("value", task.getOptional("param").get());
				latch.countDown();
				return CompletableFuture.completedFuture(null);
			}
		};
		ts.addRunner(runner);

		final PreparedTask prepared = ts.builder(runner.getClass()).param("param", "value").prepare();

		log.assertEmitsLog("Failed tasks should be logged", event -> {
			return event.getMessage().contains("Task failed") && event.getFormattedMessage().contains(prepared.getId());
		}, () -> {
			prepared.submit();
			assertTrue(latch.await(1, TimeUnit.SECONDS));
		});
	}

	@Test
	public void testTransactional() throws Exception {
		final DiskTransactionManager tm = new DiskTransactionManager(tmp.newFolder().toPath());
		tm.start();

		final TaskServiceImpl tr = initWithMockedRuntime();
		final UserTransaction ut = tm.begin();

		final CountDownLatch latch = new CountDownLatch(2);
		tr.addRunner(new TaskRunner() {

			@Override
			public boolean canRun(Task task) {
				return task.getName().equals("foo");
			}

			@Override
			public CompletableFuture<Void> run(Task task) throws Exception {
				latch.countDown();
				return CompletableFuture.completedFuture(null);
			}
		});

		assertEquals(2, latch.getCount());
		tr.builder("foo").bind(ut);
		tr.builder("foo").bind(ut);
		assertFalse(latch.await(100, TimeUnit.MILLISECONDS));
		assertEquals(2, latch.getCount());
		ut.commit();
		assertTrue(latch.await(1, TimeUnit.SECONDS));
	}

	@Test
	public void testTransactionalRollback() throws Exception {
		final DiskTransactionManager tm = new DiskTransactionManager(tmp.newFolder().toPath());
		tm.start();

		final TaskServiceImpl tr = initWithMockedRuntime();
		final UserTransaction ut = tm.begin();

		final Task task = tr.builder("foo").prepare().bind(ut);
		ut.rollback();
		assertTrue(task.isCanceled());
	}

	@Test
	public void testRecoveryAdapterCommit() throws Exception {
		testRecoveryAdapter(true);
	}

	@Test
	public void testRecoveryAdapterNoCommit() throws Exception {
		testRecoveryAdapter(false);
	}

	void testRecoveryAdapter(boolean doCommit) throws Exception {
		final TaskRecoveryHandler recoveryHandler = new TaskRecoveryHandler(taskPath);
		final TaskIOHelper io = new TaskIOHelper(taskPath);
		final DiskJournal wal = new DiskJournal(tmp.newFolder().toPath().resolve("test.wal"));
		final TaskModel task = mkTask("my task");

		io.prepare(task.getId(), task.getName(), task.getParameterMap());
		recoveryHandler.rememberTask(wal, task.getId());
		wal.flush();

		assertTrue(io.isPrepared(task.getId()));

		recoveryHandler.recover("tx-id", new DiskJournalReader(wal), doCommit);

		assertFalse(io.isPrepared(task.getId()));

		final List<String> ids = new ArrayList<>();
		io.findCommittedTaskIDs(ids::add);
		assertEquals(doCommit ? 1 : 0, ids.size());
	}

}
