package de.gwdg.cdstar.runtime;

import java.util.Arrays;
import java.util.Collections;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;

import de.gwdg.cdstar.config.MapConfig;
import de.gwdg.cdstar.runtime.client.CDStarArchive;
import de.gwdg.cdstar.runtime.client.CDStarAttribute;
import de.gwdg.cdstar.runtime.client.CDStarFile;
import de.gwdg.cdstar.runtime.client.CDStarSession;
import de.gwdg.cdstar.runtime.client.CDStarVault;
import de.gwdg.cdstar.runtime.listener.ArchiveListener;
import de.gwdg.cdstar.runtime.listener.RuntimeListener;
import de.gwdg.cdstar.runtime.listener.SessionListener;
import de.gwdg.cdstar.runtime.listener.SessionStartListener;
import de.gwdg.cdstar.runtime.listener.VaultListener;
import de.gwdg.cdstar.utils.test.TestLogger;

public class ListenerTest {
	@Rule
	public TestLogger log = new TestLogger("de.gwdg");

	@Rule
	public TemporaryFolder tmp = new TemporaryFolder();

	private CDStarRuntime runtime;

	@Before
	public void prepareRuntime() throws Exception {
		final Config cfg = new MapConfig();
		cfg.set("path.home", tmp.newFolder().toString());
		cfg.set("vault.test.create", "true");
		runtime = CDStarRuntime.bootstrap(cfg);
	}

	CDStarSession getSession() {
		return runtime.getClient(runtime.getSystemUser("test")).begin();
	}

	@After
	public void stutdownRuntime() {
		runtime.close();
	}

	@Test
	public void testRuntimeListener() throws Exception {
		final RuntimeListener listener = Mockito.mock(RuntimeListener.class);
		runtime.register(listener);
		Mockito.verify(listener).onInit(runtime);
		Mockito.verifyNoMoreInteractions(listener);

		runtime.start();
		Mockito.verify(listener).onStartup(runtime);
		Mockito.verifyNoMoreInteractions(listener);

		runtime.close();
		Mockito.verify(listener).onShutdown(runtime);
		Mockito.verifyNoMoreInteractions(listener);
	}

	@Test
	public void testSessionStartListener() throws Exception {
		final SessionStartListener listener = Mockito.mock(SessionStartListener.class);
		runtime.register(listener);
		runtime.start();

		Mockito.verifyNoMoreInteractions(listener);

		final CDStarSession ses = getSession();
		Mockito.verify(listener).onSessionStarted(ses);
		Mockito.verifyNoMoreInteractions(listener);

		ses.getVault("test").createArchive();
		ses.commit();
		Mockito.verifyNoMoreInteractions(listener);
	}

	@Test
	public void testSessionListenerCommit() throws Exception {
		final SessionListener listener = Mockito.mock(SessionListener.class);
		runtime.register(listener);
		runtime.start();

		final CDStarSession ses = getSession();
		ses.getVault("test").createArchive();
		ses.commit();

		Mockito.verify(listener)
				.onVaultOpenend(ArgumentMatchers.argThat((CDStarVault t) -> t.getName().equals("test")));
		Mockito.verify(listener).onPrepare(ArgumentMatchers.notNull());
		Mockito.verify(listener).onCommit(ArgumentMatchers.notNull());
		Mockito.verifyNoMoreInteractions(listener);
	}

	@Test
	public void testSessionListenerRollback() throws Exception {
		final SessionListener listener = Mockito.mock(SessionListener.class);
		runtime.register(listener);
		runtime.start();

		final CDStarSession ses = getSession();
		ses.getVault("test").createArchive();
		ses.rollback();

		Mockito.verify(listener)
				.onVaultOpenend(ArgumentMatchers.argThat((CDStarVault t) -> t.getName().equals("test")));
		Mockito.verify(listener).onRollback(ArgumentMatchers.notNull());
		Mockito.verifyNoMoreInteractions(listener);
	}

	@Test
	public void testVaultListener() throws Exception {
		final VaultListener listener = Mockito.mock(VaultListener.class);
		runtime.register(listener);
		runtime.start();

		final CDStarSession ses = getSession();
		final CDStarArchive ar = ses.getVault("test").createArchive();
		ses.commit();

		Mockito.verify(listener).archiveLoaded(ArgumentMatchers.argThat(a -> a.equals(ar)));
		Mockito.verifyNoMoreInteractions(listener);

		final CDStarSession ses2 = getSession();
		final CDStarArchive ar2 = ses2.getVault("test").loadArchive(ar.getId());
		ses2.rollback();

		Mockito.verify(listener).archiveLoaded(ArgumentMatchers.argThat(a -> a.equals(ar2)));
		Mockito.verifyNoMoreInteractions(listener);
	}

	@Test
	public void testArchiveListener() throws Exception {
		final ArchiveListener listener = Mockito.mock(ArchiveListener.class);
		runtime.register(listener);
		runtime.start();

		final CDStarSession ses = getSession();
		CDStarArchive ar = ses.getVault("test").createArchive();

		CDStarAttribute arg = ar.getAttribute("dc:title");
		arg.set("myArchive");
		CDStarFile fh = ar.createFile("test.txt");
		RuntimeBaseTest.writeString(fh, "Hello World");
		CDStarAttribute arg2 = fh.getAttribute("dc:title");
		arg2.set("myFile");

		ses.commit();

		Mockito.verify(listener).fileCreated(fh);
		Mockito.verify(listener).fileSizeChanged(fh, 11);
		Mockito.verify(listener).propertyChanged(arg, null, Collections.emptyList());
		Mockito.verify(listener).propertyChanged(arg2, fh, Collections.emptyList());
		Mockito.verifyNoMoreInteractions(listener);

		final CDStarSession ses2 = getSession();

		ar = ses2.getVault("test").loadArchive(ar.getId());
		arg = ar.getAttribute("dc:title");
		arg.set("myArchive2");
		fh = ar.getFile("test.txt");
		fh.setName("test2.txt");
		RuntimeBaseTest.writeString(fh, "!");
		arg2 = fh.getAttribute("dc:title");
		arg2.set("myFile2");

		ses2.commit();

		Mockito.verify(listener).fileNameChanged(fh, "test.txt");
		Mockito.verify(listener).fileSizeChanged(fh, 1);
		Mockito.verify(listener).propertyChanged(arg, null, Arrays.asList("myArchive"));
		Mockito.verify(listener).propertyChanged(arg2, fh, Arrays.asList("myFile"));
		Mockito.verifyNoMoreInteractions(listener);
	}

}
