package de.gwdg.cdstar.runtime;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import org.junit.Test;

import de.gwdg.cdstar.event.ChangeEvent;
import de.gwdg.cdstar.runtime.client.CDStarArchive;
import de.gwdg.cdstar.runtime.filter.AbstractEventFilter;

public class EventFilterTests extends RuntimeBaseTest {

	private ArrayList<ChangeEvent> events;

	@Override
	protected void initRuntime(RuntimeContext runtime) {
		events = new ArrayList<>();
		runtime.register(new AbstractEventFilter() {

			@Override
			public void triggerEvent(ChangeEvent event) {
				events.add(event);
			}
		});
	}

	@Test
	public void testCreateArchive() throws Exception {
		final CDStarArchive ar = makeArchive();
		commit();

		assertEquals(1, events.size());
		final ChangeEvent event = events.get(0);
		assertEquals(TEST_VAULT, event.getVault());
		assertEquals(ar.getId(), event.getArchive());
	}

	@Test
	public void testCreateRemoveArchive() throws Exception {
		makeArchive().remove();
		commit();

		assertEquals(0, events.size());
	}

	@Test
	public void testCreateCommitRemoveArchive() throws Exception {
		final CDStarArchive a = makeArchive();
		commit();

		loadArchive(a.getId()).remove();
		commit();

		assertEquals(2, events.size());
	}

}
