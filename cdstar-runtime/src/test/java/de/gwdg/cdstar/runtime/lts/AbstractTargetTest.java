package de.gwdg.cdstar.runtime.lts;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import de.gwdg.cdstar.config.MapConfig;
import de.gwdg.cdstar.runtime.Config;
import de.gwdg.cdstar.runtime.RuntimeBaseTest;
import de.gwdg.cdstar.runtime.RuntimeContext;
import de.gwdg.cdstar.runtime.client.CDStarArchive;
import de.gwdg.cdstar.runtime.client.CDStarProfile;
import de.gwdg.cdstar.runtime.tasks.TaskService;
import de.gwdg.cdstar.runtime.tasks.TaskServiceTest;

/**
 * Very high-level tests to verify that profile changes have the desired effect.
 */
public abstract class AbstractTargetTest<T extends LTSTarget> extends RuntimeBaseTest {

	private T ltsTarget;

	public abstract T createLTSTarget() throws Exception;

	@Override
	protected void initConfig(Config cfg) throws Exception {
		cfg.with("profile", "test-profile").setAll(new MapConfig()
			.set(LTSConfig.PROP_NAME, "test-mirror")
			.set(LTSConfig.PROP_MODE, LTSConfig.MODE_COLD));
	}

	@Override
	protected void initRuntime(RuntimeContext runtime) throws Exception {
		runtime.register(ltsTarget = createLTSTarget());
	}

	public T getLtsTarget() {
		return ltsTarget;
	}

	@Test
	public void testExportImportCycle() throws Exception {

		CDStarArchive ar = makeArchive();
		writeString(ar.createFile("folder/file.txt"), "Hello World!");
		ar.setProfile(ar.getVault().getProfileByName("test-profile"));
		commit();

		TaskServiceTest.waitForBackgroundTasks(getRuntime().lookupRequired(TaskService.class));

		ar = loadArchive(ar.getId());
		assertTrue(ar.getMirrorState().hasExternalLocation());
		assertFalse(ar.isAvailable());
		assertFalse(ar.getFile("folder/file.txt").isAvailable());
		ar.setProfile(ar.getVault().getProfileByName(CDStarProfile.DEFAULT_NAME));
		commit();

		TaskServiceTest.waitForBackgroundTasks(getRuntime().lookupRequired(TaskService.class));

		ar = loadArchive(ar.getId());
		assertFalse(ar.getMirrorState().hasExternalLocation());
		assertTrue(ar.isAvailable());
		assertTrue(ar.getFile("folder/file.txt").isAvailable());
	}

}
