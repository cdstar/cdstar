package de.gwdg.cdstar.runtime;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Date;

import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.mockito.internal.util.collections.Sets;

import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.runtime.client.CDStarArchive;
import de.gwdg.cdstar.runtime.client.CDStarFile;
import de.gwdg.cdstar.runtime.client.CDStarProfile;
import de.gwdg.cdstar.runtime.client.auth.ArchivePermission;
import de.gwdg.cdstar.runtime.client.auth.ArchivePermissionSet;
import de.gwdg.cdstar.runtime.client.auth.StringSubject;
import de.gwdg.cdstar.runtime.client.auth.VaultPermission;
import de.gwdg.cdstar.runtime.client.exc.AccessError;
import de.gwdg.cdstar.runtime.client.exc.ConstraintError;
import de.gwdg.cdstar.runtime.client.exc.FileExists;

public class ArchiveTest extends RuntimeBaseTest {

	@Test
	public void testArchiveInitialState() throws Exception {
		final Date t1 = new Date();
		final CDStarArchive ar = makeArchive();
		assertEquals("0", ar.getRev());
		assertEquals("0", ar.getNextRev());
		assertEquals(null, ar.getParentRev());
		assertEquals(ar.getRev(), ar.getNextRev());
		assertEquals(getDefaultSubject().getPrincipal().getFullId(), ar.getOwner());
		assertTrue(t1.getTime() <= ar.getCreated().getTime());
		assertTrue(ar.getCreated().getTime() <= ar.getContentModified().getTime());
		assertTrue(ar.isModified());
		assertTrue(ar.getFiles().isEmpty());
		assertTrue(ar.getACL().forOwner().isPermitted(ArchivePermissionSet.OWNER.getPermissions()));
		assertTrue(ar.getAttributes().isEmpty());
		assertEquals(CDStarProfile.DEFAULT_NAME, ar.getProfile().getName());
	}

	@Test
	public void testArchiveStateAfterCommit() throws Exception {
		final CDStarArchive ar = makeArchive();

		commit();

		assertEquals("0", ar.getRev());
		assertEquals("0", ar.getNextRev());
		assertEquals(null, ar.getParentRev());

		assertEquals(getDefaultSubject().getPrincipal().getFullId(), ar.getOwner());
		// assertTrue(t1.getTime() <= ar.getCreated().getTime());
		// assertTrue(ar.getCreated().getTime() <= t2.getTime());
		// assertTrue(t2.getTime() <= ar.getLastModified().getTime());
		assertTrue(ar.getContentModified().getTime() <= new Date().getTime());
		assertTrue(ar.isModified());
		assertTrue(ar.getFiles().isEmpty());
		assertTrue(ar.getACL().forOwner().isPermitted(ArchivePermissionSet.OWNER.getPermissions()));
		assertTrue(ar.getAttributes().isEmpty());
		assertEquals(CDStarProfile.DEFAULT_NAME, ar.getProfile().getName());
	}

	@Test
	public void testCreateArchiveWithID() throws Exception {
		assertThrows(AccessError.class, () -> getVault().createArchive("0123456789abcdef"));
		getAccount(TEST_USER).withPermissions(VaultPermission.CREATE_ID.toStringPermission(TEST_VAULT));

		assertThrows(ConstraintError.class, () -> getVault().createArchive("invalid"));
		assertThrows(ConstraintError.class, () -> getVault().createArchive(""));

		final CDStarArchive ar = getVault().createArchive("0123456789abcdef");
		assertEquals("0123456789abcdef", ar.getId());
		commit();
	}

	@Test
	public void testRevisionValues() throws Exception {

		CDStarArchive ar = makeArchive();
		assertEquals("0", ar.getRev());
		assertEquals("0", ar.getNextRev());
		assertEquals(null, ar.getParentRev());
		commit();
		assertEquals("0", ar.getRev());
		assertEquals("0", ar.getNextRev());
		assertEquals(null, ar.getParentRev());

		ar = loadArchive(ar.getId());
		assertEquals("0", ar.getRev());
		assertEquals("0", ar.getNextRev());
		assertEquals(null, ar.getParentRev());
		ar.createFile("test");
		assertEquals("0", ar.getRev());
		assertEquals("1", ar.getNextRev());
		assertEquals(null, ar.getParentRev());
		commit();
		assertEquals("0", ar.getRev());
		assertEquals("1", ar.getNextRev());
		assertEquals(null, ar.getParentRev());

		ar = loadArchive(ar.getId());
		assertEquals("1", ar.getRev());
		assertEquals("1", ar.getNextRev());
		assertEquals("0", ar.getParentRev());
		ar.createFile("test2");
		assertEquals("1", ar.getRev());
		assertEquals("2", ar.getNextRev());
		assertEquals("0", ar.getParentRev());
		commit();
		assertEquals("1", ar.getRev());
		assertEquals("2", ar.getNextRev());
		assertEquals("0", ar.getParentRev());
	}

	@Test
	public void testArchiveWriteProtectAfterCommit() throws Exception {
		final CDStarArchive ar = makeArchive();
		commit();

		assertThrows(IllegalStateException.class, () -> {
			ar.createFile("test:txt");
		});
		assertThrows(IllegalStateException.class, () -> {
			ar.getACL().forPrincipal("alice").revoke(ArchivePermission.LOAD);
		});
		assertThrows(IllegalStateException.class, () -> {
			ar.getACL().forPrincipal("newPrincipal").permit(ArchivePermission.LOAD);
		});
		assertThrows(IllegalStateException.class, () -> {
			ar.setAttribute("sys:test", "testValue");
		});
		assertThrows(IllegalStateException.class, () -> {
			ar.setOwner("alice");
		});
		assertThrows(IllegalStateException.class, () -> {
			ar.remove();
		});
	}

	@Test
	public void testCreateFile() throws Exception {
		final CDStarArchive ar = makeArchive();
		final CDStarFile f = ar.createFile("test.txt");
		f.setMediaType("text/plain");
		writeString(f, "abc");

		f.setAttribute("sys:test", "testValue");
		assertEquals(f, ar.getFile("test.txt"));
		assertEquals(3, f.getSize());
		assertEquals("abc", IOUtils.toString(f.getStream(), "UTF-8"));
		assertEquals("text/plain", f.getMediaType());
		assertEquals("test.txt", f.getName());

		commit();
		assertEquals(f, ar.getFile("test.txt"));
		assertEquals(3, f.getSize());
		assertEquals("text/plain", f.getMediaType());
		assertEquals("test.txt", f.getName());

		assertThrows(IllegalStateException.class, () -> f.getStream());
		assertThrows(IllegalStateException.class, () -> writeString(f, "xxx"));

	}

	@Test
	public void testCreateFileNameConflict() throws Exception {
		final CDStarArchive ar = makeArchive();

		ar.createFile("test.txt");
		assertThrows(FileExists.class, () -> ar.createFile("test.txt"));
		ar.createFile("a");
		assertThrows(FileExists.class, () -> ar.createFile("a/b"));
		ar.createFile("b/a");
		assertThrows(FileExists.class, () -> ar.createFile("b"));
	}

	@Test
	public void testSetArchiveUserProperties() throws Exception {
		CDStarArchive ar = makeArchive();

		ar.setAttribute("fooProp", "foo", "bar");
		assertEquals(Arrays.asList("foo", "bar"), ar.getAttribute("fooProp").values());
		assertEquals(Arrays.asList(), ar.getAttribute("meh").values());
		assertEquals(Sets.newSet("fooProp"), ar.getAttributeNames());

		commit();
		assertEquals(Arrays.asList("foo", "bar"), ar.getAttribute("fooProp").values());
		assertEquals(Arrays.asList(), ar.getAttribute("meh").values());
		assertEquals(Sets.newSet("fooProp"), ar.getAttributeNames());

		ar = loadArchive(ar.getId());
		assertEquals(Arrays.asList("foo", "bar"), ar.getAttribute("fooProp").values());
		assertEquals(Arrays.asList(), ar.getAttribute("meh").values());
		assertEquals(Sets.newSet("fooProp"), ar.getAttributeNames());
	}

	@Test
	public void testReReadArchive() throws Exception {
		final CDStarArchive ar = makeArchive();
		final CDStarArchive ar2 = loadArchive(ar.getId());
		assertEquals(ar, ar2);
		assertEquals(ar.getRev(), ar2.getRev());
	}

	@Test
	public void testCreateReadFile() throws Exception {
		CDStarArchive ar = makeArchive();
		writeString(ar.createFile("test.txt", "text/plain"), "abc");
		commit();

		ar = loadArchive(ar.getId());
		final CDStarFile f = ar.getFile("test.txt");
		assertEquals(3, f.getSize());
		assertEquals("text/plain", f.getMediaType());
		assertEquals("test.txt", f.getName());
		assertEquals("abc", IOUtils.toString(f.getStream(), "UTF-8"));
	}

	@Test
	public void testFileWriteUpdatesMtime() throws Exception {
		CDStarArchive ar = makeArchive();
		writeString(ar.createFile("test.txt", "text/plain"), "abc");
		commit();

		final Date now = ar.getContentModified();
		Utils.sleepInterruptable(1);

		ar = loadArchive(ar.getId());
		writeString(ar.getFile("test.txt"), "abc");
		commit();

		assertTrue(ar.getContentModified().after(now));
		assertTrue(ar.getFile("test.txt").getLastModified().after(now));
	}

	@Test
	public void testNonContentChangesUpdateArchiveLastModifiedOnly() throws Exception {
		CDStarArchive ar = makeArchive();
		ar.setAttribute("dc:title", "foo");
		ar.createFile("test.txt", "text/plain").setAttribute("dc:title", "foo");
		commit();

		final Date lastArchiveModified = ar.getContentModified();
		final Date lastFileModified = ar.getFile("test.txt").getLastModified();

		Utils.sleepInterruptable(1);

		// Change archive attribute
		ar = loadArchive(ar.getId());
		ar.setAttribute("dc:title", "bar");
		ar.getFile("test.txt").setAttribute("dc:title", "bar");
		ar.getFile("test.txt").setMediaType("text/bla");
		commit();

		assertTrue(ar.getContentModified().after(lastArchiveModified));
		assertTrue(ar.getFile("test.txt").getLastModified().equals(lastFileModified));
	}

	@Test
	public void testFileNameConflict() throws Exception {
		final CDStarArchive ar = makeArchive();

		// Disallow files that are also folders of other files
		ar.createFile("name", "text/plain");
		assertThrows(FileExists.class, () -> ar.createFile("name/conflict", "text/plain"));
		ar.createFile("conflict/name", "text/plain");
		assertThrows(FileExists.class, () -> ar.createFile("conflict", "text/plain"));

		// Ensure that the errors above do not actually create any files
		assertEquals(2, ar.getFileCount());

		commit();
	}

	@Test
	public void testDirectPermissions() throws Exception {
		CDStarArchive ar = makeArchive();
		ar.getACL()
			.forSubject(
				new StringSubject.PrincipalSubject(TEST_USER2 + "@" + getDefaultSubject().getPrincipal().getDomain()))
			.permit(ArchivePermissionSet.READ);
		commit();

		auth(TEST_USER2, TEST_USER2);
		ar = loadArchive(ar.getId());
		ar.getFiles();
		ar.getAttributes();
	}

	@Test
	public void testFileRemovalUpdatesRevision() throws Exception {
		CDStarArchive ar = makeArchive();
		ar.createFile("test.txt", "text/plain");
		commit();

		ar = loadArchive(ar.getId());
		ar.getFile("test.txt").remove();
		commit();

		assertEquals(ar.getNextRev(), loadArchive(ar.getId()).getRev());
	}

	public interface FailRunnable {
		void run() throws Throwable;
	}

	public final <T extends Throwable> T assertThrows(Class<T> exceptionClass, FailRunnable runnable) {
		try {
			runnable.run();
		} catch (Throwable throwable) {
			if (throwable instanceof AssertionError && throwable.getCause() != null)
				throwable = throwable.getCause();
			if (!exceptionClass.equals(throwable.getClass()))
				throw new AssertionError("Wrong exception thrown. Expected: <" + exceptionClass + "> but was: <"
					+ throwable.getClass() + ">", throwable);

			@SuppressWarnings("unchecked")
			final T result = (T) throwable;
			return result;
		}
		assert false; // expected exception was not thrown.
		return null; // to keep the compiler happy.
	}

}
