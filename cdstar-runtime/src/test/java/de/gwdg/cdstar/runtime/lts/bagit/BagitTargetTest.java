package de.gwdg.cdstar.runtime.lts.bagit;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.nio.file.Files;
import java.nio.file.Path;

import org.junit.Test;

import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.config.MapConfig;
import de.gwdg.cdstar.runtime.client.CDStarArchive;
import de.gwdg.cdstar.runtime.client.CDStarProfile;
import de.gwdg.cdstar.runtime.lts.AbstractTargetTest;
import de.gwdg.cdstar.runtime.tasks.TaskService;
import de.gwdg.cdstar.runtime.tasks.TaskServiceTest;

public class BagitTargetTest extends AbstractTargetTest<BagitTarget> {

	@Override
	public BagitTarget createLTSTarget() throws Exception {
		final Path path = tmp.newFolder().toPath().toAbsolutePath();
		final MapConfig cfg = new MapConfig()
			.set(BagitTarget.PARAM_NAME, "test-mirror")
			.set(BagitTarget.PARAM_PATH, path.toString())
			.set(BagitTarget.PARAM_CHECK_INTERVAL, "100ms")
			.set(BagitTarget.PARAM_CHECK_RETRY, "100"); // 100ms*100 = 10s
		return new BagitTarget(cfg);
	}

	@Test
	public void testExternalProcessMigrationHandling() throws Exception {
		CDStarArchive ar = makeArchive();
		writeString(ar.createFile("folder/file.txt"), "Hello World!");

		// Trigger archiving
		ar.getMirrorState().setProfile(ar.getVault().getProfileByName("test-profile"));
		commit();

		TaskServiceTest.waitForBackgroundTasks(getRuntime().lookupRequired(TaskService.class));

		// Assume archive is packed as bagit and marked as migrated
		ar = loadArchive(ar.getId());
		assertTrue(Files.exists(getPathFor(ar, BagitTarget.BAGIT_SUFFIX)));
		assertTrue(ar.getMirrorState().isMirrored());
		assertFalse(ar.getMirrorState().isAvailable());
		assertFalse(ar.getFile("folder/file.txt").isAvailable());
		rollback();

		// Simulate background process moving the bagit to LTS
		Files.move(getPathFor(ar, BagitTarget.BAGIT_SUFFIX),
			getPathFor(ar, ".archived"));

		// Trigger recovery
		ar = loadArchive(ar.getId());
		ar.getMirrorState().setProfile(ar.getVault().getProfileByName(CDStarProfile.DEFAULT_NAME));
		commit();

		// Wait for 'wants' file to appear
		for (int i = 0; !Files.exists(getPathFor(ar, BagitTarget.WANTS_SUFFIX))
			&& Utils.sleepInterruptable(100)
			&& i < 100; i++)
			;

		// Migration should still be pending
		assertTrue(Files.exists(getPathFor(ar, BagitTarget.WANTS_SUFFIX)));
		ar = loadArchive(ar.getId());
		assertTrue(ar.getMirrorState().isMigrationPending());
		assertTrue(ar.getMirrorState().isMirrored());
		assertFalse(ar.getMirrorState().isAvailable());
		rollback();

		// Simulate background process moving the bagit back from LTS
		Files.move(getPathFor(ar, ".archived"), getPathFor(ar, BagitTarget.BAGIT_SUFFIX));
		assertTrue(Files.exists(getPathFor(ar, BagitTarget.BAGIT_SUFFIX)));

		// Assume that the migration completes in time
		TaskServiceTest.waitForBackgroundTasks(getRuntime().lookupRequired(TaskService.class));
		assertTrue(Files.exists(getPathFor(ar, BagitTarget.BAGIT_SUFFIX)));
		assertFalse(Files.exists(getPathFor(ar, BagitTarget.WANTS_SUFFIX)));

		// Bagit should be marked as deleted
		assertTrue(Files.exists(getPathFor(ar, BagitTarget.DELETE_SUFFIX)));

		// Archive should now be fully recovered
		ar = loadArchive(ar.getId());
		assertFalse(ar.getMirrorState().isMigrationPending());
		assertFalse(ar.getMirrorState().isMirrored());
		assertTrue(ar.getMirrorState().isAvailable());
		rollback();
	}

	@Test
	public void testArchiveRemoval() throws Exception {
		CDStarArchive ar = makeArchive();
		writeString(ar.createFile("folder/file.txt"), "Hello World!");

		// Trigger archiving
		ar.getMirrorState().setProfile(ar.getVault().getProfileByName("test-profile"));
		commit();

		TaskServiceTest.waitForBackgroundTasks(getRuntime().lookupRequired(TaskService.class));

		// Remove archive
		ar = loadArchive(ar.getId());
		assertTrue(Files.exists(getPathFor(ar, BagitTarget.BAGIT_SUFFIX)));
		ar.remove();
		commit();

		TaskServiceTest.waitForBackgroundTasks(getRuntime().lookupRequired(TaskService.class));

		// LTS Copy should now be marked for removal
		assertTrue(Files.exists(getPathFor(ar, BagitTarget.DELETE_SUFFIX)));
	}


	private Path getPathFor(CDStarArchive ar, String suffix) {
		return getLtsTarget().getPathFor(
			ar.getVault().getName(), ar.getId(), ar.getMirrorState().getMirrorLocation(), suffix);
	}

}