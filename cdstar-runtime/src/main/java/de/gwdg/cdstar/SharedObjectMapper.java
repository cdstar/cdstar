package de.gwdg.cdstar;

import java.util.Arrays;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

public class SharedObjectMapper {
	/**
	 * An {@link ObjectMapper} with with indentation enabled.
	 */
	public static final ObjectMapper json = new ObjectMapper();

	/**
	 * An {@link ObjectMapper} with with indentation disabled.
	 */
	public static final ObjectMapper json_compact = new ObjectMapper();

	static {
		json.enable(SerializationFeature.INDENT_OUTPUT);

		for (final ObjectMapper mapper : Arrays.asList(json, json_compact)) {
			mapper.registerModule(new JavaTimeModule());
			mapper.disable(SerializationFeature.FLUSH_AFTER_WRITE_VALUE);
		}

	}

}
