package de.gwdg.cdstar.runtime.tasks;

/**
 * This exception signals that the task should immediately fail and not be
 * retried.
 */
public class FatalTaskException extends Exception {
	private static final long serialVersionUID = -2659214582191119172L;

	public FatalTaskException(String message) {
		super(message);
	}

	public FatalTaskException(Throwable e) {
		super(e);
	}

	public FatalTaskException(String message, Throwable e) {
		super(message, e);
	}

}
