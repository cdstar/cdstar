package de.gwdg.cdstar.runtime.tasks;

import java.util.Map;
import java.util.Optional;

public interface Task {

	String getId();

	String getName();

	/**
	 * Returns an immutable map of string parameters)
	 */
	Map<String, String> getParameterMap();

	/**
	 * Return a parameter, or null if the parameter is not defined.
	 */
	default String get(String key) {
		return getParameterMap().get(key);
	}

	default Optional<String> getOptional(String key) {
		return Optional.ofNullable(get(key));
	}

	void cancel();

	/**
	 * Return true of the task was canceled.
	 */
	boolean isCanceled();

	/**
	 * Return true if the task processing should be stopped as soon as possible,
	 * e.g. because the runtime is currently shutting down.
	 */

	boolean isInterrupted();

	/**
	 * Return true if this task was completed in any way (canceled, interrupted,
	 * completed, failed)
	 */
	boolean isDone();

}