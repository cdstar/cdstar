package de.gwdg.cdstar.runtime.services.health;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ScheduledExecutorService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HealthService {

	static final Logger log = LoggerFactory.getLogger(HealthMonitorFeature.class);

	Map<String, HealthMonitor> monitors = new ConcurrentHashMap<>();
	ScheduledExecutorService cron;

	synchronized void setCron(ScheduledExecutorService cronService) {
		if (cron != null)
			throw new IllegalStateException("Already started");
		cron = cronService;
		for (final HealthMonitor monitor : monitors.values())
			monitor.setCron(cron);
	}

	public synchronized HealthMonitor register(String name, HealthCheck check) {
		final HealthMonitor m = new HealthMonitor(name, check);
		if (cron != null)
			m.setCron(cron);

		final HealthMonitor old = monitors.put(m.name, m);
		if (old != null)
			old.lazy(true); // Disable auto-checking of old monitor

		log.info("New monitor (unconfigured): {}", m);
		return m;
	}

	public HealthMonitor get(String name) {
		return monitors.get(name);
	}

	public Collection<HealthMonitor> getMonitors() {
		return Collections.unmodifiableCollection(monitors.values());
	}
}