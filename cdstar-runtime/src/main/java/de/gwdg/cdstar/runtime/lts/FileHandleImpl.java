package de.gwdg.cdstar.runtime.lts;

import java.io.IOException;
import java.io.InputStream;
import java.nio.channels.Channels;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.gwdg.cdstar.runtime.client.CDStarAttribute;
import de.gwdg.cdstar.runtime.client.CDStarFile;

class FileHandleImpl implements FileHandle {

	private final CDStarFile file;

	public FileHandleImpl(CDStarFile wrapped) {
		file = wrapped;
	}

	InputStream getStream() throws IOException {
		return Channels.newInputStream(file.getReadChannel());
	}

	public CDStarFile unwrap() {
		return file;
	}

	@Override
	public String getName() {
		return file.getName();
	}

	@Override
	public String getId() {
		return file.getID();
	}

	@Override
	public long getSize() {
		return file.getSize();
	}

	@Override
	public String getType() {
		return file.getMediaType();
	}

	@Override
	public Map<String, String> getDigests() {
		return file.getDigests();
	}

	@Override
	public boolean isLocal() {
		return file.isAvailable();
	}

	@Override
	public Map<String, List<String>> getMetaAttributes() {
		final Map<String, List<String>> copy = new HashMap<>();
		for (final CDStarAttribute attr : file.getAttributes()) {
			if (!attr.isEmpty())
				copy.put(attr.getName(), attr.values());
		}
		return copy;
	}
}