package de.gwdg.cdstar.runtime.exc;

/**
 * Indicates a (hopefully temporary) failure of the backend implementation.
 *
 */
public class BackendException extends RuntimeException {
	private static final long serialVersionUID = -4333780907638529521L;

	public BackendException(String string) {
		super(string);
	}

	public BackendException(String string, Exception e) {
		super(string, e);
	}
}
