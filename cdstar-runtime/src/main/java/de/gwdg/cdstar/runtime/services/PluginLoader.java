package de.gwdg.cdstar.runtime.services;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.runtime.Config;
import de.gwdg.cdstar.runtime.ConfigException;
import de.gwdg.cdstar.runtime.Plugin;
import io.github.classgraph.ClassGraph;
import io.github.classgraph.ClassInfo;
import io.github.classgraph.ScanResult;

/**
 * A class loader that scans the classpath for {@link Plugin} annotated classes
 * if a requested class could not be loaded by name directly.
 */
public class PluginLoader {
	static final Logger log = LoggerFactory.getLogger(PluginLoader.class);

	private final PluginClassLoader pluginClassLoader;
	private boolean classpathScanned;
	private final Set<Class<?>> pluginClasses = new HashSet<>();
	private final Map<String, Class<?>> pluginNames = new HashMap<>();

	public PluginLoader(ClassLoader baseClassLoader) {
		pluginClassLoader = new PluginClassLoader(baseClassLoader);
	}

	public ClassLoader getPluginClassLoader() {
		return pluginClassLoader;
	}

	/**
	 * Load and instantiate a plugin class by name.
	 */
	@SuppressWarnings("unchecked")
	public <T> T initPlugin(String pluginName, String className, Config localConfig, Class<T> limitClass)
		throws ConfigException {
		final Class<?> genericClass;
		final Class<T> klass;

		try {
			genericClass = loadClass(className);
		} catch (final ClassNotFoundException e) {
			throw new ConfigException("Plugin [" + pluginName + "] failed to initialize: Class not found", e);
		}

		if (limitClass.isAssignableFrom(genericClass))
			klass = (Class<T>) genericClass;
		else
			throw new ConfigException("Plugin [" + pluginName + "] failed to initialize: "
				+ genericClass + " does not implement " + limitClass);

		try {
			try {
				return klass.getConstructor(Config.class).newInstance(localConfig);
			} catch (final NoSuchMethodException e) {
				return klass.getConstructor().newInstance();
			}
		} catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException
			| IllegalArgumentException e) {
			throw new ConfigException("Plugin [" + pluginName + "] failed to initialize", e);
		} catch (final InvocationTargetException e) {
			throw new ConfigException("Plugin [" + pluginName + "] failed to initialize", e.getCause());
		}
	}

	/**
	 * Load a class by full or short name. If the class cannot be loaded
	 * directly, a three-stage discovery is started, then the entire classpath
	 * is scanned for {@link Plugin} annotated classes to find a match.
	 *
	 * If all this fails, then a ClassNotFoundException exception is thrown.
	 */
	public synchronized Class<?> loadClass(String name) throws ClassNotFoundException {
		Class<?> cls = pluginNames.get(name);
		if (cls != null)
			return cls;

		if (name.contains(".")) {
			cls = pluginClassLoader.loadClass(name);
			registerPlugin(cls);
			return cls;
		}

		if (classpathScanned)
			throw new ClassNotFoundException(name);

		log.debug("Plugin '{}' not found. Scanning classpath...", name);
		scanClasspath();
		classpathScanned = true;
		return loadClass(name); // retry
	}

	/**
	 * Scan the plugin classpath for {@link Plugin} annotated classes.
	 */
	private synchronized void scanClasspath() {
		final ScanResult scanResult = new ClassGraph()
			.enableClassInfo()
			.enableAnnotationInfo()
			.addClassLoader(pluginClassLoader)
			.scan();

		for (final File cp : scanResult.getClasspathFiles()) {
			log.debug("Plugin classpath: {}", cp);
		}

		for (final ClassInfo cls : scanResult.getClassesWithAnnotation(Plugin.class.getName())) {
			try {
				registerPlugin(pluginClassLoader.loadClass(cls.getName()));
			} catch (final ClassNotFoundException e) {
				throw Utils.wtf(e);
			}
		}
	}

	/**
	 * Register a plugin class. Once registered, plugins can be referenced by
	 * their simple and full class name, as well as any aliases defined in a
	 * {@link Plugin} annotation.
	 */
	private synchronized void registerPlugin(Class<?> pluginClass) {
		final Plugin[] annos = pluginClass.getDeclaredAnnotationsByType(Plugin.class);

		if (!pluginClasses.add(pluginClass))
			return; // Already known

		log.debug("Found plugin class : {}", pluginClass.getName());
		if (!Utils.nullOrEmpty(pluginClass.getCanonicalName()))
			registerNamedPlugin(pluginClass.getCanonicalName(), pluginClass);
		if (!Utils.nullOrEmpty(pluginClass.getSimpleName()))
			registerNamedPlugin(pluginClass.getSimpleName(), pluginClass);

		for (final Plugin p : annos) {
			for (final String name : p.name()) {
				if (Utils.notNullOrEmpty(name))
					registerNamedPlugin(name, pluginClass);
			}
		}

		return;
	}

	private synchronized void registerNamedPlugin(String name, Class<?> pluginClass) {
		final Class<?> conflict = pluginNames.putIfAbsent(name, pluginClass);
		if (conflict != null && conflict != pluginClass) {
			log.warn("Found two plugins with same name '{}': {} and {} (ignored)",
				name, pluginNames.get(name).getName(), pluginClass.getName());
		}
	}

	class PluginClassLoader extends URLClassLoader {

		List<Path> paths = new ArrayList<>();

		PluginClassLoader(ClassLoader parent) {
			super(new URL[] {}, parent);
		}

		void addPath(Path lib) throws IOException {
			if (Files.isDirectory(lib)) {
				log.debug("Scanning directory: {}", lib);
				for (final Path file : Utils.iter(Files.walk(lib))) {
					if (!Files.isRegularFile(file) || !file.getFileName().toString().endsWith(".jar"))
						continue;
					addPath(file);
				}
			} else if (Files.isRegularFile(lib) && lib.getFileName().toString().endsWith(".jar")) {
				log.debug("Found jar file: {}", lib);
				addURL(lib.toUri().toURL());
			} else {
				log.warn("Skipped library path (not found): {}", lib);
			}
			paths.add(lib);
		}

		List<Path> getPaths() {
			return Collections.unmodifiableList(paths);
		}
	}

	public void addPath(Path libPath) throws IOException {
		pluginClassLoader.addPath(libPath);
	}

}
