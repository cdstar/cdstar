package de.gwdg.cdstar.runtime.filter;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;

import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.runtime.ConfigException;
import de.gwdg.cdstar.runtime.VaultConfig;
import de.gwdg.cdstar.runtime.client.CDStarVault;
import de.gwdg.cdstar.runtime.listener.SessionListener;
import de.gwdg.cdstar.runtime.listener.VaultConfigListener;

/**
 * Set default permissions for newly created archives.
 *
 * The configuration syntax is:
 *
 * <pre>
 * vault.{vaultName}.acl.default.{subject} = {permission_list}
 * </pre>
 *
 * Example:
 *
 * <pre>
 * vault:
 *   demoVault:
 *     acl.default.$owner = READ, WRITE
 *     acl.default.@admins = ADMIN
 * </pre>
 *
 */

public class DefaultPermissionsPlugin implements SessionListener, VaultConfigListener {

	static final Logger log = Utils.getLogger();
	Map<String, DefaultPermissionsVaultListener> filters = new ConcurrentHashMap<>();

	@Override
	public synchronized void vaultConfigChanged(VaultConfig vaultConfig) {
		try {
			if (DefaultPermissionsVaultListener.isEnabledFor(vaultConfig))
				filters.put(vaultConfig.getName(), new DefaultPermissionsVaultListener(vaultConfig));
			else
				filters.remove(vaultConfig.getName());
		} catch (final ConfigException e) {
			filters.remove(vaultConfig.getName());
			throw Utils.toUnchecked(e);
		}
	}

	@Override
	public void onVaultOpenend(CDStarVault vault) {
		final DefaultPermissionsVaultListener filter = filters.get(vault.getName());
		if (filter != null)
			vault.addListener(filter);
	}

}
