package de.gwdg.cdstar.runtime.tasks;

class MissingTaskRunnerException extends FatalTaskException {
	private static final long serialVersionUID = -2659214582191119172L;

	public MissingTaskRunnerException(String name) {
		super("Task runner not found: " + name);
	}

}
