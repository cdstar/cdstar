package de.gwdg.cdstar.runtime.tasks;

import java.util.concurrent.RejectedExecutionException;

import de.gwdg.cdstar.ta.UserTransaction;

/**
 * A prepared task is fully configured, but not yet submitted to a task runner.
 */
public interface PreparedTask extends Task {

	/**
	 * Bind this tasks to a transaction. The task is automatically submitted if
	 * the transaction is committed, and canceled if the transaction is rolled
	 * back.
	 *
	 * @return the not yet submitted task.
	 *
	 * @throws IllegalStateException
	 *             if the task was already submitted, canceled or bound to a
	 *             transaction.
	 */
	public Task bind(UserTransaction ta);

	/**
	 * Submit a prepared task. The task is first persisted in a way that
	 * survives a service restart, and then submitted to a task runner.
	 *
	 * @return the submitted task.
	 *
	 * @throws IllegalStateException
	 *             if the task was already submitted, canceled or bound to a
	 *             transaction.
	 * @throws RejectedExecutionException
	 *             if the task failed to persist, in which case its execution
	 *             cannot be guaranteed.
	 */
	public Task submit();

}
