package de.gwdg.cdstar.runtime.services;

import java.lang.management.ClassLoadingMXBean;
import java.lang.management.ManagementFactory;
import java.lang.management.MemoryMXBean;
import java.lang.management.MemoryPoolMXBean;
import java.lang.management.OperatingSystemMXBean;
import java.lang.management.RuntimeMXBean;
import java.lang.management.ThreadMXBean;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.management.MBeanServer;
import javax.management.MBeanServerConnection;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;

import com.codahale.metrics.Gauge;
import com.codahale.metrics.MetricRegistry;
import com.sun.management.UnixOperatingSystemMXBean;

import de.gwdg.cdstar.runtime.RuntimeContext;
import de.gwdg.cdstar.runtime.listener.RuntimeListener;
import de.gwdg.cdstar.runtime.services.health.HealthService;
import de.gwdg.cdstar.runtime.services.health.HealthStatus;

public class DefaultMetricsAndHealthChecks implements RuntimeListener {

	private RuntimeContext runtime;
	private HealthService health;
	private MetricRegistry metrics;

	@Override
	public void onInit(RuntimeContext ctx) throws Exception {
		runtime = ctx;
	}

	@Override
	public void onStartup(RuntimeContext ctx) throws Exception {
		health = runtime.lookupRequired(HealthService.class);
		metrics = runtime.lookupRequired(MetricRegistry.class);
		registerOs();
		registerVm();
		registerMem();
		registerPool();
		registerThread();
		registerClassLoader();
	}

	private void registerOs() {
		final OperatingSystemMXBean os = ManagementFactory.getOperatingSystemMXBean();
		metrics.gauge("jvm.os.load", () -> () -> os.getSystemLoadAverage());
		metrics.gauge("jvm.os.cpu", () -> () -> os.getAvailableProcessors());

		if (os instanceof UnixOperatingSystemMXBean) {
			final UnixOperatingSystemMXBean unix = (UnixOperatingSystemMXBean) os;
			metrics.gauge("jvm.os.files.max", () -> () -> unix.getMaxFileDescriptorCount());
			metrics.gauge("jvm.os.files.open", () -> () -> unix.getOpenFileDescriptorCount());
			metrics.gauge("jvm.os.swap.max", () -> () -> unix.getTotalSwapSpaceSize());
			metrics.gauge("jvm.os.swap.free", () -> () -> unix.getFreeSwapSpaceSize());
		}
	}

	private void registerVm() {
		final RuntimeMXBean vm = ManagementFactory.getRuntimeMXBean();
		metrics.gauge("jvm.uptime", () -> () -> vm.getUptime());
	}

	private void registerMem() {
		final MemoryMXBean mem = ManagementFactory.getMemoryMXBean();
		final List<MemoryPoolMXBean> pools = ManagementFactory.getMemoryPoolMXBeans();
		metrics.gauge("jvm.heap.max", () -> () -> mem.getHeapMemoryUsage().getMax());
		metrics.gauge("jvm.heap.used", () -> () -> mem.getHeapMemoryUsage().getUsed());
		metrics.gauge("jvm.heap.committed", () -> () -> mem.getHeapMemoryUsage().getCommitted());
		metrics.gauge("jvm.offheap.max", () -> () -> mem.getNonHeapMemoryUsage().getMax());
		metrics.gauge("jvm.offheap.used", () -> () -> mem.getNonHeapMemoryUsage().getUsed());
		metrics.gauge("jvm.offheap.committed", () -> () -> mem.getNonHeapMemoryUsage().getCommitted());
		metrics.gauge("jvm.fincount", () -> () -> mem.getObjectPendingFinalizationCount());

		for (final MemoryPoolMXBean pool : pools) {

			final String saveName = pool.getName()
					.toLowerCase()
					.replaceAll("[^a-z0-9]+", " ")
					.trim()
					.replace(" ", "-")
					.replace("-space", "");

			final String prefix = "jvm.pool." + saveName;
			metrics.gauge(prefix + ".max", () -> () -> pool.getUsage().getMax());
			metrics.gauge(prefix + ".used", () -> () -> pool.getUsage().getUsed());
			metrics.gauge(prefix + ".committed", () -> () -> pool.getUsage().getCommitted());
			if (pool.getCollectionUsage() != null) {
				metrics.gauge(prefix + ".used-after-gc", () -> () -> pool.getCollectionUsage().getUsed());
			}
		}
	}

	private void registerPool() throws MalformedObjectNameException {
		final MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
		for (final String pool : Arrays.asList("direct", "mapped")) {
			final ObjectName on = new ObjectName("java.nio:type=BufferPool,name=" + pool);
			metrics.gauge("jvm.buffers." + pool + ".count", () -> new JmxAttributeGauge(mbs, on, "Count"));
			metrics.gauge("jvm.buffers." + pool + ".used", () -> new JmxAttributeGauge(mbs, on, "MemoryUsed"));
			metrics.gauge("jvm.buffers." + pool + ".capacity", () -> new JmxAttributeGauge(mbs, on, "TotalCapacity"));
		}
	}

	private void registerThread() {
		final ThreadMXBean threads = ManagementFactory.getThreadMXBean();
		metrics.gauge("jvm.threads.current", () -> () -> threads.getThreadCount());
		metrics.gauge("jvm.threads.deamon", () -> () -> threads.getDaemonThreadCount());
		metrics.gauge("jvm.threads.peak", () -> () -> threads.getPeakThreadCount());
		metrics.gauge("jvm.threads.total", () -> () -> threads.getTotalStartedThreadCount());

		health.register("jvm.threads.deadlock", () -> {
			final long[] deadlocks = threads.findDeadlockedThreads();
			if (deadlocks == null || deadlocks.length == 0)
				return HealthStatus.ok();
			return HealthStatus.error("Deadlocks detected: " + deadlocks.length);
		}).interval(60, TimeUnit.SECONDS);

		health.register("jvm.threads.count", () -> {
			final int threadcount = threads.getThreadCount();
			if (threadcount < 128)
				return HealthStatus.ok();
			if (threadcount < 256)
				return HealthStatus.warn("Large number of threads: " + threadcount);
			return HealthStatus.error("Large number of threads: " + threadcount);
		}).interval(60, TimeUnit.SECONDS);
	}

	private void registerClassLoader() {
		final ClassLoadingMXBean cl = ManagementFactory.getClassLoadingMXBean();
		metrics.gauge("jvm.classloader.loaded", () -> () -> cl.getLoadedClassCount());
	}

	private class JmxAttributeGauge implements Gauge<Object> {
		private final MBeanServerConnection mBeanServerConn;
		private final ObjectName objectName;
		private final String attributeName;

		public JmxAttributeGauge(MBeanServerConnection mBeanServerConn, ObjectName objectName, String attributeName) {
			this.mBeanServerConn = mBeanServerConn;
			this.attributeName = attributeName;
			this.objectName = objectName;
		}

		@Override
		public Object getValue() {
			try {
				return mBeanServerConn.getAttribute(objectName, attributeName);
			} catch (final Exception e) {
				return null;
			}
		}
	}

}
