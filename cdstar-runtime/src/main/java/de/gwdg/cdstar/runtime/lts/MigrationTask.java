package de.gwdg.cdstar.runtime.lts;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.CancellationException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import java.util.concurrent.ExecutorService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.runtime.client.CDStarArchive;
import de.gwdg.cdstar.runtime.client.CDStarClient;
import de.gwdg.cdstar.runtime.client.CDStarFile;
import de.gwdg.cdstar.runtime.client.CDStarSession;
import de.gwdg.cdstar.runtime.client.CDStarSnapshot;
import de.gwdg.cdstar.runtime.client.LTSMigrationSupport;
import de.gwdg.cdstar.runtime.client.exc.ArchiveNotFound;
import de.gwdg.cdstar.runtime.client.exc.VaultNotFound;
import de.gwdg.cdstar.runtime.client.utils.ArchiveOrSnapshot;
import de.gwdg.cdstar.runtime.lts.LTSTarget.ExportContext;
import de.gwdg.cdstar.runtime.lts.LTSTarget.ImportContext;
import de.gwdg.cdstar.runtime.lts.StateMachine.Step;
import de.gwdg.cdstar.ta.exc.TARollbackException;

/**
 * A completely asynchronous implementation of a single archive migration
 * attempt. One-time use only.
 */
class MigrationTask {
	private static final Logger log = LoggerFactory.getLogger(MigrationTask.class);

	// Task definition
	private final CDStarClient client;
	private final LTSMigrationService service;
	private final String vaultName;
	private final String archiveId;
	private final String snapshotName;
	private final String taskId;
	private final String logKey;

	// Runtime state
	private ExecutorService pool;
	private StateMachine<State> reactor;
	private final Set<CompletableFuture<?>> cancelMe = new HashSet<>();

	// Internal state of the state machine
	static class State {
		CDStarSession session;
		ArchiveOrSnapshot pkg;
		LTSLocation location;
		LTSConfig targetConfig;
		ArchiveHandleImpl handle;
		ImportContext importContext;
		LTSTarget importTarget;
		ExportContext exportContext;
		LTSTarget exportTarget;
		Iterator<FileHandleImpl> fileIterator;
		LTSLocation newLocation;
	}

	MigrationTask(LTSMigrationService migrationService, CDStarClient systemClient, String vault,
			String archive, String snapshot, String taskKey) {
		vaultName = vault;
		archiveId = archive;
		snapshotName = snapshot;
		taskId = taskKey;
		client = systemClient;
		service = migrationService;
		logKey = Integer.toHexString(System.identityHashCode(this));
	}

	public synchronized boolean cancel() {
		if (reactor == null)
			throw new IllegalStateException("Cannot cancel, not started");
		return reactor.cancel();
	}

	public synchronized boolean isCancelled() {
		return reactor != null && reactor.getFuture().isCancelled();
	}

	public CompletableFuture<Void> start() {
		if (reactor != null)
			throw new IllegalStateException("Already started");

		this.pool = service.getPool();
		final State state = new State();
		reactor = new StateMachine<>(this::startNewSession, state, pool);
		reactor.start();

		// Log task completion
		reactor.getFuture().whenComplete((v, err) -> {
			if (err != null) {
				while (err instanceof CompletionException && err.getCause() != null)
					err = err.getCause();
				if (err instanceof CancellationException)
					log.debug("[{}] Migration canceled", logKey, err);
				else
					log.warn("[{}] Migration failed", logKey, err);
			} else {
				log.debug("[{}] Migration completed", logKey);
			}
		});

		final CompletableFuture<Void> taskFuture = reactor.getFuture().thenApply(v -> (Void) null);

		taskFuture.whenComplete((v, err) -> {
			// Forward external cancellation to the reactor. Also ensure that
			// cleanup() is always called, even in error situations.
			reactor.cancel();
			cleanup(state).join();
		});

		return taskFuture;
	}

	private CompletableFuture<Step<State>> startNewSession(State state) {

		// TODO: Delay migration from HOT to COLD, so search engines can pick up
		// the changes before the archive goes away. It might be a good idea to
		// delay ALL migrations for a short while to de-bounce repeated profile
		// changes. This should happen before starting the transaction, to
		// reduce the risk of failing commits at the end.

		CDStarArchive archive;
		try {
			state.session = client.begin();
			archive = state.session.getVault(vaultName).loadArchive(archiveId);
		} catch (ArchiveNotFound | VaultNotFound e) {
			throw new MigrationFailedException(e);
		}

		if (Utils.notNullOrEmpty(snapshotName)) {
			CDStarSnapshot snapshot = archive.getSnapshot(
					snapshotName)
					.orElseThrow(() -> new IllegalStateException("Archive has no snapshot with that name"));
			state.pkg = new ArchiveOrSnapshot(snapshot);
		} else {
			state.pkg = new ArchiveOrSnapshot(archive);
		}

		state.handle = new ArchiveHandleImpl(state.pkg);

		if (!Utils.equalNotNull(taskId, LTSMigrationSupport.getMigrationTaskId(state.pkg.getMirrorState()))) {
			throw new CancellationException("Pending task ID does not match current task. Migration skipped.");
		}

		state.location = LTSLocation.fromMirrorState(state.pkg.getMirrorState()).orElse(null);
		state.targetConfig = LTSConfig.fromProfile(state.pkg.getProfile()).orElse(null);

		if (log.isDebugEnabled()) {
			log.debug("[{}] Migration started: vault={} archive={} revision={}", logKey,
					state.pkg.getVault().getName(), state.pkg.getId(), state.pkg.getRev());
			if (state.location != null)
				log.debug("[{}] Current mirror: {}", logKey, state.location);
			if (state.targetConfig != null)
				log.debug("[{}] Target mirror: {}", logKey, state.targetConfig);
		}

		if (state.location == null && !state.pkg.isAvailable())
			throw new IllegalStateException("Cold archive with no mirror: Cannot recover");

		if (state.targetConfig == null && state.location == null)
			throw new CancellationException("Nothing to do.");

		if (isSameMirror(state) && state.pkg.isAvailable() != state.targetConfig.isCold())
			throw new CancellationException("Nothing to do.");

		return CompletableFuture.completedFuture(this::createImportContext);
	}

	private CompletableFuture<Step<State>> createImportContext(State state) {
		checkCanceled();

		if (state.location == null)
			return CompletableFuture.completedFuture(this::createExportContext);

		log.debug("[{}] Creating import context for: {}", logKey, state.location.getMirrorName());

		state.importTarget = getAdapter(state.location.getMirrorName());
		return state.importTarget
				.startImport(state.handle, state.location)
				.thenApply(ic -> {
					state.importContext = ic;
					return this::createExportContext;
				});
	}

	private CompletableFuture<Step<State>> createExportContext(State state) {
		checkCanceled();

		if (state.targetConfig == null || isSameMirror(state))
			return CompletableFuture.completedFuture(this::collectFiles);

		log.debug("[{}] Creating export context for: {}", logKey, state.targetConfig.getName());

		state.exportTarget = getAdapter(state.targetConfig.getName());
		return state.exportTarget
				.startExport(state.handle)
				.thenApply(ec -> {
					state.exportContext = ec;
					return this::collectFiles;
				});
	}

	/**
	 * Returns a single {@link CompletableFuture} that represents the migration of
	 * all files, but only migrates one file at a time and stops at the first error.
	 */
	@SuppressWarnings("unchecked")
	private CompletableFuture<Step<State>> collectFiles(State state) {
		checkCanceled();

		if (state.importContext == null && state.exportContext == null)
			throw Utils.wtf();

		state.fileIterator = (Iterator<FileHandleImpl>) (Iterator<?>) state.handle.listFiles().iterator();

		if (!state.fileIterator.hasNext())
			// No files to migrate
			return CompletableFuture.completedFuture(this::completeMigration);

		log.debug("[{}] Migrating {} files", logKey, state.pkg.getFileCount());
		return CompletableFuture.completedFuture(this::migrateNextFile);
	}

	private CompletableFuture<Step<State>> migrateNextFile(State state) {
		if (!state.fileIterator.hasNext())
			return CompletableFuture.completedFuture(this::completeMigration);

		final FileHandleImpl file = state.fileIterator.next();
		final CDStarFile unwrapped = file.unwrap();

		// TODO: Add cancellation-checks
		// TODO: Optimize same-target migrations

		CompletableFuture<Void> importFuture;
		if (file.isLocal()) {
			importFuture = CompletableFuture.completedFuture((Void) null);
		} else {
			log.debug("[{}] Importing file: {} ({} bytes)", logKey, file.getName(), file.getSize());
			if (state.importContext == null)
				throw new IllegalStateException("Import file not possible: No mirror");

			importFuture = add(state.importContext.importFile(file))
					.thenApplyAsync(is -> {
						try {
							LTSMigrationSupport.unfreezeFile(unwrapped, is);
							return null;
						} catch (final IOException e) {
							throw new MigrationFailedException(e);
						} finally {
							Utils.closeQuietly(is);
						}
					}, pool);
		}

		final CompletableFuture<Void> exportFuture;

		if (state.exportContext == null) {
			exportFuture = importFuture;
		} else {
			exportFuture = importFuture
					.thenComposeAsync(fi -> {
						checkCanceled();

						log.debug("[{}] Exporting file: {} ({} bytes)", logKey, file.getName(), file.getSize());
						try {
							final InputStream stream = unwrapped.getStream();
							return add(state.exportContext.exportFile(file, stream))
									.whenComplete((r, e) -> {
										Utils.closeQuietly(stream);
									})
									.thenApply(size -> (Void) null);
						} catch (final IOException e) {
							throw new CompletionException(e);
						}
					}, pool);
		}

		return exportFuture.thenApply(v -> {
			return this::migrateNextFile;
		});
	}

	/**
	 * Register a {@link CompletableFuture} to be canceled if the entire task is
	 * canceled. Return the same future.
	 */
	private <T> CompletableFuture<T> add(CompletableFuture<T> other) {
		synchronized (cancelMe) {
			cancelMe.add(other);
		}

		other.whenComplete((what, ever) -> {
			synchronized (cancelMe) {
				cancelMe.remove(other);
			}
		});

		return other;
	}

	private CompletableFuture<Step<State>> completeMigration(State state) {
		if (state.exportContext == null)
			return CompletableFuture.completedFuture(this::commit);

		log.debug("[{}] Finalizing export", logKey);
		return state.exportContext.complete().thenApply(newLocation -> {
			log.debug("[{}] Export finalized. New location: {}", logKey, newLocation);
			state.newLocation = newLocation;
			return this::commit;
		});
	}

	private CompletableFuture<Step<State>> commit(State state) {
		LTSMigrationSupport.clearMigrationTask(state.pkg.getMirrorState());

		if (state.targetConfig == null) {
			LTSMigrationSupport.clearMirror(state.pkg.getMirrorState());
		} else if (state.newLocation != null) {
			LTSMigrationSupport.clearMirror(state.pkg.getMirrorState());
			LTSMigrationSupport.setMirror(state.pkg.getMirrorState(), state.newLocation.getMirrorName(),
					state.newLocation.getLocation());
		} else {
			// Same mirror (e.g. hot->cold or cold->hot)
		}

		// Freese files for cold profiles (after mirror is set)
		if (state.targetConfig != null && state.targetConfig.isCold()) {
			state.pkg.getFiles().stream()
					.filter(f -> f.getSize() > 0)
					.forEach(LTSMigrationSupport::freezeFile);
		}

		if (isCancelled()) {
			state.session.rollback();
			log.debug("[{}] Task canceled. Archive rolled back", logKey);
			return CompletableFuture.completedFuture(this::cleanup);
		}

		try {
			state.session.commit();
		} catch (final TARollbackException e) {
			throw new MigrationFailedException(e);
		}

		if (state.importContext == null || isSameMirror(state))
			return CompletableFuture.completedFuture(this::cleanup);

		// Do this only after a successful commit
		log.debug("[{}] Marking old mirror location for removal: {}", logKey, state.location);
		return state.importTarget
				.markObsolete(state.handle, state.location)
				.thenApply(v -> this::cleanup);
	}

	/**
	 * Close all contexts and the transaction. In a successful migration, this is
	 * called as the last step (and before the full task completes). On errors or
	 * external cancellation, this may be called even while a step is still running
	 * and will forcefully close the resources and hopefully cause errors in the
	 * running step, stopping it faster.
	 */
	private CompletableFuture<Step<State>> cleanup(State state) {
		Utils.closeQuietly(state.importContext);
		Utils.closeQuietly(state.exportContext);
		Utils.closeQuietly(state.session);
		return CompletableFuture.completedFuture(null);
	}

	/**
	 * Throw a {@link CancellationException} if the task itself was canceled.
	 */
	private void checkCanceled() {
		if (isCancelled())
			throw new CancellationException("Parent future was canceled");
	}

	private LTSTarget getAdapter(String name) {
		return service.getAdapter(name)
				.orElseThrow(() -> new MigrationFailedException("No suitable adapter for mirror: " + name));
	}

	/**
	 * Return true if current and target mirror are the same.
	 */
	private static boolean isSameMirror(State state) {
		return state.location != null && state.targetConfig != null
				&& state.location.getMirrorName().equals(state.targetConfig.getName());
	}

}
