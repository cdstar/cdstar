package de.gwdg.cdstar.runtime.filter;

import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.event.ChangeEvent;
import de.gwdg.cdstar.runtime.client.CDStarArchive;
import de.gwdg.cdstar.runtime.client.CDStarSession;
import de.gwdg.cdstar.runtime.client.CDStarVault;
import de.gwdg.cdstar.runtime.listener.SessionListener;

/**
 * Abstract filter that is triggered after a successful commit and notified
 * about all archives changed (created, updated or deleted) by this commit.
 * Archives that were created and removed within the same transaction are
 * skipped.
 */
public abstract class AbstractEventFilter implements SessionListener {

	@Override
	final public void onCommit(CDStarSession session) {
		for (final CDStarVault vault : session.listOpenedVaults()) {
			for (final CDStarArchive archive : vault.listOpenedArchives()) {
				archiveCommitted(archive);
			}
		}
	}

	public void archiveCommitted(CDStarArchive archive) {
		if (archive.isModified()) {
			// Removing a new archive should not trigger any work.
			if (archive.isRemoved() && archive.getRev().equals(archive.getNextRev()))
				return;
			try {
				triggerEvent(new ChangeEvent(archive));
			} catch (final Exception e) {
				var msg = Utils.format("Failed to dispatch event: {0}/{1}",
						archive.getVault().getName(), archive.getId());
				throw new RuntimeException(msg, e);
			}
		}
	}

	public abstract void triggerEvent(ChangeEvent event) throws Exception;

}
