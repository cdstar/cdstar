package de.gwdg.cdstar.runtime.services;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import org.apache.commons.io.FileExistsException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codahale.metrics.MetricRegistry;

import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.config.ConfigLoader;
import de.gwdg.cdstar.config.MapConfig;
import de.gwdg.cdstar.pool.PoolError;
import de.gwdg.cdstar.pool.StoragePool;
import de.gwdg.cdstar.pool.nio.NioPool;
import de.gwdg.cdstar.runtime.Config;
import de.gwdg.cdstar.runtime.ConfigException;
import de.gwdg.cdstar.runtime.RuntimeContext;
import de.gwdg.cdstar.runtime.VaultConfig;
import de.gwdg.cdstar.runtime.listener.RuntimeListener;
import de.gwdg.cdstar.runtime.listener.VaultConfigListener;

/**
 * Registry to discover existing vaults on disk or create new vaults with
 * persistent configuration.
 */

/*
 * TODO: Unload pools with no active sessions after a certain time.
 *
 * TODO: Reconfigure vaults and pools if the Config file changes on-disk.
 */
public class VaultRegistry implements RuntimeListener {
	static final Pattern vaultNamePattern = Pattern.compile("[a-zA-Z][a-zA-Z-_0-9]{0,64}");

	private static final Logger log = LoggerFactory.getLogger(VaultRegistry.class);

	private RuntimeContext runtime;

	private final Path dataPath;
	private ClassLoader poolClassLoader;
	private final Map<String, VaultConfigImpl> vaultConfigs = new ConcurrentHashMap<>();
	private final Map<String, StoragePool> pools = new ConcurrentHashMap<>();

	public VaultRegistry(Path dataPath, ClassLoader poolClassLoader) {
		this.dataPath = dataPath;
		this.poolClassLoader = poolClassLoader;
	}

	@Override
	public void onInit(RuntimeContext ctx) throws Exception {
		runtime = ctx;
	}

	@Override
	public void onStartup(RuntimeContext ctx) throws Exception {

		// Find and load existing vaults
		discover();

		// Bootstrap missing vaults from config
		for (final Entry<String, Config> row : ctx.getConfig().getTable("vault").entrySet()) {
			final String vaultName = row.getKey();
			final Config vaultConfig = row.getValue();
			if (!getVault(vaultName).isPresent()) {
				createVault(vaultName, vaultConfig);
			} else if (vaultConfig.hasKey("create") && vaultConfig.keySet().size() > 1) {
				log.debug("Vault configuration ignored as vault already exists on disk: {} {}", vaultName,
						vaultConfig.toMap());
			}
		}
	}

	/**
	 * Scan for and try to load existing vaults from disk.
	 */
	public void discover() throws IOException {
		try (Stream<Path> ds = Files.list(dataPath)) {
			for (final Path vaultDir : Utils.iter(ds)) {
				var name = vaultDir.getFileName().toString();
				if (isValidVaultName(name))
					loadVault(name);
			}
		}
	}

	@Override
	public synchronized void onShutdown(RuntimeContext ctx) {
		pools.forEach((name, pool) -> {
			Utils.closeQuietly(pool);
		});
		vaultConfigs.clear();
		pools.clear();
	}

	/**
	 * Create a new vault. This can also be called on a non-started runtime.
	 */
	public synchronized VaultConfigImpl createVault(String vaultName, Config properties) throws IOException {
		if (!isValidVaultName(vaultName))
			throw new IllegalArgumentException("Invalid vault name: " + vaultName);
		if (vaultConfigs.containsKey(vaultName) || Files.exists(getConfigPathFor(vaultName)))
			throw new FileExistsException("Vault config exists: " + getConfigPathFor(vaultName));

		var vaultConfig = new VaultConfigImpl(vaultName, properties);
		runtime.lookupAll(VaultConfigListener.class).forEach(v -> v.vaultConfigChanged(vaultConfig));
		persist(vaultConfig);
		vaultConfigs.put(vaultName, vaultConfig);
		return vaultConfig;
	}

	public synchronized VaultConfigImpl loadVault(String vaultName) throws IOException {
		if (vaultConfigs.containsKey(vaultName))
			return vaultConfigs.get(vaultName);

		if (!isValidVaultName(vaultName))
			throw new IllegalArgumentException("Invalid vault name");

		var confFile = getConfigPathFor(vaultName);
		var config = ConfigLoader.fromFile(confFile.toFile());

		var vaultConfig = new VaultConfigImpl(vaultName, config);
		runtime.lookupAll(VaultConfigListener.class).forEach(v -> v.vaultConfigChanged(vaultConfig));
		vaultConfigs.put(vaultName, vaultConfig);
		return vaultConfig;
	}

	synchronized void persist(VaultConfigImpl vault) throws IOException {
		final Path confFile = getConfigPathFor(vault.getName());

		if (!Files.isDirectory(confFile.getParent()))
			Files.createDirectories(confFile.getParent());

		ConfigLoader.saveAs(confFile.toFile(), vault.getConfig());
	}

	private void addMetrics(String vaultName, StoragePool pool) {
		if (pool instanceof NioPool) {
			runtime.lookup(MetricRegistry.class).ifPresent(mr -> {
				final String name = "pool." + vaultName;
				final NioPool nioPool = (NioPool) pool;
				mr.gauge(name + ".cache.hit", () -> () -> nioPool.getStats().cacheHitCount());
				mr.gauge(name + ".cache.miss", () -> () -> nioPool.getStats().cacheMissCount());
				mr.gauge(name + ".cache.size", () -> () -> nioPool.getStats().cacheSize());
				mr.gauge(name + ".disk.total", () -> () -> nioPool.getStats().diskTotal());
				mr.gauge(name + ".disk.free", () -> () -> nioPool.getStats().diskFree());
				mr.gauge(name + ".sess.count", () -> () -> nioPool.getStats().sessionCount());
			});
		}
	}

	private boolean isValidVaultName(String name) {
		return vaultNamePattern.matcher(name).matches();
	}

	private Path getConfigPathFor(String vaultName) {
		return dataPath.resolve(vaultName).resolve("vault.yml");
	}

	public StoragePool getPoolFor(VaultConfig vault) {
		var pool = pools.get(vault.getName());
		if (pool != null)
			return pool;

		return initPool(vault);
	}

	private synchronized StoragePool initPool(VaultConfig vault) {
		StoragePool pool = pools.get(vault.getName());
		if (pool != null)
			return pool;
		var config = vaultConfigs.get(vault.getName()).getConfig();

		try {
			var scopedConfig = new MapConfig(config.with("pool"));
			scopedConfig.setDefault("class", NioPool.class.getName());
			scopedConfig.set("path", dataPath.resolve(config.get("path", vault.getName())).toAbsolutePath().toString());

			var poolClassName = scopedConfig.get("class");
			var poolClass = poolClassLoader.loadClass(poolClassName);
			if (poolClass == null || !StoragePool.class.isAssignableFrom(poolClass))
				throw new ConfigException("Unable to load StoragePool class " + poolClassName);

			pool = (StoragePool) poolClass.getConstructor(Config.class).newInstance(scopedConfig);
			pools.put(vault.getName(), pool);
			addMetrics(vault.getName(), pool);

			return pool;

		} catch (final Exception e) {
			throw new PoolError("Unable to open vault: " + vault.getName(), e);
		}
	}

	public Optional<VaultConfig> getVault(String name) {
		try {
			return Optional.of(loadVault(name));
		} catch (Exception e) {
			log.warn("Failed to load {}", name, e);
			return Optional.empty();
		}
	}

	public Set<String> getVaultNames() {
		return Collections.unmodifiableSet(vaultConfigs.keySet());
	}

	public class VaultConfigImpl implements VaultConfig {

		private final String name;
		private final Config config;

		private VaultConfigImpl(String name, Config config) {
			this.name = name;
			this.config = config;
		}

		@Override
		public String getName() {
			return name;
		}

		@Override
		public boolean isPublic() {
			return config.getBool("public");
		}

		@Override
		public List<String> getPropertyNames() {
			return new ArrayList<>(config.keySet());
		}

		@Override
		public String getProperty(String name) {
			return config.get(name, null);
		}

		@Override
		public Map<String, String> getPropertyMap() {
			return config.toMap();
		}

		public Config getConfig() {
			return config;
		}
	}

	public List<VaultConfig> getAll() {
		return new ArrayList<>(vaultConfigs.values());
	}

}
