package de.gwdg.cdstar.runtime.client;

import java.io.IOException;
import java.io.InputStream;
import java.nio.channels.Channels;
import java.nio.channels.WritableByteChannel;

import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.runtime.client.exc.ArchiveNotAvailable;

/**
 * Perform actions that are normally not allowed on cdstar archives and files.
 *
 * TODO: Make proper public API for this functionality
 */
public class LTSMigrationSupport {

	private LTSMigrationSupport() {
	}

	private static MirrorStateImpl cast(CDStarMirrorState state) {
		if (state instanceof MirrorStateImpl)
			return (MirrorStateImpl) state;
		throw new IllegalArgumentException("Implementation not supported: " + state.getClass());
	}

	/**
	 * Change or set the archives 'pending' state.
	 *
	 * @param archive
	 *            the archive to set to 'pending'.
	 * @param taskId
	 *            the task identifier. Can be used to check or abort the pending
	 *            task later.
	 */
	public static void setMigrationTaskPending(CDStarMirrorState mirror, String taskId) {
		cast(mirror).setMigrationTaskId(taskId);
	}

	/**
	 * Return the task id of the pending task, if any.
	 *
	 * @param archive
	 *                the archive to check.
	 * @return a task id, or null.
	 */
	public static String getMigrationTaskId(CDStarMirrorState mirror) {
		return cast(mirror).getMigrationTaskID();
	}


	/**
	 * Remove the 'pending' state from the archive.
	 *
	 * @param archive
	 *            the archive to change.
	 */
	public static void clearMigrationTask(CDStarMirrorState mirror) {
		cast(mirror).setMigrationTaskId(null);
	}

	/**
	 * Clear the mirror state. This is only possible on fully available
	 * archives.
	 *
	 * @param archive
	 *            the archive to act upon.
	 * @throws ArchiveNotAvailable
	 *             if the archive is not fully available
	 */
	public static void clearMirror(CDStarMirrorState mirror) {
		mirror.clearMirror();
	}

	/**
	 * Set a mirror (name and location) and optionally make all files
	 * unavailable. After this, the files have to be recovered from the mirror
	 * before the mirror can be changed again.
	 *
	 * @param archive
	 *            the archive to act upon.
	 * @param mirror
	 *            the mirror name.
	 * @param location
	 *            the mirror-specific location of the copies, used during
	 *            recovery.
	 * @throws ArchiveNotAvailable
	 *             if the archive is not fully available
	 */
	public static void setMirror(CDStarMirrorState mirror, String mirrorName, String location) {
		mirror.setMirror(mirrorName, location);
	}

	public static void freezeFile(CDStarFile file) {
		file.setUnavailable();
	}

	public static void unfreezeFile(CDStarFile file, InputStream data) throws IOException {
		try (WritableByteChannel restoreChannel = file.getRestoreChannel()) {
			Utils.copy(data, Channels.newOutputStream(restoreChannel));
		}
	}

}
