package de.gwdg.cdstar.runtime.tasks;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.CancellationException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codahale.metrics.Counter;
import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.Timer;
import com.codahale.metrics.Timer.Context;

import de.gwdg.cdstar.GromitIterable;
import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.runtime.RuntimeContext;
import de.gwdg.cdstar.runtime.listener.RuntimeListener;
import de.gwdg.cdstar.runtime.services.CronService;
import de.gwdg.cdstar.runtime.services.PoolService;
import de.gwdg.cdstar.runtime.tasks.TaskIOHelper.TaskModel;
import de.gwdg.cdstar.ta.TAListener;
import de.gwdg.cdstar.ta.TransactionInfo;
import de.gwdg.cdstar.ta.UserTransaction;

/**
 * A simple in-memory (plus disk persistence) implementation of
 * {@link TaskService}.
 */
/*
 * TODO: Limit the number of tasks concurrently handled by runners. Have a
 * separate queue for each runner, so a slow runner cannot block fast runners.
 */
public class TaskServiceImpl implements RuntimeListener, TaskService {

	private static final Logger log = LoggerFactory.getLogger(TaskServiceImpl.class);

	private Counter mTotal;
	private Counter mRunning;
	private Counter mFinished;
	private Counter mFailed;
	private Timer mTimer;

	private final GromitIterable<TaskRunner> runners = new GromitIterable<>();

	private final Map<String, TaskImpl> enqueuedTasks = new ConcurrentHashMap<>();
	private final Map<String, BoundTaskGroup> boundTaskGroups = new ConcurrentHashMap<>();

	private Executor pool;
	private int maxErrors = 3;
	private long minDelayMillis = 1000;
	private long maxDelayMillis = 300000;

	private TaskIOHelper io;
	private TaskRecoveryHandler recoveryHandler;

	private CronService cron;

	@Override
	public void onInit(RuntimeContext ctx) throws Exception {
		io = new TaskIOHelper(ctx.getServiceDir("tasks"));
		recoveryHandler = new TaskRecoveryHandler(io.getBasePath());
	}

	TaskRecoveryHandler getRecoveryHandler() {
		return recoveryHandler;
	}

	@Override
	public void onStartup(RuntimeContext ctx) throws Exception {
		io.createPaths();

		final MetricRegistry metrics = ctx.lookupRequired(MetricRegistry.class);
		mTotal = metrics.counter("tasks.total");
		mRunning = metrics.counter("tasks.running");
		mFinished = metrics.counter("tasks.finished");
		mFailed = metrics.counter("tasks.failed");
		mTimer = metrics.timer("tasks.runtime");

		ctx.lookupAll(TaskRunner.class).forEach(runners::addIfNotPresent);
		pool = ctx.lookupRequired(PoolService.class).getNamedPool("cdstar/tasks");
		cron = ctx.lookupRequired(CronService.class);

		log.debug("Loading interrupted or unfinished tasks from disk...");
		try {
			io.findCommittedTaskIDs(id -> {
				TaskModel tm;
				try {
					tm = io.loadTask(id);
				} catch (final IOException e) {
					log.error("Failed to load task {} from disk. Continuing anyway...", id, e);
					return;
				}
				final TaskImpl task = new TaskImpl(this, tm.getId(), tm.getName(), tm.getParameterMap());
				log.debug("Found task: {}", task);
				enqueueTask(task);
			});
		} catch (final IOException e) {
			log.error("Failed to load tasks from disk. Continuing anyway...", e);
		}

	}


	@Override
	public synchronized void onShutdown(RuntimeContext ctx) {
		for (final TaskImpl task : enqueuedTasks.values()) {
			task.interrupt();
		}
	}

	/**
	 * Return the task instance, if not already completed.
	 */
	@Override
	public Optional<Task> getTask(String id) {
		return Optional.ofNullable(enqueuedTasks.get(id));
	}

	@Override
	public List<Task> allTasks() {
		return new ArrayList<>(enqueuedTasks.values());
	}

	@Override
	public Iterator<String> findTaskIDs() {
		return enqueuedTasks.keySet().iterator();
	}

	@Override
	public TaskBuilder builder(String name) {
		return new TaskBuilder(name) {
			@Override
			public PreparedTask prepare() {
				final String id = Utils.bytesToHex(Utils.randomBytes(128 / 8));
				final TaskImpl task = new TaskImpl(TaskServiceImpl.this, id, getName(), getParameterMap());
				return task;
			}
		};
	}

	public TaskIOHelper getIO() {
		return io;
	}

	/**
	 * Called by {@link TaskBuilder#submit()} to create and prepare a new task.
	 */
	TaskImpl prepareTask(TaskImpl task) {
		if (task.isCanceled())
			return task;

		try {
			io.prepare(task.getId(), task.getName(), task.getParameterMap());
			log.debug("Task prepared: {}", task);
		} catch (final Exception e) {
			throw new RejectedExecutionException("Failed to prepare task", e);
		}
		return task;
	}

	/**
	 * Called by {@link TaskImpl#submit()} to mark a task as submitted on disk
	 * (so it can be restarted after a shutdown) and submit it to the task
	 * scheduler. Must be prepared first.
	 */
	void commitTask(TaskImpl task) {
		if (task.isCanceled())
			return;

		try {
			io.submit(task.getId());
			log.debug("Task comitted: {}", task);
		} catch (final RejectedExecutionException e) {
			throw e;
		} catch (final IOException e) {
			throw new RejectedExecutionException(e);
		}
	}

	/**
	 * Submit task to the worker queue
	 */
	void enqueueTask(TaskImpl task) {
		final String taskId = task.getId();
		enqueuedTasks.put(taskId, task);
		mTotal.inc();
		pool.execute(() -> tryRunTask(taskId));
		log.debug("Task enqueued: {}", task);
	}

	/**
	 * Called by {@link TaskImpl#rollback()} to forget a task. This is a nop if
	 * the task was never prepared.
	 **/
	void rollbackTask(TaskImpl task) {
		io.forget(task.getId(), false);
	}

	/**
	 * Called by {@link TaskImpl#submit()} to mark a task as submitted on disk
	 * (so it can be restarted after a shutdown) and submit it to the task
	 * scheduler. Must be prepared first.
	 */
	void submitTask(TaskImpl task) {
		prepareTask(task);
		commitTask(task);
		enqueueTask(task);
	}

	void bindTask(TaskImpl task, UserTransaction ta) {

		final BoundTaskGroup tg = boundTaskGroups.computeIfAbsent(ta.getId(), txId -> {
			final BoundTaskGroup rx = new BoundTaskGroup(txId, this);
			ta.bind(rx);
			ta.addListener(new TAListener() {
				@Override
				public void afterCommit(TransactionInfo t) {
					boundTaskGroups.remove(t.getId());
				}

				@Override
				public void afterRollback(TransactionInfo t) {
					boundTaskGroups.remove(t.getId());
				}
			});
			return rx;
		});

		tg.addTask(task);
		log.debug("Task {} bound to {}", task.getId(), ta.getId());
	}

	/**
	 * Materialize the {@link TaskImpl} from its string id and pass it to its
	 * task runner.
	 */
	private void tryRunTask(String taskId) {
		mRunning.inc();
		log.debug("Task started: {}", taskId);

		final TaskImpl task = enqueuedTasks.get(taskId);
		if (task == null)
			return;

		CompletableFuture<Void> cf = null;
		try {
			if (task.isCanceled())
				throw new CancellationException();

			if (task.isInterrupted())
				throw new InterruptedException();

			final TaskRunner runner = Utils.first(runners, r -> r.canRun(task));
			if (runner == null)
				throw new MissingTaskRunnerException(task.getName());

			if ((cf = runner.run(task)) == null)
				throw new FatalTaskException("Task runner returned null");

			final Context timer = mTimer.time();
			cf.whenComplete((the, err) -> timer.close());
			cf.whenComplete((the, err) -> whenComplete(task, err));
			task.setFuture(cf);
		} catch (final Exception e) {
			if (cf == null) {
				cf = new CompletableFuture<Void>();
				task.setFuture(cf);
			}
			cf.completeExceptionally(e);
			whenComplete(task, e);
		}

	}

	private void whenComplete(TaskImpl task, Throwable err) {
		mRunning.dec();
		final String taskId = task.getId();

		// Ensure task is marked as canceled/interrupted if needed
		if (err instanceof CancellationException && !task.isCanceled())
			task.cancel();
		if (err instanceof InterruptedException && !task.isInterrupted())
			task.interrupt();

		// Task completed successfully (or was canceled, which is a success
		// even if the actual task failed)
		if (err == null || task.isCanceled()) {
			log.debug("Task finished: {}", task);
			mFinished.inc();
			io.forget(task.getId(), true);
			enqueuedTasks.remove(taskId);
			return; // Done
		}

		// Task was interrupted (e.g. during shutdown. Do not re-schedule!)
		if (task.isInterrupted()) {
			return;
		}

		task.errorCount++;

		// Task exceeded retry limits or failed with an unrecoverable error
		if (task.errorCount > maxErrors || err instanceof FatalTaskException || err instanceof Error) {
			mFailed.inc();
			enqueuedTasks.remove(taskId);
			log.error("Task failed (no retry): {}", task, err);
			return;
		}

		// Task failed, but can be retried
		final long waitMillis = getDelayMillis(task.errorCount);

		log.warn("Task failed (retry {}/{}, delay {}ms): {}", task.errorCount, maxErrors, waitMillis, task, err);

		cron.schedule(() -> pool.execute(() -> tryRunTask(taskId)),
			waitMillis, TimeUnit.MILLISECONDS);
	}

	public void addRunner(TaskRunner runner) {
		runners.addIfNotPresent(runner);
	}

	public long getTasksTotal() {
		return mTotal.getCount();
	}

	public long getTasksRunning() {
		return mRunning.getCount();
	}

	public long getTasksFinished() {
		return mFinished.getCount();
	}

	public long getTasksFailed() {
		return mFailed.getCount();
	}

	public void setDelay(long minDelay, long maxDelay, TimeUnit unit) {
		minDelayMillis = Math.max(0, unit.toMillis(minDelay));
		maxDelayMillis = Math.max(minDelayMillis, unit.toMillis(maxDelay));
	}

	public void setMaxErrors(int allowedErrors) {
		maxErrors = Math.max(1, allowedErrors);
	}

	private long getDelayMillis(long errorCount) {
		// The Math.max protects against races when
		return Math.max(0, minDelayMillis + (((maxDelayMillis - minDelayMillis) / maxErrors) * (errorCount - 1)));
	}

}
