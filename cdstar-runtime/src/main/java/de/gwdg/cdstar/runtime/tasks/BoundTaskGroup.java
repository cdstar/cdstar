package de.gwdg.cdstar.runtime.tasks;

import java.util.ArrayList;
import java.util.List;

import de.gwdg.cdstar.ta.TAListener;
import de.gwdg.cdstar.ta.TAResource;
import de.gwdg.cdstar.ta.TransactionHandle;
import de.gwdg.cdstar.ta.TransactionInfo;

public class BoundTaskGroup implements TAResource {

	List<TaskImpl> tasks = new ArrayList<>();
	private final TaskServiceImpl service;

	public BoundTaskGroup(String txId, TaskServiceImpl taskServiceImpl) {
		service = taskServiceImpl;
	}

	@Override
	public void bind(TransactionHandle t) {
		t.bindRecoveryHandler(service.getRecoveryHandler());
	}

	@Override
	public void rollback(TransactionHandle t) {
		for (final TaskImpl task : tasks) {
			task.cancel();
			service.rollbackTask(task);
		}
	}

	@Override
	public void prepare(TransactionHandle t) throws Exception {
		for (final TaskImpl task : tasks) {
			service.getRecoveryHandler().rememberTask(t.getJournal(), task.getId());
			service.prepareTask(task);
		}
	}

	@Override
	public void commit(TransactionHandle t) {
		for (final TaskImpl task : tasks) {
			service.commitTask(task);
		}

		// Delay task submission to make sure a task can see the
		// results of the transaction is was bound to.
		t.addListener(new TAListener() {
			@Override
			public void afterCommit(TransactionInfo t) {
				for (final TaskImpl task : tasks)
					service.enqueueTask(task);
			}
		});
	}

	public void addTask(TaskImpl task) {
		tasks.add(task);
	}

}
