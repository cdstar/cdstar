package de.gwdg.cdstar.runtime.client;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.channels.Channels;
import java.nio.channels.WritableByteChannel;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;

import de.gwdg.cdstar.SharedObjectMapper;
import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.pool.BackendError;
import de.gwdg.cdstar.pool.Resource;
import de.gwdg.cdstar.runtime.client.auth.ArchivePermission;
import de.gwdg.cdstar.runtime.client.exc.SnapshotLocked;

class AttributeCache {

	private final ArchiveImpl a;
	private final Map<String, Map<String, AttributeImpl>> attrMap;
	private boolean modified = false;
	private boolean modifiedInternal;

	AttributeJsonFormat<AttributeImpl> jsonFormat = new AttributeJsonFormat<AttributeImpl>() {
		@Override
		public AttributeImpl makeAttr(String fileId, String attrName, List<String> values) {
			CDStarFile file = null;
			if (Utils.notNullOrEmpty(fileId))
				file = getFileByID(fileId);
			return new AttributeImpl(AttributeCache.this, file, attrName, values);
		}

		private CDStarFile getFileByID(String fileId) {
			for(CDStarFile file: snapshot != null ? snapshot.getFiles() : a.getFiles()) {
				if(fileId.equals(file.getID()))
					return file;
			}
			throw new BackendError.DamagedDataError("Metadata table references unknown file id: " + fileId);
		}

		@Override
		public List<String> getValues(AttributeImpl attr) {
			if (attr.getFile() != null && attr.getFile().isRemoved())
				return Collections.emptyList();
			return attr.values();
		}
	};
	private SnapshotImpl snapshot;

	public AttributeCache(ArchiveImpl archive, SnapshotImpl snapshot) {
		this.a = archive;
		this.snapshot = snapshot;

		Resource store = snapshot != null
				? snapshot.getResource(ArchiveImpl.METAFILE)
				: a.getResource(ArchiveImpl.METAFILE);

		if (store != null && !ArchiveImpl.METAFILE_TYPE.equals(store.getMediaType()))
			throw new BackendError("Unexpected metadata file type");

		if (store == null || store.getSize() == 0) {
			attrMap = new HashMap<>();
		} else {
			try (InputStream is = new BufferedInputStream(Channels.newInputStream(store.getReadChannel(0)))) {
				final JsonParser parser = SharedObjectMapper.json.getFactory().createParser(is);
				attrMap = jsonFormat.parse(parser);
				parser.close();
			} catch (final IOException e1) {
				throw new BackendError("Failed to load metadata table", e1);
			}
		}
	}

	synchronized void flush() {
		if (!modified && !modifiedInternal)
			return; // Nothing to do

		if (snapshot != null)
			throw Utils.wtf();

		Resource store = a.getResource(ArchiveImpl.METAFILE);

		if (attrMap.isEmpty()) {
			if (store != null)
				store.remove();
			return;
		}

		if (store == null) {
			store = a.createResource(ArchiveImpl.METAFILE);
			store.setMediaType(ArchiveImpl.METAFILE_TYPE);
		}

		try (WritableByteChannel wch = store.getWriteChannel(0);
				BufferedOutputStream out = new BufferedOutputStream(Channels.newOutputStream(wch));
				JsonGenerator gen = SharedObjectMapper.json.getFactory().createGenerator(out)) {
			jsonFormat.serialize(gen, attrMap);
		} catch (final IOException e) {
			throw new BackendError("Failed to store metadata", e);
		}

	}

	public synchronized CDStarAttribute getAttribute(FileImpl file, String name) {
		if (!CDStarAttributeHelper.isValidAttributeName(name))
			throw new IllegalArgumentException("Not a valid attribute name: " + name);

		final String key = file == null ? "" : file.getID();
		return attrMap.computeIfAbsent(key, f -> new HashMap<>()).computeIfAbsent(name,
				propName -> new AttributeImpl(this, file, name, new ArrayList<>()));
	}

	public synchronized Set<CDStarAttribute> getAttributes(FileImpl file) {
		final String key = file == null ? "" : file.getID();
		return new HashSet<>(attrMap.computeIfAbsent(key, f -> new HashMap<>()).values());
	}

	public synchronized Set<String> getAttributeNames(FileImpl file) {
		final String key = file == null ? "" : file.getID();
		return new HashSet<>(attrMap.computeIfAbsent(key, f -> new HashMap<>()).keySet());
	}

	void setModifed(AttributeImpl attr) {
		if (!modified) {
			if (snapshot != null)
				throw new SnapshotLocked();
			a.requireTransactionWriteable();
			a.requirePayloadWriteable();
			a.checkPermission(ArchivePermission.CHANGE_META);
			modified = true;
			a.markContentModified();
		}
		a.forEachListener(l -> l.propertyChanged(attr, attr.getFile(), attr.getOrigValues()));
	}

	/**
	 * Clears the attributes for a given file, if present. Does not check for
	 * permissions, as we assume that this is just a side-effect of removing a file.
	 */
	void onFileRemoved(FileImpl file) {
		if (attrMap.remove(file.getID()) != null) {
			modifiedInternal = true;
		}
	}

}
