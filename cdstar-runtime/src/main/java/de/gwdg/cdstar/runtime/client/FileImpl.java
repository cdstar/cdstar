package de.gwdg.cdstar.runtime.client;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;
import java.util.Date;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.pool.PoolError.ExternalResourceException;
import de.gwdg.cdstar.pool.PoolError.StaleHandle;
import de.gwdg.cdstar.pool.Resource;
import de.gwdg.cdstar.runtime.client.auth.ArchivePermission;
import de.gwdg.cdstar.runtime.client.exc.ArchiveNotAvailable;
import de.gwdg.cdstar.runtime.client.exc.FileAvailable;
import de.gwdg.cdstar.runtime.client.exc.FileExists;
import de.gwdg.cdstar.runtime.client.exc.InvalidFileName;
import de.gwdg.cdstar.runtime.client.exc.SnapshotLocked;

class FileImpl implements CDStarFile {

	private static final String DEFAULT_MEDIA_TYPE = "application/octet-stream";
	static final String RESOURCE_PREFIX = "data/";
	final ArchiveImpl archive;
	final Resource resource;
	String name;
	private final long oldSize;
	private SnapshotImpl snapshot;

	FileImpl(ArchiveImpl a, Resource r, String n, SnapshotImpl snapshot) {
		archive = Objects.requireNonNull(a);
		resource = Objects.requireNonNull(r);
		name = n;
		oldSize = r.getSize();
		this.snapshot = snapshot;
	}

	void flush() {
		// Nothing to do. Everything is synced with resource object already.
	}

	@Override
	public CDStarArchive getArchive() {
		return archive;
	}

	@Override
	public CDStarSnapshot getSnapshot() {
		return snapshot;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getID() {
		return snapshot == null ? resource.getId() : snapshot.getOrigFileId(resource);
	}

	private static final String ds = "." + PATH_SEP;
	private static final String sd = PATH_SEP + ".";
	private static final String sds = PATH_SEP + "." + PATH_SEP;
	private static final String dds = ".." + PATH_SEP;
	private static final String sdds = PATH_SEP + ".." + PATH_SEP;
	private static final String sdd = PATH_SEP + "..";

	/**
	 * File names must not be empty, start or end with a slash (PATH_SEP='/') or
	 * contain a path segment that is empty, '.' or '..'. Filenames also must
	 * not start with a colon (':') as this is used to mark special queries in
	 * the REST API.
	 *
	 * @param name
	 * @throws InvalidFileName
	 */
	private static String normalizeFilename(String name) throws InvalidFileName {
		if (name == null || name.isEmpty())
			throw new InvalidFileName(name, "Names must not be empty.");
		name = Utils.normalizeNFKC(name);
		if (name.startsWith(PATH_SEP))
			throw new InvalidFileName(name, "Names must not start with a path separator.");
		if (name.endsWith(PATH_SEP))
			throw new InvalidFileName(name, "Names must not end with a path separator.");
		if (name.startsWith(ds) || name.contains(sds) || name.endsWith(sd))
			throw new InvalidFileName(name, "Names must not contain '.' as a path segment.");
		if (name.startsWith(dds) || name.contains(sdds) || name.endsWith(sdd))
			throw new InvalidFileName(name, "Names must not contain '..' as a path segment.");
		final char c = name.charAt(0);
		if (c < 0x1f || c == 0x7f || c == ':' || c == '\n' || c == '\r' || c == '\t')
			throw new InvalidFileName(name, "Names must not start with unsafe characters or ':'");
		return name;
	}

	private void checkNameConflict(String newName) throws FileExists {
		if (newName == null)
			newName = getName();
		if (newName == null)
			return;

		final String prefix = newName + PATH_SEP;

		for (final CDStarFile other : archive.getInternalFileList()) {
			final String otherName = other.getName();

			if (otherName == null)
				continue;

			if (otherName.equals(newName))
				throw new FileExists(newName);

			if (otherName.startsWith(prefix))
				throw new FileExists(newName, otherName);

			if (newName.startsWith(otherName)
					&& newName.regionMatches(otherName.length(), PATH_SEP, 0, PATH_SEP.length()))
				throw new FileExists(newName, otherName);
		}
	}

	private void requireWriteable() {
		if (snapshot != null)
			throw new SnapshotLocked();
		archive.requireTransactionWriteable();
		archive.requirePayloadWriteable();
		archive.checkPermission(ArchivePermission.CHANGE_FILES);
	}

	private void requireAvailable() {
		if (resource.isExternal()) {
			String id = snapshot == null ? archive.getId() : snapshot.getId();
			throw new ArchiveNotAvailable(archive.getVault().getName(), id);
		}
	}

	@Override
	public void setName(String name) throws FileExists, InvalidFileName {
		requireWriteable();
		name = normalizeFilename(name);

		if (Utils.equal(this.name, name))
			return;

		checkNameConflict(name);

		resource.setName(RESOURCE_PREFIX + name);

		final String oldName = this.name;
		this.name = name;

		archive.markContentModified();
		if (oldName != null)
			archive.forEachListener(l -> l.fileNameChanged(this, oldName));
	}

	@Override
	public String getMediaType() {
		final String type = resource.getMediaType();
		if (type == null)
			return DEFAULT_MEDIA_TYPE;
		return type;
	}

	@Override
	public String getContentEncoding() {
		return resource.getContentEncoding();
	}

	@Override
	public void setMediaType(String type, String coding) {
		requireWriteable();
		if (Utils.equal(getMediaType(), type) && Utils.equal(getContentEncoding(), coding))
			return;
		if (Utils.equal(type, DEFAULT_MEDIA_TYPE))
			type = null; // Do not store the obvious
		resource.setMediaType(type, coding);
		archive.markContentModified();
	}

	@Override
	public void setMediaType(String type) {
		setMediaType(type, null);
	}

	@Override
	public long getSize() {
		return resource.getSize();
	}

	@Override
	public Date getCreated() {
		return snapshot == null ? resource.getCreated() : snapshot.getOrigFileCreated(resource);
	}

	@Override
	public Date getLastModified() {
		return snapshot == null ? resource.getLastModified() : snapshot.getOrigFileModified(resource);
	}

	@Override
	public boolean isAvailable() {
		return !resource.isExternal();
	}

	@Override
	public InputStream getStream() throws IOException {
		archive.requireTransactionAlive();
		archive.checkPermission(ArchivePermission.READ_FILES);
		requireAvailable();

		return Channels.newInputStream(resource.getReadChannel(0));
	}

	@Override
	public Map<String, String> getDigests() {
		return resource.getDigests();
	}

	@Override
	public CDStarFile truncate(long length) throws StaleHandle, ExternalResourceException, IOException {
		requireWriteable();

		if (length >= 0 && length < getSize()) {
			final long diff = length - getSize();
			resource.getWriteChannel(length).close();
			archive.markContentModified();
			archive.forEachListener(l -> l.fileSizeChanged(this, diff));
		}
		return this;
	}

	@Override
	public ReadableByteChannel getReadChannel(long pos) throws IOException {
		archive.requireTransactionAlive();
		archive.checkPermission(ArchivePermission.READ_FILES);
		requireAvailable();

		return resource.getReadChannel(pos);
	}

	@Override
	public WritableByteChannel getWriteChannel() throws StaleHandle, ExternalResourceException, IOException {
		requireWriteable();

		return new WritableByteChannel() {
			@SuppressWarnings("resource")
			final WritableByteChannel delegate = resource.getWriteChannel(-1);
			long bytesWritten;

			@Override
			public boolean isOpen() {
				return delegate.isOpen();
			}

			@Override
			public void close() throws IOException {
				if (bytesWritten > 0) {
					archive.markContentModified();
					archive.forEachListener(l -> l.fileSizeChanged(FileImpl.this, bytesWritten));
				}
				delegate.close();
			}

			@Override
			public int write(ByteBuffer src) throws IOException {
				final int len = delegate.write(src);
				bytesWritten += len;
				return len;
			}
		};
	}

	@Override
	public WritableByteChannel getRestoreChannel() throws StaleHandle, IOException, FileAvailable {
		archive.requireTransactionWriteable();
		if (!resource.isExternal())
			throw new FileAvailable(archive.getVault().getName(),
					snapshot != null ? snapshot.getId() : archive.getId(), name);

		return resource.getRestoreChannel();
	}

	@Override
	public void transferFrom(CDStarFile source) throws IOException {
		requireWriteable();
		if (getSize() == 0 && source instanceof FileImpl) {
			resource.cloneFrom(((FileImpl) source).resource);
			archive.markContentModified();
			archive.forEachListener(l -> l.fileSizeChanged(FileImpl.this, getSize()));
		} else {
			CDStarFile.super.transferFrom(source);
		}
	}

	@Override
	public void remove() {
		requireWriteable();
		archive.removeFile(this);
		archive.forEachListener(l -> l.fileRemoved(this));
	}

	public long getOldSize() {
		return oldSize;
	}

	@Override
	public CDStarAttribute getAttribute(String name) {
		return (snapshot == null ? archive.getMeta() : snapshot.getAttributeCache()).getAttribute(this,
				name.toString());
	}

	@Override
	public Set<CDStarAttribute> getAttributes() {
		return (snapshot == null ? archive.getMeta() : snapshot.getAttributeCache()).getAttributes(this);
	}

	@Override
	public void setUnavailable() {
		CDStarMirrorState mirror = snapshot != null ? snapshot.getMirrorState() : archive.getMirrorState();
		if (!mirror.isMirrored())
			throw new IllegalStateException("Freezing files is only allowed if mirror is set.");
		try {
			// We use an URI of the form mirror:<name>#<location> as the external location
			// hint
			String location = new URI("mirror", mirror.getMirrorName(), mirror.getMirrorLocation()).toString();
			resource.setExternalLocation(location);
		} catch (URISyntaxException e) {
			throw Utils.wtf(e);
		}
	}

}
