package de.gwdg.cdstar.runtime.client;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.auth.Principal;
import de.gwdg.cdstar.auth.Subject;
import de.gwdg.cdstar.runtime.client.auth.ArchivePermission;
import de.gwdg.cdstar.runtime.client.auth.StringSubject;
import de.gwdg.cdstar.runtime.client.auth.StringSubject.GroupSubject;
import de.gwdg.cdstar.runtime.client.auth.StringSubject.PrincipalSubject;

/**
 * This class implements permission checks against the ACL list of an archive.
 */
public class AclChecker {

	private final String vaultName;
	private final String archiveId;
	private final Subject subject;
	private final PrincipalSubject owner;
	private final Map<StringSubject, EnumSet<ArchivePermission>> acl;

	private final EnumSet<ArchivePermission> cachedPermissions = EnumSet.noneOf(ArchivePermission.class);
	private HashSet<GroupSubject> checkedGroups;

	/**
	 * Create a new access control resolver
	 */
	public AclChecker(String vaultName, String archiveId, Subject subject, PrincipalSubject owner,
		Map<StringSubject, EnumSet<ArchivePermission>> acl) {
		this.vaultName = vaultName;
		this.archiveId = archiveId;
		this.subject = subject;
		this.owner = owner;
		this.acl = acl;
		loadDirectPermissions();
	}

	/**
	 * Load all permissions that apply to the current subject and do not require
	 * any expensive lookup (e.g. group permissions)
	 */
	private void loadDirectPermissions() {
		Set<ArchivePermission> tmp;
		if ((tmp = acl.get(StringSubject.SpecialSubject.ANY)) != null)
			cachedPermissions.addAll(tmp);
		if (subject.hasPrincipal()) {
			final Principal pcpl = subject.getPrincipal();
			final PrincipalSubject pFull = new PrincipalSubject(pcpl.getId(), pcpl.getDomain());
			final PrincipalSubject pNoDomain = new PrincipalSubject(pcpl.getId(), null);
			final boolean isOwner = Utils.equalNotNull(owner, pFull);

			if ((tmp = acl.get(StringSubject.SpecialSubject.USER)) != null)
				cachedPermissions.addAll(tmp);
			if (isOwner && (tmp = acl.get(StringSubject.SpecialSubject.OWNER)) != null)
				cachedPermissions.addAll(tmp);
			if ((tmp = acl.get(pFull)) != null)
				cachedPermissions.addAll(tmp);
			if ((tmp = acl.get(pNoDomain)) != null)
				cachedPermissions.addAll(tmp);
		}
	}

	/**
	 * Check if an ACL group permission matches the current subject and contains
	 * the requested permissions.
	 *
	 * Found permissions are cached, and already checked groups are also
	 * remembered, so multiple calls to this function only check groups that
	 * were not already checked, and only if no other entry already granted that
	 * permission.
	 */
	private boolean checkGroupPermissions(ArchivePermission permission) {
		if (checkedGroups == null)
			checkedGroups = new HashSet<>();

		for (final Entry<StringSubject, EnumSet<ArchivePermission>> entry : acl.entrySet()) {
			if (!(entry.getKey() instanceof GroupSubject))
				continue;
			if (!entry.getValue().contains(permission))
				continue;
			final GroupSubject group = (GroupSubject) entry.getKey();
			if (!checkedGroups.add(group))
				continue;

			final boolean groupMatch = group.isQualified()
				? subject.isMemberOf(group.getName(), group.getDoamin())
				: subject.isMemberOf(group.getName());
			if (!groupMatch)
				continue;

			cachedPermissions.addAll(entry.getValue());
			return true;
		}

		return false;
	}

	private boolean checkExternalPermissions(ArchivePermission permission) {
		if (subject.isPermitted(permission.toStringPermission(vaultName, archiveId))) {
			cachedPermissions.add(permission);
			return true;
		}
		return false;
	}

	/**
	 * Check if the current principal is granted the given permission by this
	 * ACL (or globally). The results are cached locally to speed up future
	 * checks.
	 */
	synchronized boolean isPermitted(ArchivePermission permission) {
		if (cachedPermissions.contains(permission))
			return true;
		if (checkGroupPermissions(permission))
			return true;
		if (checkExternalPermissions(permission))
			return true;
		return false;
	}

	public PrincipalSubject getOwner() {
		return owner;
	}

	/**
	 * Return (a deep copy of) the ACL map used to initialize this checker.
	 */
	public Map<StringSubject, EnumSet<ArchivePermission>> getAclMap() {
		final Map<StringSubject, EnumSet<ArchivePermission>> copy = new HashMap<>(acl.size());
		acl.forEach((s, p) -> copy.put(s, p.clone()));
		return copy;
	}

}
