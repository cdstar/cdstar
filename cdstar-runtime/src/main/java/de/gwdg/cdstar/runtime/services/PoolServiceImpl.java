package de.gwdg.cdstar.runtime.services;

import java.io.Closeable;
import java.io.IOException;
import java.nio.file.FileStore;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.gwdg.cdstar.NamedThreadFactory;
import de.gwdg.cdstar.runtime.RuntimeContext;
import de.gwdg.cdstar.runtime.listener.RuntimeListener;

public class PoolServiceImpl implements PoolService, RuntimeListener, Closeable {
	private static final Logger log = LoggerFactory.getLogger(PoolServiceImpl.class);

	private final Map<FileStore, ThreadPoolExecutor> ioPools = new HashMap<>();
	private final Map<String, ThreadPoolExecutor> namedPools = new HashMap<>();

	private final long defaultKeepAliveMillis = TimeUnit.SECONDS.toMillis(60);
	private final int cpuPoolThreads = Runtime.getRuntime().availableProcessors() * 2;
	private final int ioPoolThreads = Math.max(4, cpuPoolThreads);
	private boolean closed;

	@Override
	public synchronized ExecutorService getIOPool(Path forFile) throws IOException {
		if (closed)
			throw new IllegalStateException("Service closed");
		return ioPools.computeIfAbsent(Files.getFileStore(forFile), (fs) -> {
			log.info("Creating managed IO pool for file system: {}", fs);
			return mkPool("IO " + fs.toString(), ioPoolThreads);
		});
	}

	@Override
	public synchronized ExecutorService getNamedPool(String name) {
		if (closed)
			throw new IllegalStateException("Service closed");
		return namedPools.computeIfAbsent(name, (key) -> {
			log.info("Creating managed named pool: {}", key);
			return mkPool(key, cpuPoolThreads);
		});
	}

	public ThreadPoolExecutor mkPool(String name, int poolSize) {
		final ThreadPoolExecutor pool = new ThreadPoolExecutor(poolSize, poolSize,
			defaultKeepAliveMillis, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<>(), new NamedThreadFactory(name));
		pool.allowCoreThreadTimeOut(true);
		return pool;
	}

	@Override
	public void onShutdown(RuntimeContext ctx) {
		close();
	}

	@Override
	public void close() {
		final List<ThreadPoolExecutor> allPools;

		synchronized (this) {
			closed = true;
			allPools = new ArrayList<>(ioPools.size() + namedPools.size());
			allPools.addAll(ioPools.values());
			allPools.addAll(namedPools.values());
		}

		allPools.forEach(ThreadPoolExecutor::shutdown);
		allPools.removeIf(ThreadPoolExecutor::isTerminated);
		while (!allPools.isEmpty()) {
			try {
				log.info("Waiting for managed thread pools to terminate ({} left)...", allPools.size());
				allPools.get(0).awaitTermination(10, TimeUnit.SECONDS);
				allPools.removeIf(ThreadPoolExecutor::isTerminated);
			} catch (final InterruptedException e) {
				log.warn("Shutdown interrupted. Some threads may still be running.");
				Thread.currentThread().interrupt();
				return;
			}
		}

		log.info("All managed thread pools terminated.");
	}
}