package de.gwdg.cdstar.runtime.services;

import java.io.Closeable;
import java.io.IOException;
import java.util.concurrent.Callable;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import de.gwdg.cdstar.NamedThreadFactory;
import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.runtime.RuntimeContext;
import de.gwdg.cdstar.runtime.listener.RuntimeListener;

public class CronFeature implements RuntimeListener {
	private CronServiceImpl cron;

	@Override
	public void onInit(RuntimeContext ctx) throws Exception {
		cron = new CronServiceImpl();
		ctx.register(cron);
		ctx.register(cron.pool);
		ctx.register(new RuntimeListener() {
			@Override
			public void onShutdown(RuntimeContext ctx) {
				Utils.closeQuietly(cron);
			}
		});
	}

	public static class CronServiceImpl implements Closeable, CronService {
		private ScheduledExecutorService pool;

		public CronServiceImpl() {
			pool = new ScheduledThreadPoolExecutor(4, new NamedThreadFactory("cron"));
		}

		@Override
		public ScheduledFuture<?> schedule(Runnable command, long delay, TimeUnit unit) {
			return pool.schedule(command, delay, unit);
		}

		@Override
		public <V> ScheduledFuture<V> schedule(Callable<V> callable, long delay, TimeUnit unit) {
			return pool.schedule(callable, delay, unit);
		}

		@Override
		public ScheduledFuture<?> scheduleAtFixedRate(Runnable command, long initialDelay, long period, TimeUnit unit) {
			return pool.scheduleAtFixedRate(command, initialDelay, period, unit);
		}

		@Override
		public ScheduledFuture<?> scheduleWithFixedDelay(Runnable command, long initialDelay, long delay,
				TimeUnit unit) {
			return pool.scheduleWithFixedDelay(command, initialDelay, delay, unit);
		}

		@Override
		public void close() throws IOException {
			pool.shutdown();
		}

	}

}
