package de.gwdg.cdstar.runtime.lts;

import java.util.concurrent.CompletableFuture;

import de.gwdg.cdstar.runtime.RuntimeContext;
import de.gwdg.cdstar.runtime.client.CDStarClient;
import de.gwdg.cdstar.runtime.listener.RuntimeListener;
import de.gwdg.cdstar.runtime.tasks.Task;
import de.gwdg.cdstar.runtime.tasks.TaskRunner;

class MigrationTaskRunner implements TaskRunner, RuntimeListener {
	static final String PARAM_VAULT = "vault";
	static final String PARAM_ARCHIVE = "archive";
	public static final String PARAM_SNAPSHOT = "snapshot";
	private final LTSMigrationService migrationService;
	private RuntimeContext runtime;
	private CDStarClient systemClient;

	public MigrationTaskRunner(LTSMigrationService migrationService) {
		this.migrationService = migrationService;
	}

	@Override
	public void onStartup(RuntimeContext ctx) throws Exception {
		runtime = ctx;
		systemClient = runtime.getClient(runtime.getSystemUser("migration"));
	}

	private String getRequiredParameter(Task task, String param) {
		return task
			.getOptional(param)
			.orElseThrow(() -> new IllegalArgumentException("Missing task parameter: " + param));
	}

	@Override
	public CompletableFuture<Void> run(Task task) {
		final String vault = getRequiredParameter(task, PARAM_VAULT);
		final String archive = getRequiredParameter(task, PARAM_ARCHIVE);
		final String snapshot = task.getOptional(PARAM_SNAPSHOT).orElse(null);
		final MigrationTask migration = new MigrationTask(migrationService, systemClient, vault,
				archive, snapshot, task.getId());
		// TODO: Implement retry logic
		return migration.start();
	}

}
