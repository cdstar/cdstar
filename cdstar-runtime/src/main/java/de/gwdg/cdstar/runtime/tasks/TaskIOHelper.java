package de.gwdg.cdstar.runtime.tasks;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Map;
import java.util.function.Consumer;
import java.util.stream.Stream;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import de.gwdg.cdstar.SharedObjectMapper;
import de.gwdg.cdstar.Utils;

class TaskIOHelper {

	private static final String EXT_PREPARED = ".json.tmp";
	private static final String EXT_SUBMITTED = ".json";

	@JsonPropertyOrder({ "id", "name", "params" })
	static class TaskModel {
		final String id;
		final String name;
		final Map<String, String> params;

		@JsonCreator
		public TaskModel(@JsonProperty("id") String id, @JsonProperty("name") String name,
			@JsonProperty("params") Map<String, String> params) {
			this.id = id;
			this.name = name;
			this.params = params;
		}

		@JsonProperty("id")
		public String getId() {
			return id;
		}

		@JsonProperty("name")
		public String getName() {
			return name;
		}

		@JsonProperty("params")
		public Map<String, String> getParameterMap() {
			return params;
		}
	}

	private final Path base;

	TaskIOHelper(Path basePath) {
		base = basePath;
	}

	void createPaths() throws IOException {
		Files.createDirectories(base);
	}

	Path getBasePath() {
		return base;
	}

	Path pathFor(String taskId, boolean submitted) {
		return base.resolve(taskId + (submitted ? EXT_SUBMITTED : EXT_PREPARED));
	}

	void prepare(String id, String name, Map<String, String> params) throws IOException {
		final TaskModel model = new TaskModel(id, name, params);
		SharedObjectMapper.json_compact.writeValue(pathFor(id, false).toFile(), model);
	}

	void submit(String taskId) throws IOException {
		Files.move(pathFor(taskId, false), pathFor(taskId, true));
	}

	void forget(String taskId, boolean submitted) {
		Utils.deleteQuietly(pathFor(taskId, submitted));
	}

	boolean isPrepared(String taskId) {
		return Files.exists(pathFor(taskId, false));
	}

	/**
	 * Load a (submitted) task.
	 */
	TaskModel loadTask(String taskId) throws IOException {
		return SharedObjectMapper.json_compact.readValue(pathFor(taskId, true).toFile(),
			TaskModel.class);
	}

	void findCommittedTaskIDs(Consumer<String> consumeId) throws IOException {
		try (Stream<Path> ds = Files.list(base)) {
			ds
				.filter(Files::isRegularFile)
				.map(p -> p.getFileName().toString())
				.filter(fn -> fn.endsWith(EXT_SUBMITTED))
				.map(fn -> fn.substring(0, fn.length() - EXT_SUBMITTED.length()))
				.forEach(consumeId);
		}
	}

}
