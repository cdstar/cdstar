package de.gwdg.cdstar.runtime.lts.bagit;

import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Duration;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.gwdg.cdstar.runtime.Config;
import de.gwdg.cdstar.runtime.ConfigException;
import de.gwdg.cdstar.runtime.Plugin;
import de.gwdg.cdstar.runtime.RuntimeContext;
import de.gwdg.cdstar.runtime.listener.RuntimeListener;
import de.gwdg.cdstar.runtime.lts.ArchiveHandle;
import de.gwdg.cdstar.runtime.lts.ArchiveReference;
import de.gwdg.cdstar.runtime.lts.LTSLocation;
import de.gwdg.cdstar.runtime.lts.LTSTarget;
import de.gwdg.cdstar.runtime.services.CronService;
import de.gwdg.cdstar.runtime.services.PoolService;

/**
 * This {@link LTSTarget} exports archives into BagIt folders, and is designed
 * to work with external worker processes for the actual migration from/to LTS
 * storage (e.g. tape).
 *
 * The exporter will create a BagIt package in a temporary folder, then rename
 * it to `[name].bagit` with a unique name. A worker process may check for these
 * folders and copy or move these to LTS.
 *
 * The importer will create a file named `[name].want` and start the import as
 * soon as the `[name].bagit` folder can be found. A worker process should check
 * for these `[name].want` files and recover the missing `[name].bagit` folder
 * from LTS. Once complete, the importer will delete the `[name].want` file and
 * the recovered `[name].bagit` folder can be cleaned up by the external worker.
 *
 * If the external copy is no longer needed, a `[name].delete` file is created.
 * This usually happens right after an import. A worker process should watch for
 * these files, remove the external copy (if any), remove the `[name].bagit`
 * directory (if present), and only then remove the `[name].delete` file.
 *
 * External workers are allowed to create additional files for their own state
 * handling, as long as they do not interfere with the names defined here.
 *
 * <h2>BagIt</h2>
 *
 * The `[name].bagit` exports follow the BagIt (V1.0) standard:
 * https://tools.ietf.org/html/draft-kunze-bagit-17
 *
 * In addition, there will be a <code>cdstar.json</code> in the root folder of
 * the bagit export containing additional information about the cdstar archive
 * (e.g. user-defined metadata attributes).
 *
 * TODO: Allows worker processes to prevent recovery timeouts (e.g. by touching
 * the `[name].want` file?).
 *
 * TODO: Watch the directory for changes to be able to continue faster.
 *
 */
@Plugin
public class BagitTarget implements LTSTarget, RuntimeListener {

	static final Logger log = LoggerFactory.getLogger(BagitTarget.class);

	public static final String WANTS_SUFFIX = "wants";
	public static final String DELETE_SUFFIX = "delete";
	public static final String BAGIT_SUFFIX = "bagit";

	public static final String PARAM_NAME = "name";
	public static final String PARAM_PATH = "path";
	public static final String PARAM_CHECK_RETRY = "check.retry";
	public static final String PARAM_CHECK_INTERVAL = "check.interval";

	private final String name;
	private ExecutorService pool;
	private final Path path;
	private final Path tmpPath;
	private CronService cron;
	int retryMax;
	Duration retryWait;

	public BagitTarget(Config config) throws ConfigException {
		this(config.get(PARAM_NAME, config.get("_name", "bagit")), Paths.get(config.get(PARAM_PATH)));
		config.setDefault(PARAM_CHECK_INTERVAL, "60s");
		config.setDefault(PARAM_CHECK_RETRY, "60");
		setRetry(config.getInt(PARAM_CHECK_RETRY), config.getDuration(PARAM_CHECK_INTERVAL));
	}

	private void setRetry(int retryMax, Duration wait) {
		this.retryMax = retryMax;
		retryWait = wait;
	}

	public BagitTarget(String name, Path path) {
		this.name = Objects.requireNonNull(name, "Name must not be null");
		this.path = Objects.requireNonNull(path, "Path must not be null");
		tmpPath = path.resolve(".tmp");
	}

	@Override
	public void onStartup(RuntimeContext ctx) throws Exception {
		if (!Files.isDirectory(path))
			Files.createDirectories(path);
		if (!Files.isDirectory(tmpPath))
			Files.createDirectories(tmpPath);

		pool = ctx.lookupRequired(PoolService.class).getIOPool(path);
		cron = ctx.lookupRequired(CronService.class);
	}

	@Override
	public CompletableFuture<ExportContext> startExport(ArchiveHandle archive) {
		return CompletableFuture.supplyAsync(() -> new BagitExportContext(this, archive), pool);
	}

	@Override
	public CompletableFuture<ImportContext> startImport(ArchiveReference ref, LTSLocation location) {
		return CompletableFuture.supplyAsync(() -> new BagitImportContext(this, ref, location), pool);
	}

	@Override
	public CompletableFuture<Void> markObsolete(ArchiveReference ref, LTSLocation location) {
		final CompletableFuture<Void> cf = new CompletableFuture<>();
		var deleteMarker = getPathFor(ref, location, BagitTarget.DELETE_SUFFIX);
		getPool().submit(() -> {
			try {
				Files.createFile(deleteMarker);
				cf.complete(null);
			} catch (final FileAlreadyExistsException e) {
				cf.complete(null);
			} catch (final Exception e) {
				cf.completeExceptionally(e);
			}
		});
		return cf;
	}

	@Override
	public String getName() {
		return name;
	}

	ExecutorService getPool() {
		return pool;
	}

	CronService getCron() {
		return cron;
	}

	Path createTempPath(String locationHint) throws IOException {
		return Files.createTempDirectory(tmpPath, locationHint);
	}

	Path getPathFor(ArchiveReference ref, LTSLocation location, String suffix) {
		return getPathFor(ref.getVault(), ref.getId(), location.getLocation(), suffix);
	}

	Path getPathFor(String vault, String archive, String uid, String suffix) {
		Objects.requireNonNull(vault);
		Objects.requireNonNull(archive);
		Objects.requireNonNull(uid);
		Objects.requireNonNull(suffix);
		return path.resolve(vault).resolve(archive + "-" + uid + "." + suffix);
	}

}
