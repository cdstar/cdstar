package de.gwdg.cdstar.runtime.lts;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import java.util.concurrent.ExecutorService;

import de.gwdg.cdstar.Utils;

/**
 * A helper class to implement non-blocking automata with an unlimited number of
 * potentially asynchronous steps. This can be used to implement asynchronous
 * program flows with loops or other constructs that are hard to implement with
 * {@link CompletableFuture} alone.
 *
 * The idea is that a program is split into any number of runnable steps. Each
 * step operates on a shared state, may have side-effects, and eventually
 * decides which step to execute next. This decision can be delayed, so a step
 * can await an asynchronous event before deciding how the program should
 * proceed, without blocking a thread the whole time.
 *
 * Thread safety: Steps are executed in a thread-pool and allowed to block, but
 * not run in parallel, so no synchronization should be necessary. A strong
 * happens-before relationship is guaranteed between steps as defined by the
 * ExecutionService API.
 *
 * The state machine is initialized with a shared state and the first step to
 * execute. Steps are run one after the other until a step completes with 'null'
 * (successful termination) or throws an exception.
 *
 * TODO: This is not actually a state machine, so find a better name?
 */
class StateMachine<StateType> {

	// Runtime state
	private CompletableFuture<StateType> parentFuture;
	private final ExecutorService pool;

	private final StateType state;
	private final Step<StateType> step;
	private CompletableFuture<Step<StateType>> runningStep;

	/**
	 * A single execution step.
	 */
	@FunctionalInterface
	interface Step<StateType> {
		/**
		 * Execute a single step of the state machine, optionally mutating the
		 * state and deciding for the next step to execute.
		 *
		 * Completing the returned {@link CompletionException} with null signals
		 * a successful termination. Throwing an exception or completing the
		 * future exceptionally will signal a failure. Otherwise, the result of
		 * the future is used to determine the next step to execute.
		 *
		 * Thread-safety: Steps are executed one after another with proper
		 * synchronization between them (happens-before semantics guaranteed by
		 * the {@link ExecutorService} API). A step 'owns' the state as long as
		 * the {@link CompletableFuture} is not completed.
		 *
		 * Steps are executed in a thread pool, so simple steps can be computed
		 * in a blocking fashion and then return an already completed
		 * {@link CompletableFuture} pointing to the next step. More complex
		 * steps can return a future that is completed asynchronously,
		 * optionally chaining multiple asynchronous sub-steps or even wrapping
		 * a nested {@link StateMachine}.
		 *
		 * @param Shared state
		 * @return Next step, or null to signal termination.
		 */
		CompletableFuture<Step<StateType>> run(StateType state) throws Exception;
	}

	StateMachine(Step<StateType> startStep, StateType startState, ExecutorService pool) {
		step = startStep;
		state = startState;
		this.pool = pool;
	}

	public synchronized CompletableFuture<StateType> start() {
		if (parentFuture != null)
			throw new IllegalStateException("Already started");

		parentFuture = new CompletableFuture<>();

		// Cancel current step if parent future is canceled.
		parentFuture.whenComplete((v, err) -> {
			synchronized (this) {
				if (err != null && runningStep != null && !runningStep.isDone())
					runningStep.completeExceptionally(err);
			}
		});

		pool.execute(() -> progress(this.step));

		return parentFuture;
	}

	public synchronized CompletableFuture<StateType> getFuture() {
		if (parentFuture == null)
			return start();
		return parentFuture;
	}

	/**
	 * Execute a step in a separate thread and once the returned future is done,
	 * schedule the next step for execution. On any errors, fail the entire
	 * task. The terminating step may return a null-value success.
	 */
	private synchronized void progress(Step<StateType> next) {
		if (runningStep != null && !runningStep.isDone())
			throw Utils.wtf();

		// Parent failed or (more likely) was canceled. Do not continue.
		if (parentFuture.isDone())
			return;

		try {
			runningStep = next.run(state);
			if (runningStep == null)
				runningStep = CompletableFuture.completedFuture(null);
		} catch (final Exception e) {
			runningStep = new CompletableFuture<>();
			runningStep.completeExceptionally(e);
		}

		runningStep.whenComplete((nextStep, err) -> {
			if (err != null) {
				// Terminate (error or canceled)
				parentFuture.completeExceptionally(err);
			} else if (nextStep == null) {
				// Terminate (success)
				parentFuture.complete(null);
			} else {
				// Schedule next step
				pool.execute(() -> progress(nextStep));
			}
		});
	}

	/**
	 * Try to cancel execution. The currently running step is signaled to stop
	 * by canceling the {@link CompletableFuture} it returned.
	 */
	public boolean cancel() {
		return parentFuture.cancel(false);
	}

}