package de.gwdg.cdstar.runtime.client;

import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.pool.MetaStore;
import de.gwdg.cdstar.runtime.client.auth.ArchivePermission;
import de.gwdg.cdstar.runtime.client.auth.StringSubject;

public class AclImpl implements CDStarACL {

	private final ArchiveImpl archive;
	private final Map<StringSubject, EnumSet<ArchivePermission>> originalAcl;
	private Map<StringSubject, AclEntryImpl> wrappedEntities;

	AclImpl(ArchiveImpl archive, Map<StringSubject, EnumSet<ArchivePermission>> acl) {
		this.archive = archive;
		originalAcl = acl;
	}

	Map<StringSubject, EnumSet<ArchivePermission>> getAclMap() {
		return originalAcl;
	}

	void saveChanges(MetaStore sto) {
		if (wrappedEntities == null)
			return;
		for (final Entry<StringSubject, AclEntryImpl> entry : wrappedEntities.entrySet()) {
			final StringSubject subject = entry.getKey();
			final EnumSet<ArchivePermission> permissions = entry.getValue().permissions;
			if (!Utils.equal(permissions, originalAcl.get(subject))) {
				PropertyHelper.setAclEntry(sto, subject, permissions, true);
			}
		}
	}

	@Override
	public synchronized CDStarACLEntry forSubject(StringSubject subject) {
		if (wrappedEntities == null)
			wrappedEntities = new HashMap<>();
		return wrappedEntities.computeIfAbsent(subject, s -> {
			return new AclEntryImpl(archive, s, originalAcl.get(s));
		});
	}

	@Override
	public synchronized Collection<CDStarACLEntry> getAccessList() {
		if (wrappedEntities == null || !wrappedEntities.keySet().containsAll(originalAcl.keySet())) {
			originalAcl.keySet().forEach(this::forSubject);
		}
		return Collections.unmodifiableCollection(wrappedEntities.values());
	}

}
