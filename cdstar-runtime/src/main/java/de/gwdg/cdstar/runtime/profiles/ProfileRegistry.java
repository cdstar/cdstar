package de.gwdg.cdstar.runtime.profiles;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.gwdg.cdstar.runtime.Config;
import de.gwdg.cdstar.runtime.RuntimeContext;
import de.gwdg.cdstar.runtime.client.CDStarProfile;
import de.gwdg.cdstar.runtime.listener.RuntimeListener;

/**
 * Registry to define and configure {@link CDStarProfile}s.
 */
public class ProfileRegistry implements RuntimeListener {
	private static final Logger log = LoggerFactory.getLogger(ProfileRegistry.class);

	private static final Pattern profileNamePattern = Pattern.compile("[a-zA-Z][a-zA-Z-_0-9]{0,64}");

	private final Map<String/* vault */, Map<String/* profile */, CDStarProfile>> profiles = new ConcurrentHashMap<>();

	private final CDStarProfile defaultProfile;

	// An invalid vault name to store global profiles
	public static final String GLOBAL_VAULT = "";

	public ProfileRegistry() {
		defaultProfile = defineProfile(GLOBAL_VAULT, CDStarProfile.DEFAULT_NAME);
	}

	@Override
	public void onInit(RuntimeContext ctx) throws Exception {
		for (final Entry<String, Config> row : ctx.getConfig().getTable("profile").entrySet()) {
			final Config cfg = row.getValue();
			final String name = cfg.get("name", row.getKey());

			final Map<String, String> properties = cfg.toMap();
			properties.remove("name");

			if (cfg.hasKey("vaults")) {
				properties.remove("vaults");
				for (final String vault : cfg.getArray("vaults"))
					defineProfile(vault, name, properties);
			} else {
				defineProfile(null, name, properties);
			}
		}

		// TODO: support persisted runtime profiles
	}

	public CDStarProfile defineProfile(String vault, String name) {
		return defineProfile(vault, name, Collections.emptyMap());
	}

	public CDStarProfile defineProfile(String vault, String name, Map<String, String> config) {
		Objects.requireNonNull(name);
		if (vault == null)
			vault = GLOBAL_VAULT;

		if (!profileNamePattern.matcher(name).matches())
			throw new IllegalArgumentException("Invalid profile name: " + name);

		return profiles.computeIfAbsent(vault, v -> new ConcurrentHashMap<>())
			.computeIfAbsent(name, key -> {
				log.debug("Added profile: {} {}", name, config);
				return new ProfileImpl(name, config);
			});
	}

	/**
	 * Return the profile as configured for this vault, or globally.
	 */
	public Optional<CDStarProfile> getProfileByName(String vault, String name) {
		CDStarProfile found = null;

		if (vault != null && !vault.equals(GLOBAL_VAULT)) {
			final Map<String, CDStarProfile> vaultMap = profiles.get(vault);
			if (vaultMap != null)
				found = vaultMap.get(name);
		}

		if (found == null)
			found = profiles.get(GLOBAL_VAULT).get(name);

		return Optional.ofNullable(found);
	}

	public List<CDStarProfile> getProfilesByVault(String vault, boolean includeGlobal) {
		Objects.requireNonNull(vault);
		final List<CDStarProfile> result = new ArrayList<>();
		result.addAll(profiles.getOrDefault(vault, Collections.emptyMap()).values());
		if (includeGlobal && !vault.equals(GLOBAL_VAULT))
			result.addAll(profiles.get(GLOBAL_VAULT).values());
		return result;
	}

	public CDStarProfile getDefaultProfile() {
		return defaultProfile;
	}

}
