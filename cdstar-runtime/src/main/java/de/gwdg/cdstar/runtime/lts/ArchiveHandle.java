package de.gwdg.cdstar.runtime.lts;

import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import de.gwdg.cdstar.runtime.client.CDStarArchive;

/**
 * Read-only representation of an {@link CDStarArchive}
 */
public interface ArchiveHandle extends ArchiveReference {
	/**
	 * List files in this archive. Not available during {@link LTSTarget#markObsolete(ArchiveHandle, LTSLocation)}.
	 */
	Stream<FileHandle> listFiles();

	/**
	 * Return archive metadata. Not available during {@link LTSTarget#markObsolete(ArchiveHandle, LTSLocation)}.
	 */
	Map<String, List<String>> getMetaAttributes();
}