package de.gwdg.cdstar.runtime.services;

import com.codahale.metrics.MetricRegistry;

import de.gwdg.cdstar.runtime.RuntimeContext;
import de.gwdg.cdstar.runtime.listener.RuntimeListener;

public class MetricsFeature implements RuntimeListener {

	@Override
	public void onInit(RuntimeContext ctx) throws Exception {
		ctx.register(new MetricRegistry());
	}

}
