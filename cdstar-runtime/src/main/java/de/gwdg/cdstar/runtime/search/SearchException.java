package de.gwdg.cdstar.runtime.search;

public class SearchException extends Exception {
	private static final long serialVersionUID = 1532258076837139035L;

	public SearchException(String string) {
		super(string);
	}

	public SearchException(String string, Throwable cause) {
		super(string, cause);
	}

}
