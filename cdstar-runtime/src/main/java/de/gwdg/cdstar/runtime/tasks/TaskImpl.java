package de.gwdg.cdstar.runtime.tasks;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;

import de.gwdg.cdstar.ta.UserTransaction;

class TaskImpl implements PreparedTask {
	private final String id;
	private final String name;
	private final Map<String, String> params;
	private volatile boolean canceled;
	private CompletableFuture<Void> future;
	private volatile boolean interrupted;

	protected int errorCount = 0;
	private final TaskServiceImpl owner;

	boolean submitted = false;
	boolean bound = false;

	TaskImpl(TaskServiceImpl owner, String id, String name, Map<String, String> params) {
		this.owner = owner;
		this.id = id;
		this.name = name;
		this.params = params == null ? Collections.emptyMap() : Collections.unmodifiableMap(new HashMap<>(params));
	}

	@Override
	public int hashCode() {
		return id.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		return obj != null && obj instanceof TaskImpl && ((TaskImpl) obj).id.equals(id);
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public Map<String, String> getParameterMap() {
		return params;
	}

	@Override
	public Optional<String> getOptional(String key) {
		return Optional.ofNullable(params.get(key));
	}

	@Override
	public synchronized void cancel() {
		canceled = true;
		owner.rollbackTask(this);
		if (future != null)
			future.cancel(false);
	}

	@Override
	public boolean isCanceled() {
		return canceled;
	}

	synchronized void interrupt() {
		interrupted = true;
		if (future != null)
			future.completeExceptionally(new InterruptedException());
	}

	@Override
	public boolean isInterrupted() {
		return interrupted;
	}

	@Override
	public boolean isDone() {
		return isCanceled() || isInterrupted() || (future != null && future.isDone());
	}

	/**
	 * Bind a future to this task. If the task was canceled or interrupted,
	 * immediately cancel or interrupt the future.
	 */
	synchronized void setFuture(CompletableFuture<Void> f) {
		if (future != null && !future.isDone())
			throw new IllegalStateException("Task already associated with a non-completed future");
		future = f;
		if (canceled)
			cancel();
		if (interrupted)
			interrupt();
	}

	@Override
	public String toString() {
		return new StringBuilder()
			.append("Task(id=")
			.append(id)
			.append(" name=")
			.append(name)
			.append(")")
			.toString();
	}

	@Override
	public synchronized Task submit() {
		if (submitted || bound || canceled || interrupted)
			throw new IllegalStateException("Task already submitted, bound or canceled.");
		owner.submitTask(this);
		submitted = true;
		return this;
	}

	@Override
	public synchronized Task bind(UserTransaction ta) {
		if (submitted || bound || canceled || interrupted)
			throw new IllegalStateException("Task already submitted, bound or canceled.");
		owner.bindTask(this, ta);
		bound = true;
		return this;
	}

}
