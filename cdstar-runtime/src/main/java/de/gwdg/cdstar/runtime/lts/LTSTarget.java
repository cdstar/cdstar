package de.gwdg.cdstar.runtime.lts;

import java.io.Closeable;
import java.io.InputStream;
import java.util.concurrent.CompletableFuture;

/**
 * An interface for LTS import/export operations with support for very long
 * running operations.
 *
 * This interface makes heavy use of {@link CompletableFuture} to handle
 * potentially long-running operations. These might be canceled by the runtime
 * if an operation is no longer required and should be aborted, or if the
 * runtime is shutting down. Implementations should avoid unnecessary work in
 * such cases.
 *
 * Import and export operation may be split across multiple threads of a
 * thread-pool and must be thread-safe.
 *
 * As a general rule, failing imports or exports are re-tried a couple of times
 * and then aborted. During server shutdown, all running operations are
 * canceled, but not waited for. Also, if a crash happens, running imports or
 * exports may be lost and not tried again until the archive is accessed again.
 * This may leave old copies in a mirror, but will NEVER cause an archive to be
 * not mirrored and not available at the same time. This is considered
 * acceptable, and simplifies the implementation of mirror adapters
 * significantly. Full transaction guarantees are not required.
 *
 */
public interface LTSTarget {

	/**
	 * An import context represents a single import operation stated via
	 * {@link LTSTarget#startImport(ArchiveHandle, LTSLocation)}.
	 */
	interface ImportContext extends Closeable {
		/**
		 * Import a single file, provide a readable {@link InputStream}.
		 *
		 * The returned stream should be readable with reasonable speed and not
		 * block for a long time. For example, if the data must be recovered from
		 * tape to a disk buffer, this should happen before the stream is returned
		 * and the stream should read from the cache directly.
		 *
		 * The returned {@link CompletableFuture} may be be canceled if the import
		 * is no longer needed.
		 */
		CompletableFuture<InputStream> importFile(FileHandle handle);

		@Override
		void close();
	}

	interface ExportContext extends Closeable {
		/**
		 * Export a single file from the archive.
		 *
		 * The returned {@link CompletableFuture} may be canceled if the export
		 * should be stopped.
		 */
		CompletableFuture<Void> exportFile(FileHandle handle, InputStream stream);

		/**
		 * Finish the export. Return a mirror location. The mirror operation
		 * MUST be completed and committed in the remote system in a safe way
		 * before this future is resolved. The returned {@link LTSLocation}
		 * MUST allow a future import from the same mirror adapter.
		 */
		CompletableFuture<LTSLocation> complete();

		/**
		 * Close the context and remove any temporary files or resources. If
		 * this is called before {@link #complete()}, then any remotely created
		 * resources should also be removed.
		 */
		@Override
		void close();
	}

	/**
	 * Return a (unique) name for this adapter.
	 */
	String getName();

	/**
	 * Return true if this handler can import a given {@link LTSLocation}. Do not
	 * check if the location is actually resolvable, only if it is compatible with
	 * this {@link LTSTarget}. The default implementation returns true if
	 * {@link LTSLocation#getMirrorName()} equals {@link #getName()}.
	 */
	default boolean canImport(LTSLocation location) {
		return location.getMirrorName().equals(getName());
	}

	/**
	 * Prepare an export. The LTS implementation should make sure that the target
	 * system is available and can accept the export before completing the future.
	 */
	CompletableFuture<ExportContext> startExport(ArchiveHandle archive);

	/**
	 * Prepare an import. Make sure the given {@link LTSLocation} exists and is
	 * readable before completing the future.
	 */
	CompletableFuture<ImportContext> startImport(ArchiveReference archive, LTSLocation location);

	/**
	 * Mark the given {@link LTSLocation} as obsolete. This happens after an archive
	 * was successfully moved to a different {@link LTSTarget}, or deleted from the
	 * vault.
	 */
	CompletableFuture<Void> markObsolete(ArchiveReference archive, LTSLocation location);

}
