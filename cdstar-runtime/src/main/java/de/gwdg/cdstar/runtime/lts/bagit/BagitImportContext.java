package de.gwdg.cdstar.runtime.lts.bagit;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.runtime.lts.ArchiveReference;
import de.gwdg.cdstar.runtime.lts.FileHandle;
import de.gwdg.cdstar.runtime.lts.LTSLocation;
import de.gwdg.cdstar.runtime.lts.LTSTarget.ImportContext;
import de.gwdg.cdstar.runtime.lts.MigrationFailedException;

public class BagitImportContext implements ImportContext {

	private final BagitTarget adapter;
	private final Path cachePath;
	private final Path wantPath;
	private volatile int retryCounter = 0;

	public BagitImportContext(BagitTarget target, ArchiveReference ref, LTSLocation location) {
		adapter = target;
		cachePath = target.getPathFor(ref, location, BagitTarget.BAGIT_SUFFIX);
		wantPath = target.getPathFor(ref, location, BagitTarget.WANTS_SUFFIX);

		try {
			Files.createDirectories(wantPath.getParent());
			Files.createFile(wantPath);
		} catch (final FileAlreadyExistsException e) {
			throw new MigrationFailedException("Migration already running", e);
		} catch (final IOException e) {
			throw new MigrationFailedException(e);
		}
	}

	/**
	 * Try to open a file. If not present, wait for
	 * {@link BagitTarget#retryWait} and retry. Stop trying after
	 * {@link BagitTarget#retryMax} events, or if the want-file disappears.
	 *
	 * @param handle
	 * @param cf
	 */
	void tryOpen(FileHandle handle, CompletableFuture<InputStream> cf) {
		final Path src = cachePath.resolve("data").resolve(handle.getName());

		try {
			cf.complete(Files.newInputStream(src)); // READ
		} catch (final IOException e) {
			if (cf.isDone())
				return;

			if (!Files.exists(wantPath)) {
				cf.completeExceptionally(
					new MigrationFailedException("Externally aborted (want file missing): " + wantPath, e));
				return;
			}

			if (Files.exists(cachePath)) {
				final String err = Files.exists(src)
					? "Failed to open file: " + src
					: "File missing: " + src;
				cf.completeExceptionally(new MigrationFailedException(err, e));
				return;
			}

			if (++retryCounter > adapter.retryMax) {
				cf.completeExceptionally(
					new MigrationFailedException("Timeout while waiting for: " + wantPath, e));
				return;
			}

			BagitTarget.log.info("Waiting for file {} to appear (attempt: {}/{} interval: {}ms)",
				src, retryCounter, adapter.retryMax, adapter.retryWait.toMillis());

			// Re-schedule again after a certain delay.
			adapter.getCron().schedule(() -> {
				adapter.getPool().submit(() -> tryOpen(handle, cf));
			}, adapter.retryWait.toMillis(), TimeUnit.MILLISECONDS);
		}
	}

	@Override
	public CompletableFuture<InputStream> importFile(FileHandle file) {
		final CompletableFuture<InputStream> cf = new CompletableFuture<>();
		retryCounter = 0;
		adapter.getPool().submit(() -> tryOpen(file, cf));
		return cf;
	}

	@Override
	public void close() {
		Utils.deleteQuietly(wantPath);
	}

}
