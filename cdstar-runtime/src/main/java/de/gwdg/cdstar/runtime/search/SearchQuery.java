package de.gwdg.cdstar.runtime.search;

import java.util.List;
import java.util.Set;

public interface SearchQuery {

	String getVault();

	String getPrincipal();

	Set<String> getGroups();

	String getQuery();

	List<String> getOrder();

	int getLimit();

	String getScrollId();

	List<String> getFields();

	static SearchQueryBuilder builder() {
		return new SearchQueryBuilder();
	}
}
