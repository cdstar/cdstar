package de.gwdg.cdstar.runtime.client;

import java.util.Collection;
import java.util.Date;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.pool.MetaStore;
import de.gwdg.cdstar.pool.StorageObject;
import de.gwdg.cdstar.runtime.client.auth.ArchivePermission;
import de.gwdg.cdstar.runtime.client.auth.ArchivePermissionSet;
import de.gwdg.cdstar.runtime.client.auth.StringSubject;
import de.gwdg.cdstar.runtime.client.auth.StringSubject.PrincipalSubject;

public class PropertyHelper {


	private PropertyHelper() {
	}


	public static Map<StringSubject, EnumSet<ArchivePermission>> getAclMap(MetaStore src) {
		final HashMap<StringSubject, EnumSet<ArchivePermission>> map = new HashMap<>();
		for (final String prop : src.getPropertyNames()) {
			if (prop.startsWith(ArchiveImpl.PROP_ACL_NS)) {
				final String subjName = prop.substring(ArchiveImpl.PROP_ACL_NS.length());
				map.put(StringSubject.fromString(subjName), parseEntry(src.getProperty(prop)));
			}
		}
		return map;
	}

	public static EnumSet<ArchivePermission> getAclEntry(MetaStore src, StringSubject subj) {
		final String propName = ArchiveImpl.PROP_ACL_NS + subj.toString();
		return parseEntry(src.getProperty(propName));
	}

	static EnumSet<ArchivePermission> parseEntry(String value) {
		final EnumSet<ArchivePermission> r = EnumSet.noneOf(ArchivePermission.class);
		for (final String item : Utils.split(value, ',')) {
			try {
				if (Character.isUpperCase(item.charAt(0))) {
					r.addAll(ArchivePermissionSet.valueOf(item).getPermissions());
				} else {
					r.add(ArchivePermission.valueOf(item.toUpperCase()));
				}
			} catch (final IllegalArgumentException e) {
				throw new RuntimeException("Unknown permission or grant: " + item);
			}
		}
		return r;
	}

	static String entryToString(Collection<ArchivePermission> grants, boolean compress) {
		final StringBuilder sb = new StringBuilder();
		final EnumSet<ArchivePermission> pset = EnumSet.copyOf(grants);
		if (compress) {
			for (final ArchivePermissionSet set : ArchivePermissionSet.group(pset)) {
				sb.append(set.name().toUpperCase()).append(",");
				pset.removeAll(set.getPermissions());
			}
		}
		for (final ArchivePermission p : pset)
			sb.append(p.name().toLowerCase()).append(",");
		sb.setLength(sb.length() - 1);
		return sb.toString();
	}

	public static void setAclEntry(MetaStore src, StringSubject subject, Collection<ArchivePermission> grants,
		boolean compress) {
		if (grants == null || grants.isEmpty()) {
			src.setProperty(ArchiveImpl.PROP_ACL_NS + subject.toString(), null);
		} else {
			src.setProperty(ArchiveImpl.PROP_ACL_NS + subject.toString(), entryToString(grants, compress));
		}
	}

	public static PrincipalSubject getOwner(MetaStore sto) {
		return new StringSubject.PrincipalSubject(sto.getProperty(ArchiveImpl.PROP_OWNER));
	}

	public static void setOwner(MetaStore sto, StringSubject.PrincipalSubject owner) {
		sto.setProperty(ArchiveImpl.PROP_OWNER, owner.toString());
	}

	public static Date getMtime(MetaStore sto) {
		return Utils.fromIsoDate(sto.getProperty(ArchiveImpl.PROP_CONTENT_MODIFIED));
	}

	public static void setMTime(MetaStore sto, Date now) {
		sto.setProperty(ArchiveImpl.PROP_CONTENT_MODIFIED, Utils.toIsoDate(now));
	}

	public static int getRevision(MetaStore sto) {
		return Integer.parseInt(sto.getProperty(ArchiveImpl.PROP_REV));
	}

	public static void setRevision(MetaStore sto, int rev) {
		sto.setProperty(ArchiveImpl.PROP_REV, Integer.toString(rev));
	}

	public static void setProfile(StorageObject sto, CDStarProfile profile) {
		sto.setProperty(ArchiveImpl.PROP_PROFILE, profile.getName());
	}

	public static String getProfile(StorageObject sto) {
		return sto.getProperty(ArchiveImpl.PROP_PROFILE);
	}

	public static void setLastModifiedBy(StorageObject sto, StringSubject.PrincipalSubject sub) {
		sto.setProperty(ArchiveImpl.PROP_BY, sub.toString());
	}

}
