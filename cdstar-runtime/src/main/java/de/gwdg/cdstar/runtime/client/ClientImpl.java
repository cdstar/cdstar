package de.gwdg.cdstar.runtime.client;

import de.gwdg.cdstar.auth.StringPermission;
import de.gwdg.cdstar.auth.Subject;
import de.gwdg.cdstar.runtime.CDStarRuntime;
import de.gwdg.cdstar.runtime.RuntimeContext;
import de.gwdg.cdstar.runtime.client.auth.ServicePermission;
import de.gwdg.cdstar.runtime.client.exc.AccessError;
import de.gwdg.cdstar.ta.TransactionInfo.Mode;

public class ClientImpl implements CDStarClient {

	final CDStarRuntime runtime;
	private final Subject subject;

	public ClientImpl(CDStarRuntime runtime, Subject subject) {
		this.runtime = runtime;
		this.subject = subject;
	}

	@Override
	public CDStarSession begin(boolean readOnly, Mode mode) {
		return new SessionImpl(this, readOnly, mode);
	}

	@Override
	public RuntimeContext getRuntime() {
		return runtime;
	}

	@Override
	public Subject getSubject() {
		return subject;
	}

	boolean isPermitted(StringPermission perm) {
		return subject.isPermitted(perm);
	}

	void checkPermission(StringPermission perm) {
		if (!isPermitted(perm))
			throw new AccessError(perm);
	}

	void checkPermission(ServicePermission permission) {
		checkPermission(StringPermission.ofParts("service", permission.name().toLowerCase()));
	}

}
