package de.gwdg.cdstar.runtime.tasks;

public interface TaskListener {

	void onTaskSubmitted(Task task);

	void onTaskFinished(Task task);

	void onTaskFailed(Task task, Throwable err, boolean willRetry);

}
