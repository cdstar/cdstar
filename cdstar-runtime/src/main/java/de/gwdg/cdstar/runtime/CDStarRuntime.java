package de.gwdg.cdstar.runtime;

import java.io.IOException;
import java.nio.file.AccessDeniedException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.ServiceLoader;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.gwdg.cdstar.AtomicEnum;
import de.gwdg.cdstar.PidFileLock;
import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.auth.AuthConfig;
import de.gwdg.cdstar.auth.AuthConfigImpl;
import de.gwdg.cdstar.auth.KnownPrincipalCredentials;
import de.gwdg.cdstar.auth.Subject;
import de.gwdg.cdstar.auth.realm.Realm;
import de.gwdg.cdstar.auth.simple.Account;
import de.gwdg.cdstar.auth.simple.SimpleAuthorizer;
import de.gwdg.cdstar.config.LoggingConfig;
import de.gwdg.cdstar.config.MapConfig;
import de.gwdg.cdstar.config.WildcardConfig;
import de.gwdg.cdstar.runtime.client.CDStarClient;
import de.gwdg.cdstar.runtime.client.CDStarSession;
import de.gwdg.cdstar.runtime.client.ClientImpl;
import de.gwdg.cdstar.runtime.filter.DefaultPermissionsPlugin;
import de.gwdg.cdstar.runtime.listener.ArchiveListener;
import de.gwdg.cdstar.runtime.listener.RuntimeListener;
import de.gwdg.cdstar.runtime.listener.SessionListener;
import de.gwdg.cdstar.runtime.listener.SessionStartListener;
import de.gwdg.cdstar.runtime.listener.VaultListener;
import de.gwdg.cdstar.runtime.lts.LTSMigrationService;
import de.gwdg.cdstar.runtime.profiles.ProfileRegistry;
import de.gwdg.cdstar.runtime.services.AutoAdminUser;
import de.gwdg.cdstar.runtime.services.CronFeature;
import de.gwdg.cdstar.runtime.services.DefaultMetricsAndHealthChecks;
import de.gwdg.cdstar.runtime.services.FeatureRegistry;
import de.gwdg.cdstar.runtime.services.MetricsFeature;
import de.gwdg.cdstar.runtime.services.PluginLoader;
import de.gwdg.cdstar.runtime.services.PoolServiceImpl;
import de.gwdg.cdstar.runtime.services.ServiceRegistry;
import de.gwdg.cdstar.runtime.services.SessionRegistryImpl;
import de.gwdg.cdstar.runtime.services.VaultRegistry;
import de.gwdg.cdstar.runtime.services.health.HealthMonitorFeature;
import de.gwdg.cdstar.runtime.tasks.TaskServiceImpl;
import de.gwdg.cdstar.ta.TransactionInfo.Mode;
import de.gwdg.cdstar.ta.TransactionManager;
import de.gwdg.cdstar.ta.UserTransaction;
import de.gwdg.cdstar.tm.DiskTransactionManager;

/**
 * Chain of ownership: Runtime, Client, Session, Vault, Archive, File
 *
 */
public class CDStarRuntime implements RuntimeContext {
	public static Logger log = LoggerFactory.getLogger(CDStarRuntime.class);

	public enum State {
		NEW, INITIALIZED, STARTING, STARTED, CLOSING, CLOSED;
	}

	private final AtomicEnum<State> state = AtomicEnum.build(State.NEW)
		.allowChain(State.NEW, State.INITIALIZED, State.STARTING, State.STARTED, State.CLOSING, State.CLOSED)
		.allow(State.NEW, State.CLOSED)
		.allow(State.INITIALIZED, State.CLOSED)
		.allow(State.STARTING, State.CLOSING)
		.build();

	private final PluginLoader pluginLoader;
	private final TransactionManager transactionService;
	private final VaultRegistry vaultRegistry;
	private final ServiceRegistry services;
	private final AuthConfigImpl authConfig;
	private final SessionRegistryImpl sessionRegistry;
	private final ProfileRegistry profileRegistry;
	private final SimpleAuthorizer systemRealm;

	private Config config;
	private final Path dataPath;
	private final Path homePath;
	private final Path varPath;
	private final List<Path> libPaths;

	private final CountDownLatch shutdownLatch = new CountDownLatch(1);
	private final PidFileLock pidfile;
	private Thread shutdownHook;
	private final Set<RuntimeListener> startedRuntimeListeners = ConcurrentHashMap.newKeySet();

	private final class KnownServiceListener implements ServiceRegistry.ServiceListener {
		@Override
		public void serviceAdded(ServiceRegistry reg, Object service) throws Exception {
			if (service instanceof ArchiveListener && !(service instanceof VaultListener))
				register(VaultListener.wrap((ArchiveListener) service));

			if (service instanceof VaultListener && !(service instanceof SessionListener))
				register(SessionListener.wrap((VaultListener) service));

			if (service instanceof SessionListener && !(service instanceof SessionStartListener))
				register(SessionStartListener.wrap((SessionListener) service));

			if (service instanceof Realm)
				authConfig.addRealm((Realm) service);

			if (service instanceof RuntimeListener)
				((RuntimeListener) service).onInit(CDStarRuntime.this);
		}
	}

	public static CDStarRuntime bootstrap(Config cfg) throws ConfigException {
		final CDStarRuntime runtime = new CDStarRuntime(cfg);
		runtime.bootstrap();
		return runtime;
	}

	/**
	 * Create a new runtime from a {@link Config} object. The runtime is not
	 * initialized or started. Bootstrapping happens
	 */
	private CDStarRuntime(Config cfg) throws ConfigException {

		// Make a private copy
		config = new MapConfig(cfg);
		// Set default values for core configuration parameters
		config.setDefault("path.home", "${CDSTAR_HOME:/var/lib/cdstar}");
		config.setDefault("path.data", "${path.home}/data");
		config.setDefault("path.lib", "${path.home}/lib");
		config.setDefault("path.var", "${path.home}/var");
		config.setDefault("http.host", "${CDSTAR_HOST:127.0.0.1}");
		config.setDefault("http.port", "${CDSTAR_PORT:8080}");
		// config.setDefault("pidfile", "${path.var}/cdstar.pid"); // Undocumented
		config = new WildcardConfig(new LoggingConfig(config));

		// Resolve paths (all paths are relative to path.home, not the current
		// working directory)
		homePath = Paths.get(config.get("path.home")).toAbsolutePath();
		dataPath = homePath.resolve(config.get("path.data")).toAbsolutePath();
		varPath = homePath.resolve(config.get("path.var")).toAbsolutePath();
		libPaths = config.getList("path.lib").stream().map(s -> homePath.resolve(s).toAbsolutePath())
				.collect(Collectors.toList());

		pidfile = config.hasKey("pidfile") ? new PidFileLock(homePath.resolve(config.get("pidfile"))) : null;

		services = new ServiceRegistry();
		authConfig = new AuthConfigImpl();
		sessionRegistry = new SessionRegistryImpl();
		pluginLoader = new PluginLoader(getClass().getClassLoader());
		vaultRegistry = new VaultRegistry(dataPath, pluginLoader.getPluginClassLoader());
		profileRegistry = new ProfileRegistry();
		transactionService = new DiskTransactionManager(getServiceDir("tx"));
		systemRealm = new SimpleAuthorizer("system");
	}

	/**
	 * Discover and trigger {@link Feature} hooks, load plugins and realms.
	 *
	 * This has to happen outside of the constructor so that plugins do not
	 * access a potentially not fully initialized runtime.
	 */
	private synchronized void bootstrap() throws ConfigException {

		log.info("Initializing runtime...");
		log.info(" path.home: {}", homePath);
		log.info(" path.data: {}", dataPath);
		log.info(" path.var:  {}", varPath);
		log.info(" path.lib:  {}", Utils.join(", ", libPaths));
		if (pidfile != null)
			log.info(" pidfile:   {}", pidfile.getPath());

		log.debug("Installing core services...");

		// The order is significant, as this is the order services are started.

		services.addInstance(new KnownServiceListener());
		services.addInstance(this);
		services.addInstance(new FeatureRegistry());
		services.addInstance(systemRealm);
		services.addInstance(pluginLoader);
		services.addInstance(getConfig());
		services.addInstance(authConfig);
		services.addInstance(sessionRegistry);
		services.addInstance(profileRegistry);
		services.addInstance(vaultRegistry);
		services.addInstance(transactionService);
		services.addInstance(new PoolServiceImpl());
		services.addInstance(new CronFeature());
		services.addInstance(new HealthMonitorFeature());
		services.addInstance(new DefaultPermissionsPlugin());
		services.addInstance(new TaskServiceImpl());
		services.addInstance(new LTSMigrationService());
		services.addInstance(new MetricsFeature());
		services.addInstance(new DefaultMetricsAndHealthChecks());
		services.addInstance(new AutoAdminUser());


		// This triggers onInit for all core services.

		log.debug("Scanning plugin class path...");

		for (final Path libPath : libPaths) {
			if (!Files.exists(libPath)) {
				log.info("Skipping library path: {} (not found)", libPath);
				continue;
			}

			try {
				pluginLoader.addPath(libPath);
			} catch (final IOException e) {
				throw new ConfigException("Inaccessible library path: " + libPath, e);
			}
		}

		log.debug("Starting early feature discovery...");

		for (final Feature feature : ServiceLoader.load(Feature.class, pluginLoader.getPluginClassLoader())) {
			feature.configure(this);
		}

		log.debug("Loading configured plugins and realms...");

		for (final Entry<String, Config> row : config.getTable("plugin").entrySet()) {
			final Config pluginConf = row.getValue();
			final String plugnName = row.getKey();
			final String klassName = pluginConf.get("class");
			pluginConf.set("_name", plugnName);
			log.info("Loading plugin [{}] (class={})...", plugnName, klassName);
			services.addInstance(pluginLoader.initPlugin(plugnName, klassName, pluginConf, Object.class));
		}
		for (final Entry<String, Config> row : config.getTable("realm").entrySet()) {
			final Config pluginConf = row.getValue();
			final String plugnName = row.getKey();
			final String klassName = pluginConf.get("class");
			pluginConf.set("_name", plugnName);
			log.info("Loading realm [{}] (class={})...", plugnName, klassName);
			services.addInstance(pluginLoader.initPlugin(plugnName, klassName, pluginConf, Realm.class));
		}

		if (log.isDebugEnabled()) {
			log.debug("List of loaded services:");
			services.lookupAll(Object.class).forEach(s -> {
				log.debug("  {}", Utils.instanceId(s));
			});
		}

		state.set(State.INITIALIZED);
	}

	public void start() throws Exception {
		state.set(State.STARTING);
		log.info("Starting runtime...");

		try {
			shutdownHook = new Thread(this::close, "cdstar-shutdown");
			shutdownHook.setDaemon(false);
			Runtime.getRuntime().addShutdownHook(shutdownHook);

			ensureDirectoryExistsAndIsWriteable(dataPath);
			ensureDirectoryExistsAndIsWriteable(varPath);

			if (pidfile != null) {
				try {
					pidfile.create();
				} catch (final IOException e) {
					if (e instanceof FileAlreadyExistsException) {
						log.error("Pidfile already exists. This may be caused by an unclean shutdown. "
								+ "Make sure no other instance of CDSTAR is running, then remove the pidfile manually: {}",
								pidfile.getPath());
					}
					throw new IOException("Failed to create pidfile: " + pidfile.getPath(), e);
				}
			}

			for (final RuntimeListener rl : lookupAll(RuntimeListener.class))
				ensureStarted(rl);

			state.set(State.STARTED);
			log.info("Runtime started");

		} catch (final Exception e) {
			log.error("Failed to start runtime :(", e);
			close();
			throw e;
		}

		// Warn about unused config keys (typos, bad plugins)
		for (final String key : ((LoggingConfig) ((WildcardConfig) config).getSrc()).getUnusedKeys()) {
			if (key.startsWith("vars."))
				continue;
			log.warn("Config key [{}] not used by any plugin. Is this a typo?", key);
		}
	}

	@Override
	public void close() {
		if (state.is(State.CLOSING, State.CLOSED))
			return;

		if (state.is(State.NEW, State.INITIALIZED)) {
			state.set(State.CLOSED);
			return;
		}

		state.set(State.CLOSING);
		log.info("Starting shutdown...");

		for (final RuntimeListener service : startedRuntimeListeners) {
			try {
				log.debug("Closing runtime listener: {}", service);
				service.onShutdown(this);
			} catch (final Exception e) {
				log.error("Error during service shutdown:", e);
			}
		}
		startedRuntimeListeners.clear();

		if (pidfile != null && pidfile.isLocked()) {
			try {
				pidfile.remove();
			} catch (final IOException e) {
				log.error("Failed to remove pidfile. Delete it manually: {}", pidfile.getPath(), e);
			}
		}

		// Remove shutdown hook if installed, so close() is not called a second
		// time and the cdstar runtime and everything in it can be garbage
		// collected.
		try {
			Runtime.getRuntime().removeShutdownHook(shutdownHook);
		} catch (final IllegalStateException | NullPointerException e) {
			// We are probably already within the shutdown hook.
		}

		state.set(State.CLOSED);
		log.info("Runtime shutdown complete");
		shutdownLatch.countDown();
	}

	/**
	 * Checks if a given path is a writable directory, and tries to create it if
	 * its missing. Existing files or directories are not changed.
	 */
	private void ensureDirectoryExistsAndIsWriteable(Path path) throws IOException {
		if (!Files.exists(path)) {
			try {
				Files.createDirectories(path);
			} catch (final AccessDeniedException e) {
				throw new IOException("Missing permissions to create: " + path);
			}
		} else if (!Files.isDirectory(path)) {
			throw new IOException("Not a directory: " + path);
		} else if (!Files.isWritable(path)) {
			throw new IOException("Missing permissions (write): " + path);
		}
	}

	@Override
	public void register(Object instance) {
		services.addInstance(instance);
	}

	private <T> T ensureStarted(T service) {
		if (service instanceof RuntimeListener && state.is(State.STARTING)) {
			final RuntimeListener rl = (RuntimeListener) service;
			synchronized (service) {
				// Check again if we really need to start this service
				if (state.is(State.STARTING) && startedRuntimeListeners.add(rl)) {
					try {
						rl.onStartup(this);
					} catch (final Exception e) {
						startedRuntimeListeners.remove(rl);
						throw new RuntimeException("RuntimeListener failed to start", e);
					}
				}
			}
		}
		return service;
	}

	@Override
	public <T> Optional<T> lookup(Class<T> lookupClass) {
		state.expect(State.INITIALIZED, State.STARTING, State.STARTED);
		return services.lookup(lookupClass).map(this::ensureStarted);
	}

	@Override
	public <T> List<T> lookupAll(Class<T> lookupClass) {
		state.expect(State.INITIALIZED, State.STARTING, State.STARTED);
		final List<T> srvs = services.lookupAll(lookupClass);
		if (state.is(State.STARTING)) {
			for (final T srv : srvs) {
				ensureStarted(srv);
			}
		}
		return srvs;
	}

	@Override
	public Path getServiceDir(String serviceName) {
		return varPath.resolve(serviceName);
	}

	public UserTransaction createTransaction(boolean readOnly, Mode mode) {
		final UserTransaction tx = getTransactionManager().begin();
		if (readOnly)
			tx.setRollbackOnly();
		if (mode != null)
			tx.setMode(mode);
		return tx;
	}

	public UserTransaction resumeTransaction(String transactionId) {
		return getTransactionManager().getTransaction(transactionId);
	}

	@Override
	public CDStarSession resumeSession(String tx) {
		return ensureStarted(sessionRegistry).get(tx).orElse(null);
	}

	@Override
	public Subject createSubject() {
		return getAuthConfig().getSubject();
	}

	public AuthConfig getAuthConfig() {
		return ensureStarted(authConfig);
	}

	public TransactionManager getTransactionManager() {
		return ensureStarted(transactionService);
	}

	@Override
	public Config getConfig() {
		return config;
	}

	/**
	 * Block until the runtime is shut down.
	 */
	public void join() throws InterruptedException {
		shutdownLatch.await();
	}

	/**
	 * Block until the runtime is shut down or a timeout occurs.
	 *
	 * @return true if the shutdown is complete, false if the time elapsed
	 * before that.
	 */
	public boolean join(long timeout, TimeUnit unit) throws InterruptedException {
		return shutdownLatch.await(timeout, unit);
	}

	@Override
	public CDStarClient getClient(Subject subject) {
		Utils.notNull(subject);
		return new ClientImpl(this, subject);
	}

	/**
	 * Get a system user session. A system user allows internal components
	 * (including plugins) to bypass normal access restrictions, but has no
	 * password and is not accessible through normal credentials-based login. If
	 * the requested user does not exist, a new account is created.
	 */
	@Override
	public Subject getSystemUser(String name) {
		state.expect(State.STARTING, State.STARTED);
		final Account acc = systemRealm.account(name);
		if (acc.getPermissions().isEmpty()) {
			acc.withPermissions("*");
			acc.setProperty("autocreated", "true");
			log.info("Created system user: {}", acc.getFullId());
		}
		final Subject sub = createSubject();
		sub.tryLogin(new KnownPrincipalCredentials(acc));
		return sub;
	}

}
