package de.gwdg.cdstar.runtime.exc;

import java.io.PrintStream;
import java.io.PrintWriter;

/**
 * A multi-exception thrown if one or more filters failed with an exception.
 *
 */
public class FilterError extends BackendException {
	private static final long serialVersionUID = -8192677416738769787L;

	public FilterError(String string, Exception e) {
		super(string);
		addSuppressed(e);
	}

	/**
	 * Gets the message associated with this exception
	 */
	@Override
	public String getMessage() {
		final StringBuilder sb = new StringBuilder(
				"This FilterError has " + getSuppressed().length + " exceptions.  They are:\n");

		int lcv = 1;
		for (final Throwable th : getSuppressed()) {
			sb.append(lcv++ + ". " + th.getClass().getName() + ((th.getMessage() != null) ? ": " + th.getMessage() : "")
					+ "\n");
		}

		return sb.toString();
	}

	@Override
	public void printStackTrace(PrintStream s) {
		if (getSuppressed().length == 0) {
			super.printStackTrace(s);
			return;
		}

		int lcv = 1;
		for (final Throwable th : getSuppressed()) {
			s.println("MultiException stack " + lcv++ + " of " + getSuppressed().length);
			th.printStackTrace(s);
		}
	}

	@Override
	public void printStackTrace(PrintWriter s) {
		if (getSuppressed().length == 0) {
			super.printStackTrace(s);
			return;
		}

		int lcv = 1;
		for (final Throwable th : getSuppressed()) {
			s.println("MultiException stack " + lcv++ + " of " + getSuppressed().length);
			th.printStackTrace(s);
		}
	}

}
