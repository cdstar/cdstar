package de.gwdg.cdstar.runtime.client;

import java.util.Objects;

import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.runtime.client.auth.ArchivePermission;
import de.gwdg.cdstar.runtime.client.exc.ArchiveNotAvailable;
import de.gwdg.cdstar.runtime.lts.LTSConfig;

class MirrorStateImpl implements CDStarMirrorState {

	private final ArchiveImpl archive;
	private final SnapshotImpl snapshot;

	public MirrorStateImpl(ArchiveImpl archive, SnapshotImpl snapshot) {
		this.archive = Objects.requireNonNull(archive);
		this.snapshot = snapshot;
	}

	private String getRawProperty(String name) {
		return snapshot == null ? archive.getRawProperty(name) : snapshot.getRawProperty(name, null);
	}

	private void setRawProperty(String name, String value) {
		if (snapshot == null)
			archive.setRawProperty(name, value);
		else
			snapshot.setRawProperty(name, value);
	}

	@Override
	public boolean isAvailable() {
		// Non-mirrored archives are ALWAYS readable.
		if (!isMirrored())
			return true;

		// Cold profiles are NEVER readable, even if the actual state is.
		if (LTSConfig.isCold(getProfile()))
			return false;

		// Hot profile and mirrored. File SHOULD be available, but profile hot/cold
		// modes can change. Better check actual state.
		return allFilesAavilable();
	}

	private boolean allFilesAavilable() {
		for (FileImpl file : (snapshot == null ? archive.getInternalFileList() : snapshot.getInternalFileList()))
			if (!file.isAvailable())
				return false;

		return true;
	}

	@Override
	public CDStarProfile getProfile() {
		return snapshot == null ? archive.getProfile() : snapshot.getProfile();
	}

	@Override
	public void setProfile(CDStarProfile profile) {
		if (snapshot == null)
			archive.setProfile(profile);
		else
			snapshot.setProfile(profile);
	}

	@Override
	public String getMirrorName() {
		final String mirror = getRawProperty(ArchiveImpl.PROP_LTS_NAME);
		return Utils.nullOrEmpty(mirror) ? null : mirror;
	}

	@Override
	public String getMirrorLocation() {
		if (!isMirrored())
			return null;
		final String loc = getRawProperty(ArchiveImpl.PROP_LTS_HINT);
		return loc == null ? "" : loc;
	}

	@Override
	public boolean isMigrationPending() {
		return getMigrationTaskID() != null;
	}

	String getMigrationTaskID() {
		return getRawProperty(ArchiveImpl.PROP_LTS_TASK);
	}

	void setMigrationTaskId(String id) {
		archive.requireTransactionWriteable();
		archive.markModified();
		setRawProperty(ArchiveImpl.PROP_LTS_TASK, id);
	}

	@Override
	public void setMirror(String mirror, String location) {
		archive.checkPermission(ArchivePermission.CHANGE_MIRROR);
		if (isMirrored())
			throw new IllegalStateException("Mirror already set");
		archive.requireTransactionWriteable();
		archive.markModified();
		Objects.requireNonNull(mirror);
		Objects.requireNonNull(location);
		setRawProperty(ArchiveImpl.PROP_LTS_NAME, mirror);
		setRawProperty(ArchiveImpl.PROP_LTS_HINT, location);
	}

	@Override
	public void clearMirror() {
		archive.checkPermission(ArchivePermission.CHANGE_MIRROR);
		archive.requireTransactionWriteable();
		archive.markModified(); // TODO: Needed?
		if (!allFilesAavilable())
			throw new ArchiveNotAvailable(archive);
		setRawProperty(ArchiveImpl.PROP_LTS_NAME, null);
		setRawProperty(ArchiveImpl.PROP_LTS_HINT, null);
	}

}
