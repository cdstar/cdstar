package de.gwdg.cdstar.runtime.services.health;

public class HealthStatus {
	final HealthLevel health;
	private final String message;
	private final Throwable error;

	private HealthStatus(HealthLevel h, String msg, Throwable err) {
		health = h;
		message = msg;
		error = err;
	}

	public HealthLevel getHealth() {
		return health;
	}

	public String getMessage() {
		return message;
	}

	public Throwable getError() {
		return error;
	}

	@Override
	public String toString() {
		return health.name() + ": " + message;
	}

	private static HealthStatus OK = new HealthStatus(HealthLevel.OK, "OK", null);

	public static HealthStatus ok() {
		return OK;
	}

	public static HealthStatus ok(String msg) {
		return new HealthStatus(HealthLevel.OK, msg, null);
	}

	public static HealthStatus warn(String msg) {
		return new HealthStatus(HealthLevel.WARN, msg, null);
	}

	public static HealthStatus error(String msg) {
		return new HealthStatus(HealthLevel.ERROR, msg, null);
	}

	public static HealthStatus error(String msg, Throwable error) {
		return new HealthStatus(HealthLevel.ERROR, msg, error);
	}

}