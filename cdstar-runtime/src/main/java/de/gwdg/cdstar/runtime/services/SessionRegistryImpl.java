package de.gwdg.cdstar.runtime.services;

import java.util.Optional;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Predicate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codahale.metrics.MetricRegistry;

import de.gwdg.cdstar.runtime.RuntimeContext;
import de.gwdg.cdstar.runtime.client.CDStarSession;
import de.gwdg.cdstar.runtime.listener.RuntimeListener;
import de.gwdg.cdstar.runtime.listener.SessionListener;
import de.gwdg.cdstar.runtime.listener.SessionStartListener;

public class SessionRegistryImpl implements SessionStartListener, SessionListener, RuntimeListener, SessionRegistry {
	private final ConcurrentHashMap<String, CDStarSession> allSessions = new ConcurrentHashMap<>();

	private ScheduledFuture<?> sessionReaper;
	private final AtomicLong totalCounter = new AtomicLong();

	private static final Logger log = LoggerFactory.getLogger(SessionRegistryImpl.class);

	@Override
	public void onStartup(RuntimeContext ctx) throws Exception {
		sessionReaper = ctx.lookupRequired(CronService.class).scheduleWithFixedDelay(this::reapExpiredSessions, 60, 10,
				TimeUnit.SECONDS);

		ctx.lookup(MetricRegistry.class).ifPresent(mr -> {
			mr.gauge("sess.open", () -> () -> allSessions.size());
			mr.gauge("sess.open.ro", () -> () -> allSessions.values().stream()
					.filter(CDStarSession::isReadOnly).count());
			mr.gauge("sess.open.rw", () -> () -> allSessions.values().stream()
					.filter(((Predicate<CDStarSession>)CDStarSession::isReadOnly).negate()).count());
			mr.gauge("sess.total", () -> () -> totalCounter.get());
		});
	}

	private void reapExpiredSessions() {
		for (final CDStarSession s : allSessions.values()) {
			if (s.isExpired()) {
				log.info("Closing expired session: {} (readOnly={})", s.getSessionId(), String.valueOf(s.isReadOnly()));
				s.rollback(new TimeoutException("Session expired"));
			}
		}
	}

	@Override
	public void onShutdown(RuntimeContext ctx) {
		sessionReaper.cancel(false);
		for (final CDStarSession s : allSessions.values()) {
			try {
				synchronized (s) {
					if (!s.isClosed()) {
						log.error("Forcefully closing session: {}", s.getSessionId());
						s.rollback(new CancellationException("Runtime shutting down"));
					}
				}
			} catch (final Exception e) {
				log.error("Failed to rollback session during shutdown: {}", s.getSessionId(), e);
			}
		}
	}

	@Override
	public void onSessionStarted(CDStarSession session) {
		allSessions.put(session.getSessionId(), session);
		totalCounter.incrementAndGet();
		session.addListener(this);
	}

	@Override
	public void onCommit(CDStarSession session) {
		allSessions.remove(session.getSessionId());
	}

	@Override
	public void onRollback(CDStarSession session) {
		allSessions.remove(session.getSessionId());
	}

	@Override
	public Optional<CDStarSession> get(String tx) {
		return Optional.ofNullable(allSessions.get(tx));
	}

}
