package de.gwdg.cdstar.runtime.services;

import java.util.Optional;

import de.gwdg.cdstar.runtime.client.CDStarSession;

public interface SessionRegistry {

	/**
	 * Return the session with the specified ID, if present.
	 */
	Optional<CDStarSession> get(String tx);

}