package de.gwdg.cdstar.runtime.services;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Plugins or optional components can register themselves here to signal to
 * clients that specific APIs or optional features are available. The features
 * and their metadata are returned by <code>GET /v3</code> requests.
 *
 * Feature names should be simple camelCased alphanumeric strings, preferable words.
 * Feature info is a free-form key/value map (both strings).
 */
public class FeatureRegistry {

	public static class FeatureEntry {
		String feature;
		Map<String, String> info;

		public FeatureEntry(String feature, Map<String, String> info) {
			super();
			this.feature = feature;
			this.info = info;
		}

		public String getFeature() {
			return feature;
		}
		public Map<String, String> getInfo() {
			return info;
		}


	}

	Map<String, FeatureEntry> features = new HashMap<>();

	public Map<String, FeatureEntry> getFeatures() {
		return Collections.unmodifiableMap(features);
	}

	public void addFeature(FeatureEntry value) {
		features.put(value.feature, value);
	}

	public void addFeature(String feature, Map<String, String> info) {
		addFeature(new FeatureEntry(feature, info));
	}


}
