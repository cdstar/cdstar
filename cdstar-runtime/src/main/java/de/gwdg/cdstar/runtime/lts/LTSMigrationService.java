package de.gwdg.cdstar.runtime.lts;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.gwdg.cdstar.runtime.Plugin;
import de.gwdg.cdstar.runtime.RuntimeContext;
import de.gwdg.cdstar.runtime.listener.RuntimeListener;
import de.gwdg.cdstar.runtime.services.PoolService;

/**
 * This service observes profile changes in archives and manages
 * {@link LTSTarget} instances. If a profile is changed, the matching
 * {@link LTSTarget} is used to perform the migration.
 */
@Plugin
public class LTSMigrationService implements RuntimeListener {

	private static final Logger log = LoggerFactory.getLogger(LTSMigrationService.class);
	private final Map<String, LTSTarget> adapters = new ConcurrentHashMap<>();
	private ExecutorService pool;

	@Override
	public void onInit(RuntimeContext ctx) throws Exception {
		ctx.register(new MigrationTaskRunner(this));
		ctx.register(new CleanupTaskRunner(this));
		ctx.register(new ProfileChangeListener());
	}

	@Override
	public void onStartup(RuntimeContext ctx) throws Exception {
		if (ctx.lookupAll(LTSMigrationService.class).size() > 1)
			throw new IllegalStateException("Service " + getClass() + " installed twice.");

		pool = ctx.lookupRequired(PoolService.class).getNamedPool("migration");

		for (final LTSTarget adapter : ctx.lookupAll(LTSTarget.class)) {
			final String name = adapter.getName();
			if (adapters.putIfAbsent(name, adapter) != null)
				throw new IllegalStateException("LTS target with same name already registered: " + name);
			log.info("Added LTS target: {} (implemented by {})", name, adapter.getClass().getName());
		}
	}

	public ExecutorService getPool() {
		return pool;
	}

	/**
	 * Return the storage adapter with the given name.
	 */
	public Optional<LTSTarget> getAdapter(String name) {
		return Optional.ofNullable(adapters.get(name));
	}
}
