package de.gwdg.cdstar.runtime.profiles;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import de.gwdg.cdstar.runtime.client.CDStarProfile;

class ProfileImpl implements CDStarProfile {

	private final String name;
	private final Map<String, String> parameters = new HashMap<>();

	public ProfileImpl(String name) {
		this.name = name;
	}

	public ProfileImpl(String name, Map<String, String> config) {
		this(name);
		if (config != null)
			parameters.putAll(config);
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public String toString() {
		return "profile:" + name;
	}

	@Override
	public Map<String, String> getPropertyMap() {
		return Collections.unmodifiableMap(parameters);
	}

}