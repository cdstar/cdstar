package de.gwdg.cdstar.runtime.services.health;

public enum HealthLevel {
	OK, WARN, ERROR;

	/**
	 * Return one of the given strings based on the value of this enum.
	 */
	public String toString(String ok, String warn, String error) {
		switch (this) {
		case OK:
			return ok;
		case WARN:
			return warn;
		case ERROR:
		default:
			return error;
		}
	}

	/**
	 * Return "green", "yellow" or "red".
	 */
	public String toColor() {
		return toString("green", "yellow", "red");
	}

	@Override
	public String toString() {
		return toString("ok", "warn", "error");
	}
}