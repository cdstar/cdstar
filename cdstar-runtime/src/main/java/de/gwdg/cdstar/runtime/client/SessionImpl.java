package de.gwdg.cdstar.runtime.client;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.gwdg.cdstar.GromitIterable;
import de.gwdg.cdstar.runtime.VaultConfig;
import de.gwdg.cdstar.runtime.client.exc.VaultNotFound;
import de.gwdg.cdstar.runtime.listener.SessionListener;
import de.gwdg.cdstar.runtime.listener.SessionStartListener;
import de.gwdg.cdstar.runtime.services.VaultRegistry;
import de.gwdg.cdstar.ta.TAListener;
import de.gwdg.cdstar.ta.TransactionInfo;
import de.gwdg.cdstar.ta.TransactionInfo.Mode;
import de.gwdg.cdstar.ta.UserTransaction;
import de.gwdg.cdstar.ta.exc.TARollbackException;

class SessionImpl implements CDStarSession {
	private static final Logger listenerLog = LoggerFactory.getLogger(SessionListener.class);

	private final class TransactionListenerBridge implements TAListener {

		@Override
		public void beforeCommit(TransactionInfo t) throws Exception {
			listeners.forEach(l -> l.onPrepare(SessionImpl.this));
		}

		@Override
		public void afterCommit(TransactionInfo t) {
			listeners.forEach(l -> l.onCommit(SessionImpl.this),
				e -> listenerLog.warn("After-commit hook for session {} failed", tx.getId(), e));
		}

		@Override
		public void afterRollback(TransactionInfo t) {
			listeners.forEach(l -> l.onRollback(SessionImpl.this),
				e -> listenerLog.warn("After-rollback hook for session {} failed", tx.getId(), e));
		}

	}

	final ClientImpl client;
	private final UserTransaction tx;
	private final GromitIterable<SessionListener> listeners = new GromitIterable<>();
	private final ArrayList<VaultImpl> vaults = new ArrayList<>(1);
	long timeout = -1;
	private Map<String, Object> context;
	private final VaultRegistry vaultRegistry;
	private boolean timeoutSuspended;

	/**
	 * Create a new session
	 */
	SessionImpl(ClientImpl client, boolean readOnly, Mode mode) {
		this.client = client;
		tx = client.runtime.createTransaction(readOnly, mode);
		client.runtime.lookupAll(SessionStartListener.class).forEach(l -> l.onSessionStarted(this));
		vaultRegistry = client.runtime.lookupRequired(VaultRegistry.class);
		tx.addListener(new TransactionListenerBridge());
	}

	UserTransaction getTx() {
		return tx;
	}

	@Override
	public CDStarClient getClient() {
		return client;
	}

	@Override
	public List<String> getVaultNames() {
		final ArrayList<String> result = new ArrayList<>();
		for (final VaultConfig vc : vaultRegistry.getAll()) {
			if (VaultImpl.isVisible(vc, client))
				result.add(vc.getName());
		}
		return result;
	}

	@Override
	public CDStarVault getVault(String vaultName) throws VaultNotFound {
		synchronized (vaults) {
			for (final VaultImpl v : vaults)
				if (v.getName().equals(vaultName))
					return v;
			final VaultConfig vc = vaultRegistry.getVault(vaultName).orElseThrow(() -> new VaultNotFound(vaultName));
			final VaultImpl vault = new VaultImpl(this, vc, vaultRegistry.getPoolFor(vc));
			vaults.add(vault);
			listeners.forEach(l -> l.onVaultOpenend(vault));
			return vault;
		}
	}

	@Override
	public void addListener(SessionListener listener) {
		listeners.add(listener);
	}

	@Override
	public String getSessionId() {
		return tx.getId();
	}

	@Override
	public synchronized void commit() throws TARollbackException {
		tx.commit();
	}

	@Override
	public synchronized void rollback(Throwable reason) {
		tx.rollback(reason);
	}

	@Override
	public synchronized void close() {
		tx.close();
	}

	@Override
	public boolean isClosed() {
		return tx.isClosed();
	}

	@Override
	public synchronized void setTimeoutSuspended(boolean suspend) {
		timeoutSuspended = suspend;
		// Let setTimeout handle the new suspend state
		refreshTimeout();
	}

	@Override
	public boolean isTimeoutSuspended() {
		return timeoutSuspended;
	}

	@Override
	public long setTimeout(long timeout) {
		final long oldTimeout = this.timeout;
		this.timeout = timeout;

		if (timeout < 0 || timeoutSuspended)
			tx.setExpires(null);
		else
			tx.setExpires(Instant.now().plusMillis(timeout));

		return oldTimeout;
	}

	@Override
	public long getTimeout() {
		return timeout;
	}

	@Override
	public long getRemainingTime() {
		final Instant expire;
		if (timeoutSuspended || (expire  = tx.getExpires()) == null)
			return -1;
		return Math.max(0, expire.toEpochMilli() - System.currentTimeMillis());
	}

	@Override
	public void setMode(Mode mode) {
		tx.setMode(mode);
	}

	@Override
	public Mode getMode() {
		return tx.getMode();
	}

	@Override
	public boolean isReadOnly() {
		return tx.isRollbackOnly();
	}

	@Override
	public Collection<CDStarVault> listOpenedVaults() {
		return Collections.unmodifiableList(vaults);
	}

	@Override
	public synchronized Map<String, Object> getContext() {
		if (context == null)
			context = Collections.synchronizedMap(new HashMap<String, Object>());
		return context;
	}

	@Override
	public UserTransaction getUserTransaction() {
		return tx;
	}

}
