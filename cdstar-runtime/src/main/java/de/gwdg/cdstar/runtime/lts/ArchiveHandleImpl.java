package de.gwdg.cdstar.runtime.lts;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import de.gwdg.cdstar.runtime.client.CDStarAttribute;
import de.gwdg.cdstar.runtime.client.utils.ArchiveOrSnapshot;

class ArchiveHandleImpl implements ArchiveHandle {

	private ArchiveOrSnapshot pkg;

	public ArchiveHandleImpl(ArchiveOrSnapshot pkg) {
		this.pkg = pkg;
	}

	@Override
	public String getVault() {
		return pkg.getVault().getName();
	}

	@Override
	public String getId() {
		return pkg.getId();
	}

	@Override
	public String getRevision() {
		return pkg.getRev();
	}

	@Override
	public Stream<FileHandle> listFiles() {
		return pkg.getFiles().stream().map(FileHandleImpl::new);
	}

	@Override
	public Map<String, List<String>> getMetaAttributes() {
		final Map<String, List<String>> copy = new HashMap<>();
		for (final CDStarAttribute attr : pkg.getAttributes()) {
			if (!attr.isEmpty())
				copy.put(attr.getName(), attr.values());
		}
		return copy;
	}

}