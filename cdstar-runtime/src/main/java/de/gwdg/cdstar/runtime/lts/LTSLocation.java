package de.gwdg.cdstar.runtime.lts;

import java.util.Objects;
import java.util.Optional;

import de.gwdg.cdstar.runtime.client.CDStarMirrorState;

/**
 * LTS target name and target-specific location string of a stored archive.
 *
 * The matching {@link LTSTarget} MUST be able to locate and recover a specific
 * LTS copy based on a pair of {@link ArchiveReference} and {@link LTSLocation}
 * instances. It SHOULD be able to do so with just the {@link LTSLocation}.
 */
public final class LTSLocation {
	private static final String SEP = "#";
	private final String mirror;
	private final String location;

	public LTSLocation(String mirror, String location) {
		this.mirror = Objects.requireNonNull(mirror, "Mirror name must not be null");
		this.location = Objects.requireNonNull(location, "Location must not be null");
		if (mirror.contains(SEP))
			throw new IllegalArgumentException("Mirror names must not contain: " + SEP);
	}

	public String getMirrorName() {
		return mirror;
	}

	public String getLocation() {
		return location;
	}

	@Override
	public String toString() {
		return mirror + SEP + location;
	}

	public static LTSLocation fromString(String mirrorLocation) {
		int index = mirrorLocation.indexOf(SEP);
		if (index > 0 && index < mirrorLocation.length() - 1) {
			return new LTSLocation(mirrorLocation.substring(0, index), mirrorLocation.substring(index + 1));
		} else {
			throw new IllegalArgumentException("Invalid mirror location: " + mirrorLocation);
		}
	}

	public static Optional<LTSLocation> fromMirrorState(CDStarMirrorState mirror) {
		if (mirror.isMirrored())
			return Optional.of(new LTSLocation(mirror.getMirrorName(), mirror.getMirrorLocation()));
		return Optional.empty();
	}
}