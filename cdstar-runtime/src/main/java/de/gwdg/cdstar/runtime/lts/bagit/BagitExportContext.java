package de.gwdg.cdstar.runtime.lts.bagit;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Map.Entry;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;

import com.fasterxml.jackson.core.JsonGenerator;

import de.gwdg.cdstar.GitInfo;
import de.gwdg.cdstar.SharedObjectMapper;
import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.runtime.client.CDStarFile;
import de.gwdg.cdstar.runtime.lts.ArchiveHandle;
import de.gwdg.cdstar.runtime.lts.FileHandle;
import de.gwdg.cdstar.runtime.lts.LTSLocation;
import de.gwdg.cdstar.runtime.lts.LTSTarget.ExportContext;
import de.gwdg.cdstar.runtime.lts.MigrationFailedException;

public class BagitExportContext implements ExportContext {

	private final BagitTarget adapter;

	private final LTSLocation location;
	private final Path targetPath;
	private final ExecutorService pool;
	private Path tempPath;
	private final ArchiveHandle archive;
	private final ArrayList<FileHandle> files;


	public BagitExportContext(BagitTarget adapter, ArchiveHandle archive) {
		this.adapter = adapter;
		location = new LTSLocation(adapter.getName(), Utils.bytesToHex(Utils.randomBytes(4)));
		pool = adapter.getPool();
		this.archive = archive;
		files = new ArrayList<>();

		try {
			tempPath = adapter.createTempPath(location.getLocation()).toAbsolutePath();
			targetPath = adapter.getPathFor(archive, location, BagitTarget.BAGIT_SUFFIX);
		} catch (final IOException e) {
			throw new MigrationFailedException(e);
		}
	}

	@Override
	public CompletableFuture<Void> exportFile(FileHandle file, InputStream stream) {
		return CompletableFuture.runAsync(() -> {

			final Path dataPath = tempPath.resolve("data");
			final Path target = dataPath.resolve(file.getName());

			if (!target.toAbsolutePath().startsWith(dataPath))
				throw new MigrationFailedException("Invalid file name: " + file.getName());

			try {
				Files.createDirectories(target.getParent());
				Files.copy(stream, target);
				files.add(file);
			} catch (final IOException e) {
				throw new MigrationFailedException(e);
			}

		}, pool);
	}

	@Override
	public CompletableFuture<LTSLocation> complete() {
		return CompletableFuture.supplyAsync(() -> {

			// Write /cdstar.json (v0)
			try (final BufferedWriter writer = Files.newBufferedWriter(
				tempPath.resolve("cdstar.json"), StandardCharsets.UTF_8, StandardOpenOption.CREATE_NEW)) {
				final JsonGenerator json = SharedObjectMapper.json.getFactory().createGenerator(writer);
				json.writeStartObject();
				json.writeStringField("_version", "0");
				if (GitInfo.isAvailable())
					json.writeStringField("_generator", "cdstar-" + GitInfo.getInstance().getVersion());

				json.writeStringField("vault", archive.getVault());
				json.writeStringField("archive", archive.getId());
				json.writeStringField("revision", archive.getRevision());

				json.writeStringField("exportId", location.getLocation());
				json.writeNumberField("fileCount", files.size());
				json.writeObjectField("meta", archive.getMetaAttributes());

				json.writeArrayFieldStart("files");
				for (final FileHandle file : files) {
					json.writeStartObject();

					json.writeStringField("id", file.getId());
					json.writeStringField("name", file.getName());
					json.writeNumberField("size", file.getSize());
					json.writeStringField("type", file.getType());

					for (Entry<String, String> entry : file.getDigests().entrySet()) {
						String name = entry.getKey().toLowerCase().replace("-", "");
						json.writeStringField(name, entry.getValue());
					}

					json.writeObjectField("meta", file.getMetaAttributes());
					json.writeEndObject();
				}

				json.writeEndArray();
				json.writeEndObject();
				json.close();
			} catch (final IOException e) {
				throw new MigrationFailedException("Failed to write: cdstar.json", e);
			}

			// Write /manifest-sha256.txt
			try (final BufferedWriter writer = Files.newBufferedWriter(
					tempPath.resolve("manifest-sha256.txt"), StandardCharsets.UTF_8, StandardOpenOption.CREATE_NEW)) {

				for (final FileHandle file : files) {
					writer.write(file.getDigests().get(CDStarFile.DIGEST_SHA256));
					writer.write(" /data/");
					writer.write(file.getName());
					writer.write('\n');
				}

			} catch (final IOException e) {
				throw new MigrationFailedException("Failed to write: manifest-md5.txt", e);
			}

			// Write /bagit.txt
			try (final BufferedWriter writer = Files.newBufferedWriter(
					tempPath.resolve("bagit.txt"), StandardCharsets.UTF_8, StandardOpenOption.CREATE_NEW)) {

				writer.write("BagIt-version: 1.0\n");
				writer.write("Tag-File-Character-Encoding: UTF-8\n");
				writer.flush();

			} catch (final IOException e) {
				throw new MigrationFailedException("Failed to write: bagit.txt", e);
			}

			// TODO: Write /tagmanifest-md5.txt

			try {
				// Move folder to target location (atomically)
				Files.createDirectories(targetPath.getParent());
				Files.move(tempPath, targetPath);
				BagitTarget.log.debug("Exported to to {}", targetPath);
			} catch (final IOException e) {
				throw new MigrationFailedException("Failed to move export folder to target location:" + targetPath, e);
			}

			return location;
		}, pool);
	}

	@Override
	public void close() {
		if (Files.exists(tempPath))
			Utils.deleteDirectoryTree(tempPath);
	}

}
