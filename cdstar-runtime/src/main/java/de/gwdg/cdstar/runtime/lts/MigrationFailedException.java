package de.gwdg.cdstar.runtime.lts;

/**
 * {@link RuntimeException} to wrap checked exceptions in.
 */
public class MigrationFailedException extends RuntimeException {

	private static final long serialVersionUID = -449326962217216996L;

	public MigrationFailedException(String message, Exception e) {
		super(message, e);
	}

	public MigrationFailedException(Throwable e) {
		super(e);
	}

	public MigrationFailedException(String message) {
		super(message);
	}
}