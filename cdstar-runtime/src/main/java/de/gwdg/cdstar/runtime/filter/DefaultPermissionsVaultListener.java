package de.gwdg.cdstar.runtime.filter;

import java.util.HashMap;
import java.util.Map;

import de.gwdg.cdstar.runtime.VaultConfig;
import de.gwdg.cdstar.runtime.ConfigException;
import de.gwdg.cdstar.runtime.client.CDStarArchive;
import de.gwdg.cdstar.runtime.listener.VaultListener;

class DefaultPermissionsVaultListener implements VaultListener {
	final Map<String, DefaultPermission> defaults = new HashMap<>();

	public static boolean isEnabledFor(VaultConfig vault) {
		return vault.getPropertyNames().stream().anyMatch(key -> key.startsWith("acl.default."));
	}

	DefaultPermissionsVaultListener(VaultConfig vaultConfig) throws ConfigException {
		for (final String key : vaultConfig.getPropertyNames()) {
			if (key.startsWith("acl.default.")) {
				final String value = vaultConfig.getProperty(key);
				final String subject = key.substring("acl.default.".length());
				defaults.put(subject, new DefaultPermission(subject, value.split("\\s*,\\s*")));
			}
		}
		if (!defaults.containsKey("$owner")) {
			defaults.put("$owner", new DefaultPermission("$owner", new String[] { "OWNER" }));
		}
	}

	@Override
	public void archiveLoaded(CDStarArchive archive) {
		if (archive.getParentRev() != null)
			return; // Not a new archive

		for (final DefaultPermission d : defaults.values()) {
			d.apply(archive.getACL());
		}
	}

}