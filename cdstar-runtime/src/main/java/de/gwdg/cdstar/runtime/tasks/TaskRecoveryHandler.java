package de.gwdg.cdstar.runtime.tasks;

import java.net.URI;
import java.nio.file.Path;
import java.nio.file.Paths;

import de.gwdg.cdstar.ta.TAJournal;
import de.gwdg.cdstar.ta.TAJournalReader;
import de.gwdg.cdstar.ta.TAJournalRecord;
import de.gwdg.cdstar.ta.TARecoveryHandler;

class TaskRecoveryHandler implements TARecoveryHandler {

	private static final long serialVersionUID = -5664460716465954332L;
	private final URI path;
	private final String journalKey;

	TaskRecoveryHandler(Path persistPath) {
		path = persistPath.toUri();
		journalKey = "task:" + System.identityHashCode(this);
	}

	@Override
	public void recover(String tx, TAJournalReader log, boolean doCommit) throws Exception {
		final TaskIOHelper io = new TaskIOHelper(Paths.get(path));
		TAJournalRecord record;
		while ((record = log.next(journalKey)) != null) {
			final String taskId = record.getStringValue();
			if (!io.isPrepared(taskId))
				continue;
			if (doCommit) {
				io.submit(taskId);
				// Submitted tasks will be picked up by the TaskService during
				// start-up.
			} else {
				io.forget(taskId, false);
			}
		}
	}

	void rememberTask(TAJournal journal, String taskId) {
		journal.write(journalKey, taskId);
	}

}
