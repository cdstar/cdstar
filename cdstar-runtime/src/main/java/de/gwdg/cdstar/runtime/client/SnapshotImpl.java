package de.gwdg.cdstar.runtime.client;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.auth.Subject;
import de.gwdg.cdstar.pool.BackendError;
import de.gwdg.cdstar.pool.PoolError.ExternalResourceException;
import de.gwdg.cdstar.pool.PoolError.StaleHandle;
import de.gwdg.cdstar.pool.Resource;
import de.gwdg.cdstar.pool.StorageObject;
import de.gwdg.cdstar.runtime.client.auth.ArchivePermission;
import de.gwdg.cdstar.runtime.client.auth.StringSubject.PrincipalSubject;
import de.gwdg.cdstar.runtime.client.exc.ArchiveNotAvailable;
import de.gwdg.cdstar.runtime.client.exc.FileNotFound;
import de.gwdg.cdstar.runtime.client.exc.InvalidSnapshotName;
import de.gwdg.cdstar.runtime.client.exc.ProfileNotDefined;

/**
 * Snapshots are managed by {@link SnapshotList} and most properties (name,
 * profile, creator, ...) are part of the {@link SnapshotEntry} stored in
 * the same {@link StorageObject} as the owning {@link CDStarArchive}. Immutable
 * snapshot payload however (files and metadata) is stored in separate
 * {@link StorageObject}s and prefixed with {@code /snapshots/{snapshotName}/}
 * to allow multiple snapshots to be stored in the same storage object. Snapshot
 * names must not contain a literal slash for this reason.
 */
public class SnapshotImpl implements CDStarSnapshot {

	private static final String SNAPSHOT_TYPE = "application/x-cdstar-snapshot;v=3";
	private static final String RESOURCE_PREFIX = "snapshots/";
	private static final String FILEPROP_MODIFIED = "snapshot:origLastModified";
	private static final String FILEPROP_CREATED = "snapshot:origCreated";
	private static final String FILEPROP_ID = "snapshot:oridId";

	private ArchiveImpl archive;
	private SnapshotEntry meta;
	private StorageObject payload;
	private AttributeCache cachedMeta;
	private ArrayList<FileImpl> cachedFiles;
	private MirrorStateImpl cachedMirrorState;
	private CDStarProfile cachedProfile;
	private boolean isModified;
	private boolean isNew;

	public SnapshotImpl(ArchiveImpl archive, SnapshotEntry meta) {
		this.archive = archive;
		this.meta = meta;
	}

	/**
	 * Create a NEW snapshot in the given target {@link StorageObject}.
	 * 
	 * @return
	 * 
	 * @throws InvalidSnapshotName
	 * @throws IOException
	 * @throws ExternalResourceException
	 * @throws StaleHandle
	 */
	public static SnapshotImpl createFrom(ArchiveImpl archive,
			String name,
			StorageObject targetObject)
			throws InvalidSnapshotName, StaleHandle, ExternalResourceException, IOException {
		archive.requireTransactionWriteable();
		archive.requirePayloadAvailable();
		if (archive.isContentModified())
			throw new IllegalStateException("Snapshots can only be created from unmodified content");

		if (name.contains("/"))
			throw new InvalidSnapshotName(name, "Invalid character in snapshot name: /");
		
		if (targetObject.getType() == null)
			targetObject.setType(SNAPSHOT_TYPE);
		if (!SNAPSHOT_TYPE.equals(targetObject.getType()))
			throw new BackendError("Snapshot mime-type mismatch: " + targetObject.getType());

		// Copy files (clone if possible)
		String resourcePrefix = RESOURCE_PREFIX + name + "/";
		String filePrefix = resourcePrefix + FileImpl.RESOURCE_PREFIX;
		for(FileImpl file: archive.getInternalFileList()) {
			Resource target = targetObject.createResource(filePrefix + file.getName());
			target.cloneFrom(file.resource);
			target.setMediaType(file.getMediaType(), file.getContentEncoding());
			// Store information we cannot control in resource properties.
			target.setProperty(FILEPROP_ID, file.getID());
			target.setProperty(FILEPROP_CREATED, Long.toString(file.getCreated().getTime()));
			target.setProperty(FILEPROP_MODIFIED, Long.toString(file.getLastModified().getTime()));
		}

		// Copy metadata, if present
		Resource metaResource = archive.getResource(ArchiveImpl.METAFILE);
		if (metaResource != null) {
			Resource target = targetObject.createResource(resourcePrefix + ArchiveImpl.METAFILE);
			target.setMediaType(ArchiveImpl.METAFILE_TYPE);
			target.cloneFrom(metaResource);
		}
		
		Subject subject = archive.getVault().getSession().getClient().getSubject();
		String creator = new PrincipalSubject(
				subject.getPrincipal().getId(),
				subject.getPrincipal().getDomain()).toString();
		Date now = new Date();

		SnapshotEntry meta = new SnapshotEntry(archive.getId(), archive.getRev(), name, creator, now, now, null,
				targetObject.getId(), new HashMap<>(0));

		SnapshotImpl snap = new SnapshotImpl(archive, meta);
		snap.isNew = true;
		return snap;
	}

	private synchronized StorageObject load() {
		if (payload == null) {
			payload = ((VaultImpl) archive.getVault()).loadDirect(meta.ref);
			if (!SNAPSHOT_TYPE.equals(payload.getType())) {
				payload = null;
				throw new BackendError("Snapshot mime-type mismatch: " + meta.ref);
			}
		}

		return payload;
	}

	@Override
	public synchronized CDStarMirrorState getMirrorState() {
		if (cachedMirrorState == null)
			cachedMirrorState = new MirrorStateImpl(archive, this);
		return cachedMirrorState;
	}

	synchronized AttributeCache getAttributeCache() {
		if (cachedMeta == null) {
			archive.checkPermission(ArchivePermission.READ_META);
			cachedMeta = new AttributeCache(archive, this);
		}
		return cachedMeta;
	}

	@Override
	public CDStarAttribute getAttribute(String name) {
		return getAttributeCache().getAttribute(null, name);
	}

	@Override
	public Set<CDStarAttribute> getAttributes() {
		return getAttributeCache().getAttributes(null);
	}

	@Override
	public CDStarArchive getSource() {
		return archive;
	}

	@Override
	public String getRevision() {
		return meta.revision;
	}

	@Override
	public String getName() {
		return meta.name;
	}

	@Override
	public boolean isAvailable() {
		return getMirrorState().isAvailable();
	}

	@Override
	public Date getCreated() {
		return meta.created;
	}

	@Override
	public Date getModified() {
		return meta.modified;
	}

	@Override
	public String getCreator() {
		return meta.creator;
	}

	@Override
	public synchronized CDStarProfile getProfile() {
		if (cachedProfile == null)
			cachedProfile = archive.loadProfile(getRawProperty(ArchiveImpl.PROP_PROFILE, CDStarProfile.DEFAULT_NAME));
		return cachedProfile;
	}

	@Override
	public void setProfile(CDStarProfile newProfile) {
		Objects.requireNonNull(newProfile);
		archive.requireTransactionWriteable();
		archive.checkPermission(ArchivePermission.CHANGE_PROFILE);

		try {
			newProfile = archive.getVault().getProfileByName(newProfile.getName());
		} catch (final ProfileNotDefined e) {
			throw new IllegalArgumentException("Profile not defined for the given vault", e);
		}

		final CDStarProfile oldProfile = getProfile();
		if (!oldProfile.equals(newProfile)) {
			cachedProfile = newProfile;
			setRawProperty(ArchiveImpl.PROP_PROFILE, cachedProfile.getName());
			archive.forEachListener(l -> l.snapshotProfileChanged(this, oldProfile));
		}
	}

	synchronized List<FileImpl> getInternalFileList() {
		String filePrefix = RESOURCE_PREFIX + getName() + "/" + FileImpl.RESOURCE_PREFIX;
		if (cachedFiles == null) {
			cachedFiles = new ArrayList<>();
			for (final Resource r : load().getResourcesByPrefix(filePrefix)) {
				String name = r.getName().substring(filePrefix.length());
				cachedFiles.add(new FileImpl(archive, r, name, this));
			}
		}
		return cachedFiles;
	}

	@Override
	public List<CDStarFile> getFiles() {
		archive.checkPermission(ArchivePermission.LIST_FILES);
		return Collections.unmodifiableList(getInternalFileList());
	}

	@Override
	public boolean hasFile(String name) {
		archive.checkPermission(ArchivePermission.LIST_FILES);
		return Utils.first(getInternalFileList(), f -> name.equals(f.getName())) != null;
	}

	@Override
	public CDStarFile getFile(String name) throws FileNotFound {
		archive.checkPermission(ArchivePermission.LIST_FILES);
		FileImpl file = Utils.first(getInternalFileList(), f -> name.equals(f.getName()));
		if (file != null)
			return file;
		throw new FileNotFound(archive.getVault().getName(), getId(), name);
	}

	@Override
	public synchronized void remove() {
		if (meta.removed != null)
			return;

		// TODO: Allow this once the LTS migration can handle deleted snapshots)
		if (getMirrorState().isMirrored())
			throw new ArchiveNotAvailable(archive.getVault().getName(), getId());

		archive.checkPermission(ArchivePermission.DELETE);
		archive.markModified();
		isModified = true;

		// Remove all files belonging to this snapshot.
		String resourcePrefix = RESOURCE_PREFIX + getName() + "/";
		load().getResourcesByPrefix(resourcePrefix).forEach(r -> r.remove());
		// If the snapshot storage object is now empty, remove it
		if (load().getResources().isEmpty() && load().getPropertyNames().isEmpty())
			load().remove();

		cachedFiles = null;
		cachedMeta = null;
		cachedMirrorState = null;
		meta.removed = new Date();
		archive.forEachListener(l -> l.snapshotRemoved(this));
	}

	@Override
	public boolean isRemoved() {
		// TODO Auto-generated method stub
		return meta.removed != null;
	}

	boolean isModified() {
		return isModified || isNew;
	}

	boolean isNew() {
		return isNew;
	}

	SnapshotEntry getMeta() {
		return meta;
	}

	@Override
	public String getProperty(String name) {
		return getRawProperty(ArchiveImpl.PROP_SYS_NS + name, null);
	}

	@Override
	public void setProperty(String name, String value) {
		setRawProperty(ArchiveImpl.PROP_SYS_NS + name, value);
	}

	public String getRawProperty(String name, String defaultvalue) {
		return meta.properties.getOrDefault(name, defaultvalue);
	}

	public void setRawProperty(String name, String value) {
		archive.requireTransactionWriteable();
		archive.markModified();
		isModified = true;
		meta.properties.put(name, value);
	}

	String getOrigFileId(Resource resource) {
		return resource.getProperty(FILEPROP_ID);
	}

	Date getOrigFileCreated(Resource resource) {
		return new Date(Long.parseLong(resource.getProperty(FILEPROP_CREATED)));
	}

	Date getOrigFileModified(Resource resource) {
		return new Date(Long.parseLong(resource.getProperty(FILEPROP_MODIFIED)));
	}

	public Resource getResource(String resource) {
		return load().getResource(RESOURCE_PREFIX + getName() + "/" + resource);
	}

	@Override
	public String toString() {
		return "Snapshot(" + archive.getVault().getName() + "/" + getId() + ")";
	}

}
