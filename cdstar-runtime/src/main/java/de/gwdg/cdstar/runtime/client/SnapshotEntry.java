package de.gwdg.cdstar.runtime.client;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import de.gwdg.cdstar.pool.StorageObject;

/**
 * Metadata about a single snapshot. This info is stored in the
 * {@link SnapshotList#SNAPSHOT_INDEX_FILE} and partly in properties of the
 * snapshot {@link StorageObject} itself.
 */
public class SnapshotEntry {
	public final String archive; // The archive ID this snapshot was taken from.
	public final String revision;
	public final String name; // Snapshot name
	public String ref; // ID or location of the snapshot data object, or null
						// for local snapshots.
	public final Date created;
	public final String creator;
	public Date modified; // Time this snapshot was last modified (e.g. profile)
	public Date removed; // Time this snapshot was removed, or null

	public Map<String, String> properties;

	@JsonCreator
	public SnapshotEntry(@JsonProperty("archive") String archive, @JsonProperty("revision") String revision,
			@JsonProperty("name") String name, @JsonProperty("creator") String creator,
			@JsonProperty("created") Date created, @JsonProperty("modified") Date modified,
			@JsonProperty("removed") Date removed, @JsonProperty("ref") String ref,
			@JsonProperty("properties") Map<String, String> properties) {
		this.archive = archive;
		this.revision = revision;
		this.name = name;
		this.created = created;
		this.creator = creator;
		this.modified = modified;
		this.ref = ref;
		this.removed = removed;
		this.properties = properties;
		if (this.properties == null)
			this.properties = new HashMap<>(0);
	}
}