package de.gwdg.cdstar.runtime.lts;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.runtime.RuntimeContext;
import de.gwdg.cdstar.runtime.client.CDStarArchive;
import de.gwdg.cdstar.runtime.client.CDStarMirrorState;
import de.gwdg.cdstar.runtime.client.CDStarProfile;
import de.gwdg.cdstar.runtime.client.CDStarSession;
import de.gwdg.cdstar.runtime.client.CDStarSnapshot;
import de.gwdg.cdstar.runtime.client.LTSMigrationSupport;
import de.gwdg.cdstar.runtime.client.utils.ArchiveOrSnapshot;
import de.gwdg.cdstar.runtime.listener.ArchiveListener;
import de.gwdg.cdstar.runtime.listener.RuntimeListener;
import de.gwdg.cdstar.runtime.listener.SessionListener;
import de.gwdg.cdstar.runtime.tasks.Task;
import de.gwdg.cdstar.runtime.tasks.TaskBuilder;
import de.gwdg.cdstar.runtime.tasks.TaskService;

/*
 * TODO: Rewrite this to not use the ArchiveListener interface, then remove archive listeners.
 */
class ProfileChangeListener implements ArchiveListener, RuntimeListener {

	private static final Logger log = LoggerFactory.getLogger(ProfileChangeListener.class);

	private TaskService taskService;

	@Override
	public void onStartup(RuntimeContext ctx) throws Exception {
		taskService = ctx.lookupRequired(TaskService.class);
	}

	@Override
	public void profileChanged(CDStarArchive archive, CDStarProfile oldProfile) {
		profileChanged(new ArchiveOrSnapshot(archive), oldProfile);
	}

	@Override
	public void snapshotProfileChanged(CDStarSnapshot snapshot, CDStarProfile oldProfile) {
		profileChanged(new ArchiveOrSnapshot(snapshot), oldProfile);
	}

	private void profileChanged(ArchiveOrSnapshot pkg, CDStarProfile oldProfile) {
		synchronized (pkg.getSourceArchive()) {
			if (pkg.getProfile().equals(oldProfile))
				return;

			log.debug("Profile changed: vault={} archive={} old={} new={}",
					pkg.getVault().getName(), pkg.getId(),
					oldProfile.getName(), pkg.getProfile().getName());

			CDStarMirrorState mirrorState = pkg.getMirrorState();
			final String pendingTaskId = LTSMigrationSupport.getMigrationTaskId(pkg.getMirrorState());

			if (pendingTaskId != null) {
				LTSMigrationSupport.clearMigrationTask(mirrorState);

				// Try to cancel running or pending tasks that are now obsolete.
				pkg.getVault().getSession().addListener(new SessionListener() {
					@Override
					public void onCommit(CDStarSession session) {
						taskService.getTask(pendingTaskId).ifPresent(Task::cancel);
					}
				});
			}

			if (!needsMigration(pkg))
				return; // Nothing to do

			// Spawn a new task (bound to the transaction).
			final TaskBuilder taskBuilder = taskService
					.builder(MigrationTaskRunner.class)
					.param(MigrationTaskRunner.PARAM_VAULT, pkg.getVault().getName())
					.param(MigrationTaskRunner.PARAM_ARCHIVE, pkg.getSourceArchive().getId());
			pkg.asSnapshot().ifPresent(s -> taskBuilder.param(MigrationTaskRunner.PARAM_SNAPSHOT, s.getName()));
			Task task = taskBuilder.prepare().bind(pkg.getVault().getSession().getUserTransaction());

			// Store task ID to archive, marking it as pending.
			LTSMigrationSupport.setMigrationTaskPending(mirrorState, task.getId());
		}
	}

	@Override
	public void archiveRemoved(CDStarArchive archive) {
		objectRemoved(new ArchiveOrSnapshot(archive));
	}

	@Override
	public void snapshotRemoved(CDStarSnapshot snapshot) {
		objectRemoved(new ArchiveOrSnapshot(snapshot));
	}

	private void objectRemoved(ArchiveOrSnapshot pkg) {
		final CDStarMirrorState state = pkg.getMirrorState();
		if (!state.isMirrored()) return;
		final LTSLocation loc = new LTSLocation(state.getMirrorName(), state.getMirrorLocation());

		final Task task = taskService
				.builder(CleanupTaskRunner.class)
				.param(CleanupTaskRunner.PARAM_VAULT, pkg.getVault().getName())
				.param(CleanupTaskRunner.PARAM_ARCHIVE, pkg.getSourceArchive().getId())
				.param(CleanupTaskRunner.PARAM_REVISION, pkg.getRev())
				.param(CleanupTaskRunner.PARAM_LOC, loc.toString())
				.prepare().bind(pkg.getVault().getSession().getUserTransaction());

		log.debug("LTS Removal triggered for {} in task {}", loc, task);
	}

	private boolean needsMigration(ArchiveOrSnapshot pkg) {
		final CDStarMirrorState state = pkg.getMirrorState();
		final LTSConfig target = LTSConfig.fromProfile(pkg.getProfile()).orElse(null);

		if (target == null) {
			return state.isMirrored(); // recover
		} else {
			if (!state.isMirrored())
				return true; // archive
			if (!Utils.equal(target.getName(), state.getMirrorName()))
				return true; // migrate
			if (pkg.getFileCount() > 0 && target.isCold() == pkg.isAvailable())
				return true; // Same location, but cold <-> hot change.
		}

		// No need to do anything
		return false;
	}

}
