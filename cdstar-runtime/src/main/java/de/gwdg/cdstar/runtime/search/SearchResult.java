package de.gwdg.cdstar.runtime.search;

import java.util.List;

public interface SearchResult {
	default int getSize() {
		return hits().size();
	}

	List<SearchHit> hits();

	/**
	 * Return a string that can be used to fetch the next page of the current search
	 * result.
	 */
	String getScrollID();

	long getTotal();

}
