package de.gwdg.cdstar.runtime.services;

import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.auth.realm.Authorizer;
import de.gwdg.cdstar.auth.simple.Account;
import de.gwdg.cdstar.auth.simple.SimpleAuthorizer;
import de.gwdg.cdstar.runtime.CDStarRuntime;
import de.gwdg.cdstar.runtime.RuntimeContext;
import de.gwdg.cdstar.runtime.listener.RuntimeListener;

public class AutoAdminUser implements RuntimeListener {
	private static final Logger log = LoggerFactory.getLogger(AutoAdminUser.class);
	private SimpleAuthorizer systemRealm;

	@Override
	public void onStartup(RuntimeContext ctx) throws Exception {
		final CDStarRuntime runtime = (CDStarRuntime) ctx;

		final List<Authorizer> authRealms = runtime.getAuthConfig().listRealms().stream()
			.filter(r -> r instanceof Authorizer)
			.map(r -> (Authorizer) r)
			.collect(Collectors.toList());

		if (authRealms.size() == 1
			&& authRealms.get(0) instanceof SimpleAuthorizer
			&& authRealms.get(0).getName() == "system") {
			log.info("No realm configured. Adding admin user to system realm...");
			systemRealm = (SimpleAuthorizer) authRealms.get(0);
			final char[] adminPassword = Utils.randomChars(16,
				"0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ");
			final Account admin = systemRealm.account("admin").withPermissions("*").password(adminPassword);
			log.info("\n" +
				"**********************************************************************\n" +
				"Created '{}' user with random password: {}\n" +
				"**********************************************************************",
				admin.getFullId(), new String(adminPassword));
		}
	}
}
