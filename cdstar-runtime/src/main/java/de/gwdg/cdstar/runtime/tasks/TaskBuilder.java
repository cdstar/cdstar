package de.gwdg.cdstar.runtime.tasks;

import java.util.HashMap;
import java.util.Map;

import de.gwdg.cdstar.ta.UserTransaction;

/**
 * A builder for new tasks. The same builder can be used to create multiple
 * tasks with the same name.
 */
public abstract class TaskBuilder {

	private final String name;
	private final Map<String, String> params = new HashMap<>();

	TaskBuilder(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public Map<String, String> getParameterMap() {
		return params;
	}

	public TaskBuilder param(String name, String value) {
		params.put(name, value);
		return this;
	}

	public TaskBuilder param(String name, int value) {
		return param(name, Integer.toString(value));
	}

	public TaskBuilder param(String name, long value) {
		return param(name, Long.toString(value));
	}

	public TaskBuilder param(String name, boolean value) {
		return param(name, Boolean.toString(value));
	}

	public TaskBuilder param(String name, double value) {
		return param(name, Double.toString(value));
	}

	public TaskBuilder clear() {
		params.clear();
		return this;
	}

	/**
	 * Shortcut for {@link #prepare()} followed by a
	 * {@link PreparedTask#submit()}.
	 */
	public Task submit() {
		return prepare().submit();
	}

	/**
	 * Shortcut for {@link #prepare()} followed by a
	 * {@link PreparedTask#bind(de.gwdg.cdstar.ta.UserTransaction)}.
	 */
	public Task bind(UserTransaction ta) {
		return prepare().bind(ta);
	}

	public abstract PreparedTask prepare();

}
