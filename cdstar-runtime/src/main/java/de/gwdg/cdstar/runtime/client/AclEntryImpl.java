package de.gwdg.cdstar.runtime.client;

import java.util.EnumSet;
import java.util.Set;

import de.gwdg.cdstar.runtime.client.auth.ArchivePermission;
import de.gwdg.cdstar.runtime.client.auth.ArchivePermissionSet;
import de.gwdg.cdstar.runtime.client.auth.StringSubject;

public class AclEntryImpl implements CDStarACLEntry {

	private final StringSubject subject;
	final EnumSet<ArchivePermission> permissions;
	private final ArchiveImpl archive;

	public AclEntryImpl(ArchiveImpl archive, StringSubject subject, EnumSet<ArchivePermission> permissions) {
		this.archive = archive;
		this.subject = subject;
		this.permissions = permissions == null ? EnumSet.noneOf(ArchivePermission.class) : permissions.clone();
	}

	@Override
	public StringSubject getSubject() {
		return subject;
	}

	@Override
	public Set<ArchivePermission> getPermissions() {
		return permissions.clone();
	}

	@Override
	public void permit(ArchivePermission permit) {
		archive.checkPermission(ArchivePermission.CHANGE_ACL);
		archive.requireTransactionWriteable();
		EnumSet<ArchivePermission> old = permissions.clone();
		if (!permissions.add(permit))
			return;
		archive.markModified();
		archive.forEachListener(l -> l.aclChanged(archive, this, old));
	}

	@Override
	public void permit(ArchivePermissionSet permissionSet) {
		archive.checkPermission(ArchivePermission.CHANGE_ACL);
		archive.requireTransactionWriteable();
		EnumSet<ArchivePermission> old = permissions.clone();
		if (!permissions.addAll(permissionSet.getPermissions()))
			return;
		archive.markModified();
		archive.forEachListener(l -> l.aclChanged(archive, this, old));
	}

	@Override
	public void revoke(ArchivePermission permit) {
		archive.checkPermission(ArchivePermission.CHANGE_ACL);
		archive.requireTransactionWriteable();
		EnumSet<ArchivePermission> old = permissions.clone();
		if (!permissions.remove(permit))
			return;
		archive.markModified();
		archive.forEachListener(l -> l.aclChanged(archive, this, old));
	}

	@Override
	public void revoke(ArchivePermissionSet permissionSet) {
		archive.checkPermission(ArchivePermission.CHANGE_ACL);
		archive.requireTransactionWriteable();
		EnumSet<ArchivePermission> old = permissions.clone();
		if (!permissions.removeAll(permissionSet.getPermissions()))
			return;
		archive.markModified();
		archive.forEachListener(l -> l.aclChanged(archive, this, old));
	}
}