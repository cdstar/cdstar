package de.gwdg.cdstar.runtime.filter;

import java.util.EnumSet;

import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.runtime.client.CDStarACL;
import de.gwdg.cdstar.runtime.client.CDStarACLEntry;
import de.gwdg.cdstar.runtime.client.auth.ArchivePermission;
import de.gwdg.cdstar.runtime.client.auth.ArchivePermissionSet;
import de.gwdg.cdstar.runtime.client.auth.StringSubject;

class DefaultPermission {
	private final StringSubject subject;
	private final EnumSet<ArchivePermission> permits = EnumSet.noneOf(ArchivePermission.class);
	private final EnumSet<ArchivePermissionSet> sets = EnumSet.noneOf(ArchivePermissionSet.class);

	public DefaultPermission(String ident, String... permits) {
		subject = StringSubject.fromString(ident);
		for (final String permit : permits)
			add(permit);
	}

	DefaultPermission add(String grant) {
		try {
			if (Utils.isUpperCase(grant)) {
				sets.add(ArchivePermissionSet.valueOf(grant));
			} else if (Utils.isLowerCase(grant)) {
				permits.add(ArchivePermission.valueOf(grant.toUpperCase()));
			}
		} catch (final IllegalArgumentException e) {
			DefaultPermissionsPlugin.log.error("Unknown permission or permission set in config: {}", grant);
			throw e;
		}
		return this;
	}

	void apply(CDStarACL acc) {
		CDStarACLEntry acl = acc.forSubject(subject);
		acl.revokeAll();
		permits.forEach(acl::permit);
		sets.forEach(acl::permit);
	}
}