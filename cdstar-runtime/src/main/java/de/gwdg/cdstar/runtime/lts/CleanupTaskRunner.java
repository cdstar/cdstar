package de.gwdg.cdstar.runtime.lts;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;

import de.gwdg.cdstar.runtime.RuntimeContext;
import de.gwdg.cdstar.runtime.client.CDStarClient;
import de.gwdg.cdstar.runtime.listener.RuntimeListener;
import de.gwdg.cdstar.runtime.tasks.Task;
import de.gwdg.cdstar.runtime.tasks.TaskRunner;

class CleanupTaskRunner implements TaskRunner, RuntimeListener {
	static final String PARAM_VAULT = "vault";
	static final String PARAM_ARCHIVE = "archive";
	static final String PARAM_REVISION = "revision";
	static final String PARAM_LOC = "location";

	private final LTSMigrationService migrationService;
	private ExecutorService pool;
	private RuntimeContext runtime;
	private CDStarClient systemClient;

	public CleanupTaskRunner(LTSMigrationService migrationService) {
		this.migrationService = migrationService;
	}

	private String getRequiredParameter(Task task, String param) {
		return task
				.getOptional(param)
				.orElseThrow(() -> new IllegalArgumentException("Missing task parameter: " + param));
	}

	@Override
	public CompletableFuture<Void> run(Task task) {
		LTSLocation loc = LTSLocation.fromString(getRequiredParameter(task, PARAM_LOC));
		var adapter = migrationService.getAdapter(loc.getMirrorName())
				.orElseThrow(() -> new MigrationFailedException("No suitable adapter for mirror: " + loc));

		var ref = new ArchiveReferenceImpl(
				getRequiredParameter(task, PARAM_VAULT),
				getRequiredParameter(task, PARAM_ARCHIVE),
				getRequiredParameter(task, PARAM_REVISION));
		return adapter.markObsolete(ref, loc);
	}

}
