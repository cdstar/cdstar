package de.gwdg.cdstar.runtime.tasks;

import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

public interface TaskService {

	/**
	 * Create a {@link TaskBuilder} for a task with a given name.
	 *
	 * @return new {@link TaskBuilder}
	 */
	TaskBuilder builder(String name);

	/**
	 * Create a {@link TaskBuilder} for a task with a given name.
	 *
	 * @return new {@link TaskBuilder}
	 */
	default TaskBuilder builder(Class<? extends TaskRunner> runner) {
		return builder(Objects.requireNonNull(runner.getName()));
	}

	/**
	 * Return a given task. Tasks are only guaranteed to be accessible if they
	 * were successfully submitted, and are not yet completed, canceled or
	 * failed.
	 */
	Optional<Task> getTask(String id);

	/**
	 * Return all currently enqueued or running tasks.
	 * 
	 * Some of the returned tasks may already be finished when this method
	 * returns, and tasks added after this method was called may or may not be not
	 * included. Only use this for monitoring or testing.
	 */
	List<Task> allTasks();

	/**
	 * Iterate over task IDs that are likely to return a value when passed to
	 * {@link #getTask(String)}. Tasks submitted, completed or failed while the
	 * stream exists, may or may not be included.
	 */
	Iterator<String> findTaskIDs();

}