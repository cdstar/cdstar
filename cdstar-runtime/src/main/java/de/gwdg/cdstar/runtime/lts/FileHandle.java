package de.gwdg.cdstar.runtime.lts;

import java.util.List;
import java.util.Map;

import de.gwdg.cdstar.runtime.client.CDStarFile;

/**
 * Read-only representation of an {@link CDStarFile}.
 */
public interface FileHandle {
	String getName();

	String getId();

	long getSize();

	String getType();

	Map<String, String> getDigests();

	boolean isLocal();

	Map<String, List<String>> getMetaAttributes();

}