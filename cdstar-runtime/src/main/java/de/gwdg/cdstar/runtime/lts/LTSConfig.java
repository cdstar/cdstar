package de.gwdg.cdstar.runtime.lts;

import java.util.Objects;
import java.util.Optional;

import de.gwdg.cdstar.runtime.client.CDStarProfile;

/**
 * LTS configuration for a specific {@link CDStarProfile}.
 */
public final class LTSConfig {
	public static final String MODE_COLD = "cold";
	public static final String MODE_HOT = "hot";
	public static final String PROP_NAME = "lts.name";
	public static final String PROP_MODE = "lts.mode";
	private final String name;
	private final String mode;

	LTSConfig(String name, String mode) {
		this.name = Objects.requireNonNull(name, "Name must not be null");
		this.mode = Objects.requireNonNull(mode, "Mode must not be null");
	}

	public String getName() {
		return name;
	}

	public boolean isCold() {
		return mode.equals(MODE_COLD);
	}

	@Override
	public String toString() {
		return name + "(" + mode + ")";
	}

	/**
	 * Read a {@link LTSConfig} from profile properties.
	 */
	public static Optional<LTSConfig> fromProfile(CDStarProfile profile) {
		if (profile.getProperty(PROP_NAME, null) != null) {
			String mode = profile.getProperty(PROP_MODE, MODE_HOT);
			mode = (mode == null || mode.equals(MODE_HOT))
				? MODE_HOT
				: MODE_COLD;
			return Optional.of(new LTSConfig(profile.getProperty(PROP_NAME), mode));
		}
		return Optional.empty();
	}

	/**
	 * Return true if the profile is a cold mirror.
	 */
	public static boolean isCold(CDStarProfile profile) {
		return profile.getProperty(PROP_NAME) != null
				&& profile.getProperty(PROP_MODE, MODE_HOT).equals(MODE_COLD);
	}
}