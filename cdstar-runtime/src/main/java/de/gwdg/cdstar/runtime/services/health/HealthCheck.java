package de.gwdg.cdstar.runtime.services.health;

@FunctionalInterface
public interface HealthCheck {
	HealthStatus checkHealth() throws Exception;
}