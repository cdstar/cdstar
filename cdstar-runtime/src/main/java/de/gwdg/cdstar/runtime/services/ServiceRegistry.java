package de.gwdg.cdstar.runtime.services;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Consumer;

import de.gwdg.cdstar.GromitIterable;

/**
 * Registry for all kinds of services that can be looked up by class or
 * interface. Multiple providers per class are supported.
 */
public class ServiceRegistry implements Iterable<Object> {

	public interface ServiceListener {
		/**
		 * Called once for each new service that is added to the
		 * {@link ServiceRegistry} (including itself and all services already
		 * installed). Adding or removing services from within this callback is
		 * allowed.
		 */
		default void serviceAdded(ServiceRegistry reg, Object service) throws Exception {
		}
	}

	private final GromitIterable<Object> instances = new GromitIterable<>();
	private final Map<Class<?>, List<?>> lookupCache = new ConcurrentHashMap<>();

	/**
	 * Type-save shortcut for {@link #addInstance(Object)};
	 */
	public boolean addListener(ServiceListener listener) {
		return addInstance(listener);
	}

	/**
	 * Add a new service and return true if the service was not already
	 * registered.
	 *
	 * {@link ServiceListener#serviceAdded(ServiceRegistry, Object)} is called
	 * on all installed listeners, including the new service just added if it
	 * implements the {@link ServiceListener} interface.
	 *
	 */
	public synchronized boolean addInstance(Object instance) {
		if (instances.contains(instance))
			return false;

		instances.add(instance);
		lookupCache.clear();

		try {
			// Notify a new ServiceListener about all already installed services
			// (including itself, excluding new services added recursively.
			// These are handled below)
			if (instance instanceof ServiceListener) {
				for (final Object other : lookupAll(Object.class)) {
					((ServiceListener) instance).serviceAdded(this, other);
				}
			}
			// Notify all installed ServiceListeners about a new service
			// (excluding itself, because that case was already handled above)
			for (final Object other : instances) {
				if (other instanceof ServiceListener && other != instance) {
					((ServiceListener) other).serviceAdded(this, instance);
				}
			}
		} catch (final Exception e) {
			throw new RuntimeException("Service listener failed while adding:" + instance, e);
		}

		return true;
	}

	/**
	 * Do stuff for each service implementing a given class, bypassing the cache
	 * and including services added while iterating.
	 */
	@SuppressWarnings("unchecked")
	<T> void forEachFiltered(Class<T> lookupClass, Consumer<T> consumer) {
		for (final Object service : instances) {
			if (lookupClass.isInstance(service)) {
				consumer.accept((T) service);
			}
		}
	}

	/**
	 * Request a service by interface or class. If multiple implementations are
	 * installed, return the first one added to the registry.
	 */
	public <T> Optional<T> lookup(Class<T> lookup) {
		final List<T> list = lookupAll(lookup);
		if (list.isEmpty())
			return Optional.empty();
		return Optional.of(list.get(0));
	}

	/**
	 * Request all instances that implement the given class, ordered by
	 * injection order.
	 */
	@SuppressWarnings("unchecked")
	public <T> List<T> lookupAll(Class<T> lookup) {
		return (List<T>) lookupCache.computeIfAbsent(lookup, lc -> {
			final List<T> result = new ArrayList<>();
			for (final Object service : instances) {
				if (lookup.isInstance(service)) {
					result.add((T) service);
				}
			}
			return Collections.unmodifiableList(result);
		});
	}

	@Override
	public Iterator<Object> iterator() {
		return lookupAll(Object.class).iterator();
	}

}
