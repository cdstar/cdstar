package de.gwdg.cdstar.runtime.search;

import de.gwdg.cdstar.Promise;

public interface SearchProvider {

	/**
	 * Perform a search.
	 *
	 * Implementations SHOULD abort the search if the promise is canceled, if
	 * possible.
	 *
	 * The search can fail with a {@link SearchException} with a human readable
	 * message, or a more detailed {@link ErrorResponse} which is then returned
	 * directly to the client.
	 */
	Promise<SearchResult> search(SearchQuery q);

}