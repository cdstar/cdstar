package de.gwdg.cdstar.runtime.client;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

class AttributeImpl implements CDStarAttribute {
	private static final String META_SEP = ":";
	private static final String META_PREFIX = "meta" + META_SEP;

	private List<String> newValues;
	private final List<String> origValues;
	private final String name;
	private final AttributeCache parent;
	private final CDStarFile file;

	public AttributeImpl(AttributeCache parent, CDStarFile file, String name, List<String> values) {
		this.parent = parent;
		this.file = file;
		this.name = name;
		origValues = values;
	}

	List<String> getOrigValues() {
		return origValues;
	}

	CDStarFile getFile() {
		return file;
	}

	@Override
	public List<String> values() {
		return Collections.unmodifiableList(newValues != null ? newValues : origValues);
	}

	@Override
	public void set(List<String> values) {
		final List<String> copy = new ArrayList<>(values);
		copy.removeIf(v -> v == null);

		if (copy.equals(newValues))
			return;

		newValues = copy;
		parent.setModifed(this);
	}

	boolean isModified() {
		return newValues != null;
	}

	/**
	 * Split a fully qualified parameter name into namespace and local name. If
	 * no namespace is given, the "default" namespace is used.
	 *
	 * @param fullId
	 *            Fully qualified field name.
	 * @return A string array with two elements: Namespace and fieldName.
	 */
	public static String[] splitFieldName(String fullId) {
		final int i = fullId.indexOf(META_SEP);
		if (i > 0 && i + 1 < fullId.length())
			return new String[] { fullId.substring(0, i), fullId.substring(i + 1) };
		return new String[] { "default", fullId };
	}

	static boolean isPropertyName(String name) {
		return name.startsWith(META_PREFIX);
	}

	@Override
	public String getName() {
		return name;
	}


}
