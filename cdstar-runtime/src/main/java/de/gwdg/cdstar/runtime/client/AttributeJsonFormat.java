package de.gwdg.cdstar.runtime.client;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;

/**
 * Helper class to parse or serialize between a json document and a tree of
 * attributes.
 */
abstract class AttributeJsonFormat<AttrType> {

	public abstract AttrType makeAttr(String fileId, String attrName, List<String> values);

	public abstract List<String> getValues(AttrType attr);

	public void serialize(JsonGenerator gen, Map<String, Map<String, AttrType>> data) throws IOException {

		gen.writeStartObject();
		for (final Entry<String, Map<String, AttrType>> fileEntry : data.entrySet()) {
			writeFileEntry(gen, fileEntry);
		}
		gen.writeEndObject();
		gen.close();
	}

	private void writeFileEntry(JsonGenerator gen, final Entry<String, Map<String, AttrType>> fileEntry)
			throws IOException {

		// Skip files with no attributes
		if (fileEntry.getValue().isEmpty())
			return;

		// Delay writing the file entry until we actually find a non-empty
		// attribute in it
		boolean fileEntryWritten = false;

		for (final Entry<String, AttrType> attrEntry : fileEntry.getValue().entrySet()) {
			final List<String> values = getValues(attrEntry.getValue());

			// Skip attributes with no values
			if (values.isEmpty())
				continue;

			if (!fileEntryWritten) {
				gen.writeObjectFieldStart(fileEntry.getKey());
				fileEntryWritten = true;
			}

			gen.writeArrayFieldStart(attrEntry.getKey());
			for (final String v : values) {
				gen.writeString(v);
			}
			gen.writeEndArray();
		}

		if (fileEntryWritten)
			gen.writeEndObject();
	}

	public Map<String, Map<String, AttrType>> parse(JsonParser parser)
			throws JsonParseException, IOException {
		final Map<String, Map<String, AttrType>> result = new HashMap<>();
		readJson(parser, result);
		return result;
	}

	private void readJson(JsonParser parser, Map<String, Map<String, AttrType>> result) throws IOException {
		consumeToken(parser, JsonToken.START_OBJECT);
		String fileId;
		while ((fileId = parser.nextFieldName()) != null) {
			final Map<String, AttrType> attrMap = result.computeIfAbsent(fileId, f -> new HashMap<>());
			readAttrMap(parser, fileId, attrMap);
		}
		assertCurrentToken(parser, JsonToken.END_OBJECT);
	}

	private void readAttrMap(JsonParser parser, String fileId, final Map<String, AttrType> attrMap)
			throws IOException {
		consumeToken(parser, JsonToken.START_OBJECT);
		String attrName;
		while ((attrName = parser.nextFieldName()) != null) {
			// Start with a very small ArrayList as most attributes only have a
			// single value.
			final List<String> attrList = new ArrayList<>(1);
			readAttrList(parser, attrList);
			attrMap.put(attrName, makeAttr(fileId, attrName, attrList));
		}
		assertCurrentToken(parser, JsonToken.END_OBJECT);
	}

	private void readAttrList(JsonParser parser, final List<String> attrList) throws IOException {
		consumeToken(parser, JsonToken.START_ARRAY);
		while (parser.nextToken() == JsonToken.VALUE_STRING)
			attrList.add(parser.getText());
		assertCurrentToken(parser, JsonToken.END_ARRAY);
	}

	private void consumeToken(JsonParser parser, JsonToken token) throws IOException {
		parser.nextToken();
		assertCurrentToken(parser, token);
	}

	private void assertCurrentToken(JsonParser parser, JsonToken token) throws IOException {
		if (parser.getCurrentToken() != token) {
			String msg = "Error in json document. Expected token " + token + " but got " + parser.getCurrentToken();
			msg += " (" + parser.getCurrentLocation().getLineNr() + ":" + parser.getCurrentLocation().getColumnNr()
					+ ")";
			throw new IOException(msg);
		}
	}

}
