package de.gwdg.cdstar.runtime.services.health;

import java.time.Instant;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import de.gwdg.cdstar.Utils;

public class HealthMonitor {
	private final HealthCheck check;
	final String name;
	private long interval;
	private long cooldown;

	private boolean lazy = true;
	private ScheduledFuture<?> autoCheckFuture;

	private Instant lastCheck;
	private HealthStatus lastResult;
	private ScheduledExecutorService cron;

	public HealthMonitor(String name, HealthCheck check) {
		this.name = name;
		this.check = check;
		interval(10, TimeUnit.SECONDS);
		cooldown(10, TimeUnit.SECONDS);
		lazy(true);
	}

	public String getName() {
		return name;
	}

	/**
	 * If true (default), checks are performed only if requested. Otherwise,
	 * they are performed automatically based on the interval setting.
	 */
	public synchronized HealthMonitor lazy(boolean lazy) {
		this.lazy = lazy;
		reschedule();
		return this;
	}

	public boolean isLazy() {
		return lazy;
	}

	/**
	 * Time to wait between checks (default: 10 Seconds)
	 */
	public HealthMonitor interval(long interval, TimeUnit unit) {
		this.interval = Math.max(100, unit.toMillis(interval));
		cooldown = Math.max(interval, cooldown);
		reschedule();
		return this;
	}

	public long getInterval() {
		return interval;
	}

	/**
	 * Time to wait after a check returned {@link HealthLevel#ERROR}, raised an
	 * exception or returned nothing at all (default: same as interval)
	 */
	public HealthMonitor cooldown(long cooldown, TimeUnit unit) {
		this.cooldown = Math.max(100, unit.toMillis(cooldown));
		reschedule();
		return this;
	}

	public long getCooldown() {
		return cooldown;
	}

	/**
	 * Perform health check or return the last cached value.
	 */
	public synchronized HealthStatus check() {
		if (lastResult == null)
			performCheck();
		else if (lazy && lastCheck.isBefore(Instant.now().minusMillis(getMaxAgeMillis())))
			performCheck();
		return lastResult;
	}

	void setCron(ScheduledExecutorService cronService) {
		if (cron != null)
			throw new IllegalStateException("Already started");
		cron = cronService;
		reschedule();
	}

	private void reschedule() {
		if (autoCheckFuture != null)
			autoCheckFuture.cancel(false);

		if (!lazy)
			autoCheckFuture = cron.scheduleWithFixedDelay(this::performCheck, getMaxAgeMillis(),
				getMaxAgeMillis(), TimeUnit.MILLISECONDS);
	}

	long getMaxAgeMillis() {
		return lastResult == null || lastResult.health == HealthLevel.ERROR ? cooldown : interval;
	}

	private synchronized void performCheck() {
		HealthStatus result;
		try {
			result = check.checkHealth();
		} catch (final Exception e) {
			result = HealthStatus.error("Check failed with an exception", e);
		}

		switch (result.getHealth()) {
		case OK:
			HealthService.log.debug("{} {}", name, result);
			break;
		case WARN:
			HealthService.log.warn("{} {}", name, result);
			break;
		case ERROR:
			HealthService.log.error("{} {}", name, result, result.getError());
			break;
		default:
			break;
		}

		lastCheck = Instant.now();
		lastResult = result;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder();
		sb.append("Monitor(");
		sb.append("name=").append(getName()).append(", ");
		sb.append("interval=").append(getInterval()).append(", ");
		sb.append("cooldown=").append(getCooldown()).append(", ");
		sb.append("lazy=").append(isLazy()).append(", ");
		sb.append("status=").append(Utils.repr(lastResult));
		sb.append(")");
		return sb.toString();
	}
}