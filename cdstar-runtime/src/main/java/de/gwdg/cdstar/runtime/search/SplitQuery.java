package de.gwdg.cdstar.runtime.search;

import java.util.ArrayList;
import java.util.List;

/**
 * Search queries may contain special control markers as top-level tokens, for
 * example to change the default result ordering, select a specific
 * {@link SearchProvider} or change other aspects of the search. These markers
 * must be reliably detected and removed from the actual query before it is
 * passed to the {@link SearchProvider}.
 *
 * Markers should only be considered at the top-level, and ignored if they
 * appear within quoted strings or parenthesis, or as part of larger tokens.
 *
 * This implementation splits the query string into whitespace-separated tokens,
 * but does not split within quotes or parenthesis. Nested parenthesis are
 * supported. Nested quotes (of a different kind) or parenthesis within quotes
 * are ignored. Escaped whitespace, quotes or parenthesis are also ignored. The
 * escape character itself is preserved and not removed from the token.
 *
 */
public class SplitQuery {
	private final String q;
	private List<String> qsplit;

	public SplitQuery(String query) {
		q = query;
	}

	/**
	 * Split the query string into whitespace separated tokens, ignoring
	 * whitespace between quotes or parenthesis. Characters can be escaped with
	 * a backslash.
	 *
	 * @throws IndexOutOfBoundsException if the parenthesis depth exceeds 128
	 */
	public List<String> split() {
		if (qsplit != null)
			return qsplit;

		final List<String> results = new ArrayList<>();
		final char[] pStack = new char[128];
		int pDepth = 0;
		int start = 0;
		char inQuote = 0;
		for (int i = 0; i < q.length(); i++) {
			final char c = q.charAt(i);
			if (c == '\\') {
				// Escape -> ignore the next char
				i++;
			} else if (inQuote > 0) {
				// Within quotes, look for the closing quote.
				if (c == inQuote)
					inQuote = 0;
			} else if (c == '\'' || c == '"') {
				inQuote = c;
			} else if (c == '(') {
				pStack[++pDepth] = ')';
			} else if (c == '[') {
				pStack[++pDepth] = ']';
			} else if (c == '{') {
				pStack[++pDepth] = '}';
			} else if (pDepth > 0) {
				// Within parenthesis, look for the closing parenthesis
				if (c == pStack[pDepth])
					pDepth--;
			} else if (c == ' ' || c == '\t' || c == '\n') {
				// Whitespace ends a token
				if (start < i)
					results.add(q.substring(start, i));
				start = i + 1; // Skip (repeated) whitespace
			}
			// Everything else is just a normal char
		}
		if (start < q.length())
			results.add(q.substring(start));

		qsplit = results;
		return qsplit;

	}

}
