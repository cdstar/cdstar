package de.gwdg.cdstar.runtime.lts;

class ArchiveReferenceImpl implements ArchiveReference {

	private String vault;
	private String archive;
	private String revision;

	public ArchiveReferenceImpl(String vault, String archive, String revision) {
		this.vault=vault;
		this.archive=archive;
		this.revision=revision;
	}

	@Override
	public String getVault() {
		return vault;
	}

	@Override
	public String getId() {
		return archive;
	}

	@Override
	public String getRevision() {
		return revision;
	}

}