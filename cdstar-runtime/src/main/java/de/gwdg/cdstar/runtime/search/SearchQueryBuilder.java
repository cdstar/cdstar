package de.gwdg.cdstar.runtime.search;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class SearchQueryBuilder implements SearchQuery {

	private final Set<String> groups = new HashSet<>();
	private String principal;
	private String query;
	private final List<String> order = new ArrayList<>();
	private String scrollId;
	private String vault;
	private int limit;
	private List<String> fields = new ArrayList<>();

	public SearchQueryBuilder group(String group) {
		groups.add(group);
		return this;
	}

	@Override
	public Set<String> getGroups() {
		return Collections.unmodifiableSet(groups);
	}

	public SearchQueryBuilder principal(String principal) {
		this.principal = principal;
		return this;
	}

	@Override
	public String getPrincipal() {
		return principal;
	}

	public SearchQueryBuilder query(String query) {
		this.query = query;
		return this;
	}

	@Override
	public String getQuery() {
		return query;
	}

	public SearchQueryBuilder resetOrder() {
		order.clear();
		return this;
	}

	public SearchQueryBuilder order(String... order) {
		for (final String o : order)
			this.order.add(o);
		return this;
	}

	@Override
	public List<String> getOrder() {
		return Collections.unmodifiableList(order);
	}

	public SearchQueryBuilder scrollId(String scrollId) {
		this.scrollId = scrollId;
		return this;
	}

	@Override
	public String getScrollId() {
		return scrollId;
	}

	public SearchQueryBuilder vault(String vault) {
		this.vault = vault;
		return this;
	}

	@Override
	public String getVault() {
		return vault;
	}

	public SearchQueryBuilder limit(int limit) {
		this.limit = limit;
		return this;
	}

	@Override
	public int getLimit() {
		return limit;
	}

	public SearchQueryBuilder resetFields() {
		fields.clear();
		return this;
	}

	public SearchQueryBuilder field(String name) {
		if(!fields.contains(name));
			fields.add(name);
		return this;
	}

	public SearchQueryBuilder fields(String ... fields) {
		for(var field: fields)
			field(field);
		return this;
	}

	@Override
	public List<String> getFields() {
		return List.copyOf(fields);
	}

	/**
	 * Returns a {@link SearchQuery} based on the current state of the builder.
	 */
	public SearchQuery build() {
		final SearchQueryBuilder copy = new SearchQueryBuilder();
		copy.groups.addAll(groups);
		copy.limit = limit;
		copy.order.addAll(order);
		copy.principal = principal;
		copy.query = query;
		copy.scrollId = scrollId;
		copy.vault = vault;
		copy.fields.addAll(fields);
		return copy;
	}

}
