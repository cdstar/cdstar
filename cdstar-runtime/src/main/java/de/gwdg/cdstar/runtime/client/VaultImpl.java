package de.gwdg.cdstar.runtime.client;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.StreamSupport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.gwdg.cdstar.GromitIterable;
import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.auth.StringPermission;
import de.gwdg.cdstar.auth.Subject;
import de.gwdg.cdstar.pool.PoolError;
import de.gwdg.cdstar.pool.PoolError.NotFound;
import de.gwdg.cdstar.pool.StorageObject;
import de.gwdg.cdstar.pool.StoragePool;
import de.gwdg.cdstar.pool.StorageSession;
import de.gwdg.cdstar.runtime.VaultConfig;
import de.gwdg.cdstar.runtime.client.auth.ArchivePermission;
import de.gwdg.cdstar.runtime.client.auth.VaultPermission;
import de.gwdg.cdstar.runtime.client.exc.ArchiveNotFound;
import de.gwdg.cdstar.runtime.client.exc.ConstraintError;
import de.gwdg.cdstar.runtime.listener.VaultListener;
import de.gwdg.cdstar.runtime.profiles.ProfileRegistry;

class VaultImpl implements CDStarVault {
	private static final Logger log = LoggerFactory.getLogger(VaultImpl.class);

	private final VaultConfig vaultConfig;
	private final SessionImpl session;
	private final GromitIterable<VaultListener> listeners = new GromitIterable<>();

	private final Map<String, ArchiveImpl> openedArchives = new HashMap<>();
	private final Optional<ProfileRegistry> profileRegistry;

	private final StoragePool pool;

	private final StorageSession poolSess;

	public VaultImpl(SessionImpl session, VaultConfig vc, StoragePool pool) {
		this.session = session;
		vaultConfig = vc;
		this.pool = pool;
		poolSess = pool.open(session.getTx());
		profileRegistry = session.getRuntime().lookup(ProfileRegistry.class);
	}

	@Override
	public CDStarSession getSession() {
		return session;
	}

	@Override
	public CDStarArchive createArchive() {
		checkPermission(VaultPermission.CREATE);
		return openAndRemember(poolSess.createObject());
	}

	@Override
	public CDStarArchive createArchive(String preDefinedId) {
		Utils.notNull(preDefinedId);
		checkPermission(VaultPermission.CREATE);
		checkPermission(VaultPermission.CREATE_ID);
		try {
			return openAndRemember(poolSess.createObject(preDefinedId));
		} catch (final PoolError.InvalidObjectId e) {
			throw new ConstraintError("Invalid or existing archive ID", e)
				.detail("id", preDefinedId)
				.detail("help", e.getMessage());
		}
	}

	@Override
	public CDStarArchive loadArchive(String id) throws ArchiveNotFound {
		Utils.notNull(id);

		if (!isPublic())
			checkPermission(VaultPermission.READ);

		synchronized (openedArchives) {
			if (openedArchives.containsKey(id))
				return openedArchives.get(id);
		}

		StorageObject sto;
		try {
			sto = poolSess.readObject(id);
		} catch (final NotFound e) {
			throw new ArchiveNotFound(getName(), id);
		}

		if (sto.isRemoved()) {
			throw new ArchiveNotFound(getName(), sto.getId());
		} else if (isTombstone(sto)) {
			// Tombstones do not have a public API yet and are useless. This is
			// just here for backwards compatibility.
			log.warn("Archive tombstone found: {}/{} (delete manually)", getName(), id);
			throw new ArchiveNotFound(getName(), id);
		}

		return openAndRemember(sto);
	}

	StorageObject createDirect() {
		return poolSess.createObject();
	}

	StorageObject loadDirect(String ref) {
		return poolSess.readObject(ref);
	}

	@SuppressWarnings("deprecation")
	private boolean isTombstone(StorageObject sto) {
		return sto.getProperty(ArchiveImpl.PROP_TOMBSTONE) != null;
	}

	private ArchiveImpl openAndRemember(StorageObject sto) {
		final ArchiveImpl a = new ArchiveImpl(this, sto);
		listeners.forEach(l -> l.archiveLoaded(a));
		synchronized (openedArchives) {
			openedArchives.put(a.getId(), a);
		}
		return a;
	}

	@Override
	public Iterable<String> getArchiveIterable(String scanOffset, boolean strict) {
		if (!(isPublic() || strict))
			checkPermission(VaultPermission.LIST);
		final Iterable<String> idIterator = pool.getObjectIDs(scanOffset);
		if (!strict)
			return idIterator;

		final Subject subject = getSession().getClient().getSubject();
		return () -> StreamSupport.stream(idIterator.spliterator(), false).filter(id -> {
			try {
				final StorageObject obj = poolSess.getPool().loadObjectDirect(id, null);
				if(obj.isRemoved())
					return false;
				final AclChecker access = new AclChecker(getName(), id, subject,
					PropertyHelper.getOwner(obj), PropertyHelper.getAclMap(obj));
				return access.isPermitted(ArchivePermission.LOAD)
					|| subject.isPermitted(ArchivePermission.LOAD.toStringPermission(getName(), id));
			} catch (final NotFound e) {
				return false;
			}
		}).iterator();
	}

	@Override
	public String getName() {
		return vaultConfig.getName();
	}

	@Override
	public void addListener(VaultListener listener) {
		listeners.add(listener);
	}

	@Override
	public Collection<CDStarArchive> listOpenedArchives() {
		synchronized (openedArchives) {
			return new ArrayList<>(openedArchives.values());
		}
	}

	@Override
	public boolean isPublic() {
		return vaultConfig.isPublic();
	}

	void checkPermission(VaultPermission perm) {
		session.client.checkPermission(StringPermission.ofParts("vault", getName(), perm.name().toLowerCase()));
	}

	public static boolean isVisible(VaultConfig v, ClientImpl client) {
		return v.isPublic() || client
			.isPermitted(StringPermission.ofParts("vault", v.getName(), VaultPermission.READ.name().toLowerCase()));
	}

	@Override
	public List<CDStarProfile> getProfiles() {
		return profileRegistry
			.map(pr -> pr.getProfilesByVault(getName(), true))
			.orElseGet(Collections::emptyList);
	}

}
