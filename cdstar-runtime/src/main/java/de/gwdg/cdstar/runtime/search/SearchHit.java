package de.gwdg.cdstar.runtime.search;

import java.util.Map;

import com.fasterxml.jackson.databind.JsonNode;

public interface SearchHit {

	String getId();

	String getType();

	/**
	 * File name (if {@link #getType()} returns 'file'}
	 */
	String getName();

	double getScore();

	Map<String, JsonNode> getFields();

}
