package de.gwdg.cdstar.runtime.services.health;

import java.util.concurrent.ScheduledExecutorService;

import de.gwdg.cdstar.runtime.Plugin;
import de.gwdg.cdstar.runtime.RuntimeContext;
import de.gwdg.cdstar.runtime.listener.RuntimeListener;

@Plugin(name = "health")
public class HealthMonitorFeature implements RuntimeListener {

	private HealthService hs;

	@Override
	public void onInit(RuntimeContext ctx) throws Exception {
		hs = new HealthService();
		ctx.register(hs);
	}

	@Override
	public void onStartup(RuntimeContext ctx) throws Exception {
		hs.setCron(ctx.lookupRequired(ScheduledExecutorService.class));
	}

}
