package de.gwdg.cdstar.runtime.client;

import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Consumer;

import de.gwdg.cdstar.GromitIterable;
import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.auth.Subject;
import de.gwdg.cdstar.pool.BackendError;
import de.gwdg.cdstar.pool.Resource;
import de.gwdg.cdstar.pool.StorageObject;
import de.gwdg.cdstar.runtime.CDStarRuntime;
import de.gwdg.cdstar.runtime.client.auth.ArchivePermission;
import de.gwdg.cdstar.runtime.client.auth.ArchivePermissionSet;
import de.gwdg.cdstar.runtime.client.auth.StringSubject;
import de.gwdg.cdstar.runtime.client.auth.StringSubject.PrincipalSubject;
import de.gwdg.cdstar.runtime.client.exc.AccessError;
import de.gwdg.cdstar.runtime.client.exc.ArchiveLocked;
import de.gwdg.cdstar.runtime.client.exc.ArchiveNotAvailable;
import de.gwdg.cdstar.runtime.client.exc.FileExists;
import de.gwdg.cdstar.runtime.client.exc.FileNotFound;
import de.gwdg.cdstar.runtime.client.exc.InvalidFileName;
import de.gwdg.cdstar.runtime.client.exc.InvalidSnapshotName;
import de.gwdg.cdstar.runtime.client.exc.ProfileNotDefined;
import de.gwdg.cdstar.runtime.listener.ArchiveListener;
import de.gwdg.cdstar.runtime.listener.SessionListener;

class ArchiveImpl implements CDStarArchive {

	static final String MIME_TYPE = "application/x-cdstar;v=3";

	static final String PROP_NS = "cdstar:";
	static final String PROP_SYS_NS = "sys:";
	static final String PROP_CONTENT_MODIFIED = PROP_NS + "mtime";
	static final String PROP_REV = PROP_NS + "rev";
	static final String PROP_BY = PROP_NS + "by";

	@Deprecated
	static final String PROP_TOMBSTONE = PROP_NS + "tombstone";

	static final String PROP_PROFILE = PROP_NS + "profile";

	static final String PROP_OWNER = PROP_NS + "owner";
	static final String PROP_ACL_NS = PROP_NS + "acl:";
	static final String PROP_ACL_ANY = PROP_ACL_NS + StringSubject.SpecialSubject.ANY.toString();
	static final String PROP_ACL_OWNER = PROP_ACL_NS + StringSubject.SpecialSubject.OWNER.toString();
	static final String PROP_ACL_USER = PROP_ACL_NS + StringSubject.SpecialSubject.USER.toString();
	static final String PROP_ACL_GROUP_PREFIX = PROP_ACL_NS + "@";

	static final String PROP_LTS_NAME = ArchiveImpl.PROP_NS + "lts:name";
	static final String PROP_LTS_HINT = ArchiveImpl.PROP_NS + "lts:location";
	static final String PROP_LTS_TASK = ArchiveImpl.PROP_NS + "lts:task";

	static final String USERFILE_PREFIX = "data/";
	static final String METAFILE = "metadata.json";
	static final String METAFILE_TYPE = "application/x-cdstar-meta;v=3";

	private final StorageObject sto;
	private final VaultImpl vault;
	private final AclChecker acl;
	private final GromitIterable<ArchiveListener> listeners = new GromitIterable<>();

	private boolean isModified = false;
	private boolean isContentModified = false;
	private boolean flushed = false;

	// Lazily created wrappers for complex sub-structures
	private List<FileImpl> filesCache;
	private AttributeCache metaCache;
	private AclImpl aclCache;
	private MirrorStateImpl xMirrorCache;
	private SnapshotList xSnapshots;

	// Simple STO parameters
	private Date xContentModified;
	private int xRevision;
	private CDStarProfile xProfile;
	private PrincipalSubject xOwner;

	/**
	 * Create a new archive (empty storage object) or load an existing archive
	 * within this session.
	 *
	 * @param vault the containing vault.
	 * @param sto the storage object storing this archive.
	 */
	public ArchiveImpl(VaultImpl vault, StorageObject sto) {
		this.vault = vault;
		this.sto = sto;

		final Subject subject = vault.getSession().getClient().getSubject();

		if (sto.getType() == null) {
			sto.setType(MIME_TYPE);

			isModified = isContentModified = true;

			// Set start values for important properties
			xContentModified = new Date();
			xRevision = -1;
			xProfile = loadProfile(CDStarProfile.DEFAULT_NAME);

			// Apply owner and basic permissions so the archive creator can
			// actually access it
			PropertyHelper.setOwner(sto, new PrincipalSubject(
				subject.getPrincipal().getId(),
				subject.getPrincipal().getDomain()));
			PropertyHelper.setAclEntry(sto,
				StringSubject.SpecialSubject.OWNER,
				ArchivePermissionSet.OWNER.getPermissions(),
				true);

		} else if (Utils.equal(sto.getType(), MIME_TYPE)) {

			try {
				xContentModified = PropertyHelper.getMtime(sto);
				xRevision = PropertyHelper.getRevision(sto);
			} catch (DateTimeParseException | NumberFormatException | NullPointerException e) {
				throw new BackendError("Missing or invalid properties in source document: " + this, e);
			}
			isContentModified = false;

		} else {
			throw new BackendError("StorageObject mime-type mismatch: " + sto.getType());
		}

		acl = new AclChecker(getVault().getName(), getId(), subject, PropertyHelper.getOwner(sto),
			PropertyHelper.getAclMap(sto));
		xOwner = acl.getOwner();

		checkPermission(ArchivePermission.LOAD);

		getSession().addListener(new SessionListener() {
			@Override
			public void onPrepare(CDStarSession session) {
				flush();
			}
		});
	}

	/**
	 * Return a profile by name, or fall back to the default profile (and warn).
	 * Only used in constructor.
	 */
	CDStarProfile loadProfile(String name) {
		try {
			return vault.getProfileByName(name);
		} catch (final ProfileNotDefined e) {
			CDStarRuntime.log.warn(
				"Archive {}/{} references unknown profile {}. Falling back to {}.",
				vault.getName(), getId(), name, CDStarProfile.DEFAULT_NAME);
			try {
				return vault.getProfileByName(CDStarProfile.DEFAULT_NAME);
			} catch (final ProfileNotDefined e2) {
				throw new BackendError("Default profile not defined.", e2);
			}
		}
	}

	@Override
	public synchronized void addListener(ArchiveListener listener) {
		listeners.add(listener);
	}

	void forEachListener(Consumer<? super ArchiveListener> handler) {
		listeners.forEach(handler);
	}

	private synchronized void flush() {

		if (isModified && !flushed) {
			flushed = true;

			if (filesCache != null)
				filesCache.forEach(FileImpl::flush);

			if (metaCache != null)
				metaCache.flush();

			if (aclCache != null)
				aclCache.saveChanges(sto);

			if (xOwner != null)
				PropertyHelper.setOwner(sto, xOwner);

			if (xProfile != null)
				PropertyHelper.setProfile(sto, xProfile);

			if (isContentModified) {
				xContentModified = new Date();
				PropertyHelper.setMTime(sto, xContentModified);
				PropertyHelper.setRevision(sto, xRevision + 1);
			}

			if (xSnapshots != null)
				xSnapshots.flush();

			final Subject who = getSession().getClient().getSubject();
			if (!who.isAnonymous())
				PropertyHelper.setLastModifiedBy(sto, new PrincipalSubject(
					who.getPrincipal().getId(), who.getPrincipal().getDomain()));
		}
	}

	void checkPermission(ArchivePermission check) {
		if (acl.isPermitted(check))
			return;
		throw new AccessError(check.toStringPermission(getVault().getName(), getId()));
	}

	void markContentModified() {
		markModified();
		isContentModified = true;
	}

	void markModified() {
		isModified = true;
	}

	void requireTransactionAlive() {
		if (getSession().isClosed())
			throw new IllegalStateException("Archive already prepared.");
	}

	void requireTransactionWriteable() {
		requireTransactionAlive();
		if (flushed || getSession().isReadOnly())
			throw new IllegalStateException("Archive opened in read-only mode.");
	}

	void requirePayloadAvailable() {
		requireTransactionAlive();

		if (!isAvailable())
			throw new ArchiveNotAvailable(vault.getName(), getId());
	}

	void requirePayloadWriteable() {
		requireTransactionWriteable();

		if (isLocked())
			throw new ArchiveLocked(vault.getName(), getId());
		if (!isAvailable())
			throw new ArchiveNotAvailable(vault.getName(), getId());
	}

	@Override
	public CDStarVault getVault() {
		return vault;
	}

	@Override
	public String getId() {
		return sto.getId();
	}

	@Override
	public String getRev() {
		return xRevision > -1 ? String.valueOf(xRevision) : getNextRev();
	}

	@Override
	public String getParentRev() {
		return xRevision > 0 ? String.valueOf(xRevision - 1) : null;
	}

	@Override
	public String getNextRev() {
		return String.valueOf(isContentModified ? xRevision + 1 : xRevision);
	}

	@Override
	public synchronized CDStarProfile getProfile() {
		if (xProfile == null)
			xProfile = loadProfile(sto.getProperty(PROP_PROFILE, CDStarProfile.DEFAULT_NAME));
		return xProfile;
	}

	@Override
	public void setProfile(CDStarProfile newProfile) {
		Objects.requireNonNull(newProfile);

		requireTransactionWriteable();
		checkPermission(ArchivePermission.CHANGE_PROFILE);

		try {
			newProfile = vault.getProfileByName(newProfile.getName());
		} catch (final ProfileNotDefined e) {
			throw new IllegalArgumentException("Profile not defined for the given vault", e);
		}

		final CDStarProfile oldProfile = getProfile();
		if (!oldProfile.equals(newProfile)) {
			xProfile = newProfile;
			markModified();
			listeners.forEach(l -> l.profileChanged(this, oldProfile));
		}
	}

	@Override
	public boolean isLocked() {
		return getMirrorState().isMirrored();
	}

	@Override
	public boolean isAvailable() {
		return getMirrorState().isAvailable();
	}

	@Override
	public synchronized CDStarMirrorState getMirrorState() {
		if (xMirrorCache == null)
			xMirrorCache = new MirrorStateImpl(this, null);
		return xMirrorCache;
	}

	@Override
	public CDStarSession getSession() {
		return vault.getSession();
	}

	synchronized List<FileImpl> getInternalFileList() {
		if (filesCache == null) {
			filesCache = new ArrayList<>();
			for (final Resource r : sto.getResourcesByPrefix(FileImpl.RESOURCE_PREFIX)) {
				String name = r.getName();
				name = name.substring(FileImpl.RESOURCE_PREFIX.length());
				filesCache.add(new FileImpl(this, r, name, null));
			}
		}
		return filesCache;
	}

	Optional<FileImpl> getInternalFileById(String fileId) {
		for (FileImpl file : getInternalFileList())
			if (fileId.equals(file.getID()))
				return Optional.of(file);
		return Optional.empty();
	}

	Optional<FileImpl> getInternalFileByName(String fileName) {
		for (FileImpl file : getInternalFileList())
			if (fileName.equals(file.getName()))
				return Optional.of(file);
		return Optional.empty();
	}

	synchronized AttributeCache getMeta() {
		checkPermission(ArchivePermission.READ_META);
		return getMetaNoCheck();
	}

	private synchronized AttributeCache getMetaNoCheck() {
		if (metaCache == null)
			metaCache = new AttributeCache(this, null);
		return metaCache;
	}

	@Override
	public List<CDStarFile> getFiles() {
		checkPermission(ArchivePermission.LIST_FILES);
		return Collections.unmodifiableList(getInternalFileList());
	}

	@Override
	public int getFileCount() {
		return getInternalFileList().size();
	}

	@Override
	public boolean hasFile(String name) {
		checkPermission(ArchivePermission.LIST_FILES);
		for (final CDStarFile file : getInternalFileList()) {
			if (Utils.equalNotNull(file.getName(), name))
				return true;
		}
		return false;
	}

	@Override
	public CDStarFile getFile(String name) throws FileNotFound {
		checkPermission(ArchivePermission.LIST_FILES);
		for (final CDStarFile file : getInternalFileList()) {
			if (Utils.equalNotNull(file.getName(), name))
				return file;
		}
		throw new FileNotFound(getVault().getName(), getId(), name);
	}

	Resource getResource(String name) {
		return sto.getResource(name);
	}

	Resource createResource(String name) {
		return sto.createResource(name);
	}

	@Override
	public synchronized CDStarFile createFile(String name, String mediaType)
		throws FileExists, InvalidFileName {
		requireTransactionWriteable();
		requirePayloadWriteable();
		checkPermission(ArchivePermission.CHANGE_FILES);

		getInternalFileList(); // Triggers discovery of existing files.

		final Resource r = sto.createResource(null);
		final FileImpl file = new FileImpl(this, r, null, null);
		try {
			file.setName(name);
			file.setMediaType(mediaType);
			filesCache.add(file);
			markContentModified();
			listeners.forEach(l -> l.fileCreated(file));
			return file;
		} catch (FileExists | InvalidFileName e) {
			filesCache.remove(file);
			r.remove();
			throw e;
		}
	}

	synchronized void removeFile(FileImpl file) {
		requireTransactionWriteable();
		requirePayloadWriteable();
		checkPermission(ArchivePermission.CHANGE_FILES);

		// Must be loaded before file is removed, or it will complain about references
		// to missing files.
		getMetaNoCheck();
		if (filesCache.remove(file)) {
			getMetaNoCheck().onFileRemoved(file);
			file.resource.remove();
			markContentModified();
		}
	}

	@Override
	public Date getCreated() {
		return sto.getCreated();
	}

	@Override
	public Date getModified() {
		return sto.getLastModified();
	}

	@Override
	public Date getContentModified() {
		return xContentModified;
	}

	@Override
	public boolean isModified() {
		return isModified;
	}

	@Override
	public boolean isContentModified() {
		return isContentModified;
	}

	@Override
	public String getOwner() {
		if (xOwner == null)
			xOwner = acl.getOwner();
		return xOwner.toString();
	}

	@Override
	public void setOwner(String owner) {
		requireTransactionWriteable();
		checkPermission(ArchivePermission.CHANGE_OWNER);
		markModified();
		String oldOwner = getOwner();
		xOwner = new StringSubject.PrincipalSubject(owner);
		forEachListener(l -> l.ownerChanged(this, oldOwner));
	}

	/**
	 * Mark this archive for deletion. This cannot be undone.
	 */
	@Override
	public void remove() {
		requireTransactionWriteable();
		// requirePayloadWriteable(); <-- Deleting archived archives is allowed.
		checkPermission(ArchivePermission.DELETE);

		getSnapshots().forEach(s -> s.remove());
		sto.remove();

		markContentModified();
		listeners.forEach(l -> l.archiveRemoved(this));
	}

	@Override
	public boolean isRemoved() {
		return sto.isRemoved();

	}

	@Override
	public synchronized CDStarACL getACL() {
		checkPermission(ArchivePermission.READ_ACL);
		if (aclCache == null)
			aclCache = new AclImpl(this, acl.getAclMap());
		return aclCache;
	}

	@Override
	public CDStarAttribute getAttribute(String name) {
		return getMeta().getAttribute(null, name);
	}

	@Override
	public Set<CDStarAttribute> getAttributes() {
		return getMeta().getAttributes(null);
	}

	@Override
	public String getProperty(String name) {
		return getRawProperty(PROP_SYS_NS + name);
	}

	@Override
	public void setProperty(String name, String value) {
		requireTransactionWriteable();
		markModified();
		setRawProperty(PROP_SYS_NS + name, value);
	}

	String getRawProperty(String name) {
		return sto.getProperty(name);
	}

	void setRawProperty(String name, String value) {
		sto.setProperty(name, value);
	}

	Set<String> getRawPropertyNames() {
		return sto.getPropertyNames();
	}

	synchronized SnapshotList getSnapshotsInternal() {
		if (xSnapshots == null)
			xSnapshots = new SnapshotList(this);
		return xSnapshots;
	}

	@Override
	public synchronized List<CDStarSnapshot> getSnapshots() {
		return new ArrayList<CDStarSnapshot>(getSnapshotsInternal().load());
	}

	@Override
	public synchronized CDStarSnapshot createSnapshot(String name) throws InvalidSnapshotName {
		Objects.requireNonNull(name);

		checkPermission(ArchivePermission.SNAPSHOT);
		requireTransactionWriteable();
		requirePayloadAvailable();

		try {
			final CDStarSnapshot snap = getSnapshotsInternal().createSnapshot(name);
			markModified();
			return snap;
		} catch (InvalidSnapshotName | IllegalStateException e) {
			throw e;
		} catch (Exception e) {
			throw new BackendError("Unable to create snapshot", e);
		}

	}

	@Override
	public String toString() {
		return "Archive(" + getVault().getName() + "/" + getId() + ")";
	}

}
