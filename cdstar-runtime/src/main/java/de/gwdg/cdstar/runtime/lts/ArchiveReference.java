package de.gwdg.cdstar.runtime.lts;

public interface ArchiveReference {

	String getVault();

	String getId();

	String getRevision();

}