package de.gwdg.cdstar.runtime.client;

import java.io.IOException;
import java.io.InputStream;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.gwdg.cdstar.SharedObjectMapper;
import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.pool.BackendError;
import de.gwdg.cdstar.pool.PoolError.ExternalResourceException;
import de.gwdg.cdstar.pool.PoolError.StaleHandle;
import de.gwdg.cdstar.pool.Resource;
import de.gwdg.cdstar.pool.StorageObject;
import de.gwdg.cdstar.runtime.client.exc.InvalidSnapshotName;

class SnapshotList {
	private static final Logger log = LoggerFactory.getLogger(SnapshotList.class);

	public static final String SNAPSHOT_INDEX_FILE = "snapshots.json";
	public static final String SNAPSHOT_INDEX_TYPE = "application/x-cdstar-snapshotlist;v=3";
	public static final String SNAPSHOT_FILE = "snapshot.json";
	public static final Pattern NAME_PATTERN = Pattern.compile("^[0-9a-z][0-9a-z-._]{0,64}$");

	private List<SnapshotImpl> loaded;
	private ArchiveImpl archive;
	private Resource snapshotIndex;

	public SnapshotList(ArchiveImpl archive) {
		this.archive = archive;
	}

	void flush() {
		if (loaded == null)
			return;

		// Cleanup snapshots that never really existed
		loaded.removeIf(s -> s.isNew() && s.isRemoved());

		if (loaded.isEmpty())
			return;

		// Stop if no snapshot was modified during this session
		if (null == Utils.first(loaded, SnapshotImpl::isModified))
			return;

		// Create snapshot index resource if needed
		if (snapshotIndex == null) {
			snapshotIndex = archive.createResource(SNAPSHOT_INDEX_FILE);
			snapshotIndex.setMediaType(SNAPSHOT_INDEX_TYPE);
		}

		// Store new snapshot list
		List<SnapshotEntry> snapIndex = loaded.stream().map(f -> f.getMeta()).collect(Collectors.toList());
		try (WritableByteChannel out = snapshotIndex.getWriteChannel(0)) {
			SharedObjectMapper.json.writeValue(Channels.newOutputStream(out), snapIndex);
		} catch (IOException | RuntimeException e) {
			throw new BackendError("Unable to persist snapshot list", e);
		}
	}

	List<SnapshotImpl> load() throws StaleHandle, ExternalResourceException {
		if (loaded != null)
			return loaded;

		synchronized (this) {
			if (loaded != null)
				return loaded;

			loaded = new ArrayList<>();
			snapshotIndex = archive.getResource(SNAPSHOT_INDEX_FILE);
			if (snapshotIndex == null)
				return loaded;

			if (!SNAPSHOT_INDEX_TYPE.equals(snapshotIndex.getMediaType()))
				throw new BackendError("Failed to load snapshot list: Wrong type");

			if (snapshotIndex.getSize() == 0)
				return loaded;

			try (ReadableByteChannel rc = snapshotIndex.getReadChannel(0)) {
				InputStream stream = Channels.newInputStream(rc);
				for (SnapshotEntry meta : SharedObjectMapper.json.readValue(stream, SnapshotEntry[].class)) {
					loaded.add(new SnapshotImpl(archive, meta));
				}
			} catch (IOException e) {
				throw new BackendError("Failed to load snapshot list", e);
			}

			return loaded;
		}
	}

	CDStarSnapshot createSnapshot(String name)
			throws InvalidSnapshotName, StaleHandle, ExternalResourceException, IOException {
		archive.requireTransactionWriteable();
		archive.requirePayloadAvailable();

		if (archive.isContentModified())
			throw new IllegalStateException("Snapshots can only be created from unmodified content");

		name = Utils.normalizeNFKC(name);
		if (!isValidSnapshotName(name))
			throw new InvalidSnapshotName(name, "Snapshot name contains illegal characters or is too short or long");
		if (getSnapshot(name) != null)
			throw new InvalidSnapshotName(name, "Snapshot with that name already exists");

		StorageObject target = ((VaultImpl) archive.getVault()).createDirect();
		log.debug("Creating snapshot {} of {} in {}", archive, target.getId());

		SnapshotImpl snap = SnapshotImpl.createFrom(archive, name, target);
		loaded.add(snap);
		archive.forEachListener(l -> l.snapshotCreated(snap));
		return snap;
	}

	private boolean isValidSnapshotName(String name) {
		return NAME_PATTERN.matcher(name).matches();
	}

	private SnapshotImpl getSnapshot(String name) {
		return Utils.first(load(), s -> s.getName().contentEquals(name));
	}

}
