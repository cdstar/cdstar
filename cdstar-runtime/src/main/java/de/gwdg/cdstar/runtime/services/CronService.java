package de.gwdg.cdstar.runtime.services;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/**
 * Same as {@link ScheduledExecutorService}, but without any methods inherited
 * from {@link ExecutorService}
 */
public interface CronService {

	/**
	 * Execute a one-shot {@link Runnable} after the given delay.
	 */
	public ScheduledFuture<?> schedule(Runnable command, long delay, TimeUnit unit);

	/**
	 * Execute a one-shot {@link Callable} after the given delay and return its
	 * value.
	 */
	public <V> ScheduledFuture<V> schedule(Callable<V> callable, long delay, TimeUnit unit);

	/**
	 * Execute a {@link Runnable} repeatedly at a fixed rate. If the execution of a
	 * task takes longer than the period, it will delay (or suppress) subsequent
	 * executions to prevent overlapping tasks. Any exception will end the schedule
	 * for this task.
	 */
	public ScheduledFuture<?> scheduleAtFixedRate(Runnable command, long initialDelay, long period, TimeUnit unit);

	/**
	 * Execute a {@link Runnable} repeatedly with a fixed delay between executions.
	 * Any exception will end the schedule for this task.
	 */
	public ScheduledFuture<?> scheduleWithFixedDelay(Runnable command, long initialDelay, long delay, TimeUnit unit);

}