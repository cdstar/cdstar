package de.gwdg.cdstar.runtime.services;

import java.io.IOException;
import java.nio.file.FileStore;
import java.nio.file.Path;
import java.util.concurrent.ExecutorService;

public interface PoolService {

	/**
	 * Return an IO pool bound to the {@link FileStore} of the path.
	 *
	 * IO pools have a minimum number of 4 threads, even if the VM has less CPU
	 * cores available, since most modern storage systems (SSD) benefit from
	 * parallel IO and most modern OS can re-order queued operations to improve
	 * performance.
	 *
	 * @throws IOException
	 *             if the file store could not been found.
	 */
	ExecutorService getIOPool(Path forFile) throws IOException;

	/**
	 * Get a named pool with an upper thread count limit based on CPU count.
	 */
	ExecutorService getNamedPool(String name);

}