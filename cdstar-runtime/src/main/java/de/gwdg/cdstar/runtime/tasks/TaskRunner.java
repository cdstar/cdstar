package de.gwdg.cdstar.runtime.tasks;

import java.util.concurrent.CancellationException;
import java.util.concurrent.CompletableFuture;

/**
 * A {@link TaskRunner}
 */
public interface TaskRunner {

	/**
	 * Return true if this handler can run the given task, false otherwise.
	 *
	 * The default implementation returns true if the class name equals the task
	 * name.
	 */
	default boolean canRun(Task task) {
		return task.getName().equals(getClass().getName());
	}

	/**
	 * Do work as requested by the task handle and return a
	 * {@link CompletableFuture}. This method is allowed to block a short time,
	 * but long running tasks SHOULD be handled in a separate thread pool.
	 *
	 * The {@link TaskRunner} MUST ensure that the returned
	 * {@link CompletableFuture} is completed eventually, as there is no
	 * automatic timeout handling and 'lost' tasks are not rescheduled or
	 * cleaned up until the server is restarted.
	 *
	 * The returned {@link CompletableFuture} may be completed early as the
	 * result of a {@link Task#cancel()} call. Runners should occasionally check
	 * {@link Task#isCanceled()} or listen to the {@link CompletableFuture} and
	 * avoid unnecessary work in case the task was canceled early. Canceled
	 * tasks count as success.
	 *
	 * Tasks may also be interrupted during server shutdown. Runners should
	 * check {@link Task#isInterrupted()} regularly and stop processing
	 * interrupted task in a timely manner to avoid unnecessary delays during
	 * shutdown. These tasks are automatically re-submitted once the server is
	 * started again.
	 *
	 * If the runner throws an exception before returning a future, or if the
	 * future is completed exceptionally with an exception other than
	 * {@link CancellationException} or {@link InterruptedException}, the task
	 * is logged as failed. Failed tasks are re-scheduled and passed to the
	 * runner again after a certain delay, but only a limited number of times.
	 * If the task fails too often, or completes with a
	 * {@link FatalTaskException} or {@link Error}, it is considered
	 * unrecoverable, logged and then removed.
	 *
	 * In certain edge cases (e.g. an unclean VM shutdown), tasks may be
	 * re-scheduled even if they completed successfully or were canceled.
	 */

	CompletableFuture<Void> run(Task task) throws Exception;

}
