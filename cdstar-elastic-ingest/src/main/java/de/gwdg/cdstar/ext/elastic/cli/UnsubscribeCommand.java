package de.gwdg.cdstar.ext.elastic.cli;

import de.gwdg.cdstar.ext.elastic.ActiveMQListener;
import de.gwdg.cdstar.runtime.Config;
import picocli.CommandLine.Command;
import picocli.CommandLine.Parameters;

@Command(name = "unsubscribe")
public class UnsubscribeCommand extends BaseCommand {
	@Parameters
	String vault;

	@Override
	public void run(Config cfg) throws Exception {
		final ActiveMQListener listener = new ActiveMQListener(cfg.get("broker"), cfg.get("topic"));
		listener.unsubscribe();
	}

}
