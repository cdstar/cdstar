package de.gwdg.cdstar.ext.elastic.cli;

import java.nio.file.Path;
import java.nio.file.Paths;

import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.client.CDStarClientConfig;
import de.gwdg.cdstar.client.CDStarRestClient;
import de.gwdg.cdstar.ext.elastic.ActiveMQListener;
import de.gwdg.cdstar.ext.elastic.ElasticIngest;
import de.gwdg.cdstar.ext.elastic.ElasticSearchClient;
import de.gwdg.cdstar.runtime.Config;
import picocli.CommandLine.Command;

@Command(name = "listen")
public class ListenCommand extends BaseCommand {

	@Override
	public void run(Config cfg) throws Exception {
		final Path journal = Paths.get(cfg.get("journal"));
		final ActiveMQListener listener = new ActiveMQListener(cfg.get("broker"), cfg.get("topic"));
		final ElasticSearchClient elastic = new ElasticSearchClient(cfg.getURI("elastic"));
		final CDStarRestClient cdstar = new CDStarRestClient(new CDStarClientConfig(cfg.getURI("cdstar")));
		final ElasticIngest ingest = new ElasticIngest(journal, cdstar, elastic);

		try {
			elastic.resolveVersion();
			ingest.setIndexMask(cfg.get("index"));
			ingest.start();

			while (true) {
				listener.handleOne(ingest::submit);
			}
		} finally {
			Utils.closeQuietly(listener::close);
			Utils.closeQuietly(ingest::stop);
			Utils.closeQuietly(cdstar);
			Utils.closeQuietly(elastic);
		}
	}

}
