package de.gwdg.cdstar.ext.elastic;

import java.io.IOException;

import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.jms.Topic;
import javax.jms.TopicSubscriber;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

import de.gwdg.cdstar.FailableConsumer;
import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.event.ChangeEvent;

public class ActiveMQListener {

	public static final String CLIENT_IDENTITY = "esingest";
	public static final String CLIENT_NAME = "esingest";

	private static final Logger log = LoggerFactory.getLogger(ActiveMQListener.class);

	public static ObjectMapper om = new ObjectMapper();
	private Connection connection;

	private final String topicName;
	private final String target;
	private final ActiveMQConnectionFactory connectionFactory;
	private boolean closed;
	private TopicSubscriber consumer;

	public ActiveMQListener(String target, String topicName) {
		this.target = target;
		this.topicName = topicName;

		if (!target.startsWith("failover:"))
			log.warn("Broker url does not use the 'failover' meta-transport."
					+ " The process will NOT reconnect automatically if the connection is lost.");

		connectionFactory = new ActiveMQConnectionFactory(target);
	}

	public void unsubscribe() throws IOException {
		try {
			connect().createSession(false, Session.AUTO_ACKNOWLEDGE).unsubscribe(CLIENT_NAME);
			log.info("Succesfully unsubscribed from {} (client: {}:{})",
				target, CLIENT_IDENTITY, CLIENT_NAME);
		} catch (final JMSException e) {
			close();
			throw new IOException(e);
		}
	}

	private synchronized Connection connect() throws JMSException {
		if (connection != null)
			return connection;
		connection = connectionFactory.createConnection();
		connection.setClientID(CLIENT_IDENTITY);
		connection.start();
		return connection;
	}

	public void handleOne(FailableConsumer<ChangeEvent> handler) throws Exception {
		if (consumer == null) {
			try {
				final Session session = connect().createSession(false, Session.CLIENT_ACKNOWLEDGE);
				final Topic topic = session.createTopic(topicName);
				consumer = session.createDurableSubscriber(topic, CLIENT_NAME);
				log.info("Succesfully subscribed to {} (topic: {}, client: {}:{})",
					target, topicName, CLIENT_IDENTITY, CLIENT_NAME);
			} catch (final JMSException e) {
				throw new IOException("Failed to subscribe to topic", e);
			}
		}

		while (!closed) {
			final Message message = consumer.receive(1000);

			if (message == null)
				continue;

			if (!(message instanceof TextMessage)) {
				log.warn("Ignored non-text message: {}", message);
				continue;
			}

			final TextMessage txtMsg = (TextMessage) message;
			final String text = txtMsg.getText();

			log.debug("Message recieved: {}", message);

			final ChangeEvent event = om.readValue(text, ChangeEvent.class);
			handler.accept(event);
			
			// Acknowledge only on success.
			txtMsg.acknowledge();
		}
	}

	public void close() {
		closed = true;
		if (connection != null)
			Utils.closeQuietly(connection::close);
	}

}
