package de.gwdg.cdstar.ext.elastic;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import de.gwdg.cdstar.Promise;
import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.client.CDStarRestClient;
import de.gwdg.cdstar.event.ChangeEvent;
import journal.io.api.ClosedJournalException;
import journal.io.api.CompactedDataFileException;
import journal.io.api.Journal;
import journal.io.api.Journal.ReadType;
import journal.io.api.Journal.WriteType;
import journal.io.api.JournalBuilder;
import journal.io.api.Location;

public class ElasticIngest {
	private static ObjectMapper om = new ObjectMapper();
	public static final Logger log = LoggerFactory.getLogger(ElasticIngest.class);
	private static final String MASK_PLACEHOLDER = "{}";
	private final Map<String, ElasticMapping> mappings = new ConcurrentHashMap<>();

	private final ElasticSearchClient es;
	private final CDStarRestClient cdstar;
	private final JournalBuilder journalBuilder;
	private Journal journal;

	ExecutorService pool = new ThreadPoolExecutor(0, 4,
		60L, TimeUnit.SECONDS, new ArrayBlockingQueue<>(1024));

	Map<String, IngestTask> nextBatch = new HashMap<>();

	private boolean stopped;
	private Thread batchThread;
	private boolean started;
	private String indexMask;

	public ElasticIngest(Path journalDir, CDStarRestClient cdstar, ElasticSearchClient esClient) throws IOException {
		this.cdstar = cdstar.clone();
		es = esClient;
		Files.createDirectories(journalDir);
		journalBuilder = JournalBuilder.of(journalDir.toFile());
		setIndexMask(MASK_PLACEHOLDER);
	}

	public void setIndexMask(String mask) {
		final int i = mask.indexOf(MASK_PLACEHOLDER);
		if (i == -1 || mask.indexOf(MASK_PLACEHOLDER, i + 1) != -1)
			throw new IllegalArgumentException("Mask must contain '{}' exactly once");
		if (mask.contains(","))
			throw new IllegalArgumentException("Mask must not contain ','");
		indexMask = mask;
	}

	private String indexNameFromVault(String vault) {
		return indexMask.replace(MASK_PLACEHOLDER, vault);
	}

	/**
	 * Start the ingest threads and re-submit all events that were not processed
	 * during last run.
	 */
	public void start() throws ClosedJournalException, CompactedDataFileException, IOException, Exception {

		log.debug("Staring task handling thread...");
		batchThread = new Thread(this::ingestLoop, "ElasticIngest");
		batchThread.start();

		log.info("Reading unfinished tasks from journal...");
		journal = journalBuilder.open();
		for (final Location loc : journal.undo()) {
			final ChangeEvent e = decodeEvent(journal.read(loc, ReadType.SYNC));
			submitInternal(e, true);
			journal.delete(loc);
			journal.sync();
		}

		started = true;
	}

	public void stop() {
		if (!started)
			throw new IllegalStateException("Ingest not started.");
		stopped = true;
		try {
			batchThread.join();
		} catch (final InterruptedException e) {
			log.warn("Shutdown interrupted", e);
		} finally {
			Utils.closeQuietly(pool::shutdown);
			if(journal != null) Utils.closeQuietly(journal::close);
		}
	}

	public class IngestTask {
		final ChangeEvent event;
		final Promise<IngestTask> promise = Promise.empty();
		long startTime = System.currentTimeMillis();

		IngestTask(ChangeEvent event) {
			this.event = event;
		}

		public String getKey() {
			return event.getVault() + ":" + event.getArchive();
		}

		@Override
		public String toString() {
			return getKey();
		}
	}

	/**
	 * Sumbit an event. If this returns a promise, then the event was successfully
	 * written to the write-ahead-log (journal) and will be handled eventually. The
	 * Promise may succeed or fail, but is recoverable in case of a server crash. If
	 * this throws an exception, the event is NOT persisted.
	 *
	 * @throws IOException The event could not be persisted to disk.
	 */
	public Promise<IngestTask> submit(ChangeEvent event) throws IOException {
		if (!started)
			throw new IllegalStateException("Ingest not ready. Call start() first.");
		if (stopped)
			throw new IllegalStateException("Ingest stopped.");
		return submitInternal(event, true);
	}

	/**
	 * Trigger an update, but do not persist the (virtual) event to disk. Just
	 * submit it to the in-memory queue.
	 */
	public Promise<IngestTask> forceUpdate(String vault, String id) throws IOException {
		if (!started)
			throw new IllegalStateException("Ingest not ready. Call start() first.");
		if (stopped)
			throw new IllegalStateException("Ingest stopped.");

		final ChangeEvent ce = new ChangeEvent();
		ce.setVault(vault);
		ce.setArchive(id);
		ce.setTs(System.currentTimeMillis());
		return submitInternal(ce, false);
	}

	private Promise<IngestTask> submitInternal(ChangeEvent event, boolean withJournal) throws IOException {

		final Location loc = withJournal ? journal.write(encodeEvent(event), WriteType.SYNC) : null;
		final IngestTask task = new IngestTask(event);
		log.debug("[{}] Task accepted", task.getKey());

		addTask(task);

		// Ensure that completed or canceled tasks are removed from the journal.
		// Failed tasks remain in journal.
		task.promise.then((t, err) -> {
			try {
				if (err == null) {
					log.debug("[{}] Task finished.", task, err);
					if (loc != null)
						journal.delete(loc);
				} else if (err instanceof CancellationException) {
					log.info("[{}] Task canceled.", task, err);
					if (loc != null)
						journal.delete(loc);
				} else {
					log.error("[{}] Task failed.", task, err);
				}
			} catch (final IOException e) {
				log.warn("[{}] Failed to remove task from journal.", task, e);
			}
		});

		return task.promise;
	}

	/**
	 * Put task into waiting list, replacing old task for the same archive if
	 * present. The old event is canceled.
	 */
	private void addTask(IngestTask task) {
		synchronized (nextBatch) {
			nextBatch.merge(task.getKey(), task, (a, b) -> {
				if (a.event.getTs() > b.event.getTs()) {
					log.debug("[{}] Replacing old task for same item in queue.", task.getKey());
					b.promise.cancel();
					return a;
				} else {
					log.debug("[{}] Skipping. Newer task for same item already queued.", task.getKey());
					a.promise.cancel();
					return b;
				}
			});
			nextBatch.notifyAll();
		}
	}

	private void ingestLoop() {
		try {
			while (!stopped) {
				List<IngestTask> currentBatch = null;
				synchronized (nextBatch) {
					if (nextBatch.isEmpty()) {
						nextBatch.wait(1000);
						continue;
					}
					currentBatch = new ArrayList<>(nextBatch.values());
					nextBatch.clear();
				}
				runSingleBatch(currentBatch);
			}
		} catch (final InterruptedException e) {
			log.info("Ingest loop interrupted");
		} catch (final Exception e) {
			log.error("Ingest loop stopped with Exception", e);
		} finally {
			stopped = true;
			log.debug("Ingest thread stopped");
		}
	}

	private void runSingleBatch(List<IngestTask> currentBatch) throws InterruptedException {
		log.debug("Starting batch with {} tasks", currentBatch.size());

		final CountDownLatch latch = new CountDownLatch(currentBatch.size());

		for (final IngestTask task : currentBatch) {
			pool.submit(() -> runSingleTask(task));
			task.promise.then((t, err) -> latch.countDown());
		}

		latch.await();
	}

	private void runSingleTask(IngestTask task) {
		ElasticIngestJob job = null;
		try {
			ensureIndex(task.event.getVault());
			job = new ElasticIngestJob(cdstar, es, indexNameFromVault(task.event.getVault()), task.event);
			job.sync();
			task.promise.tryResolve(task);
		} catch (final Exception e) {
			task.promise.tryReject(e);
		} finally {
			Utils.closeQuietly(job);
		}
	}

	private void ensureIndex(String vaultName) throws IOException {
		final String indexName = indexNameFromVault(vaultName);
		mappings.computeIfAbsent(indexName, name -> new ElasticMapping(es, name)).ensureIndex();
	}

	public void dropIndex(String vaultName) throws IOException {
		final String indexName = indexNameFromVault(vaultName);
		mappings.computeIfAbsent(indexName, name -> new ElasticMapping(es, name)).dropIndexIfExist();
	}

	private byte[] encodeEvent(ChangeEvent event) throws JsonProcessingException {
		return om.writeValueAsBytes(event);
	}

	private ChangeEvent decodeEvent(byte[] read) throws IOException {
		return om.readValue(read, ChangeEvent.class);
	}


}
