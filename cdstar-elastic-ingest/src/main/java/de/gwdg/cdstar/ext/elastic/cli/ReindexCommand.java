package de.gwdg.cdstar.ext.elastic.cli;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.concurrent.Semaphore;

import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.client.CDStarClientConfig;
import de.gwdg.cdstar.client.CDStarRestClient;
import de.gwdg.cdstar.client.actions.GetServiceInfo;
import de.gwdg.cdstar.client.helper.ListVaultIterable;
import de.gwdg.cdstar.ext.elastic.ElasticIngest;
import de.gwdg.cdstar.ext.elastic.ElasticSearchClient;
import de.gwdg.cdstar.runtime.Config;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;
import picocli.CommandLine.Parameters;

@Command(name = "reindex", description = "Re-index an entire vault")
public class ReindexCommand extends BaseCommand {
	@Option(names = "--nuke", description = "Remove and re-create the index. This is required for mapping changes, but requires a complete re-index.")
	boolean nukeIndex;

	@Parameters(description = "A list of vaults (optionally followed by a ':' and a start id) to re-index. (default: all vaults)")
	List<String> vaults;

	@Override
	public void run(Config cfg) throws Exception {
		final Path journal = Paths.get(cfg.get("journal"));
		final ElasticSearchClient elastic = new ElasticSearchClient(cfg.getURI("elastic"));
		final CDStarRestClient cdstar = new CDStarRestClient(new CDStarClientConfig(cfg.getURI("cdstar")));
		final ElasticIngest ingest = new ElasticIngest(journal, cdstar, elastic);

		try {
			elastic.resolveVersion();
			ingest.setIndexMask(cfg.get("index"));
			ingest.start();

			// Limit to 100 in-memory promises.
			final int maxTasks = 100;
			final Semaphore sem = new Semaphore(maxTasks);

			if (vaults == null || vaults.isEmpty()) {
				vaults = cdstar.execute(new GetServiceInfo()).vaults;
			}

			for (final String jobString : vaults) {
				final String vault;
				String startId = "";
				if (jobString.contains(":")) {
					final String tmp = jobString;
					vault = tmp.substring(0, tmp.indexOf(":"));
					startId = tmp.substring(tmp.indexOf(":") + 1);
				} else {
					vault = jobString;
				}

				if (nukeIndex && startId.isEmpty()) {
					ingest.dropIndex(vault);
				} else if (nukeIndex) {
					ElasticIngest.log.warn("Ignoring --nuke command for vault {} (start id given)", vault);
				}

				for (final String id : new ListVaultIterable(cdstar, vault, startId)) {
					sem.acquire();
					ingest.forceUpdate(vault, id).then(task -> {
						System.out.println(vault + ":" + id);
						sem.release();
					});
				}
			}

			while (sem.availablePermits() < maxTasks && Utils.sleepInterruptable(100)) {
				// Wait for last promises to finish
			}

		} finally {
			Utils.closeQuietly(ingest::stop);
			Utils.closeQuietly(cdstar);
			Utils.closeQuietly(elastic);
		}
	}

}
