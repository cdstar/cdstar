package de.gwdg.cdstar.ext.elastic;

import java.io.IOException;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import com.fasterxml.jackson.databind.node.ObjectNode;

import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.ext.elastic.dao.IndexDocument;

public class ElasticMapping {
	private final ElasticSearchClient es;
	private final String index;
	private final ObjectNode defaultMapping;

	private boolean exists;
	Throwable error;

	public ElasticMapping(ElasticSearchClient client, String indexName) {
		es = client;
		index = indexName;

		try (var defMapFile = getClass().getClassLoader().getResourceAsStream("default-mapping.json")) {
			defaultMapping = (ObjectNode) ElasticSearchClient.om.readTree(defMapFile);
		} catch (IOException e) {
			throw Utils.wtf("Failed to load default mapping", e);
		}

		if (es.version().supportsRuntimeFields())
			defaultMapping
				.with("mappings")
				.with("properties")
				.with(IndexDocument.META_FIELD).put("dynamic", "runtime");

		addAdditionalMappings();
	}

	void addAdditionalMappings() {
		// Define a couple of well known meta fields as aliases.
		// TODO: Allow additional fields via config.
		defineMetaField("dc:contributor", "text", "dcContributor");
		defineMetaField("dc:coverage", "text", "dcCoverage");
		defineMetaField("dc:creator", "text", "dcCreator");
		defineMetaField("dc:date", "date", "dcDate");
		defineMetaField("dc:description", "text", "dcDescription");
		defineMetaField("dc:format", "keyword", "dcFormat");
		defineMetaField("dc:identifier", "keyword", "dcIdentifier");
		defineMetaField("dc:language", "keyword", "dcLanguage");
		defineMetaField("dc:publisher", "text", "dcPublisher");
		defineMetaField("dc:relation", "keyword", "dcRelation");
		defineMetaField("dc:rights", "text", "dcRights");
		defineMetaField("dc:source", "keyword", "dcSource");
		defineMetaField("dc:subject", "keyword", "dcSubject");
		defineMetaField("dc:title", "text", "dcTitle");
		defineMetaField("dc:type", "keyword", "dcType");
	}

	/**
	 * Values below 'meta' are not indexed by default (dynamic=false/runtime),
	 * because their names are user-defined and may create a large number of index
	 * fields (index explosion). Only known meta-fields are indexed.
	 *
	 * Because cdstar meta-attribute names usually contain a namespace (e.g.
	 * dc:title) and the namespace separator needs escaping in search queries, a
	 * more user-friendly alias should be added to the search mapping (e.g. dcTitle
	 * -> meta.dc:title)
	 *
	 * @param metaField name of the key below the 'meta' field (e.g. 'dc:title').
	 * @param type      elasticsearch type
	 * @param alias     one or more alias fields the value is copied to.
	 */
	private void defineMetaField(String metaField, String type, String... aliases) {
		final ObjectNode fieldDef = defaultMapping
			.with("mappings")
			.with("properties")
			.with(IndexDocument.META_FIELD)
			.with("properties")
			.with(metaField);
		fieldDef.put("type", type);

		// TODO: Find all datatypes where this makes sense
		if (type.equals("date") || type.equals("integer"))
			fieldDef.put("ignore_malformed", true);

		for (final String alias : aliases)
			metaAlias(metaField, alias);
	}

	/**
	 * Define an alias field that points to a field below `meta`.
	 *
	 * @param metaField Name of the alias target.
	 * @param alias     Name of the alias. Must be a root-level field.
	 */
	private void metaAlias(String metaField, String alias) {
		if (alias.contains("."))
			throw new IllegalArgumentException("Alias names must be root-level: " + alias);

		final ObjectNode prop = defaultMapping
			.with("mappings")
			.with("properties")
			.with(alias);

		if (prop.size() > 0)
			throw new IllegalArgumentException("Alias name conflicts with existing field: " + alias);

		prop.put("type", "alias").put("path", IndexDocument.META_FIELD + "." + metaField);
	}

	/**
	 * Meta fields from different namespaces may overlap in their semantic meaning.
	 * For example, dcterms:dateAccepted is a more specific refinement of dc:date.
	 * With this method, one already defined field (see
	 * {@link #defineMetaField(String, String, String)} can be copied_to a second
	 * already defined field.
	 *
	 * @param src           Copy from this one
	 * @param target        Copy to this one
	 * @param bidirectional Copy in both directions
	 */
	private void metaCopyTo(String srcName, String dstName, boolean bidirectional) {

		final ObjectNode src = defaultMapping
			.with("mappings")
			.with("properties")
			.with(IndexDocument.META_FIELD)
			.with("properties")
			.with(srcName);
		final ObjectNode dst = defaultMapping
			.with("mappings")
			.with("properties")
			.with(IndexDocument.META_FIELD)
			.with("properties")
			.with(dstName);

		if (!src.has("type"))
			throw new IllegalArgumentException("Source field must exist: " + srcName);
		if (!dst.has("type"))
			throw new IllegalArgumentException("Target field must exist: " + dstName);
		if (!src.get("type").asText().equals(dst.get("type").asText()))
			throw new IllegalArgumentException("Source and target fields must habe the same type");

		src.withArray("copy_to").add(IndexDocument.META_FIELD + "." + dstName);
		if (bidirectional)
			dst.withArray("copy_to").add(IndexDocument.META_FIELD + "." + srcName);
	}

	public synchronized boolean ensureIndex() throws IOException {
		if (exists)
			return true;

		waitForYellow();
		try {

			try {
				es.createIndex(index)
					.mappings(defaultMapping.with("mappings"))
					.settings(defaultMapping.with("settings"))
					.execute();
				ElasticIngest.log.debug("Created index '{}' with mapping: {}", index, defaultMapping);
			} catch (final ESErrorResponse e) {
				if (!e.getType().equals(ESErrorResponse.TYPE_INDEX_ALREADY_EXISTS))
					throw e;
			}

			exists = true;
			return exists;
		} catch (CancellationException | InterruptedException | ESErrorResponse e) {
			throw new IOException("Failed to prepare index", e);
		}

	}

	void waitForYellow() throws IOException {
		try {
			es.waitForYellow().get(60, TimeUnit.SECONDS);
		} catch (CancellationException | ExecutionException | InterruptedException | TimeoutException e) {
			throw new IOException("Cluster not ready", e);
		}
	}

	public synchronized void dropIndexIfExist() throws IOException {
		if (exists)
			throw new IllegalStateException("Index already in use");

		waitForYellow();

		try {
			es.deleteIndex(index).execute();
		} catch (CancellationException | ESErrorResponse | InterruptedException e) {
			throw new IOException("Failed to drop index", e);
		}
	}

}
