package de.gwdg.cdstar.ext.elastic;

import java.io.InputStream;

import org.apache.tika.metadata.HttpHeaders;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.metadata.TikaCoreProperties;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.sax.ContentHandlerDecorator;
import org.xml.sax.SAXException;

import de.gwdg.cdstar.ext.elastic.TikaHelper.LimitedTextExtractor.SizeLimitReachedException;

public class TikaHelper {
	private int textLimit;

	public TikaHelper() {
		// 64MB ought to be enough for anybody - Bill Gates, 1981
		setTextLimit(64 * 1024 * 1024);
	}

	public int getTextLimit() {
		return textLimit;
	}

	public void setTextLimit(int textLimit) {
		this.textLimit = textLimit;
	}

	public static class ParseResult {

		private final String text;
		private final Metadata meta;
		private final Exception error;

		ParseResult(String text, Metadata meta, Exception e) {
			this.text = text;
			this.meta = meta;
			error = e;
		}

		public String getText() {
			return text;
		}

		public Metadata getMeta() {
			return meta;
		}

		public Exception getError() {
			return error;
		}

		public boolean hasError() {
			return error != null;
		}

		public boolean isTruncated() {
			return error instanceof SizeLimitReachedException;
		}

	}

	static class LimitedTextExtractor extends ContentHandlerDecorator {
		StringBuilder sb = new StringBuilder(1024);
		private final int maxLen = 1024 * 1024;

		@Override
		public void ignorableWhitespace(char[] ch, int start, int length) throws SAXException {
			// characters(ch, start, length);
		}

		@Override
		public void endElement(String uri, String localName, String name) throws SAXException {
			if (localName.equals("p"))
				sb.append("\n");
		}

		@Override
		public void characters(char[] ch, int start, int length) throws SAXException {
			if (length <= 0)
				return;
			sb.append(ch, start, length);
			if (sb.length() > maxLen)
				throw new SizeLimitReachedException(sb.length());
		}

		public String getString() {
			return sb.toString();
		}

		public boolean isTruncated() {
			return sb.length() > maxLen;
		}

		protected static class SizeLimitReachedException extends SAXException {
			private static final long serialVersionUID = 8651398189308287854L;

			public SizeLimitReachedException(long limit) {
				super("Input truncated at " + limit + " bytes");
			}
		}

	}

	public ParseResult parse(InputStream src, String fileName, String mediaType) {
		final LimitedTextExtractor handler = new LimitedTextExtractor();
		final AutoDetectParser parser = new AutoDetectParser();
		final Metadata metadata = new Metadata();
		if (fileName != null)
			metadata.add(TikaCoreProperties.RESOURCE_NAME_KEY, fileName);
		if (mediaType != null)
			metadata.add(HttpHeaders.CONTENT_TYPE, mediaType);

		try {
			parser.parse(src, handler, metadata);
			return new ParseResult(handler.getString().trim(), metadata, null);
		} catch (final Exception e) {
			return new ParseResult(handler.getString().trim(), metadata, e);
		}
	}
}
