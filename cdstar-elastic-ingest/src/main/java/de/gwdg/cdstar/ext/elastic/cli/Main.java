package de.gwdg.cdstar.ext.elastic.cli;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

import de.gwdg.cdstar.config.ConfigLoader;
import de.gwdg.cdstar.runtime.Config;
import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;
import picocli.CommandLine.ParameterException;

public class Main {

	@Command(name = "main", subcommands = { ListenCommand.class, UnsubscribeCommand.class, ReindexCommand.class })
	public static class MainCommand {
		@Option(names = { "-c", "--config" }, required = true)
		public String configFile = "cdstar-ingest.yml";
		@Option(names = { "--debug" })
		public List<String> debug;
	}

	private static final Logger log = LoggerFactory.getLogger(Main.class);
	public static ObjectMapper om = new ObjectMapper();

	public static void main(String[] args) throws Exception {
		final CommandLine cmd = new CommandLine(new MainCommand());
		List<CommandLine> commands;
		try {
			commands = cmd.parseArgs(args).asCommandLineList();
			if (cmd.isUsageHelpRequested() || commands.size() < 2) {
				cmd.usage(System.err);
				System.exit(0);
				return;
			} else if (cmd.isVersionHelpRequested()) {
				cmd.printVersionHelp(System.err);
				System.exit(0);
				return;
			}
		} catch (final ParameterException ex) {
			System.err.println(ex.getMessage());
			cmd.usage(System.err);
			System.exit(1);
			return;
		}

		final MainCommand main = (MainCommand) commands.get(0).getCommand();

		if (main.debug != null) {
			for (final String d : main.debug) {
				final Logger l = LoggerFactory.getLogger(d.equals("ROOT") ? Logger.ROOT_LOGGER_NAME : d);
				((ch.qos.logback.classic.Logger) l).setLevel(ch.qos.logback.classic.Level.DEBUG);
			}
		}

		final Path config = Paths.get(main.configFile);
		log.info("Config file: {}", main.configFile);
		final Config cfg = ConfigLoader.fromFile(config.toFile());

		cfg.setDefault("topic", "cdstar");
		cfg.setDefault("index", "cdstar-{}");

		final BaseCommand obj = (BaseCommand) commands.get(1).getCommand();
		obj.run(cfg);
	}

}
