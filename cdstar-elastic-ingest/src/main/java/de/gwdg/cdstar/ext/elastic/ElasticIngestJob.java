package de.gwdg.cdstar.ext.elastic;

import java.io.IOException;
import java.io.InputStream;
import java.time.Duration;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.apache.commons.collections4.map.HashedMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.node.ObjectNode;

import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.client.CDStarRestClient;
import de.gwdg.cdstar.client.ErrorResponseException;
import de.gwdg.cdstar.client.actions.GetArchiveInfo;
import de.gwdg.cdstar.client.actions.GetFileStream;
import de.gwdg.cdstar.client.helper.ArchiveFileIterable;
import de.gwdg.cdstar.event.ChangeEvent;
import de.gwdg.cdstar.ext.elastic.BulkRequest.ESBulkResult;
import de.gwdg.cdstar.ext.elastic.ElasticSearchClient.BaseResult;
import de.gwdg.cdstar.ext.elastic.ElasticSearchClient.ESPreparedRequest;
import de.gwdg.cdstar.ext.elastic.SearchRequest.ESSearchHit;
import de.gwdg.cdstar.ext.elastic.TikaHelper.ParseResult;
import de.gwdg.cdstar.ext.elastic.dao.ArchiveDocument;
import de.gwdg.cdstar.ext.elastic.dao.FileDocument;
import de.gwdg.cdstar.ext.elastic.dao.IndexDocument;
import de.gwdg.cdstar.web.common.model.ArchiveInfo;
import de.gwdg.cdstar.web.common.model.FileInfo;
import de.gwdg.cdstar.web.common.model.MetaMap;

public class ElasticIngestJob implements AutoCloseable {
	private static final Logger log = LoggerFactory.getLogger(ElasticIngestJob.class);

	private int maxBulk = 32;
	private long msTimeout = Duration.ofSeconds(60).toMillis();

	private static TikaHelper tika = new TikaHelper();

	static {
		tika.setTextLimit(8 * 1024 * 1024); // 8MB
	}

	private final ElasticSearchClient es;
	private final CDStarRestClient cdstar;
	private final ChangeEvent event;
	private final String index;
	private ArchiveInfo archive;
	private ArrayList<FileInfo> allFiles;
	private Map<String, FileInfo> filesToSync;

	private BulkRequest currentBulk;
	private final HashSet<String> readAccessForArchive = new HashSet<>();
	private final HashSet<String> readAccessForFiles = new HashSet<>();

	private final String taskKey;

	/*
	 * Ideas:
	 *
	 * b) Store merged metadata from the archive and the file into FileDocument.
	 */

	public ElasticIngestJob(CDStarRestClient cdstar, ElasticSearchClient esClient, String index, ChangeEvent event) {
		this.cdstar = cdstar.clone();
		this.event = event;
		es = esClient;
		this.index = index;
		taskKey = event.getVault() + ":" + event.getArchive();
	}

	/**
	 * Change the maximum number of items in bulk request. Note that a bulk request
	 * must fit into memory at least twice.
	 */
	public void setMaxBulk(int maxBulk) {
		this.maxBulk = maxBulk;
	}

	/**
	 * Set the timeout for requests to the search cluster or cdstar instance.
	 */
	public void setTimeout(int timeout, TimeUnit unit) {
		msTimeout = unit.toMillis(timeout);
	}

	@Override
	public void close() {
		Utils.closeQuietly(cdstar);
	}

	public void sync() throws InterruptedException, CancellationException, ExecutionException, TimeoutException,
		IOException {
		cdstar.begin(true);

		checkInterrupted();

		try {
			archive = new GetArchiveInfo(event.getVault(), event.getArchive())
				.withAcl(true)
				.withMeta(true)
				.execute(cdstar);
		} catch (final ErrorResponseException e) {
			if (e.getError().getStatus() == 404) {
				log.debug("[{}] Archive does not exist or is not visible. Removing from index.", taskKey);
				deleteByTermQuery(IndexDocument.ID_FIELD, event.getArchive());
				return;
			}
			log.warn("[{}] Failed to read archive info from cdstar", taskKey, e);
			throw e;
		}

		if (!archive.state.isReadable()) {
			log.warn("[{}] Archive is in {} state: File content will not be indexed.", taskKey, archive.state);
		}

		checkInterrupted();

		if (event.getRevision() != null && !archive.revision.equals(event.getRevision())) {
			log.debug("[{}] Archive revision does not match. Skipping this task.", taskKey);
			return;
		}

		// Collect permissions (subjects or groups able to read the archive or
		// file metadata)
		for (final Entry<String, List<String>> acl : archive.acl.getMap().entrySet()) {
			String subject = acl.getKey();
			final List<String> grants = acl.getValue();

			if (subject.equals("$owner"))
				subject = archive.owner;

			// TODO: Resolve actual permissions here
			if (grants.contains("READ") || grants.contains("OWNER")) {
				readAccessForArchive.add(subject);
				readAccessForFiles.add(subject);
			} else if (grants.contains("load") && grants.contains("read") && grants.contains("read_meta")) {
				readAccessForArchive.add(subject);
				if (grants.contains("read_files"))
					readAccessForFiles.add(subject);
			}
		}

		if (readAccessForArchive.isEmpty()) {
			log.debug("[{}] Archive ACL for read access is empty. Entry will be indexed anyway.", taskKey);
		}

		checkInterrupted();

		// Collect all files from the archive
		// TODO: Stream files from search and cdstar to better support large
		// archives.
		allFiles = new ArrayList<>();
		new ArchiveFileIterable(cdstar, archive.vault, archive.id).withMeta(true).forEach(f -> allFiles.add(f));
		filesToSync = new HashedMap<>(allFiles.size());
		allFiles.forEach(f -> filesToSync.put(f.id, f));

		checkInterrupted();

		// Collect all documents from search index
		final SearchRequest query = es.search(index);

		query.dsl()
			.with("bool")
			.withArray("filter")
			.addObject()
			.with("term")
			.put(IndexDocument.ID_FIELD,
				archive.id);
		query.sources(
			IndexDocument.ID_FIELD,
			IndexDocument.SCOPE_MARKER,
			IndexDocument.MODIFIED_FIELD,
			FileDocument.FID_FIELD,
			FileDocument.SHA256_FIELD);
		query.limit(1024);

		ArchiveDocument foundArchive = null;

		// Update all documents that already exist in ES
		try (var paginator = query.paginate(Duration.ofMinutes(10), Duration.ofMillis(msTimeout))) {
			for (var page : paginator) {
				for (final ESSearchHit hit : page.hits()) {
					checkInterrupted();
					log.debug("FOUND {}", hit);

					final IndexDocument doc = hit.source(IndexDocument.class);
					if (doc instanceof ArchiveDocument) {
						foundArchive = (ArchiveDocument) doc;
					} else if (doc instanceof FileDocument) {
						final FileInfo file = filesToSync.remove(((FileDocument) doc).fid);
						// file may be null, which issues a delete query
						indexFile((FileDocument) doc, archive, file);
					}
				}
			}
		}

		// Index new files not yet indexed
		for (final FileInfo file : filesToSync.values()) {
			indexFile(new FileDocument(), archive, file);
		}

		// Index archive (always last)
		indexArchive(foundArchive == null ? new ArchiveDocument() : foundArchive, archive);

		// Flush last bulk requests
		sendBulk();
	}

	private <T extends BaseResult> T waitFor(ESPreparedRequest<T> pr)
		throws CancellationException, ExecutionException, InterruptedException, TimeoutException {
		return pr.submit().get(msTimeout, TimeUnit.MILLISECONDS);
	}

	private void deleteByTermQuery(String idField, String id)
		throws CancellationException, ExecutionException, InterruptedException, TimeoutException {
		sendBulk();
		final DeleteByQueryRequest dq = es.deleteByQuery(index);
		dq.dsl().with("term").put(idField, id);
		waitFor(dq);
	}

	private BulkRequest getBulk()
		throws CancellationException, ExecutionException, InterruptedException, TimeoutException {
		if (currentBulk != null && currentBulk.size() >= maxBulk) {
			sendBulk();
		}
		if (currentBulk == null)
			currentBulk = es.bulk();
		return currentBulk;
	}

	private void sendBulk() throws CancellationException, ExecutionException, InterruptedException, TimeoutException {
		if (currentBulk == null || currentBulk.isEmpty())
			return;
		log.debug("[{}] Sending bulk request ({} items)", taskKey, currentBulk.size());
		final ESBulkResult br = waitFor(currentBulk);
		if (br.hasErrors())
			throw new RuntimeException("Bulk request failed: " + br.getResult());
		currentBulk = null;
	}

	private void checkInterrupted() throws InterruptedException {
		if (Thread.interrupted())
			throw new InterruptedException();
	}

	/**
	 * Fill in fields for a given archive. The {@link IndexDocument} may be empty
	 * for new archives.
	 *
	 * @throws TimeoutException
	 * @throws InterruptedException
	 * @throws ExecutionException
	 * @throws CancellationException
	 */
	private void indexArchive(ArchiveDocument doc, ArchiveInfo archive)
		throws CancellationException, ExecutionException, InterruptedException, TimeoutException {
		doc.id = archive.id;
		doc.vault = archive.vault;
		doc.owner = archive.owner;
		doc.tags = new HashSet<>();
		doc.created = archive.created;
		doc.modified = archive.modified;
		doc.read_access = readAccessForArchive;
		doc.size = allFiles.stream().mapToLong(f -> f.size).sum();
		doc.revision = archive.revision;
		doc.profile = archive.profile;
		copyMetaAttributes(doc, archive.meta);

		log.debug("[{}] Indexing archive", taskKey);

		getBulk().index(index, archive.id, doc);
	}

	/**
	 * Fill in fields for a given file.
	 *
	 * The {@link IndexDocument} may be completely empty for new files, or partly
	 * filled for files that may need an update.
	 *
	 * @param file
	 * @throws InterruptedException
	 * @throws TimeoutException
	 * @throws ExecutionException
	 * @throws CancellationException
	 * @throws IOException
	 */
	private void indexFile(FileDocument doc, ArchiveInfo archive, FileInfo file)
		throws IOException, CancellationException, ExecutionException, InterruptedException, TimeoutException {

		if (file == null) {
			log.debug("[{}] Removing file from index: {}", taskKey, doc.fid);
			getBulk().delete(index, doc.getIndexID());
			return;
		}

		final boolean isNew = doc.id == null;
		final boolean contentModified = isNew || !file.digests.sha256.equals(doc.sha256);
		final boolean contentAvailable = archive.state.isReadable();

		// TODO: We currently won't detect ACL or metdata changes, so we have to update
		// all files every time :(

		// Build entire document (excluding content)
		doc.id = archive.id;
		doc.vault = archive.vault;
		doc.owner = archive.owner;
		doc.tags = new HashSet<>();
		doc.created = file.created;
		doc.modified = file.modified;
		doc.read_access = readAccessForFiles;
		doc.size = file.size;

		copyMetaAttributes(doc, file.meta);

		doc.fid = file.id;
		doc.type = file.type;
		doc.sha256 = file.digests.sha256;
		doc.name = file.name;

		final int i = doc.name.lastIndexOf('/');
		doc.filename = (i > -1) ? doc.name.substring(i + 1) : doc.name;

		log.debug("[{}] Indexing file: {} ({})", taskKey, doc.fid, doc.type);

		// Calculate doc.content, if needed and available
		if (file.size == 0) {
			doc.content = "";
		} else if (!mayContainText(file)) {
			doc.content = "";
			doc.tags.add("not_analysed");
		} else if (contentModified && contentAvailable) {
			final ParseResult parsed = extractText(file);
			if (parsed.isTruncated())
				log.debug("[{}] Text truncated to {} bytes", taskKey, parsed.getText().length());
			else if (parsed.hasError())
				log.warn("[{}] Text extraction failed", taskKey, parsed.getError());
			// TODO: Warn if detected mime type does not match real mime type
			doc.content = parsed.getText();
		} else if (contentModified) {
			// Keep current value, but mark the archive as stale
			doc.tags.add("stale");
		}

		if (doc.content != null || isNew) {
			// We can (or must) index the whole document
			getBulk().index(index, doc.getIndexID(), doc);
		} else {
			// We update all but the content. This requires a small painless-script because
			// the default update behavior would merge old and new sub-document (e.g. meta).
			final ObjectNode json = es.json();
			json.with("script").put("lang", "painless");
			final String script = "for(key in params.keySet()) {if(key != \"ctx\") ctx._source[key] = params[key];}";
			json.with("script").put("source", script);
			json.with("script").putPOJO("params", doc);
			getBulk().update(index, doc.getIndexID(), json);
		}
	}

	private boolean mayContainText(FileInfo file) {
		if (file.size == 0)
			return false;

		final String ftype = file.type != null ? file.type : "application/octet-stream";

		// Whitelist
		if (ftype.startsWith("text/"))
			return true;

		// Blacklist
		if (ftype.startsWith("image/") || ftype.startsWith("video/") || ftype.startsWith("audio/"))
			return false;

		// default
		return true;
	}

	private ParseResult extractText(FileInfo file) throws IOException {
		final ParseResult parsed;
		try (InputStream stream = cdstar.execute(
			new GetFileStream(archive.vault, archive.id, file.name)
				.range(0, tika.getTextLimit()))) {
			parsed = tika.parse(stream, file.name, file.type);
		}
		return parsed;
	}

	private void copyMetaAttributes(IndexDocument doc, MetaMap meta) {
		doc.meta = new HashMap<>();
		for (final Entry<String, List<String>> e : meta.getMap().entrySet()) {
			doc.meta.put(e.getKey(), e.getValue());
		}
	}

}
