package de.gwdg.cdstar.ext.elastic.cli;

import de.gwdg.cdstar.runtime.Config;

public abstract class BaseCommand {

	public abstract void run(Config cfg) throws Exception;

}
