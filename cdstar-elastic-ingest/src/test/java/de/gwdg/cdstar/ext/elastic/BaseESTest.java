package de.gwdg.cdstar.ext.elastic;

import static org.junit.Assert.assertNotNull;

import java.net.URI;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assume;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.rules.TemporaryFolder;

import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.auth.UsernamePasswordCredentials;
import de.gwdg.cdstar.auth.error.LoginFailed;
import de.gwdg.cdstar.auth.simple.SimpleAuthorizer;
import de.gwdg.cdstar.client.CDStarClientConfig;
import de.gwdg.cdstar.client.CDStarRestClient;
import de.gwdg.cdstar.config.MapConfig;
import de.gwdg.cdstar.rest.servlet.CDStarServletInitializer;
import de.gwdg.cdstar.runtime.CDStarRuntime;
import de.gwdg.cdstar.runtime.client.CDStarSession;
import de.gwdg.cdstar.server.jetty.JettyServer;
import de.gwdg.cdstar.utils.test.TestLogger;

public abstract class BaseESTest {

	protected static CDStarRuntime runtime;

	@ClassRule
	public static TemporaryFolder tmp = new TemporaryFolder();

	@Rule
	public TestLogger log = new TestLogger("de.gwdg");

	private static JettyServer server;

	@BeforeClass
	public static void beforeClass() throws Throwable {
		new TestLogger("de.gwdg").run(() -> {

			// CDSTAR Runtime + activemq sink
			final MapConfig config = new MapConfig();
			config.set("path.home", tmp.newFolder().toString());
			config.set("vault.test.create", "true");
			config.set("vault.public.create", "true");
			config.set("vault.public.public", "true");

			runtime = CDStarRuntime.bootstrap(config);
			runtime.start();

			final SimpleAuthorizer realm = runtime.lookupRequired(SimpleAuthorizer.class);
			assertNotNull("Runtime should install default realm", realm);
			realm.account("test").password("test").withPermissions("vault:*:*");
			realm.account("es")
				.password("es")
				.withPermissions("vault:*:*", "archive:*:*:load", "archive:*:*:read_acl", "archive:*:*:read_meta",
					"archive:*:*:read_files", "archive:*:*:list_files");

			server = new JettyServer("localhost", 0);
			server.addComponent(new CDStarServletInitializer(runtime));
			server.start();
		});

	}

	protected CDStarSession getTestUserSession() throws LoginFailed {
		return runtime.getClient(new UsernamePasswordCredentials("test", "test".toCharArray())).begin(false);
	}

	protected URI getCDStarUri() {
		return URI.create(server.getURI().toString() + "v3/");
	}

	protected ElasticSearchClient esClient;
	protected CDStarRestClient cdClient;

	@Before
	public void beforeTest() {
		var esUri = System.getenv("TEST_ES_URI");
	    Assume.assumeTrue("Environment variable TEST_ES_URI not defined. Skipping test ...", esUri != null);
		esClient = new ElasticSearchClient(URI.create(esUri));
		esClient.resolveVersion();
		cdClient = new CDStarRestClient(new CDStarClientConfig(getCDStarUri()).basicAuth("es", "es"));
	}

	@After
	public void afterTest() {
		if(esClient != null)
			Utils.closeQuietly(esClient.deleteIndex("test")::execute);
		Utils.closeQuietly(esClient);
		Utils.closeQuietly(cdClient);
	}

	@AfterClass
	public static void afterClass() throws Throwable {
		new TestLogger("de.gwdg").run(() -> {
			runtime.close();
		});
	}
}
