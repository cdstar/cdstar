package de.gwdg.cdstar.ext.elastic;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.WritableByteChannel;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;

import de.gwdg.cdstar.event.ChangeEvent;
import de.gwdg.cdstar.ext.elastic.SearchRequest.ESSearchResult;
import de.gwdg.cdstar.ext.elastic.dao.ArchiveDocument;
import de.gwdg.cdstar.ext.elastic.dao.FileDocument;
import de.gwdg.cdstar.runtime.client.CDStarArchive;
import de.gwdg.cdstar.runtime.client.CDStarFile;
import de.gwdg.cdstar.runtime.client.CDStarSession;
import journal.io.api.ClosedJournalException;
import journal.io.api.CompactedDataFileException;

public class HalfStackIT extends BaseESTest {

	private static final int DEFAULT_TIMEOUT = 30000;
	private ElasticIngest ingest;

	@Before
	public void startIngest() throws ClosedJournalException, CompactedDataFileException, IOException, Exception {
		ingest = new ElasticIngest(tmp.newFolder().toPath(), cdClient, esClient);
		ingest.start();
	}

	@After
	public void stopIngest() throws InterruptedException {
		if (ingest != null)
			ingest.stop();
	}

	private void writeString(CDStarFile file, String data) throws IOException {
		try (WritableByteChannel ch = file.getWriteChannel()) {
			ch.write(ByteBuffer.wrap(data.getBytes(StandardCharsets.UTF_8)));
		}
	}

	@Test
	public void testBasicIngestNewArchive() throws Exception {
		CDStarArchive ar;
		CDStarFile f;
		try (CDStarSession sess = getTestUserSession()) {
			ar = sess.getVault("test").createArchive();
			ar.setAttribute("dc:title", "My Archive");
			f = ar.createFile("test.txt");
			writeString(f, "Hello World");
			f.setAttribute("dc:title", "My File");
			sess.commit();
		}
		final ChangeEvent event = new ChangeEvent(ar);
		submitAndRefresh(event);

		final ESSearchResult result = searchAll(event.getVault());
		assertEquals(2, result.getTotal());

		final ArchiveDocument archiveDoc = fetchArchive(event.getVault(), ar.getId());
		final FileDocument fileDoc = fetchFile(event.getVault(), ar.getId(), f.getID());

		assertNotNull(archiveDoc);
		assertNotNull(fileDoc);

		assertEquals("My Archive", archiveDoc.meta.get("dc:title").get(0));
		assertEquals("My File", fileDoc.meta.get("dc:title").get(0));
		assertEquals("Hello World", fileDoc.content);

		// TODO: Check more stuff here
	}

	@Test
	public void testSearchFields() throws Exception {
		CDStarArchive ar, ar2;
		CDStarFile f;
		try (CDStarSession sess = getTestUserSession()) {
			ar = sess.getVault("test").createArchive();
			ar.setAttribute("dc:title", "My Archive");
			f = ar.createFile("test.txt");
			writeString(f, "Hello World");
			f.setAttribute("dc:title", "My File");

			ar2 = sess.getVault("test").createArchive();
			ar2.setAttribute("dc:title", "My other archive");
			f = ar2.createFile("some/path.txt");
			writeString(f, "Hello CDSTAR");
			f.setAttribute("dc:title", "My other File");

			sess.commit();
		}
		submit(new ChangeEvent(ar));
		submit(new ChangeEvent(ar2));
		refresh("test");

		assertEquals(4, searchAll("test").getTotal());
		assertEquals(4, search("test", "*:*").getTotal());
		assertEquals(1, search("test", "CDSTAR").getTotal());
		assertEquals(1, search("test", "dcTitle:\"My other File\"").getTotal());
		assertEquals(1, search("test", "meta.dc\\:title:\"My other File\"").getTotal());
		assertEquals(1, search("test", "\"My other File\"").getTotal());
		assertEquals(2, search("test", "+My +File").getTotal());
		assertEquals(4, search("test", "My").getTotal());
		assertEquals(4, search("test", "dcTitle:My").getTotal());
		assertEquals(0, search("test", "dcAuthor:My").getTotal());
		assertEquals(2, search("test", "scope:archive").getTotal());
		assertEquals(2, search("test", "profile:default").getTotal());
	}

	@Test
	public void testUpdateArchive() throws Exception {
		CDStarArchive ar;
		CDStarFile f;

		try (CDStarSession sess = getTestUserSession()) {
			ar = sess.getVault("test").createArchive();
			ar.setAttribute("dc:title", "My Archive");
			f = ar.createFile("test.txt");
			writeString(f, "Hello World");
			f.setAttribute("dc:title", "My File");
			sess.commit();
		}
		final ChangeEvent event = new ChangeEvent(ar);
		submitAndRefresh(event);

		try (CDStarSession sess = getTestUserSession()) {
			ar = sess.getVault("test").loadArchive(ar.getId());
			ar.setAttribute("dc:title", "My Archive 2");
			f = ar.getFile("test.txt");
			f.truncate(0);
			writeString(f, "Hello World 2");
			f.setAttribute("dc:title", "My File 2");
			sess.commit();
		}

		final ChangeEvent event2 = new ChangeEvent(ar);
		submitAndRefresh(event2);

		final ESSearchResult result = searchAll(event.getVault());
		assertEquals(2, result.getTotal());

		final ArchiveDocument archiveDoc = fetchArchive(event.getVault(), ar.getId());
		final FileDocument fileDoc = fetchFile(event.getVault(), ar.getId(), f.getID());

		assertNotNull(archiveDoc);
		assertNotNull(fileDoc);

		assertEquals("My Archive 2", archiveDoc.meta.get("dc:title").get(0));
		assertEquals("My File 2", fileDoc.meta.get("dc:title").get(0));
		assertEquals("Hello World 2", fileDoc.content);
		// TODO: Check more stuff here
	}

	@Test
	public void testForceUpdate() throws Exception {
		CDStarArchive ar;
		CDStarFile f;

		try (CDStarSession sess = getTestUserSession()) {
			ar = sess.getVault("test").createArchive();
			ar.setAttribute("dc:title", "My Archive");
			f = ar.createFile("test.txt");
			writeString(f, "Hello World");
			f.setAttribute("dc:title", "My File");
			sess.commit();
		}
		ingest.forceUpdate("test", ar.getId()).get(DEFAULT_TIMEOUT, TimeUnit.MILLISECONDS);
		refresh("test");

		final ESSearchResult result = searchAll("test");
		assertEquals(2, result.getTotal());
	}

	@Test
	public void testRemoveArchive() throws Exception {
		CDStarArchive ar;
		CDStarFile f;
		try (CDStarSession sess = getTestUserSession()) {
			ar = sess.getVault("test").createArchive();
			ar.setAttribute("dc:title", "My Archive");
			f = ar.createFile("test.txt");
			writeString(f, "Hello World");
			f.setAttribute("dc:title", "My File");
			sess.commit();
		}

		final ChangeEvent event = new ChangeEvent(ar);
		submitAndRefresh(event);
		final ESSearchResult result = searchAll("test");
		assertEquals(2, result.getTotal());

		try (CDStarSession sess = getTestUserSession()) {
			ar = sess.getVault("test").loadArchive(ar.getId());
			ar.remove();
			sess.commit();
		}

		submitAndRefresh(new ChangeEvent(ar));
		assertEquals(0, searchAll("test").getTotal());
	}

	@Test
	public void testRemoveFile() throws Exception {
		CDStarArchive ar;
		CDStarFile f;
		try (CDStarSession sess = getTestUserSession()) {
			ar = sess.getVault("test").createArchive();
			ar.setAttribute("dc:title", "My Archive");
			f = ar.createFile("test.txt");
			writeString(f, "Hello World");
			f.setAttribute("dc:title", "My File");
			sess.commit();
		}

		final ChangeEvent event = new ChangeEvent(ar);
		submitAndRefresh(event);
		final ESSearchResult result = searchAll("test");
		assertEquals(2, result.getTotal());

		try (CDStarSession sess = getTestUserSession()) {
			ar = sess.getVault("test").loadArchive(ar.getId());
			ar.getFile("test.txt").remove();
			sess.commit();
		}

		submitAndRefresh(new ChangeEvent(ar));
		assertEquals(1, searchAll("test").getTotal());
	}

	void submit(ChangeEvent event)
		throws CancellationException, ExecutionException, InterruptedException, TimeoutException, IOException {
		ingest.submit(event).get(DEFAULT_TIMEOUT, TimeUnit.MILLISECONDS);
	}

	void refresh(String indexName)
		throws CancellationException, ExecutionException, InterruptedException, TimeoutException {
		esClient.refresh(indexName).submit().get(DEFAULT_TIMEOUT, TimeUnit.MILLISECONDS);
	}

	void submitAndRefresh(ChangeEvent event)
		throws CancellationException, ExecutionException, InterruptedException, TimeoutException, IOException {
		submit(event);
		refresh(event.getVault());
	}

	ESSearchResult searchAll(String index)
		throws CancellationException, ExecutionException, InterruptedException, TimeoutException {
		final SearchRequest s = esClient.search(index);
		s.dsl().with("match_all");
		return s.submit().get(DEFAULT_TIMEOUT, TimeUnit.MILLISECONDS);
	}

	ESSearchResult search(String index, String query)
		throws CancellationException, ExecutionException, InterruptedException, TimeoutException {
		final SearchRequest s = esClient.search(index);
		s.dsl().with("query_string").put("query", query);
		return s.submit().get(DEFAULT_TIMEOUT, TimeUnit.MILLISECONDS);
	}

	ArchiveDocument fetchArchive(String index, String archiveId) throws CancellationException, ExecutionException,
		InterruptedException, TimeoutException, JsonProcessingException {
		return esClient.fetch(index, archiveId).submit()
			.get(DEFAULT_TIMEOUT, TimeUnit.MILLISECONDS)
			.source(ArchiveDocument.class);
	}

	FileDocument fetchFile(String index, String archiveId, String fileId) throws CancellationException,
		ExecutionException, InterruptedException, TimeoutException, JsonProcessingException {
		return esClient.fetch(index, FileDocument.fullId(archiveId, fileId)).submit()
			.get(DEFAULT_TIMEOUT, TimeUnit.MILLISECONDS)
			.source(FileDocument.class);
	}

}
