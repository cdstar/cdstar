package de.gwdg.cdstar.common;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import java.util.Arrays;

import org.junit.Test;

import de.gwdg.cdstar.Utils;

public class UtilsTest {

	@Test
	public void testSplitTrimSpace() throws Exception {
		assertEquals(Arrays.asList("a", "b", "c"), Utils.splitTrimSpace("a,b,c", ','));
		assertEquals(Arrays.asList("a", "b", "c"), Utils.splitTrimSpace(" a  ,   b    ,     c     ", ','));
		assertEquals(Arrays.asList("a", "b", "c"), Utils.splitTrimSpace("a ,b ,c", ','));
		assertEquals(Arrays.asList("a", "b", "c"), Utils.splitTrimSpace("a, b, c", ','));
		assertEquals(Arrays.asList("a", "b", "c"), Utils.splitTrimSpace("a, b ,c", ','));
		assertEquals(Arrays.asList("a", "b", "c"), Utils.splitTrimSpace("a ,b, c", ','));
		assertEquals(Arrays.asList("a"), Utils.splitTrimSpace("a", ','));
		assertEquals(Arrays.asList("a"), Utils.splitTrimSpace(" a", ','));
		assertEquals(Arrays.asList("a"), Utils.splitTrimSpace("a ", ','));
		assertEquals(Arrays.asList("a"), Utils.splitTrimSpace(" a ", ','));
		assertEquals(Arrays.asList("a", ""), Utils.splitTrimSpace("a,", ','));
		assertEquals(Arrays.asList("a", ""), Utils.splitTrimSpace(" a , ", ','));
		assertEquals(Arrays.asList("", "a"), Utils.splitTrimSpace(",a", ','));
		assertEquals(Arrays.asList("", "a"), Utils.splitTrimSpace(" , a ", ','));
		assertEquals(Arrays.asList("", "a", ""), Utils.splitTrimSpace(",a,", ','));
		assertEquals(Arrays.asList("", "a", ""), Utils.splitTrimSpace(" , a , ", ','));
		assertEquals(Arrays.asList("", ""), Utils.splitTrimSpace(",", ','));
		assertEquals(Arrays.asList("", ""), Utils.splitTrimSpace(" , ", ','));
	}

	public void hexEncodeDecode() throws Exception {
		final byte[] bytes = new byte[] { -128, -127, -1, 0, 1, 126, 127 };
		assertEquals("8081ff00017e7f", Utils.bytesToHex(bytes));
		assertArrayEquals(bytes, Utils.hexToBytes("8081ff00017e7f"));
	}

	@Test(expected = IllegalArgumentException.class)
	public void hexDecodeHalfByteError() throws Exception {
		Utils.hexToBytes("001122334");
	}

	@Test(expected = IllegalArgumentException.class)
	public void hexDecodeIllegalCharacterError() throws Exception {
		Utils.hexToBytes("g");
	}

	@Test(expected = IllegalArgumentException.class)
	public void hexDecodeNullCharacterError() throws Exception {
		Utils.hexToBytes("\0");
	}

}
