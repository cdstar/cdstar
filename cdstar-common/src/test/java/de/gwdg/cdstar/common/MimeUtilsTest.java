package de.gwdg.cdstar.common;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import de.gwdg.cdstar.MimeUtils;
import de.gwdg.cdstar.MimeUtils.MimeGuess;

public class MimeUtilsTest {

	private void assertMime(String mime, String encoding, MimeGuess test) {
		assertEquals(mime, test.getMime());
		assertEquals(encoding, test.getEncoding());
	}

	@Test
	public void lotsOfTests() {
		assertMime("text/plain", null, MimeUtils.guess("file.txt"));
		assertMime("text/plain", null, MimeUtils.guess("file..txt"));
		assertMime(null, null, MimeUtils.guess("file."));
		assertMime(null, null, MimeUtils.guess("no-extention-at-all"));
		assertMime("text/plain", "gzip", MimeUtils.guess("file.txt.gz"));
		assertMime("application/x-tar", "gzip", MimeUtils.guess("file.tar.gz"));
		assertMime("application/x-tar", "gzip", MimeUtils.guess("file.tgz"));
		assertMime(null, "gzip", MimeUtils.guess("file.gz"));
		assertMime(null, "gzip", MimeUtils.guess("file..gz"));
		assertMime("application/x-gzip", "gzip", MimeUtils.guess("file.gz.gz"));
		assertMime(null, "gzip", MimeUtils.guess("file.junk.gz"));

		assertMime("text/plain", null, MimeUtils.guess("file.txt", false));
		assertMime("text/plain", null, MimeUtils.guess("file..txt", false));
		assertMime(null, null, MimeUtils.guess("file.", false));
		assertMime(null, null, MimeUtils.guess("no-extention-at-all", false));
		assertMime("application/x-gzip", null, MimeUtils.guess("file.txt.gz", false));
		assertMime("application/x-gzip", null, MimeUtils.guess("file.tar.gz", false));
		assertMime("application/x-gzip", null, MimeUtils.guess("file.tgz", false));
		assertMime("application/x-gzip", null, MimeUtils.guess("file.gz", false));
		assertMime("application/x-gzip", null, MimeUtils.guess("file..gz", false));
		assertMime("application/x-gzip", null, MimeUtils.guess("file.gz.gz", false));
		assertMime("application/x-gzip", null, MimeUtils.guess("file.junk.gz", false));
	}

}
