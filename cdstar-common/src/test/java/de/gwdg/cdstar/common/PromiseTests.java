package de.gwdg.cdstar.common;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicLong;

import org.junit.Test;

import de.gwdg.cdstar.Promise;

public class PromiseTests {

	@Test
	public void testFlatMap() throws Exception {
		assertEquals(6,
			Promise.of("foo")
				.map(f -> f + "bar")
				.flatMap(s -> Promise.of(s).map(String::length))
				.await()
				.get()
				.intValue());
	}

	@Test
	public void testParallelExecution() throws Exception {
		final Promise<String> pSync = Promise.empty();
		final Promise<String> pParallel = pSync.parallel();

		final AtomicLong tIdS = new AtomicLong();
		final AtomicLong tIdP = new AtomicLong();
		final AtomicLong tIdP2 = new AtomicLong();
		pSync.then(s -> tIdS.set(Thread.currentThread().getId()));
		pParallel.then(s -> tIdP.set(Thread.currentThread().getId()));
		pSync.resolve("foo");
		pParallel.then(s -> tIdP2.set(Thread.currentThread().getId()));

		// Ensure that the synchronous callback is called in this thread, but
		// the parallel callbacks are executed in a different thread, even if
		// the promise was already resolved.
		assertEquals(Thread.currentThread().getId(), tIdS.get());
		assertTrue(Thread.currentThread().getId() != tIdP.get());
		assertTrue(Thread.currentThread().getId() != tIdP2.get());
	}

	@Test
	public void testMap() throws Exception {
		assertEquals(3, Promise.of("foo").map(f -> f.length()).get().intValue());
		assertEquals(6, Promise.of("foo").map(f -> f.length()).map(i -> i * 2).get().intValue());
	}

	@Test
	public void testReject() throws Exception {
		final RuntimeException error = new RuntimeException("nooo :(");
		final Promise<Void> p = Promise.empty();

		p.then((val, err) -> {
			assertEquals(error, err);
			assertNull(val);
		});

		p.reject(error);

		p.then((val, err) -> {
			assertEquals(error, err);
			assertNull(val);
		});

		assertTrue(p.isRejected());
		assertTrue(p.isCompleted());
		assertTrue(!p.isOpen());
		assertTrue(!p.isResolved());

		try {
			p.get();
			fail("Expected exception");
		} catch (final ExecutionException e) {
			assertEquals(error, e.getCause());
		}
	}

	@Test
	public void testExpect() throws Exception {
		final Promise<String> p = Promise.empty();
		final Promise<String> p2 = p.expect(Throwable::getMessage);

		p.then((val, err) -> {
			assertNotNull(err);
			assertEquals("Noo!", err.getMessage());
		});

		p2.then((val, err) -> {
			assertEquals("Noo!", val);
			assertNull(err);
		});

		p.reject(new RuntimeException("Noo!"));
	}

	@Test
	public void testThenResolved() throws Exception {
		final List<String> results = new ArrayList<>();
		final List<Throwable> errors = new ArrayList<>();
		Promise.of("foo")
			.then(s -> results.add(s))
			.then(results::add, errors::add)
			.then((val, err) -> {
				results.add(val);
				errors.add(err);
			});

		assertEquals(Arrays.asList("foo", "foo", "foo"), results);
		assertEquals(Arrays.asList((Throwable) null), errors);
	}

	@Test
	public void testThenRejected() throws Exception {
		final List<String> results = new ArrayList<>();
		final List<Throwable> errors = new ArrayList<>();
		final RuntimeException error = new RuntimeException("meh");
		Promise.<String>empty()
			.reject(error)
			.then(s -> results.add(s))
			.then(results::add, errors::add)
			.then((val, err) -> {
				results.add(val);
				errors.add(err);
			});

		assertEquals(Arrays.asList((String) null), results);
		assertEquals(Arrays.asList(error, error), errors);
	}

	@Test
	public void testErrorInMapFunction() throws Exception {
		final Promise<String> p = Promise.of("foo");
		assertEquals("foo", p.get());
		assertTrue(p.map(f -> 1 / 0).isRejected());
		assertTrue(p.map(f -> 1 / 0).error() instanceof ArithmeticException);
	}

	@Test
	public void testErrorInErrorMapFunction() throws Exception {
		final Promise<String> p = Promise.empty();
		final RuntimeException error = new RuntimeException("meh");
		final RuntimeException error2 = new RuntimeException("meh meh");
		p.reject(error);
		assertEquals(error, p.error());
		assertEquals(error2, p.expect(err -> {
			throw error2;
		}).error());
		assertEquals(error, p.expect(err -> {
			throw err;
		}).error());
	}

	@Test
	public void testErrorInFlatMapFunction() throws Exception {
		final Promise<String> p = Promise.of("foo");
		assertTrue(p.flatMap(f -> Promise.of(1 / 0)).isRejected());
		assertTrue(p.flatMap(f -> Promise.of(1 / 0)).error() instanceof ArithmeticException);

		final Promise<String> p2 = Promise.ofError(new RuntimeException("meh"));
		assertTrue(p2.flatMap(null, err -> Promise.of(1 / 0)).isRejected());
		assertTrue(p2.flatMap(null, err -> Promise.of(1 / 0)).error() instanceof ArithmeticException);
	}
}
