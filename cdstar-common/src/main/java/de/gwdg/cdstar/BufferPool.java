package de.gwdg.cdstar;

import java.nio.ByteBuffer;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.concurrent.atomic.LongAdder;

/**
 * Pool for re-usable {@link ByteBuffer} instances.
 */
public class BufferPool {

	private int bufferSize = 1024 * 8;
	private int poolSize = 16;
	private final Deque<ByteBuffer> pool = new ArrayDeque<>(poolSize);

	private final LongAdder recycledCounter = new LongAdder();
	private final LongAdder allocatedCounter = new LongAdder();

	private static BufferPool shared = new BufferPool(16, 1024 * 64);

	public BufferPool(int poolSize, int bufferSize) {
		setPoolSize(poolSize);
		setBufferSize(bufferSize);
	}

	public static BufferPool getSharedPool() {
		return shared;
	}

	public int getPoolSize() {
		return poolSize;
	}

	public void setPoolSize(int newSize) {
		poolSize = newSize;
	}

	public int getBufferSize() {
		return bufferSize;
	}

	public void setBufferSize(int readBuffer) {
		if (readBuffer != bufferSize) {
			synchronized (pool) {
				bufferSize = readBuffer;
				pool.clear();
			}
		}
	}

	public long getRecycleCount() {
		return recycledCounter.sum();
	}

	public long getAllocateCount() {
		return allocatedCounter.sum();
	}

	/**
	 * Return a recycled, if available. Otherwise allocate and return a new
	 * buffer.
	 *
	 * Recycled buffers are cleared, but not zeroed out. They may still contain
	 * stale data from a previous use.
	 */
	public ByteBuffer getBuffer() {
		final ByteBuffer buf;
		synchronized (pool) {
			// We poll from end (FiLo) to increase the chance the buffer is
			// still 'warm'.
			buf = pool.pollLast();
		}
		if (buf == null) {
			allocatedCounter.add(1);
			return ByteBuffer.allocate(bufferSize);
		} else {
			recycledCounter.add(1);
			return buf;
		}
	}

	/**
	 * Recycle a buffer. Return true if it was recycled, false otherwise.
	 * Buffers may be rejected if the recycle pool is full, or if the buffer
	 * does not meet capacity requirements.
	 *
	 * Recycled buffers are cleared, but not zeroed out. Do not recycle buffers
	 * with sensitive information. Do not keep references to recycled buffers.
	 */
	public boolean recycleBuffer(ByteBuffer buf) {
		if (buf.hasArray() && buf.arrayOffset() == 0 && buf.capacity() == bufferSize) {
			synchronized (pool) {
				if (pool.size() < poolSize) {
					buf.clear();
					pool.addLast(buf);
					return true;
				}
			}
		}
		return false;
	}
}
