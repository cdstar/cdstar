package de.gwdg.cdstar;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public class LRUCache<K, V> extends LinkedHashMap<K, V> {
	private static final long serialVersionUID = 1742009069542139233L;
	private static AtomicInteger counter = new AtomicInteger(0);

	private int cacheSize;
	private String name;

	public LRUCache(String name, int cacheSize) {
		super(16, 0.75f, true);
		this.name = name;
		setCapacity(cacheSize);
	}

	public LRUCache(int cacheSize) {
		this("lru-" + counter.getAndIncrement(), cacheSize);
	}

	public String getName() {
		return name;
	}

	public int getCapacity() {
		return cacheSize;
	}

	public void setCapacity(int cap) {
		cacheSize = Math.max(0, cap);
	}

	@Override
	protected boolean removeEldestEntry(Map.Entry<K, V> eldest) {
		return size() > cacheSize;
	}

}
