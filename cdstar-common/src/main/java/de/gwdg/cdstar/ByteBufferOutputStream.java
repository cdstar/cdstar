package de.gwdg.cdstar;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.util.Arrays;

/**
 * Similar to {@link ByteArrayOutputStream}, but saves an expensive copy
 * operation by returning a {@link ByteBuffer} wrapping the internal byte array.
 */
public class ByteBufferOutputStream extends OutputStream {

	byte[] buffer;
	int bytes = 0;

	private final int minBufferSize;

	public ByteBufferOutputStream(int minBufferSize) {
		this.minBufferSize = minBufferSize;
		buffer = new byte[minBufferSize];
	}

	public ByteBufferOutputStream() {
		this(1024 * 8);
	}

	@Override
	public void write(int b) throws IOException {
		ensureCapacity(bytes + 1);
		buffer[bytes] = (byte) b;
		bytes += 1;
	}

	@Override
	public void write(byte[] b) throws IOException {
		write(b, 0, b.length);
	}

	@Override
	public void write(byte[] b, int off, int len) throws IOException {
		ensureCapacity(bytes + len);
		System.arraycopy(b, off, buffer, bytes, len);
		bytes += len;
	}

	/**
	 * Grow by doubling the size of the buffer. If that would overflow, grow only as
	 * much as absolutely needed.
	 */
	private void ensureCapacity(int minCapacity) {
		if (minCapacity > buffer.length) {
			final int newCapacity = buffer.length <= Integer.MAX_VALUE >> 1
				? buffer.length << 1
				: minCapacity;
			buffer = Arrays.copyOf(buffer, newCapacity);
		} else if (minCapacity < bytes) {
			// minCapacity overflow.
			throw new OutOfMemoryError();
		}
	}

	/**
	 * Return the current buffer wrapped in a {@link ByteBuffer} and immediately
	 * replace the internal buffer with a fresh one.
	 */
	public ByteBuffer popBytes() {
		final ByteBuffer out = ByteBuffer.wrap(buffer, 0, bytes);
		bytes = 0;
		buffer = new byte[minBufferSize];
		return out;
	}

	public int getBufferSize() {
		return bytes;
	}

}
