package de.gwdg.cdstar;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;

/**
 * A simple alternative to {@link CompletableFuture} and maybe {@link Optional}.
 * The API is heavily inspired by promises from other languages and aims to be
 * smaller, clearer and less verbose than {@link CompletableFuture}, while still
 * covering the most common used cases.
 *
 * You can switch between {@link Promise} and {@link CompletableFuture} APIs via
 * {@link #wrap(CompletableFuture)} and {@link #toCompletableFuture()} as
 * needed.
 *
 * Methods from the 'then' family register listeners to the current promise and
 * are fired as soon as the promise is resolved or rejected, but do not change
 * the outcome of the promise. Exceptions are silently ignored and the execution
 * order is undefined.
 *
 * Methods from the {@link #map(ResultMapper, ErrorMapper)},
 * {@link #flatMap(ResultMapper, ErrorMapper)} and {@link #expect(ErrorMapper)}
 * families as well as {@link #or(Promise)} and
 * {@link #merge(Promise, ResultMerger)} change the outcome of a promise and
 * therefore always return a new promise. Canceling a child promises created by
 * one these methods will also try to cancel the parent promises. Other state
 * changes are not propagated upstream.
 *
 * You should therefore always resolve or reject the root promise of a chain in
 * order to reject/resolve all children, but may cancel any promise in the chain
 * to cancel all promises involved. {@link #split()} can be used to prevent a
 * cancel request to propagate upstream.
 *
 * Synchronous promises will execute all registered listeners, mapping functions
 * or error handlers during the first {@link #reject(Throwable)} or
 * {@link #resolve(Object)} call. Blocking listeners may therefore block the
 * calling thread. If you need to mix blocking and non-blocking APIs, use
 * parallel promises or make sure that listeners never block.
 *
 * Parallel promises will send listeners to a separate thread pool for
 * execution, which adds a little overhead but ensures that a blocking listener
 * won't block any other thread.
 *
 *
 * Best practices:
 *
 * Promises are owned by the class that created them, and should not be
 * resolved, rejected or canceled by anyone else unless clearly stated in the
 * documentation of the owning class. A common use case where this is desirable
 * would be a long-running task that can be canceled if the result is no longer
 * needed.
 *
 * If the promise represents a cancel-able task and the user is allowed to
 * reject it, or if the promise depends on a cancel-able tasks result, the owner
 * should check if the promise was rejected (e.g. with a
 * {@link CancellationException}) before doing any significant work.
 *
 * Try to avoid blocking or long-running work in listeners, mapping functions or
 * error handlers registered to non-parallel promise, as these may block a
 * calling thread that does not expect them to block.
 */
public class Promise<T> {

	private static ExecutorService defaultPool = ForkJoinPool.commonPool();

	private T value;
	private Throwable error;
	boolean completed = false;
	private ExecutorService pool;
	private final List<PromiseHandler<T>> handlers = new ArrayList<>(1);

	/**
	 * Change the default pool used by {@link #parallel()} calls without an
	 * explicit pool. By default, {@link ForkJoinPool#commonPool()} is used.
	 */
	public static void setDefaultPool(ExecutorService pool) {
		Promise.defaultPool = pool;
	}

	/**
	 * Create a synchronous promise.
	 */
	Promise() {
	}

	/**
	 * Create a parallel promise.
	 */
	Promise(ExecutorService pool) {
		if (pool == null)
			throw new NullPointerException();
		this.pool = pool;
	}

	/**
	 * Submit a task to a thread pool and return a promise for the result. The
	 * returned promise will be parallel.
	 *
	 * @param task
	 *            Supplier to compute the value
	 * @param pool
	 *            Thread pool to use, or null to use the default pool
	 * @return a new parallel promise.
	 */
	public static <V> Promise<V> submit(Callable<V> task, ExecutorService pool) {
		final Promise<V> p = new Promise<>(pool == null ? defaultPool : pool);
		p.pool.submit(() -> {
			try {
				p.resolve(task.call());
			} catch (final Exception e) {
				p.reject(e);
			}
		});
		return p;
	}

	/**
	 * Wrap a {@link CompletableFuture} in a synchronous #{@link Promise}. The
	 * promise and future are connected in both directions: Completing or
	 * canceling the promise will also complete or cancel the future and vice
	 * versa.
	 */
	public static <V> Promise<V> wrap(CompletableFuture<V> future) {
		final Promise<V> p = new Promise<>();
		future.whenComplete(p::tryComplete);
		p.then(future::complete, future::completeExceptionally);
		return p;
	}

	/**
	 * Wrap a {@link CompletableFuture} in a parallel #{@link Promise}. The
	 * promise and future are connected in both directions: Completing or
	 * canceling the promise will also complete or cancel the future and vice
	 * versa.
	 */
	public static <V> Promise<V> wrap(CompletableFuture<V> future, ExecutorService pool) {
		final Promise<V> p = new Promise<>(pool);
		future.whenComplete((result, error) -> {
			while(error instanceof CompletionException && error.getCause() != null)
				error = error.getCause();
			p.tryComplete(result, error);
		});
		p.then(future::complete, future::completeExceptionally);
		return p;
	}

	/**
	 * Return this promise as a {@link CompletableFuture}. The promise and
	 * future are connected in both directions: Completing or canceling the
	 * promise will also complete or cancel the future and vice versa.
	 */
	public CompletableFuture<T> toCompletableFuture() {
		final CompletableFuture<T> cf = new CompletableFuture<>();
		cf.whenComplete((result, error) -> {
			while(error instanceof CompletionException && error.getCause() != null)
				error = error.getCause();
			tryComplete(result, error);
		});
		then(cf::complete, cf::completeExceptionally);

		return cf;
	}

	/**
	 * Create a resolved synchronous promise.
	 */
	public static <V> Promise<V> of(V value) {
		return new Promise<V>().resolve(value);
	}

	/**
	 * Return a parallel promise that will be resolved or rejected based on the
	 * result of a supplier. The supplier will be executed in the provided pool.
	 *
	 * @param supplier
	 * @param pool
	 * @return
	 */
	public static <V> Promise<V> ofComputed(FailableSupplier<V, ?> supplier, ExecutorService pool) {
		final Promise<V> p = new Promise<>(pool);
		pool.submit(() -> {
			try {
				p.resolve(supplier.supply());
			} catch (final Throwable err) {
				p.reject(err);
			}
		});
		return p;
	}

	/**
	 * Create a rejected parallel promise.
	 */
	public static <V> Promise<V> of(V value, ExecutorService pool) {
		return new Promise<V>(pool).resolve(value);
	}

	/**
	 * Create a rejected synchronous promise.
	 */
	public static <T> Promise<T> ofError(Exception e) {
		return new Promise<T>().reject(e);
	}

	/**
	 * Create a rejected parallel promise.
	 */
	public static <T> Promise<T> ofError(Exception e, ExecutorService pool) {
		return new Promise<T>(pool).reject(e);
	}

	/**
	 * Create an empty synchronous promise.
	 */
	public static <V> Promise<V> empty() {
		return new Promise<>();
	}

	/**
	 * Create an empty parallel promise.
	 */
	public static <V> Promise<V> empty(ExecutorService pool) {
		return new Promise<>(pool == null ? defaultPool : pool);
	}

	/**
	 * Returns true if this promise is associated with a thread pool and will
	 * execute listeners in parallel, or false if listeners are triggered
	 * sequentially and synchronously in the same thread they were triggered by.
	 *
	 * Listeners are triggered by the first call to {@link #reject(Throwable)}
	 * or {@link #resolve(Object)}. After that, new listeners will be triggered
	 * immediately by the call that registered them (e.g.
	 * {@link #then(ResultConsumer)} or {@link #map(ResultMapper)}.
	 */
	public boolean isParallel() {
		return pool != null;
	}

	/**
	 * Run a given handler, either synchronously or in thread pool. All
	 * exceptions are ignored.
	 */
	private void runHandler(PromiseHandler<T> handler) {
		try {
			if (pool == null)
				handler.handle(value, error);
			else
				pool.submit(() -> {
					try {
						handler.handle(value, error);
					} catch (final Exception e) {
						// swallow
					}
				});
		} catch (final Exception e) {
			// swallow
		}
	}

	/**
	 * Wait for this promise to complete and return the resolved value, or
	 * re-raise the exception that caused rejection. This method blocks until
	 * the promise is completed.
	 *
	 * @throws ExecutionException
	 *             if the promise was rejected with an error. The original
	 *             exception can be acquired via
	 *             {@link ExecutionException#getCause()}.
	 * @throws InterruptedException
	 *             if the current thread was interrupted while waiting for the
	 *             promise to complete.
	 * @throws CancellationException
	 *             if the promise was canceled.
	 *
	 * @return The resolved value.
	 */
	public T get() throws ExecutionException, InterruptedException, CancellationException {
		try {
			return get(-1, null);
		} catch (final TimeoutException e) {
			throw Utils.wtf("A timeout when waiting indifinately should not happen.", e);
		}
	}

	/**
	 * Wait for this promise to complete and return the resolved value, or raise
	 * an exception. This method blocks until the promise is completed or a
	 * certain time has passed. If the promise does not complete within the
	 * given time, a {@link TimeoutException} is thrown.
	 *
	 * A timeout does not reject the promise.
	 *
	 * @throws ExecutionException
	 *             if the promise was rejected with an error. The original
	 *             exception can be acquired via
	 *             {@link ExecutionException#getCause()}. The original exception
	 *             is wrapped to preserve both stack-traces: The trace of the
	 *             {@link #reject(Throwable)} call, and the trace of the
	 *             {@link #get()} call.
	 * @throws InterruptedException
	 *             if the current thread was interrupted while waiting for the
	 *             promise to complete.
	 * @throws TimeoutException
	 *             if the promise did not complete within the given timeout.
	 * @throws CancellationException
	 *             if the promise was canceled.
	 *
	 */
	public T get(long timeout, TimeUnit unit)
			throws ExecutionException, InterruptedException, TimeoutException, CancellationException {
		if (!completed)
			await(timeout, unit);

		synchronized (this) {
			if (!completed) {
				throw new TimeoutException("Promise did not complete in time.");
			}
			if (error != null) {
				if (error instanceof CancellationException)
					throw (CancellationException) error;
				throw new ExecutionException(error);
			}
			return value;
		}
	}

	/**
	 * Return the resolved value (if available), or null.
	 *
	 * Note that {@link #resolve(Object)} accepts <code>null</code> as a value,
	 * so this method may return <code>null</code> even if {@link #isResolved()}
	 * returns <code>true</code>;
	 */
	public T value() {
		return value;
	}

	/**
	 * Return the error that caused rejection of this promise (if applicable),
	 * or null.
	 */
	public Throwable error() {
		return error;
	}

	/**
	 * Wait for this promise to complete. This method may block indefinitely if
	 * the promise is never resolved or rejected.
	 *
	 * @throws InterruptedException
	 *             if any thread interrupted the current thread while the
	 *             current thread was waiting for this promise to complete.
	 *
	 * @return this promise.
	 */

	public Promise<T> await() throws InterruptedException {
		return await(-1, null);
	}

	/**
	 * Wait for this promise to complete, or a certain time has passed,
	 * whichever happens first.
	 *
	 * @param timeout
	 *            Maximum time to wait for completion. May be negative to
	 *            indicate an infinite timeout.
	 * @param unit
	 *            Time unit of the timeout parameter. May be null if the timeout
	 *            parameter is negative or 0.
	 *
	 * @throws InterruptedException
	 *             if any thread interrupted the current thread while the
	 *             current thread was waiting for this promise to complete.
	 *
	 * @return this promise.
	 */
	public Promise<T> await(long timeout, TimeUnit unit) throws InterruptedException {
		if (completed || timeout == 0)
			return this;

		final CountDownLatch latch = new CountDownLatch(1);
		then((val, err) -> latch.countDown());

		if (timeout < 0)
			latch.await();
		else
			latch.await(timeout, unit);

		return this;
	}

	/**
	 * Return true if this promise is resolved and {@link #get()} will return a
	 * value.
	 */
	public boolean isResolved() {
		return completed && error == null;
	}

	/**
	 * Return true if this promise is rejected and {@link #error()} will return
	 * a value.
	 */
	public boolean isRejected() {
		return error != null;
	}

	/**
	 * Retrun true if this promise is neither resolved nor rejected.
	 */
	public boolean isOpen() {
		return !completed;
	}

	/**
	 * Retrun true if this promise is either resolved nor rejected.
	 */
	public boolean isCompleted() {
		return completed;
	}

	/**
	 * Return true if this promise was rejected with a
	 * {@link CancellationException}.
	 */
	public boolean isCanceled() {
		return error != null && error instanceof CancellationException;
	}

	/**
	 * Reject this promise with an error. Registered handlers will be called
	 * synchronously or submitted to a thread pool depending on the value of
	 * {@link #isParallel()}.
	 *
	 * @throws IllegalStateException
	 *             if this promise is already resolved or rejected.
	 *
	 * @return this promise.
	 */
	public Promise<T> reject(Throwable err) {
		if (!tryReject(err))
			throw new IllegalStateException("Cannot reject promise which is already completed.");
		return this;
	}

	/**
	 * Try to reject this promise with a {@link CancellationException} if it is
	 * still open and return true on success, false otherwise.
	 *
	 * The owner of the promise may or may not stop doing work for this promise
	 * once it is cancelled.
	 *
	 * This equals <code>tryReject(new CancellationException())</code>
	 */
	public boolean cancel() {
		return tryReject(new CancellationException());
	}

	/**
	 * Resolve this promise. Registered handlers will be called synchronously or
	 * submitted to a thread pool depending on the value of
	 * {@link #isParallel()}.
	 *
	 * @throws IllegalStateException
	 *             if this promise is already resolved or rejected.
	 *
	 * @return this promise.
	 */
	public Promise<T> resolve(T val) {
		if (!tryResolve(val))
			throw new IllegalStateException("Cannot resolve promise which is already completed.");
		return this;
	}

	/**
	 * Try to reject this promise if it is still open and return true on
	 * success, false otherwise.
	 */
	public synchronized boolean tryReject(Throwable err) {
		if (err == null)
			throw new NullPointerException("Promises cannot be rejected with a null value.");
		if (completed)
			return false;
		error = err;
		completed = true;

		handlers.forEach(this::runHandler);
		handlers.clear();
		return true;
	}

	/**
	 * Try to resolve this promise if it is still open and return true on
	 * success, false otherwise.
	 */
	public synchronized boolean tryResolve(T val) {
		if (completed)
			return false;
		value = val;
		completed = true;

		handlers.forEach(this::runHandler);
		handlers.clear();
		return true;
	}

	private void tryComplete(T val, Throwable err) {
		if (err != null)
			tryReject(err);
		else
			tryResolve(val);
	}

	/**
	 * Register a consumer to be called on success. Exceptions thrown by the
	 * consumer are silently ignored.
	 *
	 * @return this promise
	 */
	public Promise<T> then(ResultConsumer<T> onSuccess) {
		return then(onSuccess, null);
	}

	/**
	 * Register a consumer and an error handler. Exceptions thrown by any of the
	 * consumers are silently ignored.
	 *
	 * TODO: rename to listen?
	 *
	 * @return this promise
	 */
	public Promise<T> then(ResultConsumer<T> onSuccess, ErrorConsumer onError) {
		return then((v, t) -> {
			if (t == null) {
				if (onSuccess != null) {
					onSuccess.handle(v);
				}
			} else if (onError != null) {
				onError.handle(t);
			}
		});
	}

	/**
	 * Register a {@link PromiseHandler} to be called as soon as this promise is
	 * resolved or rejected. Exceptions thrown by the handler are silently
	 * ignored.
	 *
	 * @return this promise
	 */

	public synchronized Promise<T> then(PromiseHandler<T> handler) {
		if (completed) {
			runHandler(handler);
		} else {
			handlers.add(handler);
		}
		return this;
	}

	/**
	 * Return a new parallel promise that will execute handlers in the given
	 * thread pool. See {@link #isParallel()} for details.
	 *
	 * The returned promise will be resolved or rejected with the same result as
	 * this promise.
	 *
	 * @param pool
	 *            Pool to use, or null for the default pool.
	 *
	 * @return a parallel promise.
	 */
	public Promise<T> parallel(ExecutorService pool) {
		final Promise<T> p = new Promise<>(pool != null ? pool : defaultPool);
		then(p::tryResolve, p::tryReject);
		return p;
	}

	/**
	 * Return a new parallel promise that will execute handlers in a thread
	 * pool. See {@link #isParallel()} for details.
	 *
	 * If this promise is already parallel, then the new promise uses the same
	 * thread pool. Otherwise, a new parallel promise is created using a default
	 * pool, which defaults to {@link ForkJoinPool#commonPool()}).
	 *
	 * The returned promise will be resolved or rejected with the same result as
	 * this promise.
	 *
	 * @return a parallel promise.
	 */
	public Promise<T> parallel() {
		return parallel(pool != null ? pool : defaultPool);
	}

	/**
	 * Return a new promise that is resolved with a fallback value provided by
	 * an error handler if the original promise is rejected. If the error
	 * handler returns null, the new promise will be rejected with the original
	 * exception. If the error handler throws a new exception, then the new
	 * exception is used to reject the new promise. Canceling the new promise
	 * will try to cancel the original promise.
	 *
	 * This equals <code>map(v-&gt;v, mapFunction);</code>.
	 *
	 * The returned promise will be parallel if this promise is parallel and use
	 * the same thread pool.
	 *
	 * @return a new promise
	 */

	public Promise<T> expect(ErrorMapper<T> mapError) {
		return map(val -> val, mapError);
	}

	/**
	 * Return a new promise that is resolved with the mapped value of this
	 * promise, or rejected with the same exception. Exceptions thrown from the
	 * mapping function will reject the new promise. Canceling the new promise
	 * will try to cancel the original promise.
	 *
	 * The returned promise will be parallel if this promise is parallel and use
	 * the same thread pool.
	 */
	public <R> Promise<R> map(ResultMapper<T, R> mapSuccess) {
		return map(mapSuccess, null);
	}

	/**
	 * Same as {@link #map(ResultMapper)} for mappers that do not return a value,
	 * resulting in a {@link Void} promise.
	 */
	public Promise<Void> mapVoid(ResultConsumer<T> mapSuccess) {
		return map(mapperFromConsumer(mapSuccess), null);
	}

	/**
	 * Same as {@link #map(ResultMapper, ErrorMapper)} for mappers that do not
	 * return value, resulting in a {@link Void} promise.
	 */
	public Promise<Void> mapVoid(ResultConsumer<T> mapSuccess, ErrorConsumer mapError) {
		return map(mapperFromConsumer(mapSuccess), mapperFormConsumer(mapError));
	}

	/**
	 * Return a new promise that is resolved with the mapped value of this
	 * promise, or with a fallback value provided by an error handler.
	 * Exceptions thrown from the mapping function or the error handler will
	 * reject the new promise. Canceling the new promise will try to cancel the
	 * original promise.
	 *
	 * The returned promise will be parallel if this promise is parallel and use
	 * the same thread pool.
	 *
	 * @return a new promise
	 */

	public <R> Promise<R> map(ResultMapper<T, R> mapSuccess, ErrorMapper<R> mapError) {
		final Promise<R> p = pool == null ? new Promise<>() : new Promise<>(pool);
		then((value, err) -> {
			try {
				if (err == null) {
					if (mapSuccess != null) {
						p.resolve(mapSuccess.map(value));
					}
				} else if (mapError != null) {
					p.resolve(mapError.map(err));
				} else {
					p.reject(err);
				}
			} catch (final Throwable e) {
				p.reject(e);
			}
		});

		p.then(null, err -> {
			if (err instanceof CancellationException)
				cancel();
		});

		return p;
	}

	/**
	 * Similar to {@link #map(ResultMapper)}, but for mapping functions that
	 * return a promise instead of a value. The new promise returned by this
	 * method is completed once the promise returned by the mapping function is
	 * completed. Canceling the new promise will try to cancel the original
	 * promise.
	 *
	 * The returned promise will be parallel if this promise is parallel and use
	 * the same thread pool.
	 */
	public <R> Promise<R> flatMap(ResultMapper<T, Promise<R>> mapSuccess) {
		return flatMap(mapSuccess, null);
	}

	/**
	 * Similar to {@link #map(ResultMapper, ErrorMapper)}, but for mapping
	 * functions and error handlers that return promises instead of concrete
	 * values. The new promise returned by this method is completed once the
	 * promise returned by the mapping function or error handlers is completed.
	 * Canceling the new promise will try to cancel the original promise.
	 *
	 * The returned promise will be parallel if this promise is parallel and use
	 * the same thread pool.
	 */
	public <R> Promise<R> flatMap(ResultMapper<T, Promise<R>> mapSuccess, ErrorMapper<Promise<R>> mapError) {
		final Promise<R> p = pool == null ? new Promise<>() : new Promise<>(pool);
		map(mapSuccess, mapError).then((promisedValue, err) -> {
			if (err != null)
				p.tryReject(err);
			else
				promisedValue.then(p::tryResolve, p::tryReject);
		});

		p.then(null, err -> {
			if (err instanceof CancellationException)
				cancel();
		});

		return p;
	}

	/**
	 * Return a new promise that is rejected or resolve as soon as the current
	 * promise is rejected or resolved, but does not cancel the current promise
	 * if it is canceled.
	 *
	 * This can be used in front of a map, flatMap or expect operation to
	 * decouple a child promise form its parent and ensure that the parent
	 * survives the cancellation of a child promise.
	 *
	 * The returned promise will be parallel if this promise is parallel and use
	 * the same thread pool
	 */
	public Promise<T> split() {
		final Promise<T> p = pool == null ? new Promise<>() : new Promise<>(pool);
		then(p::tryResolve, p::tryReject);
		return p;
	}

	/**
	 * Return a new promise that combines the results of two promises.
	 *
	 * The returned promise is resolved with the combined values of both
	 * promises as soon as the results are available. If any of the promises is
	 * rejected, then the returned promise is also rejected. Canceling the new
	 * promise will try to cancel the original promises.
	 *
	 * The returned promise will be parallel if this promise is parallel and use
	 * the same thread pool.
	 *
	 * @param other
	 *            the promise to join with.
	 * @param mapSuccess
	 *            mapping function to combine both results.
	 * @return a new promise.
	 */
	public <U, R> Promise<R> merge(Promise<U> other, ResultMerger<T, U, R> mergeFunction) {
		final Promise<R> p = pool == null ? new Promise<>() : new Promise<>(pool);

		// First wait for THIS, then wait for OTHER to complete.
		then(val -> other.then(val2 -> {
			try {
				p.tryResolve(mergeFunction.merge(val, val2));
			} catch (final Exception e) {
				p.tryReject(e);
			}
		}, p::tryReject), p::tryReject);

		// Reject immediately if OTHER fails before THIS completes.
		other.then(null, p::tryReject);

		p.then(null, err -> {
			if (err instanceof CancellationException) {
				cancel();
				other.cancel();
			}
		});

		return p;
	}

	/**
	 * Return a new promise that combines the results of two promises.
	 *
	 * Like {@link #merge(Promise, ResultMerger)}, but the merge function may return
	 * a promise.
	 *
	 * @param other      the promise to join with.
	 * @param mapSuccess mapping function to combine both results.
	 * @return a new promise.
	 */
	public <U, R> Promise<R> flatMerge(Promise<U> other, ResultMerger<T, U, Promise<R>> mergeFunction) {
		return merge(other, mergeFunction).flatMap(p -> p);
	}

	/**
	 * Return a new promise that is resolved by one of two promises, whichever
	 * resolves first. If both promises are rejected, the new promise is also
	 * rejected. Canceling the new promise will try to cancel the original
	 * promises.
	 *
	 * The returned promise will be parallel if this promise is parallel and use
	 * the same thread pool.
	 */

	public Promise<T> or(Promise<T> other) {
		final Promise<T> p = pool == null ? new Promise<>() : new Promise<>(pool);

		then(p::tryResolve, err -> other.then(null, p::tryReject));
		other.then(p::tryResolve, err -> then(null, p::tryReject));

		p.then(null, err -> {
			if (err instanceof CancellationException) {
				cancel();
				other.cancel();
			}
		});

		return p;
	}

	private static <P> ErrorMapper<Void> mapperFormConsumer(ErrorConsumer consumer) {
		return new ErrorMapper<Void>() {
			@Override
			public Void map(Throwable value) throws Throwable {
				consumer.handle(value);
				return null;
			}
		};
	}

	private static <P> ResultMapper<P, Void> mapperFromConsumer(ResultConsumer<P> consumer) {
		return new ResultMapper<P, Void>() {
			@Override
			public Void map(P value) throws Throwable {
				consumer.handle(value);
				return null;
			}
		};
	}

	/**
	 * Functional interface similar to {@link Function}, but may throw checked
	 * exceptions.
	 */
	@FunctionalInterface
	public interface ResultMapper<ParameterType, ReturnType> {
		ReturnType map(ParameterType value) throws Throwable;
	}

	/**
	 * Functional interface similar to {@link Consumer}, but may throw checked
	 * exceptions.
	 */
	@FunctionalInterface
	public interface ResultConsumer<ParameterType> {
		void handle(ParameterType value) throws Exception;
	}

	/**
	 * A {@link ResultMapper} for {@link Throwable}.
	 */
	@FunctionalInterface
	public interface ErrorMapper<ReturnType> extends ResultMapper<Throwable, ReturnType> {
	}

	/**
	 * A {@link ResultConsumer} for {@link Throwable}.
	 *
	 */
	@FunctionalInterface
	public interface ErrorConsumer extends ResultConsumer<Throwable> {
	}

	/**
	 * Functional interface similar to {@link BiFunction}, but may throw checked
	 * exceptions.
	 */
	@FunctionalInterface
	public interface ResultMerger<ParameterType, ParameterType2, ReturnType> {
		ReturnType merge(ParameterType value, ParameterType2 value2) throws Exception;
	}

	/**
	 * A combined handler for resolve and reject state.
	 *
	 * Remember that the value may be null on success, but the error is never
	 * null on failure. The usual implementation looks like this:
	 *
	 * <pre>
	 *  (val, err) -&gt; {
	 *  	if(err==null)
	 *  		// success
	 *  	else
	 *  		// error
	 *  }
	 * </pre>
	 */
	@FunctionalInterface
	public interface PromiseHandler<ValueType> {
		void handle(ValueType value, Throwable error) throws Exception;
	}

}
