package de.gwdg.cdstar;

/**
 * Like {@link Runnable}, but allowed to throw checked exceptions.
 */
@FunctionalInterface
public interface Failable {
	void run() throws Exception;
}