package de.gwdg.cdstar;

import java.util.Arrays;
import java.util.Collection;
import java.util.EnumMap;
import java.util.EnumSet;

/**
 * A boxed enum that allows atomic compare-and-set operations as well as
 * defining a predefined set of allowed transitions. Can be used to implement
 * state maschines.
 */
public class AtomicEnum<E extends Enum<E>> {

	private volatile E state;
	private final EnumMap<E, EnumSet<E>> transitions;

	private AtomicEnum(E startState, EnumMap<E, EnumSet<E>> transitions) {
		state = startState;
		this.transitions = transitions;
	}

	/**
	 * Check if a transition is allowed.
	 */
	public boolean isAllowed(E from, E to) {
		return transitions != null && transitions.get(from).contains(to);
	}

	/**
	 * Move to newState if the current state is the expected state. Return true on
	 * success, false otherwise.
	 *
	 * @throws IllegalStateException if the transition is not allowed.
	 * @throws NullPointerException  if any of the arguments are null.
	 */
	public synchronized boolean compareAndSet(E expect, E newState) {
		if (expect == null || newState == null)
			throw new NullPointerException();
		if (state != expect)
			return false;
		set(newState);
		return true;
	}

	/**
	 * Move to new state if the current state is one of the expected states. Return
	 * the old state, or null if the move did not happen.
	 *
	 * @throws IllegalStateException if the transition is not allowed.
	 * @throws NullPointerException  if any of the arguments are null.
	 */
	public synchronized E compareAndSet(EnumSet<E> expect, E newState) {
		if (expect == null || newState == null)
			throw new NullPointerException();
		if (!expect.contains(state))
			return null;
		return set(newState);
	}

	/**
	 * Move to a new state regardless of current state.
	 *
	 * @throws IllegalStateException if the transition is not allowed.
	 * @throws NullPointerException  if any of the arguments are null.
	 * @return the old state.
	 *
	 */
	public synchronized E set(E newState) {
		if (newState == null)
			throw new NullPointerException("Cannot move to null state.");
		checkAllowed(state, newState);
		final E oldState = state;
		state = newState;
		return oldState;
	}

	private void checkAllowed(E from, E to) {
		if (!isAllowed(from, to))
			throw new IllegalStateException("Cannot move from State " + state + " to " + to + ".");
	}

	/**
	 * Throw an {@link IllegalStateException} if the current state is not as
	 * expected.
	 *
	 * @throws IllegalStateException if the current state does not match the
	 *                               expected state.
	 */
	public void expect(E expect) {
		if (!is(expect))
			throw new IllegalStateException("Expected state to be " + expect + " but was " + state + ".");
	}

	/**
	 * Throw an {@link IllegalStateException} if the current state is not as
	 * expected.
	 *
	 * @throws IllegalStateException if the current state does not match the
	 *                               expected state.
	 */
	@SuppressWarnings("unchecked")
	public void expect(E expect, E... alternatives) {
		if (!is(expect, alternatives))
			throw new IllegalStateException(
				"Expected state to be " + expect + "/" + Utils.join("/", Arrays.asList((Object[]) alternatives)) +
				" but was " + state + ".");
	}

	/**
	 * Return true if the current state is the expected one, false otherwise.
	 */
	public boolean is(E expect) {
		return state == expect;
	}

	/**
	 * Return true if the current state is one of the expected ones, false
	 * otherwise.
	 */
	@SuppressWarnings("unchecked")
	public synchronized boolean is(E expect, E... alternatives) {
		if (state == expect)
			return true;
		for (final E alt : alternatives)
			if (state == alt)
				return true;
		return false;
	}

	public E get() {
		return state;
	}

	/**
	 * Create a new atomic enum with no transition checks and a given start state.
	 */
	public static <E extends Enum<E>> AtomicEnum<E> of(E startState) {
		return new AtomicEnum<>(startState, null);
	}

	/**
	 * Start building an atomic enum with transition checks and a given start state.
	 */
	public static <E extends Enum<E>> AtomicEnumBuilder<E> build(E startState) {
		return new AtomicEnumBuilder<>(startState);
	}

	@Override
	public String toString() {
		return state.toString();
	}

	@Override
	public boolean equals(Object obj) {
		return obj == state || (obj instanceof AtomicEnum && ((AtomicEnum<?>) obj).state == state);
	}

	@Override
	public int hashCode() {
		return state.hashCode();
	}

	public static class AtomicEnumBuilder<E extends Enum<E>> {
		private final EnumMap<E, EnumSet<E>> transitions;
		private final AtomicEnum<E> ae;

		private AtomicEnumBuilder(E startState) {
			transitions = new EnumMap<>(startState.getDeclaringClass());
			for (final E e : startState.getDeclaringClass().getEnumConstants())
				transitions.put(e, EnumSet.noneOf(e.getDeclaringClass()));
			ae = new AtomicEnum<>(startState, transitions);
		}

		@SuppressWarnings("unchecked")
		public AtomicEnumBuilder<E> allow(E fromState, E... toStates) {
			return allow(fromState, Arrays.asList(toStates));
		}

		@SuppressWarnings("unchecked")
		public AtomicEnumBuilder<E> allowChain(E... chainStates) {
			for (int i = 0; i < chainStates.length - 1; i++)
				allow(chainStates[i], chainStates[i + 1]);
			return this;
		}

		public AtomicEnumBuilder<E> allow(E fromState, Collection<E> toStates) {
			transitions.get(fromState).addAll(toStates);
			return this;
		}

		@SuppressWarnings("unchecked")
		public AtomicEnumBuilder<E> deny(E fromState, E... toStates) {
			return deny(fromState, Arrays.asList(toStates));
		}

		public AtomicEnumBuilder<E> deny(E fromState, Collection<E> toStates) {
			transitions.get(fromState).removeAll(toStates);
			return this;
		}

		public AtomicEnum<E> build() {
			return ae;
		}
	}

}
