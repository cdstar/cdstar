package de.gwdg.cdstar;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * This class can be used with a try-with-resource block to manage multiple
 * resources that need to be closed while keeping the nesting flat and tidy.
 *
 * Registered resources are closed in reverse order (to simulate nested
 * try-with-resource blocks) and exceptions thrown are collected until all
 * resource had a chance to close.
 *
 * The first exception thrown is re-thrown at the end of the closing phase. Any
 * additional exceptions are attached as suppressed exceptions to the first one.
 *
 * Since {@link AutoCloseable} is a functional interface similar to an exception
 * throwing Runnable, it can be used with lambdas.
 */
public class MultiCloseable implements AutoCloseable {
	final List<AutoCloseable> toClose;

	private MultiCloseable() {
		toClose = new ArrayList<>(4);
	}

	private MultiCloseable(AutoCloseable... resources) {
		toClose = new ArrayList<>(resources.length);
		for (final AutoCloseable resource : resources)
			toClose.add(resource);
	}

	/**
	 * Wrapper for resources that do not directly implement {@link AutoCloseable}
	 */
	private static class Wrapped implements AutoCloseable {
		private final Object resource;
		private final AutoCloseable autoCloseable;

		private Wrapped(Object res, AutoCloseable closeAction) {
			resource = res;
			autoCloseable = closeAction;
		}

		private <T> Wrapped(T res, FailableConsumer<T> closeAction) {
			this(res, () -> closeAction.accept(res));
		}

		@Override
		public boolean equals(Object obj) {
			return resource.equals(obj) || autoCloseable.equals(obj);
		}

		@Override
		public void close() throws Exception {
			autoCloseable.close();
		}
	}

	public static MultiCloseable empty() {
		return new MultiCloseable();
	}

	public static MultiCloseable of(AutoCloseable... toClose) {
		return new MultiCloseable(toClose);
	}

	/**
	 * Register a resource with an explicit close action.
	 *
	 * @return the resource
	 */
	public <T> T add(T resource, FailableConsumer<T> closeAction) {
		return add(resource, () -> closeAction.accept(resource));
	}

	/**
	 * Register a resource with an explicit close action.
	 *
	 * @return the resource
	 */
	public <T> T add(T resource, AutoCloseable closeAction) {
		add(new Wrapped(resource, closeAction));
		return resource;
	}

	/**
	 * Register an {@link AutoCloseable} resource.
	 *
	 * @return the resource
	 */
	public synchronized <T extends AutoCloseable> T add(T resource) {
		toClose.add(resource);
		return resource;
	}

	/**
	 * Close a resource that was previously registered with this
	 * {@link MultiCloseable} and remove it from the list of managed resources.
	 * Throw any {@link Exception} the close method may throw.
	 *
	 * @return True if it was found, false otherwise.
	 */
	public synchronized boolean close(Object resource) throws Exception {
		for (final Iterator<AutoCloseable> iter = toClose.iterator(); iter.hasNext();) {
			final AutoCloseable o = iter.next();
			if (o.equals(resource)) {
				o.close();
				iter.remove();
				return true;
			}
		}
		return false;
	}

	/**
	 * Like {@link #close(Object)}, but swallows any errors.
	 *
	 * @return True if the resource was found and closed successfully, false
	 *         otherwise.
	 */
	public synchronized boolean closeQuietly(Object resource) {
		for (final Iterator<AutoCloseable> iter = toClose.iterator(); iter.hasNext();) {
			final AutoCloseable o = iter.next();
			if (o.equals(resource)) {
				try {
					o.close();
					return true;
				} catch (final Exception e) {
					return false;
				} finally {
					iter.remove();
				}
			}
		}
		return false;
	}

	/**
	 * Find and remove a resource that was previously registered with this
	 * {@link MultiCloseable}. The resource is not closed.
	 *
	 * @return True if a matching resource was found and removed, false otherwise.
	 */
	public synchronized boolean forget(Object resource) {
		return toClose.remove(resource);
	}

	/**
	 * Close and remove all managed resources that were not closed explicitly (see
	 * {@link #close(Object)}) in reverse order. Exceptions are collected and
	 * returned as a list after all resources had a chance do close.
	 */
	public List<Exception> closeAll() {
		List<Exception> errors = null;
		for (int i = toClose.size() - 1; i >= 0; i--) {
			try {
				toClose.get(i).close();
			} catch (final Exception e) {
				if (errors == null)
					errors = new ArrayList<>(1);
				errors.add(e);
			}
		}
		toClose.clear();
		return errors != null ? errors : Collections.emptyList();
	}

	/**
	 * Close and removes all managed resources that were not closed explicitly (see
	 * {@link #close(Object)}) in reverse order. If any of them throws, the
	 * exception is re-thrown after all other resources had a chance to close.
	 * Additional exceptions are attached as suppressed to the first error.
	 */
	@Override
	public synchronized void close() throws Exception {
		final List<Exception> errors = closeAll();
		if (errors.isEmpty())
			return;

		final Exception error = errors.get(0);
		for (int i = 1; i < errors.size(); i++)
			error.addSuppressed(errors.get(i));
		throw error;
	}

}
