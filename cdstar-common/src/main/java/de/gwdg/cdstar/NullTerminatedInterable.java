package de.gwdg.cdstar;

import java.util.Iterator;
import java.util.function.Supplier;

/**
 * An iterator that repeatedly calls a producer until it returns null.
 */
public class NullTerminatedInterable<T> implements Iterable<T>, Iterator<T> {

	T next;
	private Supplier<T> producer;

	public NullTerminatedInterable(Supplier<T> producer) {
		this.producer = producer;
	}

	@Override
	public Iterator<T> iterator() {
		return this;
	}

	@Override
	public synchronized boolean hasNext() {
		if (next == null && producer != null && (next = producer.get()) == null) {
			producer = null;
		}
		return next != null;
	}

	@Override
	public synchronized T next() {
		hasNext();
		final T result = next;
		next = null;
		return result;
	}

}
