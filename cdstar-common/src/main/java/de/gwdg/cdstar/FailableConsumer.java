package de.gwdg.cdstar;

import java.util.function.Consumer;

/**
 * Like {@link Consumer}, but allowed to throw checked exceptions.
 */
@FunctionalInterface
public interface FailableConsumer<T> {
	void accept(T obj) throws Exception;

	default void acceptOrHandle(T obj, Consumer<Exception> exceptionHandler) {
		try {
			accept(obj);
		} catch (final Exception e) {
			if (exceptionHandler != null)
				exceptionHandler.accept(e);
		}
	}
}