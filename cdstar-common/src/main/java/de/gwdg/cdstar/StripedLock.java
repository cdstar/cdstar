package de.gwdg.cdstar;

import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Supplier;

/**
 * This is a simple solution for
 *
 */
public class StripedLock {

	private final int lockCount;
	private final ReentrantLock[] locks;

	/**
	 *
	 * @param stripes
	 *            Number of stripes. This number should be larger than the
	 *            desired concurrency (e.g. the tread pool size), otherwise
	 *            collisions may result in a concurrency lower than expected.
	 */
	public StripedLock(int stripes) {
		lockCount = stripes;
		locks = new ReentrantLock[lockCount];
		for (int i = 0; i < lockCount; i++)
			locks[i] = new ReentrantLock();
	}

	public ReentrantLock getLockFor(String id) {
		return getLockFor(id.hashCode());
	}

	ReentrantLock getLockFor(int hash) {
		// Hint: The java '%' operator calculates the remainder (which may be
		// negative), not modulo. The expression '(a%b+b)%b' is a fast way to
		// get the actual modulo (0 <= (a%b+b)%b < b). This is not the same as
		// Math.abs(a)%b through.
		return locks[(hash % lockCount + lockCount) % lockCount];
	}

	public void runWithLock(String id, Runnable runnable) {
		final ReentrantLock lock = getLockFor(id);
		lock.lock();
		try {
			runnable.run();
		} finally {
			lock.unlock();
		}
	}

	public <T> T runWithLock(String id, Supplier<T> cb) {
		final ReentrantLock lock = getLockFor(id);
		lock.lock();
		try {
			return cb.get();
		} finally {
			lock.unlock();
		}
	}

}
