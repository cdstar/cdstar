package de.gwdg.cdstar;

import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;

public abstract class WeakValueCache<KeyType, ValueType> {

	private static final int COMPRESS_INTERVAL = 1024;
	final ReferenceQueue<ValueType> referenceQueue = new ReferenceQueue<>();
	final Map<KeyType, WeakReference<ValueType>> values = new HashMap<>();
	private volatile int opsCounter = 0;

	private class KeyBoundReference extends WeakReference<ValueType> {
		private final KeyType key;

		public KeyBoundReference(KeyType key, ValueType referent) {
			super(referent, referenceQueue);
			this.key = key;
		}
	}

	@SuppressWarnings("unchecked")
	private void compress() {
		opsCounter = 0;
		KeyBoundReference ref;
		while ((ref = (KeyBoundReference) referenceQueue.poll()) != null) {
			values.remove(ref.key);
		}
	}

	public synchronized ValueType get(KeyType key) {
		if (opsCounter++ > COMPRESS_INTERVAL) {
			compress();
		}
		WeakReference<ValueType> objref;
		ValueType obj;
		if ((objref = values.get(key)) != null && (obj = objref.get()) != null) {
			return obj;
		}
		obj = produce(key);
		values.put(key, new KeyBoundReference(key, obj));
		return obj;
	}

	protected abstract ValueType produce(KeyType key);

}
