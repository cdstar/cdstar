package de.gwdg.cdstar;

import java.io.IOException;
import java.io.Writer;
import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

public class PidFileLock {

	private final Path pidfile;
	private boolean locked = false;

	public PidFileLock(Path pidPile) {
		pidfile = pidPile;
	}

	public boolean isLocked() {
		return locked;
	}

	public Path getPath() {
		return pidfile;
	}

	public long getPid() {
		final RuntimeMXBean mb = ManagementFactory.getRuntimeMXBean();
		try {
			return (Long) mb.getClass().getMethod("getPid").invoke(mb);
		} catch (final NoSuchMethodException | IllegalAccessException | IllegalArgumentException
			| InvocationTargetException e) {
			// Java 8 fallback
		}

		try {
			return Long.parseLong(mb.getName().split("@")[0]);
		} catch (final Exception e) {
			// Non-Hotspot JVM?
		}

		try {
			return Long.parseLong(new String(Files.readAllBytes(Paths.get("/proc/self/stats"))).split(" ")[0]);
		} catch (final Exception e) {
			// Not unix?
		}

		throw new UnsupportedOperationException("Unable to determine current process pid");
	}

	/**
	 * Create the pidfile and write the PID to it. If the file already exists or
	 * cannot be created, an {@link IOException} is thrown.
	 */
	public synchronized void create() throws IOException {
		try (Writer wr = Files.newBufferedWriter(pidfile, StandardOpenOption.CREATE_NEW)) {
			wr.write(Long.toString(getPid()));
			wr.close();
			locked = true;
		}
	}

	/**
	 * Remove the pidfile previously created via {@link #create()}.
	 */
	public synchronized void remove() throws IOException {
		if (!locked)
			throw new IllegalStateException("Not locked");
		Files.delete(pidfile);
	}

}
