package de.gwdg.cdstar;

import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.function.Function;

public class UriBuilder {

	private String scheme;
	private String auth;
	private String host;
	private int port;
	private StringBuilder path = new StringBuilder();
	private StringBuilder query = new StringBuilder();
	private String fragment;
	
	public UriBuilder() {
	}
	
	public UriBuilder(URI base) {
		scheme = base.getScheme();
		auth = base.getRawUserInfo();
		host = base.getHost();
		port = base.getPort();
		if(base.getRawPath() != null)
			path.append(base.getRawPath());
		if(base.getRawQuery() != null)
			query.append(base.getRawQuery());
		fragment = base.getRawFragment();
	}

	public UriBuilder scheme(String scheme) {
		this.scheme = scheme;
		return this;
	}

	public UriBuilder auth(String auth) {
		this.auth = auth;
		return this;
	}

	public UriBuilder host(String host) {
		this.host = host;
		return this;
	}

	public UriBuilder port(int port) {
		this.port = port;
		return this;
	}

	public UriBuilder path(String ... parts) {
		if(parts.length == 0)
			path.setLength(0);
		if(path.length() > 0 && path.charAt(path.length()-1) == '/')
			path.setLength(path.length()-1);
		for(var part: parts)
			path.append("/").append(URLEncoder.encode(part, StandardCharsets.UTF_8).replace("+", "%20"));
		return this;
	}

	public UriBuilder query() {
		this.query.setLength(0);
		return this;
	}

	public UriBuilder query(String key, String value) {
		if(query.length() > 0)
			query.append("&");
		query.append(URLEncoder.encode(key, StandardCharsets.UTF_8));
		if(value != null)
			query.append("=").append(URLEncoder.encode(value, StandardCharsets.UTF_8));
		return this;
	}
		
	public UriBuilder fragment(String fragment) {
		this.fragment = fragment;
		return this;
	}

	public URI build() {
		try {
			return new URI(scheme, auth, host, port, path.toString(), query.toString(), fragment);
		} catch (URISyntaxException e) {
			throw new IllegalStateException("Cannot build URI", e);
		}
	}
	
	public <T> T map(Function<URI, T> mapping) {
		return mapping.apply(build());
	}

}
