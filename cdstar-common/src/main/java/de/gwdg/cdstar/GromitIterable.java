package de.gwdg.cdstar;

import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.function.Consumer;
import java.util.function.Predicate;

/**
 * A fully thread-save linked list that allows modification while iterating over
 * it. Iterators will never throw {@link ConcurrentModificationException}, but
 * instead reflect any changes made to the list by any thread.
 *
 * Adding items, removing the first item or calling {@link Iterator#remove()} is
 * O(1). All other operations are O(N).
 *
 * This may be useful for implementations of the Observer pattern, when
 * triggering an observer may add or remove other observers to the list. This
 * implementation should be cheaper than the iterate-over-copy approach often
 * used and also does not skip new observers the first time they are added.
 *
 * See https://i.imgur.com/vDdQTst.mp4 or google for 'Gromit train chase' for a
 * helpful visualization.
 */
public final class GromitIterable<T> implements Iterable<T> {

	/*
	 * Implementation notes on thread safety:
	 *
	 * Nodes are added and removed to this linked list via synchronized
	 * functions, so thread-safety is guaranteed for these operations. Iterators
	 * however are NOT synchronized on the entire linked list, but only on
	 * themselves, so special care has to be taken to ensure iterators and
	 * internal for-loops are thread-save:
	 *
	 * 1) The next pointer is volatile, so new nodes are picked up immediately
	 * and removed nodes can be skipped (most of the time).
	 *
	 * 2) The next pointer of an intermediate node is not nulled after removal.
	 * This ensures that an iterator can move forward even if it points to a
	 * node that was removed.
	 *
	 * 3) The value of removed nodes is nulled, so iterators can detect removed
	 * nodes even if they already point to them. The value field does not need
	 * to be volatile, though, as iterators cache the reference anyway during
	 * hasNext() and are supposed to return a value, even if the node was
	 * removed between the hasNext() and next() call.
	 *
	 * 4) The prev pointer of a removed node is nulled. This allows the GC to
	 * collect nodes that are no longer reachable by an iterator.
	 */

	static final class Node<T> {
		Node<T> prev;
		volatile Node<T> next;
		T value;

		public Node(T value) {
			this.value = value;
		}
	}

	Node<T> first;
	Node<T> last;
	int size;

	public synchronized void add(T value) {
		final Node<T> node = new Node<>(value);
		if (last == null) {
			first = last = node;
		} else {
			node.prev = last;
			last.next = node;
			last = node;
		}
		size++;
	}

	public synchronized void addAll(Iterable<T> listeners) {
		listeners.forEach(this::add);
	}

	public synchronized boolean addIfNotPresent(T value) {
		if (contains(value))
			return false;
		add(value);
		return true;
	}

	public boolean contains(Object test) {
		for (Node<T> current = first; current != null; current = current.next) {
			if (test.equals(current.value))
				return true;
		}
		return false;
	}

	private synchronized void remove(Node<T> removed) {
		if (removed.value == null)
			return;

		removed.value = null;

		if (first == last) {
			first = last = null;
		} else if (first == removed) {
			first = removed.next;
			first.prev = null;
		} else if (last == removed) {
			last = last.prev;
			last.next = null;
			removed.prev = null;
		} else {
			removed.prev.next = removed.next;
			removed.next.prev = removed.prev;
			removed.prev = null;
		}

		size--;
	}

	public boolean remove(T value) {
		for (Node<T> current = first; current != null; current = current.next) {
			if (value.equals(current.value)) {
				remove(current);
				return true;
			}
		}
		return false;
	}

	public void removeAll(T value) {
		for (Node<T> current = first; current != null; current = current.next) {
			if (value.equals(current.value)) {
				remove(current);
			}
		}
	}

	public boolean removeIf(Predicate<T> pred) {
		boolean removed = false;
		for (Node<T> current = first; current != null; current = current.next) {
			final T val = current.value;
			if (val != null && pred.test(val)) {
				remove(current);
				removed = true;
			}
		}
		return removed;
	}

	@Override
	public void forEach(Consumer<? super T> action) {
		// Slightly faster than Iterable#forEach() (no Iterator needed).
		for (Node<T> current = first; current != null; current = current.next) {
			final T val = current.value;
			if (val != null)
				action.accept(val);
		}
	}

	public void forEach(FailableConsumer<? super T> action,
			Consumer<Exception> exceptionHandler) {
		forEach(t -> action.acceptOrHandle(t, exceptionHandler));
	}

	@Override
	public Spliterator<T> spliterator() {
		// More characteristics than Iterable#spliterator().
		return Spliterators.spliteratorUnknownSize(iterator(), Spliterator.CONCURRENT | Spliterator.NONNULL);
	}

	@Override
	public Iterator<T> iterator() {
		return new GromitIterator();
	}

	private class GromitIterator implements Iterator<T> {

		Node<T> pointer;
		T cachedValue;

		@Override
		public synchronized boolean hasNext() {
			if (pointer == null) {
				if ((pointer = first) == null)
					return false;
				cachedValue = pointer.value;
			}
			while (cachedValue == null && pointer.next != null) {
				pointer = pointer.next;
				cachedValue = pointer.value;
			}
			return cachedValue != null;
		}

		@Override
		public synchronized T next() {
			if (cachedValue == null && !hasNext())
				throw new NoSuchElementException();
			final T tmp = cachedValue;
			cachedValue = null;
			return tmp;
		}

		@Override
		public void remove() {
			GromitIterable.this.remove(pointer);
		}
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder();
		sb.append("GromitIterable(");
		final Iterator<T> it = iterator();
		while (it.hasNext()) {
			sb.append(it.next());
			if (it.hasNext())
				sb.append(", ");
		}
		sb.append(")");
		return sb.toString();
	}
}
