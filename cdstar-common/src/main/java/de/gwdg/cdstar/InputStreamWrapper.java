package de.gwdg.cdstar;

import java.io.IOException;
import java.io.InputStream;

/**
 * A wrapper that delegates all method calls to a source {@link InputStream}.
 *
 * TODO: Remove. There is a FilterInputStream already.
 */
public abstract class InputStreamWrapper extends InputStream {
	private final InputStream src;

	public InputStreamWrapper(InputStream src) {
		this.src = src;
	}

	public final InputStream getSource() {
		return src;
	}

	@Override
	public int read() throws IOException {
		return src.read();
	}

	@Override
	public int read(byte[] b) throws IOException {
		return src.read(b);
	}

	@Override
	public int read(byte[] b, int off, int len) throws IOException {
		return src.read(b, off, len);
	}

	@Override
	public long skip(long n) throws IOException {
		return src.skip(n);
	}

	@Override
	public int available() throws IOException {
		return src.available();
	}

	@Override
	public synchronized void mark(int readlimit) {
		src.mark(readlimit);
	}

	@Override
	public boolean markSupported() {
		return src.markSupported();
	}

	@Override
	public synchronized void reset() throws IOException {
		src.reset();
	}

	@Override
	public void close() throws IOException {
		src.close();
	}

}
