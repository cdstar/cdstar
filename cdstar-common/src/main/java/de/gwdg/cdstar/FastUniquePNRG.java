package de.gwdg.cdstar;

import java.util.concurrent.atomic.AtomicLong;

/**
 * A fast and thread-safe pseudo random number generator based on an LSFR
 * algorithm (Xorshift*).
 * 
 * This PRNG will return a sequence of 2^64-1 unique long numbers (excluding
 * zero) before repeating itself. The only internal state is the last returned
 * number (64bit) and the computation of the next number only requires a couple
 * of shifts and XOR operations and is therefore extremely fast.
 * 
 * Since there are no duplicates (when using the entire 64bit of the output),
 * this is not really random or secure, so beware. This property is useful
 * however to generate random looking unique IDs for tracking or load balancing
 * purpose.
 * 
 * This implementation is thread-safe and lock-free, but if multiple threads are
 * requesting numbers at a high rate, creating multiple instances may be more
 * efficient, especially since instances are so cheap.
 */
public class FastUniquePNRG {

	private final AtomicLong seed;

	private static final FastUniquePNRG shared = new FastUniquePNRG();

	public static FastUniquePNRG getShared() {
		return shared;
	}

	/** Same as XORShiftRandom(0) */
	public FastUniquePNRG() {
		this(0);
	}

	/**
	 * Create a new (seeded) instance. A seed of zero will cause the instance to
	 * seed itself with a time-based number.
	 */
	public FastUniquePNRG(long seed) {
		while (seed == 0)
			seed = System.currentTimeMillis() ^ System.nanoTime();
		this.seed = new AtomicLong(seed);
	}

	public long next() {
		long oldSeed, newSeed;

		do {
			oldSeed = newSeed = seed.get();
			newSeed ^= newSeed >> 12;
			newSeed ^= newSeed << 25;
			newSeed ^= newSeed >> 27;
		} while (!seed.compareAndSet(oldSeed, newSeed));

		return newSeed;
	}

}
