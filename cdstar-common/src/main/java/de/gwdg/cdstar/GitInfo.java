package de.gwdg.cdstar;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.time.Instant;
import java.util.Optional;
import java.util.Properties;

/**
 * Provide meta information about the git
 *
 * @author mahe
 *
 */
public class GitInfo {

	private static final String PROPERTIES_FILE = "cdstar.git.properties";
	private static final GitInfo instance;

	static {
		GitInfo tmp;
		try {
			tmp = new GitInfo();
		} catch (final Exception e) {
			e.printStackTrace();
			tmp = null;
		}
		instance = tmp;
	}

	private final String branch;
	private final String commit;
	private final String version;
	private final boolean dirty;

	private final Instant buildTime;
	private final Instant commitTime;

	private final String tagName;
	private final int tagDistance;

	private GitInfo() throws IOException {
		final Properties gitInfo = new Properties();
		try (InputStream res = getClass().getClassLoader().getResourceAsStream(PROPERTIES_FILE)) {
			if (res == null)
				throw new IOException(PROPERTIES_FILE + " not found on classpath");
			gitInfo.load(res);
		}
		version = gitInfo.getProperty("git.build.version");
		branch = gitInfo.getProperty("git.branch");
		commit = gitInfo.getProperty("git.commit.id.full");
		dirty = gitInfo.getProperty("git.dirty").equalsIgnoreCase("true");
		tagName = gitInfo.getProperty("git.closest.tag.name");
		final String tagDistanceValue = gitInfo.getProperty("git.closest.tag.commit.count");
		tagDistance = Utils.nullOrEmpty(tagDistanceValue) ? -1 : Integer.parseInt(tagDistanceValue);
		buildTime = Utils.fromIsoDate(gitInfo.getProperty("git.build.time")).toInstant();
		commitTime = Utils.fromIsoDate(gitInfo.getProperty("git.commit.time")).toInstant();
	}

	/**
	 * Return the {@link GitInfo} singleton, if available. null otherwise.
	 */
	public static GitInfo getInstance() {
		return instance;
	}

	public static Optional<GitInfo> get() {
		return Optional.of(instance);
	}

	public static boolean isAvailable() {
		return instance != null;
	}

	public static void main(String[] args) {
		final PrintStream out = System.out;
		if (instance == null) {
			out.println("Build information not available.");
			System.exit(1);
		}

		out.println("Version:     " + instance.getVersion());
		out.println("Branch:      " + instance.getBranch());
		out.println("Commit:      " + instance.getCommit());
		out.println("Tag:         " + instance.describe(true));
		out.println("Build-Time:  " + instance.getBuildTime());
		out.println("Commit-Time: " + instance.getCommitTime());
	}

	/**
	 * Return the branch this build was created from.
	 */
	public final String getBranch() {
		return branch;
	}

	/**
	 * Return the commit id/hash this build was created from.
	 */
	public final String getCommit() {
		return commit;
	}

	/**
	 * Return the maven version string.
	 */
	public final String getVersion() {
		return version;
	}

	/**
	 * Return true if the build contained uncommitted changes.
	 */
	public final boolean isDirty() {
		return dirty;
	}

	/**
	 * Return the time this library was built.
	 */
	public final Instant getBuildTime() {
		return buildTime;
	}

	/**
	 * Return the time the commit was created.
	 */
	public final Instant getCommitTime() {
		return commitTime;
	}

	/**
	 * Return the name of the closest git tag, or an empty string if there is no
	 * tag information.
	 */
	public final String getTagName() {
		return tagName;
	}

	/**
	 * Return the distance from the closest tag (see {@link #getTagName()}), or
	 * -1 if no information is available.
	 */
	public final int getTagDistance() {
		return tagDistance;
	}

	/**
	 * Return a descriptive string similar to 'git-describe --tags --always
	 * --dirty'
	 *
	 * @param includeHash
	 *            if true, the commit has is always included, even if there is a
	 *            tag.
	 */
	public String describe(boolean includeHash) {
		final StringBuilder sb = new StringBuilder();
		if (tagName.isEmpty()) {
			sb.append(commit.substring(0, 7));
		} else {
			sb.append(tagName);
			if (tagDistance > 0)
				sb.append('-').append(tagDistance);
			if (includeHash)
				sb.append("-g").append(commit.substring(0, 7));
		}
		if (dirty)
			sb.append("-dirty");

		return sb.toString();
	}

}
