package de.gwdg.cdstar;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileChannel.MapMode;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Class to calculate multiple digests using different algorithms in one go.
 *
 * Hint: During tests, using parallel streams (three threads, 128k blocksize)
 * had over 60% overhead while only increasing throughput by 20%.
 */
public class MultiDigest {

	/* read buffer for InputStream inputs */
	private static final int READ_BUFFER = 1024 * 64;
	private final List<MessageDigest> runningDigests;
	private final Map<String, byte[]> results;
	long size = 0;

	public MultiDigest(Collection<String> algorithms) {
		this(algorithms.toArray(new String[algorithms.size()]));
	}

	public MultiDigest(String... algorithms) {
		results = new HashMap<>(algorithms.length);
		runningDigests = new ArrayList<>(algorithms.length);
		try {
			for (String name : algorithms) {
				runningDigests.add(MessageDigest.getInstance(name));
			}
		} catch (final NoSuchAlgorithmException e) {
			throw new RuntimeException(e);
		}
	}

	public void reset() {
		runningDigests.forEach(MessageDigest::reset);
		results.clear();
		size = 0;
	}

	public Map<String, byte[]> digest() {
		if (results.isEmpty()) {
			runningDigests.forEach(md -> results.put(md.getAlgorithm(), md.digest()));
		}
		return results;
	}

	public MultiDigest update(byte[] data, int skip, int limit) {
		int len = Math.min(data.length - skip, limit);
		if (len <= 0)
			return this;

		// Note: Parallel processing 1MB chunks with 4 digests (4 threads) reduced
		// runtime by only 20% (instead of 75%). Smaller chunks perform even worse.
		// Conclusion: Not worth it.
		runningDigests.forEach(md -> md.update(data, skip, len));
		size += len;

		return this;
	}

	public MultiDigest update(ByteBuffer buffer) {
		if (buffer.remaining() <= 0)
			return this;

		runningDigests.forEach(md -> md.update(buffer.duplicate()));
		size += buffer.remaining();

		return this;
	}

	public MultiDigest update(byte b) {
		runningDigests.forEach(md -> md.update(b));
		size += 1;
		return this;
	}

	public MultiDigest update(byte[] data) {
		return update(data, 0, data.length);
	}

	public MultiDigest update(InputStream stream) throws IOException {
		final byte[] buffer = new byte[READ_BUFFER];
		int bytes;
		while ((bytes = stream.read(buffer)) > -1) {
			update(buffer, 0, bytes);
		}
		return this;
	}

	public MultiDigest update(Path path) throws IOException {
		try (FileChannel fc = FileChannel.open(path, StandardOpenOption.READ)) {
			for (long i = 0; i < fc.size(); i += Integer.MAX_VALUE) {
				update(fc.map(MapMode.READ_ONLY, i, Math.min(fc.size() - i, Integer.MAX_VALUE)));
			}
		}
		return this;
	}

	public InputStream wrap(InputStream stream) {
		return new FilterInputStream(stream) {

			@Override
			public int read(byte[] b, int off, int len) throws IOException {
				final int actuallyRead = in.read(b, off, len);
				if (actuallyRead > 0)
					update(b, off, actuallyRead);
				return actuallyRead;
			}

			@Override
			public int read() throws IOException {
				final int tmp = in.read();
				if (tmp > -1)
					update((byte) (tmp - 128));
				return tmp;
			}

			@Override
			public int read(byte[] b) throws IOException {
				return read(b, 0, b.length);
			}

		};
	}

	public long totalBytes() {
		return size;
	}

}
