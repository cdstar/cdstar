package de.gwdg.cdstar;

import java.util.function.Function;

public class Result<T, E extends Throwable> {
	private T value;
	private E error;

	Result(T value, E error) {
		this.value = value;
		this.error = error;
	}

	public T unwrap() throws E {
		if(error != null)
			throw error;
		return value;
	}

	public T get(T fallback) {
		return error == null ? value : fallback;
	}

	public E error() {
		return error;
	}

	public static <T, E extends Throwable> Result<T, E> of(T value) {
		return new Result<>(value, null);
	}

	public static <T, E extends Throwable> Result<T, E> error(E error) {
		return new Result<>(null, error);
	}

	public <V> Result<V, E> flatMap(Function<T, Result<V, E>> f) {
		if (this.error != null)
			return Result.error(error);
		return f.apply(value);
	}

	public <V> Result<V, E> map(Function<T, V> f) {
		return flatMap(f.andThen(Result::of));
	}

}