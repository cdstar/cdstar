package de.gwdg.cdstar;

import java.util.function.Supplier;

/**
 * Like {@link Supplier}, but allowed to throw checked exceptions.
 */
@FunctionalInterface
public interface FailableSupplier<T, E extends Throwable> {
	T supply() throws E;
}