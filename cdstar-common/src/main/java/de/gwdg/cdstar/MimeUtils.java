package de.gwdg.cdstar;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;

public class MimeUtils {
	private static final MimeGuess unknwonType = new MimeGuess(null, null);
	public static final String OCTET_STREAM = "application/octet-stream";
	public static final String APPLICATION_X_AUTODETECT = "application/x-autodetect";

	public static class MimeGuess {
		final String encoding;
		final String mime;

		public MimeGuess(String mime, String encoding) {
			this.mime = mime;
			this.encoding = encoding;
		}

		public String getEncoding() {
			return encoding;
		}

		public String getMime() {
			return mime;
		}

		public String getMimeDefault(String defaultMime) {
			return mime == null ? defaultMime : mime;
		}
	}

	static Map<String, String> encodingNames;
	static Map<String, String> aliasMap;
	static Map<String, String> mime;
	static Set<String> compressed;
	static Set<String> uncompressed;

	private MimeUtils() {
	}

	private static synchronized void init() {
		if (mime != null)
			return;

		try {
			mime = new HashMap<>();
			encodingNames = new HashMap<>();
			aliasMap = new HashMap<>();
			compressed = new HashSet<>();
			uncompressed = new HashSet<>();

			forLineInResource("mimeutils.types", line -> {
				final String[] parts = line.split(" ");
				if (parts.length == 2)
					mime.put(parts[0], parts[1]);
			});

			forLineInResource("mimeutils.encoding", line -> {
				final String[] parts = line.split(" ");
				if (parts.length != 2)
					return;
				encodingNames.put(parts[0], parts[1]);
			});

			forLineInResource("mimeutils.alias", line -> {
				final String[] parts = line.split(" ");
				if (parts.length != 2 || !parts[1].contains("."))
					return;
				aliasMap.put(parts[0], parts[1]);
			});

			forLineInResource("mimeutils.compressed", line -> {
				compressed.add(line.trim());
			});

			forLineInResource("mimeutils.uncompressed", line -> {
				uncompressed.add(line.trim());
			});

		} catch (final IOException e) {
			throw new RuntimeException(e);
		}
	}

	private static void forLineInResource(String resourceName, Consumer<String> lineConsumer) throws IOException {
		for (final URL url : Utils.iter(MimeUtils.class.getClassLoader().getResources(resourceName))) {
			try (final BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()))) {
				String line;
				while ((line = reader.readLine()) != null) {
					if (line.startsWith("#"))
						continue;
					lineConsumer.accept(line);
				}
			}
		}
	}

	public static MimeGuess guess(String filename) {
		return guess(filename, true);
	}

	public static MimeGuess guess(String filename, boolean guessEncoding) {
		if (mime == null)
			init();

		int dot = filename.lastIndexOf('.');
		if (dot < 0) {
			// No dot in filename
			return unknwonType;
		}
		String ext = filename.substring(dot + 1);

		// Replace file.tgz with file.tar.gz
		if (aliasMap.containsKey(ext)) {
			filename = filename.substring(0, dot + 1) + aliasMap.get(ext);
			dot = filename.lastIndexOf('.');
			ext = filename.substring(dot + 1);
		}

		if (guessEncoding) {
			final String encoding = encodingNames.get(ext);
			if (encoding != null) {
				final int dot2 = filename.lastIndexOf('.', dot - 1);
				if (dot2 > 0) {
					// Nested extensions --> file.*.gz
					ext = filename.substring(dot2 + 1, dot);
					return new MimeGuess(mime.get(ext), encoding);
				} else {
					// Compressed blob --> file.gz
					return new MimeGuess(null, encoding);
				}
			}
		}

		// Not a known encoding, or encoding-guessing is disabled.
		return new MimeGuess(mime.get(ext), null);
	}

	/**
	 * Return true if the given mime type is likely to be compressed natively.
	 */
	public static boolean isCompressed(String type) {
		if (compressed == null)
			init();
		return compressed.contains(type) || type.contains("compressed") || type.startsWith("video/");
	}

	/**
	 * Return true if the given mime type is likely to be uncompressed natively and
	 * might benefit from compression.
	 */
	public static boolean isUncompressed(String type) {
		if (uncompressed == null)
			init();
		return uncompressed.contains(type) || type.startsWith("text/");
	}

}
