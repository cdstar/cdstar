package de.gwdg.cdstar;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * A thread factory that creates named threads: "<prefix><number>"
 *
 */
public class NamedThreadFactory implements ThreadFactory {

	AtomicInteger counter = new AtomicInteger(0);
	private final String prefix;
	private final ThreadGroup group;
	private boolean deamon = false;
	private int priority = Thread.NORM_PRIORITY;

	public NamedThreadFactory(String pefixString) {
		prefix = pefixString;
		group = new ThreadGroup(pefixString + "-group");
	}

	public String getPrefix() {
		return prefix;
	}

	public NamedThreadFactory deamon(boolean mode) {
		deamon = mode;
		return this;
	}

	public boolean getDeamon() {
		return deamon;
	}

	public NamedThreadFactory priority(int prio) {
		priority = prio;
		return this;
	}

	public int getPriority() {
		return priority;
	}

	@Override
	public Thread newThread(Runnable target) {
		final String uniqueName = prefix + "-" + counter.getAndIncrement();
		final Thread t = new Thread(group, target, uniqueName);
		if (t.isDaemon() != deamon)
			t.setDaemon(deamon);
		if (t.getPriority() != priority)
			t.setPriority(priority);
		return t;
	}
}
