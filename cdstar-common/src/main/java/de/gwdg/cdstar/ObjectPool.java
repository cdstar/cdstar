package de.gwdg.cdstar;

import java.nio.ByteBuffer;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.concurrent.atomic.LongAdder;
import java.util.function.Predicate;
import java.util.function.Supplier;

/**
 * Pool for re-usable {@link ByteBuffer} instances.
 */
public abstract class ObjectPool<T> {

	private int poolSize = 16;
	private final Deque<T> pool = new ArrayDeque<>(poolSize);

	private final LongAdder recycledCounter = new LongAdder();
	private final LongAdder allocatedCounter = new LongAdder();

	public static <T> ObjectPool<T> createPool(int poolSize, Supplier<T> alloc, Predicate<T> free) {
		return new ObjectPool<T>(poolSize) {
			@Override
			protected T alloc() {
				return alloc.get();
			}
			@Override
			protected boolean free(T obj) {
				return free.test(obj);
			}
		};
	}

	public ObjectPool(int poolSize) {
		setPoolSize(poolSize);
	}

	public int getPoolSize() {
		return poolSize;
	}

	public void setPoolSize(int newSize) {
		poolSize = newSize;
	}

	public long getRecycleCount() {
		return recycledCounter.sum();
	}

	public long getAllocateCount() {
		return allocatedCounter.sum();
	}

	/**
	 * Return a recycled, if available. Otherwise allocate and return a new buffer.
	 *
	 * Recycled buffers are cleared, but not zeroed out. They may still contain
	 * stale data from a previous use.
	 */
	public T get() {
		final T obj;
		synchronized (pool) {
			obj = pool.pollLast();
		}

		if (obj == null) {
			allocatedCounter.add(1);
			return alloc();
		} else {
			recycledCounter.add(1);
			return obj;
		}
	}

	/**
	 * Recycle an instance. Return true if it was recycled, false otherwise.
	 * Instances may be rejected if the recycle pool is full or the instances does
	 * not meet certain criteria (see {@link #free(Object)}.
	 */
	public boolean recycle(T obj) {
		if (!free(obj))
			return false;

		synchronized (pool) {
			if (pool.size() < poolSize) {
				pool.addLast(obj);
				return true;
			}
		}
		return false;
	}

	/**
	 * Clean an instance before it is re-entered into the pool. Return true if the
	 * instance can be recycled, false otherwise.
	 */
	protected abstract boolean free(T obj);

	protected abstract T alloc();

}
