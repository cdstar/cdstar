package de.gwdg.cdstar;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.FileAttribute;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.text.MessageFormat;
import java.text.Normalizer;
import java.time.Duration;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Collection;
import java.util.Date;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Utils {

	/**
	 * @deprecated use StandardCharsets.UTF_8
	 */
	@Deprecated
	public static final Charset UTF8 = StandardCharsets.UTF_8;

	/**
	 * Given an iterator, return an iterable that returns this iterator. Used to
	 * bridge interfaces that return iterators from various methods (e.g.
	 * JsonNode.fieldNames()).
	 */
	public static <E> Iterable<E> iter(final Iterator<E> iterator) {
		if (iterator == null) {
			throw new NullPointerException();
		}
		return new Iterable<E>() {
			@Override
			public Iterator<E> iterator() {
				return iterator;
			}
		};
	}

	public static <E> Iterable<E> iter(final Stream<E> stream) {
		if (stream == null) {
			throw new NullPointerException();
		}
		return iter(stream.iterator());
	}

	public static <E> Iterable<E> iter(final Enumeration<E> enumeration) {
		if (enumeration == null) {
			throw new NullPointerException();
		}
		return iter(new Iterator<E>() {
			@Override
			public boolean hasNext() {
				return enumeration.hasMoreElements();
			}

			@Override
			public E next() {
				return enumeration.nextElement();
			}
		});
	}

	/**
	 * Return a new instance of a named class implementing a specific interface
	 * or superclass. If the name does not specify a package (does not contain a
	 * dot), it is prefixed with the package of the base class.
	 *
	 * The (unchecked) ClassCastException is wrapped in a (checked)
	 * ReflectiveOperationException so you can handle it just like any other
	 * class-loading error. There are various subclasses of
	 * ReflectiveOperationException that can be thrown, but
	 * ClassNotFoundException is probably the most interesting.
	 *
	 * @throws ReflectiveOperationException
	 *             on any errors during instantiation or casting.
	 * @throws ClassNotFoundException
	 *             if class was not found.
	 */
	public static <T> T getInstance(String name, Class<T> baseClass) throws ReflectiveOperationException {
		if (!name.contains("."))
			name = baseClass.getPackage() + "." + name;

		try {
			ClassLoader cl = baseClass.getClassLoader();
			if (cl == null)
				cl = ClassLoader.getSystemClassLoader();
			return baseClass.cast(cl.loadClass(name).getConstructor().newInstance());
		} catch (final ClassCastException e) {
			throw new ReflectiveOperationException(e.getMessage(), e);
		}
	}

	public static void assertTrue(boolean value, String msg) {
		if (!value)
			throw new AssertionError(msg);
	}

	public static <T> T notNull(T value) {
		if (value == null)
			throw new NullPointerException();
		return value;
	}

	public static void notNull(Object... values) {
		for (final Object v : values)
			if (v == null)
				throw new NullPointerException();
	}

	/**
	 * Quietly delete a file.
	 *
	 * @param path
	 *            file to delete, or null.
	 * @return True the file existed and was deleted, false otherwise.
	 */
	public static boolean deleteQuietly(Path path) {
		if (path == null)
			return false;
		try {
			return Files.deleteIfExists(path);
		} catch (final IOException e) {
			return false;
		}
	}

	/**
	 * Quietly delete an entire directory tree, and return true on success.
	 *
	 * @param path to delete (recursively)
	 * @return true on success
	 */
	public static boolean deleteDirectoryTree(Path path) {
		try {
			FileUtils.deleteDirectory(path.toFile());
			return true;
		} catch (final IOException e) {
			return false;
		}
	}

	/**
	 * Copy everything from source to target. Return the number of bytes copied.
	 */
	public static long copy(InputStream source, OutputStream target) throws IOException {
		// TODO: In Java 9, change this to transferTo
		long count = 0;
		int n;
		final byte[] buffer = new byte[1024 * 8];
		while (-1 != (n = source.read(buffer))) {
			target.write(buffer, 0, n);
			count += n;
		}
		return count;
	}

	/**
	 * Creates a relative symlink from two relative or absolute paths. Can be
	 * used if symlink and target are tightly coupled and always moved together.
	 * Absolute symlinks tend to break, e.g. when changing the mountpoint of a
	 * file system.
	 */
	public static void relativeSymlink(Path from, Path to, FileAttribute<?>... attrs) throws IOException {
		Files.createSymbolicLink(from, from.getParent().relativize(to));
	}

	/**
	 * Move (rename) a file. If the target file exists, it is renamed into a
	 * backup file before the move, and deleted after. This way, if anything
	 * goes wrong, a backup of the original file is still present.
	 */
	public static void moveBackup(Path from, Path to) throws IOException {
		if (Files.exists(to)) {
			final String suffix = ".bak-" + System.currentTimeMillis();
			final Path tmp = to.resolveSibling(to.getFileName().toString() + suffix);
			Files.move(to, tmp);
			try {
				Files.move(from, to);
			} catch (final IOException e) {
				Files.move(tmp, to);
				throw e;
			}
			Files.delete(tmp);
		} else {
			Files.move(from, to);
		}
	}

	protected final static char[] hexArray = "0123456789abcdef".toCharArray();

	/**
	 * Return a (lowercase) hexadecimal representation of the given byte array.
	 */
	public static String bytesToHex(byte[] bytes) {
		return new String(bytesToHexChars(bytes));
	}

	public static char[] bytesToHexChars(byte[] bytes) {
		if (bytes.length == 0)
			return new char[0];
		final char[] hexChars = new char[bytes.length * 2];
		for (int j = 0; j < bytes.length; j++) {
			final int v = bytes[j] & 0xFF;
			hexChars[j * 2] = hexArray[v >>> 4];
			hexChars[j * 2 + 1] = hexArray[v & 0x0F];
		}
		return hexChars;
	}

	public static byte[] hexToBytes(String hex) {
		final int len = hex.length();
		final byte[] result = new byte[len / 2];
		try {
			for (int i = 0; i < len; i += 2) {
				final int high = hexToHalfByte(hex.charAt(i)) << 4;
				final int low = hexToHalfByte(hex.charAt(i + 1));
				result[i / 2] = (byte) (low + high);
			}
		} catch (final StringIndexOutOfBoundsException e) {
			throw new IllegalArgumentException("Uneven number of characters in hex string.");
		}
		return result;
	}

	private static final byte hexToHalfByte(char c) {
		if (c >= '0' && c <= '9')
			return (byte) (c - '0');
		if (c >= 'A' && c <= 'F')
			return (byte) (10 + c - 'A');
		if (c >= 'a' && c <= 'f')
			return (byte) (10 + c - 'a');
		throw new IllegalArgumentException("Not a valid hex character: " + (int) c);
	}

	public static String base64encode(byte[] data) {
		return new String(Base64.getEncoder().encode(data), StandardCharsets.UTF_8);
	}

	public static String base64encode(ByteBuffer data) {
		if (data.hasArray() && data.arrayOffset() == 0 && data.position() == 0 && data.remaining() == data.limit())
			return new String(Base64.getEncoder().encode(data.array()), StandardCharsets.UTF_8);
		final byte[] copy = new byte[data.remaining()];
		data.get(copy);
		return base64encode(copy);
	}

	public static String base64encode(String string) {
		return base64encode(string.getBytes(StandardCharsets.UTF_8));
	}

	public static byte[] base64decode(String data) {
		return Base64.getDecoder().decode(data);
	}

	/**
	 * Compare two objects in a null-save way. Return true if both objects are
	 * equal or both are null. Return false otherwise.
	 */
	public static boolean equal(Object a, Object b) {
		return (a != null && b != null && a.equals(b)) || (a == null && b == null);
	}

	/**
	 * Compare two objects in a null-save way. Return true if both objects are
	 * equal and not null. Return false otherwise.
	 */
	public static boolean equalNotNull(Object a, Object b) {
		return (a != null && b != null && a.equals(b));
	}

	public static <T, R> List<R> map(Collection<T> list, Function<T, R> cb) {
		final List<R> results = new ArrayList<>(list.size());
		list.forEach(o -> results.add(cb.apply(o)));
		return results;
	}

	@SafeVarargs
	public static <T, R> R[] map(Function<T, R> cb, T... values) {
		@SuppressWarnings("unchecked")
		final R[] results = (R[]) new Object[values.length];
		for (int i = 0; i < values.length; i++) {
			results[i] = cb.apply(values[i]);
		}
		return results;
	}

	public static <T, R> Iterator<R> imap(Iterator<T> elements, Function<T, R> cb) {
		return Utils.imap((Iterable<T>) () -> elements, cb);
	}

	public static <T, R> Iterator<R> imap(Iterable<T> elements, Function<T, R> cb) {
		return StreamSupport.stream(elements.spliterator(), false).map(cb).iterator();
	}

	/**
	 * Turn stuff into an array list (make a copy).
	 */

	public static <T> List<T> toArrayList(Enumeration<? extends T> iterator) {
		final List<T> result = new ArrayList<>();
		while (iterator.hasMoreElements())
			result.add(iterator.nextElement());
		return result;
	}

	public static <T> List<T> toArrayList(Iterator<? extends T> iterator) {
		final List<T> result = new ArrayList<>();
		while (iterator.hasNext())
			result.add(iterator.next());
		return result;
	}

	private static <T> List<T> toArrayList(Iterator<? extends T> iterator, int sizeHint) {
		final List<T> result = new ArrayList<>(sizeHint);
		while (iterator.hasNext())
			result.add(iterator.next());
		return result;
	}

	public static <T> List<T> toArrayList(Iterable<? extends T> iterable) {
		return toArrayList(iterable.iterator());
	}

	public static <T> List<T> toArrayList(Collection<? extends T> collection) {
		return new ArrayList<>(collection);
	}

	public static <T> List<T> toArrayList(T[] collection) {
		final List<T> result = new ArrayList<>(collection.length);
		for (final T item : collection)
			result.add(item);
		return result;
	}

	@SafeVarargs
	public static <T> List<T> toArrayList(T firstElement, T... collection) {
		final List<T> result = new ArrayList<>(collection.length + 1);
		result.add(firstElement);
		for (final T item : collection)
			result.add(item);
		return result;
	}

	public static <T> List<T> toArrayList(Stream<? extends T> stream) {
		return toArrayList(stream.iterator());
	}

	public static <T> List<T> toArrayList(Stream<? extends T> stream, int sizeHint) {
		return toArrayList(stream.iterator(), sizeHint);
	}

	/**
	 * String formatting with cached patterns.
	 */

	static Map<String, MessageFormat> patternCache = new LRUCache<>(1024);

	public static String format(String pattern, Object... arguments) {
		MessageFormat m;
		synchronized (patternCache) {
			m = patternCache.get(pattern);
		}
		if (m == null) {
			m = new MessageFormat(pattern);
			synchronized (patternCache) {
				patternCache.put(pattern, m);
			}
		}

		return m.format(arguments);
	}

	public static void printf(String pattern, Object... arguments) {
		System.out.println(format(pattern, arguments));
	}

	/**
	 * Turn a string into something that can be used as a filename in almost any
	 * file system. The process is irreversible, but tries hard to create unique
	 * file names (by using strong hashes) while still keeping as much of the
	 * original string as possible.
	 *
	 * The returned string contains only legal and less than 128 characters.
	 */
	public static String saveFilename(String source) {
		String clean = source.replaceAll("[^a-zA-Z0-9-_\\.\\\\/]+", "_");
		if (clean.equals(source) && source.length() < 128)
			return source;
		if (clean.length() + 40 + 1 > 128)
			clean = clean.substring(0, 128 - 40 - 1);
		return clean + "-" + sha1(source);

	}

	/**
	 * Normalizes a unicode string using NFKC mode (Compatibility decomposition,
	 * followed by canonical composition) which is considered best practice to
	 * prevent visual spoofing (strings look the same but have different
	 * byte-level representations).
	 */
	public static String normalizeNFKC(String name) {
		return Normalizer.normalize(name, Normalizer.Form.NFKC);
	}

	public static byte[] md5(byte[] input) {
		try {
			return MessageDigest.getInstance("md5").digest(input);
		} catch (final NoSuchAlgorithmException e) {
			throw wtf(e);
		}
	}

	public static String sha1(String input) {
		try {
			return bytesToHex(MessageDigest.getInstance("SHA-1").digest(input.getBytes()));
		} catch (final NoSuchAlgorithmException e) {
			throw wtf("Runtime does not implement mandatory hashing algorithm SHA-1", e);
		}
	}

	public static String sha256(String input) {
		try {
			return bytesToHex(MessageDigest.getInstance("SHA-256").digest(input.getBytes()));
		} catch (final NoSuchAlgorithmException e) {
			throw wtf("Runtime does not implement mandatory hashing algorithm SHA-256", e);
		}
	}

	public static void closeQuietly(AutoCloseable stream) {
		try {
			if (stream != null)
				stream.close();
		} catch (final Exception e) {
			// psssst !
		}
	}

	private static final SecureRandom rnd = new SecureRandom();

	public static byte[] randomBytes(int n) {
		final byte[] result = new byte[n];
		rnd.nextBytes(result);
		return result;
	}

	public static char[] randomChars(int len, String alphabet) {
		final char[] chars = new char[len];
		for (int i = 0; i < chars.length; i++)
			chars[i] = alphabet.charAt(rnd.nextInt(alphabet.length()));
		return chars;
	}

	public static byte[] sign(byte[] data, byte[] key) {
		final SecretKeySpec signingKey = new SecretKeySpec(key, "HmacSHA256");
		Mac hmac;
		try {
			hmac = Mac.getInstance("HmacSHA256");
			hmac.init(signingKey);
		} catch (final NoSuchAlgorithmException | InvalidKeyException e) {
			throw new RuntimeException(e);
		}
		hmac.update(data);
		return hmac.doFinal();
	}

	public static boolean signCheck(byte[] compare, byte[] data, byte[] key) {
		return isEqual(compare, sign(data, key));
	}

	public static boolean isEqual(byte[] a, byte[] b) {
		if (a.length != b.length) {
			return false;
		}

		int result = 0;
		for (int i = 0; i < a.length; i++) {
			result |= a[i] ^ b[i];
		}
		return result == 0;
	}

	/**
	 * Convert a char array to a byte array (UTF8) without instantiating a
	 * string. Strings are immutable and may remain in memory even after their
	 * instances are gone. It is thus considered bad practice to store passwords
	 * in strings.
	 */
	public static byte[] toBytes(char[] chars) {
		final ByteBuffer byteBuffer = StandardCharsets.UTF_8.encode(CharBuffer.wrap(chars));
		final byte[] result = Arrays.copyOfRange(byteBuffer.array(), byteBuffer.position(), byteBuffer.limit());
		Arrays.fill(byteBuffer.array(), (byte) 0); // clear sensitive data
		return result;
	}

	/**
	 * Convert a byte array to a char array (UTF8) without instantiating a
	 * string. Strings are immutable and may remain in memory even after their
	 * instances are gone. It is thus considered bad practice to store passwords
	 * in strings.
	 */
	public static char[] toChars(byte[] bytes) {
		final CharBuffer charBuffer = StandardCharsets.UTF_8.decode(ByteBuffer.wrap(bytes));
		final char[] result = Arrays.copyOfRange(charBuffer.array(), charBuffer.position(), charBuffer.limit());
		Arrays.fill(charBuffer.array(), '\0'); // clear sensitive data
		return result;
	}

	/**
	 * Convert a char array to a byte array (UTF8) without instantiating a
	 * string, then destroy the content of the char array.
	 */
	public static byte[] toBytesAndBurnAfterReading(char[] chars) {
		try {
			return toBytes(chars);
		} finally {
			Arrays.fill(chars, '\u0000'); // clear sensitive data
		}
	}

	public static Logger getLogger() {
		final String name = new Exception().getStackTrace()[1].getClassName();
		return LoggerFactory.getLogger(name);
	}

	/**
	 * Return true if there is no lowercase letter in this string. Non-letter
	 * characters are ignored.
	 */
	public static boolean isUpperCase(String grant) {
		for (final char c : grant.toCharArray()) {
			if (Character.isLowerCase(c))
				return false;
		}
		return true;
	}

	/**
	 * Return true if there is no uppercase letter in this string. Non-letter
	 * characters are ignored.
	 */
	public static boolean isLowerCase(String grant) {
		for (final char c : grant.toCharArray()) {
			if (Character.isUpperCase(c))
				return false;
		}
		return true;
	}

	private static String trimSpace(String src, int start, int end) {
		while (end - start > 0 && src.charAt(start) == ' ')
			start++;
		while (end - start > 0 && src.charAt(end - 1) == ' ')
			end--;
		return src.substring(start, end);
	}

	/**
	 * Split a string on a character and also remove space (only the literal
	 * space character, other whitespace is kept) around the separator in a
	 * single step. The result may include empty strings.
	 */
	public static List<String> splitTrimSpace(String string, char ch) {
		final ArrayList<String> list = new ArrayList<>();
		final int len = string.length();
		int last = 0;
		int next;

		while ((next = string.indexOf(ch, last)) != -1) {
			list.add(trimSpace(string, last, next));
			last = next + 1;
		}

		if (last < len)
			list.add(trimSpace(string, last, len));
		else if (last == len)
			list.add("");

		return list;
	}

	public static List<String> split(String string, char ch) {
		final ArrayList<String> list = new ArrayList<>();
		int off = 0;
		int next;

		while ((next = string.indexOf(ch, off)) != -1) {
			list.add(string.substring(off, next));
			off = next + 1;
		}

		if (off < string.length())
			list.add(string.substring(off));

		return list;
	}

	public static List<String> split(String string, String ch) {
		if (ch.length() == 1)
			return split(string, ch.charAt(0));
		final ArrayList<String> list = new ArrayList<>();
		int off = 0;
		int next;

		while ((next = string.indexOf(ch, off)) != -1) {
			list.add(string.substring(off, next));
			off = next + ch.length();
		}

		if (off < string.length())
			list.add(string.substring(off));

		return list;
	}

	public static String join(String sep, Iterable<?> parts) {
		final Iterator<?> iter = parts.iterator();
		final StringBuilder sb = new StringBuilder(iter.next().toString());
		if (notNullOrEmpty(sep)) {
			while (iter.hasNext())
				sb.append(sep).append(iter.next().toString());
		} else {
			while (iter.hasNext())
				sb.append(iter.next());
		}
		return sb.toString();
	}

	public static String join(String sep, Collection<?> parts) {
		final int len = parts.size();
		if (len == 0)
			return "";

		if (len == 1)
			return parts.iterator().next().toString();

		return join(sep, (Iterable<? extends Object>) parts);
	}

	public static String join(String sep, String... parts) {
		final int len = parts.length;
		if (len == 0)
			return "";
		if (len == 1)
			return parts[0];
		final StringBuilder sb = new StringBuilder(parts[0]);
		if (notNullOrEmpty(sep)) {
			for (int i = 1; i < len; i++) {
				sb.append(sep).append(parts[i]);
			}
		} else {
			for (int i = 1; i < len; i++) {
				sb.append(parts[i]);
			}
		}
		return sb.toString();
	}

	/**
	 * Count how often a string appears in another string.
	 *
	 * @param text   String to search in
	 * @param needle String to search for
	 * @return number of times the needle appears in text.
	 */
	public static int count(String text, String needle) {
		int count = 0;
		for (int i = 0; (i = text.indexOf(needle, i)) != -1; i += needle.length())
			count++;
		return count;
	}

	/**
	 * Count how often a char appears in another string.
	 *
	 * @param text   String to search in
	 * @param needle Char to search for
	 * @return number of times the needle appears in text.
	 */
	public static int count(String text, char needle) {
		int count = 0;
		for (int i = 0; i < text.length(); i++)
			if (text.charAt(i) == needle)
				count++;
		return count;
	}

	public static boolean notNullOrEmpty(String value) {
		return value != null && !value.isEmpty();
	}

	public static boolean notNullOrEmpty(Collection<?> value) {
		return value != null && !value.isEmpty();
	}

	public static boolean notNullOrEmpty(Map<?, ?> value) {
		return value != null && !value.isEmpty();
	}

	/**
	 * Fetch a property from the environment. Try java properties first, then
	 * environment variables (uppercase and '.' replaced with '_').
	 */
	public static String getProperty(String propName, String defaultValue) {
		String value;
		if ((value = System.getProperty(propName)) == null
				&& (value = System.getenv(propName.toUpperCase().replaceAll("\\.", "_"))) == null)
			value = defaultValue;
		return value;
	}

	/**
	 * @deprecated Use {@link #wtf()}
	 */
	@Deprecated
	public static RuntimeException impossibleException(Exception e) {
		return new RuntimeException("This should not be possible.", e);
	}

	private static final DateTimeFormatter ISO8601 = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSX");
	private static final DateTimeFormatter ISO8601_buggy = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSSX");

	public static final ZoneId GMT = ZoneId.of("GMT");

	public static String toIsoDate(Instant date) {
		return ISO8601.format(date);
	}

	public static String toIsoDate(Date date) {
		return ISO8601.format(date.toInstant().atZone(GMT));
	}

	public static String toIsoDate(long millis) {
		return ISO8601.format(Instant.ofEpochMilli(millis).atZone(GMT));
	}

	/**
	 * Parses a string as a {@link Duration}. Without suffix, the string is
	 * interpreted as a decimal millisecond count. With a case insensitive suffix of
	 * either S,M,H or D the number is interpreted as seconds, minutes, hours or
	 * days respectively.
	 */
	public static Duration parseDuration(String input) {
		char last = input.charAt(input.length() - 1);
		if (last == 's' || last == 'S')
			return Duration.ofSeconds(Long.parseLong(input, 0, input.length()-1, 10));
		if (last == 'm' || last == 'M')
			return Duration.ofMinutes(Long.parseLong(input, 0, input.length()-1, 10));
		if (last == 'h' || last == 'H')
			return Duration.ofHours(Long.parseLong(input, 0, input.length()-1, 10));
		if (last == 'd' || last == 'D')
			return Duration.ofDays(Long.parseLong(input, 0, input.length()-1, 10));
		return Duration.ofMillis(Long.parseLong(input));
	}

	public static Date fromIsoDate(String date) throws DateTimeParseException {
		try {
			return new Date(ISO8601.parse(date, Instant::from).toEpochMilli());
		} catch (DateTimeParseException e) {
			// Fallback for dates written by the buggy DateTimeFormatter
			return new Date(ISO8601_buggy.parse(date, Instant::from).toEpochMilli());
		}
	}

	public static boolean nullOrEmpty(String value) {
		return !notNullOrEmpty(value);
	}

	/**
	 * See {@link #wtf()}
	 */
	public static RuntimeException wtf(String string) {
		throw new RuntimeException("This should not be possible: " + string);
	}

	/**
	 * See {@link #wtf()}
	 */
	public static RuntimeException wtf(String string, Throwable e) {
		throw new RuntimeException("This should not be possible: " + string, e);
	}

	/**
	 * See {@link #wtf()}
	 */
	public static RuntimeException wtf(Throwable e) {
		throw new RuntimeException("This should not be possible.", e);
	}

	/**
	 * Throw a {@link RuntimeException}. This method also returns a
	 * {@link RuntimeException} so it can be used in a `throw` statement the
	 * compiler recognizes it as an exit point for a function.
	 */
	public static RuntimeException wtf() {
		throw new RuntimeException("This should not be possible.");
	}

	public static <T> boolean arrayContains(T[] array, T search) {
		return indexOf(array, search, 0) > -1;
	}

	public static boolean arrayContains(char[] array, char search) {
		return indexOf(array, search, 0) > -1;
	}

	public static boolean arrayContains(byte[] array, byte search) {
		return indexOf(array, search, 0) > -1;
	}

	public static <T> int indexOf(T[] array, T search, int offset) {
		for (int i = offset; i < array.length; i++)
			if (equal(array[i], search))
				return i;
		return -1;
	}

	public static int indexOf(char[] array, char search, int offset) {
		for (int i = offset; i < array.length; i++)
			if (array[i] == search)
				return i;
		return -1;
	}

	public static int indexOf(byte[] array, byte search, int offset) {
		for (int i = offset; i < array.length; i++)
			if (array[i] == search)
				return i;
		return -1;
	}

	public static <T> int indexOf(List<T> list, T search, int offset) {
		for (int i = offset; i < list.size(); i++)
			if (equal(list.get(i), search))
				return i;
		return -1;
	}

	public static float gate(float min, float value, float max) {
		if (value < min)
			return min;
		if (value > max)
			return max;
		return value;
	}

	public static double gate(double min, double value, double max) {
		if (value < min)
			return min;
		if (value > max)
			return max;
		return value;
	}

	public static int gate(int min, int value, int max) {
		if (value < min)
			return min;
		if (value > max)
			return max;
		return value;
	}

	public static long gate(long min, long value, long max) {
		if (value < min)
			return min;
		if (value > max)
			return max;
		return value;
	}

	/**
	 * Return the lowest positive integer that is equal to or larger than
	 * <code>i</code>, and also a power of two.
	 */
	public static int nextNiceNumber(int i) {
		int n = Integer.highestOneBit(i);
		if (n != i)
			n = n << 1;
		return Math.max(n, i);
	}

	/**
	 * Return the distance (number of inheritances) between a superclass and its
	 * subclass, or -1 if the second parameter is not a subclass of the first
	 * parameter.
	 */
	public static int getClassDistance(Class<?> superclass, Class<?> subclass) {
		if (!superclass.isAssignableFrom(subclass))
			return -1;
		int distance = 0;
		Class<?> current = subclass;
		while (current != null && !current.equals(superclass)) {
			distance++;
			current = current.getSuperclass();
		}
		return distance;
	}

	/**
	 * Sleeps for some milliseconds. Return true if the sleep was not
	 * interrupted, false otherwise.
	 *
	 * Any {@link InterruptedException}s are caught, but the interrupt bit is
	 * re-set in that case so {@link Thread#isInterrupted()} returns true again.
	 * The caller should abort any work and bring the thread to a halt if this
	 * method returns false.
	 *
	 * <pre>
	 * while (Utils.sleepInterruptable(1000))
	 * 	  do work;
	 * </pre>
	 *
	 */
	public static boolean sleepInterruptable(long milliseconds) {
		if (milliseconds <= 0)
			return Thread.currentThread().isInterrupted();
		try {
			Thread.sleep(milliseconds);
			return true;
		} catch (final InterruptedException e) {
			Thread.currentThread().interrupt();
			return false;
		}
	}

	public static String toString(InputStream stream) throws IOException {
		final ByteArrayOutputStream result = new ByteArrayOutputStream();
		final byte[] buffer = new byte[1024];
		int length;
		while ((length = stream.read(buffer)) != -1) {
			result.write(buffer, 0, length);
		}
		return result.toString(StandardCharsets.UTF_8.name());
	}

	/**
	 * Turn any object into a form that is save to print or log as ASCII text, but
	 * does not loose any information. Null, {@link Boolean} and {@link Number} are
	 * returned unquoted. Any other object is returned as a double-quoted string
	 * with any non-printable, special or non-ASCII characters escaped via backslash
	 * escape sequences.
	 */
	public static String repr(Object obj) {
		if (obj == null)
			return "null";
		if (obj instanceof Boolean || obj instanceof Number)
			return obj.toString();
		final String text = obj.toString();
		final StringBuilder sb = new StringBuilder(text.length() + 2);
		sb.append('"');
		for (int i = 0; i < text.length(); i++) {
			final char ch = text.charAt(i);
			switch (ch) {

			case '\0':
				sb.append("\\0");
				break;
			case '\t':
				sb.append("\\t");
				break;
			case '\n':
				sb.append("\\n");
				break;
			case '\r':
				sb.append("\\r");
				break;
			case '"':
				sb.append("\\\"");
				break;
			case '\\':
				sb.append("\\\\");
				break;

			default:
				if (' ' <= ch && ch <= '\u007e')
					sb.append(ch);
				else {
					sb.append("\\u");
					for (int j = 0; j < 4; j++)
						sb.append(hexArray[((ch << (j * 4)) & 0xFF)]);
				}
			}
		}
		sb.append('"');
		return sb.toString();
	}

	public static void wrapError(Failable cb) {
		try {
			cb.run();
		} catch (final RuntimeException e) {
			throw e;
		} catch (final Exception e) {
			throw new RuntimeException("Wrapped exception", e);
		}
	}

	public static <T, E extends Exception> T wrapError(FailableSupplier<T, E> cb) {
		try {
			return cb.supply();
		} catch (final RuntimeException e) {
			throw e;
		} catch (final Exception e) {
			throw new RuntimeException("Wrapped exception", e);
		}
	}

	public static <T> void forEach(T[] array, Consumer<T> callback) {
		for (final T t : array)
			callback.accept(t);
	}

	/**
	 * Replace one char with another in a string.
	 */
	public static String charReplace(char search, char replace, String source) {
		final char[] chars = source.toCharArray();
		for (int i = 0; i < chars.length; i++) {
			if (chars[i] == search)
				chars[i] = replace;
		}
		return new String(chars);
	}

	/**
	 * Return a string similar to {@link Object#toString()} without actually
	 * calling {@link Object#toString()} on the passed instance.
	 */
	public static String instanceId(Object s) {
		if (s == null)
			return "null";
		return s.getClass().getName() + "@" + Integer.toHexString(System.identityHashCode(s));
	}

	/**
	 * Return the first item of an iterator that matches a predicate, or null.
	 *
	 * This is often faster than a using the streaming API and easier to read
	 * than a loop.
	 */
	public static <T> T first(Iterable<T> items, Predicate<T> test) {
		if (items == null)
			return null;
		for (final T item : items)
			if (test.test(item))
				return item;
		return null;
	}

	/**
	 * Return the first item of an iterator, or null.
	 */
	public static <T> T first(Iterable<T> items) {
		if (items == null)
			return null;
		for (final T item : items)
			return item;
		return null;
	}

	public static int readNBytes(InputStream input, byte[] buffer) throws IOException {
		return readNBytes(input, buffer, 0, buffer.length);
	}

	/**
	 * {@link InputStream#read(byte[], int, int)} may read less bytes than
	 * requested. This method repeatedly reads from the {@link InputStream} until
	 * either the requested bytes were collected, or the end of stream is reached.
	 */
	public static int readNBytes(InputStream input, byte[] buffer, int off, int len) throws IOException {
		int read = 0;
		int total = 0;
		while (len > 0 && (read = input.read(buffer, off, len)) > 0) {
			total += read;
			off += read;
			len -= read;
		}
		return total > 0 ? total : read;
	}

	/**
	 * Return the same exception if it is a subclass of {@link RuntimeException}, or
	 * wrap it in a {@link RuntimeException}.
	 */
	public static RuntimeException toUnchecked(Throwable e) {
		if (e instanceof RuntimeException)
			return (RuntimeException) e;
		return new RuntimeException(e);
	}


}
