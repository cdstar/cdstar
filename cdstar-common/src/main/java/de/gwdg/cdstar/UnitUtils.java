package de.gwdg.cdstar;

public class UnitUtils {
	public static String[] factor = { "", "k", "m", "g", "t", "p" };

	public static String dataRateToString(long bytes, long milliseconds) {
		double bps = ((double) bytes * 1000) / milliseconds;
		int f = 0;
		while (bps > 1024 && f < factor.length - 1) {
			bps /= 1024;
			f++;
		}
		return Double.toString(bps) + factor[f] + "/s";
	}

}
