package de.gwdg.cdstar;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * Mark interfaces that are not considered stable.
 */
@Documented
@Retention(RUNTIME)
@Target(METHOD)
public @interface Beta {
}
