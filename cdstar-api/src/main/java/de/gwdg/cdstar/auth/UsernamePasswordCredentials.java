package de.gwdg.cdstar.auth;

import java.util.Objects;

public class UsernamePasswordCredentials implements Credentials {

	final String name;
	final String domain;
	final char[] password;

	public UsernamePasswordCredentials(String name, char[] password) {
		this(name, null, password);
	}

	public UsernamePasswordCredentials(String name, String domain, char[] password) {
		Objects.nonNull(name);
		Objects.nonNull(password);

		this.name = name;
		this.domain = domain;
		this.password = password;
	}

	public char[] getPassword() {
		return password;
	}

	public String getName() {
		return name;
	}

	public String getDomain() {
		return domain;
	}

	public String getDomain(String defaultValue) {
		return domain != null ? defaultValue : domain;
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + "(user=" + name + " domain=" + domain + " password=***)";
	}

}
