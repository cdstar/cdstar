package de.gwdg.cdstar.auth;

import java.util.Collection;
import java.util.Collections;

import de.gwdg.cdstar.auth.realm.Authorizer;

/**
 * A limited view on a Principal that can be passed to templates or other
 * unprivileged code.
 */
public interface BasePrincipal {

	/**
	 * The identifying name of the principal, local to the domain.
	 *
	 * This string should be as simple as possible. It MUST start with an
	 * alphanumeric character and MUST NOT contain the characters '@', '#' or
	 * backspace.
	 *
	 * Example: mmuster
	 */
	String getId();

	/**
	 * The authorizing domain of this principal. Defaults to
	 * {@link Authorizer#getName()} but may be overridden.
	 *
	 * This string should be as simple as possible. It MUST start with an
	 * alphanumeric character and MUST NOT contain the characters '@', '#' or
	 * backspace.
	 *
	 * Example: domain.com, local, ldap
	 */
	String getDomain();

	/**
	 * A globally unique identifier for this identity. Defaults to
	 * {@link #getId()} + "@" + {@link #getDomain()}.
	 *
	 * Example: mmuster@domain.com, mmuster@local
	 */
	default String getFullId() {
		return getId() + "@" + getDomain();
	}

	/**
	 * Return the value of a named property, or null if the property is not set.
	 */
	default String getProperty(String propertyName) {
		return null;
	}

	/**
	 * Return the names of non-null properties.
	 */
	default Collection<String> getPropertyNames() {
		return Collections.emptyList();
	}
}