package de.gwdg.cdstar.auth;

public interface Principal extends BasePrincipal {

	/**
	 * Set a property to a new value, returning the old value or 'null' if the
	 * property was not defined. Optional operation.
	 */
	default String setProperty(String propertyName, String value) {
		throw new UnsupportedOperationException();
	}

}
