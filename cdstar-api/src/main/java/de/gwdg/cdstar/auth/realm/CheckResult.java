package de.gwdg.cdstar.auth.realm;

import java.util.function.Function;

public enum CheckResult {
	YES, NO, UNKNOWN;

	public static CheckResult voteReduce(CheckResult cur, CheckResult next) {
		if (cur == null || cur == CheckResult.UNKNOWN)
			return next;
		if (cur == CheckResult.NO || next == CheckResult.NO)
			return CheckResult.NO;
		return CheckResult.YES;
	}

	public static <T> boolean vote(Iterable<T> voters, Function<T, CheckResult> castVote) {
		CheckResult result = CheckResult.UNKNOWN;
		for(T voter: voters) {
			result = voteReduce(result, castVote.apply(voter));
			if(result == NO)
				return false;
		}

		return result == YES;
	}

}