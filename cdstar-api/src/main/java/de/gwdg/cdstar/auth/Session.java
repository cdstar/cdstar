package de.gwdg.cdstar.auth;

import java.io.Serializable;

import de.gwdg.cdstar.auth.realm.Authenticator;
import de.gwdg.cdstar.auth.realm.SessionStore;

/**
 * Object returned by {@link Authenticator} instances to answer authentication
 * requests.
 */
public interface Session {

	/**
	 * Return true if this identity was recovered from a less secure token or
	 * session key and the actual user credentials were NOT checked.
	 */
	boolean isRemembered();

	/**
	 * Return the {@link Authenticator} that authenticated this session.
	 */
	Authenticator getAuthenticator();

	/**
	 * Return the authenticated principal. The instance should be
	 * {@link Serializable} to support disk-based {@link SessionStore}s.
	 */
	Principal getPrincipal();

}
