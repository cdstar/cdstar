package de.gwdg.cdstar.auth;

import de.gwdg.cdstar.auth.error.LoginFailed;
import de.gwdg.cdstar.auth.realm.Authenticator;
import de.gwdg.cdstar.auth.realm.Authorizer;

/**
 * A {@link Subject} is a stateful handle for an anonymous or authenticated
 * {@link Principal} and provides APIs to authenticate itself.
 */
public interface Subject {

	/**
	 * Return true if the subject does not represent any known identity.
	 */
	default boolean isAnonymous() {
		return !hasPrincipal();
	}

	/**
	 * Return true if this subject is associated with an identity. It may be
	 * autenticated or remembered.
	 */
	boolean hasPrincipal();

	/**
	 * Return true if this subject is authenticated (not remembered).
	 */
	default boolean isAuthenticated() {
		return hasPrincipal() && !isRemembered();
	}

	/**
	 * Return true if this subject was remembered by an {@link Authenticator}, but
	 * not logged in using any form of credentials.
	 *
	 * This may be used as an indicator of lowered security to an application. For
	 * example, a web service may allow users to come back within a certain time
	 * frame and be logged in automatically (e.g. via cookies), but require an
	 * explicit login for highly sensitive operations such as changing the password.
	 */
	boolean isRemembered();

	/**
	 * Return the token associated with this subject, if remembered, or create a new
	 * token. Requires a SessionStore realm.
	 */
	String remember();

	/**
	 * Try to authenticate this subject with the given credentials. Return true on
	 * success, false otherwise. Calling this on an already authenticated subject
	 * triggers a {@link #logout()}.
	 */
	boolean tryLogin(Credentials request);

	default void login(Credentials request) throws LoginFailed {
		if (!tryLogin(request))
			throw new LoginFailed();
	}

	/**
	 * Turn the subject into an anonymous subject and tell the {@link Authenticator}
	 * to forget about it.
	 */
	void logout();

	/**
	 * The identifying name of the subject, or null for anonymous subjects.
	 */
	Principal getPrincipal();

	/**
	 * Return true if the subject is member of the given group (assuming the same
	 * domain than the principal).
	 */
	default boolean isMemberOf(String group) {
		return hasPrincipal() && isMemberOf(group, null);
	}

	/**
	 * Return true if the subject is member of the given group. If the group domain
	 * is null, the same domain as the principal is assumed.
	 */
	boolean isMemberOf(String group, String groupDomain);

	/**
	 * Return the context that created this subject.
	 */
	AuthConfig getConfig();

	/**
	 * Return True if any of the installed {@link Authorizer}s approves this
	 * permission.
	 */
	boolean isPermitted(Permission p);

}
