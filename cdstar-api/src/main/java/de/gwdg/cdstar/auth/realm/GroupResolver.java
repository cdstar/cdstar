package de.gwdg.cdstar.auth.realm;

import de.gwdg.cdstar.auth.Principal;
import de.gwdg.cdstar.auth.Session;

/**
 * A {@link GroupResolver} is able to decide if a given {@link Principal} is
 * member of a specific group.
 */
public interface GroupResolver extends Realm {

	/**
	 * Check if a given {@link Principal} is member of a specific group.
	 *
	 * This method takes a {@link Session} instead of a {@link Principal} because
	 * the same principal may have different group memberships based on the
	 * authentication method. For example, a token based authentication may restrict
	 * the groups a principal is allowed to identify with (limited access token).
	 *
	 * The {@link GroupResolver} must return {@link CheckResult#YES} if the
	 * principal is member of the given group, {@link CheckResult#NO} if it is not
	 * member of the given group or {@link CheckResult#UNKNOWN} if the membership
	 * status is not known. A principal is considered member of a group if at least
	 * one {@link GroupResolver} votes {@link CheckResult#YES} and no
	 * {@link GroupResolver} votes {@link CheckResult#NO};
	 *
	 * @param session     The session (or principal) under test.
	 * @param groupName   Name of the group (without leading '@')
	 * @param groupDomain Domain of the group.
	 *
	 * @return Group membership status.
	 */
	CheckResult isMemberOf(Session session, String groupName, String groupDomain);

	/**
	 * Same as {@link #isMemberOf(Session, String, String)} but passes the
	 * principals domain (as returned by {@link Principal#getDomain()} as the group
	 * domain.
	 */
	default CheckResult isMemberOf(Session session, String groupName) {
		return isMemberOf(session, groupName, session.getPrincipal().getDomain());
	}

}
