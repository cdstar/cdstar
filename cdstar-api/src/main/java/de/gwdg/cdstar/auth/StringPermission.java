package de.gwdg.cdstar.auth;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.StringJoiner;

/**
 * Hierarchical permission based on a list of string elements or wildcards.
 *
 * A {@link StringPermission} can imply (grant) another permission if every part
 * either matches that of the other permission, or is a wildcard.
 *
 * Examples:
 * <ul>
 * <li><code>print</code> implies <code>print:HP1234</code> and
 * <code>print:HP1234:color</code> (prefix match)</li>
 * <li><code>print:*:color</code> implies <code>print:HP1234:color</code> and
 * <code>print:Brother1337:color</code>, but not <code>print:HP1234:mono</code>,
 * <code>print</code> or <code>print:color</code> (wildcard match)</li>
 * <li><code>print:*:color</code> equals <code>print::color</code> (empty parts
 * are wildcards)</li>
 * <li><code>print:*:</code> equals <code>print</code> (tailing wildcards are
 * ignored)</li>
 * </ul>
 *
 *
 */
public class StringPermission implements Permission {

	private static final char ESCAPE = '\\';
	private static final char SEP = ':';
	private static final char WILDCARD = '*';

	private final String[] parts;

	/**
	 * Create a new string permission out of an explicit list of raw string
	 * parts. Empty strings or NULL values are interpreted as wildcards.
	 *
	 * The input array is not modified. Internally, wildcards are represented by
	 * null and leading wildcards are removed.
	 */
	private StringPermission(String... parts) {
		String[] copy = Arrays.copyOf(parts, parts.length);

		// Replace empty strings with null
		int lastNonNull = 0;
		for (int i = 0; i < copy.length; i++) {
			if (copy[i] != null) {
				if (copy[i].isEmpty())
					copy[i] = null;
				else
					lastNonNull = i;
			}
		}

		// Remove leading nulls, if any
		if (lastNonNull + 1 < copy.length)
			copy = Arrays.copyOf(copy, lastNonNull + 1);
		this.parts = copy;
	}

	/**
	 * Create a new string permission out of an explicit list of raw string
	 * parts. Empty strings or NULL values are interpreted as wildcards.
	 */
	public static StringPermission ofParts(String... parts) {
		return new StringPermission(parts);
	}

	/**
	 * Parse a permission string previously created with {@link #toString()}.
	 *
	 * Parts are separated with ':'. Empty parts or the wildcard symbol '*' are
	 * recognized as wildcards. Special characters ('*', ':' and backslash)
	 * within parts can be escaped with a backslash.
	 */
	public static StringPermission parse(String permission) {
		final List<String> result = new ArrayList<>(8);

		char c = 0;
		boolean escaped = false;
		final StringBuilder part = new StringBuilder();
		for (int i = 0; i < permission.length(); i++) {
			c = permission.charAt(i);
			if (escaped) {
				part.append(c);
				escaped = false;
			} else if (c == ESCAPE) {
				escaped = true;
			} else if (c == WILDCARD
					&& part.length() == 0
					&& (i + 1 == permission.length() || permission.charAt(i + 1) == SEP)) {
				// eat the wildcard if its the only character in a segment.
				// (:*: equals ::)
			} else if (c == SEP) {
				result.add(part.length() == 0 ? null : part.toString());
				part.setLength(0);
			} else {
				part.append(c);
			}
		}

		if (part.length() > 0)
			result.add(part.toString());

		return new StringPermission(result.toArray(new String[result.size()]));
	}

	/**
	 * Escape special characters ('*', ':' and backslash) with backslash.
	 */
	private static String escape(String part) {
		final StringBuilder sb = new StringBuilder(part.length());
		for (int i = 0; i < part.length(); i++) {
			final char c = part.charAt(i);
			if (c == SEP || c == WILDCARD || c == ESCAPE)
				sb.append('\\').append(c);
			else
				sb.append(c);
		}
		return sb.toString();
	}

	/**
	 * Return the string representation of this permission in the same format as
	 * {@link #parse(String)} can read.
	 */
	@Override
	public String toString() {
		final StringJoiner sj = new StringJoiner("" + SEP);
		for (final String part : parts)
			sj.add(part == null ? "" : escape(part));
		return sj.toString();
	}

	public List<String> getParts() {
		return Collections.unmodifiableList(Arrays.asList(parts));
	}

	public boolean hasWildcard() {
		for (final String part : parts)
			if (part == null)
				return true;
		return false;
	}

	public String getPart(int i) {
		return parts.length > i ? parts[i] : null;
	}

	public boolean isWildcard(int i) {
		return getPart(i) == null;
	}

	public int length() {
		return parts.length;
	}

	/**
	 * Return an new {@link StringPermission} with the given index replaced with
	 * a new string part or a wildcard. The index may be greater than the
	 * current number of parts, in which case all missing parts are added as
	 * wildcards.
	 *
	 * @param i
	 *            index of the part to replace.
	 * @param part
	 *            Either a non-empty string part, or null for a wildcard.
	 * @return a new, immutable {@link StringPermission} instance.
	 */
	public StringPermission replace(int i, String part) {
		final String[] copy = Arrays.copyOfRange(parts, 0, Math.max(i + 1, parts.length));
		copy[i] = part;
		return ofParts(copy);
	}

	/**
	 * Return true if the current permission implies a given permission request.
	 *
	 * Currently only {@link StringPermission}s can imply other
	 * {@link StringPermission}s.
	 */
	public boolean implies(Permission p) {
		if (!(p instanceof StringPermission))
			return false;

		final String[] other = ((StringPermission) p).parts;

		if (parts.length > other.length)
			return false;

		for (int i = 0; i < parts.length; i++) {
			if (!(parts[i] == null || parts[i].equals(other[i])))
				return false;
		}

		return true;
	}

	@Override
	public boolean equals(Object obj) {
		return obj instanceof StringPermission && Arrays.equals(parts, ((StringPermission) obj).parts);
	}

	@Override
	public int hashCode() {
		return Arrays.hashCode(parts);
	}

}
