package de.gwdg.cdstar.auth.realm;

import de.gwdg.cdstar.auth.Permission;
import de.gwdg.cdstar.auth.Principal;
import de.gwdg.cdstar.auth.Session;

/**
 * An {@link Authorizer} checks if a given {@link Principal} is allowed to
 * perform an action represented by a {@link Permission}.
 *
 */
public interface Authorizer extends Realm {
	/**
	 * Check if the identified (not necessary authenticated) subject is allowed
	 * to perform a specific action. The tested {@link Session} is null if
	 * anonymous access is requested.
	 *
	 * This method takes a {@link Session} instead of a {@link Principal}
	 * because the same principal may have different permissions based on the
	 * authentication method. For example, a token based authentication may
	 * restrict the session to certain permission scopes (e.g. read-only access
	 * tokens).
	 * 
	 * Return {@link CheckResult#YES} if the principal is allowed to perform this
	 * action, {@link CheckResult#NO} if the principal is blocked from performing
	 * this action, or {@link CheckResult#UNKNOWN} if the principal is not known or is
	 * not explicitly allowed or denied this action.
	 * 
	 * The end result depends on the return values of all installed
	 * {@link Authorizer}s: The action is granted only if at least one Authorizer
	 * returns {@link CheckResult#YES} and not a single {@link CheckResult#NO}
	 * is returned. In other words, {@link CheckResult#NO} will overrule
	 * {@link CheckResult#YES} and should only be used by {@link Authorizer}s
	 * that wish to restrict other more permissive {@link Authorizer}s.
	 * 
	 * @param p
	 *                The permission to check.
	 * @param session
	 *                The session asking for permission, or null if anonymous access
	 *                is requested.
	 * @return {@link CheckResult}
	 */
	CheckResult isPermitted(Session session, Permission p);
}
