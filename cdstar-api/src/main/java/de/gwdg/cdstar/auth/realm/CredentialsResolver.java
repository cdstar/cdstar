package de.gwdg.cdstar.auth.realm;

import de.gwdg.cdstar.auth.Credentials;

/**
 * A {@link CredentialsResolver} extracts {@link Credentials} out of arbitrary
 * contexts. For example, an imaginary ServletCredentialsResolver may extract
 * BASIC Authentication credentials (username and password) out of servlet
 * request context.
 */
public interface CredentialsResolver extends Realm {

	/**
	 * Return a subclass of {@link Credentials} if the given object contains a
	 * login or session continuation request. Return null if the given context
	 * is not understood or no credentials were found.
	 */
	Credentials getCredentials(Object context);

}
