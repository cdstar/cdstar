package de.gwdg.cdstar.auth;

public class TokenCredentials implements Credentials {
	private final String token;

	public TokenCredentials(String token) {
		this.token = token;
	}

	public String getToken() {
		return token;
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + "(***)";
	}

}
