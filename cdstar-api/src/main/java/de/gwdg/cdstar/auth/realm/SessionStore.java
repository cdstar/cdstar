package de.gwdg.cdstar.auth.realm;

import de.gwdg.cdstar.auth.Session;
import de.gwdg.cdstar.auth.TokenCredentials;

/**
 * A {@link SessionStore} is able to remember the principal of a session and
 * return a unique session identifier, which can then be used to continue a
 * previously remembered session.
 *
 * Implementations should also implement {@link Authenticator} and listen for
 * {@link TokenCredentials} so persisted sessions can be continued, as well as
 * {@link Authenticator#logout(Session)} and remove persisted sessions.
 *
 */
public interface SessionStore extends Authenticator {

	/**
	 * Persist the principal of the given session. Calling this on a session
	 * that is already persisted should update the persisted state and
	 * return the same string.
	 */
	String remember(Session session);


}
