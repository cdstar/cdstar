package de.gwdg.cdstar.auth;

import de.gwdg.cdstar.auth.realm.Authenticator;

/**
 * Credentials used to start a session with an already authenticated principal.
 * Accepting {@link Authenticator}s should check if they are responsible for the
 * given principal, and if yes, return a new (remembered) session with this
 * principal.
 */
public class KnownPrincipalCredentials implements Credentials {
	private final Principal principal;

	public KnownPrincipalCredentials(Principal principal) {
		this.principal = principal;
	}

	public Principal getPrincipal() {
		return principal;
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + "(" + principal + ")";
	}

}
