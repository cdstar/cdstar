package de.gwdg.cdstar.auth.realm;

import java.util.List;

import de.gwdg.cdstar.auth.Permission;
import de.gwdg.cdstar.auth.StringPermission;

/**
 * A {@link PermissionResolver} translates domain specific {@link Permission}
 * types into zero or more permissions of a more general type, for example
 * 	{@link StringPermission}.
 *
 * This allows the use of custom {@link Permission} subclasses and bridge them
 * to other general purpose {@link Authorizer} implementations that usually only
 * support {@link StringPermission}.
 *
 * The returned permissions are not resolved again to prevent loops.
 */
public interface PermissionResolver extends Realm {

	/**
	 * Translate a domain specific permission type into zero or more permissions
	 * of a more general type, for example {{@link StringPermission}}.
	 */
	List<Permission> resolve(Permission p);

}
