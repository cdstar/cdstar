package de.gwdg.cdstar.auth.realm;

import de.gwdg.cdstar.auth.Credentials;
import de.gwdg.cdstar.auth.Session;

/**
 * An authenticator verifies an active (credentials) or passive (session
 * continuation) login request and returns a {@link Session}.
 */
public interface Authenticator extends Realm {

	/**
	 * Try to login or remember the subject for supported request types.
	 *
	 * @return {@link Session} on success, null otherwise.
	 */
	Session login(Credentials request);

	/**
	 * Explicitly logout an authenticated session, if supported by the
	 * implementation.
	 */
	void logout(Session who);
}
