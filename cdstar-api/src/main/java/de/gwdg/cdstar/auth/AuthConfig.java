package de.gwdg.cdstar.auth;

import java.util.List;

import de.gwdg.cdstar.auth.error.LoginFailed;
import de.gwdg.cdstar.auth.realm.CredentialsResolver;
import de.gwdg.cdstar.auth.realm.Realm;

public interface AuthConfig {

	void addRealm(Realm realm);

	boolean removeRealm(Realm realm);

	List<Realm> listRealms();

	<T extends Realm> List<T> getRealmsByClass(Class<T> klass);

	/**
	 * Return a new unbound subject.
	 */
	Subject getSubject();

	/**
	 * Extract login credentials from a third-party context (e.g. HTTPSession)
	 * with the help of {@link CredentialsResolver}s. If the context contains
	 * login information, a login is tried immediately.
	 *
	 * @throws LoginFailed
	 *             if the context contained a login request, but login failed.
	 */
	Subject getSubject(Object context) throws LoginFailed;


}
