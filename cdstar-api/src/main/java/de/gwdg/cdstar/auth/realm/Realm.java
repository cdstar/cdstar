package de.gwdg.cdstar.auth.realm;

import de.gwdg.cdstar.auth.AuthConfig;

/**
 * Base interface for all the different types of auth framework extensions.
 */
public interface Realm {

	/**
	 * Used for logging and some toString() implementations.
	 */
	String getName();

	/**
	 * Called by the {@link AuthConfig} implementation after this realm was added.
	 */
	default void setConfig(AuthConfig config) {
		// NOP
	}

}
