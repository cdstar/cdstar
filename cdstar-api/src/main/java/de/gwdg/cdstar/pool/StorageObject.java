package de.gwdg.cdstar.pool;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import de.gwdg.cdstar.pool.PoolError.NameConflict;
import de.gwdg.cdstar.pool.PoolError.StaleHandle;

/**
 * Transaction bound (isolated) view on a specific storage object.
 *
 * Stale handles: Storage objects are only fully accessible while the associated
 * transaction is still alive. After the transaction is closed, whether
 * successfully or due to a rollback, the storage object automatically enters a
 * stale (read-only) state with reduced capabilities. Methods that are required
 * to work on stale objects are marked in their documentation. All other
 * operations should not be used and may raise a {@link PoolError.StaleHandle}
 * exception.
 */

public interface StorageObject extends MetaStore {

	/**
	 * Return the backend-specific unique identifier for this object.
	 *
	 * For better interoperability, the returned string should only contain
	 * printable ASCII characters and avoid special characters that are
	 * problematic in filenames. Backends that use binary or numeric identifiers
	 * should encode them with a suitable codec, e.g. decimal, hex or base64.
	 *
	 */

	String getId();

	/**
	 * Return the original revision of this object, or 'null' for new objects.
	 */

	String getRevision();

	/**
	 * Return the original parent revision of this object, or 'null' for objects
	 * with no parent.
	 *
	 * The returned revision is not guaranteed to exist, as the history may have
	 * been removed via {@link StoragePool#trimObject(String)}.
	 */

	String getParentRevision();

	/**
	 * Return the next revision this object will have after a successful commit.
	 *
	 * Note that if {@link #isModified()} returns false on commit, the revision
	 * is not changed and the string returned by {@link #getNextRevision()} is
	 * not used as a revision.
	 *
	 */
	String getNextRevision();

	/**
	 * Return true if this object was created or modified during the current
	 * transaction and needs to be stored back to the persistence layer on
	 * commit.
	 */
	boolean isModified();

	/**
	 * Update lastModified to the current time. This is a modifying operation.
	 *
	 * @throws PoolError.StaleHandle
	 *             if accessed after transaction is closed.
	 */
	void touch() throws StaleHandle;

	/**
	 * Return the declared type of this archive, or null of no type was
	 * declared.
	 */

	String getType();

	/**
	 * Return true if the type value is not null.
	 */

	default boolean hasType() {
		return getType() != null;
	}

	/**
	 * Change the declared type of this object. This is a modifying operation.
	 *
	 * The type value is application specific and not interpreted by the storage
	 * backend. It can be used to distinguish between different object types
	 * stored in the same backend. It is recommended to follow Content-Type
	 * header syntax (e.g. "type/subtype; option=value").
	 *
	 * @throws PoolError.StaleHandle
	 *             if accessed after transaction is closed.
	 */
	void setType(String type) throws StaleHandle;

	/**
	 * Return the time this object was created.
	 */

	Date getCreated();

	/**
	 * Return the time this object, its properties, or any of its resources
	 * content or properties was last modified.
	 *
	 * If the object was modified during the current transaction, the new
	 * modification time is returned.
	 *
	 */

	Date getLastModified();

	/**
	 * Return a list of all resources currently associated with this object.
	 * Changes to the list (deletions/additions) do not change the state of the
	 * object, but the contained resource instances themselves are modifiable.
	 * The list also contains temporary resources or resources that were removed
	 * earlier. These can be identified by their missing name attribute.
	 */
	List<Resource> getResources();

	/**
	 * Return the number of resources in this object. This may be faster than
	 * actually loading and counting resource handles via
	 * {@link #getResources()}.
	 */
	default int getResourceCount() {
		return getResources().size();
	}

	/**
	 * Return true of a resource with this exact name exists. Resource names are
	 * case-sensitive.
	 */
	default boolean hasResource(String name) {
		return getResource(name) != null;
	}

	/**
	 * Return the resource with this exact name, or null if no such resource
	 * exist. Resource names are case-sensitive.
	 */
	default Resource getResource(String name) {
		for (final Resource resource : getResources()) {
			if (name.equals(resource.getName()))
				return resource;
		}
		return null;
	}

	/**
	 * Return the resource with this id, or null if no such resource exist.
	 */
	default Resource getResourceByID(String id) {
		for (final Resource resource : getResources()) {
			if (resource.getId().equals(id))
				return resource;
		}
		return null;
	}

	/**
	 * Return a list of all resource with a name that starts with the specified
	 * prefix. The list may be empty. Resource names are case-sensitive.
	 */

	default List<Resource> getResourcesByPrefix(String prefix) {
		final List<Resource> result = new ArrayList<>();
		getResources().forEach(r -> {
			final String n = r.getName();
			if (n != null && n.startsWith(prefix))
				result.add(r);
		});
		return result;
	}

	/**
	 * Create a new (empty) resource and attach it to the archive. This is a
	 * modifying operation.
	 *
	 * The name can be null, in which case the resource is temporary and thrown
	 * away at the end of the transaction. If the name is not null, it must be
	 * unique for a given archive.
	 *
	 * @param name
	 *            Resource name.
	 * @return Empty fragment with type and name set.
	 * @throws NameConflict
	 *             if a resource with that name already exists.
	 * @throws PoolError.StaleHandle
	 *             if accessed after transaction is closed.
	 */
	Resource createResource(String name) throws NameConflict, StaleHandle;

	/**
	 * Mark this object for deletion. After commit, the object may be
	 * garbage-collected at any time.
	 *
	 * @throws PoolError.StaleHandle if accessed after transaction is closed.
	 */
	void remove() throws StaleHandle;

	/**
	 * Return true if this object is marked for deletion.
	 */
	boolean isRemoved();

}