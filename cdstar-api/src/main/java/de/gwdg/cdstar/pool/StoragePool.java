package de.gwdg.cdstar.pool;

import java.io.Closeable;
import java.util.Date;
import java.util.Set;

import de.gwdg.cdstar.pool.BackendError.DamagedDataError;
import de.gwdg.cdstar.pool.PoolError.ExternalResourceException;
import de.gwdg.cdstar.pool.PoolError.NotFound;
import de.gwdg.cdstar.ta.UserTransaction;

/**
 * Interface for storage pool implementations.
 *
 * All operations must be thread-save.
 *
 * Implementations MUST provide a single-parameter constructor that accepts a
 * Config instance. This config will always contain at lease three fields: class
 * (the class name of this pool), name (the vault name), and path (the data path
 * for this pool). pools that do not store data locally may still whish to store
 * cache or report data there.
 */

public interface StoragePool extends Closeable {

	/**
	 * Create a new, or return an existing {@link StorageSession} bound to the given
	 * transaction.
	 */
	StorageSession open(UserTransaction tx);

	/**
	 * Calculate the (estimated) on-disk size of a given storage object including
	 * resources, temporary files and revisions. The value can be used for
	 * quota-management and metrics, but is not required to be exact.
	 *
	 * @return Estimated size of an storage object in bytes.
	 */
	long objectSize(String objectId) throws NotFound;

	/**
	 * Check if an object exists. This is a weak check and may return true for
	 * storage objects that were recently removed. This is only useful if the check
	 * is significantly faster than actually loading a storage object.
	 */
	boolean objectExists(String objectId);

	/**
	 * Iterate over the IDs of all storage objects stored in this storage pool.
	 * Storage objects created or removed during the lifetime of the iterator may or
	 * may not be included in the result. The object IDs are returned in undefined,
	 * but stable order.
	 */
	default Iterable<String> getObjectIDs() {
		return getObjectIDs("");
	}

	/**
	 * Same as {@link #getObjectIDs()}, but skip all IDs that are ordered smaller or
	 * equal to the given string.
	 *
	 * This can be used to paginate through large vaults and should be optimized by
	 * the back-end.
	 */
	Iterable<String> getObjectIDs(String skipUntil);

	/**
	 * Load a specific revision (or HEAD if revision is null) directly from the
	 * backing storage (bypassing any caches) and return an unbound
	 * {@link StorageObject} handle. Since the handle does not have an associated
	 * transaction, it will be stale and cannot be modified.
	 */
	StorageObject loadObjectDirect(String objectId, String revision) throws NotFound;

	/**
	 * Run migration steps and integrity checks on a specific storage object. This
	 * includes reading the entire content of all attached resources and comparing
	 * the computed hashes.
	 */
	void validateObject(String objectId) throws NotFound, ExternalResourceException, DamagedDataError;

	/**
	 * Run garbage collection and other clean-up routines on a specific storage
	 * object. This also removes the revision history, but keeps revisions marked as
	 * persistent.
	 *
	 * All resources reachable form a running {@link StorageSession} must remain
	 * readable. Thus, the newest revision still older than the oldest running
	 * {@link StorageSession}, and all revisions newer than that, cannot be trimmed.
	 *
	 * @param objectId
	 * @return Storage space (in bytes, estimate) freed by this garbage collection
	 *         run.
	 */
	long trimObject(String objectId) throws NotFound;

	/**
	 * Return the last modified date for an object on a best-effort basis. The
	 * returned date might be more recent than the actual last-modified attribute of
	 * the object, for example after migrating objects to a new storage.
	 *
	 * This function should be significantly faster than actually loading the object
	 * and calling {@link StorageObject#getLastModified()}.
	 */
	Date lastModified(String objectId);

	/**
	 * Return the names of digest hashing algorithms calculated for new or modified
	 * {@link Resource} content. Existing resources may have different hashes
	 * already available, so the set of algorithms does not necessarily match that
	 * of {@link Resource#getDigestNames()}.
	 */
	Set<String> getDefaultDigests();

}
