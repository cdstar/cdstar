package de.gwdg.cdstar.pool;

public enum CacheState {
	/**
	 * The object and all its resources are fully cached and immediately
	 * accessible.
	 */
	FULL,
	/**
	 * Object metadata is available, but some or all of the resource binary
	 * streams may be absent from the cache.
	 */
	PARTIAL,
	/**
	 * The object is not available and cannot be loaded.
	 */
	MISSING
}
