package de.gwdg.cdstar.pool;

/**
 * This group of exceptions represents usage errors by the client. They should
 * be forwarded to the client and not handled or obfuscated by intermediate
 * layers.
 *
 */
@SuppressWarnings("serial")
public class PoolError extends RuntimeException {
	public PoolError() {
		super();
	}

	public PoolError(String msg) {
		super(msg);
	}

	public PoolError(Exception e) {
		super(e);
	}

	public PoolError(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * The object ID does not match the pools criteria, or the ID of a new object is
	 * already used.
	 */
	public static class InvalidObjectId extends PoolError {
		public InvalidObjectId(String msg) {
			super(msg);
		}
	}

	/**
	 * Resource names must be unique per object.
	 */
	public static class NameConflict extends PoolError {
		public NameConflict(String msg) {
			super(msg);
		}
	}

	/**
	 * Operation not possible because it conflicts with a previous transaction.
	 * This may happen on any modifying operations on a storage object or its
	 * resources.
	 */
	public static class Conflict extends PoolError {
		public Conflict(String msg) {
			super(msg);
		}

		public Conflict(String msg, Throwable cause) {
			super(msg, cause);
		}
	}

	/**
	 * The archive or resource not found.
	 */
	public static class NotFound extends PoolError {
		public NotFound(String id, Exception e) {
			super(id, e);
		}

		public NotFound(String id) {
			super(id);
		}
	}

	/**
	 * The archive or resource is not available.
	 */
	public static class ExternalResourceException extends PoolError {
		public ExternalResourceException(Exception e) {
			super(e);
		}

		public ExternalResourceException() {
			super();
		}
	}

	/**
	 * The transaction associated with this storage object was closed and the
	 * requested operation is no longer possible.
	 */
	public static class StaleHandle extends PoolError {
		public StaleHandle() {
			super();
		}

		public StaleHandle(String msg) {
			super(msg);
		}
	}

	/**
	 * Raised when accessing or committing a resource while here is an unclosed
	 * write channel.
	 */
	public static class WriteChannelNotClosed extends PoolError {
		public WriteChannelNotClosed() {
			super();
		}

		public WriteChannelNotClosed(String msg) {
			super(msg);
		}
	}
}
