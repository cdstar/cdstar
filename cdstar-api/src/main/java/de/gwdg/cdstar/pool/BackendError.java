package de.gwdg.cdstar.pool;

/**
 * This group of exceptions represents errors within the storage backend that
 * are not resolvable by a client.
 *
 */
@SuppressWarnings("serial")
public class BackendError extends RuntimeException {

	public BackendError(Throwable e) {
		super(e);
	}

	public BackendError(String msg, Throwable e) {
		super(msg, e);
	}

	public BackendError(String msg) {
		super(msg);
	}

	/**
	 * Signals a temporary error condition that does not require manual
	 * intervention, but pending requests should be delayed and retried later.
	 *
	 */
	public static class TemporaryError extends BackendError {
		public TemporaryError(Throwable e) {
			super(e);
		}
	}

	/**
	 * Singals an inconsistency in persisted data structures or binary files.
	 * This is thrown whenever the backend detects damaged data files (checksums
	 * don't match) or fails to deserialize or migrate an internal data
	 * structure.
	 */
	public static class DamagedDataError extends BackendError {
		public DamagedDataError(Throwable e) {
			super(e);
		}

		public DamagedDataError(String msg, Throwable e) {
			super(msg, e);
		}

		public DamagedDataError(String msg) {
			super(msg);
		}

	}

}
