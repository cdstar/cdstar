package de.gwdg.cdstar.pool;

import java.util.Set;

/**
 * Both {@link StorageObject} and {@link Resource} can be annotated with user
 * defined properties. This interface defines a common API for property access.
 */
public interface MetaStore {

	/**
	 * Return the names of defined properties.
	 */
	Set<String> getPropertyNames();

	/**
	 * Return a property value, or null.
	 */
	String getProperty(String name);

	/**
	 * Return a property value, or a default value if the property is undefined.
	 */
	default String getProperty(String name, String defaultValue) {
		final String value = getProperty(name);
		return value != null ? value : defaultValue;
	}

	/**
	 * Change the values of a property. If value is null the property is
	 * removed.
	 */
	void setProperty(String name, String values);
}
