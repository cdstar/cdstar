package de.gwdg.cdstar.pool;

import de.gwdg.cdstar.pool.PoolError.InvalidObjectId;
import de.gwdg.cdstar.pool.PoolError.NotFound;
import de.gwdg.cdstar.pool.PoolError.StaleHandle;
import de.gwdg.cdstar.ta.UserTransaction;

/**
 * A transaction-bound handle to access pool resources.
 *
 * All operations are thread-safe and snapshot-isolated from other sessions. In
 * other words: A session represents a point-in-time snapshot of the entire
 * pool, and can only see state that was committed before the session was
 * created.
 *
 * Uncommitted changes MUST be private to the current session and MUST NOT be
 * visible to other sessions. Committed changes MUST NOT be visible to sessions
 * created prior to the commit.
 *
 */

public interface StorageSession {

	/**
	 * Return the storage pool this sessionw as created from.
	 */
	StoragePool getPool();

	/**
	 * Allocate a new storage object.
	 *
	 * @return Handle to a new storage object.
	 * @throws PoolError.StaleHandle Session closed or in read-only mode.
	 */

	StorageObject createObject() throws StaleHandle;

	/**
	 * Allocate a new storage object with a predefined ID.
	 *
	 * The ID must not already exist, must be compatible with the pool-specific ID
	 * format, and should be mostly random. Bad randomness of IDs, especially the
	 * first few bytes, may result in unbalanced sharding and may cause performance
	 * regressions.
	 *
	 * @param id Predefined object ID for the new storage object.
	 *
	 * @return Handle to a new storage object.
	 * @throws PoolError.StaleHandle Session closed
	 *
	 */

	StorageObject createObject(String id) throws StaleHandle, InvalidObjectId;

	/**
	 * Receive a handle for an existing storage object.
	 *
	 * If no explicit revision is requested (third parameter is null), then the
	 * 'head' revision is returned, representing the current state of the object.
	 *
	 * Alternatively, the caller can request a specific revision, as long as this
	 * revision is marked as 'persistent'.
	 *
	 * If the object is not found or marked as deleted, or the requested revision is
	 * not visible to the current session, a {@link NotFound} exception is raised.
	 *
	 * Calling this method multiple times with the same parameter during the same
	 * transaction MUST return the same instance.
	 *
	 * @param objectId storage object ID
	 * @param revision a specific revision number, or null
	 *
	 * @return Handle for an existing storage object.
	 *
	 * @throws NotFound if the specified revision is not visible to the current
	 *                  transaction or does not exist.
	 * @throws          PoolError.StaleHandle Session closed
	 */

	StorageObject readObject(String objectId, String revision) throws NotFound, StaleHandle;

	/**
	 * Same as {@link #readObject(UserTransaction, String, String)} with
	 * <code>revision=null</code>
	 */
	default StorageObject readObject(String objectId) throws NotFound, StaleHandle {
		return readObject(objectId, null);
	}

}
