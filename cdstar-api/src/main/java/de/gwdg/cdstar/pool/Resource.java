package de.gwdg.cdstar.pool;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;
import java.nio.file.Path;
import java.util.Date;
import java.util.Map;

import de.gwdg.cdstar.pool.PoolError.ExternalResourceException;
import de.gwdg.cdstar.pool.PoolError.NameConflict;
import de.gwdg.cdstar.pool.PoolError.StaleHandle;
import de.gwdg.cdstar.pool.PoolError.WriteChannelNotClosed;

/**
 * Represents a binary file resource within a {@link StorageObject}.
 * 
 * External resources are stubs for which the binary content is not
 * available. They cannot be read or written to, but their metadata remains
 * accessible. Resources can be marked as external by calling
 * {@link #setExternalLocation(String)}, which allows the pool implementation to
 * remove the binary content for this resource from storage, once the change is
 * committed. External resources are recovered by re-writing the exact same
 * byte-content they once contained via a {@link #getRestoreChannel()}.
 * 
 * At any given time there can only be one write-channel
 * ({@link #getWriteChannel(long)} or {@link #getRestoreChannel()}) for a
 * resource. Opening a write channel closes all existing channels, including
 * read-channels. Read-channels however can be opened multiple times, if
 * necessary. Committing a resource for which an open write channel exists is
 * considered an error and will fail.
 *
 */
public interface Resource extends MetaStore {

	int DEFAULT_BUFFER = 1024 * 64;

	String DIGEST_MD5 = "MD5";
	String DIGEST_SHA1 = "SHA-1";
	String DIGEST_SHA256 = "SHA-256";
	String[] DIGEST_NAMES = { DIGEST_MD5, DIGEST_SHA1, DIGEST_SHA256 };

	/**
	 * Return the containing storage object.
	 */
	StorageObject getObject();

	/**
	 * Return the permanent ID of this resource.
	 *
	 * The resource id is generated at creation time and does not change after that.
	 * It can be used to consistently identify or track a resource over its entire
	 * lifetime, even if the resource name or content changes. The resource id MUST
	 * be unique for a given storage object and revision, and SHOULD be unique for
	 * the entire lifetime of the storage object.
	 *
	 */
	String getId();

	/**
	 * Return the resource name, or null if the resource has no name. This is the
	 * case for temporary resources or resources that were removed recently.
	 */
	String getName();

	/**
	 * Change the resource name. If not null, the name must be unique for a given
	 * archive. If null, the resource is considered temporary and discarded at the
	 * end of the transaction.
	 *
	 * @throws NameConflict Name conflicts with another resource.
	 */
	void setName(String name) throws NameConflict, StaleHandle;

	/**
	 * Return the media-type of the resource. Resources with no mime-type (raw
	 * binary data) default to "application/octet-stream".
	 */
	String getMediaType();

	/**
	 * Return the content encoding name. Null is returned for resources that have no
	 * content encoding applied.
	 */
	String getContentEncoding();

	/**
	 * Change the resource media-type and set content encoding to null.
	 */
	default void setMediaType(String type) throws StaleHandle {
		setMediaType(type, null);
	}

	/**
	 * Change the resource media-type as well as the content encoding type.
	 *
	 * The values should follow 'Content-Type' and 'Content-Encoding' HTTP header
	 * format. Example media-type: `image/png` or `text/plain; charset=utf8` Example
	 * content-encoding: `gzip`, `deflate`
	 *
	 */
	void setMediaType(String type, String coding) throws StaleHandle;

	/**
	 * Number of bytes in this binary stream.
	 */
	long getSize();

	/**
	 * Return the time the resource was created.
	 */
	Date getCreated();

	/**
	 * Return the last time the content of the resource was modified.
	 */
	Date getLastModified();

	/**
	 * Return true if the resource is stored externally and its binary content
	 * cannot be accessed (read from or written to).
	 */
	default boolean isExternal() {
		return getExternalLocation() != null;
	}

	/**
	 * Return the external location hint, or null if the resource is not external.
	 * May be an empty string.
	 */
	String getExternalLocation();

	/**
	 * Mark this resource as external and set the external location hint. The pool
	 * is allowed to remove its own copy of the binary data, but must preserve all
	 * meta-data (size, digests, mtime).
	 *
	 * If the resource is already external, then only the location hint is updated.
	 *
	 * @param hint An arbitrary string that may help to locate the binary content
	 *             for this resource (e.g. an URL)
	 */
	void setExternalLocation(String hint);

	/**
	 * Return a {@link WritableByteChannel} ready to accept bytes for restoring an
	 * external resource. The caller is expected to write the original byte content
	 * of this resource (same size and hash) and then close the channel. If
	 * successful, the resource will be available again for regular data access.
	 * 
	 * The returned channel will raise {@link IOException}s when writing more data
	 * than expected, or when content hashes or size do not match the original
	 * resource at the moment the channel is closed. The restore can be aborted by
	 * closing the channel prematurely and catch the raised {@link IOException}.
	 * 
	 * Commit will fail with a {@link WriteChannelNotClosed} exception if there is
	 * an unclosed restore-channel.
	 *
	 * @return An open {@link WritableByteChannel}.
	 * @throws StaleHandle
	 * @throws {@link      IllegalStateException} if called on an already available
	 *                     resource.
	 */
	WritableByteChannel getRestoreChannel() throws StaleHandle, IOException;

	/**
	 * Restore this resource from an {@link InputStream}. The default implementation
	 * simply copies all available bytes to a {@link #getRestoreChannel()} target.
	 * The provided {@link InputStream} is NOT closed.
	 */
	default void restoreExternal(InputStream source) throws IOException {
		ReadableByteChannel io = Channels.newChannel(source);
		try (WritableByteChannel restore = getRestoreChannel()) {
			ByteBuffer buffer = ByteBuffer.allocate(DEFAULT_BUFFER);
			int read;
			while ((read = io.read(buffer)) > 0) {
				buffer.flip();
				while (read > 0)
					read -= restore.write(buffer);
				buffer.clear();
			}
			restore.close();
		}
	}

	/**
	 * Restore this resource from a file. The default implementation opens the file
	 * for reading and copies its content to a {@link #getRestoreChannel()} target.
	 */
	default void restoreExternal(Path file) throws IOException {
		try (WritableByteChannel restore = getRestoreChannel();
				FileChannel io = FileChannel.open(file)) {
			if (io.size() != getSize())
				throw new IOException("Source file size mismatch");
			io.transferTo(0, io.size(), restore);
			restore.close();
		}
	}

	/**
	 * Get a {@link ReadableByteChannel} to read from this resource, positioned at
	 * the specified byte mark.
	 *
	 * Creating a write- or restore-channel will close all existing read-channels
	 * and also prevent new read-channels from being created. Do not read from a
	 * resource while also writing to it.
	 *
	 * @param pos Position to start reading from.
	 * @return An opened {@link ReadableByteChannel}
	 * 
	 * @throws WriteChannelNotClosed if an opened write channel exists.
	 */
	ReadableByteChannel getReadChannel(long pos) throws StaleHandle, ExternalResourceException, IOException;

	/**
	 * Get a {@link WritableByteChannel} to append data to this resource, optionally
	 * truncating it to a specified maximum number of bytes first.
	 *
	 * If the truncate parameter is larger than or equal to the current size, then
	 * no data is discarded and new data is simply appended. A negative value has
	 * the same effect.
	 *
	 * Calling this method will close all existing read-, write- or restore-channels
	 * for the same resource. While a write-channel is open, no new read-channels
	 * can
	 * be opened and commit will fail with a {@link WriteChannelNotClosed}
	 * exception.
	 *
	 * @return An open {@link WritableByteChannel}.
	 * @param truncate Number of bytes to truncate this resource to, before
	 *                 returning the channel. If the number is larger or equal to
	 *                 the current size, then no data is discarded and new data is
	 *                 simply appended.
	 * @throws StaleHandle
	 * @throws ExternalResourceException
	 */
	WritableByteChannel getWriteChannel(long truncateTo) throws StaleHandle, ExternalResourceException, IOException;

	/**
	 * Return true if there exists and unclosed write- or restore-channel for this
	 * resource.
	 */
	boolean isWriteInProgress();

	/**
	 * Truncate the resource to the specified number of bytes, then copy up to count
	 * bytes from the source channel to the resource. The default implementation
	 * simply calls {@link #getWriteChannel(long)}, copies <code>count</code> bytes
	 * and then closes the write-channel again. Implementations may support more
	 * efficient ways (e.g. zero-copy) if the source channel is backed by a file or
	 * socket.
	 *
	 * Only a single write channel to a resource may exist at any given time.
	 * Previously returned read- or write-channels will be closed.
	 *
	 * @param src        Source channel to copy data from.
	 * @param truncateTo Number of bytes to truncate this resource to, before
	 *                   copying new data to the channel. If the number is larger or
	 *                   equal to the current size, then no data is discarded and
	 *                   new data is simply appended.
	 * @param count      Maximum number of bytes to copy from the source channel.
	 * @return Actual number of bytes returned.
	 * @throws IOException
	 */
	default long transferFrom(final ReadableByteChannel src, final long truncateTo, final long count)
			throws StaleHandle, ExternalResourceException, IOException {
		try (WritableByteChannel dst = getWriteChannel(truncateTo)) {
			long copied = 0;
			final ByteBuffer buf = ByteBuffer.allocate((int) Math.min(count, 1024 * 64));
			while (count > copied && src.read(buf) != -1) {
				buf.flip();
				while (buf.hasRemaining())
					copied += dst.write(buf);
				buf.clear();
				if ((copied - count) < buf.limit())
					buf.limit((int) (copied - count));
			}
			return copied;
		}
	}

	/**
	 * Clone the content of a different {@link Resource} in the most efficient way
	 * possible.
	 * 
	 * The default implementation simply calls
	 * {@link #transferFrom(ReadableByteChannel, long, long)} with a read channel
	 * of the source.
	 *
	 * Only a single write channel to a resource may exist at any given time.
	 * Previously returned read- or write-channels will be closed.
	 * 
	 * @throws IOException
	 * @throws ExternalResourceException
	 * @throws StaleHandle
	 */
	default void cloneFrom(Resource other) throws StaleHandle, ExternalResourceException, IOException {
		try (ReadableByteChannel src = other.getReadChannel(0)) {
			this.transferFrom(src, 0, other.getSize());
		}
	}

	/**
	 * Get an input stream to read from. This is just a shortcut for
	 * {@link #getReadChannel(long)} wrapped in a
	 * {@link Channels#newInputStream(ReadableByteChannel)}.
	 *
	 * @throws ExternalResourceException The resource is not available. See
	 *                                   {@link #isExternal()}.
	 *
	 */
	@Deprecated
	default InputStream getStream() throws StaleHandle, ExternalResourceException {
		try {
			return Channels.newInputStream(getReadChannel(0));
		} catch (final IOException e) {
			throw new BackendError(e);
		}
	}

	@Deprecated
	default Resource truncate(long truncateTo) throws StaleHandle, ExternalResourceException, IOException {
		getWriteChannel(truncateTo).close();
		return this;
	}

	@Deprecated
	default void write(byte[] data, int offset, int length) throws StaleHandle, ExternalResourceException, IOException {
		write(ByteBuffer.wrap(data, offset, length));
	}

	@Deprecated
	default void write(ByteBuffer data) throws StaleHandle, ExternalResourceException, IOException {
		try (WritableByteChannel ch = getWriteChannel(Long.MAX_VALUE)) {
			while (data.hasRemaining())
				ch.write(data);
		}
	}

	@Deprecated
	default void write(byte[] data) throws StaleHandle, ExternalResourceException, IOException {
		write(data, 0, data.length);
	}

	@Deprecated
	default void write(InputStream stream) throws StaleHandle, IOException, ExternalResourceException {
		transferFrom(Channels.newChannel(stream), getSize(), Long.MAX_VALUE);
	}

	/**
	 * Mark this resource for removal. This sets the resource name to null, but the
	 * resource remains intact until the transaction is committed.
	 */
	void remove() throws StaleHandle, ExternalResourceException;

	/**
	 * Get digests computed for the current resource binary content as hex strings.
	 * 
	 * For new or modified resources, the returned map will contain one entry for
	 * each algorithm name in {@link StoragePool#getDefaultDigests()}. Note that
	 * requesting digests may trigger computation and block for a considerable
	 * amount of time.
	 * 
	 * For existing resources, only known and pre-computed values are returned.
	 * Available algorithms depend on pool configuration at the time the resource
	 * was last modified.
	 * 
	 * The returned map is unmodifiable and will always contains at least one entry.
	 * 
	 * @throws WriteChannelNotClosed if a write channel exists and is still open.
	 */
	Map<String, String> getDigests();

}