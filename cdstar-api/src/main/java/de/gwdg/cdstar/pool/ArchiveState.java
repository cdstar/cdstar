package de.gwdg.cdstar.pool;

public enum ArchiveState {
	/**
	 * The object only exists as a draft. It was never archived.
	 */
	DRAFT,
	/**
	 * The object exists in the archive.
	 */
	ARCHIVED,
	/**
	 * The object exists in the archive, but was modified since.
	 */
	MODIFIED

}
