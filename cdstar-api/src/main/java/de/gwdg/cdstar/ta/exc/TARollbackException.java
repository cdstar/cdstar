package de.gwdg.cdstar.ta.exc;

public class TARollbackException extends Exception {
	private static final long serialVersionUID = 2589012897952178523L;

	public TARollbackException(Exception e) {
		super(e);
	}

}
