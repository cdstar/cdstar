package de.gwdg.cdstar.ta;

import java.time.Instant;

/**
 * Minimal information API about a transaction. Used for unprivileged actors
 * such as logging or templates.
 */
public interface TransactionInfo {
	String getId();

	Instant getStarted();

	Instant getExpires();

	public static enum Mode {
		SERIALIZABILITY, SNAPSHOT
	}

	TAState getState();

	Mode getMode();

	default boolean isClosed() {
		return getState() == TAState.COMMITTED || getState() == TAState.CLOSED;
	}

	default boolean isOpen() {
		return getState() == TAState.NEW || getState() == TAState.ROLLBACKONLY;
	}

	default boolean isRollbackOnly() {
		return getState() == TAState.ROLLBACKONLY;
	}

	default boolean isExpired() {
		final Instant e = getExpires();
		return e == null ? false : e.isBefore(Instant.now());
	}
}