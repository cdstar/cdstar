package de.gwdg.cdstar.ta;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;

/**
 * The journal associated with a given transaction can be used to persists small
 * records to disk before or during a transaction is prepared, to aid recovery
 * after a crash and allow a service to finish or repeat an interrupted commit.
 *
 * Write operations are buffered until {@link #flush()} is called. An automatic
 * {@link #flush()} is triggered after all services prepared successfully and
 * the transaction moved to the {@link TAState#PREPARED} state. The log file is
 * removed after a successful commit or rollback.
 *
 * If significant resources are allocated before or during the prepare phase,
 * and these resources need to be freed by a {@link TARecoveryHandler} in case
 * of a crash, make sure to {@link #flush()} at least once. Otherwise, records
 * may be missing or the bound {@link TARecoveryHandler} may not be called at
 * all after a crash. See {@link TARecoveryHandler} for details on when and how
 * the recovery is triggered after a restart.
 */
public interface TAJournal {

	/**
	 * Write a binary record to the write ahead log. The passed in byte array
	 * must not be modified before {@link #flush()} is called.
	 *
	 * @param key
	 *            Record type identifier or record ID. Can be used to filter
	 *            records during recovery. Keys are not required to be unique,
	 *            but MUST NOT collide with keys used by other services. Use
	 *            some kind of service-specific prefix to prevent collisions.
	 * @param data
	 *            Raw binary data, or null for an empty message
	 */
	void write(String key, ByteBuffer data);

	/**
	 * Write a record of size 0. Only the key is persisted.
	 */
	default void write(String key) {
		write(key, (ByteBuffer) null);
	}

	default void write(String key, byte[] data, int offset, int length) {
		write(key, ByteBuffer.wrap(data, offset, length));
	}

	default void write(String key, byte[] data) {
		if (data == null)
			write(key, (ByteBuffer) null);
		else
			write(key, ByteBuffer.wrap(data));
	}

	default void write(String key, String data) {
		if (data == null)
			write(key, (ByteBuffer) null);
		else
			write(key, data.getBytes(StandardCharsets.UTF_8));
	}

	/**
	 * Persist all buffered records to disk.
	 *
	 * @throws IOException
	 *             some or all records failed to persist. This is a fatal error
	 *             as the log is most likely corrupted and recovery will not be
	 *             possible from this log. The transaction should fail
	 *             immediately.
	 */
	void flush() throws IOException;

	/**
	 * True if the journal is closed for writing (after prepare or during
	 * rollback).
	 */
	boolean isClosed();
}
