package de.gwdg.cdstar.ta.exc;

/**
 * A commit failed because of the enclosed exception.
 */
public class TAFatalError extends Error {
	private static final long serialVersionUID = -7304175435496705408L;

	public TAFatalError(String string, Exception exception) {
		super(string, exception);
	}
}