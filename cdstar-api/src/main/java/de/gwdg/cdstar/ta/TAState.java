package de.gwdg.cdstar.ta;

import java.util.EnumSet;

public enum TAState {
	NEW, ROLLBACKONLY, PREPARING, PREPARED, COMMITTED, CLOSING, CLOSED;

	/**
	 * <code>
	 * NEW >--> PREPARING >--> PREPARED >--> COMMITTED
	 * | |         |
	 * | >--------->-------->----> CLOSING >--> CLOSED
	 * |                    |
	 * >--> ROLLBACKONLY >--+
	 * </code>
	 */

	private EnumSet<TAState> next;

	static {
		NEW.next = EnumSet.of(PREPARING, CLOSING, ROLLBACKONLY);
		ROLLBACKONLY.next = EnumSet.of(CLOSING);
		PREPARING.next = EnumSet.of(CLOSING, PREPARED);
		PREPARED.next = EnumSet.of(COMMITTED);
		COMMITTED.next = EnumSet.noneOf(TAState.class);
		CLOSING.next = EnumSet.of(CLOSED);
		CLOSED.next = EnumSet.noneOf(TAState.class);
	}

	public final TAState changeTo(TAState newState) {
		if (next.contains(newState))
			return newState;
		throw new IllegalStateException("Cannot change state from " + this + " to " + newState);
	}
}