package de.gwdg.cdstar.ta;

import java.time.Instant;

import de.gwdg.cdstar.ta.exc.TARollbackException;

/**
 * User facing API for a running transaction.
 */
public interface UserTransaction extends AutoCloseable, TransactionInfo {

	/**
	 * Bind a transactional resource to this transaction. The is usually not
	 * called by the user directly, but by a factory creating business objects.
	 */
	void bind(TAResource resource);

	/**
	 * Try to commit this transaction, or throw an exception wrapping the cause
	 * of the rollback.
	 */
	void commit() throws TARollbackException;

	/**
	 * Rollback the transaction without giving a cause, if not already rolled
	 * back.
	 */
	default void rollback() {
		rollback(null);
	}

	/**
	 * Rollback the transaction, if not already rolled back.
	 */
	void rollback(Throwable reason);

	/**
	 * Same as {@link #rollback()}
	 */
	@Override
	default void close() {
		rollback(null);
	}

	/**
	 * Mark this transaction as rollback-only. It cannot be committed after
	 * this.
	 */
	void setRollbackOnly();

	/**
	 * Change the desired isolation mode used for conflict detection.
	 */
	void setMode(Mode mode);

	void setExpires(Instant instant);

	void addListener(TAListener listener);

}