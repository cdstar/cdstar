package de.gwdg.cdstar.ta;

import de.gwdg.cdstar.pool.BackendError.TemporaryError;

/**
 * A resource participating in a transaction. This may represent a database
 * connection representing a running transaction, a message bus with all
 * messages buffered and not actually sent yet, or some other transactionally
 * enabled type of service.
 *
 * Resources are bound to transactions via
 * {@link UserTransaction#bind(TAResource)}. If the same resource is bound to
 * multiple transactions or not depends on the caller.
 *
 * This interface should not be made visible to the user, as it is only used by
 * the transaction manager implementation.
 */
public interface TAResource {

	/**
	 * Called by the {@link UserTransaction#bind(TAResource)}, should call
	 * {@link TransactionHandle#bindRecoveryHandler(TARecoveryHandler)}.
	 */
	void bind(TransactionHandle t);

	/**
	 * Prepare a commit and ensure than no other transaction can prepare or
	 * commit any conflicting changes.
	 *
	 * On success, the resource should be locked against conflicts and ready to
	 * be committed. The implementation should do as much work as possible
	 * during prepare, and only perform a single atomic action during the
	 * subsequent {@link #commit(TransactionHandle)} call.
	 *
	 * The {@link TAJournal} returned by {@link TransactionHandle#getJournal()}
	 * should be used to assist recovery in case of a crash. Alternatively, a
	 * custom undo/redo log may be persisted to disk.
	 *
	 * If any service of this transaction fails to prepare, a call to
	 * {@link #rollback(TransactionHandle)} will follow, allowing the
	 * implementation to free any temporary resources. If the raised exception
	 * is a subclass of {@link TemporaryError}, the runtime may also decide to
	 * retry after a certain amount of time. This is the only situation for
	 * which {@link #prepare(TransactionHandle)} is called more than once on a
	 * given transaction.
	 *
	 * @param t
	 *            Transaction handle in {@link TAState#NEW} state.
	 * @throws Exception
	 *             to singal that the transaction cannot commit and should be
	 *             rolled back.
	 */
	void prepare(TransactionHandle t) throws Exception;

	/**
	 * Perform a commit. This is called after
	 * {@link #prepare(TransactionHandle)} if and only if all other participants
	 * in the transaction were successfully prepared.
	 *
	 * On success, all changes to resources associated with the given
	 * transaction should be made visible to new transactions and the resources
	 * should be unlocked to allow other transactions to prepare and commit.
	 *
	 * This method must not fail. If it does, all resources associated with the
	 * transaction should remain locked until recovery was successful. The
	 * runtime might decide to continue with a partially locked services, or
	 * shut down completely and wait for manual recovery.
	 *
	 * @param t
	 *            Transaction handle in {@link TAState#PREPARED} state.
	 */
	void commit(TransactionHandle t);

	/**
	 * Undo all changes made during a transaction. This might be called before
	 * or after {@link #prepare(TransactionHandle)}, but never after a
	 * {@link #commit(TransactionHandle)}.
	 *
	 * On success, all temporary or otherwise unused resources should be freed.
	 * On failure, the implementation should log the cause of the failure, but
	 * otherwise continue working. Resource leaks are not considered fatal.
	 *
	 * {@link #rollback(TransactionHandle)} is also called to close
	 * {@link UserTransaction#isRollbackOnly()} transactions or if no changes
	 * were made during the transaction.
	 *
	 * @param t
	 *            Transaction handle in {@link TAState#CLOSING} state
	 */
	void rollback(TransactionHandle t);

}
