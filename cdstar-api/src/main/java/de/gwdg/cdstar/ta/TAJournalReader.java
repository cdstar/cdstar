package de.gwdg.cdstar.ta;

import java.io.IOException;
import java.util.function.Consumer;
import java.util.function.Predicate;

/**
 * This interface allows reading a {@link TAJournal} multiple times. Instances
 * are NOT thread-safe and MUST NOT be used outside of a
 * {@link TARecoveryHandler#recover(String, TAJournalReader, boolean)} call.
 */
public interface TAJournalReader {
	/*
	 * Note: This interface intentionally does not implement {@link Iterable},
	 * as it breaks the iterable contract in multiple ways: For example, it can
	 * be reset and throws checked exceptions.
	 */

	/**
	 * Return the next record (in order written), or NULL if no records remain
	 * in the log. Note that all records from all parties involved in the
	 * transaction are returned, not only records written by the currently
	 * recovering service.
	 */
	TAJournalRecord next() throws IOException;

	/**
	 * Return the next record that matches a specific predicate, or NULL if no
	 * such record is found.
	 */
	default TAJournalRecord next(Predicate<TAJournalRecord> pred) throws IOException {
		for (TAJournalRecord e; (e = next()) != null;) {
			if (pred.test(e))
				return e;
		}
		return null;
	}

	/**
	 * Return the next record with a specific key, or NULL if no such record is
	 * found.
	 */
	default TAJournalRecord next(String key) throws IOException {
		return next(e -> e.getName().equals(key));
	}

	/**
	 * Call a consumer for each remaining records.
	 */
	default void forEach(Consumer<TAJournalRecord> consumer) throws IOException {
		for (TAJournalRecord e = next(); e != null; e = next()) {
			consumer.accept(e);
		}
	}

	/**
	 * Reset the reader to point to the first record.
	 */
	void reset() throws IOException;
}