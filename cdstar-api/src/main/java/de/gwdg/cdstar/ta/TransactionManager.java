package de.gwdg.cdstar.ta;

public interface TransactionManager {

	UserTransaction begin();

	UserTransaction getTransaction(String txId);

}
