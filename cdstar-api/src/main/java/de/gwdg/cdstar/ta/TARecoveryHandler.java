package de.gwdg.cdstar.ta;

import java.io.Serializable;

/**
 * A {@link TARecoveryHandler} can be bound to transactions via
 * {@link TransactionHandle#bindRecoveryHandler(TARecoveryHandler)}. The first
 * time a new recovery handler is bound, it is persisted to disk. After a crash,
 * all {@link TARecoveryHandler}s that were bound to interrupted transactions
 * are loaded again and given a chance to rollback or finish any interrupted or
 * failed transaction they were involved in. They are given access to the
 * write-ahead-log of a transaction via a {@link TAJournalReader}.
 * 
 * Persisting a {@link TARecoveryHandler} is expensive and once persisted, the
 * files are not removed until the next service restart. {@link TAResource}s of
 * the same type should therefore share a single {@link TARecoveryHandler}
 * instance, if possible. Any resource- or transaction-specific information
 * needed by the recovery handler can be stored cheaply into the
 * transaction-bound {@link TAJournal}. The {@link TARecoveryHandler} should
 * generate a unique and specific {@link TAJournalRecord} key or key namespace
 * for the {@link TAResource}s to use, and make sure this key is part of the
 * serialized state of the handler instance.
 */
public interface TARecoveryHandler extends Serializable {

	/**
	 * Called once for each incomplete or failed transaction during the recovery
	 * phase. This method must be thread-safe or at least synchronized.
	 * 
	 * Note that the {@link TAJournalReader} provides records from all
	 * {@link TAResource}s involved in a transaction. The recovery handler MUST
	 * ignore {@link TAJournalRecord}s with unknown keys.
	 * 
	 * @param tx       Transaction ID
	 * @param log      Read-only handle for the transaction-specific write ahead
	 *                 log.
	 * @param doCommit True if the transaction should be committed, false if it
	 *                 should be rolled back.
	 * 
	 * @throws Exception Any exception thrown will be logged and will cause the
	 *                   recovery to fail. The transaction manager may still try to
	 *                   recover as many transactions as possible.
	 */
	void recover(String tx, TAJournalReader log, boolean doCommit) throws Exception;

}
