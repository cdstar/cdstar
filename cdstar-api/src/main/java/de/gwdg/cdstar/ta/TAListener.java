package de.gwdg.cdstar.ta;

/**
 * Interface for classes interested in the state of a transaction, but not
 * actively participating in the transaction as a {@link TAResource}. Listeners
 * do not take part in crash recovery.
 */
public interface TAListener {

	/**
	 * Called before a transaction moves from the {@link TAState#NEW} to the
	 * {@link TAState#PREPARING} state. Implementations can abort the commit by
	 * throwing an exception.
	 */
	default void beforeCommit(TransactionInfo t) throws Exception {
	}

	/**
	 * Called after a transaction reaches the {@link TAState#COMMITTED} state.
	 * Exceptions are logged but otherwise ignored.
	 */
	default void afterCommit(TransactionInfo t) {
	}

	/**
	 * Called after the transaction reaches {@link TAState#CLOSED} state.
	 * Exceptions are logged but otherwise ignored.
	 */
	default void afterRollback(TransactionInfo t) {
	}

}
