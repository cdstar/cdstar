package de.gwdg.cdstar.ta;

/**
 * Extended transactions API for services.
 */
public interface TransactionHandle extends TransactionInfo {

	/**
	 * Associate a recovery handler to this transaction, It will be called
	 * during recovery after a crash.
	 *
	 * If the recovery handler is unknown to the transaction manager, it is
	 * persisted to disc.
	 *
	 * Binding the same recovery handler to the same transaction multiple times
	 * is a NOOP
	 */
	void bindRecoveryHandler(TARecoveryHandler handler);

	TAJournal getJournal();

	void addListener(TAListener listener);

}