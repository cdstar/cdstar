package de.gwdg.cdstar.ta;

import java.nio.charset.StandardCharsets;

public interface TAJournalRecord {
	String getName();

	byte[] getValue();

	/**
	 * Return the value as a (UTF-8) String.
	 */
	default String getStringValue() {
		return new String(getValue(), StandardCharsets.UTF_8);
	}
}