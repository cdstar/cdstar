package de.gwdg.cdstar.event;

import de.gwdg.cdstar.runtime.client.CDStarArchive;

public class ChangeEvent {

	String vault;
	String archive;
	String tx;
	String revision; // null for deleted archives
	String parent; // null for new archives
	long ts;

	public ChangeEvent() {
	}

	public ChangeEvent(CDStarArchive ar) {
		vault = ar.getVault().getName();
		archive = ar.getId();
		parent = ar.getParentRev() == null ? null : ar.getRev();
		revision = ar.isRemoved() ? null : ar.getNextRev();
		tx = ar.getSession().getSessionId();
		ts = ar.getContentModified().getTime();
	}

	public String getVault() {
		return vault;
	}

	public void setVault(String vault) {
		this.vault = vault;
	}

	public String getArchive() {
		return archive;
	}

	public void setArchive(String archive) {
		this.archive = archive;
	}

	public String getTx() {
		return tx;
	}

	public void setTx(String session) {
		tx = session;
	}

	public String getRevision() {
		return revision;
	}

	public void setRevision(String revision) {
		this.revision = revision;
	}

	public String getParent() {
		return parent;
	}

	public void setParent(String oldRevision) {
		parent = oldRevision;
	}

	public long getTs() {
		return ts;
	}

	public void setTs(long ts) {
		this.ts = ts;
	}

}
