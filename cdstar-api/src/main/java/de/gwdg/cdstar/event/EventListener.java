package de.gwdg.cdstar.event;

public interface EventListener {
	void onEvent(ChangeEvent event);

}
