package de.gwdg.cdstar.runtime.client;

import de.gwdg.cdstar.auth.Subject;
import de.gwdg.cdstar.runtime.RuntimeContext;
import de.gwdg.cdstar.ta.TransactionInfo.Mode;

/**
 * A handle for an authenticated subject interacting with the service.
 */
public interface CDStarClient {

	/**
	 * Create a session.
	 */
	CDStarSession begin(boolean readOnly, Mode mode);

	/**
	 * Create a session with {@link Mode#SNAPSHOT} isolation.
	 */
	default CDStarSession begin(boolean readOnly) {
		return begin(readOnly, Mode.SNAPSHOT);
	}

	/**
	 * Create a writeable session with {@link Mode#SNAPSHOT} isolation.
	 */
	default CDStarSession begin() {
		return begin(false);
	}

	RuntimeContext getRuntime();

	Subject getSubject();

}
