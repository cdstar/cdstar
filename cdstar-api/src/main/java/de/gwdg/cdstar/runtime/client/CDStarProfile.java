package de.gwdg.cdstar.runtime.client;

import java.util.Map;

public interface CDStarProfile {
	String DEFAULT_NAME = "default";
	String getName();

	Map<String, String> getPropertyMap();

	default String getProperty(String key) {
		return getPropertyMap().get(key);
	}

	default String getProperty(String key, String defaultValue) {
		return getPropertyMap().getOrDefault(key, defaultValue);
	}

}