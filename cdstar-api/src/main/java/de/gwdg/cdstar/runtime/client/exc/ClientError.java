package de.gwdg.cdstar.runtime.client.exc;

public class ClientError extends Exception {
	private static final long serialVersionUID = -7040812671953201442L;

	public ClientError(String string) {
		super(string);
	}

	public ClientError(String string, Exception e) {
		super(string, e);
	}

}
