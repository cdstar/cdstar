package de.gwdg.cdstar.runtime.client.exc;

public class NoSuchProvider extends ClientError {
	private static final long serialVersionUID = 4925623198110134542L;

	public NoSuchProvider(String vaultName) {
		super(vaultName);
	}

}
