package de.gwdg.cdstar.runtime.client;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;
import java.util.Date;
import java.util.Map;

import de.gwdg.cdstar.pool.PoolError.WriteChannelNotClosed;
import de.gwdg.cdstar.pool.Resource;
import de.gwdg.cdstar.runtime.client.auth.ArchivePermission;
import de.gwdg.cdstar.runtime.client.exc.FileAvailable;
import de.gwdg.cdstar.runtime.client.exc.FileExists;
import de.gwdg.cdstar.runtime.client.exc.InvalidFileName;

public interface CDStarFile extends CDStarAnnotateable {
	String PATH_SEP = "/";
	int BUFFER_SIZE = 8192; // TODO: Benchmark this
	String DIGEST_MD5 = "MD5";
	String DIGEST_SHA1 = "SHA-1";
	String DIGEST_SHA256 = "SHA-256";

	/**
	 * Return the file name, or null if the file has no name. This is the case
	 * for temporary file or file that were removed recently.
	 */
	String getName();

	/**
	 * Change the file name. The name must be unique for a given archive.
	 *
	 * @throws FileExists
	 *             if a file or directory with the same name exists.
	 * @throws InvalidFileName
	 *             if the name contains invalid path segments, is empty or null.
	 */
	void setName(String name) throws FileExists, InvalidFileName;

	/**
	 * Return the permanent ID of this file.
	 *
	 * The file ID is generated at file creation time and does not change after
	 * that. It can be used to consistently identify files within an archive
	 * independently of their (mutable) names or content.
	 *
	 * The value is guaranteed to be unique for a given archive and revision,
	 * and SHOULD be unique for the entire lifetime of an archive.
	 */
	String getID();

	/**
	 * Return the media-type of the file. Files with no mime-type (raw binary
	 * data) default to "application/octet-stream".
	 */
	String getMediaType();

	/**
	 * Return the name of the content encoding. Null is returned for files that
	 * have no content encoding applied.
	 */
	String getContentEncoding();

	/**
	 * Return the computed digest (hex) values of the files binary content.
	 * 
	 * If file content was modified, then all supported algorithms are returned
	 * (and computed, if necessary). Otherwise, only already known values are
	 * returned. At least one digest is always present, the returned map is never
	 * empty.
	 * 
	 * @thors {@link WriteChannelNotClosed} if the file is currently opened for
	 *        writing.
	 */
	Map<String, String> getDigests();

	/**
	 * Change the file media-type and set content encoding to null.
	 */
	default void setMediaType(String type) {
		setMediaType(type, null);
	}

	/**
	 * Change the file media-type as well as the content encoding.
	 *
	 * The values should follow 'Content-Type' and 'Content-Encoding' HTTP
	 * header format. Example: `image/png` or `text/plain; charset=utf8`
	 */
	void setMediaType(String type, String coding);

	/**
	 * Return size in bytes.
	 */
	long getSize();

	/**
	 * Return the time the file was created.
	 */
	Date getCreated();

	/**
	 * Return the time the content of the file was last modified, not
	 * considering modifications during the current session.
	 */
	Date getLastModified();

	/**
	 * Get an input stream to read from.
	 *
	 * The returned stream should not be used after any modification to the files
	 * binary data.
	 */
	InputStream getStream() throws IOException;

	/**
	 * Reduce the size of this file to match <code>len</code>. If <code>len</code>
	 * is larger or equal to the current file size, do nothing.
	 */
	CDStarFile truncate(long len) throws IOException;

	/**
	 * Same as {@link #getReadChannel(long)} with a position of 0.
	 */
	default ReadableByteChannel getReadChannel() throws IOException {
		return getReadChannel(0);
	}

	/**
	 * Return a {@link ReadableByteChannel} opened for reading, starting from a
	 * specific position.
	 *
	 * Only one read- or write-channel can be open at any given time. If a new one
	 * is requested, all previously created channels are closed.
	 */
	ReadableByteChannel getReadChannel(long pos) throws IOException;

	/**
	 * Return a {@link WritableByteChannel} opened for writing (appending) data to
	 * the file.
	 *
	 * The returned {@link WritableByteChannel} will be blocking and always write
	 * all bytes before returning, as defined by
	 * {@link WritableByteChannel#write(ByteBuffer)}.
	 *
	 * Only one read- or write-channel can be open at any given time. If a new one
	 * is requested, all previously created channels are closed.
	 * 
	 * As long as a write-channel is open, no read-channel can be created and the
	 * transaction cannot be committed.
	 */
	WritableByteChannel getWriteChannel() throws IOException;

	/**
	 * Return a {@link WritableByteChannel} that can be used to restore an
	 * unavailable file. see {@link Resource#getRestoreChannel()}
	 */
	WritableByteChannel getRestoreChannel() throws IOException, FileAvailable;

	/**
	 * Mark this file as unavailable, allowing the backing storage pool to remove
	 * binary content for this file from local storage. Only allowed if
	 * {@link CDStarMirrorState#isMirrored()} returns true. Requires restore (see
	 * {@link #getRestoreChannel()} before the file can be read again.
	 * 
	 * Requires {@link ArchivePermission#CHANGE_MIRROR} permissions.
	 * 
	 * @throws {@link IllegalStateException} if the {@link CDStarMirrorState} has no
	 *                mirror configured.
	 */
	void setUnavailable();

	/**
	 * Copy bytes from a stream. Return the number of bytes actually copied. The
	 * stream is not closed.
	 *
	 * @throws IOException if reading from the {@link InputStream} fails.
	 * @deprecated Use {@link #getWriteChannel()} for repeated writes.
	 */
	@Deprecated
	default long write(InputStream stream) throws IOException {
		return write(stream, Long.MAX_VALUE);
	}

	/**
	 * Copy up to <code>limit</code> bytes from a stream. Return the number of bytes
	 * actually copied.
	 *
	 * @throws IOException if reading from the {@link InputStream} fails.
	 * @deprecated Use {@link #getWriteChannel()} for repeated writes.
	 */
	@Deprecated
	default long write(InputStream stream, long limit) throws IOException {
		long copied = 0;
		final ByteBuffer buffer = ByteBuffer.allocate((int) Math.min(BUFFER_SIZE, limit));
		try (WritableByteChannel ch = getWriteChannel()) {
			while (copied < limit && buffer
				.limit(stream.read(buffer.array(), 0, (int) Math.min(BUFFER_SIZE, limit - copied))).hasRemaining()) {
				while (buffer.hasRemaining())
					copied += ch.write(buffer);
				buffer.clear();
			}
			return copied;
		}
	}

	/**
	 * Copy bytes from a byte array.
	 *
	 * @deprecated Use {@link #getWriteChannel()} for repeated writes.
	 */
	@Deprecated
	default void write(byte[] data) throws IOException {
		write(data, data.length);
	}

	/**
	 * Copy up to <code>limit</code> bytes from a byte array.
	 *
	 * @deprecated Use {@link #getWriteChannel()} for repeated writes.
	 */
	@Deprecated
	default void write(byte[] data, int limit) throws IOException {
		write(data, 0, limit);
	}

	/**
	 * Copy up to <code>limit</code> bytes from a byte array, skipping the first
	 * 'skip' bytes.
	 *
	 * @deprecated Use {@link #getWriteChannel()} for repeated writes.
	 */
	@Deprecated
	default void write(byte[] data, int skip, int limit) throws IOException {
		write(ByteBuffer.wrap(data, skip, limit));
	}

	/**
	 * Copy all bytes from a {@link ByteBuffer}.
	 *
	 * @deprecated Use {@link #getWriteChannel()} for repeated writes.
	 */
	@Deprecated
	default void write(ByteBuffer buffer) throws IOException {
		try (WritableByteChannel ch = getWriteChannel()) {
			while (buffer.hasRemaining())
				ch.write(buffer);
		}
	}

	void remove();

	default boolean isRemoved() {
		return getName() == null;
	}

	CDStarArchive getArchive();

	/**
	 * Return snapshot this file belongs to, or null.
	 */
	CDStarSnapshot getSnapshot();

	boolean isAvailable();

	/**
	 * Transfer all content from one {@link CDStarFile} into this one, appending to
	 * current content, if any.
	 * 
	 * The default implementation simply copies all bytes from the sources
	 * {@link #getReadChannel()} to a new {@link #getWriteChannel()}. Specific
	 * implementations should optimize the case where data can be copied more
	 * efficiently.
	 */
	default void transferFrom(CDStarFile source) throws IOException {
		ByteBuffer buffer = ByteBuffer.allocate(BUFFER_SIZE);
		try (ReadableByteChannel in = source.getReadChannel(); WritableByteChannel out = getWriteChannel()) {
			in.read(buffer);
			buffer.flip();
			while (buffer.hasRemaining())
				out.write(buffer);
		}
	}


}
