package de.gwdg.cdstar.runtime.listener;

import de.gwdg.cdstar.runtime.client.CDStarSession;
import de.gwdg.cdstar.runtime.client.CDStarVault;

public interface SessionListener {

	/**
	 * Called once for each vault that is opened by this session.
	 *
	 * The callback is triggered after access control, but before the vault is
	 * actually returned.
	 */
	default void onVaultOpenend(CDStarVault vault) {
	}

	/**
	 * Called during prepare phase, before the archives are committed but after the
	 * transaction was marked as prparing.
	 */
	default void onPrepare(CDStarSession session) {
	}

	/**
	 * Called after a successful commit. Errors thrown here are logged but ignored.
	 */
	default void onCommit(CDStarSession session) {
	}

	/**
	 * Called after a session rollback. Errors are logged but ignored.
	 */
	default void onRollback(CDStarSession session) {
	}

	static SessionListener wrap(VaultListener listener) {
		return new SessionListener() {
			@Override
			public void onVaultOpenend(CDStarVault vault) {
				vault.addListener(listener);
			}
		};
	}

}
