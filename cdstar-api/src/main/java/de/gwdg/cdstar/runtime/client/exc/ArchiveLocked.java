package de.gwdg.cdstar.runtime.client.exc;

/**
 * This unchecked exception is thrown if an operation is not allowed due to the
 * archive being in an unavailable storage state.
 */
public class ArchiveLocked extends IllegalStateException {
	private static final long serialVersionUID = -8102695906202684177L;
	private final String vault;
	private final String archive;

	public ArchiveLocked(String vault, String archive) {
		super(vault + "/" + archive);
		this.vault = vault;
		this.archive = archive;
	}

	public String getVault() {
		return vault;
	}

	public String getArchive() {
		return archive;
	}

}
