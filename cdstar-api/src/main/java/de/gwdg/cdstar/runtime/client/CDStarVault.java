package de.gwdg.cdstar.runtime.client;

import java.util.Collection;
import java.util.List;

import de.gwdg.cdstar.runtime.client.auth.ArchivePermission;
import de.gwdg.cdstar.runtime.client.auth.VaultPermission;
import de.gwdg.cdstar.runtime.client.exc.ArchiveNotFound;
import de.gwdg.cdstar.runtime.client.exc.ProfileNotDefined;
import de.gwdg.cdstar.runtime.listener.VaultListener;

public interface CDStarVault {

	CDStarSession getSession();

	CDStarArchive createArchive();

	CDStarArchive createArchive(String preDefinedId);

	CDStarArchive loadArchive(String id) throws ArchiveNotFound;

	String getName();

	boolean isPublic();

	void addListener(VaultListener listener);

	Collection<CDStarArchive> listOpenedArchives();

	/**
	 * Return a named storage profile, if available for this vault.
	 */
	default CDStarProfile getProfileByName(String name) throws ProfileNotDefined {
		for (final CDStarProfile p : getProfiles()) {
			if (p.getName().equals(name))
				return p;
		}
		throw new ProfileNotDefined(name);
	}

	/**
	 * Return a list of profiles available for this vault (or globally).
	 */
	List<CDStarProfile> getProfiles();

	/**
	 * Return an lexicographically ordered iterator over all IDs of a vault.
	 *
	 * In non-strict mode, IDs of recently deleted archives may be included and
	 * archive-level permissions are NOT checked. This mode required
	 * {@link VaultPermission#LIST} permissions and is usually significantly faster
	 * than strict mode.
	 *
	 * In strict mode, only archives that are actually
	 * {@link ArchivePermission#LOAD}-able by the current user are returned. The
	 * extra check may be expensive, but in return, this also works without
	 * {@link VaultPermission#LIST} permissions and should still be faster than
	 * actually loading individual archives.
	 *
	 * @param scanOffset Start with the smallest ID (lexicographically ordered)
	 *                   larger than this one. Pass an empty string to return ALL
	 *                   ids.
	 * @param strict     Take extra care in not returning deleted archives or
	 *                   archives not readable by the current user.
	 */
	Iterable<String> getArchiveIterable(String offset, boolean strict);

}
