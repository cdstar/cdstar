package de.gwdg.cdstar.runtime.client.auth;

import java.util.Objects;

/**
 * Value types for string based grant subjects for CDSTAR object-bound access
 * control.
 *
 * Subjects are immutable, and in case of {@link SpecialSubject} singletons.
 * {@link #toString()} and {@link #fromString(String)} can be used to convert
 * between subjects and a type-aware string representation.
 */

public abstract class StringSubject {
	private static final String QUALIFIER = "@";
	private static final String GROUP_PREFIX = "@";
	private static final String SPECIAL_PREFIX = "$";
	protected final String name;
	protected final String domain;

	private StringSubject(String name, String domain) {
		if (name == null || name.isEmpty())
			throw new IllegalArgumentException("Subject name must not be null or empty");
		if (domain == null && name.contains(QUALIFIER))
			throw new IllegalArgumentException("Subject domain is required if name contains: " + QUALIFIER);
		this.name = name;
		this.domain = domain;
	}

	private StringSubject(String qname) {
		final int i = qname.lastIndexOf(QUALIFIER);
		if (i > 0) {
			name = qname.substring(0, i);
			domain = qname.substring(i + 1);
		} else {
			name = qname;
			domain = null;
		}
	}

	/**
	 * Return the name of the principal or group (no prefix).
	 */
	public final String getName() {
		return name;
	}

	/**
	 * Return the domain of the principal or group, or null if the subject is
	 * unqualified.
	 */
	public final String getDoamin() {
		return name;
	}

	public boolean isQualified() {
		return domain != null;
	}

	public String getQualifiedName() {
		if (!isQualified())
			throw new UnsupportedOperationException("Unqualified subject");
		return name + QUALIFIER + domain;
	}

	@Override
	public boolean equals(Object obj) {
		return obj.getClass() == getClass()
			&& ((StringSubject) obj).name.equals(name)
			&& Objects.equals(((StringSubject) obj).domain, domain);
	}

	@Override
	public int hashCode() {
		return name.hashCode() + (domain == null ? 0 : (31 * domain.hashCode()));
	}

	/**
	 * Return a string uniquely identifying this subject (including a type-specific
	 * prefix). The returned string can be used with {@link #fromString(String)} to
	 * get back the original subject.
	 */
	@Override
	public abstract String toString();

	/**
	 * Return a subject based on the specified string, as long as the string was
	 * acquired via {@link #toString()}. The exact subject type is derived from the
	 * string prefix.
	 */
	public static StringSubject fromString(String name) {
		if (name.startsWith(SPECIAL_PREFIX))
			return SpecialSubject.fromString(name);
		if (name.startsWith(GROUP_PREFIX))
			return GroupSubject.fromString(name);
		return PrincipalSubject.fromString(name);
	}

	public static class PrincipalSubject extends StringSubject {

		public PrincipalSubject(String name, String domain) {
			super(name, domain);
			if (name.startsWith(GROUP_PREFIX) || name.startsWith(SPECIAL_PREFIX))
				throw new IllegalArgumentException("Principals must not start with reserved characters.");
		}

		public PrincipalSubject(String qname) {
			super(qname);
			if (name.startsWith(GROUP_PREFIX) || name.startsWith(SPECIAL_PREFIX))
				throw new IllegalArgumentException("Principals must not start with reserved characters.");
		}

		public static PrincipalSubject fromString(String name) {
			final int i = name.lastIndexOf(QUALIFIER);
			if(i > 0)
				return new PrincipalSubject(name.substring(0,i), name.substring(i+1));
			return new PrincipalSubject(name, null);
		}

		@Override
		public String toString() {
			return isQualified() ? getQualifiedName() : getName();
		}
	}

	public static class GroupSubject extends StringSubject {
		public GroupSubject(String name, String domain) {
			super(name, domain);
		}

		public GroupSubject(String qname) {
			super(qname);
		}

		public static GroupSubject fromString(String name) {
			if (!name.startsWith(GROUP_PREFIX))
				throw new IllegalArgumentException("Does not start with: " + GROUP_PREFIX);
			final int i = name.lastIndexOf(QUALIFIER);
			if (i > 0)
				return new GroupSubject(name.substring(1, i), name.substring(i + 1));
			return new GroupSubject(name.substring(1), null);
		}

		@Override
		public String toString() {
			return GROUP_PREFIX + (isQualified() ? getQualifiedName() : getName());
		}
	}

	public static class SpecialSubject extends StringSubject {
		public static final SpecialSubject OWNER = new SpecialSubject("owner");
		public static final SpecialSubject USER = new SpecialSubject("user");
		public static final SpecialSubject ANY = new SpecialSubject("any");

		private SpecialSubject(String name) {
			super(name, null);
		}

		public static SpecialSubject fromString(String name) {
			if (!name.startsWith(SPECIAL_PREFIX))
				throw new IllegalArgumentException("Does not start with: " + SPECIAL_PREFIX);
			name = name.substring(1);
			if (name.equals(OWNER.name))
				return OWNER;
			if (name.equals(USER.name))
				return USER;
			if (name.equals(ANY.name))
				return ANY;
			throw new IllegalArgumentException("Unsupported value for SpecialSubject: " + name);
		}

		@Override
		public String toString() {
			return SPECIAL_PREFIX + getName();
		}
	}

}
