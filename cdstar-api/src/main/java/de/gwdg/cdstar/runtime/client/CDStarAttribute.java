package de.gwdg.cdstar.runtime.client;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;

public interface CDStarAttribute {

	public static final String SCHEMA_SEPARATOR = ":";
	public static final Pattern NAME_PATTERN = Pattern
		.compile("([a-z][0-9a-z_]*"
				+ Pattern.quote(SCHEMA_SEPARATOR)
				+ ")?[a-z][0-9a-z_.]*",
			Pattern.CASE_INSENSITIVE);

	/**
	 * Return the attribute name
	 */
	String getName();

	/**
	 * Return a unmodifiable list of values. The list may be empty.
	 */
	List<String> values();

	/**
	 * Replace all values of a attribute with new values. If the collection
	 * contains null values, these are filtered out.
	 */
	void set(List<String> values);

	/**
	 * Return the number of values in this attribute.
	 */
	default int size() {
		return values().size();
	}

	/**
	 * Return true if there are no values stored in this attribute, false
	 * otherwise.
	 */
	default boolean isEmpty() {
		return size() == 0;
	}

	/**
	 * Return the first value, or null if there is no value in this attribute.
	 */
	default String getFirst() {
		return get(0);
	}

	/**
	 * Return a specific value, or null if the attribute does not contain enough
	 * values.
	 */
	default String get(int index) {
		return get(index, null);
	}

	/**
	 * Return a specific value, or a default value if the attribute is does not
	 * contain enough values.
	 */
	default String get(int index, String defaultValue) {
		final List<String> values = values();
		if (index < 0 || values.size() <= index)
			return defaultValue;
		return values.get(index);
	}

	/**
	 * Append a single value.
	 */
	default void append(String value) {
		insert(size(), Arrays.asList(value));
	}

	/**
	 * Append values.
	 */
	default void append(String... values) {
		insert(size(), Arrays.asList(values));
	}

	/**
	 * Append values.
	 */
	default void append(List<String> values) {
		insert(size(), values);
	}

	/**
	 * Insert a single value at a specific index.
	 */
	default void insert(int index, String value) {
		insert(index, Arrays.asList(value));
	}

	/**
	 * Insert values at a specific index.
	 */
	default void insert(int index, String... values) {
		insert(index, Arrays.asList(values));
	}

	/**
	 * Insert values at a specific index.
	 */
	default void insert(int index, List<String> values) {
		final ArrayList<String> newValues = new ArrayList<>(values());
		newValues.addAll(index, values);
		set(newValues);
	}

	/**
	 * Remove a single value, if present.
	 */
	default boolean remove(String value) {
		return remove(Arrays.asList(value));
	}

	/**
	 * Remove multiple values, if present.
	 */
	default boolean remove(String... values) {
		return remove(Arrays.asList(values));
	}

	/**
	 * Remove multiple values, if present.
	 */
	default boolean remove(Collection<String> values) {
		final ArrayList<String> newValues = new ArrayList<>(values());
		final boolean changed = newValues.removeAll(values);
		set(newValues);
		return changed;
	}

	/**
	 * Change the list of values to only contain a single new value.
	 */
	default void set(String value) {
		set(Arrays.asList(value));
	}

	/**
	 * Replaces all values.
	 */
	default void set(String... values) {
		set(Arrays.asList(values));
	}

	/**
	 * Remove all values.
	 */
	default void clear() {
		set(Collections.emptyList());
	}

}
