package de.gwdg.cdstar.runtime.client;

import java.util.Date;
import java.util.List;

import de.gwdg.cdstar.runtime.client.exc.FileNotFound;

public interface CDStarSnapshot extends CDStarAnnotateable {

	public static final String ID_SEP = "@";

	default String getId() {
		return getSource().getId() + ID_SEP + getName();
	}

	/**
	 * Return the archive this snapshot was created from.
	 */
	CDStarArchive getSource();

	/**
	 * Return the revision this snapshot is based on.
	 */
	String getRevision();

	/**
	 * Return the name of this snapshot.
	 */
	String getName();

	CDStarMirrorState getMirrorState();

	/**
	 * Same as {@link CDStarMirrorState#getProfile()}
	 */
	@Deprecated
	default CDStarProfile getProfile() {
		return getMirrorState().getProfile();
	}

	/**
	 * Same as {@link CDStarMirrorState#setProfile(CDStarProfile)}
	 **/
	@Deprecated
	default void setProfile(CDStarProfile profile) {
		getMirrorState().setProfile(profile);
	}

	/**
	 * Same as {@link CDStarMirrorState#isAvailable()}
	 */
	@Deprecated
	default boolean isAvailable() {
		return getMirrorState().isAvailable();
	}

	/**
	 * Get the value of a system property, or null if the property is undefined.
	 */
	String getProperty(String name);

	default String getProperty(String name, String defaultValue) {
		final String value = getProperty(name);
		return value != null ? value : defaultValue;
	}

	/**
	 * Set the value of a system property. If the value is null, the property is
	 * removed.
	 */
	void setProperty(String name, String value);

	/**
	 * Return the date the snapshot was created.
	 */
	Date getCreated();

	/**
	 * Return the date the snapshot was last modified (e.g. profile changed)
	 */
	Date getModified();

	/**
	 * Return the name of the suer that created this snapshot.
	 */
	String getCreator();

	/**
	 * Get a list of all files in this snapshot.
	 *
	 * @throws IllegalStateException if the snapshot was removed.
	 */
	List<CDStarFile> getFiles();

	default int getFileCount() {
		return getFiles().size();
	}

	/**
	 * Return true if the file exists.
	 */
	boolean hasFile(String name);

	/**
	 * Get an existing file with the given name.
	 */
	CDStarFile getFile(String name) throws FileNotFound;

	/**
	 * Remove this snapshot.
	 */
	void remove();

	/**
	 * Return true if this snapshot no longer exists.
	 */
	boolean isRemoved();

}