package de.gwdg.cdstar.runtime.client.exc;

public class ProfileNotDefined extends ClientError {
	private static final long serialVersionUID = 4925623198110134542L;

	public ProfileNotDefined(String profileName) {
		super(profileName);
	}

	public String getName() {
		return getMessage();
	}

}
