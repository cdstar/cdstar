package de.gwdg.cdstar.runtime.client;

import java.util.Optional;

public final class CDStarAttributeHelper {

	public static Optional<String> splitSchemaName(String name) {
		final int i = name.indexOf(CDStarAttribute.SCHEMA_SEPARATOR);
		if (i > 0)
			return Optional.of(name.substring(0, i));
		return Optional.empty();
	}

	public static String splitAttributeName(String name) {
		final int i = name.indexOf(CDStarAttribute.SCHEMA_SEPARATOR);
		if (i > 0)
			return name.substring(i + 1);
		return name;
	}

	public static String join(String namespace, String name) {
		if (namespace == null)
			return name;
		return namespace + CDStarAttribute.SCHEMA_SEPARATOR + name;
	}

	public static String normalizeAttributeName(String name) {
		return name.toLowerCase();
	}

	public static boolean isValidAttributeName(String name) {
		return CDStarAttribute.NAME_PATTERN.matcher(name).matches();
	}

}
