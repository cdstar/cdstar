package de.gwdg.cdstar.runtime;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.Duration;
import java.util.AbstractMap;
import java.util.AbstractSet;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.regex.Pattern;

/**
 * A flat key/value based configuration with basic support for namespaced views
 * and tables.
 */
public interface Config {
	String SEP = ".";

	boolean hasKey(String key);

	String get(String key) throws ConfigException;

	/**
	 * Set a value, or remove the key if null is passed as a value.
	 */
	Config set(String key, String value);

	Set<String> keySet();

	/**
	 * Set a value, but only if it is not null and does not have a value yet.
	 *
	 * @param key   key to set. If already associated with a value, do nothing.
	 * @param value new value. If null, do nothing.
	 */
	default Config setDefault(String key, String value) {
		if (value != null && !hasKey(key))
			set(key, value);
		return this;
	}

	default String get(String key, String fallback) {
		try {
			return hasKey(key) ? get(key) : fallback;
		} catch (final ConfigException e) {
			return fallback;
		}
	}

	default Optional<String> getOptional(String key) {
		return Optional.ofNullable(get(key, null));
	}

	default int getInt(String key) throws ConfigException {
		try {
			return Integer.parseInt(get(key));
		} catch (final NumberFormatException e) {
			throw new ConfigException("Not an integer: " + key);
		}
	}

	default long getLong(String key) throws ConfigException {
		try {
			return Long.parseLong(get(key));
		} catch (final NumberFormatException e) {
			throw new ConfigException("Not a long: " + key);
		}
	}

	default double getDouble(String key) throws ConfigException {
		try {
			return Double.parseDouble(get(key));
		} catch (final NumberFormatException e) {
			throw new ConfigException("Not a double: " + key);
		}
	}

	default URI getURI(String key) throws ConfigException {
		try {
			return new URI(get(key));
		} catch (final URISyntaxException e) {
			throw new ConfigException("Not an URI: " + key);
		}
	}

	/**
	 * Parse the value as a {@link Duration} with a time unit suffix (ns, ms, s, m,
	 * h or d). If no time unit is specified, milliseconds are assumed.
	 */
	default Duration getDuration(String key) throws ConfigException {
		final String value = get(key).toLowerCase();
		try {
			if (value.endsWith("ns"))
				return Duration.ofNanos(Long.parseLong(value.substring(0, value.length() - 2)));
			if (value.endsWith("ms"))
				return Duration.ofMillis(Long.parseLong(value.substring(0, value.length() - 2)));
			if (value.endsWith("s"))
				return Duration.ofSeconds(Long.parseLong(value.substring(0, value.length() - 1)));
			if (value.endsWith("m"))
				return Duration.ofMinutes(Long.parseLong(value.substring(0, value.length() - 1)));
			if (value.endsWith("h"))
				return Duration.ofHours(Long.parseLong(value.substring(0, value.length() - 1)));
			if (value.endsWith("d"))
				return Duration.ofDays(Long.parseLong(value.substring(0, value.length() - 1)));
			return Duration.ofMillis(Long.parseLong(value));
		} catch (final NumberFormatException e) {
			throw new ConfigException("Not a duration: " + key);
		}

	}

	/**
	 * Return true if the value equals "true" (case insensitive). Any other value
	 * (or no value) will return false.
	 */
	default boolean getBool(String key) {
		return "true".equalsIgnoreCase(get(key, "false"));
	}

	Pattern splitPattern = Pattern.compile("\\s*,\\s*");

	default String[] getArray(String key) throws ConfigException {
		return splitPattern.split(get(key));
	}

	default List<String> getList(String key) throws ConfigException {
		return Arrays.asList(getArray(key));
	}

	/**
	 * Return a namespaced view of the original config. Only keys that start with
	 * the given namespace (separated by a dot) are included, and the namespace is
	 * transparently added or removed from key names when working with the view.
	 *
	 * Example: Given the key 'transport.http.port' and the namespace
	 * 'transport.http', the returned config will contain a key named 'port' which
	 * maps to 'transport.http.port'.
	 */
	Config with(String name);

	default Config with(String name, String... names) {
		Config view = with(name);
		for (final String n : names)
			view = view.with(n);
		return view;
	}

	/**
	 * A shortcut for <code>with(tableName).getTable()</code>.
	 */
	default Map<String, Config> getTable(String tableName) {
		if (tableName.isEmpty())
			return getTable();
		return with(tableName).getTable();
	}

	/**
	 * Group keys based on their first namespace element and return a map that maps
	 * unique namespace names to namespaced views of sub-keys. The imaginary key
	 * 'a.b.c' will end up in the 'a' group and the value will be a namespaces view
	 * created with <code>with('a')</code>.
	 *
	 * This can be used to extract lists or tables of structured data from an
	 * otherwise flat configuration. Keys should follow a 'rowName.columnName'
	 * scheme where 'columnName' may itself be a namespaced key.
	 *
	 * Example:
	 *
	 * <code>
	 *   cfg.set("users.bob.nickname", "Bob the Magnificent")
	 *   cfg.set("users.bob.password", "secret")
	 *   cfg.set("users.alice.nickname", "Super Alice")
	 *   cfg.set("users.alice.password", "supersecret")
	 *
	 *   Map&lt;String, Config&gt; users = cfg.with("users").getTable();
	 *   assert users.get("bob").get("nickname").equals("Bob the Magnificent")
	 * </code>
	 */
	default Map<String, Config> getTable() {
		final Map<String, Config> map = new HashMap<>();
		for (final String key : keySet()) {
			final int i = key.indexOf(SEP);
			if (i < 0)
				continue;
			map.computeIfAbsent(key.substring(0, i), k -> with(k));
		}
		return map;
	}

	/**
	 * Return the equivalent of {@link #toMap()} followed by a
	 * {@link Map#entrySet()}. The set and the entries are immutable, but reflect
	 * changes made to the underlying {@link Config}.
	 *
	 * @implNote The default implementation returns an unmodifiable
	 *           {@link AbstractSet} that uses {@link #keySet()} and
	 *           {@link #get(String, String)} to generate {@link Entry}s. It also
	 *           uses {@link #hasKey(String)}, {@link #size()} and
	 *           {@link #isEmpty()} internally, so make sure to not introduce
	 *           recursion loops when overriding these.
	 */
	default Set<Map.Entry<String, String>> entrySet() {
		return new AbstractSet<Map.Entry<String, String>>() {

			@Override
			public Iterator<Entry<String, String>> iterator() {
				return new Iterator<Map.Entry<String, String>>() {
					Iterator<String> keyIter = Config.this.keySet().iterator();

					@Override
					public boolean hasNext() {
						return keyIter.hasNext();
					}

					@Override
					public Entry<String, String> next() {
						final String currentKey = keyIter.next();
						return new Entry<String, String>() {
							@Override
							public String getKey() {
								return currentKey;
							}

							@Override
							public String getValue() {
								return Config.this.get(currentKey, null);
							}

							@Override
							public String setValue(String value) {
								final String old = Config.this.get(currentKey, null);
								Config.this.set(currentKey, value);
								return old;
							}
						};
					}
				};
			}

			@Override
			public int size() {
				return Config.this.size();
			}

			@Override
			public boolean isEmpty() {
				return Config.this.isEmpty();
			}

			@Override
			public boolean contains(Object key) {
				if (key instanceof String)
					return Config.this.hasKey((String) key);
				return false;
			}

		};
	}

	/**
	 * Return a map view of this {@link Config} object. The map is read-only
	 * (unmodifiable) but reflects changes made to the underlying {@link Config}.
	 *
	 * @implNote The default implementation returns an unmodifiable
	 *           {@link AbstractMap} that calls {@link #entrySet()},
	 *           {@link #keySet()}, {@link #hasKey(String)},
	 *           {@link #get(String, String)}, {@link #size()} and
	 *           {@link #isEmpty()} internally, so make sure to not introduce
	 *           recursion loops when overriding these.
	 */
	default Map<String, String> toMap() {
		return new AbstractMap<String, String>() {
			@Override
			public Set<Entry<String, String>> entrySet() {
				return Config.this.entrySet();
			}

			@Override
			public String get(Object key) {
				if (key instanceof String)
					return Config.this.get((String) key, null);
				return null;
			}

			@Override
			public Set<String> keySet() {
				return Config.this.keySet();
			}

			@Override
			public int size() {
				return Config.this.size();
			}

			@Override
			public boolean isEmpty() {
				return Config.this.isEmpty();
			}

			@Override
			public boolean containsKey(Object key) {
				if (key instanceof String)
					return Config.this.hasKey((String) key);
				return false;
			}
		};
	}

	default boolean isEmpty() {
		return size() == 0;
	}

	default int size() {
		return keySet().size();
	}

	default void setAll(Config from) {
		setAll(from.toMap());
	}

	default void setAll(Map<String, String> from) {
		for (final Map.Entry<String, String> e : from.entrySet()) {
			set(e.getKey(), e.getValue());
		}
	}

}