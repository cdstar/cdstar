package de.gwdg.cdstar.runtime.client.exc;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * This unchecked exception can be thrown by plugins to signal a constraint
 * violation, e.g. wrong values for metadata properties, limits, quotas or other
 * server-side enforced constraints.
 *
 * These usually produce 400 exceptions in the REST API.
 */
public class ConstraintError extends RuntimeException {
	private static final long serialVersionUID = 7354871320119964121L;
	private Map<String, String> details;

	public ConstraintError(String message) {
		super(message);
	}

	public ConstraintError(String message, Throwable cause) {
		super(message, cause);
	}

	public ConstraintError detail(String key, String value) {
		if (details == null)
			details = new HashMap<>();
		details.put(key, value);
		return this;
	}

	public Map<String, String> getDetails() {
		return details == null ? Collections.emptyMap() : details;
	}

}
