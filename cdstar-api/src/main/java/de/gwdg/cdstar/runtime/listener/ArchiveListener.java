package de.gwdg.cdstar.runtime.listener;

import java.util.List;
import java.util.Set;

import de.gwdg.cdstar.runtime.client.CDStarACLEntry;
import de.gwdg.cdstar.runtime.client.CDStarArchive;
import de.gwdg.cdstar.runtime.client.CDStarAttribute;
import de.gwdg.cdstar.runtime.client.CDStarFile;
import de.gwdg.cdstar.runtime.client.CDStarProfile;
import de.gwdg.cdstar.runtime.client.CDStarSnapshot;
import de.gwdg.cdstar.runtime.client.auth.ArchivePermission;

/**
 * Interface for plugins that want to listen for cdstar archive changes. The
 * callbacks are called synchronously immediately after the change happened.
 */
public interface ArchiveListener {

	/**
	 * Called after an archive was removed, even if it never existed outside of
	 * the current transaction in the first place.
	 */
	default void archiveRemoved(CDStarArchive archive) {
	}

	default void profileChanged(CDStarArchive archive, CDStarProfile oldProfile) {
	}

	default void ownerChanged(CDStarArchive archive, String oldOwner) {
	}

	default void aclChanged(CDStarArchive archive, CDStarACLEntry entry, Set<ArchivePermission> oldPermissions) {
	}

	default void propertyChanged(CDStarAttribute property, CDStarFile file, List<String> originalValues) {
	}

	default void fileCreated(CDStarFile file) {
	}

	default void fileNameChanged(CDStarFile file, String oldName) {
	}

	/**
	 *
	 * @param file
	 *            The file which was changed.
	 * @param bytes
	 *            Number of bytes written to this file. The number may be
	 *            negative if the file was truncated.
	 */
	default void fileSizeChanged(CDStarFile file, long bytes) {
	}

	default void fileRemoved(CDStarFile archiveFile) {
	}

	default void snapshotCreated(CDStarSnapshot snap) {
	}

	default void snapshotRemoved(CDStarSnapshot snap) {
	}

	default void snapshotProfileChanged(CDStarSnapshot snap, CDStarProfile oldProfile) {
	}

}
