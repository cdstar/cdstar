package de.gwdg.cdstar.runtime.client;

import de.gwdg.cdstar.runtime.client.auth.ArchivePermission;
import de.gwdg.cdstar.runtime.client.exc.ArchiveNotAvailable;

/**
 * An archive can be copied or moved to an external storage. Transitions are
 * usually triggered by profile changes or plugins.
 */
public interface CDStarMirrorState {

	/**
	 * Return true if the archive is mirrored.
	 */
	default boolean isMirrored() {
		return getMirrorName() != null;
	}

	/**
	 * Return the name of the mirror that holds a copy of the archive, or null
	 * if the archive is not mirrored.
	 */
	String getMirrorName();

	/**
	 * Return the mirror-specific location string that identifies the copy
	 * within the mirror.
	 */
	String getMirrorLocation();

	/**
	 * Set mirror location. Only allowed if no mirror is set.
	 * 
	 * Requires {@link ArchivePermission#CHANGE_MIRROR} permissions.
	 * 
	 * @throws IllegalStateException if {@link #isMirrored()} returns true
	 */
	void setMirror(String mirrorName, String location);

	/**
	 * Clear mirror location. Has no effect if no mirror is set.
	 * Only allowed on fully available archives.
	 * 
	 * Requires {@link ArchivePermission#CHANGE_MIRROR} permissions.
	 * 
	 * @throws ArchiveNotAvailable if {@link #isAvailable()} returns false
	 */
	void clearMirror();

	/**
	 * Return true if there is a pending migration running for this archive, for
	 * example, from one mirror to another, to or from a mirror.
	 *
	 * TODO: Add ways to inspect the running migration (start_date, started_by,
	 * progress, task_id)
	 */
	boolean isMigrationPending();

	/**
	 * Same as {@link #isMirrored()}
	 */
	@Deprecated
	default boolean hasExternalLocation() {
		return isMirrored();
	}

	/**
	 * Return true if this archive is fully readable. This may also be true for
	 * mirrored archive if data is still cached locally.
	 */
	boolean isAvailable();

	/**
	 * Return the current storage profile set for this archive.
	 */
	CDStarProfile getProfile();

	/**
	 * Change the current storage profile.
	 * 
	 * Requires {@link ArchivePermission#CHANGE_PROFILE} permissions.
	 */
	void setProfile(CDStarProfile profile);

}