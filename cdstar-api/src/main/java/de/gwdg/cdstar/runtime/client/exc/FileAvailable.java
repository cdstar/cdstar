package de.gwdg.cdstar.runtime.client.exc;

/**
 * Available files cannot be recovered.
 */
public class FileAvailable extends IllegalStateException {
	private static final long serialVersionUID = 5283169367870215874L;

	private final String vault;
	private final String archive;
	private final String name;

	public FileAvailable(String vault, String archive, String fileName) {
		super(vault + "/" + archive + "/" + fileName);
		this.vault = vault;
		this.archive = archive;
		this.name = fileName;
	}

	public String getVault() {
		return vault;
	}

	public String getArchive() {
		return archive;
	}

	public String getFile() {
		return name;
	}

}
