package de.gwdg.cdstar.runtime;

public interface Listenable<LisenerType> {
	void addListener(LisenerType lisneter);

}
