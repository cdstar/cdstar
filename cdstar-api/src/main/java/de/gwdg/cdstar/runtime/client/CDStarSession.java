package de.gwdg.cdstar.runtime.client;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import de.gwdg.cdstar.runtime.RuntimeContext;
import de.gwdg.cdstar.runtime.client.exc.VaultNotFound;
import de.gwdg.cdstar.runtime.listener.SessionListener;
import de.gwdg.cdstar.ta.TransactionInfo.Mode;
import de.gwdg.cdstar.ta.UserTransaction;
import de.gwdg.cdstar.ta.exc.TARollbackException;

public interface CDStarSession extends AutoCloseable {

	CDStarClient getClient();

	default RuntimeContext getRuntime() {
		return getClient().getRuntime();
	}

	/**
	 * Return a list of all vaults that are visible to this client
	 */
	List<String> getVaultNames();

	/**
	 * Return a specific vault, visible or not.
	 *
	 * @throws VaultNotFound
	 */
	CDStarVault getVault(String vaultName) throws VaultNotFound;

	void addListener(SessionListener listener);

	String getSessionId();

	void commit() throws TARollbackException;

	void rollback(Throwable reason);

	default void rollback() {
		rollback(null);
	}

	@Override
	void close();

	boolean isClosed();

	/**
	 * Set a new timeout (in milliseconds) and return the old value.
	 *
	 * Sessions have a default timeout of -1, which means they never expire. An
	 * expired session is closed by the runtime at some point after the timeout
	 * passed.
	 *
	 * @param millis
	 *            New timeout in milliseconds.
	 * @return Old value
	 */
	long setTimeout(long millis);

	/**
	 * Disable (or re-enable) timeout processing for this session. Useful for tasks
	 * that take a long time. Re-enabling the timeout (passing false) will also
	 * refresh the timeout.
	 */
	void setTimeoutSuspended(boolean suspend);

	boolean isTimeoutSuspended();

	/**
	 * Return the timeout value, which might be -1 for sessions that never expire.
	 */
	long getTimeout();

	/**
	 * Return the remaining time in milliseconds, which is 0 for expired sessions
	 * and -1 for sessions that do not expire, either because they have an infinite
	 * timeout or the timeout is suspended.
	 */
	long getRemainingTime();

	/**
	 * Extend the lifetime of this session by applying the last timeout again.
	 */
	default long refreshTimeout() {
		return setTimeout(getTimeout());
	}

	/**
	 * Check if this session is expired.
	 *
	 * @return True if session expired and may be closed at any time.
	 */
	default boolean isExpired() {
		return getRemainingTime() == 0;
	}

	boolean isReadOnly();

	void setMode(Mode mode);

	Mode getMode();

	Collection<CDStarVault> listOpenedVaults();

	/**
	 * Return a session-bound context that can be used to store short-lived
	 * (session-scoped) information. The returned map is thread-safe.
	 */
	Map<String, Object> getContext();

	/**
	 * Return the transacton this session is bound to.
	 */
	UserTransaction getUserTransaction();

}
