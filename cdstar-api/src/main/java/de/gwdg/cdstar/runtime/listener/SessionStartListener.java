package de.gwdg.cdstar.runtime.listener;

import de.gwdg.cdstar.runtime.client.CDStarSession;

public interface SessionStartListener {

	default void onSessionStarted(CDStarSession session) {
	}

	static SessionStartListener wrap(SessionListener listener) {
		return new SessionStartListener() {
			@Override
			public void onSessionStarted(CDStarSession session) {
				session.addListener(listener);
			}
		};
	}

}
