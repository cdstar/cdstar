package de.gwdg.cdstar.runtime.client.auth;

import java.util.Arrays;
import java.util.Collection;
import java.util.EnumSet;
import java.util.Iterator;
import java.util.Set;

/**
 * Permission-sets bundle common groups of permissions that are usually granted
 * together.
 *
 * In principle, permission sets are very similar to 'roles' in traditional
 * authorization, but named differently to avoid confusion with user-assigned
 * roles or groups.
 *
 * Rationale: Permissions are very fine grained and some combinations of
 * permissions do not make much sense. For example, a client would only be able
 * to excel the READ_FILES permission if it is allowed to list the available
 * files first, which requires LIST_FILES. The permissions READ_FILES and
 * LIST_FILES are still separate, because LIST_FILES might be useful on its own.
 * Permissions (by definition) never imply each other. This is where
 * permission-sets come in handy.
 *
 * Using predefined permission sets instead of individual permissions helps
 * preventing inconsistent permission sets and helps reasoning about permissions
 * on a more intuitive way.
 *
 * Some notes:
 *
 * User defined permission sets may be added in the future, but these would be
 * statically configured and not change at runtime.
 *
 * Predefined permission sets may be, but don't have to be represented by roles
 * in the global authentication mechanism. If they are, it is recommended to
 * keep them in sync to avoid confusion. For example, the "cdstar_manage" role
 * should grant the same permissions as the permission set with the
 * corresponding name.
 *
 * The 'MANAGE' set is intended for management and reporting jobs. These are
 * usually only interested in the meta-data of an archive, not the content. The
 * set therefore inherits LIST instead of READ or even WRITE to protect user
 * data per default. While accounts with this permission set would be able to
 * grant more permissions to themselves, these changes would show up in audit
 * logs and be accountable.
 *
 * READ, WRITE and MANAGE reassemble the permissions defined in cdstar-v2.
 *
 * The OWNER set is commonly used on vault-level (global or default permissions)
 * and assigned to the current owner of an archive, but has no special meaning
 * or semantics. The 'owner' property of an archive does not imply OWNER
 * permissions. In fact, a vault might be configured in a way that the OWNER
 * rights can be revoked while keeping the owner property of an archive intact.
 * This is a common use case for publishing, where an archive should not be
 * writable after a certain point (archive locking).
 *
 */
public enum ArchivePermissionSet implements Iterable<ArchivePermission> {

	/* Common user permissions */
	LIST(ArchivePermission.LOAD, ArchivePermission.LIST_FILES),
	READ(LIST, ArchivePermission.READ_FILES, ArchivePermission.READ_META),
	WRITE(READ, ArchivePermission.CHANGE_FILES, ArchivePermission.CHANGE_META),
	OWNER(WRITE, ArchivePermission.READ_ACL, ArchivePermission.CHANGE_ACL, ArchivePermission.DELETE,
			ArchivePermission.CHANGE_PROFILE,
			ArchivePermission.SNAPSHOT),
	/* System account permission sets */
	MANAGE(LIST, ArchivePermission.CHANGE_OWNER, ArchivePermission.READ_ACL, ArchivePermission.CHANGE_ACL,
			ArchivePermission.CHANGE_PROFILE,
			ArchivePermission.SNAPSHOT),
	ADMIN(ArchivePermission.values());

	final EnumSet<ArchivePermission> permissions = EnumSet.noneOf(ArchivePermission.class);

	ArchivePermissionSet(ArchivePermission... grants) {
		permissions.addAll(Arrays.asList(grants));
	}

	ArchivePermissionSet(ArchivePermissionSet base, ArchivePermission... grants) {
		permissions.addAll(base.getPermissions());
		permissions.addAll(Arrays.asList(grants));
	}

	public EnumSet<ArchivePermission> getPermissions() {
		return permissions;
	}

	@Override
	public Iterator<ArchivePermission> iterator() {
		return permissions.iterator();
	}

	@Override
	public String toString() {
		return name().toUpperCase();
	}

	/**
	 * Group a bunch of {@link ArchivePermission}s into
	 * {@link ArchivePermissionSet}s. The result may contain partially
	 * overlapping sets (e.g. MANAGE and OWNER), but will skip sets completely
	 * included in others (e.g. for LIST and READ, only READ will be returned).
	 * Permissions that do not fit into any complete set are ignored.
	 */
	public static Set<ArchivePermissionSet> group(Collection<ArchivePermission> grants) {
		final EnumSet<ArchivePermissionSet> result = EnumSet.noneOf(ArchivePermissionSet.class);
		for (final ArchivePermissionSet set : ArchivePermissionSet.values()) {
			if (grants.containsAll(set.getPermissions())) {
				// Remove sets that are fully contained within this new set.
				result.removeIf(s -> set.getPermissions().containsAll(s.getPermissions()));
				result.add(set);
			}
		}
		return result;
	}

	/**
	 * Return all unique {@link ArchivePermission}s included in a collection of
	 * {@link ArchivePermissionSet}s.
	 */
	public static Set<ArchivePermission> explode(Collection<ArchivePermissionSet> sets) {
		final EnumSet<ArchivePermission> result = EnumSet.noneOf(ArchivePermission.class);
		sets.forEach(s -> result.addAll(s.permissions));
		return result;
	}

}