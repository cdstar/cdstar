package de.gwdg.cdstar.runtime.client;

import java.util.Collection;

import de.gwdg.cdstar.runtime.client.auth.StringSubject;
import de.gwdg.cdstar.runtime.client.auth.StringSubject.GroupSubject;
import de.gwdg.cdstar.runtime.client.auth.StringSubject.PrincipalSubject;

public interface CDStarACL {

	Collection<CDStarACLEntry> getAccessList();

	CDStarACLEntry forSubject(StringSubject subject);

	default CDStarACLEntry forPrincipal(String name) {
		return forSubject(new PrincipalSubject(name));
	}

	default CDStarACLEntry forGroup(String name) {
		return forSubject(new GroupSubject(name));
	}

	default CDStarACLEntry forKnown() {
		return forSubject(StringSubject.SpecialSubject.USER);
	}

	default CDStarACLEntry forAny() {
		return forSubject(StringSubject.SpecialSubject.ANY);
	}

	default CDStarACLEntry forOwner() {
		return forSubject(StringSubject.SpecialSubject.OWNER);
	}

}
