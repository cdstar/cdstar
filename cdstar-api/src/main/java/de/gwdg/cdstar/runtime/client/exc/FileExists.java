package de.gwdg.cdstar.runtime.client.exc;

import java.io.IOException;

public class FileExists extends IOException {

	private static final long serialVersionUID = -1502911033939475900L;
	private final String name;
	private final String conflict;

	public FileExists(String name) {
		super("File exists: " + name);
		conflict = this.name = name;
	}

	public FileExists(String name, String conflict) {
		super("File name '" + name + "' conflicts with: " + conflict);
		this.name = name;
		this.conflict = conflict;
	}

	public String getName() {
		return name;
	}

	public String getConflict() {
		return conflict;
	}

}
