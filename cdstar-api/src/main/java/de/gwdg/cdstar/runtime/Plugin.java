package de.gwdg.cdstar.runtime;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * Annotation used by the plugin loader to find plugins on the class path.
 *
 * The classpath-scan can be avoided by referencing all plugins by their full
 * class name.
 */
@Documented
@Retention(RUNTIME)
@Target(TYPE)
public @interface Plugin {
	/**
	 * An optional plugin name. If set, it must be unique across all plugins on
	 * the classpath.
	 *
	 * The name can be used to load plugins as an alternative to their simple or
	 * canonial class name.
	 */
	String[] name() default {};
}
