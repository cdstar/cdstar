package de.gwdg.cdstar.runtime.client.exc;

public class VaultNotFound extends ClientError {
	private static final long serialVersionUID = 4925623198110134542L;

	public VaultNotFound(String vaultName) {
		super(vaultName);
	}

	public String getName() {
		return getMessage();
	}

}
