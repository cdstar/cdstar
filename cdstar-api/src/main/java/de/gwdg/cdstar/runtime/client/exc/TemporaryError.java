package de.gwdg.cdstar.runtime.client.exc;

/**
 * This unchecked exception can be thrown by plugins to signal a failing or
 * misbehaving back-end service (e.g. database connection problems).
 *
 * These usually results in a "503 Service Unavailable" REST API response and
 * the client is instructed to repeat the request (or the entire transaction in
 * case it was a transaction-bound request). The message of this exception is
 * NOT visible to the user.
 */
public class TemporaryError extends RuntimeException {
	private static final long serialVersionUID = -7239891696260743016L;

	public TemporaryError(String message, Throwable cause) {
		super(message, cause);
	}

	public TemporaryError(Throwable e) {
		super(e);
	}

}
