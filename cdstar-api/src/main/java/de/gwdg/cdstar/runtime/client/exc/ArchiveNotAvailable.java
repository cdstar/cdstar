package de.gwdg.cdstar.runtime.client.exc;

import de.gwdg.cdstar.runtime.client.CDStarArchive;

/**
 * This unchecked exception is thrown if an operation is not allowed due to the
 * archive being in an unavailable storage state.
 */
public class ArchiveNotAvailable extends IllegalStateException {
	private static final long serialVersionUID = 5283169367870215874L;

	private final String vault;
	private final String archive;

	public ArchiveNotAvailable(CDStarArchive archive) {
		this(archive.getVault().getName(), archive.getId());
	}

	public ArchiveNotAvailable(String vault, String archive) {
		super(vault + "/" + archive);
		this.vault = vault;
		this.archive = archive;
	}

	public String getVault() {
		return vault;
	}

	public String getArchive() {
		return archive;
	}

}
