package de.gwdg.cdstar.runtime;

import java.io.Closeable;
import java.io.IOException;
import java.nio.file.Path;
import java.util.List;
import java.util.Optional;

import de.gwdg.cdstar.auth.Credentials;
import de.gwdg.cdstar.auth.Subject;
import de.gwdg.cdstar.auth.error.LoginFailed;
import de.gwdg.cdstar.runtime.client.CDStarClient;
import de.gwdg.cdstar.runtime.client.CDStarSession;
import de.gwdg.cdstar.runtime.listener.RuntimeListener;

public interface RuntimeContext extends Closeable {

	/**
	 * Register one of the supported extension interfaces.
	 */
	void register(Object instance);

	/**
	 * Returns the first element of {@link #lookupAll(Class)}, or null if no
	 * matching service is found.
	 */
	default <T> Optional<T> lookup(Class<T> lookupClass) {
		final List<T> results = lookupAll(lookupClass);
		if (results.isEmpty())
			return Optional.empty();
		return Optional.of(results.get(0));
	}

	/**
	 * Same as {@link #lookup(Class)}, but raise a {@link RuntimeException} if no
	 * such service can be found.
	 */
	default <T> T lookupRequired(Class<T> lookupClass) {
		return lookup(lookupClass)
			.orElseThrow(() -> new RuntimeException("Required service not found: " + lookupClass.getName()));
	}

	/**
	 * Lookup services by class. This is only available after the initialization
	 * phase. If the runtime is starting up and a requested service implements
	 * {@link RuntimeListener}, it is started before it is returned.
	 */
	<T> List<T> lookupAll(Class<T> lookupClass);

	CDStarSession resumeSession(String tx);

	default CDStarClient getClient(Credentials creds) throws LoginFailed {
		final Subject subject = createSubject();
		if (creds != null)
			subject.login(creds);
		return getClient(subject);
	}

	CDStarClient getClient(Subject subject);

	/**
	 * Return a new (unauthorized) subject.
	 */
	Subject createSubject();

	Config getConfig();

	/**
	 * Return a directory that can be used by a service to persist its own files.
	 * This is usually <code>${path.home}/var/${serviceName}</code>
	 */
	Path getServiceDir(String serviceName) throws IOException;

	/**
	 * Get a subject that is already logged in as a named system user. The user is
	 * created if it does not already exist. By default, system users are configured
	 * with a wildcard permission and no password (to prevent external logins). They
	 * can be used by plugins to perform actions that are not allowed for the
	 * currently logged in user. Use with care.
	 */
	Subject getSystemUser(String name);

	@Override
	void close();

}
