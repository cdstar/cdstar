package de.gwdg.cdstar.runtime.client.exc;

import de.gwdg.cdstar.auth.Permission;

/**
 * Thrown then the desired operation is not permitted due to access
 * restrictions.
 *
 * This is a runtime exception because it may be thrown by virtually any API
 * interaction and should be handled on a higher level.
 */
public class AccessError extends RuntimeException {
	private static final long serialVersionUID = 7354871320119964121L;

	public AccessError() {
		super("Access denied");
	}

	public AccessError(String permissionName) {
		super(permissionName);
	}

	public AccessError(Permission sp) {
		this(sp.toString());
	}

}
