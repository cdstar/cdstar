package de.gwdg.cdstar.runtime.listener;

import de.gwdg.cdstar.runtime.client.CDStarArchive;

public interface VaultListener {
	/**
	 * Called after a new archive was created or an existing archives was loaded
	 * from a vault. This is usually the right place to attach
	 * {@link ArchiveListener}s to listen for archive specific events.
	 */
	default void archiveLoaded(CDStarArchive archive) {
	}

	/**
	 * Create a {@link VaultListener} that installs an {@link ArchiveListener}
	 * to all archives.
	 */
	public static VaultListener wrap(ArchiveListener listener) {
		return new VaultListener() {
			@Override
			public void archiveLoaded(CDStarArchive archive) {
				archive.addListener(listener);
			}
		};
	}

}
