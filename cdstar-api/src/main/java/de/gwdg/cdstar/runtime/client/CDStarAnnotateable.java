package de.gwdg.cdstar.runtime.client;

import java.util.Set;
import java.util.stream.Collectors;

public interface CDStarAnnotateable {

	/**
	 * Get or create an attribute by its field name.
	 */
	CDStarAttribute getAttribute(String name);

	/**
	 * Shortcut for <code>getAttribute(name).set(values)</code>
	 */
	default void setAttribute(String name, String... values) {
		getAttribute(name).set(values);
	}

	/**
	 * Return all non-empty attributes currently defined on this item.
	 */
	Set<CDStarAttribute> getAttributes();

	default Set<String> getAttributeNames() {
		return getAttributes().stream().filter(p -> !p.isEmpty()).map(p -> p.getName()).collect(Collectors.toSet());
	}

}
