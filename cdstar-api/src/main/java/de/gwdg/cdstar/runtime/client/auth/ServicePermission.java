package de.gwdg.cdstar.runtime.client.auth;

import de.gwdg.cdstar.auth.StringPermission;

public enum ServicePermission {
	CREATE_VAULT, // Create new vaults at runtime
	HEALTH, // read health status details and metrics
	SHUTDOWN, // Gracefully stop the entire service
	;

	private StringPermission perm;

	private ServicePermission() {
		perm = StringPermission.ofParts("service", name().toLowerCase());
	}

	public StringPermission asStringPermission() {
		return perm;
	}

}