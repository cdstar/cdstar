package de.gwdg.cdstar.runtime.client.utils;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Function;

import de.gwdg.cdstar.runtime.client.CDStarArchive;
import de.gwdg.cdstar.runtime.client.CDStarAttribute;
import de.gwdg.cdstar.runtime.client.CDStarFile;
import de.gwdg.cdstar.runtime.client.CDStarMirrorState;
import de.gwdg.cdstar.runtime.client.CDStarProfile;
import de.gwdg.cdstar.runtime.client.CDStarSnapshot;
import de.gwdg.cdstar.runtime.client.CDStarVault;
import de.gwdg.cdstar.runtime.client.exc.FileNotFound;

/**
 * Helper class to provide common functionality for {@link CDStarArchive} and
 * {@link CDStarSnapshot}.
 */
public class ArchiveOrSnapshot {

	private CDStarArchive archive;
	private CDStarSnapshot snapshot;

	public ArchiveOrSnapshot(CDStarSnapshot snapshot) {
		this(snapshot.getSource(), snapshot);
	}

	public ArchiveOrSnapshot(CDStarArchive archive) {
		this(archive, null);
	}

	public ArchiveOrSnapshot(CDStarArchive archive, CDStarSnapshot snapshot) {
		this.archive = archive;
		this.snapshot = snapshot;
	}

	public boolean isSnapshot() {
		return snapshot != null;
	}

	/**
	 * Return the archive, or the snapshot source archive.
	 */
	public CDStarArchive getSourceArchive() {
		return archive;
	}

	public Optional<CDStarArchive> asArchive() {
		return isSnapshot() ? Optional.empty() : Optional.of(archive);
	}

	public Optional<CDStarSnapshot> asSnapshot() {
		return Optional.ofNullable(snapshot);
	}

	public <T> T map(Function<CDStarArchive, T> onArchive, Function<CDStarSnapshot, T> onSnapshot) {
		return isSnapshot() ? onSnapshot.apply(snapshot) : onArchive.apply(archive);
	}

	public void apply(Consumer<CDStarArchive> onArchive, Consumer<CDStarSnapshot> onSnapshot) {
		if (isSnapshot())
			onSnapshot.accept(snapshot);
		else
			onArchive.accept(archive);
	}

	public String getId() {
		return map(CDStarArchive::getId, CDStarSnapshot::getId);
	}

	public String getRev() {
		return map(CDStarArchive::getRev, CDStarSnapshot::getRevision);
	}

	public CDStarVault getVault() {
		return archive.getVault();
	}

	@Deprecated
	public CDStarProfile getProfile() {
		return getMirrorState().getProfile();
	}

	@Deprecated
	public void setProfile(CDStarProfile newProfile) {
		getMirrorState().setProfile(newProfile);
	}

	@Deprecated
	public boolean isAvailable() {
		return getMirrorState().isAvailable();
	}

	public Date getCreated() {
		return map(CDStarArchive::getCreated, CDStarSnapshot::getCreated);
	}

	public Date getModified() {
		return map(CDStarArchive::getModified, CDStarSnapshot::getModified);
	}

	public List<CDStarFile> getFiles() {
		return map(CDStarArchive::getFiles, CDStarSnapshot::getFiles);
	}

	public int getFileCount() {
		return map(CDStarArchive::getFileCount, CDStarSnapshot::getFileCount);
	}

	public CDStarFile getFile(String name) throws FileNotFound {
		return isSnapshot() ? snapshot.getFile(name) : archive.getFile(name);
	}

	public CDStarMirrorState getMirrorState() {
		return map(CDStarArchive::getMirrorState, CDStarSnapshot::getMirrorState);
	}

	public Set<CDStarAttribute> getAttributes() {
		return map(CDStarArchive::getAttributes, CDStarSnapshot::getAttributes);
	}

	public CDStarAttribute getAttribute(String name) {
		return map(a -> a.getAttribute(name), s -> s.getAttribute(name));
	}

	public void remove() {
		apply(CDStarArchive::remove, CDStarSnapshot::remove);
	}

	@Override
	public String toString() {
		return map(CDStarArchive::toString, CDStarSnapshot::toString);
	}
}
