package de.gwdg.cdstar.runtime;

public class ConfigException extends Exception {
	private static final long serialVersionUID = -9067834703380142085L;

	public ConfigException(String message) {
		super(message);
	}

	public ConfigException(String message, Throwable throwable) {
		super(message, throwable);
	}

}
