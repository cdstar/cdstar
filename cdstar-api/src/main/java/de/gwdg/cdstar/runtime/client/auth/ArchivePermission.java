package de.gwdg.cdstar.runtime.client.auth;

import de.gwdg.cdstar.auth.StringPermission;

public enum ArchivePermission {
	/* Basic archive access */
	LOAD,
	DELETE,
	/* Access control */
	READ_ACL,
	CHANGE_ACL,
	CHANGE_OWNER,
	/* Property access */
	READ_META,
	CHANGE_META,
	/* Content access */
	LIST_FILES,
	READ_FILES,
	CHANGE_FILES,
	/* Archive state control */
	CHANGE_PROFILE,
	CHANGE_MIRROR,
	/* CHANGE_CLASS, */
	SNAPSHOT,
	TRIM,
	;

	@Override
	public String toString() {
		return name().toLowerCase();
	}

	public StringPermission toStringPermission(String vault, String archive) {
		return StringPermission.ofParts("archive", vault, archive, name().toLowerCase());
	}

}