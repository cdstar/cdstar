package de.gwdg.cdstar.runtime.listener;

import de.gwdg.cdstar.runtime.RuntimeContext;

public interface RuntimeListener {
	/**
	 * Called as soon as this listener is registered to a runtime. This is the right
	 * place to read and validate (static) configuration or register additional
	 * services. Implementations should not do any actual work here and prevent side
	 * effects as much as possible, as this phase is also triggered by runtimes that
	 * are never started (e.g. during service discovery for the the command line
	 * utility) so the {@link #onShutdown()} callback is not triggered and no
	 * resources can be cleaned up.
	 *
	 * @throws Exception Any exception thrown will abort the startup-process and
	 *                   stop/shutdown the runtime.
	 */
	default void onInit(RuntimeContext ctx) throws Exception {
	}

	/**
	 * Called during runtime start-up phase. This is the right place to create files
	 * or directories, connect to databases, get other services from the runtime or
	 * start threads.
	 * 
	 * Other services retrieved via {@link RuntimeContext#lookup(Class)} or similar
	 * means will be started as soon as they are accessed for the first time.
	 *
	 * @throws Exception Any exception thrown will abort the startup-process and
	 *                   stop/shutdown the runtime immediately.
	 */
	default void onStartup(RuntimeContext ctx) throws Exception {
	}

	/**
	 * Called during runtime shut-down phase. This is only called for listeners for
	 * which {@link #onStartup(RuntimeContext)} was called and did not throw an
	 * exception. 
	 *
	 * Do not block longer than absolutely necessary in this method.
	 *
	 * Any exceptions thrown are logged but otherwise ignored.
	 */
	default void onShutdown(RuntimeContext ctx) {
	}

}
