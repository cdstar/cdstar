package de.gwdg.cdstar.runtime;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Immutable representation of a vault configuration.
 */
public interface VaultConfig {

	String getName();

	boolean isPublic();

	default List<String> getPropertyNames() {
		return new ArrayList<>(getPropertyMap().keySet());
	}

	default String getProperty(String name) {
		return getPropertyMap().get(name);
	}

	Map<String, String> getPropertyMap();

}
