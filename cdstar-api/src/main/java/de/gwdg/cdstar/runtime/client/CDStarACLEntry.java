package de.gwdg.cdstar.runtime.client;

import java.util.EnumSet;
import java.util.Set;

import de.gwdg.cdstar.runtime.client.auth.ArchivePermission;
import de.gwdg.cdstar.runtime.client.auth.ArchivePermissionSet;
import de.gwdg.cdstar.runtime.client.auth.StringSubject;

public interface CDStarACLEntry {

	default void permit(ArchivePermissionSet permissionSet) {
		permissionSet.forEach(this::permit);
	}

	StringSubject getSubject();

	/**
	 * Return the set of permissions granted to this subject directly in the ACL.
	 * This set does not include permissions granted by external realms.
	 */
	Set<ArchivePermission> getPermissions();

	void permit(ArchivePermission permit);

	void revoke(ArchivePermission permit);

	/**
	 * Return true if no permissions are granted by this entry.
	 */
	default boolean isEmpty() {
		return getPermissions().isEmpty();
	}

	default void revokeAll() {
		EnumSet.copyOf(getPermissions()).forEach(this::revoke);
	}

	default void revoke(ArchivePermissionSet permit) {
		permit.forEach(this::revoke);
	}

	/**
	 * Return true if all of the given permissions are granted to the subject.
	 */
	default boolean isPermitted(Set<ArchivePermission> permits) {
		for (final ArchivePermission p : permits) {
			if (!isPermitted(p))
				return false;
		}
		return true;
	}

	default boolean isPermitted(ArchivePermission permit) {
		return getPermissions().contains(permit);
	}

	default boolean isPrincipalSubject() {
		return getSubject() instanceof StringSubject.SpecialSubject;
	}

	default boolean isKnownSubject() {
		return getSubject().equals(StringSubject.SpecialSubject.USER);
	}

	default boolean isAnonSubject() {
		return getSubject().equals(StringSubject.SpecialSubject.ANY);
	}

	default boolean isOwnerSubject() {
		return getSubject().equals(StringSubject.SpecialSubject.OWNER);
	}

	default boolean isGroupSubject() {
		return getSubject() instanceof StringSubject.GroupSubject;
	}

	default String getGroupName() {
		return isGroupSubject() ? getSubject().getName() : null;
	}

}
