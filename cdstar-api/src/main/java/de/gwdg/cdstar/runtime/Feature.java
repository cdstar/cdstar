package de.gwdg.cdstar.runtime;

import java.util.ServiceLoader;

/**
 * During early initialization phase, the cdstar runtime uses a
 * {@link ServiceLoader} to find implementations of this interface and calls the
 * {@link #configure(RuntimeContext)} method. This happens before any explicitly
 * defined plugins or realms are loaded and can be used as a hook to
 * auto-register services or perform other early initialization tasks.
 *
 * Features discovered this way are NOT registered with the runtime, but they
 * might do this themselves within the {@link #configure(RuntimeContext)}
 * method. Registering a feature that does not implement any other supported
 * interfaces has no effect.
 */

public interface Feature {
	void configure(RuntimeContext runtime);
}
