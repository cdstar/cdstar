package de.gwdg.cdstar.runtime.client;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import de.gwdg.cdstar.runtime.client.auth.ArchivePermission;
import de.gwdg.cdstar.runtime.client.exc.AccessError;
import de.gwdg.cdstar.runtime.client.exc.FileExists;
import de.gwdg.cdstar.runtime.client.exc.FileNotFound;
import de.gwdg.cdstar.runtime.client.exc.InvalidFileName;
import de.gwdg.cdstar.runtime.client.exc.InvalidSnapshotName;
import de.gwdg.cdstar.runtime.listener.ArchiveListener;

public interface CDStarArchive extends CDStarAnnotateable {

	String getId();

	CDStarVault getVault();

	/**
	 * Add an archive listener that is only active for the lifetime of this archive.
	 */
	void addListener(ArchiveListener listener);

	/**
	 * Returns the session this archive was created in or loaded from. The session
	 * may be closed already.
	 */
	default CDStarSession getSession() {
		return getVault().getSession();
	}

	/**
	 * Return the revision string. The revision changes every time the archive is
	 * committed with changed content (files or metadata). Other modifications (acl,
	 * profiles, settings) do not result in a new revision.
	 *
	 * For new archives, this method returns the same value as {@link #getNextRev()}
	 * and will never return null.
	 */
	String getRev();

	/**
	 * Return parent revision, or null if this archive is the first revision and has
	 * no parent.
	 */
	String getParentRev();

	/**
	 * Return the next revision this archive will have after a successful commit.
	 * Note that the revision is only updated if {@link #isContentModified()}
	 * returns true. Otherwise, this method returns the same value as
	 * {@link #getRev()}.
	 */
	String getNextRev();

	/**
	 * Same as {@link CDStarMirrorState#getProfile()}
	 */
	@Deprecated
	default CDStarProfile getProfile() {
		return getMirrorState().getProfile();
	}

	/**
	 * Same as {@link CDStarMirrorState#setProfile(CDStarProfile)}
	 **/
	@Deprecated
	default void setProfile(CDStarProfile profile) {
		getMirrorState().setProfile(profile);
	}

	/**
	 * Same as {@link CDStarMirrorState#isAvailable()}
	 */
	@Deprecated
	default boolean isAvailable() {
		return getMirrorState().isAvailable();
	}

	/**
	 * Return true if the archive is locked (write protected). The locking only
	 * affects the payload of the archive (files and meta-attributes), not the
	 * administrative attributes (owner, ACLs, ...).
	 */
	boolean isLocked();

	/**
	 * Return the archives mirror state.
	 */
	CDStarMirrorState getMirrorState();

	/**
	 * Return <code>true</code> if this archive was modified during this session.
	 */
	boolean isModified();

	/**
	 * Return <code>true</code> if the archive content (files or annotations) was
	 * modified during this session and the archive will get a new revision after
	 * commit (see {@link #getNextRev()}.
	 */
	boolean isContentModified();

	/**
	 * Return the date of archive creation.
	 */
	Date getCreated();

	/**
	 * Return the date of the last successful commit. Other than
	 * {@link #getContentModified()} this also includes commits that changed
	 * administrative meta-data or other aspects of the archive not usually
	 * accessible to the end user.
	 *
	 * For new archives, the value of {@link #getCreated()} is returned instead.
	 */
	Date getModified();

	/**
	 * Return the date of the last successful commit that updated the revision id.
	 * For new archives, the value of {@link #getCreated()} is returned instead.
	 */
	Date getContentModified();

	CDStarACL getACL();

	/**
	 * Get a list of all attached files in this archive.
	 */
	List<CDStarFile> getFiles();

	/**
	 * Get file count without read_files permission.
	 */
	int getFileCount();

	/**
	 * Return true if the file exists.
	 */
	boolean hasFile(String name);

	default CDStarFile getOrCreateFile(String name) throws InvalidFileName {
		try {
			return hasFile(name) ? getFile(name) : createFile(name);
		} catch (FileNotFound | FileExists e) {
			// This should not be possible.
			throw new RuntimeException(e);
		}
	}

	/**
	 * Get an existing file with the given name.
	 *
	 * @throws FileNotFound
	 */
	CDStarFile getFile(String name) throws FileNotFound;

	/**
	 * Create a new file with the given name and
	 * <code>application/octet-stream</code> default media type. See
	 * {@link #createFile(String, String)}.
	 *
	 */
	default CDStarFile createFile(String name) throws FileExists, InvalidFileName {
		return createFile(name, "application/octet-stream");
	}

	/**
	 * Create a new file with the given name and media type.
	 *
	 * @param name      of file. May be null to create a temporary file.
	 * @param metiaType of file. May be null to default to
	 *                  'application/octet-stream'.
	 * @return Newly created file handle.
	 * @throws FileExists      if a file with this name exists, or if the name
	 *                         conflicts with another existing file.
	 * @throws InvalidFileName if the filename is malformed.
	 * @throws AccessError
	 */
	CDStarFile createFile(String name, String metiaType) throws FileExists, InvalidFileName;

	/**
	 * Return the string representation of the owner of this archive.
	 */
	String getOwner();

	/**
	 * Change the owner of an archive. The new value is not verified against a
	 * realm, so take special care to not set an invalid or unknown owner string.
	 */
	void setOwner(String string);

	/**
	 * Remove this archive (turn it into a tombstone).
	 *
	 * This does not remove or delete any files or metadata, so the last state of
	 * the archive is preserved until the tombstone is garbage collected.
	 *
	 * @throws AccessError
	 */
	void remove();

	/**
	 * Return true if this archive no longer exists.
	 */
	boolean isRemoved();

	/**
	 * Get the value of a system property, or null if the property is undefined.
	 */
	String getProperty(String name);

	default String getProperty(String name, String defaultValue) {
		final String value = getProperty(name);
		return value != null ? value : defaultValue;
	}

	/**
	 * Set the value of a system property. If the value is null, the property is
	 * removed.
	 */
	void setProperty(String name, String value);

	/**
	 * Return a list of snapshots that exist for this archive, ordered by the time
	 * they were created (old to new).
	 */
	List<CDStarSnapshot> getSnapshots();

	/**
	 * Return a specific named snapshot, if it exists.
	 */
	default Optional<CDStarSnapshot> getSnapshot(String name) {
		return getSnapshots().stream().filter(s -> s.getName().equals(name)).findFirst();
	}

	/**
	 * Create a new snapshot. This is only possible on archives with unmodified
	 * content ({@link #isContentModified()} must return false) and requires
	 * {@link ArchivePermission#SNAPSHOT} permissions. The snapshot name must be
	 * unique and should be short and simple (like a version number). Only ASCII
	 * letters, numbers, '-' and '.' are allowed for now.
	 *
	 * Note that this operation may block for a while if a large snapshot is
	 * created, depending on the backing storage.
	 *
	 * @throws InvalidSnapshotName   if the supplied name contains invalid
	 *                               charakters or is already taken
	 * @throws IllegalStateException if the archive is modified
	 */
	CDStarSnapshot createSnapshot(String name) throws InvalidSnapshotName;

}
