package de.gwdg.cdstar.runtime.client.exc;

import java.io.IOException;

public class FileNotFound extends IOException {
	private static final long serialVersionUID = -2229561630193393899L;
	private final String name;
	private final String archive;
	private final String vault;

	public FileNotFound(String vault, String archive, String name) {
		super(vault + "/" + archive + "/" + name);
		this.vault = vault;
		this.archive = archive;
		this.name = name;
	}

	public String getVault() {
		return vault;
	}

	public String getArchive() {
		return archive;
	}

	public String getName() {
		return name;
	}

}
