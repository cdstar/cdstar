package de.gwdg.cdstar.runtime.client.exc;

public class ArchiveNotFound extends ClientError {
	private static final long serialVersionUID = 96975996829722781L;
	private final String vault;
	private final String archive;

	public ArchiveNotFound(String vault, String archive) {
		super(vault + "/" + archive);
		this.vault = vault;
		this.archive = archive;
	}

	public String getVault() {
		return vault;
	}

	public String getArchive() {
		return archive;
	}

}
