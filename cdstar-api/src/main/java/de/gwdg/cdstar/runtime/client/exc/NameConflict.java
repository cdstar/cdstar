package de.gwdg.cdstar.runtime.client.exc;

public class NameConflict extends ClientError {
	private static final long serialVersionUID = 859015736230211629L;

	public NameConflict(String vault, String archive, String name) {
		super(vault + "/" + archive + "/" + name);
	}

}
