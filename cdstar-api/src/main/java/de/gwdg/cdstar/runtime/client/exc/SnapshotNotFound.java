package de.gwdg.cdstar.runtime.client.exc;

public class SnapshotNotFound extends ClientError {

	private static final long serialVersionUID = 96975996829722781L;
	private final String vault;
	private final String snapshotId;

	public SnapshotNotFound(String vault, String snapshotId) {
		super(vault + "/" + snapshotId);
		this.vault = vault;
		this.snapshotId = snapshotId;
	}

	public String getVault() {
		return vault;
	}

	public String getSnapsotId() {
		return snapshotId;
	}
}
