package de.gwdg.cdstar.runtime.client.exc;

import java.io.IOException;

public class InvalidFileName extends IOException {
	private static final long serialVersionUID = 789416175723479630L;
	private String name;
	private String detail;

	public InvalidFileName(String name, String detail) {
		super("Invalid file name. " + detail + ": " + name);
		this.name = name;
		this.detail = detail;
	}

	public String getName() {
		return name;
	}

	public String getDetail() {
		return detail;
	}

}
