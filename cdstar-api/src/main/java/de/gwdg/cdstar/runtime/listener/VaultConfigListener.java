package de.gwdg.cdstar.runtime.listener;

import de.gwdg.cdstar.runtime.VaultConfig;

public interface VaultConfigListener {
	/**
	 * Called after a new Vault is created or an existing vault is modified.
	 * Plugins may listen to this event to update their vault-specific
	 * configuration.
	 */
	default void vaultConfigChanged(VaultConfig vaultConfig) {
	}

}
