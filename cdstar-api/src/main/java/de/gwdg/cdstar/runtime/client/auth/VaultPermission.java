package de.gwdg.cdstar.runtime.client.auth;

import de.gwdg.cdstar.auth.StringPermission;

public enum VaultPermission {
	READ, // Load archives from this vault. Not checked for public vaults
	CREATE, // Create new archives in this vault
	CREATE_ID, // Create new archives with pre-defined IDs
	LIST, // List archive IDs (without ownership checks)
	MANAGE; // Read and change plugin settings for this vault

	public StringPermission toStringPermission(String vault) {
		return StringPermission.ofParts("vault", vault, name().toLowerCase());
	}
}