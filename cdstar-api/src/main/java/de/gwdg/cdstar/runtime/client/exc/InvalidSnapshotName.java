package de.gwdg.cdstar.runtime.client.exc;

public class InvalidSnapshotName extends ClientError {
	private static final long serialVersionUID = 894470101513996640L;

	public InvalidSnapshotName(String name, String reason) {
		super("Invalid snapshot name: " + name + " (" + reason + ")");
	}
}
