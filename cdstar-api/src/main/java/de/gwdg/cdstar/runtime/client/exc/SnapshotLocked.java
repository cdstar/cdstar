package de.gwdg.cdstar.runtime.client.exc;

public class SnapshotLocked extends RuntimeException {
	private static final long serialVersionUID = 5801254217970419077L;

	public SnapshotLocked() {
		super("Snapshots are read-only");
	}
}
