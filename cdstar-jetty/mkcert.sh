#!/bin/sh

# Create a long-lived self-signed test certificate for localhost:8433

openssl req -x509 -newkey rsa:4096 -keyout key.pem -out cert.pem -days 3650 -subj '/CN=localhost:8433' -nodes -sha256
cat cert.pem key.pem > combined.pem
