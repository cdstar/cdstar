package de.gwdg.cdstar.server.jetty;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.interfaces.RSAPrivateKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.security.auth.x500.X500Principal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Helper to create key and trust stores from a PEM file.
 */
public class PemReader {
	private static final Logger log = LoggerFactory.getLogger(PemReader.class);

	private final Path pem;
	Pattern reBegin = Pattern.compile("^-+BEGIN\\s+(.*?)\\s*-+$", Pattern.CASE_INSENSITIVE);
	Pattern reEnd = Pattern.compile("^-+END\\s+(.*?)\\s*-+$", Pattern.CASE_INSENSITIVE);

	PKCS8EncodedKeySpec privateKey;
	List<X509Certificate> certificates = new ArrayList<>();

	public PemReader(Path pemFile) throws IOException {
		pem = pemFile;

		try (BufferedReader br = Files.newBufferedReader(pem)) {
			log.debug("Reading: {}", pem);

			String line;
			String section = null;
			StringBuilder sb = null;
			Matcher m;
			int ln = 0;
			while ((line = br.readLine()) != null) {
				ln++;
				if (section == null && (m = reBegin.matcher(line)).matches()) {
					section = m.group(1).toUpperCase();
					sb = new StringBuilder();
					log.debug("BEGIN {} (line {})", section, ln);
				} else if (section != null && (m = reEnd.matcher(line)).matches()) {
					log.debug("END {} (line {})", section, ln);
					if (!m.group(1).equalsIgnoreCase(section))
						throw new IllegalStateException("END marker does not match BEGIN: " + line);
					final byte[] data = Base64.getMimeDecoder().decode(sb.toString());
					addSection(section, data);
					section = null;
				} else if (sb != null) {
					sb.append(line);
				}
			}
		}
	}

	private void addSection(String section, byte[] data) throws IOException {
		if (section.equalsIgnoreCase("CERTIFICATE"))
			addCertificate(data);
		else if (section.equalsIgnoreCase("PRIVATE KEY"))
			addPrivateKey(data);
		else
			throw new IOException("Unknoen section: " + section);
	}

	private void addPrivateKey(byte[] data) throws IOException {
		if (privateKey != null)
			throw new IOException("Found more than one private key");
		privateKey = new PKCS8EncodedKeySpec(data);
		log.debug("Found private key: {}", privateKey.getFormat());
	}

	private void addCertificate(byte[] data) throws IOException {
		try {
			final X509Certificate cert = (X509Certificate) CertificateFactory.getInstance("X.509")
				.generateCertificate(new ByteArrayInputStream(data));
			certificates.add(cert);
			log.debug("Found certificate: {}", cert);
		} catch (final CertificateException e) {
			throw new IOException("Failed to read or parse certificate", e);
		}
	}

	/**
	 * Build a {@link KeyStore} with all certificates and the private key.
	 */
	public KeyStore buildKeyStore(char[] password) throws GeneralSecurityException {
		if (privateKey == null)
			throw new IllegalStateException("No private key in: " + pem.toString());
		if (certificates.isEmpty())
			throw new IllegalStateException("No certificates in: " + pem.toString());

		final RSAPrivateKey key = (RSAPrivateKey) KeyFactory.getInstance("RSA").generatePrivate(privateKey);
		final KeyStore keyStore = KeyStore.getInstance("JKS");
		try {
			keyStore.load(null, null);
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}
		keyStore.setKeyEntry("private", key, password, certificates.stream().toArray(Certificate[]::new));
		return keyStore;
	}

	/**
	 * Build a {@link KeyStore} with just the certificates.
	 */
	public KeyStore loadTrustStore() throws GeneralSecurityException {
		if (certificates.isEmpty())
			throw new IllegalStateException("No certificates in: " + pem.toString());

		final KeyStore keyStore = KeyStore.getInstance("JKS");
		try {
			keyStore.load(null, null);
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}

		for (final X509Certificate certificate : certificates) {
			final X500Principal principal = certificate.getSubjectX500Principal();
			keyStore.setCertificateEntry(principal.getName("RFC2253"), certificate);
		}
		return keyStore;
	}

}
