package de.gwdg.cdstar.server.jetty;

import java.io.IOException;
import java.net.BindException;
import java.net.Socket;
import java.net.SocketException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.eclipse.jetty.alpn.server.ALPNServerConnectionFactory;
import org.eclipse.jetty.http.HttpVersion;
import org.eclipse.jetty.http2.HTTP2Cipher;
import org.eclipse.jetty.http2.server.HTTP2ServerConnectionFactory;
import org.eclipse.jetty.io.Connection;
import org.eclipse.jetty.server.ConnectionFactory;
import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.HttpConfiguration;
import org.eclipse.jetty.server.HttpConnectionFactory;
import org.eclipse.jetty.server.SecureRequestCustomizer;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.server.SocketCustomizationListener;
import org.eclipse.jetty.server.SslConnectionFactory;
import org.eclipse.jetty.server.handler.ContextHandler.Context;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.util.JavaVersion;
import org.eclipse.jetty.util.component.LifeCycle;
import org.eclipse.jetty.util.ssl.SslContextFactory;
import org.eclipse.jetty.util.thread.QueuedThreadPool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.servlet.ServletContainerInitializer;

public class JettyServer {
	private static final Logger log = LoggerFactory.getLogger(JettyServer.class);

	private final class KeepaliveConnectoionListener extends SocketCustomizationListener {
		@Override
		protected void customize(Socket socket, Class<? extends Connection> connection, boolean ssl) {
			try {
				socket.setKeepAlive(true);
				log.trace("Enabled TCP Keepalive on {}", socket);
			} catch (SocketException e) {
				if (log.isTraceEnabled())
					log.warn("Unable to enable TCP KEEPALICE for socket {}", socket, e);
			}
		}
	}

	private final Server server;
	private final List<ServletContainerInitializer> components = new ArrayList<>();

	public JettyServer() {
		// Jetty can handle thousands of async requests with a small number of
		// threads, but synchronous requests still inflate the thread pool,
		// which is unbounded by default. This config limits the number of
		// threads and ensures that idle threads are cleaned up quickly.
		final QueuedThreadPool threadPool = new QueuedThreadPool();
		threadPool.setName("jetty");
		threadPool.setIdleTimeout(1000 * 10); // Milliseconds
		final int cpus = Runtime.getRuntime().availableProcessors();
		threadPool.setMinThreads(Math.max(cpus, 2) * 4);
		threadPool.setMaxThreads(Math.max(cpus, 2) * 16);
		log.debug("Jetty thread pool: {}", threadPool);
		server = new Server(threadPool);
		server.setStopAtShutdown(true);

		final ServletContextHandler context = new ServletContextHandler(server, "/");
		context.addEventListener(new LifeCycle.Listener() {
			Context ctx = context.getServletContext();

			@Override
			public void lifeCycleStarting(LifeCycle event) {
				try {
					final boolean isExtended = ctx.isExtendedListenerTypes();
					if (!isExtended)
						ctx.setExtendedListenerTypes(true);

					// Run explicitly installed SCIs.
					for (final ServletContainerInitializer c : components) {
						log.info("Installing ServletContainerInitializer: {}", c.getClass().getName());
						c.onStartup(null, ctx);
					}

					if (!isExtended)
						ctx.setExtendedListenerTypes(false);
				} catch (final Exception e) {
					try {
						server.stop();
					} catch (final Exception e2) {
						log.warn("Could not stop server.", e2);
					}
					log.error("Failed to inizialize container config during server startup.", e);
					throw new RuntimeException(e);
				}
			}
		});
	}

	public JettyServer(String host, int port) {
		this();
		enableHttp(host, port);
	}

	public void addComponent(ServletContainerInitializer init) {
		components.add(init);
	}

	public void start() throws Exception {
		log.info("Starting jetty server...");

		try {
			server.start();
			log.info("Listening on {}", getURI());
		} catch (final BindException e) {
			log.error(e.getMessage());
			throw e;
		} catch (final Exception e) {
			log.error("Failed to start server", e);
			throw e;
		}
	}

	public void enableHttp(String host, int port) {
		final ServerConnector httpConnector = new ServerConnector(server);
		httpConnector.setHost(host);
		httpConnector.setPort(port);
		httpConnector.addEventListener(new KeepaliveConnectoionListener());
		server.addConnector(httpConnector);
		log.debug("Enabled HTTP for {}:{}", host, port);
	}

	public void enableHttps(String host, int port, Path pemFile, boolean enableH2)
			throws IOException, GeneralSecurityException {

		// SSL Context Factory for HTTPS and HTTP/2
		final PemReader pem = new PemReader(pemFile);
		final SslContextFactory.Server sslServerFactory = new SslContextFactory.Server();
		final String ksp = UUID.randomUUID().toString(); // For in-memory encryption
		sslServerFactory.setKeyStore(pem.buildKeyStore(ksp.toCharArray()));
		sslServerFactory.setKeyStorePassword(ksp);
		sslServerFactory.setTrustStore(pem.loadTrustStore());

		// HTTPS Configuration
		final HttpConfiguration httpsConfig = new HttpConfiguration();
		httpsConfig.setSecureScheme("https");
		httpsConfig.setSecurePort(port);
		httpsConfig.addCustomizer(new SecureRequestCustomizer());

		final List<ConnectionFactory> factories = new ArrayList<>();

		if (enableH2) {
			if (JavaVersion.VERSION.getPlatform() < 9) {
				throw new UnsupportedOperationException(
						"Jetty http2 support requires ALPN, "
								+ "which is only available for Java 9 and later.");
			}

			// Always prefer non-blacklisted ciphers
			sslServerFactory.setCipherComparator(HTTP2Cipher.COMPARATOR);

			final ALPNServerConnectionFactory alpn = new ALPNServerConnectionFactory();
			alpn.setDefaultProtocol(HttpVersion.HTTP_1_1.asString());

			factories.add(new SslConnectionFactory(sslServerFactory, alpn.getProtocol()));
			factories.add(alpn);
			factories.add(new HTTP2ServerConnectionFactory(httpsConfig));

		} else {
			factories.add(new SslConnectionFactory(sslServerFactory, HttpVersion.HTTP_1_1.asString()));
		}

		factories.add(new HttpConnectionFactory(httpsConfig));

		final ServerConnector httpsConnector = new ServerConnector(server,
				factories.toArray(new ConnectionFactory[factories.size()]));
		httpsConnector.setHost(host);
		httpsConnector.setPort(port);
		httpsConnector.addEventListener(new KeepaliveConnectoionListener());
		server.addConnector(httpsConnector);
		log.debug("Enabled HTTPS on {}:{} (h2 {})", host, port, enableH2 ? "enabled" : "disabled");
	}

	private ServerConnector getFirstServerConnector() {
		for (final Connector connector : server.getConnectors()) {
			if (connector instanceof ServerConnector) {
				return (ServerConnector) connector;
			}
		}
		throw new IllegalStateException("No server connectors installed");
	}

	public void join() throws InterruptedException {
		if (server.isStarted())
			server.join();
	}

	public void stop() throws Exception {
		server.stop();
	}

	/**
	 * Return the configured listening port. If multiple connectors are installed,
	 * return the port of the first one.
	 **/
	public int getPort() {
		return getFirstServerConnector().getPort();
	}

	/**
	 * Return the real port this server is listening on, or -1 if it has not been
	 * opened, or -2 if it has been closed. If multiple connectors are installed,
	 * return the bound port of the first one.
	 **/
	public int getRealPort() {
		return getFirstServerConnector().getLocalPort();
	}

	/**
	 * Return the configured host. If multiple connectors are installed, return the
	 * host of the first one.
	 */
	public String getHost() {
		return getFirstServerConnector().getHost();
	}

	public URI getURI() {
		try {
			return new URI("http", null, getHost(), getRealPort(), "/", null, null);
		} catch (final URISyntaxException e) {
			throw new RuntimeException(e);
		}
	}

}
