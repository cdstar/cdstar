package de.gwdg.cdstar.rest.ext.tus;

import java.net.URI;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousFileChannel;
import java.nio.channels.CompletionHandler;
import java.util.Objects;
import java.util.function.BiConsumer;

import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.rest.v3.async.UrlFetchService;
import de.gwdg.cdstar.web.common.model.ErrorResponse;

/**
 * An {@link UrlFetchService} to allow other components (e.g. the archive update
 * api) to access tus-uploaded files as data source.
 */
public class TusUrlFetch implements UrlFetchService {

	private final TusService tus;
	private final String scheme;

	public TusUrlFetch(TusService tus, String urlScheme) {
		this.tus = Objects.requireNonNull(tus);
		scheme = Utils.nullOrEmpty(urlScheme) ? "tus" : urlScheme;
	}

	@Override
	public boolean canHandle(URI uri) {
		return scheme.equals(uri.getScheme());
	}

	@Override
	public FetchHandle resolve(URI uri) {

		var tusId = uri.getSchemeSpecificPart();
		var upload = tus.getUpload(tusId)
				.orElseThrow(() -> new ErrorResponse(400, "TusNotFound", "Unknown or expired TUS file id")
						.detail("id", tusId));

		if (!upload.isComplete())
			throw new ErrorResponse(409, "TusIncomplete", "TUS file is incomplete");

		return new FetchHandle() {
			private AsynchronousFileChannel readChannel;
			boolean closed;
			long position = 0;

			@Override
			public long size() {
				return upload.getLength();
			}

			@Override
			public void read(ByteBuffer dst, BiConsumer<Integer, Throwable> handler) {
				try {
					if(upload.getLength() == 0) {
						handler.accept(0, null);
						return;
					}

					if (readChannel == null)
						readChannel = upload.getReadChannel();
					readChannel.read(dst, position, null, new CompletionHandler<Integer, Void>() {
						@Override
						public void completed(Integer result, Void attachment) {
							if (result > 0)
								position += result;
							handler.accept(result, null);
						}

						@Override
						public void failed(Throwable exc, Void attachment) {
							handler.accept(null, exc);
						}
					});
				} catch (Exception e) {
					handler.accept(null, e);
				}
			}

			@Override
			public synchronized void close() {
				if (!closed) {
					upload.unlock();
					Utils.closeQuietly(readChannel);
					closed = true;
				}
			}
		};

	}
}