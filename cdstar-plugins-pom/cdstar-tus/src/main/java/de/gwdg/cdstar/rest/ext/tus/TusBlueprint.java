package de.gwdg.cdstar.rest.ext.tus;

import java.io.IOException;
import java.util.Date;
import java.util.Map;
import java.util.Optional;

import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.rest.api.Blueprint;
import de.gwdg.cdstar.rest.api.RestBlueprint;
import de.gwdg.cdstar.rest.api.RestConfig;
import de.gwdg.cdstar.rest.api.RestContext;
import de.gwdg.cdstar.rest.v3.async.AsyncUpload;
import de.gwdg.cdstar.runtime.CDStarRuntime;
import de.gwdg.cdstar.runtime.services.FeatureRegistry;
import de.gwdg.cdstar.web.common.model.ErrorResponse;

/**
 * A tus.io compatible API to upload temporary files.
 */
@Blueprint(path = "/tus")
public class TusBlueprint implements RestBlueprint {
	private static final String TUS_UPLOAD_METADATA = "Upload-Metadata";
	private static final String TUS_UPLOAD_EXPIRES = "Upload-Expires";
	private static final String TUS_UPLOAD_LENGTH = "Upload-Length";
	private static final String TUS_UPLOAD_DEFER_LENGTH = "Upload-Defer-Length";
	private static final String TUS_UPLOAD_OFFSET = "Upload-Offset";
	private static final String TUS_EXTENSION = "Tus-Extension";
	private static final String TUS_RESUMABLE = "Tus-Resumable";

	private static final String TUS_EXTENSIONS = "creation,creation-defer-length,expiration,termination";
	private static final String TUS_VERSION = "1.0.0";
	private final TusService chunkService;

	public TusBlueprint(TusService service) {
		chunkService = service;
	}

	@Override
	public void configure(RestConfig cfg) {
		cfg.route("/")
				.POST(this::handleCreate)
				.target("OPTIONS", this::handleOptions);

		cfg.route("/<chunk:re:[^/]+>")
				.PATCH(this::handlePatch)
				.DELETE(this::handleDelete)
				.HEAD(this::handleHead);

		var mount = cfg.getPrefix() + "/";
		Optional.ofNullable(cfg.lookup(CDStarRuntime.class))
				.flatMap(rt -> rt.lookup(FeatureRegistry.class))
				.ifPresent(fr -> fr.addFeature(
						"tus", Map.of(
								"v", "3",
								"path", mount)));

	}

	public Void handleOptions(RestContext ctx) throws IOException {
		// The Client SHOULD NOT include the Tus-Resumable header in the request and the
		// Server MUST ignore the header.

		ctx.status(204);
		ctx.header(TUS_RESUMABLE, TUS_VERSION);
		ctx.header(TUS_EXTENSION, TUS_EXTENSIONS);

		return null;
	}

	public Void handleCreate(RestContext ctx) throws IOException {
		checkTusRequestVersion(ctx);

		// TODO: Access control, DDOS protection

		long length = "1".equals(ctx.getHeader(TUS_UPLOAD_DEFER_LENGTH))
				? -1
				: requireLongHeader(ctx, TUS_UPLOAD_LENGTH);
		var meta = ctx.getHeader(TUS_UPLOAD_METADATA);

		TusUpload upload = chunkService.createNewUpload(length, meta);

		ctx.status(201);
		ctx.header(TUS_RESUMABLE, TUS_VERSION);
		ctx.header(TUS_UPLOAD_EXPIRES, Date.from(upload.getExpiresAt()));
		ctx.header("Location", ctx.resolvePath(upload.getName(), true));

		return null;
	}

	public Void handleHead(RestContext ctx) throws IOException {
		checkTusRequestVersion(ctx);

		final TusUpload upload = getUpload(ctx.getPathParam("chunk"));

		ctx.status(200);
		addResponseHeaders(ctx, upload);

		return null;
	}

	public Void handlePatch(RestContext ctx) throws IOException {
		checkTusRequestVersion(ctx);

		final TusUpload upload = getUpload(ctx.getPathParam("chunk"));

		if (!Utils.equal(ctx.getHeader("Content-Type"), "application/offset+octet-stream"))
			throw new ErrorResponse(406, "TusError", "Content type MUST be 'application/offset+octet-stream'.");

		if (!upload.tryLock())
			throw new ErrorResponse(409, "Conflict", "Failed to open upload: Resource locked.");

		try {
			if (requireLongHeader(ctx, TUS_UPLOAD_OFFSET) != upload.getCurrentOffset())
				throw new ErrorResponse(409, "TusError", "Upload-Offset header does not match current upload size.");

			if (upload.isComplete())
				throw new ErrorResponse(409, "TusError", "Upload complete.");

			if (ctx.getHeader(TUS_UPLOAD_LENGTH) != null) {
				long length = requireLongHeader(ctx, TUS_UPLOAD_LENGTH);
				if (!upload.hasLength()) {
					upload.setLength(length);
					upload.persistInfo();
				} else if (length != upload.getLength()) {
					throw new ErrorResponse(400, "TusError", "Cannot change Upload-Length once it has been set.");
				}
			}

			// TODO: Limit upload size if target length is known
			var channel = upload.getWriteChannel();
			new AsyncUpload(ctx, channel).dispatch().whenComplete((totalUpload, err) -> {
				Utils.closeQuietly(channel);
				upload.unlock();

				if (err != null) {
					if (!ctx.isClosed())
						ctx.abort(err);
					return;
				}

				ctx.status(204);
				addResponseHeaders(ctx, upload);
				ctx.close();
			});

			return null;
		} catch (Exception e) {
			upload.unlock();
			throw e;
		}

	}

	public Void handleDelete(RestContext ctx) {
		checkTusRequestVersion(ctx);

		final TusUpload chunk = getUpload(ctx.getPathParam("chunk"));

		if (!chunk.tryLock())
			throw new ErrorResponse(409, "Conflict", "Failed to open upload: Resource locked.");

		chunkService.removeUpload(chunk);

		ctx.status(204);
		ctx.header(TUS_RESUMABLE, TUS_VERSION);

		return null;
	}

	private TusUpload getUpload(String id) {
		return chunkService
				.getUpload(id)
				.orElseThrow(() -> new ErrorResponse(404, "TusError", "Upload not found or expired"));
	}

	/**
	 * Add Cache-Control, {@value #TUS_RESUMABLE}, {@value #TUS_UPLOAD_OFFSET},
	 * {@value #TUS_UPLOAD_LENGTH}/{@value #TUS_UPLOAD_DEFER_LENGTH} and
	 * {@value #TUS_UPLOAD_EXPIRES} headers as required for HEAD and PATCH requests.
	 *
	 * @param ctx
	 * @param chunk
	 */
	private void addResponseHeaders(RestContext ctx, final TusUpload upload) {
		ctx.header("Cache-Control", "no-store");
		ctx.header(TUS_RESUMABLE, TUS_VERSION);
		ctx.header(TUS_UPLOAD_OFFSET, upload.getCurrentOffset());
		ctx.header(TUS_UPLOAD_EXPIRES, Date.from(upload.getExpiresAt()));

		if (upload.hasLength())
			ctx.header(TUS_UPLOAD_LENGTH, upload.getLength());
		else
			ctx.header(TUS_UPLOAD_DEFER_LENGTH, "1");

		if (upload.getMeta() != null) // Not required for PATCH
			ctx.header(TUS_UPLOAD_METADATA, upload.getMeta());
	}

	private void checkTusRequestVersion(RestContext ctx) {
		if (!Utils.equal(ctx.getHeader(TUS_RESUMABLE), TUS_VERSION))
			throw new ErrorResponse(400, "TusError", "Tus-Resumable header missing or wrong version.")
					.detail("expected", TUS_VERSION);
	}

	private long requireLongHeader(RestContext ctx, String name) {
		try {
			return Long.parseUnsignedLong(ctx.getHeader(name));
		} catch (final NumberFormatException e) {
			throw new ErrorResponse(400, "TusError", name + " header missing or invalid.");
		}
	}
}