package de.gwdg.cdstar.rest.ext.tus;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.rest.v3.async.ArchiveUpdater;

/**
 * Implements a TUS (tus.io) REST endpoint to create and write to anonymous
 * temporary files, and a way for {@link ArchiveUpdater} to import from these
 * files. All bundled as an optional plugin.
 */
public class TusService {

	public static final String pluginName = "tus-upload";
	static final Logger log = LoggerFactory.getLogger(TusService.class);

	Path basePath;
	Map<String, TusUpload> uploads = new ConcurrentHashMap<>();
	private Executor pool;
	// Both timeouts are relative to the last chunk uploaded.
	Duration expireIncompleteAfter;
	Duration expireFinishedAfter;

	public TusService(Path basePath, Duration defaultLifetime) {
		this.basePath = basePath;
		this.expireIncompleteAfter = defaultLifetime;
		this.expireFinishedAfter = defaultLifetime;
	}

	public void start(Executor pool) throws IOException {
		this.pool = pool;

		log.info("Scanning {} for existing tus uploads", basePath);

		try (Stream<Path> s = Files.list(Files.createDirectories(basePath))) {
			s.filter(path -> path.getFileName().toString().endsWith(TusUpload.EXT_INFO)).forEach(infoFile -> {
				try {
					var tus = new TusUpload(this, infoFile);
					log.info("Found TUS upload: {}", tus);
					uploads.put(tus.getName(), tus);
				} catch (IOException e) {
					log.warn("Failed to load {}", infoFile, e);
				}
			});
		}
	}

	public void cleanupExpiredUploads() {
		final Instant now = Instant.now();

		// Collect (and lock) expired uploads in a thread-save way.
		// Do not expire locked uploads, as they might be in use.
		final List<TusUpload> expired = new ArrayList<>();
		uploads.values().forEach(upload -> {
			if (upload.isExpired(now) && upload.tryLock()) {
				expired.add(upload);
			}
		});

		// Remove all expired chunks
		expired.forEach(this::removeUpload);
	}

	public TusUpload createNewUpload(long length, String meta) throws IOException {
		TusUpload upload;
		do {
			upload = new TusUpload(this);
			if (length >= 0)
				upload.setLength(length);
			if (meta != null)
				upload.setMeta(meta);
		} while (uploads.putIfAbsent(upload.getName(), upload) != null);

		upload.persistInfo();
		return upload;
	}

	public Optional<TusUpload> getUpload(String name) {
		final TusUpload upload = uploads.get(name);
		if (upload == null || upload.isExpired(Instant.now()))
			return Optional.empty();
		return Optional.of(upload);
	}

	void removeUpload(TusUpload upload) {
		uploads.remove(upload.getName());
		Utils.deleteQuietly(upload.getInfoPath());
		Utils.deleteQuietly(upload.getChunkPath());
	}

	public int getUploadCount() {
		return uploads.size();
	}

	public long getUploadTotalSize() {
		return uploads.values().stream()
				.mapToLong(upload -> upload.hasLength() ? upload.getLength() : upload.getCurrentOffset())
				.sum();
	}

	public Executor getPool() {
		return pool;
	}

}
