package de.gwdg.cdstar.rest.ext.tus;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousFileChannel;
import java.nio.channels.FileChannel;
import java.nio.channels.WritableByteChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.time.Instant;
import java.time.format.DateTimeFormatter;

import de.gwdg.cdstar.SharedObjectMapper;
import de.gwdg.cdstar.Utils;

class TusUpload {
	static final String EXT_INFO = ".json";
	static final String EXT_BIN = ".bin";

	private TusService service;
	private FileChannel writeChannel;
	private boolean locked;

	private final String name;
	private long length = -1;
	private String meta;
	private final Instant created;
	private Instant lastModified;

	TusUpload(TusService service) {
		this.service = service;
		name = Utils.bytesToHex(Utils.randomBytes(16));
		created = lastModified = Instant.now();
	}

	TusUpload(TusService service, Path infoFile) throws IOException {
		this.service = service;
		var json = SharedObjectMapper.json.readTree(infoFile.toFile());
		name = json.path("name").textValue();
		length = json.path("length").longValue();
		length = length < 0 ? -1 : length;
		meta = json.path("meta").textValue();
		created = DateTimeFormatter.ISO_INSTANT.parse(json.path("created").textValue(), Instant::from);
		lastModified = Files.getLastModifiedTime(Files.exists(getChunkPath()) ? getChunkPath() : infoFile).toInstant();
	}

	public synchronized void persistInfo() throws IOException {
		var json = SharedObjectMapper.json.createObjectNode();
		json.put("name", name);
		json.put("length", length);
		json.put("meta", meta);
		json.put("created", DateTimeFormatter.ISO_INSTANT.format(created));
		Files.write(getInfoPath(), SharedObjectMapper.json.writeValueAsBytes(json));
	}

	public synchronized boolean tryLock() {
		if (locked)
			return false;
		locked = true;
		return locked;
	}

	public synchronized void unlock() {
		if (writeChannel != null) {
			Utils.closeQuietly(writeChannel);
			writeChannel = null;
		}
		locked = false;
	}

	public String getName() {
		return name;
	}

	public long getLength() {
		return length;
	}

	public void setLength(long length) {
		this.length = length;
	}

	public boolean hasLength() {
		return length > -1;
	}

	public boolean isComplete() {
		return hasLength() && getCurrentOffset() >= getLength();
	}

	public synchronized long getCurrentOffset() {
		try {
			if (writeChannel != null && writeChannel.isOpen())
				return writeChannel.position();
			return Files.size(getChunkPath());
		} catch (final IOException e) {
			return 0;
		}
	}

	public synchronized Instant getLastModified() {
		return lastModified;
	}

	public String getMeta() {
		return meta;
	}

	public void setMeta(String meta) {
		this.meta = meta;
	}

	public Instant getExpiresAt() {
		return getLastModified().plus(isComplete() ? service.expireFinishedAfter : service.expireIncompleteAfter);
	}

	Path getPath() {
		return service.basePath;
	}

	Path getChunkPath() {
		return getPath().resolve(name + EXT_BIN);
	}

	Path getInfoPath() {
		return getPath().resolve(name + EXT_INFO);
	}

	public boolean isExpired(Instant now) {
		return now.isAfter(getExpiresAt());
	}

	@Override
	public String toString() {
		return "tus:" + name;
	}

	public synchronized WritableByteChannel getWriteChannel() throws IOException {
		if (writeChannel != null && writeChannel.isOpen())
			throw new IllegalStateException("Write in progress");
		if (!locked)
			throw new IllegalStateException("Upload not locked");

		writeChannel = FileChannel.open(getChunkPath(), StandardOpenOption.CREATE, StandardOpenOption.WRITE,
				StandardOpenOption.APPEND);

		return new WritableByteChannel() {
			WritableByteChannel delegate = writeChannel;

			@Override
			public boolean isOpen() {
				return delegate.isOpen();
			}

			@Override
			public void close() throws IOException {
				if (isOpen()) {
					synchronized (TusUpload.this) {
						if (writeChannel != delegate)
							throw Utils.wtf();
						lastModified = Instant.now();
						Utils.closeQuietly(writeChannel);
						writeChannel = null;
					}
				}
			}

			@Override
			public int write(ByteBuffer src) throws IOException {
				synchronized (TusUpload.this) {
					lastModified = Instant.now();
				}
				return delegate.write(src);
			}
		};
	}

	public synchronized AsynchronousFileChannel getReadChannel() throws IOException {
		if (writeChannel != null && writeChannel.isOpen())
			throw new IllegalStateException("Write in progress");
		if (!isComplete())
			throw new IllegalStateException("Incomplete upload");
		if(getLength() <= 0)
			throw new IllegalStateException("Zero-length upload");
		return AsynchronousFileChannel.open(getChunkPath(), StandardOpenOption.READ);
	}
}