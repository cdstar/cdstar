package de.gwdg.cdstar.rest.ext.tus;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

import com.codahale.metrics.Gauge;
import com.codahale.metrics.MetricRegistry;

import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.rest.v3.async.ArchiveUpdater;
import de.gwdg.cdstar.runtime.Config;
import de.gwdg.cdstar.runtime.Plugin;
import de.gwdg.cdstar.runtime.RuntimeContext;
import de.gwdg.cdstar.runtime.listener.RuntimeListener;
import de.gwdg.cdstar.runtime.services.CronService;
import de.gwdg.cdstar.runtime.services.PoolService;

/**
 * This plugin installs a TUS (tus.io) REST endpoint to create and write to
 * anonymous temporary files, and a way for {@link ArchiveUpdater} to import
 * from these files.
 */
@Plugin(name = TusPlugin.pluginName)
public class TusPlugin implements RuntimeListener {

	public static final String pluginName = "tus-upload";
	private TusService service;
	private Duration defaultExpire;

	public TusPlugin(Config config) {
		defaultExpire = Utils.parseDuration(config.get("expire", "24h"));
	}

	@Override
	public void onInit(RuntimeContext ctx) throws Exception {
		service = new TusService(ctx.getServiceDir("tus"), defaultExpire);
		ctx.register(service);
		ctx.register(new TusUrlFetch(service, "tus"));
		ctx.register(new TusBlueprint(service));
	}

	@Override
	public void onStartup(RuntimeContext ctx) throws Exception {
		ctx.lookup(MetricRegistry.class).ifPresent(metrics -> {
			metrics.register(MetricRegistry.name(pluginName, "count"), new Gauge<Integer>() {
				@Override
				public Integer getValue() {
					return Integer.valueOf(service.getUploadCount());
				}
			});

			metrics.register(MetricRegistry.name(pluginName, "bytes"), new Gauge<Long>() {
				@Override
				public Long getValue() {
					return Long.valueOf(service.getUploadTotalSize());
				}
			});
		});

		service.start(ctx.lookupRequired(PoolService.class).getNamedPool("tus"));

		ctx.lookupRequired(CronService.class)
				.scheduleWithFixedDelay(service::cleanupExpiredUploads, 60, 60, TimeUnit.SECONDS);
	}

}
