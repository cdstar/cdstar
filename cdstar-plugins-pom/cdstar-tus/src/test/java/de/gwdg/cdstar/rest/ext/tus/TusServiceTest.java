package de.gwdg.cdstar.rest.ext.tus;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.time.Duration;
import java.util.concurrent.Executor;
import java.util.function.IntConsumer;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.mockito.Mockito;

import de.gwdg.cdstar.rest.RestConfigImpl;
import de.gwdg.cdstar.rest.testutils.TestClient;
import de.gwdg.cdstar.rest.testutils.TestResponse;

public class TusServiceTest {

	@Rule
	public TemporaryFolder tmp = new TemporaryFolder();

	private TusService tus;
	private RestConfigImpl rest;
	private TusBlueprint tusApi;

	private TestClient client;

	@Before
	public void setUp() throws IOException {
		rest = new RestConfigImpl(null);
		tus = new TusService(tmp.newFolder().toPath(), Duration.ofSeconds(10));
		tus.start(Mockito.mock(Executor.class));
		tusApi = new TusBlueprint(tus);
		rest.install(tusApi);
		client = new TestClient(rest);
	}

	@Test
	public void testChunkUpload() throws IOException {
		TestResponse rs = client
				.POST("/tus")
				.header("Tus-Resumable", "1.0.0")
				.header("Upload-Defer-Length", "1")
				.submit();
		rs.assertStatus(201);
		final String loc = rs.getHeader("Location");
		final String[] parts = loc.split("/");
		final String chunkId = parts[parts.length - 1];

		final ByteBuffer data = ByteBuffer.allocate(1024 * 1024);

		// Chunk upload helper
		final IntConsumer upload = (size) -> {
			final byte[] chunk = new byte[size];
			final int pos = data.position();
			data.get(chunk);
			final TestResponse rsi = client
					.PATCH("/tus", chunkId)
					.header("Tus-Resumable", "1.0.0")
					.header("Upload-Offset", pos)
					.entity("application/offset+octet-stream", chunk)
					.submit();
			rsi.assertStatus(204);
			rsi.assertHeaderEquals("Tus-Resumable", "1.0.0");
			rsi.assertHeaderEquals("Upload-Offset", Integer.toString(data.position()));
		};

		// Probe edge cases and borders of typical buffer sizes
		upload.accept(0);
		upload.accept(1);
		upload.accept(2);
		upload.accept(0);
		for (int i = -1; i <= 3; i++)
			upload.accept(1024 + i);
		for (int i = -1; i <= 3; i++)
			upload.accept(1024 * 8 + i);

		// Upload rest of data in bulk
		upload.accept(data.remaining());

		assertFalse(tus.getUpload(chunkId).get().isComplete());

		// Finalize upload (with a zero-length PATCH)
		final TestResponse rsi = client
				.PATCH("/tus", chunkId)
				.header("Tus-Resumable", "1.0.0")
				.header("Upload-Offset", data.position())
				.header("Upload-Length", data.position())
				.entity("application/offset+octet-stream", new byte[0])
				.submit();
		rsi.assertStatus(204);

		assertTrue(tus.getUpload(chunkId).get().isComplete());
		assertEquals(data.position(), tus.getUpload(chunkId).get().getCurrentOffset());
	}
}
