package de.gwdg.cdstar.rest.ext.tus;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.time.Duration;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.function.BiConsumer;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.mockito.Mockito;

import de.gwdg.cdstar.rest.RestConfigImpl;

public class TusUrlFetchTest {

	@Rule
	public TemporaryFolder tmp = new TemporaryFolder();

	private TusService tus;
	private RestConfigImpl rest;

	@Before
	public void setUp() throws IOException {
		rest = new RestConfigImpl(null);
		tus = new TusService(tmp.newFolder().toPath(), Duration.ofSeconds(10));

		var mockPool = Mockito.mock(Executor.class);
		Mockito.doAnswer(invocation -> {
			((Runnable) invocation.getArguments()[0]).run();
			return null;
		}).when(mockPool).execute(Mockito.any(Runnable.class));

		tus.start(mockPool);
	}

	@Test
	public void test()
			throws IOException, URISyntaxException, InterruptedException, ExecutionException, TimeoutException {
		var upload = tus.createNewUpload(1024, null);
		assertTrue(upload.tryLock());
		try (var writeChannel = upload.getWriteChannel()) {
			Channels.newOutputStream(writeChannel).write(new byte[1024]);
		}
		upload.unlock();

		var fetchService = new TusUrlFetch(tus, "tus");

		assertTrue(fetchService.canHandle(new URI("tus", upload.getName(), null)));
		assertFalse(fetchService.canHandle(new URI("notus", upload.getName(), null)));

		var fetch = fetchService.resolve(new URI("tus", upload.getName(), null));
		assertEquals(upload.getLength(), fetch.size());
		var cf = new CompletableFuture<Void>();
		var buf = ByteBuffer.allocate(1024);
		fetch.read(buf, new BiConsumer<Integer, Throwable>() {
			@Override
			public void accept(Integer r, Throwable e) {
				if (e != null)
					cf.completeExceptionally(e);
				else if (r >= 0 && buf.remaining() > 0) {
					fetch.read(buf, this);
				} else
					cf.complete(null);
			};
		});

		cf.get(10, TimeUnit.SECONDS);
	}

}
