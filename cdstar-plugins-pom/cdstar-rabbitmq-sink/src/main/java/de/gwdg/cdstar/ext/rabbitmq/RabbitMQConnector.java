package de.gwdg.cdstar.ext.rabbitmq;

import java.io.Closeable;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.time.Duration;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.rabbitmq.client.AMQP.BasicProperties;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.event.ChangeEvent;

class RabbitMQConnector implements Closeable {
	private static final Logger log = LoggerFactory.getLogger(RabbitMQConnector.class);
	private static final ObjectMapper mapper;

	static {
		mapper = new ObjectMapper();
		mapper.disable(SerializationFeature.INDENT_OUTPUT);
	}

	private volatile boolean closed = false;

	private Connection connection;
	private Channel channel;

	private final String exchangeName;

	private BlockingQueue<ChangeEvent> sendQueue;
	SortedMap<Long, ChangeEvent> inFlight = new TreeMap<>();

	private int sendQueueSize = 1024;
	private Thread sendThread;
	private final String exchangeType;
	private final ConnectionFactory factory;

	private final Duration reconnectCooldown = Duration.ofSeconds(10);

	public RabbitMQConnector(URI brokerUri, String exchangeName, String exchangeType) {
		this.exchangeName = exchangeName;
		this.exchangeType = exchangeType;

		setSendQueueSize(sendQueueSize);

		factory = new ConnectionFactory();
		try {
			factory.setUri(brokerUri);
		} catch (KeyManagementException | NoSuchAlgorithmException | URISyntaxException e) {
			throw new IllegalArgumentException("Invalid broker URL: " + brokerUri, e);
		}
	}

	public synchronized void setSendQueueSize(int sendQueueSize) {
		this.sendQueueSize = sendQueueSize;
		final BlockingQueue<ChangeEvent> oldQueue = sendQueue;
		sendQueue = sendQueueSize > 0 ? new ArrayBlockingQueue<>(sendQueueSize) : new LinkedBlockingQueue<>();
		if (oldQueue != null)
			oldQueue.drainTo(sendQueue);
	}

	public synchronized void start() throws IOException, TimeoutException {
		if (sendThread != null)
			throw new IllegalStateException("Already started");

		connect();

		sendThread = new Thread(null, this::sendLoop, "rabbitmq-send");
		sendThread.start();
	}

	/**
	 * Try to (re)connect a channel, or raise the last exception.
	 */
	private synchronized void connect() throws IOException, TimeoutException {

		if (connection != null && connection.isOpen()) {
			Utils.closeQuietly(connection);

			synchronized (inFlight) {
				inFlight.values().removeIf(event -> {
					enqueueOrDrop(event, "reconnect");
					return true;
				});
			}
		}

		connection = factory.newConnection();
		channel = connection.createChannel();

		try {
			if (exchangeType == null)
				channel.exchangeDeclarePassive(exchangeName);
			else
				channel.exchangeDeclare(exchangeName, exchangeType, true);
		} catch (final IOException e) {
			log.error(exchangeType == null ? "Exchange does not exist: {}" : "Failed to declare exchange: {}", e);
			throw e;
		}

		channel.confirmSelect();
		channel.addConfirmListener(this::handleAck, this::handleNack);
		connection.addBlockedListener(this::handleBlocked, this::handleUnblocked);
	}

	/**
	 * Send events one by one.
	 */
	private void sendLoop() {
		try {
			do {
				ChangeEvent event;
				while ((event = sendQueue.poll(1000, TimeUnit.MILLISECONDS)) != null) {
					sendOne(event);
				}
			} while (!closed);
		} catch (final InterruptedException e) {
			close();
		}
	}

	/**
	 * Try to send an event (retry on errors, forever, until closed) or drop it.
	 */
	private void sendOne(ChangeEvent event) {
		final BasicProperties props = new BasicProperties();
		final byte[] payload = toJson(event);
		final String routingKey = event.getVault() + ":" + event.getArchive();

		while (true) {
			try {

				final long seqNo = channel.getNextPublishSeqNo();
				channel.basicPublish(exchangeName, routingKey, props, payload);
				synchronized (inFlight) {
					inFlight.put(seqNo, event);
				}
				return;

			} catch (final IOException e) {

				if (closed) {
					drop(event, e);
					return;
				}

				log.warn("Send failed (will reconnect)", e);
				reconnect();
			}
		}

	}

	/**
	 * Try to reconnect (forever, until closed)
	 */
	private void reconnect() {
		do {
			try {
				connect();
			} catch (final Exception e) {
				log.debug("Reconnect failed", e);
			}
		} while (!closed && Utils.sleepInterruptable(reconnectCooldown.toMillis()));
	}

	public boolean enqueueOrDrop(ChangeEvent event, String reason) {
		if (!closed && sendQueue.offer(event))
			return true;
		drop(event, new RuntimeException("Send buffer overflow (" + reason + ")"));
		return false;
	}

	public boolean enqueueOrDrop(ChangeEvent event) {
		return enqueueOrDrop(event, "submit");
	}

	@Override
	public synchronized void close() {
		if (closed)
			return;
		closed = true;

		try {
			sendThread.join();
		} catch (final InterruptedException e) {
			Thread.currentThread().interrupt();
		}

		Utils.closeQuietly(channel);
		Utils.closeQuietly(connection);
		inFlight.values().forEach(event -> drop(event, new RuntimeException("shutdown")));
		inFlight.clear();
	}

	void handleBlocked(String reason) throws IOException {
		log.warn("Connection blocked");
	}

	void handleUnblocked() throws IOException {
		log.info("Connection unblocked");
	}

	void handleNack(long deliveryTag, boolean multiple) throws IOException {
		synchronized (inFlight) {
			if (multiple) {
				final SortedMap<Long, ChangeEvent> failed = inFlight.headMap(deliveryTag + 1);
				failed.values().forEach(event -> drop(event, new IOException("NACK")));
				failed.clear();
			} else {
				final ChangeEvent event = inFlight.remove(deliveryTag);
				if (event != null)
					drop(event, new IOException("NACK"));
			}
		}
	}

	void handleAck(long deliveryTag, boolean multiple) throws IOException {
		synchronized (inFlight) {
			if (multiple)
				inFlight.headMap(deliveryTag + 1).clear();
			else
				inFlight.remove(deliveryTag);
		}
	}

	static byte[] toJson(ChangeEvent event) {
		try {
			return mapper.writeValueAsBytes(event);
		} catch (final IOException e) {
			throw Utils.wtf(e);
		}
	}

	private void drop(ChangeEvent event, Exception e) {
		log.warn("Event dropped: {}", new String(toJson(event), StandardCharsets.UTF_8), e);
	}

}
