package de.gwdg.cdstar.ext.rabbitmq;

import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.event.ChangeEvent;
import de.gwdg.cdstar.runtime.Config;
import de.gwdg.cdstar.runtime.ConfigException;
import de.gwdg.cdstar.runtime.Plugin;
import de.gwdg.cdstar.runtime.RuntimeContext;
import de.gwdg.cdstar.runtime.filter.AbstractEventFilter;
import de.gwdg.cdstar.runtime.listener.RuntimeListener;

@Plugin
public class RabbitMQSink implements RuntimeListener {
	public static final String PROP_BROKER = "broker";
	public static final String PROP_EXCHANGE_NAME = "exchange.name";
	public static final String PROP_EXCHANGE_TYPE = "exchange.type";
	private final Config conf;

	private RabbitMQConnector conn;

	public RabbitMQSink(Config config) throws Exception {
		conf = config;
		conn = new RabbitMQConnector(conf.getURI(PROP_BROKER), conf.get(PROP_EXCHANGE_NAME),
			conf.get(PROP_EXCHANGE_TYPE, null));
	}

	@Override
	public void onInit(RuntimeContext runtime) throws ConfigException {
		runtime.register(new AbstractEventFilter() {
			@Override
			public void triggerEvent(ChangeEvent event) throws Exception {
				onEvent(event);
			}
		});
	}

	void onEvent(ChangeEvent event) {
		conn.enqueueOrDrop(event);
	}

	@Override
	public void onStartup(RuntimeContext ctx) throws Exception {
		conn.start();
	}

	@Override
	public void onShutdown(RuntimeContext ctx) {
		Utils.closeQuietly(conn);
		conn = null;
	}

}
