package de.gwdg.cdstar.ext.rabbitmq;

import java.net.URI;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import de.gwdg.cdstar.Utils;

public class RabbitTest {

	private static final Logger log = LoggerFactory.getLogger(RabbitTest.class);

	public static void main(String[] args) throws Exception {
		final ConnectionFactory factory = new ConnectionFactory();
		factory.setUri(URI.create("amqp://localhost/%2f"));
		factory.setAutomaticRecoveryEnabled(true);

		final Connection rconn = factory.newConnection();
		final Channel rc = rconn.createChannel();
		rc.confirmSelect();
		rc.addConfirmListener((tag, multi) -> log.info("ACK {} {}", tag, multi),
			(tag, multi) -> log.info("NACK {} {}", tag, multi));

		final String exchange = "test";
		rc.exchangeDeclare(exchange, BuiltinExchangeType.FANOUT, false);
		final String queue = rc.queueDeclare().getQueue();
		rc.queueBind(queue, exchange, "");

		while (Utils.sleepInterruptable(10)) {
			rc.basicPublish(exchange, "", null, "Hello World".getBytes());
			log.info("Sent!");
		}

	}
}