package de.gwdg.cdstar.ext.rabbitmq;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.net.ConnectException;
import java.net.URI;
import java.nio.charset.StandardCharsets;

import org.junit.After;
import org.junit.AssumptionViolatedException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.GetResponse;

import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.config.MapConfig;
import de.gwdg.cdstar.event.ChangeEvent;
import de.gwdg.cdstar.runtime.RuntimeContext;
import de.gwdg.cdstar.runtime.filter.AbstractEventFilter;

public class RabbitMQSinkIT {

	private Connection rconn;
	private Channel rc;
	private String exchange;
	private String queue;
	private RabbitMQSink sink;
	private RuntimeContext ctxMock;
	private AbstractEventFilter eventFilter;

	@Before
	public void setUp() throws Exception {
		final String url = Utils.getProperty("it.rabbitmq.url", "amqp://localhost/%2f");

		final ConnectionFactory factory = new ConnectionFactory();
		factory.setUri(URI.create(url));
		try {
			rconn = factory.newConnection();
		} catch (final ConnectException e) {
			throw new AssumptionViolatedException("Failed to connect to: " + url);
		}
		rc = rconn.createChannel();
		exchange = "cdstar-test-" + Utils.bytesToHex(Utils.randomBytes(16));
		rc.exchangeDeclare(exchange, BuiltinExchangeType.FANOUT, false);
		queue = rc.queueDeclare().getQueue();
		rc.queueBind(queue, exchange, "");

		sink = new RabbitMQSink(
			new MapConfig().set(RabbitMQSink.PROP_BROKER, url)
				.set(RabbitMQSink.PROP_EXCHANGE_NAME, exchange));

		ctxMock = Mockito.mock(RuntimeContext.class);
		sink.onInit(ctxMock);
		sink.onStartup(ctxMock);

		final ArgumentCaptor<AbstractEventFilter> argument = ArgumentCaptor.forClass(AbstractEventFilter.class);
		Mockito.verify(ctxMock).register(argument.capture());

		eventFilter = argument.getValue();
		assertNotNull(eventFilter);
	}

	@After
	public void tearDown() throws Exception {
		if (sink != null)
			sink.onShutdown(ctxMock);
		Utils.closeQuietly(rconn);
	}

	@Test
	public void testMessageArrives() throws Exception {
		final ChangeEvent event = new ChangeEvent();
		event.setVault("test");
		event.setArchive("ar123");
		event.setTx("sess123");
		event.setParent("0");
		event.setRevision("1");

		eventFilter.triggerEvent(event);

		assertThat(new String(getOne().getBody(), StandardCharsets.UTF_8)).contains("ar123");
	}

	GetResponse getOne() throws IOException {
		for (int i = 1; i < 10; i++) {
			final GetResponse msg = rc.basicGet(queue, true);
			if (msg != null)
				return msg;
			Utils.sleepInterruptable(100);
		}
		throw new AssertionError("No message in queue");
	}
}
