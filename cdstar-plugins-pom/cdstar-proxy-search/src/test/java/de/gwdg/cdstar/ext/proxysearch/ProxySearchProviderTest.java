package de.gwdg.cdstar.ext.proxysearch;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.net.URI;
import java.net.http.HttpRequest;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import de.gwdg.cdstar.Promise;
import de.gwdg.cdstar.runtime.ConfigException;
import de.gwdg.cdstar.runtime.search.SearchException;
import de.gwdg.cdstar.runtime.search.SearchQuery;
import de.gwdg.cdstar.runtime.search.SearchResult;

public class ProxySearchProviderTest {

	private ProxySearchProvider psp;

	@Before
	public void setUp() throws ConfigException {
		psp = new ProxySearchProvider("test", URI.create("http://example.invalid/"));
	}

	@Test
	public void testPrincipalResolution() throws Exception {
		final SearchQuery sq = SearchQuery.builder()
				.query("q").principal("testP").group("g1").group("g2").build();
		final JsonQuery jq = psp.buildQueryJson(sq);

		assertEquals("q", jq.q);
		assertEquals("testP", jq.principal.name);
		assertEquals(Arrays.asList("g1", "g2"), jq.principal.groups);
	}

	@Test
	public void testAllParameters() throws Exception {
		final SearchQuery sq = SearchQuery.builder()
				.query("q").principal("p").group("g1").group("g2").vault("v").limit(12).order("a", "b").scrollId("s").fields("f1", "f2")
				.build();
		final JsonQuery jq = psp.buildQueryJson(sq);
		assertEquals("q", jq.q);
		assertEquals("v", jq.vault);
		assertEquals(12, jq.limit);
		assertEquals(Arrays.asList("a", "b"), jq.order);
		assertEquals(Arrays.asList("f1", "f2"), jq.fields);
		assertEquals("s", jq.scroll);

		assertEquals("p", jq.principal.name);
		assertEquals(Arrays.asList("g1", "g2"), jq.principal.groups);
	}

	@Test
	public void testNoParameters() throws Exception {
		final SearchQuery sq = SearchQuery.builder().query("q").build();

		final JsonQuery jq = psp.buildQueryJson(sq);
		assertEquals("q", jq.q);
		assertNull(jq.vault);
		assertEquals(0, jq.limit);
		assertEquals(0, jq.order.size());
		assertEquals(0, jq.fields.size());
		assertNull(jq.scroll);
		assertNull(jq.principal.name);
		assertTrue(jq.principal.groups.isEmpty());
	}

	@Test
	public void testConnectionError() throws Exception {
		final Promise<SearchResult> r = psp.search(SearchQuery.builder().query("q").build()).await();
		assertTrue(r.isRejected());
		assertTrue(r.error() instanceof SearchException);
	}

	@Test
	public void testAddHeaders() {
		final SearchQuery sq = SearchQuery.builder().query("q").build();
		final Map<String, String> h = new HashMap<>();
		h.put("X-Custom", "Vaaalue");
		psp.setHeaderMap(h);
		final HttpRequest rs = psp.buildRequest(sq);
		assertEquals("Vaaalue", rs.headers().firstValue("X-Custom").orElse(null));
	}

}
