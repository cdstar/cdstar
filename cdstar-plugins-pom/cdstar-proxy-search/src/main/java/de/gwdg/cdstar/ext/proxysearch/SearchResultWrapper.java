package de.gwdg.cdstar.ext.proxysearch;

import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.databind.JsonNode;

import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.runtime.search.SearchHit;
import de.gwdg.cdstar.runtime.search.SearchResult;
import de.gwdg.cdstar.web.common.model.SearchHits;
import de.gwdg.cdstar.web.common.model.SearchHits.Hit;

class SearchResultWrapper implements SearchResult {

	private List<SearchHit> hits;
	private final SearchHits proxyResult;

	public SearchResultWrapper(SearchHits hits) {
		proxyResult = hits;
	}

	@Override
	public List<SearchHit> hits() {
		if (hits == null)
			hits = Utils.map(proxyResult.getItems(), HitWrapper::new);
		return hits;
	}

	@Override
	public String getScrollID() {
		return proxyResult.getScroll();
	}

	@Override
	public long getTotal() {
		return proxyResult.getTotal();
	}

	private static class HitWrapper implements SearchHit {
		private final Hit proxyHit;

		public HitWrapper(Hit hit) {
			proxyHit = hit;
		}

		@Override
		public String getId() {
			return proxyHit.id;
		}

		@Override
		public String getType() {
			return proxyHit.type;
		}

		@Override
		public double getScore() {
			return proxyHit.score;
		}

		@Override
		public String getName() {
			return proxyHit.name;
		}

		@Override
		public Map<String, JsonNode> getFields() {
			return proxyHit.fields;
		}

	}

}