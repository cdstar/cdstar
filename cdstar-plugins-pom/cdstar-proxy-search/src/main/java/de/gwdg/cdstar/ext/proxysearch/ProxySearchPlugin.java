package de.gwdg.cdstar.ext.proxysearch;

import java.net.URI;

import de.gwdg.cdstar.runtime.Config;
import de.gwdg.cdstar.runtime.Plugin;
import de.gwdg.cdstar.runtime.RuntimeContext;
import de.gwdg.cdstar.runtime.listener.RuntimeListener;
import de.gwdg.cdstar.runtime.search.SearchProvider;

/**
 * This plugin installs a {@link SearchProvider} that forwards search requests
 * to a separate HTTP search gateway.
 */
@Plugin(name = "proxy-search")
public class ProxySearchPlugin implements RuntimeListener {
	private final Config cfg;
	private ProxySearchProvider psearch;

	public ProxySearchPlugin(Config cfg) {
		this.cfg = cfg;
	}

	@Override
	public void onInit(RuntimeContext ctx) throws Exception {
		cfg.setDefault("name", cfg.get("_name"));
		cfg.setDefault("maxconn", "10");

		final URI target = cfg.getURI("target");
		psearch = new ProxySearchProvider(cfg.get("name"), target);
		if (cfg.hasKey("header"))
			psearch.setHeaderMap(cfg.with("header").toMap());
		psearch.setMaxConcurrency(cfg.getInt("maxconn"));
		ctx.register(psearch);
	}

}
