package de.gwdg.cdstar.ext.proxysearch;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpRequest.BodyPublishers;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ConcurrentHashMap;

import com.fasterxml.jackson.core.JsonProcessingException;

import de.gwdg.cdstar.Promise;
import de.gwdg.cdstar.SharedObjectMapper;
import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.runtime.ConfigException;
import de.gwdg.cdstar.runtime.search.SearchException;
import de.gwdg.cdstar.runtime.search.SearchProvider;
import de.gwdg.cdstar.runtime.search.SearchQuery;
import de.gwdg.cdstar.runtime.search.SearchResult;
import de.gwdg.cdstar.web.common.model.ErrorResponse;
import de.gwdg.cdstar.web.common.model.SearchHits;

class ProxySearchProvider implements SearchProvider {

	private final URI target;
	private int maxConcurrency = 10;

	private final Queue<AsyncSearchRequest> waiting = new ArrayBlockingQueue<>(1024);
	private final Set<AsyncSearchRequest> running = ConcurrentHashMap.<AsyncSearchRequest>newKeySet();
	private final String name;
	private HttpClient http;
	private Map<String, String> headers = new HashMap<>();

	public ProxySearchProvider(String name, URI target) throws ConfigException {
		this.name = name;
		this.target = target;
		this.http = HttpClient
			.newBuilder()
			.build();
	}

	public String getName() {
		return name;
	}

	public int getMaxConcurrency() {
		return maxConcurrency;
	}

	public void setMaxConcurrency(int maxConcurrency) {
		this.maxConcurrency = maxConcurrency;
	}

	public void setHeaderMap(Map<String, String> headers) {
		this.headers = headers;
	}

	private class AsyncSearchRequest {
		Promise<SearchResult> promise = Promise.empty();
		SearchQuery query;

		public AsyncSearchRequest(SearchQuery q) {
			query = q;
		}

		public void whenComplete(HttpResponse<InputStream> result, Throwable error) {
			if (error != null) {
				promise.reject(new SearchException("Failed to connect to search backend", error));
				return;
			}

			var code = result.statusCode();
			var body = result.body();
			var ctype = result.headers().firstValue("Content-Type").orElse(null);

			if (code != 200) {
				if ("application/json".equalsIgnoreCase(ctype) && body != null) {
					try (body) {
						promise.reject(SharedObjectMapper.json.readValue(body, ErrorResponse.class));
						return;
					} catch (final IOException e) {
					}
				}
				promise.reject(new SearchException("Unexpected response from search backend",
					new AssertionError("Expected status code 200, got" + code)));
				return;
			}

			try (body) {
				var hits = SharedObjectMapper.json.readValue(body, SearchHits.class);
				promise.resolve(new SearchResultWrapper(hits));
			} catch (UnsupportedOperationException | IOException e) {
				promise.reject(new SearchException("Unexpected response from search backend", e));
			}
		}
	}

	@Override
	public Promise<SearchResult> search(SearchQuery q) {
		final AsyncSearchRequest qs = new AsyncSearchRequest(q);

		if (!waiting.offer(qs))
			return Promise.ofError(new SearchException("Too many requests"));

		tryStartNext();
		return qs.promise;
	}

	/**
	 * Called on new queries or after a query finishes. Responsible to send the next
	 * waiting query, if any.
	 */
	private synchronized void tryStartNext() {
		while (!waiting.isEmpty() && running.size() < maxConcurrency) {
			final AsyncSearchRequest next = waiting.poll();
			running.add(next);
			next.promise.then((r, e) -> {
				running.remove(next);
				tryStartNext();
			});

			try {
				http.sendAsync(buildRequest(next.query), BodyHandlers.ofInputStream())
					.whenComplete(next::whenComplete);
			} catch (final Exception e) {
				next.promise.tryReject(e);
				return;
			}
		}
	}

	HttpRequest buildRequest(SearchQuery q) {
		var rb = HttpRequest
			.newBuilder(target)
			.header("Content-Type", "application/json");
		headers.forEach(rb::header);

		try {
			final byte[] payload = SharedObjectMapper.json.writeValueAsBytes(buildQueryJson(q));
			rb.POST(BodyPublishers.ofByteArray(payload));
		} catch (final JsonProcessingException e) {
			Utils.wtf(e);
		}
		return rb.build();
	}

	JsonQuery buildQueryJson(SearchQuery q) {
		final JsonQuery queryDoc = new JsonQuery();

		// User provided parameters
		queryDoc.q = q.getQuery();
		queryDoc.order = q.getOrder();
		if (q.getLimit() > 0)
			queryDoc.limit = q.getLimit();
		if (Utils.notNullOrEmpty(q.getScrollId()))
			queryDoc.scroll = q.getScrollId();
		queryDoc.fields = q.getFields();

		// Trusted parameters
		queryDoc.vault = q.getVault();
		queryDoc.principal.name = q.getPrincipal();
		queryDoc.principal.groups = new ArrayList<>(q.getGroups());
		return queryDoc;
	}

}
