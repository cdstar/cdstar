package de.gwdg.cdstar.ext.proxysearch;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class JsonQuery {
	public String q;
	public List<String> order;
	public int limit;
	public List<String> fields;
	public String scroll;

	public String vault;
	public PrincipalInfo principal = new PrincipalInfo();

	public static class PrincipalInfo {
		public String name;
		public List<String> groups;
		public boolean privileged;
	}
}
