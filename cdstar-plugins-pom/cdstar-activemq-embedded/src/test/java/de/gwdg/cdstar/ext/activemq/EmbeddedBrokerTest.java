package de.gwdg.cdstar.ext.activemq;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import javax.jms.Connection;
import javax.jms.MessageConsumer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import com.fasterxml.jackson.databind.JsonNode;

import de.gwdg.cdstar.SharedObjectMapper;
import de.gwdg.cdstar.auth.Subject;
import de.gwdg.cdstar.auth.UsernamePasswordCredentials;
import de.gwdg.cdstar.auth.simple.SimpleAuthorizer;
import de.gwdg.cdstar.config.MapConfig;
import de.gwdg.cdstar.runtime.CDStarRuntime;
import de.gwdg.cdstar.runtime.client.CDStarArchive;
import de.gwdg.cdstar.utils.test.TestLogger;

public class EmbeddedBrokerTest {

	@Rule
	public TemporaryFolder testFolder = new TemporaryFolder();

	@Rule
	public TestLogger log = new TestLogger("de.gwdg");

	private CDStarRuntime runtime;

	private ActiveMQBroker broker;

	@Before
	public void setupRuntime() throws Exception {
		final MapConfig cfg = new MapConfig();
		cfg.set("path.home", testFolder.newFolder().toString());
		cfg.set("vault.test.create", "true");
		cfg.set("plugin.activemq.class", ActiveMQBroker.class.getName());
		runtime = CDStarRuntime.bootstrap(cfg);
		runtime.start();
		runtime.lookupRequired(SimpleAuthorizer.class).account("test").password("test").withPermissions("*");

		broker = runtime.lookupRequired(ActiveMQBroker.class);
		assertNotNull(broker);
		assertTrue(broker.getBroker().isStarted());
	}

	@After
	public void stutdownRuntime() {
		runtime.close();
	}

	@Test
	public void basictest() throws Exception {
		final ActiveMQConnectionFactory cf = new ActiveMQConnectionFactory(
			broker.getBroker().getTransportConnectors().get(0).getUri());
		final Connection c = cf.createConnection();
		c.start();
		final Session sess = c.createSession(false, Session.AUTO_ACKNOWLEDGE);
		final MessageConsumer consumer = sess.createConsumer(sess.createTopic("cdstar"));

		final Subject subject = runtime.createSubject();
		subject.login(new UsernamePasswordCredentials("test", "test".toCharArray()));
		final CDStarArchive ar = runtime.getClient(subject).begin(false).getVault("test").createArchive();
		ar.getSession().commit();

		final TextMessage msg = (TextMessage) consumer.receive(1000);
		assertEquals(ar.getVault().getName(), msg.getStringProperty("vault"));

		final JsonNode json = SharedObjectMapper.json.readTree(msg.getText());
		assertEquals(ar.getVault().getName(), json.get("vault").asText());
		assertEquals(ar.getId(), json.get("archive").asText());
		assertEquals("null", json.get("parent").asText());
		assertEquals(ar.getNextRev(), json.get("revision").asText());
	}
}