package de.gwdg.cdstar.ext.activemq;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.TimeUnit;

import javax.jms.Connection;
import javax.jms.DeliveryMode;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.broker.BrokerService;
import org.apache.activemq.broker.TransportConnector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.event.ChangeEvent;
import de.gwdg.cdstar.runtime.Config;
import de.gwdg.cdstar.runtime.Plugin;
import de.gwdg.cdstar.runtime.RuntimeContext;
import de.gwdg.cdstar.runtime.filter.AbstractEventFilter;
import de.gwdg.cdstar.runtime.listener.RuntimeListener;

/**
 * @deprecated Use an external ActiveMQ server.
 */
@Deprecated
@Plugin(name = "amq-embedded")
public class ActiveMQBroker implements RuntimeListener {
	static ObjectMapper mapper = new ObjectMapper();
	static {
		mapper.disable(SerializationFeature.INDENT_OUTPUT);
	}
	private static final String DEFAULT_TRANSPORT = "auto+nio://localhost:5671";

	private static final Logger log = LoggerFactory.getLogger(ActiveMQBroker.class);

	private final BrokerService broker;
	private final Config cfg;

	private volatile boolean closed = false;

	private final int bufferSize;

	private ActiveMQConnectionFactory cf;

	private Connection conn;

	private BlockingQueue<ChangeEvent> sendQueue;

	private final List<String> topics = new ArrayList<>();
	private final List<String> queues = new ArrayList<>();

	private Thread sendThread;

	public ActiveMQBroker(Config config) throws Exception {
		cfg = config;
		cfg.setDefault("buffer", "0");
		cfg.setDefault("topic", "cdstar");

		broker = new BrokerService();
		broker.setBrokerName(cfg.get("name", "cdstar-embedded"));

		bufferSize = cfg.getInt("buffer");
		if (bufferSize > 0)
			sendQueue = new ArrayBlockingQueue<>(bufferSize);
		else if (bufferSize < 0)
			sendQueue = new LinkedBlockingQueue<>();
		else
			sendQueue = new SynchronousQueue<>();

		if (cfg.hasKey("topic"))
			topics.addAll(cfg.getList("topic"));
		if (cfg.hasKey("queue"))
			queues.addAll(cfg.getList("queue"));

	}

	private void addTransport(String name, String value) throws Exception {
		final TransportConnector transport = broker.addConnector(value);
		if (!name.isEmpty())
			transport.setName(name);
		log.info("Added transport: {}", transport);
	}

	@Override
	public void onInit(RuntimeContext runtime) throws Exception {
		broker.setDataDirectory(runtime.getServiceDir("activemq").toString());

		if (!cfg.with("transport").isEmpty()) {
			for (final Entry<String, String> tconf : cfg.with("transport").entrySet()) {
				addTransport(tconf.getKey(), tconf.getValue());
			}
		} else {
			addTransport("default", DEFAULT_TRANSPORT);
		}
		// addTransport("local", "vm://"+broker.getBrokerName());
		broker.start();

		cf = new ActiveMQConnectionFactory("vm://" + broker.getBrokerName() + "?create=false&waitForStart=10000");

		sendThread = new Thread(this::sendLoop);
		sendThread.setName("amq-send-loop");
		sendThread.start();

		log.info("sendThread {}", sendThread);

		runtime.register(new AbstractEventFilter() {
			@Override
			public void triggerEvent(ChangeEvent event) throws Exception {
				sendEvent(event);
			}
		});

	}

	@Override
	public void onShutdown(RuntimeContext ctx) {
		closed = true;
		try {
			broker.stop();
		} catch (final Exception e) {
			throw new RuntimeException(e);
		}
	}

	public BrokerService getBroker() {
		return broker;
	}

	protected void sendEvent(ChangeEvent event) throws InterruptedException {
		log.debug("Send event: ", event);
		sendQueue.put(event);
	}

	static String toJson(ChangeEvent event) {
		try {
			return mapper.writeValueAsString(event);
		} catch (final IOException e) {
			throw Utils.wtf(e);
		}
	}

	/**
	 * JMS requires that a session is only used from a single thread, but cdstar
	 * listeners may be called from any thread. This is why we need a separate
	 * thread to send messages.
	 *
	 * @throws Exception
	 */
	void sendLoop() {
		log.trace("sendLoop");
		ChangeEvent current = null;
		try {
			conn = cf.createConnection();
			final Session sess = conn.createSession(false, Session.CLIENT_ACKNOWLEDGE);

			final List<MessageProducer> prods = new ArrayList<>();
			for (final String name : topics)
				prods.add(sess.createProducer(sess.createTopic(name)));
			for (final String name : queues)
				prods.add(sess.createProducer(sess.createQueue(name)));

			// Default delivery mode is PERSISTENT, ts this is not needed:
			// prods.forEach(p->p.setDeliveryMode(DeliveryMode.PERSISTENT));

			log.trace("sendLoop {}", closed);

			while (!closed) {
				log.trace("loop");
				// Graceful shutdown: Always try to send all remaining events
				// before closing, but if nothing is left in the queue, check if
				// we
				// can close every 1000ms.
				while ((current = sendQueue.poll(1000, TimeUnit.MILLISECONDS)) != null) {
					final TextMessage msg = sess.createTextMessage(toJson(current));
					msg.setStringProperty("vault", current.getVault());
					msg.setJMSDeliveryMode(DeliveryMode.PERSISTENT);
					for (final MessageProducer p : prods)
						p.send(msg); // This will block until the message is
										// delivered and persisted!
					log.debug("Transmitted event: {}", current);
				}
			}
		} catch (final Exception e) {
			log.error("JSM error", e);
		}

		if (current != null)
			log.warn("JSM send queue closed. Unable to deliver event: {}", toJson(current));
		while (!sendQueue.isEmpty()) {
			log.warn("JSM send queue closed. Unable to deliver event: {}", toJson(sendQueue.poll()));
		}
	}

}