# Embedded ActiveMQ Message Broker

This plugin emits change events to an embedded http://activemq.apache.org/[ActiveMQ] message broker.

WARNING: Embedding an ActiveMQ broker is fine for small to medium setups with low traffic and private networks. For production environments it is usually better to run a dedicated message broker with proper configuration and switch to the `cdstar-activemq-sink` or `cdstar-rabbitmq-sink` plugin.


## Configuration

[cols="0v,0v,1"]
|====================
| Pram | Type | Description

| transport.<name> | URI | Network transports to bind to. See http://activemq.apache.org/configuring-transports.html[ActiveMQ docs] for available protocols and URI parameters. The `<name>` part is only used for logging can can be omitted for a single transport.

This plugin bundles all dependencies needed for `OpenWire`, `AMQP`, `STOMP` and `MQTT`. Transports with `vm`, `tcp`, `amqp`, `stomp`, `mqtt` and `auto` schemes as well as their `+ssl` or `+nio` variants can be used directly. Other protocols may need additional dependencies on the class path.

The http://activemq.apache.org/auto.html[auto] transport accepts `OpenWire`, `AMQP`, `STOMP` and `MQTT` clients on the same network port and is recommended in setups with mixed clients.

Default: `auto+nio://127.0.0.1:5671`

| topic | list(str) | Change events are send to the given topics. (Default: `cdstar`)
| queue | list(str) | Same as `topic`, but sends events to a queue.  (Default: _disabled_)

| buffer | int | Size of the send buffer. (Default: _unbound_)

|====================

