package de.gwdg.cdstar.ext.activemq;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.DeliveryMode;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.event.ChangeEvent;
import de.gwdg.cdstar.runtime.Config;
import de.gwdg.cdstar.runtime.ConfigException;
import de.gwdg.cdstar.runtime.RuntimeContext;
import de.gwdg.cdstar.runtime.filter.AbstractEventFilter;
import de.gwdg.cdstar.runtime.listener.RuntimeListener;

public class JmsEventFilterFactory implements RuntimeListener {
	static final ObjectMapper mapper = new ObjectMapper();
	static {
		mapper.disable(SerializationFeature.INDENT_OUTPUT);
	}

	private static final Logger log = LoggerFactory.getLogger(JmsEventFilterFactory.class);

	public static final String PROP_BROKER = "broker";
	public static final String PROP_TOPIC = "topic";
	public static final String PROP_QUEUE = "queue";
	public static final String PROP_QSIZE = "qsize";

	Set<String> topics = new HashSet<>();
	Set<String> queues = new HashSet<>();

	private Thread sendThread;
	private BlockingQueue<ChangeEvent> sendQueue;
	private volatile boolean closed = false;
	private ConnectionFactory factory;
	private final Config conf;
	private Connection conn;
	private int sendQueueSize;
	private String broker;

	public JmsEventFilterFactory(Config config) throws Exception {
		conf = config;
	}

	@Override
	public void onInit(RuntimeContext runtime) throws JMSException, ConfigException {
		broker = conf.get(PROP_BROKER);
		factory = new ActiveMQConnectionFactory(broker);
		conf.setDefault(PROP_QSIZE, "-1");
		sendQueueSize = conf.getInt(PROP_QSIZE);
		if (sendQueueSize > 0)
			sendQueue = new ArrayBlockingQueue<>(sendQueueSize);
		else
			sendQueue = new LinkedBlockingQueue<>();

		if (conf.hasKey(PROP_TOPIC))
			conf.getList(PROP_TOPIC).forEach(topics::add);
		if (conf.hasKey(PROP_QUEUE))
			conf.getList(PROP_QUEUE).forEach(queues::add);

		if (queues.isEmpty() && topics.isEmpty()) {
			log.warn("No topics or queues configured. Events will not be sent.");
			return;
		}

		sendThread = new Thread(this::sendLoop);

		runtime.register(new AbstractEventFilter() {
			@Override
			public void triggerEvent(ChangeEvent event) throws Exception {
				JmsEventFilterFactory.this.triggerEvent(event);
			}
		});
	}

	@Override
	public void onStartup(RuntimeContext ctx) throws Exception {
		sendThread.start();
	}

	@Override
	public void onShutdown(RuntimeContext ctx) {
		try {
			closed = true;
			sendThread.join(1000);
			Utils.closeQuietly(conn::close);
		} catch (final InterruptedException e) {
			sendThread.interrupt();
		}
	}

	static String toJson(ChangeEvent event) {
		try {
			return mapper.writeValueAsString(event);
		} catch (final IOException e) {
			throw Utils.wtf(e);
		}
	}

	public void triggerEvent(ChangeEvent event) {
		if (closed) {
			log.warn("JMS send queue closed. Unable to deliver event: {}", toJson(event));
		} else if (!sendQueue.offer(event)) {
			log.warn("JMS send queue overflow. Unable to deliver event: {}", toJson(event));
		} else if (log.isDebugEnabled()) {
			log.debug("Event queued: {}/{} (qsize={}/{})", event.getVault(), event.getArchive(), sendQueue.size(),
					sendQueueSize);
		}
	}

	void sendLoop() {
		ChangeEvent current = null;
		try {
			log.debug("Connecting to {}", broker);
			conn = factory.createConnection();
			conn.start();
			log.debug("Connected: {}", conn);

			final Session sess = conn.createSession(false, Session.AUTO_ACKNOWLEDGE);
			final List<MessageProducer> prods = new ArrayList<>();

			for (final String name : topics) {
				prods.add(sess.createProducer(sess.createTopic(name)));
			}
			for (final String name : queues) {
				prods.add(sess.createProducer(sess.createQueue(name)));
			}

			for (final MessageProducer p : prods) {
				p.setDeliveryMode(DeliveryMode.PERSISTENT);
			}

			while (!closed) {
				// Graceful shutdown: Always try to send all events before
				// closing.
				while ((current = sendQueue.poll(1000, TimeUnit.MILLISECONDS)) != null) {
					if (log.isTraceEnabled())
						log.trace("Event about to be sent: {}/{}", current.getVault(), current.getArchive());
					final TextMessage msg = sess.createTextMessage(toJson(current));
					msg.setStringProperty("vault", current.getVault());
					for (final MessageProducer p : prods)
						p.send(msg);
					if (log.isDebugEnabled())
						log.debug("Event sent: {}/{}", current.getVault(), current.getArchive());
				}
			}
		} catch (final Exception e) {
			if (current != null)
				log.warn("JSM error while delivering event: {}", toJson(current), e);
		}

		while (!sendQueue.isEmpty()) {
			log.warn("JSM send queue closed. Unable to deliver event: {}", toJson(sendQueue.poll()));
		}
	}

	Connection createConnection() throws JMSException {
		return factory.createConnection();
	}

}
