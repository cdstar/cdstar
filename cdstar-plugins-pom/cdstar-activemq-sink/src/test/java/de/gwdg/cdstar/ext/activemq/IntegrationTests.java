package de.gwdg.cdstar.ext.activemq;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import javax.jms.Connection;
import javax.jms.MessageConsumer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mockito;

import com.fasterxml.jackson.databind.JsonNode;

import de.gwdg.cdstar.SharedObjectMapper;
import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.config.MapConfig;
import de.gwdg.cdstar.event.ChangeEvent;
import de.gwdg.cdstar.runtime.RuntimeContext;
import de.gwdg.cdstar.utils.test.TestLogger;

public class IntegrationTests {
	private JmsEventFilterFactory sink;
	private Connection conn;

	@Rule
	public TestLogger log = new TestLogger("de.gwdg");
	private RuntimeContext ctxMock;

	@Before
	public void setup() throws Exception {
		sink = new JmsEventFilterFactory(
			new MapConfig().set(JmsEventFilterFactory.PROP_BROKER, "vm://localhost?broker.persistent=false")
				.set(JmsEventFilterFactory.PROP_TOPIC, "testTopic"));
		ctxMock = Mockito.mock(RuntimeContext.class);
		sink.onInit(ctxMock);
		sink.onStartup(ctxMock);

		conn = sink.createConnection();
		conn.start();
	}

	@After
	public void tearDown() {
		Utils.closeQuietly(() -> conn.close());
		sink.onShutdown(ctxMock);
	}

	@Test
	public void basictest() throws Exception {
		final ChangeEvent event = new ChangeEvent();
		event.setVault("test");
		event.setArchive("ar123");
		event.setTx("sess123");
		event.setParent("0");
		event.setRevision("1");

		final Session sess = conn.createSession(false, Session.AUTO_ACKNOWLEDGE);
		final MessageConsumer consumer = sess.createConsumer(sess.createTopic("testTopic"));
		sink.triggerEvent(event);

		final TextMessage msg = (TextMessage) consumer.receive(1000);
		assertNotNull(msg);
		assertEquals("test", msg.getStringProperty("vault"));
		final JsonNode json = SharedObjectMapper.json.readTree(msg.getText());
		assertEquals("test", json.get("vault").asText());
		assertEquals("ar123", json.get("archive").asText());
		assertEquals("sess123", json.get("tx").asText());
		assertEquals("1", json.get("revision").asText());
	}
}
