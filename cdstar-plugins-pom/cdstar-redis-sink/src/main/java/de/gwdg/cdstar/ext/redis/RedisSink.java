
package de.gwdg.cdstar.ext.redis;

import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;

import de.gwdg.cdstar.SharedObjectMapper;
import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.event.ChangeEvent;
import de.gwdg.cdstar.runtime.Config;
import de.gwdg.cdstar.runtime.ConfigException;
import de.gwdg.cdstar.runtime.Plugin;
import de.gwdg.cdstar.runtime.RuntimeContext;
import de.gwdg.cdstar.runtime.filter.AbstractEventFilter;
import de.gwdg.cdstar.runtime.listener.RuntimeListener;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

@Plugin
public class RedisSink implements RuntimeListener {
	private static final Logger log = LoggerFactory.getLogger(RedisSink.class);

	public static final String PROP_URI = "uri";
	public static final String PROP_KEY = "key";
	public static final String PROP_MODE = "mode";
	public static final String PROP_QSIZE = "qsize";
	private final Config conf;

	private final URI redisUri;
	private final byte[] listKey;
	private final JedisPool pool;
	private final Mode mode;
	private final BlockingQueue<ChangeEvent> eventQueue;
	private Thread sendThread;

	private volatile boolean pleaseStop;

	enum Mode {
		RPUSH, LPUSH, PUBLISH
	}

	public RedisSink(Config config) throws Exception {
		conf = config;
		conf.setDefault(PROP_URI, "redis://localhost:6379/0");
		conf.setDefault(PROP_KEY, "cdstar.events");
		conf.setDefault(PROP_MODE, "RPUSH");
		conf.setDefault(PROP_QSIZE, "1024");

		redisUri = conf.getURI(PROP_URI);
		listKey = conf.get(PROP_KEY).getBytes(StandardCharsets.UTF_8);
		mode = Mode.valueOf(conf.get(PROP_MODE));
		int qsize = conf.getInt(PROP_QSIZE);
		if (qsize <= 0)
			qsize = Integer.MAX_VALUE; // practically unbounded
		eventQueue = (qsize <= 1024) ? new ArrayBlockingQueue<>(qsize) : new LinkedBlockingQueue<>(qsize);

		pool = new JedisPool(new JedisPoolConfig(), redisUri);
		sendThread = new Thread(this::sendLoop, "redis-sink-send");
	}

	@Override
	public void onInit(RuntimeContext runtime) throws ConfigException {
		runtime.register(new AbstractEventFilter() {
			@Override
			public void triggerEvent(ChangeEvent event) throws Exception {
				enqueueEvent(event);
			}

		});
	}

	@Override
	public void onStartup(RuntimeContext ctx) throws Exception {
		sendThread.start();
	}

	@Override
	public void onShutdown(RuntimeContext ctx) {

		// Request sendThread to stop once all events are sent.
		pleaseStop = true;

		try {
			// Wait for sendThread to terminate gracefully
			for (int i = 0; i < 10 && sendThread.isAlive(); i++) {
				log.debug("Waiting for thread {} to terminate.", sendThread, eventQueue.size());
				sendThread.join(TimeUnit.SECONDS.toMillis(1));
			}

			// Stop sendThread the not-so-nice way
			if (sendThread.isAlive()) {
				log.warn("Thread {} did not terminate in time. Interrupting...", sendThread, eventQueue.size());
				sendThread.interrupt();
				sendThread.join(0);
			}
		} catch (InterruptedException e) {
			// The shutdown thread itself got interrupted. Hurry up!
		} finally {
			Utils.closeQuietly(pool);
		}

		// Log and drop unsent events
		ChangeEvent event;
		while ((event = eventQueue.poll()) != null)
			log.warn("Event dropped (shutdown): vault={} archive={} revision={}", event.getVault(), event.getArchive(),
					event.getRevision());
	}

	private void enqueueEvent(ChangeEvent event) {
		if (!eventQueue.offer(event))
			log.warn("Event dropped (queue overflow): vault={} archive={} revision={}", event.getVault(),
					event.getArchive(), event.getRevision());
	}

	/**
	 * Send events until the thread is interrupted, or {@link #pleaseStop} is true
	 * and the queue runs dry.
	 */
	private void sendLoop() {
		try {
			while (!Thread.interrupted()) {

				ChangeEvent event;
				while ((event = eventQueue.poll(1, TimeUnit.SECONDS)) == null)
					if (pleaseStop)
						return;

				try {
					sendOne(event);
				} catch (Exception e) {
					// TODO: Sleep a short time and retry on network errors.
					log.warn("Event dropped (error): vault={} archive={} revision={}", event.getVault(),
							event.getArchive(), event.getRevision(), e);
				}
			}
		} catch (InterruptedException e) {
			return;
		}
	}

	private void sendOne(ChangeEvent event) throws JsonProcessingException {
		byte[] payload = SharedObjectMapper.json_compact.writeValueAsBytes(event);

		try (Jedis jedis = pool.getResource()) {
			switch (mode) {
			case RPUSH:
				jedis.rpush(listKey, payload);
				// TODO: Warn if list grows larger than a watermark
				break;
			case LPUSH:
				jedis.lpush(listKey, payload);
				// TODO: Warn if list grows larger than a watermark
				break;
			case PUBLISH:
				jedis.publish(listKey, payload);
				// TODO: Warn if no subscribers recieved this event
				break;
			}
		}
	}

}
