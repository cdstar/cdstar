package de.gwdg.cdstar.ext.auth.jwt;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.interfaces.ECPrivateKey;
import java.security.interfaces.ECPublicKey;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Date;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.exceptions.SignatureVerificationException;
import com.auth0.jwt.exceptions.TokenExpiredException;
import com.auth0.jwt.interfaces.RSAKeyProvider;

import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.auth.StringPermission;
import de.gwdg.cdstar.auth.realm.CheckResult;
import de.gwdg.cdstar.config.MapConfig;
import de.gwdg.cdstar.runtime.ConfigException;
import de.gwdg.cdstar.utils.test.TestLogger;
import de.gwdg.cdstar.utils.test.TestUtils;

public class JWTTest {

	@Rule
	public TestLogger log = new TestLogger("de.gwdg");

	private static Algorithm RS256;
	private static Algorithm RS256JWKS;
	private static Path rsaPubFile;
	private static Path ecdsaPubFile;
	private static Algorithm ES256;
	private static byte[] hmacSecret;
	private static Algorithm HS256;

	@BeforeClass
	public static void makeKeys() throws Exception {

		rsaPubFile = Paths.get(JWTTest.class.getResource("/rsa_pub.der").toURI());
		var privFile = Paths.get(JWTTest.class.getResource("/rsa_priv.der").toURI());
		var keyFactory = KeyFactory.getInstance("RSA");
		var pubSpec = new X509EncodedKeySpec(Files.readAllBytes(rsaPubFile));
		var rsaPubKey = (RSAPublicKey) keyFactory.generatePublic(pubSpec);
		var privSpec = new PKCS8EncodedKeySpec(Files.readAllBytes(privFile));
		var rsaPrivKey = (RSAPrivateKey) keyFactory.generatePrivate(privSpec);
		RS256 = Algorithm.RSA256(rsaPubKey, rsaPrivKey);

		RS256JWKS = Algorithm.RSA256(new RSAKeyProvider() {

			@Override
			public RSAPublicKey getPublicKeyById(String keyId) {
				return rsaPubKey;
			}

			@Override
			public String getPrivateKeyId() {
				return "test-key";
			}

			@Override
			public RSAPrivateKey getPrivateKey() {
				return rsaPrivKey;
			}
		});

		ecdsaPubFile = Paths.get(JWTTest.class.getResource("/ecdsa_pub.der").toURI());
		privFile = Paths.get(JWTTest.class.getResource("/ecdsa_priv.der").toURI());
		keyFactory = KeyFactory.getInstance("EC");
		pubSpec = new X509EncodedKeySpec(Files.readAllBytes(ecdsaPubFile));
		var ecPubKey = (ECPublicKey) keyFactory.generatePublic(pubSpec);
		privSpec = new PKCS8EncodedKeySpec(Files.readAllBytes(privFile));
		var ecPrivKey = (ECPrivateKey) keyFactory.generatePrivate(privSpec);
		ES256 = Algorithm.ECDSA256(ecPubKey, ecPrivKey);

		hmacSecret = "secret".getBytes();
		HS256 = Algorithm.HMAC256(hmacSecret);

	}

	private JWTRealm realm;
	private MapConfig cfg;

	@Before
	public void setupRealm() throws IllegalArgumentException, UnsupportedEncodingException, ConfigException {
		cfg = new MapConfig();
		cfg.set("_name", "jwt");
		cfg.set("test.rsa", rsaPubFile.toString());
		cfg.set("test.ecdsa", ecdsaPubFile.toString());
		cfg.set("test.hmac", Utils.base64encode(hmacSecret));
		cfg.set("test.jwks", JWTTest.class.getResource("/jwks.json").toString());
		cfg.set("test.iss", "TEST ISS");
		cfg.set("test.domain", "testDomain");
		realm = new JWTRealm(cfg);
	}

	@Test
	public void testSignaturesAlgorithms() throws Exception {
		for (final Algorithm alg : new Algorithm[] { HS256, RS256, ES256 }) {
			final String token = JWT.create().withIssuer("TEST ISS").withSubject(alg.getName()).sign(alg);
			assertEquals(realm.fromToken(token).getId(), alg.getName());
		}
	}

	private boolean checkPermitted(JWTPrincipal jwt, String p) {
		return jwt.isPermitted(StringPermission.parse(p)) == CheckResult.YES;
	}

	private void assertPermitted(JWTPrincipal jwt, String p) {
		assertTrue(checkPermitted(jwt, p));
	}

	private void assertNotPermitted(JWTPrincipal jwt, String p) {
		assertFalse(checkPermitted(jwt, p));
	}

	@Test
	public void testStaticPermit() throws Exception {
		cfg.set("test.permit", "vault:gVault:*,archive:vVault:*:*,third:party");
		realm = new JWTRealm(cfg);

		final String token = JWT.create()
				.withIssuer("TEST ISS")
				.withSubject("Bob")
				.sign(HS256);

		final JWTPrincipal jwt = realm.fromToken(token);

		assertPermitted(jwt, "vault:gVault:read");
		assertPermitted(jwt, "vault:gVault:create");
		assertNotPermitted(jwt, "vault:vVault:read");
		assertPermitted(jwt, "archive:vVault:abc:read_acl");
		assertPermitted(jwt, "third:party");
	}

	@Test
	public void testTrustedIssuer() throws Exception {
		// This indirectly also tests SpEL patterns

		final String token = JWT.create()
				.withIssuer("TEST ISS")
				.withSubject("Bob")
				.withArrayClaim("cdstar:read", new String[] { "rVault" })
				.withArrayClaim("cdstar:create", new String[] { "cVault" })
				.withArrayClaim("cdstar:grant", new String[] { "vault:gVault:*", "archive:vVault:*:*", "not:allowed" })
				.sign(HS256);

		cfg = new MapConfig();
		cfg.set("_name", "jwt");
		cfg.set("test.hmac", Utils.base64encode(hmacSecret));
		cfg.set("test.trusted", "true");
		cfg.set("test.iss", "TEST ISS");
		final JWTRealm trustedRealm = new JWTRealm(cfg);

		final JWTPrincipal jwt = trustedRealm.fromToken(token);

		// Support cdstar:read claim
		assertPermitted(jwt, "vault:rVault:read");
		assertNotPermitted(jwt, "vault:rVault:create");

		// Support cdstar:create claim
		assertPermitted(jwt, "vault:cVault:read");
		assertPermitted(jwt, "vault:cVault:create");

		// Support cdstar:grant claim
		assertPermitted(jwt, "vault:gVault:read");
		assertPermitted(jwt, "vault:gVault:create");
		assertNotPermitted(jwt, "vault:vVault:read");
		assertPermitted(jwt, "archive:vVault:abc:read_acl");
		// Silently ignore illegal grants in cdstar:grant
		assertNotPermitted(jwt, "not:allowed");

		// Ignore these claims in untrusted issuers
		assertTrue(realm.fromToken(token).getPermissions().isEmpty());
	}

	@Test
	public void testWrongIssuer() throws Exception {
		final String token = JWT.create().withIssuer("test").withSubject("bob").sign(RS256);
		TestUtils.assertRaises(JWTVerificationException.class, () -> {
			realm.fromToken(token);
		});
	}

	@Test
	public void testNotSigned() throws Exception {
		final String token = JWT.create().withIssuer("TEST ISS").withSubject("bob").sign(Algorithm.none());
		TestUtils.assertRaises(JWTVerificationException.class, () -> {
			realm.fromToken(token);
		});
	}

	@Test
	public void testWrongSigningKey() throws Exception {
		final KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
		keyGen.initialize(512);
		final KeyPair pair = keyGen.genKeyPair();
		final Algorithm algo = Algorithm.RSA256((RSAPublicKey) pair.getPublic(), (RSAPrivateKey) pair.getPrivate());
		final String token = JWT.create().withIssuer("TEST ISS").withSubject("bob").sign(algo);

		TestUtils.assertRaises(SignatureVerificationException.class, () -> {
			realm.fromToken(token);
		});
	}

	@Test
	public void testExpired() throws Exception {
		final String okay = JWT.create()
				.withIssuer("TEST ISS")
				.withSubject("test")
				.withExpiresAt(new Date(System.currentTimeMillis() + 1000))
				.sign(HS256);
		assertEquals("test", realm.fromToken(okay).getId());

		final String invalid = JWT.create()
				.withIssuer("TEST ISS")
				.withSubject("test")
				.withExpiresAt(new Date(System.currentTimeMillis() - 1000))
				.sign(HS256);
		TestUtils.assertRaises(TokenExpiredException.class, () -> realm.fromToken(invalid));
	}

	@Test
	public void testNotBefore() throws Exception {
		final String okay = JWT.create()
				.withIssuer("TEST ISS")
				.withSubject("test")
				.withNotBefore(new Date(System.currentTimeMillis() - 10))
				.sign(HS256);
		assertEquals("test", realm.fromToken(okay).getId());

		final String invalid = JWT.create()
				.withIssuer("TEST ISS")
				.withSubject("test")
				.withNotBefore(new Date(System.currentTimeMillis() + 5000))
				.sign(HS256);
		TestUtils.assertRaises(TokenExpiredException.class, () -> realm.fromToken(invalid));
	}

	@Test
	public void testJWKS() throws Exception {
		final String okay = JWT.create()
				.withIssuer("TEST ISS")
				.withSubject("test")
				.withNotBefore(new Date(System.currentTimeMillis() - 10))
				.sign(RS256JWKS);
		assertEquals("test", realm.fromToken(okay).getId());
	}

}
