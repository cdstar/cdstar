#!/bin/sh

name=${1:-test}

# Make RSA keys
openssl genrsa -out rsa.pem 4096
# Java can read this via CertificateFactory
#openssl rsa -in rsa.pem -out rsa_pub.pem -pubout
# Java can read this directly (X509EncodedKeySpec)
openssl rsa -in rsa.pem -outform DER -out rsa_pub.der  -pubout
# Java can read this directly (PKCS8EncodedKeySpec)
openssl pkcs8 -topk8 -inform PEM -outform DER -in rsa.pem -out rsa_priv.der -nocrypt


# Make ECDSA keys
openssl ecparam -genkey -name secp256r1 -out ecdsa.pem
# Java can read this via CertificateFactory
#openssl ec -in ecdsa.pem -out ecdsa_pub.pem -pubout
# Java can read this directly (X509EncodedKeySpec)
openssl ec -in ecdsa.pem -outform DER -out ecdsa_pub.der -pubout
# Java can read this directly (PKCS8EncodedKeySpec)
openssl pkcs8 -topk8 -inform PEM -outform DER -in ecdsa.pem -out ecdsa_priv.der -nocrypt

