package de.gwdg.cdstar.ext.auth.jwt;

import java.util.List;
import java.util.Map;

import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;

/**
 * Root context for dynamic JWT verify or extractor expressions.
 */
public final class ELRootContext {
	private final DecodedJWT jwt;

	ELRootContext(DecodedJWT jwt) {
		this.jwt = jwt;
	}

	public Map<String, Claim> getClaims() {
		return jwt.getClaims();
	}

	public Claim getClaim(String name) {
		return jwt.getClaim(name);
	}

	public boolean hasClaim(String name) {
		return jwt.getClaims().containsKey(name);
	}

	public boolean getBool(String name, boolean defaultValue) {
		return getClaim(name, Boolean.class, defaultValue);
	}

	public long getLong(String name, long defaultValue) {
		return getClaim(name, Long.class, defaultValue);
	}

	public double getDouble(String name, double defaultValue) {
		return getClaim(name, Double.class, defaultValue);
	}

	public String getString(String name, String defaultValue) {
		return getClaim(name, String.class, defaultValue);
	}

	public List<String> getStringList(String name) {
		return getClaimList(name, String.class);
	}

	/**
	 * Return a single claim value or the first value of an array claim. On any
	 * errors (claim missing, null, wrong type) return null;
	 */
	public <T> T getClaim(String name, Class<T> type, T defaultValue) {
		var claim = jwt.getClaim(name);
		if (claim.isNull())
			return defaultValue;
		try {
			return claim.as(type);
		} catch (JWTDecodeException e) {
			try {
				var multi = claim.asList(type);
				if (multi == null || multi.isEmpty())
					return defaultValue;
				return multi.get(0);
			} catch (JWTDecodeException e2) {
				return defaultValue;
			}
		}
	}

	/**
	 * Return all claim values as a list. On any errors (claim missing, null, wrong
	 * type) return an empty list;
	 */
	public <T> List<T> getClaimList(String name, Class<T> type) {
		var claim = jwt.getClaim(name);
		if (claim.isNull())
			return List.of();
		try {
			var result = claim.asList(type);
			if (result == null) {
				// Single value fallback
				return List.of(claim.as(type));
			}
			return result;
		} catch (JWTDecodeException | NullPointerException e) {
			return List.of();
		}
	}
}