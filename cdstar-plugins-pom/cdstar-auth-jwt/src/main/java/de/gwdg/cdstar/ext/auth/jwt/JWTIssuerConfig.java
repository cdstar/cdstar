package de.gwdg.cdstar.ext.auth.jwt;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.springframework.expression.EvaluationException;
import org.springframework.expression.Expression;
import org.springframework.expression.ParseException;
import org.springframework.expression.spel.SpelCompilerMode;
import org.springframework.expression.spel.SpelParserConfiguration;
import org.springframework.expression.spel.standard.SpelExpressionParser;

import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.exceptions.TokenExpiredException;
import com.auth0.jwt.interfaces.DecodedJWT;

import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.auth.Permission;
import de.gwdg.cdstar.auth.StringPermission;
import de.gwdg.cdstar.auth.simple.QName;
import de.gwdg.cdstar.runtime.Config;
import de.gwdg.cdstar.runtime.ConfigException;

class JWTIssuerConfig {
	private final String name;
	private final KeyStore keys;

	private final String iss;
	private final String domain;
	private final int leeway;

	private final Set<StringPermission> staticPermits = new HashSet<>();
	private final Set<QName> staticGroups = new HashSet<>();

	private final Expression subjectExtractor;
	private final Map<String, Expression> verifyRules = new HashMap<>();
	private final Map<String, Expression> permissionExtractors = new HashMap<>();
	private final Map<String, Expression> groupExtractors = new HashMap<>();

	private final JWTRealm realm;

	public JWTIssuerConfig(JWTRealm realm, String name, Config conf) throws ConfigException {
		this.realm = realm;
		this.name = name;
		conf.setDefault("leeway", "0");
		iss = conf.get("iss", name);
		domain = conf.get("domain", name);
		leeway = conf.getInt("leeway");

		if (conf.getBool("trusted")) {
			JWTRealm.log.warn("Trusted issuers are deprectaed. Falling back to equivalent dynamic token rules ...");

			// Add `cdstar:groups` to list of groups.
			conf.set("groups._trusted", "getStringList('cdstar:groups')");
			// Allow read access to all vaults in `cdstar:read`
			conf.set("permit._trusted_read", "getStringList('cdstar:read').!['vault:' + #this + ':read']");
			// Allow create+read access to all vaults in `cdstar:create`
			conf.set("permit._trusted_create", "getStringList('cdstar:create').!['vault:' + #this + ':create']");
			conf.set("permit._trusted_create_read", "getStringList('cdstar:create').!['vault:' + #this + ':read']");
			// Grant all vault and archive permissions in `cdstar:grant`
			conf.set("permit._trusted_grant",
					"getStringList('cdstar:grant').?[#this.startsWith('vault:') or #this.startsWith('archive:')]");
		}

		if (conf.hasKey("permit"))
			conf.getList("permit").forEach(p -> staticPermits.add(StringPermission.parse(p)));
		if (conf.hasKey("groups"))
			conf.getList("groups").forEach(g -> staticGroups.add(QName.fromString(g, domain)));

		try {
			var parser = new SpelExpressionParser(
					new SpelParserConfiguration(SpelCompilerMode.MIXED, getClass().getClassLoader()));

			subjectExtractor = conf.hasKey("subject") ? parser.parseExpression(conf.get("subject")) : null;

			conf.with("verify").toMap().forEach((ruleName, ruleExpression) -> {
				if (ruleName.isBlank() || ruleExpression.isBlank())
					return;
				verifyRules.put(ruleName, parser.parseExpression(ruleExpression));
			});
			conf.with("groups").toMap().forEach((ruleName, ruleExpression) -> {
				if (ruleName.isBlank() || ruleExpression.isBlank())
					return;
				groupExtractors.put(ruleName, parser.parseExpression(ruleExpression));
			});
			conf.with("permit").toMap().forEach((ruleName, ruleExpression) -> {
				if (ruleName.isBlank() || ruleExpression.isBlank())
					return;
				permissionExtractors.put(ruleName, parser.parseExpression(ruleExpression));
			});

		} catch (ParseException e) {
			throw new ConfigException("Failed to parse dynamic expression", e);
		}

		keys = new KeyStore(conf.hasKey("jwks") ? conf.getURI("jwks") : null);
		keys.refreshWebKeys();

		if (conf.hasKey("hmac"))
			keys.addHMAC(null, Utils.base64decode(conf.get("hmac")));
		if (conf.hasKey("rsa"))
			keys.addRSA(null, loadKey("RSA", conf.get("rsa")));
		if (conf.hasKey("ecdsa"))
			keys.addECDSA(null, loadKey("EC", conf.get("ecdsa")));
	}

	public String getIss() {
		return iss;
	}

	public String getName() {
		return name;
	}

	public String getDomain() {
		return domain;
	}

	@SuppressWarnings("unchecked")
	private <T extends PublicKey> T loadKey(String type, String location) throws ConfigException {
		try {
			if (location.endsWith(".pem") || location.endsWith(".pub")) {
				final X509Certificate cer = (X509Certificate) CertificateFactory.getInstance("X.509")
						.generateCertificate(Files.newInputStream(Paths.get(location)));
				return (T) cer.getPublicKey();
			} else if (location.endsWith(".der")) {
				final X509EncodedKeySpec pubSpec = new X509EncodedKeySpec(Files.readAllBytes(Paths.get(location)));
				return (T) KeyFactory.getInstance(type).generatePublic(pubSpec);
			} else {
				final byte[] keyBytes = Utils.base64decode(location);
				final X509EncodedKeySpec pubSpec = new X509EncodedKeySpec(keyBytes);
				return (T) KeyFactory.getInstance(type).generatePublic(pubSpec);
			}
		} catch (CertificateException | InvalidKeySpecException | NoSuchAlgorithmException | IOException e) {
			throw new ConfigException("Failed to load pubic keys", e);
		}
	}

	void verify(DecodedJWT jwt) {
		keys.verify(jwt);
		checkExpired(jwt);

		if (!verifyRules.isEmpty()) {
			var context = new ELRootContext(jwt);
			for (var e : verifyRules.entrySet()) {
				var ruleName = e.getKey();
				var ruleExpression = e.getValue();
				try {
					var result = ruleExpression.getValue(context, Boolean.class);
					if (result == null || !result.booleanValue())
						throw new JWTVerificationException("Token verification failed for rule: " + ruleName);
				} catch (EvaluationException err) {
					throw new JWTVerificationException("Token verification failed because of evaluation error in rule: " + ruleName, err);
				}
			}
		}
	}

	JWTPrincipal fromToken(DecodedJWT jwt) throws JWTVerificationException {
		verify(jwt);

		String subject = jwt.getSubject();
		Set<Permission> permissions = new HashSet<>(staticPermits);
		Set<QName> groups = new HashSet<>(staticGroups);

		var elContext = new ELRootContext(jwt);

		if (subjectExtractor != null) {
			try {
				subject = subjectExtractor.getValue(elContext, String.class);
				JWTRealm.log.debug("Subject expression evaluated to {}", subject);
			} catch (EvaluationException err) {
				throw new JWTVerificationException("Subject expression failed", err);
			}
		}

		if (subject == null || subject.isBlank())
			throw new JWTVerificationException("Token subject missing or blank");

		groupExtractors.forEach((ruleName, rule) -> {
			try {
				Object result = rule.getValue(elContext);
				JWTRealm.log.debug("Group expression {} evaluated to {}", ruleName, result);
				if (result instanceof String)
					groups.add(QName.fromString((String) result, domain));
				else if (result instanceof Iterable<?>)
					for (Object val : (Iterable<?>) result)
						groups.add(QName.fromString((String) val, domain));
			} catch (EvaluationException | ClassCastException err) {
				JWTRealm.log.warn("Group expression {} failed", ruleName, err);
			}
		});

		permissionExtractors.forEach((ruleName, rule) -> {
			try {
				Object result = rule.getValue(elContext);
				JWTRealm.log.debug("Permission expression {} evaluated to {}", ruleName, result);
				if (result instanceof String)
					permissions.add(StringPermission.parse((String) result));
				else if (result instanceof Iterable<?>)
					for (Object val : (Iterable<?>) result)
						permissions.add(StringPermission.parse((String) val));
			} catch (EvaluationException | ClassCastException err) {
				JWTRealm.log.warn("Permission expression {} failed", ruleName, err);
			}
		});

		return new JWTPrincipal(realm, jwt, this, subject, groups, permissions);
	}

	void checkExpired(DecodedJWT jwt) {
		final long now = System.currentTimeMillis() / 1000;
		final Date notBefore = jwt.getNotBefore();
		final Date exporesAt = jwt.getExpiresAt();
		if (notBefore != null && notBefore.getTime() / 1000 > now + leeway)
			throw new TokenExpiredException("Token rejected based on 'nbf' (Not BeFore) claim.");
		if (exporesAt != null && exporesAt.getTime() / 1000 < now - leeway)
			throw new TokenExpiredException("Token rejected based on 'exp' (Expiration Time) claim.");
	}

}
