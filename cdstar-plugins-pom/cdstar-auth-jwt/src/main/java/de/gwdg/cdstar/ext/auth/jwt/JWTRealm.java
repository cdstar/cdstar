package de.gwdg.cdstar.ext.auth.jwt;

import java.io.UnsupportedEncodingException;
import java.time.Duration;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.auth0.jwt.JWT;
import com.auth0.jwt.exceptions.AlgorithmMismatchException;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.exceptions.SignatureVerificationException;
import com.auth0.jwt.exceptions.TokenExpiredException;
import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;

import de.gwdg.cdstar.Result;
import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.auth.Credentials;
import de.gwdg.cdstar.auth.Permission;
import de.gwdg.cdstar.auth.Session;
import de.gwdg.cdstar.auth.TokenCredentials;
import de.gwdg.cdstar.auth.realm.Authenticator;
import de.gwdg.cdstar.auth.realm.Authorizer;
import de.gwdg.cdstar.auth.realm.CheckResult;
import de.gwdg.cdstar.auth.realm.GroupResolver;
import de.gwdg.cdstar.runtime.Config;
import de.gwdg.cdstar.runtime.ConfigException;
import de.gwdg.cdstar.runtime.Plugin;

/**
 * A JWT Realm accepts JWT tokens as an authentication and optionally
 * authorization mechanism. Multiple issuers with different secrets can be
 * configured. The authenticated subjects will have statically configured
 * permissions and group memberships. Trusted issuers may inject additional
 * permits and groups via JWT custom claims within the token.
 */
@Plugin(name = "jwt")
public class JWTRealm implements Authenticator, Authorizer, GroupResolver {
	static final Logger log = LoggerFactory.getLogger(JWTRealm.class);

	private final Map<String, JWTIssuerConfig> issuerConfigs = new HashMap<>();
	private JWTIssuerConfig defaultIssuer;
	private final String realmName;

	private final Cache<String, Result<JWTPrincipal, JWTVerificationException>> cache;

	public JWTRealm(Config cfg) throws ConfigException, IllegalArgumentException, UnsupportedEncodingException {
		realmName = cfg.get("_name");
		for (final Entry<String, Config> e : cfg.getTable().entrySet()) {
			if (e.getKey().equals("class"))
				continue;
			final JWTIssuerConfig ic = new JWTIssuerConfig(this, e.getKey(), e.getValue());
			issuerConfigs.put(ic.getIss(), ic);
			if (e.getKey().equals("default"))
				defaultIssuer = ic;
		}

		// TODO: Make this configurable
		cache = Caffeine.newBuilder().maximumSize(1024).expireAfterWrite(Duration.ofSeconds(60)).build();
	}

	@Override
	public String getName() {
		return realmName;
	}

	/**
	 * Calls {@link #fromToken(String)} for {@link TokenCredentials} and swallows
	 * any errors.
	 */
	@Override
	public Session login(Credentials request) {
		if (!(request instanceof TokenCredentials))
			return null;

		final String token = ((TokenCredentials) request).getToken();

		// Fail fast if token is does not contain exactly two dots.
		int i;
		if ((i = token.indexOf('.')) == -1 || (i = token.indexOf('.', i + 1)) == -1 || token.indexOf('.', i + 1) > -1)
			return null;

		try {
			return fromToken(token);
		} catch (final JWTDecodeException e) {
			if (log.isDebugEnabled())
				log.debug("Token failed to parse as JWT: {}", Utils.repr(token));
			return null;
		} catch (final Exception e) {
			log.warn("JWT verification failed", e);
			return null;
		}

	}

	/**
	 * Turn a string JWT token into a principal.
	 *
	 * @throws JWTDecodeException             if token is not a JWT token.
	 * @throws JWTVerificationException       if the tokens issuer is undefined or
	 *                                        unknown.
	 * @throws AlgorithmMismatchException     if the signature algorithm does not
	 *                                        match the one configured for the
	 *                                        issuer.
	 * @throws SignatureVerificationException if the Token's signature is invalid
	 * @throws TokenExpiredException          if the token is expired or if the
	 *                                        'nbf' (not before) claim is in the
	 *                                        future.
	 *
	 */

	public JWTPrincipal fromToken(String token) throws JWTVerificationException {

		var result = cache.get(token, t -> {
			try {
				var jwt = JWT.decode(token);
				var issuer = jwt.getIssuer() == null ? defaultIssuer : issuerConfigs.get(jwt.getIssuer());
				if (issuer == null)
					throw new JWTVerificationException("Unknown issuer");
				return Result.of(issuer.fromToken(jwt));
			} catch (JWTVerificationException e) {
				return Result.error(e);
			} catch (RuntimeException e) {
				return Result.error(new JWTVerificationException("Internal error while processing JWT", e));
			}
		});

		var principal = result.unwrap(); // or re-throw original error
		principal.checkExpired();

		if (log.isDebugEnabled())
			log.debug("Authenticated JWT principal: {}", principal);

		return principal;
	}

	@Override
	public void logout(Session who) {
		// Not implemented for JWT
	}

	@Override
	public CheckResult isPermitted(Session session, Permission p) {
		if (!(session instanceof JWTPrincipal))
			return CheckResult.UNKNOWN;

		return ((JWTPrincipal) session).isPermitted(p);
	}

	@Override
	public CheckResult isMemberOf(Session session, String groupName, String groupDomain) {
		if (!(session instanceof JWTPrincipal))
			return CheckResult.UNKNOWN;

		return ((JWTPrincipal) session).isMemberOf(groupName, groupDomain);
	}

}
