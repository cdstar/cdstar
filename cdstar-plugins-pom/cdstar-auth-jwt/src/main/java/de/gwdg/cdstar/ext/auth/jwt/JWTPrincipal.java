package de.gwdg.cdstar.ext.auth.jwt;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.auth0.jwt.exceptions.TokenExpiredException;
import com.auth0.jwt.interfaces.DecodedJWT;

import de.gwdg.cdstar.auth.Permission;
import de.gwdg.cdstar.auth.Principal;
import de.gwdg.cdstar.auth.Session;
import de.gwdg.cdstar.auth.StringPermission;
import de.gwdg.cdstar.auth.realm.Authenticator;
import de.gwdg.cdstar.auth.realm.CheckResult;
import de.gwdg.cdstar.auth.simple.QName;

class JWTPrincipal implements Principal, Session {

	private final JWTRealm parent;
	private final DecodedJWT jwt;
	private Set<Permission> permissions = new HashSet<>();
	private final JWTIssuerConfig issuer;
	private final Map<String, String> properties = new HashMap<>();
	private Set<QName> groups = new HashSet<>();
	private String subject;

	public JWTPrincipal(JWTRealm parent, DecodedJWT jwt, JWTIssuerConfig issuer, String subject, Set<QName> groups,
			Set<Permission> permissions) {
		this.parent = parent;
		this.jwt = jwt;
		this.issuer = issuer;
		this.subject = subject;
		this.permissions.addAll(permissions);
		this.groups.addAll(groups);
	}

	@Override
	public String getId() {
		return subject;
	}

	@Override
	public String getDomain() {
		return issuer.getDomain();
	}

	@Override
	public boolean isRemembered() {
		return false;
	}

	@Override
	public Authenticator getAuthenticator() {
		return parent;
	}

	@Override
	public Principal getPrincipal() {
		return this;
	}

	public void checkExpired() {
		issuer.checkExpired(jwt);
	}

	@Override
	public String getProperty(String propertyName) {
		return properties.get(propertyName);
	}

	@Override
	public Collection<String> getPropertyNames() {
		return properties.keySet();
	}

	@Override
	public String setProperty(String propertyName, String value) {
		return properties.put(propertyName, value);
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + "(" + getFullId() + ")";
	}

	Set<Permission> getPermissions() {
		return permissions;
	}

	CheckResult isMemberOf(String groupName, String groupDomain) {
		var group = new QName(groupName, groupDomain == null ? getDomain() : groupDomain);
		if (groups.contains(group))
			return CheckResult.YES;
		return CheckResult.UNKNOWN;
	}

	CheckResult isPermitted(Permission p) {
		if (!(p instanceof StringPermission))
			return CheckResult.UNKNOWN;

		try {
			checkExpired();
		} catch (final TokenExpiredException e) {
			JWTRealm.log.warn("Use of an expired Token: {}", this, e);
			return CheckResult.NO;
		}

		for (final Permission sp : getPermissions()) {
			if (p instanceof StringPermission && ((StringPermission) sp).implies(p))
				return CheckResult.YES;
		}

		return CheckResult.UNKNOWN;
	}

}
