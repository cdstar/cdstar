package de.gwdg.cdstar.ext.auth.jwt;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse.BodyHandlers;
import java.security.AlgorithmParameters;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.interfaces.ECPrivateKey;
import java.security.interfaces.ECPublicKey;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.ECGenParameterSpec;
import java.security.spec.ECParameterSpec;
import java.security.spec.ECPoint;
import java.security.spec.ECPublicKeySpec;
import java.security.spec.RSAPublicKeySpec;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.function.Consumer;

import org.checkerframework.checker.nullness.qual.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.auth0.jwt.interfaces.ECDSAKeyProvider;
import com.auth0.jwt.interfaces.RSAKeyProvider;
import com.fasterxml.jackson.databind.JsonNode;

import de.gwdg.cdstar.SharedObjectMapper;
import de.gwdg.cdstar.Utils;

/**
 * Helper to verify JTWs against multiple keys, which are either manually added
 * or loaded from a JWKS url.
 */
class KeyStore {
	static final Logger log = LoggerFactory.getLogger(KeyStore.class);

	volatile Map<String, Map<String, Algorithm>> idTypeKeyMap = new HashMap<>();
	List<Algorithm> staticKeys = new ArrayList<>();
	List<Algorithm> jwksKeys = new ArrayList<>();

	private URI jwksUrl;

	public KeyStore(@Nullable URI jwksUrl) {
		this.jwksUrl = jwksUrl;
	}

	public void verify(DecodedJWT jwt) {
		this.getAlgorithmFor(jwt)
				.orElseThrow(() -> new JWTVerificationException(
						"No signature key found for provided 'alg' and 'kid' claims"))
				.verify(jwt);
	}

	public Optional<Algorithm> getAlgorithmFor(DecodedJWT jwt) {
		var kid = jwt.getKeyId() != null ? jwt.getKeyId() : "";
		var alg = jwt.getAlgorithm() != null ? jwt.getAlgorithm() : "none";

		return Optional.ofNullable(idTypeKeyMap.get(kid))
				.flatMap(algos -> Optional.ofNullable(algos.get(alg)));
	}

	private synchronized void buildKeyMap() {
		Map<String, Map<String, Algorithm>> newCache = new HashMap<>();

		Consumer<Algorithm> putKey = key -> {
			var kid = key.getSigningKeyId() != null ? key.getSigningKeyId() : "";
			var alg = key.getName() != null ? key.getName() : "none";
			newCache
					.computeIfAbsent(kid, k -> new HashMap<String, Algorithm>())
					.put(alg, key);
		};

		staticKeys.forEach(putKey);
		jwksKeys.forEach(putKey);
		idTypeKeyMap = newCache;
	}

	/**
	 * Download and parse JWKS keys file if configured.
	 */
	public synchronized void refreshWebKeys() {
		if (jwksUrl == null) {
			jwksKeys = new ArrayList<>(0);
			return;
		}

		CompletableFuture<InputStream> jsonData;
		if ("file".equals(jwksUrl.getScheme())) {
			try {
				jsonData = CompletableFuture.completedFuture(jwksUrl.toURL().openStream());
			} catch (IOException e) {
				jsonData = CompletableFuture.failedFuture(e);
			}
		} else {
			var client = HttpClient.newHttpClient();
			var request = HttpRequest.newBuilder(jwksUrl)
					.header("accept", "application/json")
					.timeout(Duration.ofSeconds(10))
					.build();

			jsonData = client
					.sendAsync(request, BodyHandlers.ofInputStream())
					.thenApply(response -> {
						response
								.headers()
								.firstValue("content-type")
								.filter(h -> h.contains("json"))
								.orElseThrow(() -> new RuntimeException("JWKS response content type is not json"));
						return response.body();
					});
		}

		jsonData
				.thenApply(is -> {
					try {
						return SharedObjectMapper.json.readTree(is);
					} catch (IOException e) {
						throw new RuntimeException("JWKS response failed to parse as json", e);
					} finally {
						Utils.closeQuietly(is);
					}
				}).whenComplete((json, error) -> {
					if (error != null) {
						JWTRealm.log.error("Failed to load JWT keys from JWKS url: {}", jwksUrl, error);
						return;
					}
					jwksKeys = readJWKS(json);
					buildKeyMap();
				});

	}

	private String requireText(JsonNode node, String attr) {
		var val = node.get(attr);
		if (val == null || !val.isTextual())
			throw new IllegalArgumentException("Missing or invalid key parameter: " + attr);
		return val.textValue();
	}

	private List<Algorithm> readJWKS(JsonNode json) {
		var keyDefs = json.withArray("keys");
		var keys = new ArrayList<Algorithm>(keyDefs.size());
		var base64UrlDecoder = Base64.getUrlDecoder();

		for (var keyDef : keyDefs) {
			if (!"sig".equals(keyDef.path("use").textValue()))
				continue;

			try {
				var kty = requireText(keyDef, "kty");
				var kid = keyDef.path("kid").textValue();
				var alg = keyDef.path("alg").textValue();

				switch (kty) {
				case "RSA":
					var modulus = new BigInteger(1, base64UrlDecoder.decode(requireText(keyDef, "n")));
					var exponent = new BigInteger(1, base64UrlDecoder.decode(requireText(keyDef, "e")));
					var rsaPubKey = (RSAPublicKey) KeyFactory.getInstance("RSA")
							.generatePublic(new RSAPublicKeySpec(modulus, exponent));
					var rsaKey = new RSAKeyProviderWithId(kid, rsaPubKey, null);
					if (alg == null || alg.equals("RS256"))
						keys.add(Algorithm.RSA256(rsaKey));
					if (alg == null || alg.equals("RS384"))
						keys.add(Algorithm.RSA384(rsaKey));
					if (alg == null || alg.equals("RS512"))
						keys.add(Algorithm.RSA512(rsaKey));
					break;

				case "EC":
					var ecParam = AlgorithmParameters.getInstance("EC");
					var crv = requireText(keyDef, "crv");
					switch (crv) {
					case "P-256":
						ecParam.init(new ECGenParameterSpec("secp256r1"));
						break;
					case "P-384":
						ecParam.init(new ECGenParameterSpec("secp384r1"));
						break;
					case "P-521":
						ecParam.init(new ECGenParameterSpec("secp521r1"));
						break;
					default:
						throw new RuntimeException("Invalid or unsupported curve type " + crv);
					}

					var ecPoint = new ECPoint(
							new BigInteger(base64UrlDecoder.decode(requireText(keyDef, "x"))),
							new BigInteger(base64UrlDecoder.decode(requireText(keyDef, "y"))));
					var ecPubKey = (ECPublicKey) KeyFactory.getInstance("EC")
							.generatePublic(
									new ECPublicKeySpec(ecPoint, ecParam.getParameterSpec(ECParameterSpec.class)));
					var ecKey = new ECDSAKeyProviderWithId(kid, ecPubKey, null);
					if (alg == null || alg.equals("ECDSA256"))
						keys.add(Algorithm.ECDSA256(ecKey));
					if (alg == null || alg.equals("ECDSA384"))
						keys.add(Algorithm.ECDSA384(ecKey));
					if (alg == null || alg.equals("ECDSA512"))
						keys.add(Algorithm.ECDSA512(ecKey));
					break;

				default:
					throw new NoSuchAlgorithmException("Unsupported JWK key type");
				}
			} catch (Exception e) {
				log.warn("Skipping invalid or unsuppoerted JWKS key: {}", keyDef.toString(), e);
			}
		}

		return keys;
	}

	public synchronized void addHMAC(String kid, byte[] secret) {
		staticKeys.add(Algorithm.HMAC256(secret));
		staticKeys.add(Algorithm.HMAC384(secret));
		staticKeys.add(Algorithm.HMAC512(secret));
		buildKeyMap();
	}

	public synchronized void addRSA(String kid, RSAPublicKey cert) {
		staticKeys.add(Algorithm.RSA256(cert, null));
		staticKeys.add(Algorithm.RSA384(cert, null));
		staticKeys.add(Algorithm.RSA512(cert, null));
		buildKeyMap();
	}

	public synchronized void addECDSA(String kid, ECPublicKey cert) {
		staticKeys.add(Algorithm.ECDSA256(cert, null));
		staticKeys.add(Algorithm.ECDSA384(cert, null));
		staticKeys.add(Algorithm.ECDSA512(cert, null));
		buildKeyMap();
	}

	private static class KeyProviderWithId<PUB extends PublicKey, PRIV extends PrivateKey> {
		final String id;
		private PUB pubKey;
		private PRIV privKey;

		public KeyProviderWithId(String id, PUB pubKey, PRIV privKey) {
			this.id = id;
			this.pubKey = pubKey;
			this.privKey = privKey;
		}

		public PUB getPublicKeyById(String keyId) {
			if (Objects.equals(id, keyId))
				return pubKey;
			return null;
		}

		public PRIV getPrivateKey() {
			return privKey;
		}

		public String getPrivateKeyId() {
			return id;
		}
	}

	private static final class RSAKeyProviderWithId extends KeyProviderWithId<RSAPublicKey, RSAPrivateKey>
			implements RSAKeyProvider {

		public RSAKeyProviderWithId(String id, RSAPublicKey pubKey, RSAPrivateKey privKey) {
			super(id, pubKey, privKey);
		}
	}

	private static final class ECDSAKeyProviderWithId extends KeyProviderWithId<ECPublicKey, ECPrivateKey>
			implements ECDSAKeyProvider {

		public ECDSAKeyProviderWithId(String id, ECPublicKey pubKey, ECPrivateKey privKey) {
			super(id, pubKey, privKey);
		}
	}

}
