package de.gwdg.cdstar.ext.realm.ldap;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.auth.Principal;
import de.gwdg.cdstar.auth.Session;
import de.gwdg.cdstar.auth.realm.Authenticator;

class LDAPPrincipal implements Principal, Session {

	public static final String PROPERTY_LOGIN_NAME = "ldap.name";
	public static final String PROPERTY_LDAP_DN = "ldap.dn";
	private final Authenticator parent;
	private final String uid;
	private final String domain;
	private byte[] signKey;
	private byte[] passwordHash;

	private final Map<String, String> properties = new HashMap<>();

	public LDAPPrincipal(Authenticator parent, String nameInNamespace, String uid, String domain, String loginName) {
		this.parent = parent;
		this.uid = uid;
		this.domain = domain;
		setProperty(PROPERTY_LDAP_DN, nameInNamespace);
		setProperty(PROPERTY_LOGIN_NAME, loginName);
	}

	@Override
	public String getId() {
		return uid;
	}

	@Override
	public String getDomain() {
		return domain;
	}

	@Override
	public boolean isRemembered() {
		return false;
	}

	@Override
	public Authenticator getAuthenticator() {
		return parent;
	}

	@Override
	public Principal getPrincipal() {
		return this;
	}

	String getLdapDN() {
		return getProperty(PROPERTY_LDAP_DN);
	}

	String getLoginName() {
		return getProperty(PROPERTY_LOGIN_NAME);
	}

	public boolean checkCachedPassword(char[] password) {
		return Utils.signCheck(passwordHash, Utils.toBytes(password), signKey);
	}

	public void cachePassword(char[] password) {
		signKey = Utils.randomBytes(256);
		passwordHash = Utils.sign(Utils.toBytes(password), signKey);
	}

	public boolean hasCachedPassword() {
		return passwordHash != null;
	}

	@Override
	public String getProperty(String propertyName) {
		return properties.get(propertyName);
	}

	@Override
	public Collection<String> getPropertyNames() {
		return properties.keySet();
	}

	@Override
	public String setProperty(String propertyName, String value) {
		return properties.put(propertyName, value);
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + "(" + getFullId() + ")";
	}

	@Override
	public boolean equals(Object obj) {
		return obj instanceof LDAPPrincipal && ((LDAPPrincipal) obj).getLdapDN().equals(getLdapDN());
	}

	@Override
	public int hashCode() {
		return getLdapDN().hashCode();
	}

}
