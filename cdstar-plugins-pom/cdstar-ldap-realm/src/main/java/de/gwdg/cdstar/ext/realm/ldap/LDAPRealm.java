package de.gwdg.cdstar.ext.realm.ldap;

import java.time.Duration;
import java.util.Hashtable;
import java.util.Optional;

import javax.naming.AuthenticationException;
import javax.naming.CommunicationException;
import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.InitialLdapContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;
import com.github.benmanes.caffeine.cache.Scheduler;

import de.gwdg.cdstar.FailableSupplier;
import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.auth.Credentials;
import de.gwdg.cdstar.auth.Session;
import de.gwdg.cdstar.auth.UsernamePasswordCredentials;
import de.gwdg.cdstar.auth.realm.Authenticator;
import de.gwdg.cdstar.runtime.Config;
import de.gwdg.cdstar.runtime.ConfigException;

public class LDAPRealm implements Authenticator {
	private static final Logger log = LoggerFactory.getLogger(LDAPRealm.class);

	private final String server;
	private final Duration conntectTimeout;
	private final Duration readTimeout;

	private final String searchBase;
	private final String searchFilter;
	private final SearchControls searchControls;
	private final Hashtable<String, String> searchEnv;

	private final String uidAttribute;
	private final String domainAttribute;
	private final String realmName;

	private final LoadingCache<String, Optional<LDAPPrincipal>> principalCache;
	private final Duration cacheExpire;

	public LDAPRealm(Config config) throws ConfigException, NamingException {
		config.setDefault("cache.size", "1024");
		config.setDefault("cache.expire", "600");
		config.setDefault("timeout.connect", "1000");
		config.setDefault("timeout.read", "1000");

		config.setDefault("name", config.get("_name", "ldap"));

		server = config.get("server");

		realmName = config.get("name");
		searchBase = config.get("search.base"); // "ou=Customers,dc=gwdg,dc=de"
		searchFilter = config.get("search.filter", "(|(uid={})(mail={}))");
		uidAttribute = config.get("attr.uid", "uid");
		domainAttribute = config.get("attr.domain", null);

		cacheExpire = Duration.ofSeconds(config.getInt("cache.expire"));
		principalCache = Caffeine.newBuilder()
				.maximumSize(config.getInt("cache.size"))
				.expireAfterWrite(cacheExpire)
				.scheduler(Scheduler.systemScheduler())
				.build(name -> Optional.ofNullable(this.findLdapUser(name)));

		conntectTimeout = Duration.ofMillis(config.getLong("timeout.connect"));
		readTimeout = Duration.ofMillis(config.getLong("timeout.read"));

		searchEnv = makeLdapConfig(true,
				config.get("search.user", null),
				config.get("search.password", null));

		searchControls = new SearchControls();
		searchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);
		searchControls.setReturningAttributes(new String[] { uidAttribute, domainAttribute });
		searchControls.setCountLimit(1);

	}

	private Hashtable<String, String> makeLdapConfig(boolean pool, String dn, String password) {
		// Enable pooling for search context only!

		final Hashtable<String, String> env = new Hashtable<>();
		env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
		env.put(Context.PROVIDER_URL, server);
		env.put("com.sun.jndi.ldap.connect.timeout", String.valueOf(conntectTimeout.toMillis()));
		env.put("com.sun.jndi.ldap.read.timeout", String.valueOf(readTimeout.toMillis()));

		if (pool)
			env.put("com.sun.jndi.ldap.connect.pool", "true");

		if (dn != null) {
			env.put(Context.SECURITY_PRINCIPAL, dn);
			env.put(Context.SECURITY_CREDENTIALS, password);
		} else {
			env.put(Context.SECURITY_AUTHENTICATION, "none");
		}

		return env;
	}

	@Override
	public String getName() {
		return realmName;
	}

	static final String escapeLDAPFilter(String filter) {
		final StringBuilder sb = new StringBuilder();
		for (int i = 0; i < filter.length(); i++) {
			final char curChar = filter.charAt(i);
			switch (curChar) {
			case '\\':
				sb.append("\\5c");
				break;
			case '*':
				sb.append("\\2a");
				break;
			case '(':
				sb.append("\\28");
				break;
			case ')':
				sb.append("\\29");
				break;
			case '\u0000':
				sb.append("\\00");
				break;
			default:
				sb.append(curChar);
			}
		}
		return sb.toString();
	}

	/**
	 * Called by the cache
	 */
	LDAPPrincipal findLdapUser(String name) {
		try {
			return retry(() -> {
				final InitialLdapContext ctx = new InitialLdapContext(searchEnv, null);
				try {
					final String filter = searchFilter.replace("{}", escapeLDAPFilter(name));
					final NamingEnumeration<SearchResult> results = ctx.search(searchBase, filter, searchControls);
					if (results.hasMore()) {
						final SearchResult hit = results.next();
						final LDAPPrincipal userRecord = buildPrincipalFromLdap(name, hit);
						log.debug("Found LDAP user: {} -> {}", name, userRecord.getLdapDN());
						return userRecord;
					}
				} finally {
					ctx.close();
				}
				return null;
			}, 3, Duration.ofSeconds(1));
		} catch (final NamingException e) {
			log.warn("LDAP search failed", e);
		}
		return null;
	}

	Optional<LDAPPrincipal> searchUser(String name) {
		return principalCache.get(name);
	}

	/**
	 * Retry an LDAP operation multiple times before failing. Only retries on
	 * {@link CommunicationException}. Other exceptions immediately throw.
	 */
	private <T> T retry(FailableSupplier<T, NamingException> ldapJob, int maxAttempts, Duration waitTime)
			throws NamingException {
		if (maxAttempts <= 0)
			maxAttempts = 1;
		CommunicationException lastError = null;
		for (int attempt = 1; attempt <= maxAttempts; attempt++) {
			try {
				return ldapJob.supply();
			} catch (CommunicationException e) {
				log.warn("LDAP communication failed (retry {}/{})", attempt, maxAttempts, e);
				Utils.sleepInterruptable(waitTime.toMillis());
				if (lastError != null)
					e.addSuppressed(lastError);
				lastError = e;
			}
		}
		throw lastError;
	}

	private LDAPPrincipal buildPrincipalFromLdap(String loginName, SearchResult sr) throws NamingException {
		final Attributes attrs = sr.getAttributes();
		final String uid = attrs.get(uidAttribute).get().toString();

		final String domain;
		if (domainAttribute != null && attrs.get(domainAttribute) != null)
			domain = attrs.get(domainAttribute).get().toString();
		else
			domain = realmName;

		return new LDAPPrincipal(this, sr.getNameInNamespace(), uid, domain, loginName);
	}

	boolean tryLogin(LDAPPrincipal principal, char[] cs) {
		if (cs == null || cs.length == 0 || Utils.arrayContains(cs, '\0'))
			return false;
		synchronized (principal) {

			if (principal.hasCachedPassword()) {
				if (principal.checkCachedPassword(cs)) {
					log.debug("Login success (cached): {}", principal.getLdapDN());
					return true;
				} else {
					log.warn("Login failed (cached): {}", principal.getLdapDN());
					return false;
				}
			}

			try {

				// Do not enable connection pooling for login attempts, as the pool
				// would be flooded with idling connections (one per login DN).
				final Hashtable<String, String> env = makeLdapConfig(false, principal.getLdapDN(), new String(cs));
				retry(() -> {
					new InitialLdapContext(env, null).close();
					return null;
				}, 3, Duration.ofSeconds(1));
				principal.cachePassword(cs);

				log.debug("Login success: {}", principal.getLdapDN());
				return true;
			} catch (final AuthenticationException e) {
				log.info("Login failed: {}", principal.getLdapDN());
				return false;
			} catch (final NamingException e) {
				log.warn("Login failed (ldap error): {}", principal.getLdapDN(), e);
				return false;
			}
		}
	}

	@Override
	public Session login(Credentials request) {
		if (!(request instanceof UsernamePasswordCredentials))
			return null;
		final UsernamePasswordCredentials creds = (UsernamePasswordCredentials) request;

		final Optional<LDAPPrincipal> usr = searchUser(creds.getName());
		if (!usr.isPresent())
			return null;

		final LDAPPrincipal found = usr.get();

		// Ensure that domain matches, if specified.
		if (creds.getDomain() != null && !Utils.equalNotNull(found.getDomain(), creds.getDomain())) {
			log.info("Login failed (wrong domain): {}", found.getLdapDN());
			return null;
		}

		if (!tryLogin(found, creds.getPassword())) {
			return null;
		}

		return found;
	}

	@Override
	public void logout(Session who) {
		if (who.getAuthenticator() != this)
			return;
		if (!(who.getPrincipal() instanceof LDAPPrincipal))
			return;
		principalCache.invalidate(((LDAPPrincipal) who.getPrincipal()).getLoginName());
	}

}
