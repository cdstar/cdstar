package de.gwdg.cdstar.ext.realm.ldap;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class TestLDAPPrincipal {

	@Test
	public void testPrincipal() throws Exception {
		final LDAPPrincipal p = new LDAPPrincipal(null, "cn=test", "test-uid", "test-realm", "test@email.com");
		assertEquals("test-uid", p.getId());
		assertEquals("test-realm", p.getDomain());
		assertEquals("test-uid@test-realm", p.getFullId());
		assertEquals(p, p.getPrincipal());
		assertFalse(p.isRemembered());
	}

	@Test
	public void testCachedPassword() throws Exception {
		final LDAPPrincipal p = new LDAPPrincipal(null, "cn=test", "test-uid", "test-realm", "test@email.com");
		assertFalse(p.hasCachedPassword());
		p.cachePassword("1234".toCharArray());
		assertTrue(p.hasCachedPassword());
		assertTrue(p.checkCachedPassword("1234".toCharArray()));
		assertFalse(p.checkCachedPassword("123x".toCharArray()));
	}

}
