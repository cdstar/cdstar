package de.gwdg.cdstar.ext.realm.ldap;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import javax.naming.NamingException;

import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.zapodot.junit.ldap.EmbeddedLdapRule;
import org.zapodot.junit.ldap.EmbeddedLdapRuleBuilder;

import de.gwdg.cdstar.auth.Principal;
import de.gwdg.cdstar.auth.UsernamePasswordCredentials;
import de.gwdg.cdstar.config.MapConfig;
import de.gwdg.cdstar.runtime.ConfigException;
import de.gwdg.cdstar.utils.test.TestLogger;

public class TestLDAPSearch {

	@Rule
	public TestLogger log = new TestLogger("de.gwdg");

	@ClassRule
	public static EmbeddedLdapRule embeddedLdapRule = EmbeddedLdapRuleBuilder.newInstance()
		.usingDomainDsn("dc=example,dc=com")
		.importingLdifs("ldap-users.ldif")
		.build();

	private LDAPRealm realm;

	@Before
	public void setupRealm() throws ConfigException, NamingException {
		final MapConfig cfg = new MapConfig();
		cfg.set("_name", "ldap")
			.set("server", "ldap://localhost:" + embeddedLdapRule.embeddedServerPort())
			.set("search.user", "cn=reader,ou=Users,dc=example,dc=com")
			.set("search.password", "reader1234")
			.set("search.base", "ou=Users,dc=example,dc=com")
			.set("search.filter", "(|(uid={})(mail={}))")
			.set("attr.uid", "uid")
			.set("attr.domain", "ou");
		realm = new LDAPRealm(cfg);
	}

	private Principal login(String user, String realmName, String password) {
		return (Principal) realm.login(new UsernamePasswordCredentials(user, realmName, password.toCharArray()));
	}

	@Test
	public void testValidLogin() throws Exception {
		final Principal p = login("adoe0", "Users", "alice1234");
		assertNotNull(p);
		assertEquals("adoe0", p.getId());
		assertEquals("Users", p.getDomain());
		assertEquals("cn=Alice,ou=Users,dc=example,dc=com", p.getProperty(LDAPPrincipal.PROPERTY_LDAP_DN));
	}

	@Test
	public void testLoginByEMail() throws Exception {
		assertEquals(
			login("adoe0", "Users", "alice1234"),
			login("alice.doe@example.com", "Users", "alice1234"));
	}

	@Test
	public void testUnknownUser() throws Exception {
		assertNull(login("bob", "Users", "alice1234"));
	}

	@Test
	public void testUnknownEmail() throws Exception {
		assertNull(login("bob@example.com", "Users", "alice1234"));
	}

	@Test
	public void testWrongPassword() throws Exception {
		assertNull(login("adoe0", "Users", "aliceXXXX"));
	}

	@Test
	public void testWrongRealm() throws Exception {
		assertNull(login("adoe0", "not-ldap", "alice1234"));
	}

	@Test
	public void testNoRealm() throws Exception {
		assertNotNull(login("adoe0", null, "alice1234"));
	}

}
