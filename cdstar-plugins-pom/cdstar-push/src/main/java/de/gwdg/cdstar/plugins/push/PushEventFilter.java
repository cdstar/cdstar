package de.gwdg.cdstar.plugins.push;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpClient.Redirect;
import java.net.http.HttpClient.Version;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.Duration;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

import de.gwdg.cdstar.SharedObjectMapper;
import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.event.ChangeEvent;
import de.gwdg.cdstar.runtime.Config;
import de.gwdg.cdstar.runtime.ConfigException;
import de.gwdg.cdstar.runtime.Plugin;
import de.gwdg.cdstar.runtime.RuntimeContext;
import de.gwdg.cdstar.runtime.filter.AbstractEventFilter;
import de.gwdg.cdstar.runtime.listener.RuntimeListener;

@Plugin(name = "http-push")
public class PushEventFilter extends AbstractEventFilter implements RuntimeListener {
	final ScheduledExecutorService pool = Executors.newSingleThreadScheduledExecutor();
	static final Logger log = LoggerFactory.getLogger(PushEventFilter.class);
	final static ObjectMapper om = SharedObjectMapper.json_compact;

	final List<PushConsumer> consumers = new ArrayList<>();

	private boolean stopped;
	private Path logFile;
	private FileChannel logChannel;

	int maxRetry = 3;
	Duration defaultDelay = Duration.ofSeconds(1);
	Duration defaultCooldown = Duration.ofSeconds(60);
	Duration requestTimeout = Duration.ofSeconds(60);
	int maxQueue = 1000;
	Map<String, String> additionalHeaders = new HashMap<>();
	private HttpClient httpClient;

	public PushEventFilter(Config config) throws ConfigException {

		for (final String key : config.keySet()) {
			try {
				if (key.equals("url") || key.startsWith("url.")) {
					consumers.add(new PushConsumer(this, URI.create(config.get(key))));
				} else if (key.startsWith("header.")) {
					additionalHeaders.put(key.substring("header.".length()), config.get(key));
				} else if (key.equals("retry.max")) {
					maxRetry = config.getInt(key);
				} else if (key.equals("retry.delay")) {
					defaultDelay = Duration.ofMillis(config.getInt(key));
				} else if (key.equals("retry.cooldown")) {
					defaultCooldown = Duration.ofMillis(config.getInt(key));
				} else if (key.equals("http.timeout")) {
					requestTimeout = Duration.ofMillis(config.getInt(key));
				} else if (key.equals("queue.size")) {
					maxQueue = config.getInt(key);
				} else if (key.equals("fail.log")) {
					final String fname = config.get(key).replace("%s", Long.toString(System.currentTimeMillis()));
					logFile = Paths.get(fname);
					Files.createDirectories(logFile.getParent());
					logChannel = FileChannel.open(logFile, StandardOpenOption.WRITE, StandardOpenOption.APPEND,
						StandardOpenOption.CREATE);
					if (logChannel.position() > 0)
						log.warn("Fail.log is not empty: {}", logFile);
				}
			} catch (final IOException e) {
				throw new ConfigException(key + ": " + e.getMessage(), e);
			}
		}

		httpClient = HttpClient.newBuilder()
			.connectTimeout(requestTimeout)
			.followRedirects(Redirect.NORMAL)
			.version(Version.HTTP_1_1)
			.build();

		if (logChannel == null)
			log.warn("Push filter configured without 'fail.log'. Failing push requests will be lost.");

	}

	/**
	 * This main method helps generating curl bash script that repeats all failed
	 * requests found in a fail.log file.
	 */
	public static void main(String[] args) throws IOException {
		try (final BufferedReader br = new BufferedReader(
			new InputStreamReader(new FileInputStream(new File(args[1])), StandardCharsets.UTF_8))) {

			String line;
			while ((line = br.readLine()) != null) {
				final String[] parts = line.split(" ", 2);
				final String target = parts[0];
				final String payload = new String(Utils.base64decode(parts[1]), StandardCharsets.UTF_8);
				System.out.println("curl -XPOST -HContent-Type:application/json '" + target + "' -d '"
					+ payload.replace("'", "\\'") + "'");
			}
		}
	}

	@Override
	public synchronized void triggerEvent(ChangeEvent event) throws Exception {
		if (stopped)
			throw Utils.wtf("Recieved event during or after runtime shutdown.");
		log.debug("New event: {}", event);
		consumers.forEach(c -> c.add(event));
	}

	@Override
	public void onShutdown(RuntimeContext ctx) {
		stopped = true;
		consumers.forEach(PushConsumer::stop);
	}

	public boolean isStopped() {
		return stopped;
	}

	public synchronized void logFailed(PushConsumer pushConsumer, ChangeEvent event) {
		log.error("Pust failed: vault={} archive={} target={}", event.getVault(), event.getArchive(),
			pushConsumer.target);
		if (logChannel != null) {
			try (final ByteArrayOutputStream out = new ByteArrayOutputStream(512)) {
				out.write(pushConsumer.target.toString().getBytes(StandardCharsets.UTF_8));
				out.write(' ');
				om.writeValue(out, event);
				logChannel.write(ByteBuffer.wrap(out.toByteArray()));
				logChannel.force(false);
				return;
			} catch (final IOException e) {
				log.error("Could not write to {}!", logFile);
			}
		}
	}

	public HttpClient client() {
		return httpClient;
	}

}
