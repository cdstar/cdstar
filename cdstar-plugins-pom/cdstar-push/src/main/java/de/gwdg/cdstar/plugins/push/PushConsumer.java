package de.gwdg.cdstar.plugins.push;

import java.net.URI;
import java.net.http.HttpRequest;
import java.net.http.HttpRequest.BodyPublishers;
import java.net.http.HttpResponse.BodyHandlers;
import java.time.Duration;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.SplittableRandom;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import com.fasterxml.jackson.core.JsonProcessingException;

import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.event.ChangeEvent;

public class PushConsumer implements Runnable {
	final PushEventFilter parent;
	final URI target;
	private final Deque<ChangeEvent> queue;
	private final ScheduledExecutorService pool;
	private final SplittableRandom random = new SplittableRandom();

	private int errors = 0;
	private int dropped = 0;
	private ChangeEvent currentEvent;

	public PushConsumer(PushEventFilter parent, URI target) {
		this.parent = parent;
		this.target = target;
		pool = parent.pool;
		queue = new ArrayDeque<>();
	}

	public synchronized void add(ChangeEvent event) {
		if (parent.isStopped()) {
			parent.logFailed(this, event);
			return;
		}

		final boolean needsReschedule = queue.isEmpty();
		if (queue.size() > parent.maxQueue) {
			dropped++;
			parent.logFailed(this, event);
		}

		queue.add(event);

		if (needsReschedule) {
			pool.execute(this);
		}
	}

	@Override
	public synchronized void run() {
		if (parent.isStopped())
			return;

		currentEvent = queue.peekFirst();
		if (currentEvent == null)
			return;

		byte[] payload;
		try {
			payload = PushEventFilter.om.writeValueAsBytes(currentEvent);
		} catch (final JsonProcessingException e) {
			throw Utils.wtf(e);
		}

		var requestBuilder = HttpRequest.newBuilder().uri(target);

		parent.additionalHeaders.forEach((h, v) -> requestBuilder.setHeader(h, v));
		requestBuilder.setHeader("Content-Type", "application/json; charset=UTF-8");

		if (errors > 0)
			requestBuilder.setHeader("Push-Retry", Integer.toString(errors));
		requestBuilder.setHeader("Push-Queue", Utils.join(" ", Integer.toString(queue.size() - 1),
			Integer.toString(parent.maxQueue), Integer.toString(dropped)));

		requestBuilder.POST(BodyPublishers.ofByteArray(payload));

		parent.client().sendAsync(requestBuilder.build(), BodyHandlers.discarding()).whenComplete((response, err) -> {
			if (err != null) {
				onError(err);
				return;
			}

			var code = response.statusCode();
			if (code == 200 || code == 202 || code == 204) {
				onSuccess(currentEvent);
				return;
			}

			/**
			 * Consumers can signal with 503 and a valid Retry-After header
			 * that they are busy, but still running and responsive. In this
			 * case, we will retry after the proposed delay
			 */
			if (code == 503) {
				var cooldown = response.headers()
					.firstValue("Retry-After")
					.map(seconds -> Duration.ofSeconds(Long.parseLong(seconds)))
					.orElse(parent.defaultCooldown);
				onCooldownRequested(cooldown);
				return;
			}

			onError(new RuntimeException("Wrong status code: " + code));
			return;
		});

	}

	/**
	 * Removes successful event from queue, resets error counter and
	 * re-schedules without a delay if there are more events in the queue.
	 */
	synchronized void onSuccess(ChangeEvent event) {

		if (parent.isStopped())
			return;

		if (event != queue.poll())
			Utils.wtf();

		PushEventFilter.log.debug("Event [{}] delivered to {}", event.getTx(), target);

		if (errors > parent.maxRetry)
			PushEventFilter.log.info("Consumer {} responsive again.", target);

		errors = 0;
		if (!queue.isEmpty())
			pool.execute(this);
	}

	/**
	 * Re-schedules with an delay, but also resets the error counter since
	 * cooldown requests are not errors.
	 */
	synchronized void onCooldownRequested(Duration cooldown) {
		if (parent.isStopped())
			return;

		errors = 0;
		pool.schedule(this, cooldown.toMillis(), TimeUnit.MILLISECONDS);
	}

	/**
	 * Increases error counter and reschedules with a delay (either normal delay
	 * or cooldown time depending on the error count)
	 */
	synchronized void onError(Throwable t) {
		if (parent.isStopped())
			return;

		errors++;
		var delay = parent.defaultDelay;
		if (errors > parent.maxRetry) {
			if ((errors - 1) % parent.maxRetry == 0)
				PushEventFilter.log.debug("Consumer {} unresponsive. Last error: {}", target, t.toString());
			delay = parent.defaultCooldown;
		}
		var millis = delay.toMillis();
		var jitter = random.nextLong(millis / 10);
		pool.schedule(this, millis + jitter, TimeUnit.MILLISECONDS);
	}

	synchronized void stop() {
		/*
		 * Stopping the service immediately drops all waiting or in-flight
		 * events. If there is an in-flight request, its result will be ignored.
		 */
		assert parent.isStopped();
		while (!queue.isEmpty())
			parent.logFailed(this, queue.pop());
	}

}
