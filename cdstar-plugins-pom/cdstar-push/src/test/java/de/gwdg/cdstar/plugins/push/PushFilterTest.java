package de.gwdg.cdstar.plugins.push;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Predicate;

import org.junit.After;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.slf4j.Logger;

import de.gwdg.cdstar.SharedObjectMapper;
import de.gwdg.cdstar.auth.Subject;
import de.gwdg.cdstar.auth.UsernamePasswordCredentials;
import de.gwdg.cdstar.auth.error.LoginFailed;
import de.gwdg.cdstar.auth.simple.SimpleAuthorizer;
import de.gwdg.cdstar.config.MapConfig;
import de.gwdg.cdstar.event.ChangeEvent;
import de.gwdg.cdstar.rest.servlet.CDStarServletInitializer;
import de.gwdg.cdstar.runtime.CDStarRuntime;
import de.gwdg.cdstar.runtime.client.CDStarArchive;
import de.gwdg.cdstar.runtime.client.CDStarSession;
import de.gwdg.cdstar.runtime.client.CDStarVault;
import de.gwdg.cdstar.runtime.client.exc.VaultNotFound;
import de.gwdg.cdstar.server.jetty.JettyServer;
import de.gwdg.cdstar.ta.exc.TARollbackException;
import de.gwdg.cdstar.utils.test.TestLogger;
import jakarta.servlet.ServletContainerInitializer;
import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRegistration.Dynamic;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public class PushFilterTest {
	private static final String TEST_VAULT = "test";
	private static final String TEST_USER = "test";
	private static final String TEST_USER2 = "test2";

	@Rule
	public TemporaryFolder tmp = new TemporaryFolder();

	@Rule
	public TestLogger testLog = new TestLogger(Logger.ROOT_LOGGER_NAME);

	public static int PORT = 8098;
	private CDStarRuntime runtime;
	private JettyServer jetty;
	List<ChangeEvent> capturedEvents = new ArrayList<>();
	private CDStarArchive a1;
	private CDStarArchive a2;

	@FunctionalInterface
	public interface PushEventCallback {
		void handle(HttpServletRequest req, HttpServletResponse resp, ChangeEvent event);
	}

	PushEventCallback onEvent = (rr, rq, e) -> {
	};

	public class PushServlet extends HttpServlet {
		private static final long serialVersionUID = -4548477596698629725L;

		@Override
		protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
			final ChangeEvent event = SharedObjectMapper.json.readValue(req.getInputStream(), ChangeEvent.class);

			synchronized (capturedEvents) {
				onEvent.handle(req, resp, event);
				capturedEvents.add(event);
				capturedEvents.notifyAll();
			}
		}
	}

	@After
	public void teardown() throws Exception {
		runtime.close();
		jetty.stop();
	}

	private void assertEventCount(int n) {
		synchronized (capturedEvents) {
			final long t1 = System.currentTimeMillis();
			while (capturedEvents.size() < n) {
				try {
					capturedEvents.wait(100);
				} catch (final InterruptedException e) {
					assertEquals("Interrupted while waiting for events.", n, capturedEvents.size());
				}
				if (System.currentTimeMillis() - t1 > 10000)
					break;
			}
			assertEquals("Number of events.", n, capturedEvents.size());
		}
	}

	private <T> void assertExactlyOne(Iterable<T> iter, Predicate<T> filter) {
		final List<T> found = new ArrayList<>(1);
		iter.forEach(e -> {
			if (filter.test(e))
				found.add(e);
		});
		assertEquals(1, found.size());
	}

	void startWithFilter(PushEventFilter filter) throws Exception {
		runtime = CDStarRuntime.bootstrap(
			new MapConfig().set("path.home", tmp.newFolder().toString()).set("vault.test.create", "true"));
		runtime.register(filter);
		runtime.start();

		final SimpleAuthorizer staticRealm = runtime.lookupRequired(SimpleAuthorizer.class);
		staticRealm.account(TEST_USER).password(TEST_USER).withPermissions("vault:test:create", "vault:test:read");
		staticRealm.account(TEST_USER2).password(TEST_USER2);

		jetty = new JettyServer("localhost", PORT);
		jetty.addComponent(new CDStarServletInitializer(runtime));
		jetty.addComponent(new ServletContainerInitializer() {
			@Override
			public void onStartup(Set<Class<?>> c, ServletContext ctx) throws ServletException {
				final Dynamic dyn = ctx.addServlet("push", new PushServlet());
				dyn.addMapping("/push/*");
				dyn.setLoadOnStartup(1);
			}
		});

		jetty.start();
	}

	protected CDStarArchive makeArchive() throws LoginFailed, TARollbackException, VaultNotFound {
		final Subject subject = runtime.createSubject();
		subject.login(new UsernamePasswordCredentials(TEST_USER, TEST_USER.toCharArray()));
		final CDStarSession session = runtime.getClient(subject).begin(false);
		final CDStarVault vault = session.getVault(TEST_VAULT);
		final CDStarArchive archive = vault.createArchive();
		session.commit();
		return archive;
	}

	@Test
	public void isTriggered() throws Exception {
		startWithFilter(new PushEventFilter(new MapConfig().set("url", "http://localhost:" + PORT + "/push")));

		assertEventCount(0);
		final CDStarArchive archive = makeArchive();
		assertEventCount(1);

		final ChangeEvent event = capturedEvents.get(0);
		assertEquals(archive.getId(), event.getArchive());
		assertEquals(archive.getNextRev(), event.getRevision());
		assertEquals(archive.getParentRev(), event.getParent());
		assertEquals(archive.getSession().getSessionId(), event.getTx());
	}

	@Test
	public void retryOnError() throws Exception {
		startWithFilter(new PushEventFilter(
			new MapConfig().set("url", "http://localhost:" + PORT + "/push").set("retry.delay", "100")));

		final AtomicInteger c = new AtomicInteger(0);
		final List<String> retryCounts = new ArrayList<>();

		// Fire 10 events. Let the 5th fail with a timeout.
		onEvent = (rq, rs, e) -> {
			synchronized (retryCounts) {
				retryCounts.add(rq.getHeader("Push-Retry"));
				if (c.incrementAndGet() == 5)
					rs.setStatus(500);
			}
		};

		for (int i = 0; i < 10; i++)
			makeArchive();

		assertEventCount(11);

		assertEquals(11, retryCounts.size());
		assertEquals("1", retryCounts.get(5));
	}

	@Test
	public void isTriggeredForMultipleArchives() throws Exception {
		startWithFilter(new PushEventFilter(new MapConfig().set("url", "http://localhost:" + PORT + "/push")));

		final Subject subject = runtime.createSubject();
		subject.login(new UsernamePasswordCredentials(TEST_USER, TEST_USER.toCharArray()));
		final CDStarSession session = runtime.getClient(subject).begin(false);
		final CDStarVault vault = session.getVault(TEST_VAULT);
		a1 = vault.createArchive();
		a2 = vault.createArchive();

		assertEventCount(0);
		session.commit();
		assertEventCount(2);

		assertExactlyOne(capturedEvents, e -> a1.getId().equals(e.getArchive()));
		assertExactlyOne(capturedEvents, e -> a2.getId().equals(e.getArchive()));
	}

}
