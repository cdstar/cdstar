package de.gwdg.cdstar.ext.elastic;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import org.junit.Test;

import com.fasterxml.jackson.databind.JsonNode;

import de.gwdg.cdstar.SharedObjectMapper;
import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.ext.elastic.dao.FileDocument;
import de.gwdg.cdstar.ext.elastic.dao.IndexDocument;
import de.gwdg.cdstar.runtime.search.SearchQuery;

public class SearchProviderTest {

	@Test
	public void testQueryBuilder() throws Exception {
		final ElasticSearchProvider esp = new ElasticSearchProvider(new ElasticSearchClient(null));

		final SearchQuery q = SearchQuery.builder()
				.vault("testVault")
				.principal("test@test")
				.group("testGroup")
				.group("group2")
				.limit(13)
				.order("id", "-score", "name DESC")
				.query("dc.title:\"Cat pictures\"")
				.fields("meta.field", "invalid_field()")
				.scrollId(SearchTask.encodeScrollID(
						SharedObjectMapper.json_compact.valueToTree(
								List.of("123", "0.13", "test.txt"))))
				.build();

		var task = new SearchTask(esp, q);
		var prepared = task.prepareRequest();

		assertEquals(13, prepared.getLimit());
		assertEquals("testVault", prepared.getIndex());
		assertEquals("_id,desc(_score),desc(name)", Utils.join(",", Utils.map(prepared.getSort(), SortKey::toString)));
		assertEquals("[\"123\",\"0.13\",\"test.txt\"]", prepared.getSearchAfter().toString());
		assertTrue(prepared.getSourceInclude().contains(IndexDocument.ID_FIELD));
		assertTrue(prepared.getSourceInclude().contains(IndexDocument.SCOPE_MARKER));
		assertTrue(prepared.getSourceInclude().contains(FileDocument.NAME_FIELD));
		assertTrue(prepared.getSourceInclude().contains("meta.field"));
		assertFalse(prepared.getSourceInclude().contains("invalid_field()"));

		var dsl = prepared.buildSearchBody();

		// The exact query may change, but the input should appear somewhere
		final JsonNode terms_query = dsl.path("query").path("bool").path("must").path("query_string");
		assertEquals(q.getQuery(), terms_query.path("query").asText());

		final JsonNode access_query = dsl.path("query").path("bool").path("filter").path("terms").path("__read_access");
		List<String> accessList = toList(access_query);
		assertEquals(new HashSet<>(accessList),
				new HashSet<>(Arrays.asList("test@test", "@testGroup", "@group2", "$any")));

		var fakeResult = esp.getSearchClient().om.createObjectNode();
		fakeResult.with("hits").put("total", 1);
		var hit = fakeResult.with("hits").withArray("hits").addObject();
		hit.put("_id", "fake");
		hit.with("_source").put("scope", "archive");
		hit.with("_source").with("meta").withArray("field").add("value1");

		var mapped = task.mapResults(prepared.new ESSearchResult(fakeResult));
		var firstHit = mapped.hits().get(0);
		assertEquals("value1", firstHit.getFields().get("meta.field").get(0).asText());
	}

	List<String> toList(JsonNode node) {
		assertTrue("Not an array: " + node.toString(), node.isArray());
		final List<String> list = new ArrayList<>(node.size());
		node.forEach(n -> list.add(n.asText()));
		return list;
	}

}
