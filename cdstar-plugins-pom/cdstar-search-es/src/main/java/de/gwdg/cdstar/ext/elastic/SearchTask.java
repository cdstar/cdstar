package de.gwdg.cdstar.ext.elastic;

import java.io.IOException;
import java.net.ConnectException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;

import de.gwdg.cdstar.Promise;
import de.gwdg.cdstar.SharedObjectMapper;
import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.ext.elastic.SearchRequest.ESSearchResult;
import de.gwdg.cdstar.ext.elastic.dao.FileDocument;
import de.gwdg.cdstar.ext.elastic.dao.IndexDocument;
import de.gwdg.cdstar.runtime.search.SearchException;
import de.gwdg.cdstar.runtime.search.SearchHit;
import de.gwdg.cdstar.runtime.search.SearchQuery;
import de.gwdg.cdstar.runtime.search.SearchResult;

public class SearchTask {

	private SearchQuery query;
	private Set<FieldQuery> fieldQueries;
	private ElasticSearchProvider provider;

	private static final String ES_ID_FIELD = "_id";
	private static final String ES_SCORE_FIELD = "_score";
	private static final SortKey TIE_BREAKER_KEY = new SortKey(ES_ID_FIELD, false);
	private static final Map<String, String> orderFieldAlias = Map.of("score", ES_SCORE_FIELD, "id", ES_ID_FIELD);
	private static final List<SortKey> defautSort = Arrays.asList(new SortKey(ES_SCORE_FIELD, true), TIE_BREAKER_KEY);

	public SearchTask(ElasticSearchProvider elasticSearchProvider, SearchQuery q) {
		this.provider = elasticSearchProvider;
		this.query = q;
		this.fieldQueries = q.getFields().stream().map(FieldQuery::new).collect(Collectors.toSet());
	}

	public Promise<SearchResult> submit() {
		try {
			return prepareRequest().submit().map(this::mapResults, this::mapErrors);
		} catch (final SearchException e) {
			return Promise.ofError(e);
		}
	}

	SearchRequest prepareRequest() throws SearchException {
		var indexName = provider.indexNameFromVault(query.getVault());
		var esq = provider.getSearchClient().search(indexName);
		esq.limit(query.getLimit());

		final List<SortKey> sortKeys = getSortKeys();
		esq.sort(sortKeys);

		if (Utils.notNullOrEmpty(query.getScrollId())) {
			var scrollValues = decodeScrollId(query.getScrollId());
			if (scrollValues.size() != sortKeys.size())
				throw new SearchException("Invalid scroll ID");
			esq.searchAfter(scrollValues);
		}

		/**
		 * The full query :
		 *
		 * <pre>
		 *   bool:
		 *     must:
		 *       query_string:
		 *         query: q.getQuery()
		 *     filter:
		 *       terms:
		 *         __read_access: [... $any, subjects and groups ...]
		 * </pre>
		 */

		var qdsl = esq.dsl();

		// Build bool.must.query_string query
		qdsl.with("bool")
				.with("must")
				.with("query_string")
				.put("query", query.getQuery());

		// Build __read_access filter
		var allow = qdsl.with("bool")
				.with("filter")
				.with("terms")
				.withArray(IndexDocument.READ_ACCESS_FIELD);
		getAllowList().forEach(principal -> allow.add(principal));

		esq.sources(IndexDocument.ID_FIELD, IndexDocument.SCOPE_MARKER, FileDocument.NAME_FIELD);
		fieldQueries.forEach(fq -> fq.apply(esq));

		return esq;
	}

	SearchResult mapResults(ESSearchResult result) throws SearchException {
		try {
			var hits = new ArrayList<SearchHit>();
			for (var hit : result.hits()) {
				Map<String, JsonNode> fieldQueryResults = Map.of();
				if(!fieldQueries.isEmpty()) {
					var tmp = new HashMap<String, JsonNode>(fieldQueries.size());
					for(var fq: fieldQueries) {
						fq.extract(hit).ifPresent(fqr -> tmp.put(fq.query, fqr));
					}
					fieldQueryResults = tmp;
				}
				hits.add(new HitWrapper(hit, hit.source(IndexDocument.class), fieldQueryResults));
			}
			return new ResultWrapper(result, hits);
		} catch (final JsonProcessingException e) {
			throw new SearchException("Failed to parse response from search backend.", e);
		}
	}

	private SearchResult mapErrors(Throwable error) throws Throwable {
		if (error instanceof ConnectException) {
			throw new SearchException("Unable to connect to search backend.", error);
		}

		if (error instanceof ESErrorResponse) {
			final ESErrorResponse err = (ESErrorResponse) error;
			if (err.getType().equals("index_not_found_exception"))
				throw new SearchException("Search index not found for this vault.", error);
			if (err.getType().equals("search_phase_execution_exception"))
				throw new SearchException("Query failed: " + err.getReason(), error);
		}

		throw new SearchException("Search failed for unknown reason", error);
	}

	private List<SortKey> getSortKeys() {
		if (query.getOrder().isEmpty())
			return defautSort;

		var sort = query.getOrder().stream()
				.map(SortKey::valueOf)
				.map(key -> {
					if (orderFieldAlias.containsKey(key.getField()))
						return new SortKey(orderFieldAlias.get(key.getField()), key.isReversed());
					return key;
				}).collect(Collectors.toCollection(ArrayList::new));

		if (!sort.stream().anyMatch(s -> s.getField().equals(TIE_BREAKER_KEY.getField())))
			sort.add(TIE_BREAKER_KEY);

		return sort;
	}

	private List<String> getAllowList() {
		var allow = new ArrayList<String>();
		allow.add("$any");
		if (Utils.notNullOrEmpty(query.getPrincipal()))
			allow.add(query.getPrincipal());
		if (query.getGroups() != null) {
			for (final String group : query.getGroups())
				allow.add("@" + group);
		}
		return allow;
	}

	public static String encodeScrollID(ArrayNode list) {
		try {
			return Base64.getUrlEncoder().encodeToString(SharedObjectMapper.json_compact.writeValueAsBytes(list));
		} catch (final JsonProcessingException e) {
			throw Utils.wtf("Failed to turn a string array into json??", e);
		}
	}

	public static ArrayNode decodeScrollId(String id) throws SearchException {
		try {
			var valueList = SharedObjectMapper.json.readTree(Base64.getUrlDecoder().decode(id));
			if (!valueList.isArray())
				throw new SearchException("Invalid scroll ID");
			for (var value : valueList) {
				if (!(value.isNull() || value.isBoolean() | value.isNumber() || value.isTextual()))
					throw new SearchException("Invalid scroll ID");
			}
			return (ArrayNode) valueList;
		} catch (final IllegalArgumentException | IOException e) {
			throw new SearchException("Invalid scroll ID");
		}
	}

}
