package de.gwdg.cdstar.ext.elastic;

import de.gwdg.cdstar.Promise;
import de.gwdg.cdstar.runtime.search.SearchProvider;
import de.gwdg.cdstar.runtime.search.SearchQuery;
import de.gwdg.cdstar.runtime.search.SearchResult;

public class ElasticSearchProvider implements SearchProvider {

	private static final String MASK_PLACEHOLDER = "{}";

	private final ElasticSearchClient es;
	private String indexMask;

	public ElasticSearchProvider(ElasticSearchClient esClient) {
		es = esClient;
		setIndexMask(MASK_PLACEHOLDER);
	}

	public void setIndexMask(String mask) {
		final int i = mask.indexOf(MASK_PLACEHOLDER);
		if (i == -1 || mask.indexOf(MASK_PLACEHOLDER, i + 1) != -1)
			throw new IllegalArgumentException("Mask must contain '{}' exactly once");
		if (mask.contains(","))
			throw new IllegalArgumentException("Mask must not contain ','");
		indexMask = mask;
	}

	String indexNameFromVault(String vault) {
		return indexMask.replace(MASK_PLACEHOLDER, vault);
	}

	@Override
	public Promise<SearchResult> search(SearchQuery q) {
		return new SearchTask(this, q).submit();
	}

	ElasticSearchClient getSearchClient() {
		return es;
	}

}
