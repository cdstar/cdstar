package de.gwdg.cdstar.ext.elastic;

import java.util.List;

import de.gwdg.cdstar.ext.elastic.SearchRequest.ESSearchResult;
import de.gwdg.cdstar.runtime.search.SearchHit;
import de.gwdg.cdstar.runtime.search.SearchResult;

class ResultWrapper implements SearchResult {

	private final List<SearchHit> hits;
	private final ESSearchResult results;

	public ResultWrapper(ESSearchResult result, List<SearchHit> hits) {
		this.results = result;
		this.hits = hits;
	}

	@Override
	public List<SearchHit> hits() {
		return hits;
	}

	@Override
	public String getScrollID() {
		if (hits.isEmpty())
			return null;
		return SearchTask.encodeScrollID(results.getLastSortValues());
	}

	@Override
	public long getTotal() {
		return results.getTotal();
	}

}