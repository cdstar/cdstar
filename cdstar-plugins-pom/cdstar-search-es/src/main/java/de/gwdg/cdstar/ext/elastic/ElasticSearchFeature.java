package de.gwdg.cdstar.ext.elastic;

import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.runtime.Config;
import de.gwdg.cdstar.runtime.Plugin;
import de.gwdg.cdstar.runtime.RuntimeContext;
import de.gwdg.cdstar.runtime.listener.RuntimeListener;

/**
 * Example and default values:
 *
 * <pre>
 * plugin.es:
 *   class: ElasticSearchFeature
 *   url: http://localhost:9200/
 *   index: "cdstar-{}"
 * </pre>
 *
 */
@Plugin(name = { "search-es" })
public class ElasticSearchFeature implements RuntimeListener {

	private final Config cfg;
	private ElasticSearchClient client;
	private ElasticSearchProvider provider;

	public ElasticSearchFeature(Config cfg) {
		cfg.setDefault("url", "http://localhost:9200/");
		cfg.setDefault("index", "cdstar-{}");
		this.cfg = cfg;
	}

	@Override
	public void onInit(RuntimeContext ctx) throws Exception {
		client = new ElasticSearchClient(cfg.getURI("url"));
		client.resolveVersion();
		provider = new ElasticSearchProvider(client);
		provider.setIndexMask(cfg.get("index"));
		ctx.register(provider);
	}

	@Override
	public void onShutdown(RuntimeContext ctx) {
		Utils.closeQuietly(client);
	}

}
