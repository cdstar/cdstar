package de.gwdg.cdstar.ext.elastic;

import java.util.Optional;
import java.util.regex.Pattern;

import com.fasterxml.jackson.databind.JsonNode;

import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.ext.elastic.SearchRequest.ESSearchHit;

public class FieldQuery {
	String query;
	private static final Pattern SIMPLE_FIELD_NAME = Pattern.compile(
			"^[a-z][a-z0-9-_:]+(\\.[a-z][a-z0-9-_:]+)*$",
			Pattern.CASE_INSENSITIVE);

	public FieldQuery(String query) {
		this.query = query;
	}

	public static boolean isSimple(String query) {
		return SIMPLE_FIELD_NAME.matcher(query).matches();
	}

	@Override
	public int hashCode() {
		return query.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		return obj instanceof FieldQuery && ((FieldQuery) obj).query.equals(query);
	}

	/**
	 * Modify a prepared search to also fetch any data required for this field query.
	 */
	public void apply(SearchRequest rq) {
		if (isSimple(query))
			rq.addSource(query);
	}

	/**
	 * Extract the field query results from a single search hit.
	 */
	public Optional<JsonNode> extract(ESSearchHit hit) {
		var source = hit.getResult().path("_source");
		if (isSimple(query)) {
			JsonNode node = source;
			for (var walk : Utils.split(query, '.')) {
				node = node.path(walk);
			}
			if (!node.isMissingNode())
				return Optional.of(node);
		}
		return Optional.empty();
	}

}
