package de.gwdg.cdstar.ext.elastic;

import java.util.Map;

import com.fasterxml.jackson.databind.JsonNode;

import de.gwdg.cdstar.ext.elastic.SearchRequest.ESSearchHit;
import de.gwdg.cdstar.ext.elastic.dao.ArchiveDocument;
import de.gwdg.cdstar.ext.elastic.dao.FileDocument;
import de.gwdg.cdstar.ext.elastic.dao.IndexDocument;
import de.gwdg.cdstar.runtime.search.SearchHit;

class HitWrapper implements SearchHit {

	private final ESSearchHit hit;
	private final IndexDocument idx;
	private Map<String, JsonNode> fields;

	public HitWrapper(ESSearchHit hit, IndexDocument idx, Map<String, JsonNode> fields) {
		this.hit = hit;
		this.idx = idx;
		this.fields=fields;
	}

	@Override
	public String getId() {
		return idx.id;
	}

	@Override
	public String getType() {
		return idx instanceof FileDocument ? "file" : (idx instanceof ArchiveDocument ? "archive" : "other");
	}

	@Override
	public String getName() {
		if (idx instanceof FileDocument)
			return ((FileDocument) idx).name;
		return null;
	}

	@Override
	public Map<String, JsonNode> getFields() {
		return fields;
	}

	@Override
	public double getScore() {
		return hit.getScore();
	}

}