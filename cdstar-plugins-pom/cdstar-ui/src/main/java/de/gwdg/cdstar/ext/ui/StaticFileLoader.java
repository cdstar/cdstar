package de.gwdg.cdstar.ext.ui;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;

import de.gwdg.cdstar.MimeUtils;
import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.web.common.model.ErrorResponse;

/**
 * A caching loader for (small) static files. Can load from multiple on-disk
 * folders, or the class path.
 */
public class StaticFileLoader {

	private static final Logger log = LoggerFactory.getLogger(StaticFileLoader.StaticFile.class);

	public static class StaticFile {
		final String name;
		final String contentType;
		final ByteBuffer content;

		private StaticFile(String name, String contentType, ByteBuffer content) {
			super();
			this.name = name;
			this.contentType = contentType;
			this.content = content;
		}

		public String getName() {
			return name;
		}

		public String getContentType() {
			return contentType;
		}

		public ByteBuffer getBuffer() {
			return content.duplicate();
		}

		public int getSize() {
			return content.remaining();
		}

	}

	private static final int MAX_SIZE = 1024 * 1024 * 64; // 64MB

	LoadingCache<String, Optional<StaticFile>> cache = Caffeine.newBuilder()
		.maximumWeight(1024 * 1024 * 64)
		.weigher((String key, Optional<StaticFile> value) -> value.isPresent() ? value.get().content.capacity() : 64)
		.build(this::load);

	List<Path> diskSources = new ArrayList<>(1);
	List<String> uriSources = new ArrayList<>(1);

	public Optional<StaticFile> get(String name) {
		return cache.get(name);
	}

	public synchronized void addClassPath(String path) {
		if (!path.startsWith("/"))
			path = "/" + path;
		if (!path.endsWith("/"))
			path = path + "/";
		uriSources.remove(path);
		uriSources.add(path);
	}

	public synchronized void addDiskPath(Path path) {
		path = path.toAbsolutePath().normalize();
		diskSources.remove(path);
		diskSources.add(path);
	}

	private synchronized Optional<StaticFile> load(String name) {
		if (name.contains("./") || name.contains("../"))
			throw new ErrorResponse(404, "AssetNotFound", "Unable to serve asset").detail("path", name);

		for (final Path fpath : diskSources) {
			final Path fullpah = fpath.resolve(name);
			if (!Files.isRegularFile(fullpah))
				continue;
			try (FileChannel ch = FileChannel.open(fullpah)) {
				final long size = ch.size();
				if (size > MAX_SIZE)
					throw new IOException("Static file found, but too large: " + fullpah + " (" + size + ")");
				final ByteBuffer buf = ByteBuffer.allocate((int) size);
				while (ch.read(buf) > -1)
					;
				buf.flip();
				if (buf.remaining() != size)
					throw new IOException("Static file size changed while reading: " + fullpah);
				return Optional
					.of(new StaticFile(name, MimeUtils.guess(name, false).getMimeDefault(MimeUtils.OCTET_STREAM), buf));
			} catch (final IOException e) {
				log.warn("Failed to load static file", e);
				return Optional.empty();
			}
		}

		for (final String uriBase : uriSources) {
			try (InputStream io = getClass().getResourceAsStream(uriBase + name)) {
				if (io == null)
					continue;
				final ByteArrayOutputStream os = new ByteArrayOutputStream();
				Utils.copy(io, os);
				final ByteBuffer buf = ByteBuffer.wrap(os.toByteArray());
				return Optional
					.of(new StaticFile(name, MimeUtils.guess(name, false).getMimeDefault(MimeUtils.OCTET_STREAM), buf));
			} catch (final IOException e) {
				log.warn("Failed to load static file", e);
				return Optional.empty();
			}
		}

		return Optional.empty();
	}

}
