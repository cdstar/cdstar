package de.gwdg.cdstar.ext.ui;

import java.util.HashMap;
import java.util.Map;

public class Template {

	Map<String, Object> vars = new HashMap<>(3);
	String template;

	public Template(String name) {
		template = name;
	}

	public String getTemplate() {
		return template;
	}

	public Map<String, Object> getVars() {
		return vars;
	}

	public Template set(String key, Object value) {
		vars.put(key, value);
		return this;
	}

}
