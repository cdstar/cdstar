package de.gwdg.cdstar.ext.ui;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.ext.ui.StaticFileLoader.StaticFile;
import de.gwdg.cdstar.rest.api.Blueprint;
import de.gwdg.cdstar.rest.api.RestBlueprint;
import de.gwdg.cdstar.rest.api.RestConfig;
import de.gwdg.cdstar.rest.api.RestContext;
import de.gwdg.cdstar.rest.utils.QueryHelper;
import de.gwdg.cdstar.rest.utils.SessionHelper;
import de.gwdg.cdstar.rest.v3.ArchiveEndpoint;
import de.gwdg.cdstar.rest.v3.async.SearchHandler;
import de.gwdg.cdstar.runtime.Config;
import de.gwdg.cdstar.runtime.ConfigException;
import de.gwdg.cdstar.runtime.Plugin;
import de.gwdg.cdstar.runtime.RuntimeContext;
import de.gwdg.cdstar.runtime.client.CDStarArchive;
import de.gwdg.cdstar.runtime.client.CDStarAttribute;
import de.gwdg.cdstar.runtime.client.CDStarFile;
import de.gwdg.cdstar.runtime.client.CDStarSession;
import de.gwdg.cdstar.runtime.client.CDStarVault;
import de.gwdg.cdstar.runtime.client.exc.AccessError;
import de.gwdg.cdstar.runtime.client.exc.ArchiveNotFound;
import de.gwdg.cdstar.runtime.client.exc.VaultNotFound;
import de.gwdg.cdstar.runtime.search.SearchException;
import de.gwdg.cdstar.runtime.search.SearchProvider;
import de.gwdg.cdstar.web.common.model.ErrorResponse;
import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapperBuilder;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;
import freemarker.template.TemplateModelException;

@Plugin(name = "cdstar-ui")
@Blueprint(path = "/ui", name = "cdstar-ui")
public class UIBlueprint implements RestBlueprint {

	private static final Logger log = LoggerFactory.getLogger(UIBlueprint.class);

	public static String templateLocation = "/de/gwdg/cdstar/ext/ui/templates";
	public static String assetLocation = "/de/gwdg/cdstar/ext/ui/assets";

	private final StaticFileLoader staticLoader;
	private Configuration tpl;
	private String uiPath;
	private String basePath;

	public UIBlueprint() {
		staticLoader = new StaticFileLoader();
		staticLoader.addClassPath(assetLocation);
		staticLoader.addClassPath("/META-INF/resources/webjars/");
	}

	@Override
	public void configure(RestConfig conf) throws ConfigException {
		// Example on how to override the settings defined in the Blueprint annotation:
		// conf=conf.getParent().createChildConfig("/ui2", conf.doesInherit());

		final Config cdstarConfig = conf.lookup(RuntimeContext.class).getConfig();

		uiPath = conf.getPath();
		basePath = cdstarConfig.get("api.context") + uiPath.substring(1) + "/";

		tpl = new Configuration(Configuration.VERSION_2_3_28);
		tpl.setTagSyntax(Configuration.AUTO_DETECT_TAG_SYNTAX);
		tpl.setObjectWrapper(new DefaultObjectWrapperBuilder(Configuration.VERSION_2_3_28).build());
		tpl.setDefaultEncoding("UTF-8");
		tpl.setOutputEncoding("UTF-8");
		tpl.setClassForTemplateLoading(getClass(), templateLocation);
		tpl.setRecognizeStandardFileExtensions(true);
		tpl.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);

		try {
			tpl.setSharedVariable("uibase", basePath);
			tpl.setSharedVariable("apibase", cdstarConfig.get("api.context"));
		} catch (final TemplateModelException e) {
			throw Utils.wtf(e);
		}

		conf.route("/_assets/<asset:path>").GET(this::getAsset);
		conf.route("/").GET(this::getVaultList);
		conf.route("/_login").GET(this::forceLogin);
		conf.route("/<vault>").GET(this::getVault);
		conf.route("/<vault>/<archive>").GET(this::getArchiveInfo);
		conf.route("/<vault>/<archive>/<file:path>").GET(this::getFileInfo);

		conf.mapResponse("text/html", Template.class, this::mapToTemplate);
		conf.mapResponse("*;q=0.1", Template.class, this::mapToTemplate);
		conf.mapResponse("text/html", ErrorResponse.class, this::errorToHtml);

	}

	private void errorToHtml(RestContext ctx, ErrorResponse val) throws IOException {
		ctx.status(val.getStatus());
		ctx.write(new Template("error.ftlh").set("error", val));
	}

	private void mapToTemplate(RestContext ctx, Template val) throws IOException {
		final ByteArrayOutputStream out = new ByteArrayOutputStream();
		try {
			tpl.getTemplate(val.getTemplate())
					.process(val.getVars(), new OutputStreamWriter(out, StandardCharsets.UTF_8));
			ctx.header("Content-Type", "text/html; charset=utf-8");
			ctx.write(out.toByteArray());
		} catch (final TemplateException e) {
			log.error("Failed to render template", e);
			throw Utils.wtf(e);
		}
	}

	public Void getAsset(RestContext ctx) throws IOException {
		final String name = ctx.getPathParam("asset");
		final StaticFile asset = staticLoader.get(name)
				.orElseThrow(
						() -> new ErrorResponse(404, "AssetNotFound", "Unable to serve asset").detail("path", name));

		ctx.header("Content-Type", asset.getContentType());
		ctx.write(asset.getBuffer());
		return null;
	}

	public Template getVaultList(RestContext ctx) throws IOException {
		final Template tpl = new Template("vaultlist.ftlh");
		final CDStarSession session = SessionHelper.getCDStarSession(ctx, true);
		final List<CDStarVault> vaults = new ArrayList<>();
		for (final String name : session.getVaultNames()) {
			try {
				vaults.add(session.getVault(name));
			} catch (final VaultNotFound e) {
				// pass
			}
		}
		vaults.sort(Comparator.comparing(CDStarVault::getName));
		tpl.set("user", SessionHelper.getSubject(ctx).getPrincipal());
		tpl.set("vaults", vaults);
		return tpl;
	}

	public Template getVault(RestContext ctx) throws IOException, VaultNotFound {

		final Template tpl = new Template("vault.ftlh");
		final CDStarSession session = SessionHelper.getCDStarSession(ctx, true);
		final CDStarVault vault = session.getVault(ctx.getPathParam("vault"));
		tpl.set("user", SessionHelper.getSubject(ctx).getPrincipal());
		tpl.set("vault", vault);

		// Handle search (or scroll) requests
		var search = ctx.getService(SearchProvider.class);
		tpl.set("searchAvailable", search != null);

		if (ctx.getQueryParam(SearchHandler.PARAM_Q) != null) {
			if (search == null)
				throw new ErrorResponse(501, "SearchDisabled", "Search is not available");

			var query = SearchHandler.prepareQuery(vault.getName(), new QueryHelper(ctx), SessionHelper.getSubject(ctx))
					.build();
			tpl.set("searchQuery", query);

			// Search may take a while -> switch to async
			var resultPromise = search.search(query);
			var async = ctx.startAsync();

			async.addCloseListener((ac, err) -> resultPromise.cancel());
			resultPromise.mapVoid(sr -> {
				tpl.set("searchResult", sr);
				ctx.write(tpl);
				ctx.close();
			}).then(null, e -> {
				if (e instanceof SearchException) {
					final SearchException se = (SearchException) e;
					ctx.abort(new ErrorResponse(400, "SearchFailed", se.getMessage()));
				}
				ctx.abort(e);
			});

			return null; // async response
		}

		return tpl;
	}

	public Template getArchiveInfo(RestContext ctx) throws IOException, VaultNotFound, ArchiveNotFound {
		final Template tpl = new Template("archive.ftlh");
		final CDStarSession session = SessionHelper.getCDStarSession(ctx, true);
		final CDStarVault vault = session.getVault(ctx.getPathParam("vault"));
		final CDStarArchive archive = vault.loadArchive(ctx.getPathParam("archive"));

		List<CDStarFile> files = null;
		long totalSize = -1;
		try {
			files = archive.getFiles();
			totalSize = files.stream().mapToLong(CDStarFile::getSize).sum();
		} catch (final AccessError e) {
		}

		List<CDStarAttribute> attr = null;
		try {
			attr = new ArrayList<>(archive.getAttributes());
		} catch (final AccessError e) {
		}

		Map<String, List<String>> aclMap = null;
		try {
			aclMap = ArchiveEndpoint.getArchiveAcl(archive, true).getMap();
		} catch (final AccessError e) {
		}

		tpl.set("user", SessionHelper.getSubject(ctx).getPrincipal());
		tpl.set("vault", vault);
		tpl.set("archive", archive);
		tpl.set("files", files);
		tpl.set("filecount", archive.getFileCount());
		tpl.set("totalSize", totalSize);
		tpl.set("attr", attr);
		tpl.set("acl", aclMap);
		tpl.set("byte2hex", (Function<byte[], String>) Utils::bytesToHex);
		return tpl;
	}

	public Template getFileInfo(RestContext ctx) throws IOException, VaultNotFound, ArchiveNotFound {
		final Template tpl = new Template("file.ftlh");
		final CDStarSession session = SessionHelper.getCDStarSession(ctx, true);
		final CDStarVault vault = session.getVault(ctx.getPathParam("vault"));
		final CDStarArchive archive = vault.loadArchive(ctx.getPathParam("archive"));
		final CDStarFile file = archive.getFile(ctx.getPathParam("file"));

		List<CDStarAttribute> attr = null;
		try {
			attr = new ArrayList<>(file.getAttributes());
		} catch (final AccessError e) {
			attr = new ArrayList<>();
		}

		tpl.set("user", SessionHelper.getSubject(ctx).getPrincipal());
		tpl.set("vault", vault);
		tpl.set("archive", archive);
		tpl.set("file", file);
		tpl.set("attr", attr);
		tpl.set("byte2hex", (Function<byte[], String>) Utils::bytesToHex);
		return tpl;
	}

	public Void forceLogin(RestContext ctx) throws IOException {
		if (SessionHelper.getSubject(ctx).isAnonymous()) {
			ctx.status(401);
			ctx.header("WWW-Authenticate", "BASIC realm=\"cdstar\"");
			ctx.write("Please login");
			ctx.close();
		} else {
			ctx.status(303);
			ctx.header("Location", basePath);
			ctx.write("You are already authenticated");
		}
		return null;
	}

}
