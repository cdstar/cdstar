package de.gwdg.cdstar.ext.ui;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import de.gwdg.cdstar.auth.Subject;
import de.gwdg.cdstar.config.MapConfig;
import de.gwdg.cdstar.rest.RestConfigImpl;
import de.gwdg.cdstar.rest.testutils.TestClient;
import de.gwdg.cdstar.rest.testutils.TestResponse;
import de.gwdg.cdstar.runtime.Config;
import de.gwdg.cdstar.runtime.RuntimeContext;

public class UIModuleTest {

	private UIBlueprint ui;
	private RuntimeContext runtimeMock;
	private RestConfigImpl cfg;
	private TestClient client;

	@Before
	public void setup() {
		cfg = new RestConfigImpl(null);
		runtimeMock = Mockito.mock(RuntimeContext.class);
		final Config fakeConfig = new MapConfig().set("api.context", "/");
		Mockito.when(runtimeMock.getConfig()).thenReturn(fakeConfig);
		cfg.register(runtimeMock);
		ui = new UIBlueprint();
		cfg.install(ui);
		client = new TestClient(cfg);
	}

	@Test
	public void testGetAsset() throws IOException, URISyntaxException {
		final String head = new String(
			Files.readAllBytes(
				Paths.get(
					getClass().getResource("/de/gwdg/cdstar/ext/ui/templates/HTML_head.html").toURI())),
			StandardCharsets.UTF_8);
		final Matcher m = Pattern.compile("_assets/[^'\"]+").matcher(head);
		while (m.find()) {
			final String asset = "/ui/" + m.group();
			assertEquals("Failed to fetch asset: " + asset, 200, client.GET(asset).submit().getStatus());
		}
	}

	@Test
	public void testForceLogin() {
		final Subject subjectMock = Mockito.mock(Subject.class);
		Mockito.when(runtimeMock.createSubject()).thenReturn(subjectMock);
		Mockito.when(subjectMock.isAnonymous()).thenReturn(true);
		TestResponse r = client.GET("/ui/_login").submit();
		r.assertStatus(401);

		Mockito.when(subjectMock.isAnonymous()).thenReturn(false);
		r = client.GET("/ui/_login").submit();
		r.assertStatus(303);
	}

}
