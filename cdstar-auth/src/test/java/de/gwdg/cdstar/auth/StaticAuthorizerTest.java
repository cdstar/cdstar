package de.gwdg.cdstar.auth;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import de.gwdg.cdstar.auth.realm.CheckResult;
import de.gwdg.cdstar.auth.simple.Account;
import de.gwdg.cdstar.auth.simple.SimpleAuthorizer;

public class StaticAuthorizerTest {

	private SimpleAuthorizer auth;
	private UsernamePasswordCredentials creds;
	private Account acc;

	@Before
	public void setupTestAccount() {
		auth = new SimpleAuthorizer();
		creds = new UsernamePasswordCredentials("Test", auth.getDomain(), "password".toCharArray());

		acc = auth.account(creds.getName()).password(creds.getPassword());
	}

	@Test
	public void testSessionInfo() {
		final Session session = auth.login(creds);
		assertFalse(session.isRemembered());
		assertEquals(auth, session.getAuthenticator());

		final Principal principal = session.getPrincipal();

		assertEquals("Test", principal.getId());
		assertEquals("static", principal.getDomain());
	}

	@Test
	public void testLogin() {
		UsernamePasswordCredentials tmp;

		// These should fail
		tmp = new UsernamePasswordCredentials("UnknownUser", creds.getDomain(), creds.getPassword());
		assertNull(auth.login(tmp));
		tmp = new UsernamePasswordCredentials(creds.getName(), creds.getDomain(), "Wrong Password".toCharArray());
		assertNull(auth.login(tmp));
		tmp = new UsernamePasswordCredentials(creds.getName(), "WRONG_REALM", creds.getPassword());
		assertNull(auth.login(tmp));

		// No domain login
		tmp = new UsernamePasswordCredentials(creds.getName(), null, creds.getPassword());
		assertEquals(acc, auth.login(tmp).getPrincipal());

		// Full login
		assertEquals(acc, auth.login(creds).getPrincipal());
	}

	@Test
	public void testPermissions() throws Exception {
		acc.withPermissions("p1", "p2:*", "p3:x");

		final Session session = auth.login(creds);

		assertTrue(check(session, "p1:whatever"));
		assertTrue(check(session, "p2"));
		assertTrue(check(session, "p2:whatever"));
		assertFalse(check(session, "p3"));

		assertTrue(check(session, "p3:x"));
		assertFalse(check(session, "p3:whatever"));
		assertFalse(check(session, "p4"));
	}

	@Test
	public void testGroupMembership() throws Exception {
		auth.addGroup("user", "userRole");
		auth.addGroup("admin", "adminRole");
		auth.addRole("userRole", "user:stuff");
		auth.addRole("adminRole", "admin:stuff");
		acc.withGroups("user");

		final Session session = auth.login(creds);

		assertTrue(check(session, "user:stuff"));
		assertFalse(check(session, "admin:stuff"));
		assertEquals(CheckResult.YES, auth.isMemberOf(session, "user"));
		assertEquals(CheckResult.YES, auth.isMemberOf(session, "user", acc.getDomain()));
		assertEquals(CheckResult.UNKNOWN, auth.isMemberOf(session, "admin"));
	}

	@Test
	public void testRolesWithoutGroups() throws Exception {
		auth.addRole("userRole", "user:stuff");
		auth.addRole("adminRole", "admin:stuff");
		acc.withRoles("userRole");

		final Session session = auth.login(creds);
		assertTrue(check(session, "user:stuff"));
		assertFalse(check(session, "admin:stuff"));
	}

	@Test
	public void testForeignAccounts() throws Exception {
		auth.addAnonymous().withPermissions("anon:stuff");
		auth.account("alice", "LDAP").withPermissions("do:stuff").withGroups("ldapGroup");
		final Session mockSession = new MockSession("alice", "LDAP");

		assertTrue(check(mockSession, "do:stuff"));
		assertFalse(check(mockSession, "anon:stuff"));
		assertEquals(CheckResult.YES, auth.isMemberOf(mockSession, "ldapGroup", "LDAP"));
	}

	private boolean check(Session session, String permission) {
		return CheckResult.YES == auth.isPermitted(session, StringPermission.parse(permission));
	}

}
