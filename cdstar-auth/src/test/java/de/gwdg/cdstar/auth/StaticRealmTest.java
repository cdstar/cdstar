package de.gwdg.cdstar.auth;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.Rule;
import org.junit.Test;

import de.gwdg.cdstar.auth.realm.CheckResult;
import de.gwdg.cdstar.auth.realm.StaticRealm;
import de.gwdg.cdstar.config.MapConfig;
import de.gwdg.cdstar.runtime.Config;
import de.gwdg.cdstar.runtime.ConfigException;
import de.gwdg.cdstar.utils.test.TestLogger;

public class StaticRealmTest {

	@Rule
	public TestLogger log = new TestLogger("de.gwdg");

	@Test
	public void testDefaultConfig() throws ConfigException {
		final Config cfg = new MapConfig();
		final StaticRealm realm = new StaticRealm(cfg);
		assertEquals("static", realm.getDomain());
		assertEquals("static", realm.account("test").getDomain());
	}

	@Test
	public void userCoreConfig() throws ConfigException {
		final Config cfg = new MapConfig();
		cfg.set("user.test.password", StaticRealm.hashPassword("testpwd".toCharArray()));
		cfg.set("user.test.permissions", "something, do:stuff");
		cfg.set("user.test.roles", "tr, testRole");
		cfg.set("user.test.groups", "tg, testGroup");

		cfg.set("role.testRole", "xxx, role:stuff");
		cfg.set("role.testGroupRole", "group:stuff");
		cfg.set("group.testGroup", "testGroupRole");
		final StaticRealm realm = new StaticRealm(cfg);

		final Session session = realm.login(new UsernamePasswordCredentials("test", "testpwd".toCharArray()));
		assertNotNull(session);
		assertEquals("static", session.getPrincipal().getDomain());
		assertEquals("test", session.getPrincipal().getId());

		assertEquals(CheckResult.YES, realm.isMemberOf(session, "testGroup"));
		assertEquals(CheckResult.UNKNOWN, realm.isMemberOf(session, "fakeGroup"));

		assertEquals(CheckResult.YES, realm.isPermitted(session, StringPermission.parse("do:stuff")));
		assertEquals(CheckResult.YES, realm.isPermitted(session, StringPermission.parse("role:stuff")));
		assertEquals(CheckResult.YES, realm.isPermitted(session, StringPermission.parse("group:stuff")));
	}

	@Test
	public void testDefaultDomain() throws ConfigException {
		final Config cfg = new MapConfig();
		cfg.set("domain", "myDomain");
		final StaticRealm realm = new StaticRealm(cfg);
		assertEquals("myDomain", realm.getDomain());
		assertEquals("myDomain", realm.account("test").getDomain());
	}

	@Test
	public void testUserWithDomain() throws ConfigException {
		final Config cfg = new MapConfig();
		cfg.set("user.test@myDomain.permissions", "do:stuff");
		final StaticRealm realm = new StaticRealm(cfg);
		assertEquals("static", realm.getDomain());
		assertEquals("myDomain", realm.account("test", "myDomain").getDomain());
		assertEquals(CheckResult.YES, realm.isPermitted(new MockSession("test", "myDomain"), StringPermission.parse("do:stuff")));
	}

	@Test
	public void testUserWithoutPassword() throws ConfigException {
		final Config cfg = new MapConfig();
		cfg.set("user.test.permissions", "do:stuff");
		final StaticRealm realm = new StaticRealm(cfg);

		// No password does not mean empty password.
		assertNull(realm.login(new UsernamePasswordCredentials("test", "".toCharArray())));

		// Still authorizes user, even if it comes from a different
		// authenticator
		assertEquals(CheckResult.YES, realm.isPermitted(new MockSession("test", "static"), StringPermission.parse("do:stuff")));
	}

}
