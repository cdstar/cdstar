package de.gwdg.cdstar.auth;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Rule;
import org.junit.Test;

import de.gwdg.cdstar.auth.realm.InMemorySessionStore;
import de.gwdg.cdstar.auth.simple.SimpleAuthorizer;
import de.gwdg.cdstar.utils.test.TestLogger;

public class InMemorySessionStoreTest {

	@Rule
	public TestLogger log = new TestLogger("de.gwdg");

	@Test
	public void testRememberMe() throws Exception {
		final InMemorySessionStore ta = new InMemorySessionStore("token");
		final SimpleAuthorizer sa = new SimpleAuthorizer("static");
		final AuthConfig ac = new AuthConfigImpl(ta, sa);
		sa.account("test").password("testpwd").withPermissions("foo", "bar:baz");

		final Subject su = ac.getSubject();
		assertTrue(su.tryLogin(new UsernamePasswordCredentials("test", "static", "testpwd".toCharArray())));
		assertTrue(su.isAuthenticated());
		assertFalse(su.isRemembered());

		final String token = su.remember();
		assertNotNull(token);

		final Subject su2 = ac.getSubject();

		assertTrue(su2.tryLogin(new TokenCredentials(token)));
		assertEquals(su.getPrincipal().getFullId(), su2.getPrincipal().getFullId());
		assertFalse(su2.isAuthenticated());
		assertTrue(su2.isRemembered());

		assertTrue(su2.isPermitted(StringPermission.parse("foo")));
		assertFalse(su2.isPermitted(StringPermission.parse("bar:foo")));

		// A logout invalidates a token
		su2.logout();
		assertFalse(su2.tryLogin(new TokenCredentials(token)));
	}

}
