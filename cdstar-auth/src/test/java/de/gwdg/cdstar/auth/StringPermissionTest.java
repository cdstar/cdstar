package de.gwdg.cdstar.auth;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class StringPermissionTest {

	private static StringPermission ofParts(String... parts) {
		return StringPermission.ofParts(parts);
	}

	private static StringPermission parse(String permission) {
		return StringPermission.parse(permission);
	}

	private static void implies(String a, String b) {
		assertTrue("'" + a + "' should imply '" + b + "'",
			parse(a).implies(parse(b)));
	}

	private static void notImplies(String a, String b) {
		assertFalse("'" + a + "' should not imply '" + b + "'",
			parse(a).implies(parse(b)));
	}

	@Test
	public void testExactMatch() {
		implies("A", "A");
		implies("Test", "Test");
		implies("A:b:C", "A:b:C");
		implies("A:", "A:");
	}

	@Test
	public void testPrefixMatch() {
		implies("A", "A:b");
		implies("A", "A:b:c:d:e:f");
		implies("A:b:c:d:e", "A:b:c:d:e:f");
		implies("A:", "A:b");
	}

	@Test
	public void testWildcardMatch() {
		implies("A:*", "A");
		implies("A:*", "A:b");
		implies("A:*", "A:b:c");
		implies("A:*:C", "A:b:C");
		notImplies("A:B:*", "A");
		notImplies("A:*", "B");
		notImplies("A*", "AB");
	}

	@Test
	public void testPostfixMatch() {
		implies("*:C", "A:C");
		// The wildcard matches one element only.
		notImplies("*:C", "A:B:C");
	}

	/**
	 * Empty elements are wildcards, and ignored if they appear at the end
	 */
	@Test
	public void testEmptyElements() {
		implies("A:", "A");
		implies("A:::::", "A");
		notImplies("A:::B", "A:B");
		implies("A:::B", "A:::B");
		notImplies(":::B", ":B");
		implies(":::B", ":::B");
		implies("A::B", "A:x:B");
	}

	@Test
	public void testEscapeChars() throws Exception {
		assertEquals(parse("\\\\:\\::\\*"), ofParts("\\", ":", "*"));
	}

	@Test
	public void testReplace() throws Exception {
		assertEquals(parse("X:B"), parse("A:B").replace(0, "X"));
		assertEquals(parse("*:B"), parse("A:B").replace(0, ""));
		assertEquals(parse(":B"), parse("A:B").replace(0, null));
		assertEquals(parse("A"), parse("A:B").replace(1, null));
		assertEquals(parse("A:::X"), parse("A").replace(3, "X"));
	}

	@Test
	public void testEuality() throws Exception {
		assertEquals(parse("A:\\\\:\\::\\*"), ofParts("A", "\\", ":", "*"));
		assertEquals(parse("A"), ofParts("A", "", null));
		assertEquals(parse("A"), parse("A"));
		assertEquals(parse("A:"), parse("A"));
		assertEquals(parse("A:*"), parse("A"));
		assertEquals(parse("A:*:B"), parse("A::B"));
		assertEquals(parse("A:::::::"), parse("A"));
		assertEquals(parse("A:*:*:B"), parse("A:::B"));
		assertEquals(parse("A:b:C"), parse("A:b:C"));
		assertEquals(parse("A:b:"), parse("A:b"));
		assertNotEquals(parse("A"), parse("a"));
		assertNotEquals(parse("A"), parse("B"));
	}

}
