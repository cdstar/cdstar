package de.gwdg.cdstar.auth;

import static org.junit.Assert.assertEquals;

import org.junit.Rule;
import org.junit.Test;

import de.gwdg.cdstar.auth.realm.CheckResult;
import de.gwdg.cdstar.auth.realm.DomainAuthorizer;
import de.gwdg.cdstar.utils.test.TestLogger;

public class DomainAuthorizerTest {

	@Rule
	public TestLogger log = new TestLogger("de.gwdg");

	@Test
	public void testFoo() throws Exception {
		final DomainAuthorizer a = new DomainAuthorizer();
		a.addDomainPermission("GWDG", StringPermission.parse("vault:GWDG:*"));
		a.addDomainGroup("GWDG", "user", "other@OTHER");

		assertEquals(CheckResult.YES, a.isMemberOf(new MockSession("foo", "GWDG"), "user", "GWDG"));
		assertEquals(CheckResult.YES, a.isMemberOf(new MockSession("foo", "GWDG"), "other", "OTHER"));
		assertEquals(CheckResult.YES, a.isPermitted(new MockSession("foo", "GWDG"), StringPermission.parse("vault:GWDG:anything")));

		assertEquals(CheckResult.UNKNOWN, a.isMemberOf(new MockSession("foo", "GWDG"), "other", "GWDG"));
		assertEquals(CheckResult.UNKNOWN, a.isMemberOf(new MockSession("foo", "GWDG"), "user", "OTHER"));
		assertEquals(CheckResult.UNKNOWN, a.isPermitted(new MockSession("foo", "GWDG"), StringPermission.parse("vault:other:anything")));

		assertEquals(CheckResult.UNKNOWN, a.isMemberOf(new MockSession("foo", "GWDG2"), "user", "GWDG"));
		assertEquals(CheckResult.UNKNOWN, a.isPermitted(new MockSession("foo", "GWDG2"), StringPermission.parse("vault:GWDG:anything")));

	}

}
