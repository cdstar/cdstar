package de.gwdg.cdstar.auth;

import java.util.Collection;
import java.util.Collections;

import de.gwdg.cdstar.auth.realm.Authenticator;

public class MockSession implements Principal, Session {

	private final String id;
	private final String domain;

	MockSession(String id, String domain) {
		this.id = id;
		this.domain = domain;
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public String getDomain() {
		return domain;
	}

	@Override
	public String getProperty(String propertyName) {
		return null;
	}

	@Override
	public Collection<String> getPropertyNames() {
		return Collections.emptySet();
	}

	@Override
	public String setProperty(String propertyName, String value) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean isRemembered() {
		return false;
	}

	@Override
	public Authenticator getAuthenticator() {
		return new Authenticator() {

			@Override
			public String getName() {
				return null;
			}

			@Override
			public void logout(Session who) {
			}

			@Override
			public Session login(Credentials request) {
				return null;
			}
		};
	}

	@Override
	public Principal getPrincipal() {
		return this;
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + "(" + getFullId() + ")";
	}
}