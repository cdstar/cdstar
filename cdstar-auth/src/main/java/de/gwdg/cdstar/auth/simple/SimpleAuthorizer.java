package de.gwdg.cdstar.auth.simple;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.auth.Credentials;
import de.gwdg.cdstar.auth.KnownPrincipalCredentials;
import de.gwdg.cdstar.auth.Permission;
import de.gwdg.cdstar.auth.Principal;
import de.gwdg.cdstar.auth.Session;
import de.gwdg.cdstar.auth.StringPermission;
import de.gwdg.cdstar.auth.UsernamePasswordCredentials;
import de.gwdg.cdstar.auth.realm.Authenticator;
import de.gwdg.cdstar.auth.realm.Authorizer;
import de.gwdg.cdstar.auth.realm.CheckResult;
import de.gwdg.cdstar.auth.realm.GroupResolver;

/**
 * An authenticator and authorizer for static accounts, groups, roles and
 * permissions.
 *
 * Accounts are members of groups, groups implement roles and roles grant
 * permissions. Accounts can also be directly associated with roles or
 * permissions for fine grained control.
 *
 * Account credentials are stored as hashes in memory and defined either as
 * clear-text or as already hashed key:hash pairs. The permission model supports
 * hierarchies and wildcards. See {@link StringPermission} for details and
 * examples.
 *
 * Because all accounts, groups, roles and permissions are held in-memory, this
 * implementation is quite fast, but it also does not persist changes in any
 * way.
 *
 */
public class SimpleAuthorizer implements Authorizer, Authenticator, GroupResolver {
	Account anonymous;
	Map<QName, Account> accounts = new HashMap<>();
	Map<QName, Set<String>> groups = new HashMap<>();
	Map<String, Set<StringPermission>> roles = new HashMap<>();

	String domain = "static";

	public SimpleAuthorizer(String domain) {
		this.domain = domain;
	}

	public SimpleAuthorizer() {
		this("static");
	}

	public Account account(String name) {
		return account(name, getDomain());
	}

	public Account account(String name, String domain) {
		return accounts.computeIfAbsent(new QName(name, domain), key -> new Account(this, key));
	}

	public String getDomain() {
		return domain;
	}

	public Account addAnonymous() {
		if (anonymous == null)
			anonymous = new Account(this, null);
		return anonymous;
	}

	/**
	 * Create or extend a role with the given permissions.
	 */
	public void addRole(String name, String... permissions) {
		roles.computeIfAbsent(name, k -> new HashSet<>())
			.addAll(Utils.map(Arrays.asList(permissions), StringPermission::parse));
	}

	/**
	 * Creates or extends a group with the given roles
	 */
	public void addGroup(String name, String... roles) {
		final QName qname = new QName(name, getDomain());
		groups.computeIfAbsent(qname, q -> new HashSet<>())
			.addAll(Arrays.asList(roles));
	}

	@Override
	public String getName() {
		return domain;
	}

	/**
	 * Return the account matching the given principal. If the principal is null,
	 * the anonymous account is returned. If no matching account is configured, null
	 * is returned.
	 */
	private Account resolveAccount(Session session) {
		if (session == null)
			return anonymous;

		if (session instanceof Account) {
			final Account acc = (Account) session;
			return acc.parent == this ? acc : null;
		}

		// TODO: make foreign principal handling optional.
		return resolveAccount(session.getPrincipal().getId(), session.getPrincipal().getDomain());
	}

	private Account resolveAccount(String name, String domain) {
		if (domain == null)
			domain = getDomain();
		return accounts.get(new QName(name, domain));
	}

	@Override
	public CheckResult isPermitted(Session session, Permission p) {
		final Account acc = resolveAccount(session);
		if (acc == null)
			return CheckResult.UNKNOWN;

		// TODO: Cache these.

		// Check direct permissions
		for (final StringPermission accp : acc.permissions)
			if (accp.implies(p))
				return CheckResult.YES;

		// Check role->permissions
		for (final String role : acc.roles)
			for (final StringPermission accp : roles.getOrDefault(role, Collections.emptySet()))
				if (accp.implies(p))
					return CheckResult.YES;

		// Check group->role->permissions
		for (final QName group : acc.groups)
			for (final String role : groups.getOrDefault(group, Collections.emptySet()))
				for (final StringPermission accp : roles.getOrDefault(role, Collections.emptySet()))
					if (accp.implies(p))
						return CheckResult.YES;

		return CheckResult.UNKNOWN;
	}

	@Override
	public CheckResult isMemberOf(Session session, String groupName, String groupDomain) {
		final Account acc = resolveAccount(session);
		if(acc != null && acc.groups.contains(new QName(groupName, groupDomain)))
			return CheckResult.YES;
		return CheckResult.UNKNOWN;
	}

	@Override
	public Session login(Credentials request) {

		if (request instanceof UsernamePasswordCredentials) {
			final UsernamePasswordCredentials creds = (UsernamePasswordCredentials) request;
			final Account account = resolveAccount(creds.getName(), creds.getDomain());
			if (account != null && account.checkPassword(creds.getPassword()))
				return new SimpleSession(account, false);
		}

		if (request instanceof KnownPrincipalCredentials) {
			final Principal principal = ((KnownPrincipalCredentials) request).getPrincipal();
			if (principal instanceof Account && ((Account) principal).parent == this)
				return new SimpleSession((Account) principal, true);
		}

		return null;
	}

	@Override
	public void logout(Session who) {
		// Nothing to do
	}

}
