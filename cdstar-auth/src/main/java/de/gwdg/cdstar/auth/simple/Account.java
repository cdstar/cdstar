package de.gwdg.cdstar.auth.simple;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.auth.Principal;
import de.gwdg.cdstar.auth.StringPermission;

public class Account implements Principal {
	QName name;

	byte[] passwordHash;
	byte[] signKey;
	final Set<QName> groups = new HashSet<>();
	final Set<String> roles = new HashSet<>();
	final Set<StringPermission> permissions = new HashSet<>();
	final SimpleAuthorizer parent;

	final Map<String, String> properties = new HashMap<>();

	public Set<String> getRoles() {
		return roles;
	}

	public Set<QName> getGroups() {
		return groups;
	}

	public Set<StringPermission> getPermissions() {
		return permissions;
	}

	Account(SimpleAuthorizer parent, QName name) {
		this.parent = parent;
		this.name = name;
	}

	public Account password(String password) {
		return password(password.toCharArray());
	}

	public Account password(char[] password) {
		final byte[] key = Utils.randomBytes(256);
		password(Utils.sign(Utils.toBytes(password), key), key);
		return this;
	}

	public Account password(byte[] passwordHash, byte[] signKey) {
		this.passwordHash = passwordHash;
		this.signKey = signKey;
		return this;
	}

	public Account withRoles(String... roles) {
		this.roles.addAll(Arrays.asList(roles));
		return this;
	}

	public Account withGroups(String... groups) {
		for (final String g : groups) {
			this.groups.add(new QName(g, getDomain()));
		}
		return this;
	}

	public Account withPermissions(String... permissions) {
		for (final String p : permissions) {
			this.permissions.add(StringPermission.parse(p));
		}
		return this;
	}

	public Account withPermissions(StringPermission... permissions) {
		this.permissions.addAll(Arrays.asList(permissions));
		return this;
	}

	public boolean checkPassword(char[] password) {
		if (passwordHash != null)
			return Utils.signCheck(passwordHash, Utils.toBytes(password), signKey);
		return false;
	}

	public boolean hasRole(String role) {
		if (roles.contains(role))
			return true;
		for (final QName group : groups) {
			if (parent.groups.getOrDefault(group, Collections.emptySet()).contains(role))
				return true;
		}
		return false;
	}

	@Override
	public String getId() {
		return name == null ? null : name.getName();
	}

	@Override
	public String getDomain() {
		return name == null ? null : name.getDomain();
	}

	@Override
	public String getProperty(String propertyName) {
		return properties.get(propertyName);
	}

	@Override
	public Collection<String> getPropertyNames() {
		return properties.keySet();
	}

	@Override
	public String setProperty(String propertyName, String value) {
		return properties.put(propertyName, value);
	}

	QName getQName() {
		return name;
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + "(" + getFullId() + ")";
	}

}