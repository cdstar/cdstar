package de.gwdg.cdstar.auth.realm;

import java.time.Duration;
import java.time.Instant;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import de.gwdg.cdstar.auth.Credentials;
import de.gwdg.cdstar.auth.Principal;
import de.gwdg.cdstar.auth.Session;
import de.gwdg.cdstar.auth.TokenCredentials;
import de.gwdg.cdstar.runtime.Config;
import de.gwdg.cdstar.runtime.ConfigException;
import de.gwdg.cdstar.runtime.Plugin;

/**
 * A {@link SessionStore} (and {@link Authenticator}) that stores sessions
 * in-memory.
 */
@Plugin
public class InMemorySessionStore implements SessionStore, Authenticator {
	private final String name;
	private final Map<String, TokenSession> tokenStore = new ConcurrentHashMap<>();
	private Duration defaultTTL;

	class TokenSession implements Session {
		private final String id;
		private final Instant expireAfter;
		private final Principal principal;

		public TokenSession(Principal principal) {
			this(principal, null);
		}

		public TokenSession(Principal principal, Instant expireAfter) {
			id = UUID.randomUUID().toString();
			this.principal = principal;
			this.expireAfter = expireAfter;
		}

		public String getId() {
			return id;
		}

		public boolean isExpired(Instant now) {
			return expireAfter != null && expireAfter.isBefore(now);
		}

		@Override
		public boolean isRemembered() {
			return true;
		}

		@Override
		public Authenticator getAuthenticator() {
			return InMemorySessionStore.this;
		}

		@Override
		public Principal getPrincipal() {
			return principal;
		}
	}

	public InMemorySessionStore() {
		this("token-authorizer");
	}

	public InMemorySessionStore(Config config) throws ConfigException {
		this(config.get("_name"));
		config.setDefault("ttl", "86400");
		setTimeout(Duration.ofSeconds(config.getInt("ttl")));
	}

	private void setTimeout(Duration ttl) {
		defaultTTL = ttl;
	}

	public InMemorySessionStore(String name) {
		this.name = "token-authorizer";
	}

	@Override
	public String remember(Session session) {
		final Instant expireTime = defaultTTL == null ? null : Instant.now().plus(defaultTTL);
		final TokenSession token = new TokenSession(session.getPrincipal(), expireTime);
		tokenStore.put(token.getId(), token);

		// Occasionally scan for expired tokens if the store grows too big
		if (tokenStore.size() > 1024 && token.getId().hashCode() % 16 == 0)
			expireAll();

		return token.getId();
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public Session login(Credentials request) {
		if (!(request instanceof TokenCredentials))
			return null;

		final TokenSession token = tokenStore.get(((TokenCredentials) request).getToken());
		if (token == null)
			return null;

		if (token.isExpired(Instant.now())) {
			tokenStore.remove(token.getId());
			return null;
		}

		return token;
	}

	void expireAll() {
		final Instant now = Instant.now();
		tokenStore.values().removeIf(t -> t.isExpired(now));
	}

	@Override
	public void logout(Session who) {
		if (who instanceof TokenSession)
			tokenStore.remove(((TokenSession) who).getId());
	}

}
