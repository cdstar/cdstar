package de.gwdg.cdstar.auth.simple;

import de.gwdg.cdstar.auth.Principal;
import de.gwdg.cdstar.auth.Session;
import de.gwdg.cdstar.auth.realm.Authenticator;

class SimpleSession implements Session {
	private final Account account;
	private final boolean remembered;

	public SimpleSession(Account account, boolean remembered) {
		this.account = account;
		this.remembered = remembered;
	}

	@Override
	public boolean isRemembered() {
		return remembered;
	}

	@Override
	public Authenticator getAuthenticator() {
		return account.parent;
	}

	@Override
	public Principal getPrincipal() {
		return account;
	}

}
