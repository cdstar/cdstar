package de.gwdg.cdstar.auth.simple;

import java.util.Objects;

/**
 * A qualified name consists of a local name and a domain or realm identifier.
 * The string representation of a {@link QName} uses the '@' character as a
 * separator. The local name part may contain '@' (e.g. group names may start
 * with an '@') but the domain part must not. When parsing a string into a
 * {@link QName}, the last '@' is used as a separator.
 */
public class QName {

	private final String name;
	private final String domain;

	public QName(String name, String domain) {
		Objects.requireNonNull(name);
		Objects.requireNonNull(domain);
		if (!isValidDomain(domain))
			throw new IllegalArgumentException("QName domains must not contain '@': " + domain);

		this.name = name;
		this.domain = domain;
	}

	/**
	 * Parse a '@' separated string into a {@link QName}. The last '@' character is
	 * used as a separator. If not '@' character is in the input string, or if the
	 * string starts with an '@' and thus would not contain a local part, the whole
	 * input string is used as the local part and the defaultDoomain parameter takes
	 * effect.
	 */
	public static QName fromString(String value, String defaultDomain) {
		final int i = value.lastIndexOf('@');

		// Group names may start with '@' so only indices above 0 are
		// considered.
		if (i < 1) {
			return new QName(value, defaultDomain);
		} else {
			return new QName(value.substring(0, i), value.substring(i + 1));
		}
	}

	public String getName() {
		return name;
	}

	public String getDomain() {
		return domain;
	}

	@Override
	public int hashCode() {
		return name.hashCode() ^ domain.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null || !(obj instanceof QName))
			return false;
		final QName other = (QName) obj;
		return name.equals(other.name) && domain.equals(other.domain);
	}

	public static boolean isValidDomain(String domain) {
		return domain.indexOf('@') == -1;
	}

}
