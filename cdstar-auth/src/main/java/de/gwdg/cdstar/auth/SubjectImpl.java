package de.gwdg.cdstar.auth;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.gwdg.cdstar.auth.realm.Authenticator;
import de.gwdg.cdstar.auth.realm.Authorizer;
import de.gwdg.cdstar.auth.realm.GroupResolver;
import de.gwdg.cdstar.auth.realm.PermissionResolver;
import de.gwdg.cdstar.auth.realm.SessionStore;

public class SubjectImpl implements Subject {
	private final AuthConfigImpl config;
	static Logger log = LoggerFactory.getLogger(AuthConfigImpl.class);

	private Session session;
	private String rememberToken;

	public SubjectImpl(AuthConfigImpl config) {
		this.config = config;
	}

	@Override
	public boolean tryLogin(Credentials request) {
		if (hasPrincipal())
			logout();

		for (final Authenticator realm : config.getRealmsByClass(Authenticator.class)) {
			session = realm.login(request);
			if (session != null) {
				log.debug("Login accepted: [{}] -> [{}]", request, this);
				return true;
			}
		}
		log.debug("Login failed: [{}]", request);
		return false;
	}

	@Override
	public Principal getPrincipal() {
		if (session == null)
			return null;
		return session.getPrincipal();
	}

	@Override
	public AuthConfig getConfig() {
		return config;
	}

	@Override
	public boolean isMemberOf(String groupName, String groupDomain) {
		if (session == null)
			return false;

		if(groupDomain == null)
			groupDomain = getPrincipal().getDomain();

		// Check original permission
		GroupResolver grantingGroupResolver = null;
		for (final GroupResolver gs : config.getRealmsByClass(GroupResolver.class)) {
			switch (gs.isMemberOf(session, groupName, groupDomain)) {
			case NO:
				log.debug("Group check denied: [{}@{}] for [{}] by [{}]", groupName, groupDomain, this, gs);
				return false;
			case YES:
				if(grantingGroupResolver == null)
					grantingGroupResolver = gs;
				continue;
			case UNKNOWN:
			default:
				continue;
			}
		}

		if(grantingGroupResolver != null) {
			log.debug("Group check confirmed: [{}@{}] for [{}] by [{}]", groupName, groupDomain, this, grantingGroupResolver);
			return true;
		}

		log.debug("Group check denied: [{}@{}] for [{}]", groupName, groupDomain, this);
		return false;

	}

	@Override
	public boolean isPermitted(Permission p) {
		return permittedBy(p) != null;
	}

	protected Authorizer permittedBy(Permission p) {
		if (session == null)
			return null;

		// Collect derived permissions
		final List<Permission> derieved = new ArrayList<>();
		derieved.add(p);
		for (final PermissionResolver realm : config.getRealmsByClass(PermissionResolver.class)) {
			for (final Permission pr : realm.resolve(p)) {
				derieved.add(pr);
			}
		}

		// Check permission. We need at least one PERMIT and no DENY for success.
		Authorizer permittetBy = null;
		final List<Authorizer> authorizers = config.getRealmsByClass(Authorizer.class);
		for (final Permission pr : derieved) {
			for (final Authorizer auth : authorizers) {
				switch (auth.isPermitted(session, p)) {
				case NO:
					log.info("Permission [{}] denied for [{}] by [{}]", pr, this, auth);
					return null;
				case YES:
					log.debug("Permission [{}] granted for [{}] by [{}]", pr, this, auth);
					permittetBy = auth;
					break;
				case UNKNOWN:
				default:
					break;
				}
			}
		}

		if (permittetBy != null)
			return permittetBy;

		log.info("Permission [{}] not granted for [{}] by any installed authenticator", p, this);
		return null;
	}

	@Override
	public boolean isRemembered() {
		return session != null && session.isRemembered();
	}

	@Override
	public String remember() {
		if (session == null)
			throw new IllegalStateException("Cannot remember unauthenticated session");
		if (rememberToken != null)
			return rememberToken;
		for (final SessionStore realm : config.getRealmsByClass(SessionStore.class)) {
			rememberToken = realm.remember(session);
			if (rememberToken != null)
				return rememberToken;
		}
		throw new UnsupportedOperationException("No realm was able to remember this subject.");
	}

	@Override
	public boolean hasPrincipal() {
		return session != null;
	}

	@Override
	public void logout() {
		session.getAuthenticator().logout(session);
		session = null;
	}

	@Override
	public String toString() {
		if (isAnonymous())
			return "anonymous";
		return getPrincipal().toString();
	}

}
