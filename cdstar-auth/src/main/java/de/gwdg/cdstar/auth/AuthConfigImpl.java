package de.gwdg.cdstar.auth;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import de.gwdg.cdstar.auth.error.LoginFailed;
import de.gwdg.cdstar.auth.realm.CredentialsResolver;
import de.gwdg.cdstar.auth.realm.Realm;

public class AuthConfigImpl implements AuthConfig {

	List<Realm> realms = new ArrayList<>();
	ConcurrentHashMap<Class<? extends Realm>, List<? extends Realm>> cache = new ConcurrentHashMap<>();

	public AuthConfigImpl(Realm... realms) {
		Arrays.asList(realms).forEach(this::addRealm);
	}

	@Override
	public void addRealm(Realm realm) {
		realms.add(realm);
		cache.clear();
		realm.setConfig(this);
	}

	@Override
	public boolean removeRealm(Realm realm) {
		try {
			return realms.remove(realm);
		} finally {
			cache.clear();
		}
	}

	@Override
	public List<Realm> listRealms() {
		return new ArrayList<>(realms);
	}

	@Override
	@SuppressWarnings("unchecked")
	public <T extends Realm> List<T> getRealmsByClass(Class<T> klass) {
		return (List<T>) cache.computeIfAbsent(klass, k -> {
			final ArrayList<T> result = new ArrayList<>();
			for (final Realm r : realms) {
				if (klass.isInstance(r))
					result.add((T) r);
			}
			result.trimToSize();
			return Collections.unmodifiableList(result);
		});
	}

	@Override
	public Subject getSubject() {
		return new SubjectImpl(this);
	}

	@Override
	public Subject getSubject(Object context) throws LoginFailed {
		final SubjectImpl subject = new SubjectImpl(this);
		if (context instanceof Credentials) {
			subject.login((Credentials) context);
			return subject;
		}

		for (final CredentialsResolver realm : getRealmsByClass(CredentialsResolver.class)) {
			final Credentials creds = realm.getCredentials(context);
			if (creds != null)
				subject.login(creds);
		}

		return subject;
	}

}
