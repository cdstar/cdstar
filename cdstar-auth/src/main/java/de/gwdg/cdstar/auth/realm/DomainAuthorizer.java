package de.gwdg.cdstar.auth.realm;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.gwdg.cdstar.auth.Permission;
import de.gwdg.cdstar.auth.Session;
import de.gwdg.cdstar.auth.StringPermission;
import de.gwdg.cdstar.auth.simple.QName;
import de.gwdg.cdstar.runtime.Config;
import de.gwdg.cdstar.runtime.ConfigException;
import de.gwdg.cdstar.runtime.Plugin;

/**
 * An {@link Authorizer} and {@link GroupResolver} that grants statically
 * configured {@link StringPermission}s and group memberships to all principals
 * of a realm domain. It is designed to authorize principals from other realms,
 * for example LDAP.
 */
@Plugin
public class DomainAuthorizer implements Authorizer, GroupResolver {
	private static final Logger log = LoggerFactory.getLogger(DomainAuthorizer.class);

	private final String name;
	private final Map<String, Set<StringPermission>> domainGrants = new HashMap<>();
	private final Map<String, Set<QName>> domainGroups = new HashMap<>();

	public DomainAuthorizer() {
		name = "domain-authorizer";
	}

	public DomainAuthorizer(Config config) throws ConfigException {
		name = config.get("_name");

		for (final Entry<String, Config> entry : config.getTable().entrySet()) {
			final String domain = entry.getKey();
			final Config cfg = entry.getValue();
			if (cfg.hasKey("groups")) {
				addDomainGroup(domain, cfg.getArray("groups"));
			}
			if (cfg.hasKey("permissions")) {
				for (final String p : cfg.getArray("permissions")) {
					addDomainPermission(domain, StringPermission.parse(p));
				}
			}
		}
	}

	public void addDomainPermission(String domain, StringPermission... grants) {
		log.info("Adding domain permissions for {}: {}", domain, grants);
		final Set<StringPermission> set = domainGrants.computeIfAbsent(domain, k -> new HashSet<>());
		for (final StringPermission g : grants) {
			set.add(g);
		}
	}

	public void addDomainGroup(String domain, String... groups) {
		log.info("Adding domain groups for {}: {}", domain, groups);
		final Set<QName> set = domainGroups.computeIfAbsent(domain, k -> new HashSet<>());
		for (final String g : groups) {
			set.add(QName.fromString(g, domain));
		}
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public CheckResult isMemberOf(Session session, String groupName, String groupDomain) {
		final String domain = session.getPrincipal().getDomain();
		final Set<QName> groups = domainGroups.get(domain);
		if(groups != null && groups.contains(new QName(groupName, groupDomain)))
			return CheckResult.YES;
		return CheckResult.UNKNOWN;
	}

	@Override
	public CheckResult isPermitted(Session session, Permission p) {
		if (!(p instanceof StringPermission))
			return CheckResult.UNKNOWN;

		final String domain = session.getPrincipal().getDomain();
		final Set<StringPermission> grants = domainGrants.get(domain);
		if (grants == null || grants.isEmpty())
			return CheckResult.UNKNOWN;
		for (final StringPermission grant : grants)
			if (grant.implies(p))
				return CheckResult.YES;
		return CheckResult.UNKNOWN;
	}

}
