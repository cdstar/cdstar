package de.gwdg.cdstar.auth.realm;

import java.io.Console;
import java.util.Arrays;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.gwdg.cdstar.Utils;
import de.gwdg.cdstar.auth.simple.Account;
import de.gwdg.cdstar.auth.simple.SimpleAuthorizer;
import de.gwdg.cdstar.runtime.Config;
import de.gwdg.cdstar.runtime.ConfigException;
import de.gwdg.cdstar.runtime.Plugin;

@Plugin
public class StaticRealm extends SimpleAuthorizer {
	private static final Logger log = LoggerFactory.getLogger(StaticRealm.class);

	public StaticRealm(Config config) throws ConfigException {
		super(config.get("domain", config.get("_name", "static")));

		final Config roleConf = config.with("role");
		for (final String roleName : roleConf.keySet()) {
			if (roleName.contains(Config.SEP)) {
				log.warn("Role names must not contain dots: {}", roleName);
				continue;
			}
			addRole(roleName, roleConf.getArray(roleName));
			log.info("Added role '{}' to realm '{}'", roleName, getName());
		}

		final Config groupConf = config.with("group");
		for (final String groupName : groupConf.keySet()) {
			if (groupName.contains(Config.SEP)) {
				log.warn("Group names must not contain dots: {}", groupName);
				continue;
			}
			addGroup(groupName, groupConf.getArray(groupName));
			log.info("Added group '{}' to realm '{}'", groupName, getName());
		}

		for (final Entry<String, Config> e : config.getTable("user").entrySet()) {
			final Config cfg = e.getValue();
			String user = e.getKey();
			final Account acc;
			int index;

			// Override username if specified as a value (required if names contain a dot).
			if (cfg.hasKey("name"))
				user = cfg.get("name");

			if ((index = user.lastIndexOf("@")) != -1) {
				acc = account(user.substring(0, index), user.substring(index + 1));
			} else {
				acc = account(user);
			}

			if (cfg.hasKey("password")) {
				final String password = cfg.get("password");
				if ((index = password.indexOf(":")) == -1)
					throw new ConfigException("Invalid pasword hash for user: " + user);
				final byte[] key = Utils.base64decode(password.substring(0, index));
				final byte[] hash = Utils.base64decode(password.substring(index + 1));
				acc.password(hash, key);
			}

			if (cfg.hasKey("roles"))
				acc.withRoles(cfg.getArray("roles"));

			if (cfg.hasKey("groups"))
				acc.withGroups(cfg.getArray("groups"));

			if (cfg.hasKey("permissions"))
				acc.withPermissions(cfg.getArray("permissions"));

			log.info("Added user '{}' to realm '{}'", acc.getFullId(), getName());
		}

	}

	public static String hashPassword(char[] password) {
		final byte[] key = Utils.randomBytes(32);
		return Utils.base64encode(key) + ':'
				+ Utils.base64encode(Utils.sign(Utils.toBytesAndBurnAfterReading(password), key));
	}

	public static void main(String[] args) {
		final Console console = System.console();
		if (console == null) {
			System.out.println("Couldn't get Console instance");
			System.exit(0);
			return;
		}

		final char password[] = console.readPassword("Enter password: ");
		final char password2[] = console.readPassword("Repeat password: ");
		if (!Arrays.equals(password, password2)) {
			console.printf("Passwords do not match.%n");
			System.exit(1);
			return;
		}

		console.printf("Password hash: %s%n", hashPassword(password));
	}

}
